package com.fitzoh.app.vectorchildfinder;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ${Deven} on 2/1/18.
 * PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
 * myImageView.setColorFilter(porterDuffColorFilter);
 */

public class VectorChildFinder {

    private VectorDrawableCompat vectorDrawable;

    /**
     * @param resources Your Activity Context
     * @param vectorRes Path of your vector drawable resource
     * @param imageView ImaveView that are showing vector drawable
     */
    public VectorChildFinder(Resources resources, int vectorRes, FloatingActionButton imageView) {
        vectorDrawable = VectorDrawableCompat.create(resources,
                vectorRes, null);
        vectorDrawable.setAllowCaching(false);
        imageView.setImageDrawable(vectorDrawable);
    }

    public VectorChildFinder(Resources resources, int vectorRes, ImageView imageView) {
        vectorDrawable = VectorDrawableCompat.create(resources,
                vectorRes, null);
        vectorDrawable.setAllowCaching(false);
        imageView.setImageDrawable(vectorDrawable);
    }

    public VectorChildFinder(Resources resources, int vectorRes, MenuItem imageView) {
        vectorDrawable = VectorDrawableCompat.create(resources,
                vectorRes, null);
        vectorDrawable.setAllowCaching(false);
        imageView.setIcon(vectorDrawable);
    }

    public VectorChildFinder(Resources resources, int vectorRes, TextView imageView) {
        vectorDrawable = VectorDrawableCompat.create(resources,
                vectorRes, null);
        vectorDrawable.setAllowCaching(false);
//        imageView.setImageDrawable(vectorDrawable);
    }

    public Drawable getDrawable() {
        return vectorDrawable;
    }

    /**
     * @param pathName Path name that you gave in vector drawable file
     * @return A Object type of VectorDrawableCompat.VFullPath
     */
    public VectorDrawableCompat.VFullPath findPathByName(String pathName) {
        return (VectorDrawableCompat.VFullPath) vectorDrawable.getTargetByName(pathName);
    }

    /**
     * @param groupName Group name that you gave in vector drawable file
     * @return A Object type of VectorDrawableCompat.VGroup
     */
    public VectorDrawableCompat.VGroup findGroupByName(String groupName) {
        return (VectorDrawableCompat.VGroup) vectorDrawable.getTargetByName(groupName);
    }

}
