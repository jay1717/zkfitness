package com.fitzoh.app.network;

import android.content.Context;

import com.fitzoh.app.ApplicationClass;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    private static final int OKHTTP_TIMEOUT = 60; // seconds
    private static Retrofit retrofit = null;
    private static OkHttpClient okHttpClient;
    private static Context mcontext;
    private static Interceptor networkInterceptor;

    public static Retrofit getClient(Context context, Interceptor interceptor) {
        mcontext = context;
        networkInterceptor = interceptor;
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(WebService.baseUrl)
                .client(getOKHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit;
    }

 public static Retrofit getYoutubeClient(Context context, Interceptor interceptor) {
        mcontext = context;
        networkInterceptor = interceptor;
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(WebService.youtubebaseUrl)
                .client(getOKHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit;
    }


    private static OkHttpClient getOKHttpClient() {

        Cache cache = new Cache(new File(ApplicationClass.getAppContext().getCacheDir(), "http")
                , DISK_CACHE_SIZE);
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
                .cache(cache);

        if (networkInterceptor != null) {
            builder.addNetworkInterceptor(networkInterceptor);
        }

//        if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loggingInterceptor);
//        }
        okHttpClient = builder.build();
        return okHttpClient;
    }

    public interface WebService {
        //        String baseUrl = "http://192.168.1.35:8000/";
//        String baseUrl = "http://192.168.1.46:4012/";
       // String baseUrl = "https://php6.shaligraminfotech.com/fitzoh/public/";
       String baseUrl = "https://www.fitzoh.com/";
       String youtubebaseUrl = "https://www.googleapis.com/youtube/";
       // String baseUrl = "https://php6.shaligraminfotech.com/fitzoh/public/";
      // String baseUrl = "https://www.fitzoh.com/";

        // String baseUrl = "http://php6.shaligraminfotech.com/fitzoh_test/public/";
    }
}