package com.fitzoh.app.network;

import com.facebook.AccessToken;
import com.fitzoh.app.ApplicationClass;
import com.fitzoh.app.utils.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AuthorizedNetworkInterceptor implements Interceptor {
    // region Member Variables
    private String accessToken;
    public String device_type = "android";
    public String device_token = "";
    public String x_api_key = "qiqhyah5kBu9XdyNa1KQsMVhxR";
    public String id = "0";
    public SessionManager session;

    // endregion

    // region Constructors
    public AuthorizedNetworkInterceptor(String accessToken, String id) {
        this.accessToken = accessToken;
        this.id = id;
    }
    // endregion

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (chain != null) {
            Request originalRequest = chain.request();
            session = new SessionManager(ApplicationClass.getAppContext());

            if (accessToken != null) {
                Map<String, String> headersMap = new HashMap<>();
             //   device_token = session.getNotificationToken();
//                headersMap.put("Authorization", String.format("%s %s", accessToken.getTokenType(), accessToken.getAccessToken()));
                headersMap.put("X-Api-Key", x_api_key);
                headersMap.put("device-type", device_type);
                headersMap.put("device-token", FirebaseInstanceId.getInstance().getToken());
                headersMap.put("Id", id);
                headersMap.put("User-Access-Token", accessToken);
               /* headersMap.put("Id", "103");
                headersMap.put("User-Access-Token", "idab102ac79af94b06aebf955");*/
                Request modifiedRequest = RequestUtility.updateHeaders(originalRequest, headersMap);

                return chain.proceed(modifiedRequest);
            } else {
                return chain.proceed(originalRequest);
            }
        }

        return null;
    }
}
