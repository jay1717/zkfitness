package com.fitzoh.app.network;

import android.content.Context;

import com.fitzoh.app.ApplicationClass;
import com.fitzoh.app.utils.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Hiren on 13/09/18.
 */

public class UnauthorizedNetworkInterceptor implements Interceptor {

    // region Member Variables
    public Context context;
    public String device_type = "android";
    public String device_token = "";
    public String x_api_key = "qiqhyah5kBu9XdyNa1KQsMVhxR";
    public SessionManager session;

    // endregion

    // region Constructors
    public UnauthorizedNetworkInterceptor(Context context) {
        this.context = context;

    }
    // endregion

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (chain != null) {
            Request originalRequest = chain.request();
            session = new SessionManager(ApplicationClass.getAppContext());
            Map<String, String> headersMap = new HashMap<>();

           /* String clientId = context.getString(R.string.client_id);
            String clientSecret = context.getString(R.string.client_secret);*/

            // concatenate username and password with colon for authentication
//            final String credentials = clientId + ":" + clientSecret;
//            String authorization = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            //device_token = session.getNotificationToken();

            headersMap.put("X-Api-Key", x_api_key);
            headersMap.put("device-type", device_type);
            headersMap.put("device-token", FirebaseInstanceId.getInstance().getToken());
            Request modifiedRequest = RequestUtility.updateHeaders(originalRequest, headersMap);

            return chain.proceed(modifiedRequest);
        }


        return null;
    }
}
