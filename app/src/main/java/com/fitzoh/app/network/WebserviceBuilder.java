package com.fitzoh.app.network;


import com.fitzoh.app.model.AchivementModel;
import com.fitzoh.app.model.AddClientGroupListModel;
import com.fitzoh.app.model.AddClientGroupModel;
import com.fitzoh.app.model.AddClientSchedule;
import com.fitzoh.app.model.AddDietResponse;
import com.fitzoh.app.model.AddWorkoutResponse;
import com.fitzoh.app.model.BankDetailModel;
import com.fitzoh.app.model.CertificationModel;
import com.fitzoh.app.model.CheckInDataModel;
import com.fitzoh.app.model.CheckInFormListModel;
import com.fitzoh.app.model.CheckinResponse;
import com.fitzoh.app.model.ClientEditWorkoutModel;
import com.fitzoh.app.model.ClientGroupListModel;
import com.fitzoh.app.model.ClientInquiryHistoryModel;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.ClientProfileAddModel;
import com.fitzoh.app.model.ClientProfilePackageList;
import com.fitzoh.app.model.ClientProgressModel;
import com.fitzoh.app.model.ClientShopListModel;
import com.fitzoh.app.model.ClientUpdateDetailData;
import com.fitzoh.app.model.ClientUpdateListData;
import com.fitzoh.app.model.ClientViewCartModel;
import com.fitzoh.app.model.ClientWorkoutEditModel;
import com.fitzoh.app.model.ClientWorkoutTrackingModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DeleteWorkOutModel;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.model.EditBasicTrainerModel;
import com.fitzoh.app.model.EquipmentListModel;
import com.fitzoh.app.model.ExerciseDetailData;
import com.fitzoh.app.model.ExerciseListModel;
import com.fitzoh.app.model.ExerciseSetListModel;
import com.fitzoh.app.model.FacilitiesListModel;
import com.fitzoh.app.model.FindPlanDietModel;
import com.fitzoh.app.model.FindPlanTrainingProgramModel;
import com.fitzoh.app.model.FindPlanWorkoutModel;
import com.fitzoh.app.model.FitnessCenterListModel;
import com.fitzoh.app.model.FitnessValuesModel;
import com.fitzoh.app.model.GetClientDetailModel;
import com.fitzoh.app.model.GetDocumentModel;
import com.fitzoh.app.model.GetFitzohProductModel;
import com.fitzoh.app.model.GetTrainerModel;
import com.fitzoh.app.model.GymClientListModel;
import com.fitzoh.app.model.GymSubscriptionModel;
import com.fitzoh.app.model.GymTrainerScheduleListModel;
import com.fitzoh.app.model.IFSCDetail;
import com.fitzoh.app.model.InquiryModel;
import com.fitzoh.app.model.LiveClientDetailModel;
import com.fitzoh.app.model.LiveClientsModel;
import com.fitzoh.app.model.LogWorkoutDataModel;
import com.fitzoh.app.model.MealListModel;
import com.fitzoh.app.model.MedicalFormModel;
import com.fitzoh.app.model.MenuNotificationResponseModel;
import com.fitzoh.app.model.MuscleListModel;
import com.fitzoh.app.model.NutritionValuesData;
import com.fitzoh.app.model.PackageListModel;
import com.fitzoh.app.model.ProfileGymSubscriptionModel;
import com.fitzoh.app.model.ProfilePackageModel;
import com.fitzoh.app.model.PurchaseHistoryModel;
import com.fitzoh.app.model.PurchaseHistoryTrainerModel;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.model.RequestListModel;
import com.fitzoh.app.model.ScheduleListModel;
import com.fitzoh.app.model.SearchDietPlanFoodData;
import com.fitzoh.app.model.SendNotiGetUserModel;
import com.fitzoh.app.model.ServingSizeData;
import com.fitzoh.app.model.ServingSizeUnitData;
import com.fitzoh.app.model.ShopBrandModel;
import com.fitzoh.app.model.ShopCategoryModel;
import com.fitzoh.app.model.SocialLoginModel;
import com.fitzoh.app.model.SpecialityListModel;
import com.fitzoh.app.model.SubscriptionResultData;
import com.fitzoh.app.model.SubscriptionTypeListModel;
import com.fitzoh.app.model.TotalNutritionModel;
import com.fitzoh.app.model.TrainerGalleryModel;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.model.TrainerProductInquiryListing;
import com.fitzoh.app.model.TrainerProfileAddModel;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.model.TrainerShopListModel;
import com.fitzoh.app.model.TrainingProgramClientList;
import com.fitzoh.app.model.TrainingProgramDetailClientData;
import com.fitzoh.app.model.TrainingProgramEditModel;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.model.TrainingSessionModel;
import com.fitzoh.app.model.TransformationListModel;
import com.fitzoh.app.model.VerificationOTPModel;
import com.fitzoh.app.model.ViewWorkoutNotesModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.model.WorkoutParamModel;
import com.fitzoh.app.model.WorkoutReportModel;
import com.fitzoh.app.model.YouTubeModel.YoutubeSearchModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Declare all the APIs in this class with specific interface
 * e.g. Profile for Login/Register Apis
 */
public interface WebserviceBuilder {
    @FormUrlEncoded
    @POST("api/auth/signup")
    Observable<RegisterModel> signup(@Field("role_id") int role_id,
                                     @Field("name") String name,
                                     @Field("email") String email,
                                     @Field("birth_date") String birth_date,
                                     @Field("gender") String gender,
                                     @Field("is_social_login") int is_social_login,
                                     @Field("mobile_number") String mobile_number,
                                     @Field("country_code") String country_code,
                                     @Field("id") String id);


    @FormUrlEncoded
    @POST("api/auth/login")
    Observable<RegisterModel> login(@Field("username") String email);

    @FormUrlEncoded
    @POST("api/auth/verifyOtp")
    Observable<VerificationOTPModel> varifyotp(@Field("id") int id,
                                               @Field("otp") String otp);

    @FormUrlEncoded
    @POST("api/auth/checkVersion")
    Observable<CommonApiResponse> checkVersion(@Field("version") String version);

    @FormUrlEncoded
    @POST("api/trainer/updateTimezone")
    Observable<CommonApiResponse> updateTimezone(@Field("timezone") String timezone);

    @FormUrlEncoded
    @POST("api/auth/resendOtp")
    Observable<RegisterModel> resendotp(@Field("email") String email);

    @FormUrlEncoded
    @POST("api/auth/socialsignup")
    Observable<SocialLoginModel> socialSignIn(@Field("email") String email,
                                              @Field("mobile_number") String mobile_number,
                                              @Field("provider") String provider,
                                              @Field("provider_id") String provider_id);

    @FormUrlEncoded
    @POST("api/auth/sociallogininput")
    Observable<SocialLoginModel> socialLogin(@Field("email") String email,
                                             @Field("mobile_number") String mobile_number,
                                             @Field("provider_id") String provider_id);

    @FormUrlEncoded
    @POST("api/live/clients")
    Observable<LiveClientsModel> clients(@Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/trainer/clientUpdates")
    Observable<ClientUpdateListData> clientUpdates(@Field("trainer_id") int trainer_id);

    @POST("api/gym/clientUpdates")
    Observable<ClientUpdateListData> gymClientUpdates();

    @FormUrlEncoded
    @POST("api/trainer/clientUpdateDetail")
    Observable<ClientUpdateDetailData> clientUpdateDetail(@Field("client_id") int client_id);

    @FormUrlEncoded
    @POST("api/gym/clientUpdateDetail")
    Observable<ClientUpdateDetailData> gymClientUpdateDetail(@Field("client_id") int client_id);

    @FormUrlEncoded
    @POST("api/gym/client/live")
    Observable<LiveClientsModel> getGymLiveClients(@Field("gym_id") int gym_id);

    @POST("api/gym/trainer_gym_schedule_summary")
    Observable<TrainerListModel> gymScheduleSummary();

    @FormUrlEncoded
    @POST("api/gym/trainer/schedule/get")
    Observable<GymTrainerScheduleListModel> getTrainerScheduleList(@Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/client/getCheckins")
    Observable<CheckInDataModel> getCheckInsList(@Field("client_id") int client_id);

    @Multipart
    @POST("api/client/profile/update")
    Observable<ClientProfileAddModel> create_user_profile(@Part("goal") RequestBody goal,
                                                          @Part("fitness_intrested") RequestBody fitness_interest,
                                                          @Part("address") RequestBody address,
                                                          @Part("life_style") RequestBody life_style,
                                                          @Part("id") RequestBody id,
                                                          @Part("city") RequestBody city,
                                                          @Part("state") RequestBody state,
                                                          @Part("country") RequestBody country,
                                                          @Part("location") RequestBody location,
                                                          @Part("landmark") RequestBody landmark,
                                                          @Part MultipartBody.Part UserPhoto);

    @Multipart
    @POST("api/client/profile/update")
    Observable<ClientProfileAddModel> create_user_profile_without_image(@Part("goal") RequestBody goal,
                                                                        @Part("fitness_intrested") RequestBody fitness_interest,
                                                                        @Part("address") RequestBody address,
                                                                        @Part("life_style") RequestBody life_style,
                                                                        @Part("city") RequestBody city,
                                                                        @Part("state") RequestBody state,
                                                                        @Part("country") RequestBody country,
                                                                        @Part("location") RequestBody location,
                                                                        @Part("landmark") RequestBody landmark,
                                                                        @Part("id") RequestBody id);

    @Multipart
    @POST("api/trainerprofile/add")
    Observable<TrainerProfileAddModel> addTrainerProfile(@Part("about") RequestBody goal,
                                                         @Part("facilities") RequestBody fitness_interest,
                                                         @Part("location") RequestBody address,
                                                         @Part("address") RequestBody life_style,
                                                         @Part("city") RequestBody id,
                                                         @Part("state") RequestBody state,
                                                         @Part("country") RequestBody country,
                                                         @Part MultipartBody.Part photo,
                                                         @Part List<MultipartBody.Part> certificates,
                                                         @Part("landmark") RequestBody landmark
    );

    @Multipart
    @POST("api/trainerprofile/add")
    Observable<TrainerProfileAddModel> addTrainerProfileWithoutImage(@Part("about") RequestBody goal,
                                                                     @Part("facilities") RequestBody fitness_interest,
                                                                     @Part("location") RequestBody address,
                                                                     @Part("address") RequestBody life_style,
                                                                     @Part("city") RequestBody id,
                                                                     @Part("state") RequestBody state,
                                                                     @Part("country") RequestBody country,
                                                                     @Part("landmark") RequestBody landmark,
                                                                     @Part List<MultipartBody.Part> certificates);


    @Multipart
    @POST("api/trainerprofile/edit")
    Observable<CommonApiResponse> EditTrainerProfile(@Part("about") RequestBody goal,
                                                     @Part("facilities") RequestBody fitness_interest,
                                                     @Part("location") RequestBody address,
                                                     @Part("address") RequestBody life_style,
                                                     @Part("city") RequestBody id,
                                                     @Part("state") RequestBody state,
                                                     @Part("country") RequestBody country,
                                                     @Part("landmark") RequestBody landmark,
                                                     @Part("delete_certificate_ids") RequestBody delete_certificate_ids,
                                                     @Part MultipartBody.Part photo,
                                                     @Part List<MultipartBody.Part> certificates);

    @Multipart
    @POST("api/trainerprofile/edit")
    Observable<CommonApiResponse> EditTrainerProfileWithoutImage(@Part("about") RequestBody goal,
                                                                 @Part("facilities") RequestBody fitness_interest,
                                                                 @Part("location") RequestBody address,
                                                                 @Part("address") RequestBody life_style,
                                                                 @Part("city") RequestBody id,
                                                                 @Part("state") RequestBody state,
                                                                 @Part("country") RequestBody country,
                                                                 @Part("landmark") RequestBody landmark,
                                                                 @Part("delete_certificate_ids") RequestBody delete_certificate_ids,
                                                                 @Part List<MultipartBody.Part> certificates);


    @Multipart
    @POST("api/trainerprofile/editBasicTrainerProfile")
    Observable<EditBasicTrainerModel> addBasicTrainerProfile(@Part("name") RequestBody name,
                                                             @Part("gender") RequestBody gender,
                                                             @Part("mobile_number") RequestBody mobile_number,
                                                             @Part("country_code") RequestBody country_code,
                                                             @Part("birth_date") RequestBody birth_date,
                                                             @Part MultipartBody.Part photo);

    @Multipart
    @POST("api/trainerprofile/editBasicTrainerProfile")
    Observable<EditBasicTrainerModel> addBasicTrainerProfileWithoutImage(@Part("name") RequestBody name,
                                                                         @Part("gender") RequestBody gender,
                                                                         @Part("mobile_number") RequestBody mobile_number,
                                                                         @Part("country_code") RequestBody country_code,
                                                                         @Part("birth_date") RequestBody birth_date);

    /* @Multipart
     @POST("api/trainerprofile/add")
     Observable<RegisterModel> addBasicTrainerProfileWithoutImage(@Part("about") RequestBody goal,
                                                             @Part("facilities") RequestBody fitness_interest,
                                                             @Part("location") RequestBody address,
                                                             @Part("address") RequestBody life_style,
                                                             @Part("city") RequestBody id,
                                                             @Part("state") RequestBody state,
                                                             @Part("country") RequestBody country,
                                                             @Part List<MultipartBody.Part> certificates);
 */
    @Multipart
    @POST("api/trainer/gallery/addImages")
    Observable<RegisterModel> addGalleryImages(@Part List<MultipartBody.Part> galleryImage, @Part("delete_ids") RequestBody delete_ids);

    @Multipart
    @POST("api/trainer/add/transformations")
    Observable<RegisterModel> addTransformations(@Part("before_date") RequestBody before_date,
                                                 @Part("after_date") RequestBody after_date,
                                                 @Part("description") RequestBody description,
                                                 @Part MultipartBody.Part before_image,
                                                 @Part MultipartBody.Part after_image);

    @Multipart
    @POST("api/trainer/edit/editTransformations")
    Observable<CommonApiResponse> editTransformations(@Part("before_date") RequestBody before_date,
                                                      @Part("after_date") RequestBody after_date,
                                                      @Part("description") RequestBody description,
                                                      @Part MultipartBody.Part before_image,
                                                      @Part MultipartBody.Part after_image,
                                                      @Part("trasformation_id") RequestBody trasformation_id);

    @FormUrlEncoded
    @POST("api/client/getTrainers")
    Observable<GetTrainerModel> getTrainer(@Field("user_id") int user_id, @Field("speciality") String speciality, @Field("location") String location, @Field("role_id") String role_id);

    @POST("api/fitnessvalues")
    Observable<FitnessValuesModel> getFitnessValue();

    @POST("api/trainer/getFacilities")
    Observable<FacilitiesListModel> getFacilities();


    @FormUrlEncoded
    @POST("api/workout/get")
    Observable<WorkOutListModel> getProfileWorkOuList(@Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/trainer/training_program/client/get")
    Observable<TrainingProgramList> getClientAssignTrainingList(@Field("client_id") int client_id);


//    @FormUrlEncoded
    @POST("api/workout/get")
    Observable<WorkOutListModel> getWorkOuList();

    @FormUrlEncoded
    @POST("api/client/purchaseTrainingProgram")
    Observable<CommonApiResponse> savePurchaseMethod(@Field("training_package_id") int training_package_id, @Field("transaction_id") String transaction_id, @Field("payment_method") String payment_method,
                                                     @Field("payment_status") int payment_status, @Field("start_date") String start_date, @Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/client/getClientWorkoutdetails")
    Observable<ClientEditWorkoutModel> getClientWorkoutData(@Field("workout_id") int workout_id, @Field("client_id") int client_id);

    @POST("api/trainer/shop/viewProductInquiries")
    Observable<InquiryModel> getInquiryList();

    @FormUrlEncoded
    @POST("api/workout/get")
    Observable<WorkOutListModel> getWorkOuList(@Field("is_assigned") int is_assigned,
                                               @Field("client_id") String client_id);


    @POST("api/client/getClientWorkout")
    Observable<WorkOutListModel> getClientWorkOutList();

    @POST("api/client/getClietWorkoutTracking")
    Observable<ClientWorkoutTrackingModel> getClietWorkoutTracking();

    @FormUrlEncoded
    @POST("api/client/getClietWorkoutTracking")
    Observable<ClientWorkoutTrackingModel> getTrainerClietWorkoutTracking(@Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/trainer/getFitzohPlans")
    Observable<FindPlanWorkoutModel> getFindPlanList(@Field("type") String type);

    @FormUrlEncoded
    @POST("api/trainer/getFitzohPlans")
    Observable<FindPlanDietModel> getFindPlanDietList(@Field("type") String type);

    @FormUrlEncoded
    @POST("api/trainer/getFitzohPlans")
    Observable<FindPlanTrainingProgramModel> getFindPlanTrainingList(@Field("type") String type);


    @POST("api/trainer/schedule/get")
    Observable<ScheduleListModel> getSchedualList();

    @FormUrlEncoded
    @POST("api/trainer/schedule/client/get")
    Observable<ScheduleListModel> getClientProfileScheduleList(@Field("client_id") int client_id);

    @FormUrlEncoded
    @POST("api/client/getClientProfile")
    Observable<VerificationOTPModel> getClientProfile(@Field("client_id") int client_id,
                                                      @Field("trainer_id") Integer trainer_id);

    @FormUrlEncoded
    @POST("api/client/getClientProfile")
    Observable<VerificationOTPModel> getClientProfileDetail(@Field("trainer_id") Integer trainer_id);

    @FormUrlEncoded
    @POST("api/auth/relogin")
    Observable<VerificationOTPModel> getRelogin(@Field("id") String id);

    @FormUrlEncoded
    @POST("api/trainer/schedule/delete")
    Observable<CommonApiResponse> deleteSchedual(@Field("id") int id);

    @POST("api/trainer/shop/viewProductInquiries")
    Observable<TrainerProductInquiryListing> getProductInquiryList();

    @POST("api/client/shop/viewSentInquiries")
    Observable<ClientInquiryHistoryModel> getClientHistory();

    @FormUrlEncoded
    @POST("api/trainer/liveClientDetail")
    Observable<LiveClientDetailModel> getLiveClientDetail(@Field("client_id") int client_id);

    @FormUrlEncoded
    @POST("api/gym/liveClientDetail")
    Observable<LiveClientDetailModel> getGymLiveClientDetail(@Field("client_id") int client_id);

    @FormUrlEncoded
    @POST("api/workout/client/get")
    Observable<WorkOutListModel> getClientWorkoutList(@Field("client_id") int client_id);

    @FormUrlEncoded
    @POST("api/exercises/list/workout")
    Observable<WorkoutExerciseListModel> getWorkOutExerciseList(@Field("workout_id") int workout_id,
                                                                @Field("client_id") String client_id);

    //  {{server_url}}/api/client/getClientWorkoutdetails
    @FormUrlEncoded
    @POST("api/client/getClientWorkoutdetails")
    Observable<WorkoutExerciseListModel> getWorkOutExerciseListdetail(@Field("workout_id") int workout_id,
                                                                      @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/client/exercises/list/workout")
    Observable<ClientWorkoutEditModel> clientEditWorkout(@Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/exercises/list/workout")
    Observable<WorkoutExerciseListModel> getWorkoutFindPlanDetail(@Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/client/exercises/list/workout")
    Observable<WorkoutExerciseListModel> getWorkOutExerciseListClient(@Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/client/getClientWorkoutdetails")
    Observable<LogWorkoutDataModel> getClientWorkoutDetails(@Field("workout_id") int workout_id,
                                                            @Field("training_program_id") int training_program_id,
                                                            @Field("is_am_workout") int is_am_workout,
                                                            @Field("weekday_id") int weekday_id,
                                                            @Field("client_log") Integer client_log,
                                                            @Field("client_id") String client_id,
                                                            @Field("is_show_log") int is_show_log);

    @FormUrlEncoded
    @POST("api/client/getClientWorkoutdetails")
    Observable<LogWorkoutDataModel> getClientWorkoutDetails(@Field("workout_id") int workout_id,
                                                            @Field("client_id") String client_id);

    @POST("api/client/saveLogWorkoutDetails")
    Observable<CommonApiResponse> saveLogWorkoutDetails(@Body RequestBody requestBody);

    @POST("api/client/addClientWorkoutTracking")
    Observable<CommonApiResponse> addClientWorkoutTracking(@Body RequestBody requestBody);

    @POST("api/exercises/createSuperSet")
    Observable<CommonApiResponse> createSuperSet(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/exercises/breakSuperSet")
    Observable<CommonApiResponse> breakSuperSet(@Field("exercise_set_id") int exercise_set_id,
                                                @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/workout/delete")
    Observable<DeleteWorkOutModel> deleteWorkOut(@Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/diet/delete")
    Observable<CommonApiResponse> deleteDiet(@Field("diet_plan_id") int diet_plan_id);

    @FormUrlEncoded
    @POST("api/diet/delete")
    Observable<CommonApiResponse> deleteDietClient(@Field("diet_plan_id") int diet_plan_id, @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/meal/delete")
    Observable<CommonApiResponse> deleteDietMeal(@Field("diet_plan_meal_id") int diet_plan_meal_id);

    @FormUrlEncoded
    @POST("api/exercises/delete")
    Observable<DeleteWorkOutModel> deleteWorkOutExercise(@Field("workout_id") int workout_id,
                                                         @Field("exercise_id") int exercise_id,
                                                         @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/client/exercises/delete")
    Observable<DeleteWorkOutModel> deleteWorkOutExerciseClient(@Field("workout_id") int workout_id,
                                                               @Field("exercise_id") int exercise_id,
                                                               @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/exercises/set/deleteExercise")
    Observable<DeleteWorkOutModel> deleteSetExercise(@Field("exercise_id") int exercise_id,
                                                     @Field("exercise_set_id") int exercise_set_id,
                                                     @Field("client_id") String client_id);

    @POST("api/workout/getMuscles")
    Observable<MuscleListModel> getMuscles();

    @POST("api/workout/equipments/get")
    Observable<EquipmentListModel> getEquipments();

    @Multipart
    @POST("api/exercises/create")
    Observable<CommonApiResponse> createExercise(@Part("client_id") RequestBody client_id,
                                                 @Part("workout_id") RequestBody workout_id,
                                                 @Part("equipment_id") RequestBody equipment_id,
                                                 @Part("muscles_id") RequestBody muscles_id,
                                                 @Part("owner_type") RequestBody owner_type,
                                                 @Part("owner_id") RequestBody owner_id,
                                                 @Part("exercise_name") RequestBody exercise_name,
                                                 @Part("exercise_desc") RequestBody exercise_desc,
                                                 @Part("video_title") RequestBody video_title,
                                                 @Part("video_desc") RequestBody video_desc,
                                                 @Part("video_url") RequestBody video_url,
                                                 @Part("instructions") RequestBody instructions,
                                                 @Part MultipartBody.Part image);

    @Multipart
    @POST("api/client/exercises/create")
    Observable<CommonApiResponse> createExerciseClient(@Part("workout_id") RequestBody workout_id,
                                                       @Part("equipment_id") RequestBody equipment_id,
                                                       @Part("muscles_id") RequestBody muscles_id,
                                                       @Part("owner_type") RequestBody owner_type,
                                                       @Part("owner_id") RequestBody owner_id,
                                                       @Part("exercise_name") RequestBody exercise_name,
                                                       @Part("exercise_desc") RequestBody exercise_desc,
                                                       @Part("video_title") RequestBody video_title,
                                                       @Part("video_desc") RequestBody video_desc,
                                                       @Part("video_url") RequestBody video_url,
                                                       @Part("instructions") RequestBody instructions,
                                                       @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("api/trainingsession")
    Observable<TrainingSessionModel> trainingSession(@Field("trainer_id") int trainer_id);


    @FormUrlEncoded
    @POST("api/workout/add")
    Observable<AddWorkoutResponse> addWorkout(@Field("name") String name,
                                              @Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/client/workout/add")
    Observable<AddWorkoutResponse> addClientWorkout(@Field("name") String name,
                                                    @Field("workout_id") Integer workout_id);

    @FormUrlEncoded
    @POST("api/client/startWorkout")
    Observable<CommonApiResponse> startWorkout(@Field("date") String date,
                                               @Field("time") String time,
                                               @Field("workout_id") int workout_id,
                                               @Field("is_am_workout") boolean is_am_workout);

    @FormUrlEncoded
    @POST("api/client/startDiet")
    Observable<CommonApiResponse> startDiet(@Field("date") String date,
                                            @Field("time") String time,
                                            @Field("diet_plan_food_ids") String diet_plan_food_ids,
                                            @Field("diet_plan_food_ids_unchecked") String diet_plan_food_ids_unchecked,
                                            @Field("diet_plan_meal_id") int diet_plan_meal_id);

    @FormUrlEncoded
    @POST("api/diet/add")
    Observable<AddDietResponse> addDiet(@Field("name") String name,
                                        @Field("diet_plan_id") int diet_plan_id,
                                        @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/meal/add")
    Observable<CommonApiResponse> addMeal(@Field("name") String name, @Field("time") String time, @Field("diet_plan_id") int id, @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/meal/edit")
    Observable<CommonApiResponse> editMeal(@Field("name") String name, @Field("time") String time, @Field("diet_plan_id") int diet_plan_id, @Field("diet_plan_meal_id") int diet_plan_meal_id, @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/meal/delete")
    Observable<CommonApiResponse> deleteMealDelete(@Field("diet_plan_meal_id") int diet_plan_meal_id,
                                                   @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/totalNutrition")
    Observable<TotalNutritionModel> getToalNutrition(@Field("diet_plan_id") int diet_plan_id, @Field("client_id") String client_id);


    @POST("api/workout/get")
    Observable<WorkOutListModel> getWorkoutList();

    @FormUrlEncoded
    @POST("api/workout/get")
    Observable<WorkOutListModel> getWorkoutListDialog(@Field("order_by") String order_by);

    @POST("api/client/viewMedicalFormFields")
    Observable<MedicalFormModel> viewMedicalFormFields();

    @POST("api/client/getClientmedicalform")
    Observable<MedicalFormModel> getClientmedicalform();

    @FormUrlEncoded
    @POST("api/client/getClientmedicalform")
    Observable<MedicalFormModel> getClientProileMedicalForm(@Field("client_id") int client_id);

    @POST("api/trainer/getDiet")
    Observable<DietListModel> getDietList();

    @FormUrlEncoded
    @POST("api/trainer/diet/client/get")
    Observable<DietListModel> getClientDietList(@Field("client_id") int client_id,
                                                @Field("is_assigned") int is_assigned);

    @POST("api/client/getClientDietplan")
    Observable<DietListModel> getClientDietList();

    @POST("api/reports/achievements")
    Observable<AchivementModel> getAchivement();

    @POST("api/client/submitMedicalFormData")
    Observable<CommonApiResponse> submitMedicalFormData(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/trainer/getClientList")
    Observable<ClientListModel> getClientList(@Field("workout_id") Integer workout_id,
                                              @Field("diet_plan_id") Integer diet_plan_id,
                                              @Field("training_program_id") Integer training_program_id,
                                              @Field("schedule_id") Integer schedule_id,
                                              @Field("group_id") Integer group_id);

    @POST("api/trainer/getTrainerClients")
    Observable<ClientListModel> getTrainerClients();

    @FormUrlEncoded
    @POST("api/trainer/activateDeactivateClient")
    Observable<CommonApiResponse> activateDeactivateClient(@Field("client_id") int client_id,
                                                           @Field("is_active") int is_active
            , @Field("trainer_id") Integer trainer_id);

    @FormUrlEncoded
    @POST("api/trainer/getClientGroups")
    Observable<ClientGroupListModel> getClientGroups(@Field("workout_id") Integer workout_id,
                                                     @Field("training_program_id") Integer training_program_id,
                                                     @Field("schedule_id") Integer schedule_id);

    @FormUrlEncoded
    @POST("api/diet/getClientGroups")
    Observable<ClientGroupListModel> getClientGroups(@Field("diet_plan_id") Integer diet_plan_id);


    @POST("api/trainer/getClientGroupswithuser")
    Observable<AddClientGroupListModel> getClientGroupsWithUser();

    @FormUrlEncoded
    @POST("api/trainer/addClientGroup")
    Observable<AddClientGroupModel> addClientGroup(@Field("group_name") String group_name);

    @FormUrlEncoded
    @POST("api/trainer/deleteClientGroup")
    Observable<CommonApiResponse> deleteClientGroup(@Field("client_group_id") int client_group_id);

    @POST("api/trainer/addClientGroupUser")
    Observable<CommonApiResponse> addClientGroupUser(@Body RequestBody requestBody);


    @POST("api/workout/assignWorkout")
    Observable<CommonApiResponse> assignWorkout(@Body RequestBody requestBody);

    @POST("api/client/shop/submitProductInquiry")
    Observable<CommonApiResponse> sendInquiry(@Body RequestBody requestBody);

    @POST("api/workout/assignWorkout")
    Observable<CommonApiResponse> unAssignWorkout(@Body RequestBody requestBody);

    @POST("api/diet/assign")
    Observable<CommonApiResponse> unAssignDiet(@Body RequestBody requestBody);

    @POST("api/trainer/training_program/assign")
    Observable<CommonApiResponse> unAssignTraining(@Body RequestBody requestBody);

    @POST("api/trainer/schedule/assign")
    Observable<CommonApiResponse> unAssignSchedule(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/exercises/list")
    Observable<ExerciseListModel> exerciseList(@Field("workout_id") Integer workout_id,
                                               @Field("muscle_id") Integer muscle_id,
                                               @Field("recently_used") Boolean recently_used,
                                               @Field("frequently_used") Boolean frequently_used,
                                               @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/client/exercises/list")
    Observable<ExerciseListModel> exerciseListClient(@Field("workout_id") Integer workout_id,
                                                     @Field("muscle_id") Integer muscle_id,
                                                     @Field("recently_used") Boolean recently_used,
                                                     @Field("frequently_used") Boolean frequently_used);

    @POST("api/auth/settings")
    Observable<CommonApiResponse> settings(@Body RequestBody requestBody);

    @POST("api/workout/exercise/assign")
    Observable<CommonApiResponse> assignExercise(@Body RequestBody requestBody);

    @POST("api/client/workout/exercise/assign")
    Observable<CommonApiResponse> assignExerciseClient(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/workout/exercise/set/get")
    Observable<ExerciseSetListModel> getExerciseSet(@Field("workout_id") int workout_id,
                                                    @Field("exercise_id") int exercise_id,
                                                    @Field("client_id") String client_id);

    @POST("api/workout/exercise/set/add")
    Observable<CommonApiResponse> addExerciseSet(@Body RequestBody requestBody);

    @POST("api/client/workout/exercise/set/add")
    Observable<CommonApiResponse> addExerciseSetClient(@Body RequestBody requestBody);

    @POST("api/workout/getParameters")
    Observable<WorkoutParamModel> getParameters();

    @FormUrlEncoded
    @POST("api/exercises/detail")
    Observable<ExerciseDetailData> exerciseDetail(@Field("id") int id);

    @FormUrlEncoded
    @POST("api/client/getFitnessTrainerCenterList")
    Observable<FitnessCenterListModel> getFitnessCenterList(@Field("location") String location, @Field("role_id") String role_id);

    @FormUrlEncoded
    @POST("api/client/getSpeciality")
    Observable<SpecialityListModel> getSpecialityList(@Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("api/diet/meal/get")
    Observable<MealListModel> getMealList(@Field("diet_plan_id") int diet_plan_id, @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/note/add")
    Observable<CommonApiResponse> saveDietNotes(@Field("diet_plan_id") int diet_plan_id, @Field("note") String note, @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/trainer/Rename")
    Observable<CommonApiResponse> rename(@Field("workout_id") int workout_id, @Field("diet_plan_id") int diet_plan_id, @Field("training_program_id") int training_program_id, @Field("name") String name);

    @FormUrlEncoded
    @POST("api/workout/note/add")
    Observable<CommonApiResponse> saveWorkoutNote(@Field("workout_id") int workout_id,
                                                  @Field("note") String note,
                                                  @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/workout/note/view")
    Observable<ViewWorkoutNotesModel> viewWorkoutNote(@Field("workout_id") int workout_id);

    @FormUrlEncoded
    @POST("api/diet/food/delete")
    Observable<CommonApiResponse> deleteFood(@Field("diet_plan_food_id") int diet_plan_food_id,
                                             @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/getClients")
    Observable<ClientListModel> getClients(@Field("diet_plan_id") Integer diet_plan_id);

    @POST("api/diet/assign")
    Observable<CommonApiResponse> assignDiet(@Body RequestBody requestBody);

    @POST("api/trainer/schedule/assign")
    Observable<CommonApiResponse> assignSchedule(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/diet/food/get")
    Observable<SearchDietPlanFoodData> searchDietPlanFood(@Field("search") String search);

    @POST("api/diet/serving_sizes/get")
    Observable<ServingSizeData> getServingSizes();

    @FormUrlEncoded
    @POST("api/diet/serving_size_units/get")
    Observable<ServingSizeUnitData> getServingSizeUnits(@Field("food_name") String foodName);

    @FormUrlEncoded
    @POST("api/diet/createDish")
    Observable<CommonApiResponse> createDish(@Field("food_name") String food_name,
                                             @Field("serving_size_unit") String serving_size_unit,
                                             @Field("serving_size") String serving_size,
                                             @Field("serving_size_unit_id") int serving_size_unit_id,
                                             @Field("calories") String calories,
                                             @Field("fat") String fat,
                                             @Field("carbs") String carbs,
                                             @Field("protien") String protien,
                                             @Field("diet_plan_meal_id") int diet_plan_meal_id,
                                             @Field("diet_plan_id") int diet_plan_id);

    @FormUrlEncoded
    @POST("api/diet/food/add")
    Observable<CommonApiResponse> addDietPlanFood(@Field("diet_plan_meal_id") Integer diet_plan_meal_id,
                                                  @Field("food_id") Integer food_id,
                                                  @Field("no_of_servings") String no_of_servings,
                                                  @Field("serving_size_id") Integer serving_size_id,
                                                  @Field("serving_size") String serving_size,
                                                  @Field("diet_plan_id") Integer diet_plan_id,
                                                  @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/diet/food/getNutritionValues")
    Observable<NutritionValuesData> getNutritionValues(@Field("food_id") Integer food_id,
                                                       @Field("serving_size") String serving_size,
                                                       @Field("serving_size_id") Integer serving_size_id,
                                                       @Field("no_of_servings") String no_of_servings);


    @POST("api/trainer/trainingSubscriptions")
    Observable<SubscriptionTypeListModel> getSubscriptionOption();

    @POST("api/client/user_subscription")
    Observable<SubscriptionResultData> getSubscriptionResult();

    @FormUrlEncoded
    @POST("api/trainer/training_program/get")
    Observable<TrainingProgramList> getTrainingList(@Field("client_id") int client_id,
                                                    @Field("is_assigned") int is_assigned);

    @POST("api/trainer/training_program/get")
    Observable<TrainingProgramList> getTrainingList();

    @POST("api/client/getClientTrainingProgram")
    Observable<TrainingProgramClientList> getClientTrainingProgram();

    @FormUrlEncoded
    @POST("api/trainer/training_program/get_week_wise_training_program")
    Observable<TrainingProgramEditModel> getWeekWiseTrainingProgram(@Field("trainer_program_id") int trainer_program_id,
                                                                    @Field("client_id") String client_id);

    @FormUrlEncoded
    @POST("api/client/getTrainingProgramDetail")
    Observable<TrainingProgramDetailClientData> getTrainingProgramDetail(@Field("training_program_id") String training_program_id);

    @POST("api/trainer/training_program/add_week_wise_training_program")
    Observable<CommonApiResponse> addWeekWiseTrainingProgram(@Body RequestBody requestBody);

    @POST("api/client/getCheckinform")
    Observable<CheckInFormListModel> getCheckInForm();

    @FormUrlEncoded
    @POST("api/client/getCheckinform")
    Observable<CheckInFormListModel> getCheckInForm(@Field("checkin_id") int checkin_id);

    @Multipart
    @POST("api/client/saveCheckinform")
    Observable<CheckinResponse> saveCheckInForm(@Part("checkin_form_data") RequestBody checkin_form_data,
                                                @Part("delete_ids") RequestBody delete_ids);

    @POST("api/client/getClientdetails")
    Observable<GetClientDetailModel> getClientDetails();

    @FormUrlEncoded
    @POST("api/client/UpdateWorkoutStartdate")
    Observable<CommonApiResponse> updateWorkoutDate(@Field("date") String date, @Field("training_program_id") int training_program_id);


    @POST("api/auth/logout")
    Observable<CommonApiResponse> logout();


    @FormUrlEncoded
    @POST("api/trainer/training_program/add")
    Observable<CommonApiResponse> addTrainingProgram(@Field("title") String title);

    @FormUrlEncoded
    @POST("api/trainer/training_program/edit")
    Observable<CommonApiResponse> editTrainingProgram(@Field("title") String title, @Field("id") int id);

    @FormUrlEncoded
    @POST("api/trainer/training_program/delete")
    Observable<CommonApiResponse> deleteTrainingProgram(@Field("training_program_id") int training_program_id);

    @FormUrlEncoded
    @POST("api/trainer/purchaseSubscription")
    Observable<CommonApiResponse> purchaseSubscription(@Field("subscription_id") int subscription_id,
                                                       @Field("transaction_id") String transaction_id,
                                                       @Field("payment_method") String payment_method,
                                                       @Field("trainer_id") String trainer_id);

    @FormUrlEncoded
    @POST("api/client/purchase_plan")
    Observable<CommonApiResponse> purchase_plan(@Field("type") String type,
                                                @Field("purchase_id") int purchase_id,
                                                @Field("payment_type") String payment_type,
                                                @Field("transaction_id") String transaction_id);

    @GET("{ifsc}")
    Observable<IFSCDetail> getBankDetailFromIFSC(@Path("ifsc") String ifsc);

    @GET("v3/search")
    Observable<YoutubeSearchModel> getYouTube(@Query("part") String part, @Query("key") String key,
                                              @Query("type") String type,
                                              @Query("q") String input,
                                              @Query("maxResults") int maxResults);

    @POST("api/client/getTrainerbankdetails")
    Observable<BankDetailModel> getBankDetail();


    @FormUrlEncoded
    @POST("api/trainer/add/bankDetails")
    Observable<CommonApiResponse> saveBankDetails(@Field("bank_name") String bank_name, @Field("account_holder_name") String account_holder_name, @Field("account_number") String account_number,
                                                  @Field("ifsc_code") String ifsc_code, @Field("branch") String branch);

    @POST("api/trainer/package/get")
    Observable<PackageListModel> getPackageData();

    @FormUrlEncoded
    @POST("api/client/getTrainerPackages")
    Observable<ProfilePackageModel> getPackageLst(@Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/gym/subscription/list")
    Observable<ProfileGymSubscriptionModel> getSubscriptionList(@Field("gym_id") int trainer_id);


    @FormUrlEncoded
    @POST("api/trainer/package/delete")
    Observable<CommonApiResponse> deletePackage(@Field("package_id") int package_id);

    @FormUrlEncoded
    @POST("api/client/getTrainerProfile")
    Observable<TrainerProfileModel> getTrainerProfile(@Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/notification/list")
    Observable<MenuNotificationResponseModel> getNotification(@Field("user_id") int user_id);

    @FormUrlEncoded
    @POST("api/client/getTrainerProfile")
    Observable<TrainerProfileModel> getTrainerProfileGym(@Field("gym_id") int gym_id);

    @FormUrlEncoded
    @POST("api/client/getGalleryImages")
    Observable<TrainerGalleryModel> getTrainerGallery(@Field("trainer_id") int trainer_id);


    @FormUrlEncoded
    @POST("api/client/getTrainerCertifications")
    Observable<CertificationModel> getCertification(@Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/client/getTrainerCertifications")
    Observable<TrainerGalleryModel> getCertificationForEdit(@Field("trainer_id") int trainer_id);


    @Multipart
    @POST("api/trainer/package/add")
    Observable<CommonApiResponse> addPackageData(@Part("package_name") RequestBody package_name,
                                                 @Part("training_program_id") RequestBody training_program_id,
                                                 @Part("no_of_weeks") RequestBody no_of_weeks,
                                                 @Part("desc") RequestBody desc,
                                                 @Part("price") RequestBody price,
                                                 @Part("discounted_price") RequestBody discounted_price,
                                                 @Part("media_type") RequestBody media_type,
                                                 @Part MultipartBody.Part media
    );

    @Multipart
    @POST("api/trainer/package/add")
    Observable<CommonApiResponse> addPackageDataWithoutImg(@Part("package_name") RequestBody package_name,
                                                           @Part("training_program_id") RequestBody training_program_id,
                                                           @Part("no_of_weeks") RequestBody no_of_weeks,
                                                           @Part("desc") RequestBody desc,
                                                           @Part("price") RequestBody price,
                                                           @Part("discounted_price") RequestBody discounted_price,
                                                           @Part("youtube_url") RequestBody youtube_url,
                                                           @Part("media_type") RequestBody media_type
    );

    @Multipart
    @POST("api/trainer/package/edit")
    Observable<CommonApiResponse> editPackage(@Part("id") RequestBody id,
                                              @Part("package_name") RequestBody package_name,
                                              @Part("training_program_id") RequestBody training_program_id,
                                              @Part("no_of_weeks") RequestBody no_of_weeks,
                                              @Part("desc") RequestBody desc,
                                              @Part("price") RequestBody price,
                                              @Part("discounted_price") RequestBody discounted_price,
                                              @Part("media_type") RequestBody media_type,
                                              @Part MultipartBody.Part media,
                                              @Part("youtube_url") RequestBody youtube_url
    );

    @FormUrlEncoded
    @POST("api/client/getTransformation")
    Observable<TransformationListModel> getTransformationDetails(@Field("trainer_id") int trainer_id);

    @FormUrlEncoded
    @POST("api/trainer/delete/deleteTransformations")
    Observable<CommonApiResponse> deleteTransformations(@Field("trasformation_id") int trasformation_id);

    @POST("api/trainer/training_program/assign")
    Observable<CommonApiResponse> assignTrainingProgram(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/client/send_client_request")
    Observable<CommonApiResponse> sendRequestToTrainer(@Field("trainer_id") int trainer_id);

    @POST("api/trainer/viewClientRequests")
    Observable<RequestListModel> getRequestList();

    @FormUrlEncoded
    @POST("api/trainer/updateRequest")
    Observable<CommonApiResponse> saveRequestResponse(@Field("request_id") int request_id, @Field("status") int status);

    @FormUrlEncoded
    @POST("api/trainer/client/package/list")
    Observable<ClientProfilePackageList> getClientProfilePurchaseList(@Field("client_id") int client_id);

    @POST("api/trainer/PurchasePlanHistory")
    Observable<PurchaseHistoryModel> purchasePlanHistory();

    @POST("api/trainer/PurchasePlanHistory")
    Observable<PurchaseHistoryTrainerModel> purchasePlanHistoryTrainer();

    @FormUrlEncoded
    @POST("api/client/getCheckins")
    Observable<CheckInDataModel> getCheckInData(@Field("sort_by") String sort_by);

    @POST("api/workout/equipments/get")
    Observable<EquipmentListModel> getEquipment();

    @FormUrlEncoded
    @POST("api/reports/workout")
    Observable<WorkoutReportModel> getReportData(@Field("client_id") Integer client_id, @Field("type") String type);

    @FormUrlEncoded
    @POST("api/trainer/getDocument")
    Observable<GetDocumentModel> getDocument(@Field("client_id") int client_id);

    @Multipart
    @POST("api/trainer/uploadDocument")
    Observable<CommonApiResponse> uploadDocument(@Part("client_id") RequestBody client_id,
                                                 @Part MultipartBody.Part photo);

    @Multipart
    @POST("api/client/updateCheckinformImage")
    Observable<CommonApiResponse> updateCheckinformImage(@Part("id") RequestBody id,
                                                         @Part List<MultipartBody.Part> image);

    @FormUrlEncoded
    @POST("api/reports/diet")
    Observable<WorkoutReportModel> getDietReportData(@Field("client_id") Integer client_id, @Field("type") String type);

    @FormUrlEncoded
    @POST("api/reports/strength")
    Observable<WorkoutReportModel> getStrengthReportData(@Field("client_id") Integer client_id, @Field("type") String type);

    @FormUrlEncoded
    @POST("api/trainer/schedule/add")
    Observable<CommonApiResponse> saveSchedule(@Field("day") String day, @Field("start_time") String start_time, @Field("end_time") String end_time);

 @FormUrlEncoded
    @POST("api/trainer/schedule/add")
    Observable<AddClientSchedule> saveClientSchedule(@Field("day") String day, @Field("start_time") String start_time, @Field("end_time") String end_time);


    @FormUrlEncoded
    @POST("api/trainer/client/progress")
    Observable<ClientProgressModel> getProgressData(@Field("client_id") int client_id);


    // GYM API SECTION

    @FormUrlEncoded
    @POST("api/gym/trainer/client/get")
    Observable<GymClientListModel> getGymTrainerClient(@Field("is_assigned") int is_assigned, @Field("trainer_id") int trainer_id);

    @POST("api/gym/assign/trainer/client")
    Observable<CommonApiResponse> assignClientToTrainer(@Body RequestBody requestBody);

    @POST("api/gym/trainer/get")
    Observable<TrainerListModel> getTrainerList();

    @POST("api/gym/client/get")
    Observable<GymClientListModel> getGymClientList();

    @FormUrlEncoded
    @POST("api/gym/trainer/add")
    Observable<CommonApiResponse> saveTrainerData(@Field("name") String name, @Field("email") String email, @Field("mobile_number") String mobile_number,
                                                  @Field("birth_date") String birth_date, @Field("gender") String gender, @Field("country_code") String country_code);

    @FormUrlEncoded
    @POST("api/gym/client/add")
    Observable<CommonApiResponse> saveClientData(@Field("name") String name, @Field("email") String email, @Field("mobile_number") String mobile_number,
                                                 @Field("birth_date") String birth_date, @Field("gender") String gender, @Field("country_code") String country_code);

    @FormUrlEncoded
    @POST("api/trainer/client/add")
    Observable<CommonApiResponse> addClientFromTrainer(@Field("name") String name, @Field("email") String email, @Field("mobile_number") String mobile_number,
                                                       @Field("birth_date") String birth_date, @Field("gender") String gender, @Field("country_code") String country_code);


    @POST("api/trainer/shop/getTrainerProducts")
    Observable<TrainerShopListModel> getShoppingData();

    @POST("api/trainer/shop/getCategories")
    Observable<ShopCategoryModel> getCategories();

    @POST("api/trainer/shop/getBrands")
    Observable<ShopBrandModel> getBrands();

    @POST("api/client/shop/getTrainerProducts")
    Observable<ClientShopListModel> getClientShoppingData();


    @FormUrlEncoded
    @POST("api/trainer/shop/deleteTrainerProducts")
    Observable<CommonApiResponse> deleteShoppingData(@Field("product_id") int product_id);

    @POST("api/client/shop/viewCart")
    Observable<ClientViewCartModel> getClientCartData();

    @FormUrlEncoded
    @POST("api/client/shop/addToCart")
    Observable<CommonApiResponse> addToCart(@Field("product_id") int product_id);

    @FormUrlEncoded

    @POST("api/gym/getUsers")
    Observable<SendNotiGetUserModel> getUserData(@Field("type") String type);


    @POST("api/gym/subscription/list")
    Observable<GymSubscriptionModel> getSubscriptionList();


    @POST("api/gym/sendNotification")
    Observable<CommonApiResponse> sendGymNotification(@Body RequestBody requestBody);




    @FormUrlEncoded
    @POST("api/gym/subscription/add")
    Observable<CommonApiResponse> addGymSubscription(@Field("name") String name,
                                                     @Field("no_of_months") String no_of_months,
                                                     @Field("about_subscriptions") String about_subscriptions,
                                                     @Field("price") String price,
                                                     @Field("discount_price") String discount_price);

    @FormUrlEncoded
    @POST("api/gym/subscription/update")
    Observable<CommonApiResponse> updateGymSubscription(@Field("id") Integer id,
                                                        @Field("name") String name,
                                                        @Field("no_of_months") String no_of_months,
                                                        @Field("about_subscriptions") String about_subscriptions,
                                                        @Field("price") String price,
                                                        @Field("discount_price") String discount_price);

    @FormUrlEncoded
    @POST("api/gym/subscription/delete")
    Observable<CommonApiResponse> deleteGymSubscription(@Field("id") int id);

    @POST("api/trainer/shop/getFitzohProducts")
    Observable<GetFitzohProductModel> getFitzohProduct();

    @Multipart
    @POST("api/trainer/shop/addTrainerProducts")
    Observable<CommonApiResponse> addProduct(@Part("product_id") RequestBody product_id,
                                             @Part("price") RequestBody price,
                                             @Part("qty") RequestBody qty,
                                             @Part("discounted_price") RequestBody discounted_price,
                                             @Part("name") RequestBody name,
                                             @Part("category_id") RequestBody category_id,
                                             @Part("category") RequestBody category,
                                             @Part("brand_id") RequestBody brand_id,
                                             @Part("brand") RequestBody brand,
                                             @Part("description") RequestBody description,
                                             @Part("ingredients") RequestBody ingredients,
                                             @Part("carbs") RequestBody carbs,
                                             @Part("fat") RequestBody fat,
                                             @Part("protein") RequestBody protein,
                                             @Part("calories") RequestBody calories,
                                             @Part List<MultipartBody.Part> image);

    @FormUrlEncoded
    @POST("api/trainer/shop/updateTrainerProducts")
    Observable<CommonApiResponse> updateProduct(@Field("trainer_product_id") int trainer_product_id, @Field("price") String price, @Field("qty") String qty, @Field("discounted_price") String discounted_price, @Field("is_master_product") String is_master_product);

    @FormUrlEncoded
    @POST("api/trainer/shop/addTrainerProducts")
    Observable<CommonApiResponse> editProduct(@Field("trainer_product_id") int trainer_product_id, @Field("price") String price, @Field("qty") String qty, @Field("discounted_price") String discounted_price, @Field("is_master_product") String is_master_product);

    @Multipart
    @POST("api/trainer/shop/addTrainerProducts")
    Observable<CommonApiResponse> editProductImage(@Part("trainer_product_id") RequestBody product_id,
                                                   @Part("price") RequestBody price,
                                                   @Part("qty") RequestBody qty,
                                                   @Part("discounted_price") RequestBody discounted_price,
                                                   @Part("name") RequestBody name,
                                                   @Part("category_id") RequestBody category_id,
                                                   @Part("category") RequestBody category,
                                                   @Part("brand_id") RequestBody brand_id,
                                                   @Part("brand") RequestBody brand,
                                                   @Part("description") RequestBody description,
                                                   @Part("ingredients") RequestBody ingredients,
                                                   @Part("carbs") RequestBody carbs,
                                                   @Part("fat") RequestBody fat,
                                                   @Part("protein") RequestBody protein,
                                                   @Part("calories") RequestBody calories,
                                                   @Part("is_master_product") RequestBody is_master_product,
                                                   @Part List<MultipartBody.Part> image);

    @FormUrlEncoded
    @POST("api/trainer/updateNotificationFlag")
    Observable<CommonApiResponse> updateNotification(@Field("is_show_notification") int is_show_notification);

    @FormUrlEncoded
    @POST("api/trainer/updateWeightunit")
    Observable<CommonApiResponse> updateWeightUnit(@Field("type") String type);

    /*@FormUrlEncoded
    @POST("api/LoginAPI/Mobile_ValidateUserLogin")
    Observable<LoginModel> getLogin(@Field("Email") String EmailAddress,
                                    @Field("Password") String Password);

    @GET("api/ServiceProviderAPI/GetPostalCodeList")
    Observable<PostalResponse> getPostalCodeValidation(@Query("PostCode") String PostCode,
                                                       @Query("IsCheckPostalCode") boolean IsCheckPostalCode);

    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/Mobile_SaveCustomerProfile")
    Observable<RegisterModel> saveCustomerProfile(@Field("FirstName") String FirstName,
                                                  @Field("LastName") String LastName,
                                                  @Field("Email") String Email,
                                                  @Field("Password") String Password,
                                                  @Field("Phone") String Phone,
                                                  @Field("UserType") int UserType,
                                                  @Field("AdminUserId") int AdminUserId);

    @GET("api/EmailTemplateAPI/GetForgotPasswordTemplate")
    Observable<CommonResponseModel> forgetPassword(@Query("UserName") String UserName, @Query("LoginLink") String LoginLink);

    @POST("api/ServiceProviderAPI/CurrentLocationServiceProviderList")
    Observable<ServiceProviderListModel> getServiceProvideList(@Query("longitude") double longitude, @Query("latitude") double latitude);

    @POST("api/ServiceProviderAPI/Mobile_GetServiceProviderProfile")
    Observable<SpDetailModel> getServiceProvideDetail(@Query("ServiceProviderId") int ServiceProviderId);

    @POST("api/ServiceProviderAPI/ViewClientDetails")
    Observable<ReviewRatingModel> viewClientDetails(@Query("id") int ServiceProviderId);


    @GET("api/CustomerServiceAPI/GetCustomerAddressDetails")
    Observable<AddressListModel> getCustomerAddressDetails(@Query("UserAddressId") int ServiceProviderId,
                                                           @Query("AdminUserId") int AdminUserId);

    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/SaveCustomerAddressDetails")
    Observable<CommonResponseModel> saveCustomerAddressDetails(@Field("LocationName") String ServiceProviderId,
                                                               @Field("Address") String Address,
                                                               @Field("CountyState") String CountyState,
                                                               @Field("CityTown") String CityTown,
                                                               @Field("PostalCode") String PostalCode,
                                                               @Field("UserAddressId") int UserAddressId,
                                                               @Field("AdminUserId") int AdminUserId);

    @POST("api/CustomerServiceAPI/DeleteCustomerAddressDetail")
    Observable<CommonResponseModel> deleteCustomerAddressDetail(@Query("UserAddressId") int UserAddressId);

    @POST("api/CustomerServiceAPI/DeleteCustomerCarDetail")
    Observable<CommonResponseModel> deleteCustomerCarDetail(@Query("CustomerCarId") int CustomerCarId);

    @FormUrlEncoded
    @POST("api/OrderMasterAPI/SaveOrderStatus")
    Observable<CommonResponseModel> cancelCustomerOrder(@Field("OrderId") int OrderId,
                                                        @Field("OrderType") int OrderType,
                                                        @Field("Description") String Description,
                                                        @Field("Createdby") int Createdby);


    @GET("api/OrderMasterAPI/GetAllCustomerOrderList")
    Observable<BookingListModel> getCustomerOrderList(@Query("OrderID") int OrderID,
                                                      @Query("CustID") int CustID,
                                                      @Query("ServiceProviderID") int ServiceProviderID,
                                                      @Query("Startdate") String Startdate,
                                                      @Query("EndDate") String EndDate,
                                                      @Query("OrderStatus") Integer OrderStatus);

    @GET("api/OrderMasterAPI/Mobile_GetBookingEvents")
    Observable<DashboardEventList> getSpOrderList(@Query("ServiceProviderID") int ServiceProviderID,
                                                  @Query("Month") int Startdate,
                                                  @Query("Year") int EndDate
    );

    @GET("api/ServiceProviderDetailsAPI/GetHolidayDate")
    Observable<HolidayListModel> getHolidayList(@Query("AdminUserId") int AdminUserId);

    @POST("api/OrderMasterAPI/Mobile_GetCustomerDetail")
    Observable<OrderDetailModel> getOrderDetail(@Query("OrderID") int OrderID);

    @GET("api/ServicePackageAPI/GetAllServicePackageList")
    Observable<ServicePackageListResponse> getAllServicePackageList();

    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/SaveCustomerCarDetail")
    Observable<CommonResponseModel> saveCustomerCarDetail(@Field("CustomerCarId") int CustomerCarId,
                                                          @Field("AdminUserId") int AdminUserId,
                                                          @Field("CarTypeId") int CarTypeId,
                                                          @Field("ModelMake") String ModelMake,
                                                          @Field("Description") String Description,
                                                          @Field("CarTypeName") String CarTypeName);


    @GET("api/CustomerServiceAPI/GetCustomerCarDetails")
    Observable<CarDetailListResponse> getCarDetailList(@Query("CustomerCarId") int CustomerCarId,
                                                       @Query("AdminUserId") int AdminUserId);

    @GET("api/CustomerDetailsAPI/GetCustomerProfileDetails")
    Observable<ProfileDataModel> getCustomerProfileDetail(@Query("AdminUserId") int AdminUserId);


    @GET("api/ServiceProviderAPI/GetServicePackageConfiguration")
    Observable<ServiceConfigrationResponse> getPackageConfigration(@Query("ServiceProviderId") int ServiceProviderId);

    @GET("api/CustomerServiceAPI/GetCarTypeList")
    Observable<CarTypeResponse> getCarTypeList();

    @FormUrlEncoded
    @POST("api/CustomerDetailsAPI/SaveContactDetails")
    Observable<RegisterModel> saveCustomerContectDetail(@Query("UserId") int UserId,
                                                        @Query("UserRoleId") int UserRoleId,
                                                        @Field("AdminUserId") int AdminUserId,
                                                        @Field("FirstName") String FirstName,
                                                        @Field("LastName") String LastName,
                                                        @Field("Email") String Email,
                                                        @Field("Phone") String Phone
    );

    @FormUrlEncoded
    @POST("api/ServiceProviderAPI/SaveContactDetails")
    Observable<RegisterModel> saveCustomerSPContectDetail(
            @Field("AdminUserId") int AdminUserId,
            @Field("ServiceProviderId") int ServiceProviderId,
            @Field("FirstName") String FirstName,
            @Field("LastName") String LastName,
            @Field("Email") String Email,
            @Field("PhoneNo") String Phone,
            @Field("AboutUs") String AboutUs
    );


    @FormUrlEncoded
    @POST("api/OrderMasterAPI/SaveOrderRating")
    Observable<CommonResponseModel> saveRatings(@Field("OrderID") int OrderID,
                                                @Field("ServiceProviderRating") Double ServiceProviderRating,
                                                @Field("ServiceProviderfeedback") String ServiceProviderfeedback,
                                                @Field("ClientRating") Double ClientRating,
                                                @Field("ClientFeedback") String ClientFeedback,
                                                @Field("OrderStatus") int OrderStatus,
                                                @Field("IsCommplite") Boolean IsCommplite,
                                                @Field("MarkFavouriteServiceProvider") Boolean MarkFavouriteServiceProvider);


    @POST("api/ServiceProviderAPI/SaveLocationDetails")
    Observable<SaveLocationModel> saveLocationDetails(@Query("UserId") int userId,
                                                      @Query("UserRoleId") int userRoleId,
                                                      @Body RequestBody requestBody);


    @POST("api/ServiceProviderAPI/Mobile_SaveContactandLocationDetails")
    Observable<SaveLocationModel> saveregisterLocationDetails(@Query("UserId") int userId,
                                                              @Query("UserRoleId") int userRoleId,
                                                              @Body RequestBody requestBody);

    @POST(" api/ServiceProviderDetailsAPI/ChangePassword")
    Observable<CommonResponseModel> changePassword(@Query("AdminUserId") int userId,
                                                   @Query("OldPassword") String OldPassword,
                                                   @Query("NewPassword") String NewPassword);

    @POST("api/ServiceProviderAPI/SaveServicePackageConfiguration")
    Observable<CommonResponseModel> saveServicePackageConfiguration(@Body RequestBody requestBody);

    @POST("api/CustomerServiceAPI/SaveDataPersonal")
    Observable<SavePersonalBookingModel> savePersonalBookingData(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/GetPersonalbooking")
    Observable<PersonalBookingListModel> getPersonalBooking(@Field("AdminUserId") int AdminUserId, @Field("Pid") Integer Pid);


    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/GetPidbooking")
    Observable<PersonalBookingListModel> getPidPersonalBooking(@Field("Pid") Integer Pid);


    @GET("api/ServiceProviderAPI/GetServiceProviderProfile")
    Observable<ServiceProviderProfileModel> getServiceProviderProfile(@Query("ServiceProviderId") int ServiceProviderId);

    @FormUrlEncoded
    @POST("api/ServiceProviderDetailsAPI/SetHolidayDate")
    Observable<CommonResponseModel> setHolidaySp(@Field("AdminUserId") int AdminUserId,
                                                 @Field("HolidayFromDate") String HolidayFromDate,
                                                 @Field("HolidayToDate") String HolidayToDate
    );

    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/DeletePersonalbooking")
    Observable<CommonResponseModel> deletePersonalBooking(@Field("Pid") int Pid,
                                                          @Field("AdminUserId") int AdminUserId);


    @POST("api/CustomerServiceAPI/EditPersonalbooking")
    Observable<CommonResponseModel> editPersonalBooking(@Body RequestBody requestBody);


    @POST("api/CustomerServiceAPI/SaveOrderBooking")
    Observable<SaveOrderBookingModel> saveOrderBooking(@Query("IPaddress") String IPaddress,
                                                       @Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("api/CustomerServiceAPI/Mobile_GetServiceProviderListBetweenDays")
    Observable<ServiceProviderCustomerResponse> getServiceProviderAavailibility(@Field("LocationId") int LocationId,
                                                                                @Field("ServiceProviderId") int ServiceProviderId,
                                                                                @Field("OrderDate") String OrderDate);

    @POST("api/CustomerServiceAPI/GetServiceProviderListBetweenDays")
    Observable<ServiceProviderCustomerResponse> getServiceProviderForCustomer(@Body RequestBody requestBody);

    @Multipart
    @POST("api/ServiceProviderAPI/MobileUpdateProfilePhoto")
    Observable<CommonResponseModel> updateProfilePhoto(@Query("AdminUserId") int AdminUserId,
                                                       @Part MultipartBody.Part ProfilePhoto);*/

    enum ApiNames {
        edit,login, register, logout, single, resend, liveClients, clientUpdateList, updateClientProfile, editTransformation, editMedicalForm, editTrainerProfile, workoutList, deleteWorkout, trainingSession, addWorkout, save, DietList, assignWorkout,
        fitnessCenterList, specialityList, deleteDiet, addDiet, addMeal, mealList, getNutrition, addNotes, deleteDietPlan, deleteFood, assignDiet, searchDietPlanFood, getServingSizes, getServingSizeUnits, addDietPlanFood, getNutritionValues,
        getSubscriptionOptionList, trainingList, addTrainingProgram, editTraining, deleteTraining, saveBankDetail, getPackageList, deletePackage, getTrainerProfile, getTrainerGallery, addPackage, getTransformation, deleteTransformation, getGymProfile, getTrainerCertificates, updateBasicInfo,
        sendClientRequest, getRequestList, getBankDetail, requestResponse, checkInForms, equipmentList, reportWorkout, getAchivement, getSchedualList, deleteSchedual, assignSchedule,
        unAssignWorkout, getProgressList, updateImage, unAssignDiet, unAssignTraining, addTrainer, clientWorkoutList, deleteShoppingData, sendInquiry, getCategories, getBrands, getProfileGymSubList, addProduct, updateProduct, notification, getFitnessValue, updateDate
    }
}
