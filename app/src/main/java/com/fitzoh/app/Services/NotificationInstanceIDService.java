package com.fitzoh.app.Services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.fitzoh.app.utils.SessionManager;


/**
 * Created by sit107 on 10-01-2018.
 */

public class NotificationInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIDService";
    public SessionManager session;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        storeRegIdInPref(refreshedToken);
    }


    private void storeRegIdInPref(String token) {
        NotificationToken notificationToken = new NotificationToken(token);
        Log.e(TAG, "storeRegIdInPref: " + notificationToken.getAccessToken());
        session = new SessionManager(getApplicationContext());
        session.saveNotificationToken(notificationToken.getAccessToken());



    }


}
