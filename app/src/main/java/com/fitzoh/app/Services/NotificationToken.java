package com.fitzoh.app.Services;

/**
 * Created by Hiren on 09/22/17.
 */
public class NotificationToken {

    // region Fields
    private String accessToken;
    // endregion

    // region Constructors
    public NotificationToken(String accessToken) {
        this.accessToken = accessToken;
    }
    // endregion

    // region Getters
    public String getAccessToken() {
        return accessToken;
    }
    // endregion
}



