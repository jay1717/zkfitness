package com.fitzoh.app.Services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.activity.SplashActivity;
import com.fitzoh.app.ui.dialog.AddNoteDialog;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by sit107 on 10-01-2018.
 */

public class NotificationMessagingService extends FirebaseMessagingService implements AddNoteDialog.DialogClickListener {

    private static final String TAG = NotificationMessagingService.class.getSimpleName();

    private NotificationUtility notificationUtils;
    private String title, message, timestamp, image;
    Intent pushNotification;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.


        Log.e("notification", "msg received");
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            String alert = data.get("alert").toString();
            String title = data.get("title").toString();
            String message = data.get("message").toString();
            String notification_type = data.get("notification_type").toString();
            String user_type = data.get("user_type").toString();
            String sound = data.get("sound").toString();
            String client_id = data.get("client_id").toString();
            switch (notification_type) {
                case "17a":
                    getNotification(title, message, timestamp);
                    break;
            }
          /*  String click_action = remoteMessage.getNotification().getClickAction();
            sendNotification(alert, title, message, notification_type);*/
        }

        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        String channelId = "Default";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Fitzoh")
                .setContentText(remoteMessage.getNotification().getBody()).setAutoCancel(true).setContentIntent(pendingIntent);
        ;
        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        manager.notify(0, builder.build());

        if (remoteMessage.getNotification() != null) {
            //     Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent("pushNotification");
            pushNotification.putExtra("message", message);
            // LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtility notificationUtils = new NotificationUtility(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
//            Toast.displayMessage(getApplicationContext(), "App in background");
            NotificationUtility notificationUtils = new NotificationUtility(getApplicationContext());
            notificationUtils.playNotificationSound();
        }
    }




       /*if (remoteMessage.getData().size() > 0) {
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());

            }
            if (true) {
                //  scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
                sendNotification(remoteMessage.getNotification().getBody());
            }
        }*/


    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            title = data.getString("title");
            message = data.getString("message");
            timestamp = data.getString("timestamp");
            if (!message.equals("Trainer accepted request")) {
                //   sendChatNotification();
            } else {
                // getNotification(title, message, timestamp);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }
   /* private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }*/

    private void sendNotification(String messageBody, String title, String message, String notification_type) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("message", message);
        intent.putExtra("title", title);
        intent.putExtra("notification_type", notification_type);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("FCM Message")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    private void getNotification(String title, String message, String timestamp) {


        if (!NotificationUtility.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            pushNotification = new Intent("pushNotification");
            pushNotification.putExtra("title", title);
            pushNotification.putExtra("message", message);
            pushNotification.putExtra("timestamp", timestamp);

            //  LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtility notificationUtils = new NotificationUtility(getApplicationContext());
            notificationUtils.playNotificationSound();
            // image is present, show notification with image
           /* Intent intent = new Intent(getApplicationContext(), ReloginActivity.class);
                        intent.putExtra("title", title);
                        intent.putExtra("message", message);
                        intent.putExtra("timestamp", timestamp);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
           /* Intent intent = new Intent(this, Receiver.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this.getApplicationContext(), 0, intent, 0);
            sendBroadcast(intent);*/
           /* NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setContentIntent(contentIntent);*/

            Log.e("notification", "getNotification");
            Intent intent = new Intent("myFunction");
            // add data
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);


        }
       /* pushNotification = new Intent(getApplicationContext(), SplashActivity.class);
        pushNotification.putExtra("title", title);
        pushNotification.putExtra("message", message);
        pushNotification.putExtra("timestamp", timestamp);*/

//        showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);

    }


    // Showing the status in Snackbar


    /**
     * re
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String
            message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtility(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String
            message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtility(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    @Override
    public void setClick() {

    }
}
