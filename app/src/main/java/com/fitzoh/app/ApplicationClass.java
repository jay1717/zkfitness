package com.fitzoh.app;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class ApplicationClass extends Application {

    private static ApplicationClass context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //Facebook Init
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public static Context getAppContext() {
        return context;
    }



}
