package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientEditWorkoutExerciseAdapter;
import com.fitzoh.app.databinding.FragmentWorkoutDetailsBinding;
import com.fitzoh.app.model.ClientEditWorkoutModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentWorkoutClientCreated#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentWorkoutClientCreated extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentWorkoutDetailsBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<List<ClientEditWorkoutModel.DataBean>> workOutListData;
    ClientEditWorkoutExerciseAdapter workoutListAdapter;
    int clientId, workoutId;


    public FragmentWorkoutClientCreated() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentWorkout.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentWorkoutClientCreated newInstance(Bundle bundle) {
        FragmentWorkoutClientCreated fragment = new FragmentWorkoutClientCreated();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

           /* workoutId = getArguments().getInt("workout_id");
            clientId = getArguments().getInt("client_id");*/
           /* mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                break;
        }
        return true;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.workouts));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Intent intent = getActivity().getIntent();
        if(null!=intent){
            workoutId = intent.getExtras().getInt("workout_id");
            clientId = intent.getExtras().getInt("client_id");
        }
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_workout_details, container, false);

        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

//        mBinding.toolbar.btnFindplan.setVisibility(View.VISIBLE);
//        mBinding.toolbar.btnFindplan.setOnClickListener(view -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "workout")));
        setHasOptionsMenu(true);
        workOutListData = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        workoutListAdapter = new ClientEditWorkoutExerciseAdapter(getActivity(), workOutListData);
        mBinding.layoutWorkout.recyclerView.setAdapter(workoutListAdapter);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkoutData(workoutId, clientId)
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                workOutListData = new ArrayList<>();
                ClientEditWorkoutModel workOutListModel = (ClientEditWorkoutModel) o;
                if (workOutListModel.getStatus() == AppConstants.SUCCESS) {

                    if (workOutListModel != null) {
                        workOutListData.addAll(workOutListModel.getData());

                        // workoutListAdapter.notifyDataSetChanged();
                        if (workOutListData.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            workoutListAdapter = new ClientEditWorkoutExerciseAdapter(getActivity(), workOutListData);
                            mBinding.layoutWorkout.recyclerView.setAdapter(workoutListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, workOutListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }


    @Override
    public void onRefresh() {
        callWorkoutList();
        mBinding.layoutWorkout.swipeContainer.setEnabled(false);
    }
}
