package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.RequestListAdapter;
import com.fitzoh.app.databinding.FragmentNotificationBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.RequestListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getRequestList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationRejectedFragment extends BaseFragment implements View.OnClickListener, RequestListAdapter.onAction, SingleCallback, FragmentLifeCycle, SwipeRefreshLayout.OnRefreshListener {


    FragmentNotificationBinding mBinding;
    List<RequestListModel.DataBean> dataList;
    String userId, userAccessToken;
    RequestListAdapter requestListAdapter;

    public NotificationRejectedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
//        mBinding.toolbar.imgBack.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back));
        //  Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        //  mBinding.toolbar.tvTitle.setText(R.string.notificaion);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        setClickEvents();
        callRequestData();
        return mBinding.getRoot();
    }

    public static NotificationRejectedFragment newInstance() {
        NotificationRejectedFragment fragment = new NotificationRejectedFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    private void setClickEvents() {

        //     mBinding.toolbar.imgBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().finish();
                break;
        }
    }

    private void callRequestData() {
        if (Utils.isOnline(getActivity())) {
            mBinding.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getRequestList()
                    , getCompositeDisposable(), getRequestList, this);
        } else {
            showSnackBar(mBinding.layoutNotification, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void accept(RequestListModel.DataBean dataBean, int status) {
        onRequest(dataBean, status);
    }

    @Override
    public void reject(RequestListModel.DataBean dataBean, int status) {
        onRequest(dataBean, status);
    }

    private void onRequest(RequestListModel.DataBean dataBean, int status) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveRequestResponse(dataBean.getId(), status)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.requestResponse, this);
        } else {
            showSnackBar(mBinding.layoutNotification, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getRequestList:
                dataList = new ArrayList<>();
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                RequestListModel requestListModel = (RequestListModel) o;
                if (requestListModel.getStatus() == AppConstants.SUCCESS) {
                    if (requestListModel != null) {
                        if (requestListModel.getData().size() > 0) {
                            int size = requestListModel.getData().size();
                            for (int i = 0; i < size; i++) {
                                if (requestListModel.getData().get(i).getStatus().equals("2")) {
                                    dataList.add(requestListModel.getData().get(i));
                                }
                            }
                        }
                        // workoutListAdapter.notifyDataSetChanged();
                        if (dataList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            requestListAdapter = new RequestListAdapter(getActivity(), dataList, this);
                            mBinding.recyclerView.setAdapter(requestListAdapter);
                        }
                    } else
                        showSnackBar(mBinding.layoutNotification, requestListModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case requestResponse:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callRequestData();
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case getRequestList:
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        callRequestData();
    }

    @Override
    public void onRefresh() {
        callRequestData();
    }
}
