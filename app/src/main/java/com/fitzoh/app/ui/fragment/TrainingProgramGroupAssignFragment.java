package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramAssignGroupAdapter;
import com.fitzoh.app.databinding.FragmentGroupAssignBinding;
import com.fitzoh.app.model.ClientGroupListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.dialog.AssignToClientDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class TrainingProgramGroupAssignFragment extends BaseFragment implements SingleCallback, TrainingProgramAssignGroupAdapter.onChecked, AssignToClientDialog.onSave, SwipeRefreshLayout.OnRefreshListener {

    private TrainingProgramList.DataBean dataBean = null;
    private FragmentGroupAssignBinding mBinding;
    private String userId, userAccessToken;
    private List<ClientGroupListModel.DataBean> data = new ArrayList<>();
    private TrainingProgramAssignGroupAdapter adapter;
    JSONObject jsonObject;
    JSONArray jsonArray;

    public TrainingProgramGroupAssignFragment() {
        // Required empty public constructor
    }

    public static TrainingProgramGroupAssignFragment newInstance(TrainingProgramList.DataBean dataBean) {
        TrainingProgramGroupAssignFragment fragment = new TrainingProgramGroupAssignFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", dataBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (TrainingProgramList.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_group_assign, container, false);
        Utils.getShapeGradient(mActivity, mBinding.client.btnAssign);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        jsonArray = new JSONArray();
        jsonObject = new JSONObject();
        if (Utils.isOnline(getActivity())) {
            getClientGroups();
        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
        mBinding.client.btnAssign.setOnClickListener(view -> {
            if (validation()) {
                assignWorkout();
            }
        });
        mBinding.client.swipeContainer.setOnRefreshListener(this);
        mBinding.client.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));

        return mBinding.getRoot();
    }

    private void assignWorkout() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignTrainingProgram(requestBody)
                , getCompositeDisposable(), assignWorkout, this);

    }

    private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            jsonObject.put("client_ids", new JSONArray());
            jsonObject.put("client_group_ids", jsonArray);
            jsonObject.put("training_program_id", dataBean.getId());
          /*  jsonObject.put("start_date", dateFormat.format(Calendar.getInstance().getTime()));
            jsonObject.put("allowed_to_change_date", 1);*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private void getClientGroups() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientGroups(null, dataBean.getId(), null)
                , getCompositeDisposable(), single, this);
    }

    private boolean validation() {
        if (data == null || data.size() <= 0) {
            showSnackBar(mBinding.layoutClient, "No Data Available", Snackbar.LENGTH_LONG);
            return false;
        } /*else if (adapter.getData() == null || adapter.getData().size() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please select client to assign", Snackbar.LENGTH_LONG);
            return false;
        }*/
        return true;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientGroupListModel clientGroupListModel = (ClientGroupListModel) o;
                if (clientGroupListModel != null && clientGroupListModel.getStatus() == AppConstants.SUCCESS && clientGroupListModel.getData() != null && clientGroupListModel.getData().size() > 0) {
                    mBinding.client.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.client.imgNoData.setVisibility(View.GONE);
                    data = clientGroupListModel.getData();
                    adapter = new TrainingProgramAssignGroupAdapter(getActivity(), data, this);
                    mBinding.client.recyclerView.setAdapter(adapter);
                } else {
                    mBinding.client.recyclerView.setVisibility(View.GONE);
                    mBinding.client.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.layoutClient, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.client.recyclerView.setVisibility(View.GONE);
                mBinding.client.imgNoData.setVisibility(View.VISIBLE);
        }
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutClient, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void check(ClientGroupListModel.DataBean dataBean, boolean isChecked) {
        if (isChecked) {
            AssignToClientDialog dialog = new AssignToClientDialog(dataBean.getId(), dataBean.getIs_assigned(), this);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AssignToClientDialog.class.getSimpleName());
            dialog.setCancelable(false);
        } else {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("client_group_id", dataBean.getId());
                jsonObject.put("is_assigned", 0);
                jsonObject.put("start_date", "");
                jsonObject.put("allowed_to_change_date", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(int id, String date, int isAllowed, int isAssign) {
        try {
            jsonObject = new JSONObject();
            jsonObject.put("client_group_id", id);
            jsonObject.put("is_assigned", isAssign);
            jsonObject.put("start_date", date);
            jsonObject.put("allowed_to_change_date", isAllowed);
            jsonArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(getActivity())) {
            getClientGroups();
        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

        mBinding.client.swipeContainer.setRefreshing(false);
    }
}

