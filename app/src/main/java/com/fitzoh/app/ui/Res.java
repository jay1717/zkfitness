package com.fitzoh.app.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;

import com.fitzoh.app.R;

public class Res extends Resources {
    private SharedPreferences pref;

    public Res(Context context, Resources original) {
        super(original.getAssets(), original.getDisplayMetrics(), original.getConfiguration());
        pref = context.getSharedPreferences("colors", 0);
    }

    @Override
    public int getColor(int id) throws NotFoundException {
        /*Theme theme = context.getTheme();
        theme.applyStyle(R.style.AppTheme, true);*/
        final Theme theme = newTheme();
        theme.applyStyle(R.style.AppTheme, true);
        return getColor(id, theme);
    }

    @Override
    public int getColor(int id, Theme theme) throws NotFoundException {
        switch (getResourceEntryName(id)) {
            case "colorPrimary":
                int red = pref.getInt("redPrimary", 21);
                int green = pref.getInt("greenPrimary", 153);
                int blue = pref.getInt("bluePrimary", 215);
                return Color.rgb(red, green, blue); // used as an example. Change as needed.
            case "colorPrimaryDark":
                int red1 = pref.getInt("redPrimary", 21);
                int green1 = pref.getInt("greenPrimary", 153);
                int blue1 = pref.getInt("bluePrimary", 215);
                return Color.rgb(red1, green1, blue1);
            case "colorAccent":
                int red2 = pref.getInt("redAccent", 58);
                int green2 = pref.getInt("greenAccent", 209);
                int blue2 = pref.getInt("blueAccent", 190);
                return Color.rgb(red2, green2, blue2);
            default:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    return super.getColor(id, theme);
                }
                return super.getColor(id);
        }
    }

}
