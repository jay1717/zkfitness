package com.fitzoh.app.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.DrawerItemAdapter;
import com.fitzoh.app.databinding.ActivityNavigationMainBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DrawerItem;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.ClientInquiryFragment;
import com.fitzoh.app.ui.fragment.FragmentDashboard;
import com.fitzoh.app.ui.fragment.FragmentGymDashboard;
import com.fitzoh.app.ui.fragment.FragmentNutrition;
import com.fitzoh.app.ui.fragment.FragmentPurchaseHistory;
import com.fitzoh.app.ui.fragment.FragmentTrainerProfile;
import com.fitzoh.app.ui.fragment.FragmentTrainingProgram;
import com.fitzoh.app.ui.fragment.FragmentWorkout;
import com.fitzoh.app.ui.fragment.GymClientListFragment;
import com.fitzoh.app.ui.fragment.GymSubscriptionListFragment;
import com.fitzoh.app.ui.fragment.GymTrainerListFragment;
import com.fitzoh.app.ui.fragment.MyClientListFragment;
import com.fitzoh.app.ui.fragment.NotificationTypeFragment;
import com.fitzoh.app.ui.fragment.PackageListFragment;
import com.fitzoh.app.ui.fragment.ProfileFragment;
import com.fitzoh.app.ui.fragment.ScheduleFragment;
import com.fitzoh.app.ui.fragment.SendNewNotificationFragment;
import com.fitzoh.app.ui.fragment.SettingFragment;
import com.fitzoh.app.ui.fragment.TrainerInquiriesFragment;
import com.fitzoh.app.ui.fragment.TrainerShopFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.logout;
import static com.fitzoh.app.utils.SessionManager.KEY_IS_SP;


public class NavigationMainActivity extends BaseActivity implements DrawerItemAdapter.OnRecyclerViewClickListener, View.OnClickListener, SingleCallback {

    public ActivityNavigationMainBinding mBinding;
    private HashMap<String, Stack<Fragment>> mStacks;
    public ArrayList<DrawerItem> objectDrawerItems;
    private DrawerItemAdapter drawerItemCustomAdapter;
    boolean doubleBackToExitPressedOnce = false;
    boolean isSP = true;
    String rollId = "1";
    private SharedPreferences pref;
    private int position = 0;
    private List<ImageView> imageViews = new ArrayList<>();
    private List<TextView> textViews = new ArrayList<>();
    String userId, userAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_navigation_main);
        userId = String.valueOf(session.getAuthorizedUser(this).getId());
        userAccessToken = session.getAuthorizedUser(this).getUserAccessToken();
        if (session.getAuthorizedUser(this) != null)
            rollId = session.getAuthorizedUser(this).getRoleId();
        mBinding.relativeNavigation.setBackground(Utils.getShapeGradient(this, 0));
        pref = getSharedPreferences("colors", 0);
        prepareLayout();
//        Log.d("AuthorizedDetail",session.getAuthorizedUser(this).getUserType()+" user type");
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBinding.txtUserName.setText(session.getAuthorizedUser(this).getName().toString());
        if (session.getAuthorizedUser(this).getPhoto() != null) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user);
            Glide.with(this)
                    .load(session.getAuthorizedUser(this).getPhoto())
                    .apply(options)
                    .into(mBinding.imgLogin);
        }
    }

    private void prepareLayout() {
        isSP = session.getBooleanDataByKey(KEY_IS_SP);
        setTitle("");
        mStacks = new HashMap<>();
        mStacks.put(AppConstants.NAVIGATION_KEY, new Stack<>());
        mBinding.txtUserName.setText(session.getAuthorizedUser(this).getName().toString());
        if (session.getAuthorizedUser(this).getPhoto() != null) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder);
            Glide.with(this)
                    .load(session.getAuthorizedUser(this).getPhoto())
                    .apply(options)
                    .into(mBinding.imgLogin);
        }

        /*if (rollId == 1) {
            pushFragments(AppConstants.NAVIGATION_KEY, new HomeClientFragment(), true, true);
        } else {*/
        if (session.getRollId() == 2) {
            //trainer dashboard
            pushFragments(AppConstants.NAVIGATION_KEY, FragmentDashboard.newInstance(), true, true);
        } else {
            //gym dashboard
            pushFragments(AppConstants.NAVIGATION_KEY, FragmentGymDashboard.newInstance(), true, true);
        }
        //   }
        setData();
        setTintOfBottomImage();
        mBinding.txtLogout.setOnClickListener(view -> {
            if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
                mBinding.drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
            setLogout();
        });
        mBinding.imgLogin.setOnClickListener(view -> {
            pushFragments(AppConstants.NAVIGATION_KEY, ProfileFragment.newInstance(), true, true);
            position = 5;
            drawerItemCustomAdapter.setPosition(5);
            if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
                mBinding.drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }

        });
    }

    private void setTintOfBottomImage() {
        imageViews.add(mBinding.uiNavigation.imgClient);
        imageViews.add(mBinding.uiNavigation.imgProgramTemplate);
        imageViews.add(mBinding.uiNavigation.imgProfile);
        imageViews.add(mBinding.uiNavigation.imgPackages);
        textViews.add(mBinding.uiNavigation.txtClient);
        textViews.add(mBinding.uiNavigation.txtProgramTemplate);
        textViews.add(mBinding.uiNavigation.txtProfile);
        textViews.add(mBinding.uiNavigation.txtPackages);

        mBinding.uiNavigation.txtClient.setTextColor(makeSelector());
        mBinding.uiNavigation.txtProgramTemplate.setTextColor(makeSelector());
        mBinding.uiNavigation.txtProfile.setTextColor(makeSelector());
        mBinding.uiNavigation.txtPackages.setTextColor(makeSelector());

        tintButton(mBinding.uiNavigation.imgClient);
        tintButton(mBinding.uiNavigation.imgProgramTemplate);
        tintButton(mBinding.uiNavigation.imgProfile);
        tintButton(mBinding.uiNavigation.imgPackages);

        Utils.setFabBackgroundWithDrawable(this, mBinding.uiNavigation.fab, R.drawable.home_icon);
    }

    public void tintButton(ImageView button) {
        ColorStateList colours = makeSelector();
        Drawable d = DrawableCompat.wrap(button.getDrawable());
        DrawableCompat.setTintList(d, colours);
        button.setImageDrawable(d);
    }

    public ColorStateList makeSelector() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_selected}, // selected
                new int[]{}};

        int[] colors = new int[]{
                ContextCompat.getColor(this, R.color.colorPrimary),
                Color.parseColor("#8a8a8a"),
        };
        ColorStateList myList = new ColorStateList(states, colors);
        return myList;
    }

    private void setCheckedImage(int id) {
        Log.e("setCheckedImage()", "" + id);
        for (int i = 0; i < imageViews.size(); i++)
            if (id != 0 && imageViews.get(i).getId() == id) {
                imageViews.get(i).setSelected(true);
                textViews.get(i).setSelected(true);
            } else {
                imageViews.get(i).setSelected(false);
                textViews.get(i).setSelected(false);
            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mBinding.drawer.openDrawer(GravityCompat.START);
                return true;
            case R.id.menu_notification:
                 startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pushFragments(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.fragment_replace, fragment);
        ft.commit();
    }

    public void popFragments() {

        Fragment fragment = mStacks.get(AppConstants.NAVIGATION_KEY).elementAt(mStacks.get(AppConstants.NAVIGATION_KEY).size() - 2);
        /*pop current fragment from stack.. */
        mStacks.get(AppConstants.NAVIGATION_KEY).pop();

        /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mBinding.drawer.isDrawerOpen(GravityCompat.START);
        if (drawerOpen) {
            mBinding.txtUserName.setText(session.getAuthorizedUser(this).getName().toString());
            if (session.getAuthorizedUser(this).getPhoto() != null) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_user_placeholder)
                        .error(R.drawable.ic_user_placeholder);
                Glide.with(this)
                        .load(session.getAuthorizedUser(this).getPhoto())
                        .apply(options)
                        .into(mBinding.imgLogin);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void setData() {

        if (session.getRollId() == 2) {
            if (session.getAuthorizedUser(this).isGym_trainer()) {
                objectDrawerItems = getObjectDrawerItemGymTrainer();
            } else {
                objectDrawerItems = getObjectDrawerItemNormalTrainer();
            }
        } else {
            objectDrawerItems = getObjectDrawerItemGym();
        }

//        mBinding.txtLogin.setText(session.getAuthorizedUser(this).getUserName());
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mBinding.drawerList.setLayoutManager(lm);
        mBinding.drawerList.setNestedScrollingEnabled(false);
        drawerItemCustomAdapter = new DrawerItemAdapter(this, objectDrawerItems, this, position);
        mBinding.drawerList.setAdapter(drawerItemCustomAdapter);
        mBinding.uiNavigation.imgClient.setOnClickListener(this);
        mBinding.uiNavigation.txtClient.setOnClickListener(this);
        mBinding.uiNavigation.imgProgramTemplate.setOnClickListener(this);
        mBinding.uiNavigation.imgProfile.setOnClickListener(this);
        mBinding.uiNavigation.txtProfile.setOnClickListener(this);
        mBinding.uiNavigation.txtProgramTemplate.setOnClickListener(this);
        mBinding.uiNavigation.txtPackages.setOnClickListener(this);
        mBinding.uiNavigation.imgPackages.setOnClickListener(this);
        /*if (getIntent().hasExtra("order") && getIntent().getBooleanExtra("order", false) && !isSP) {
            pushFragments(AppConstants.NAVIGATION_KEY, FragmentBookingHistory.newInstance(), true, true);
            drawerItemCustomAdapter.setPosition(1);
        }
*/
    }

    //gym trainer navigation
    private ArrayList<DrawerItem> getObjectDrawerItemGymTrainer() {
        ArrayList<DrawerItem> objectDrawerItems = new ArrayList<>();
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_home, R.drawable.ic_home, getString(R.string.home)));
        //objectDrawerItems.add(new DrawerItem(R.drawable.ic_users, R.drawable.ic_users, "My Clients"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_workout_nav, R.drawable.ic_workout_nav, getString(R.string.workouts)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_apple_nav, R.drawable.ic_apple_nav, getString(R.string.nutrition)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_training_program_nav, R.drawable.ic_training_program_nav, getString(R.string.training_program)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_schedule, R.drawable.ic_schedule, getString(R.string.scheduler)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_gym_drawer, R.drawable.ic_gym_drawer, "Gym Profile"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_client_enqriry, R.drawable.ic_client_enqriry, "Gym Inquiries"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_shop, R.drawable.ic_shop, "Shop"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_chat_line_nav, R.drawable.ic_chat_line_nav, getString(R.string.chat)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_nav_purchase, R.drawable.ic_nav_purchase, "Purchase"));
      //  objectDrawerItems.add(new DrawerItem(R.drawable.ic_settings_nav, R.drawable.ic_settings_nav, getString(R.string.settings)));
        return objectDrawerItems;
    }

    //gym navigation
    private ArrayList<DrawerItem> getObjectDrawerItemGym() {
        ArrayList<DrawerItem> objectDrawerItems = new ArrayList<>();
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_home, R.drawable.ic_home, getString(R.string.home)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_users, R.drawable.ic_users, "My Clients"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_workout_nav, R.drawable.ic_workout_nav, getString(R.string.workouts)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_apple_nav, R.drawable.ic_apple_nav, getString(R.string.nutrition)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_training_program_nav, R.drawable.ic_training_program_nav, getString(R.string.training_program)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_package_nav, R.drawable.ic_package_nav, getString(R.string.packages)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_subscription, R.drawable.ic_subscription, "Gym Subscription"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_schedule, R.drawable.ic_schedule, getString(R.string.scheduler)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_shop, R.drawable.ic_shop, "Shop"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_client_enqriry, R.drawable.ic_client_enqriry, "Client Inquiries"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_client_request, R.drawable.ic_client_request, "Client Request"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_gym_drawer, R.drawable.ic_gym_drawer, "Gym Trainer"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_chat_line_nav, R.drawable.ic_chat_line_nav, getString(R.string.chat)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_nav_purchase, R.drawable.ic_nav_purchase, "Purchase"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_subscription, R.drawable.ic_subscription, "Send Notification"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_settings_nav, R.drawable.ic_settings_nav, getString(R.string.settings)));
        return objectDrawerItems;
    }

    //normal trainer navigation
    private ArrayList<DrawerItem> getObjectDrawerItemNormalTrainer() {
        ArrayList<DrawerItem> objectDrawerItems = new ArrayList<>();
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_home, R.drawable.ic_home, getString(R.string.home)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_workout_nav, R.drawable.ic_workout_nav, getString(R.string.workouts)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_apple_nav, R.drawable.ic_apple_nav, getString(R.string.nutrition)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_training_program_nav, R.drawable.ic_training_program_nav, getString(R.string.training_program)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_package_nav, R.drawable.ic_package_nav, getString(R.string.packages)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_schedule, R.drawable.ic_schedule, getString(R.string.scheduler)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_client_enqriry, R.drawable.ic_client_enqriry, "Client Inquiries"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_client_request, R.drawable.ic_client_request, "Client Request"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_shop, R.drawable.ic_shop, "Shop"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_chat_line_nav, R.drawable.ic_chat_line_nav, getString(R.string.chat)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_nav_purchase, R.drawable.ic_nav_purchase, "Purchase"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_settings_nav, R.drawable.ic_settings_nav, getString(R.string.settings)));
        return objectDrawerItems;
    }

    @Override
    public void onItemClicked(int position, String name) {
        setCheckedImage(0);
        if (this.position != position) {
            switch (name) {
                case "Home":
                    this.position = position;
                    if (session.getRollId() == 2) {
                        pushFragments(AppConstants.NAVIGATION_KEY, FragmentDashboard.newInstance(), true, true);
                    } else {
                        pushFragments(AppConstants.NAVIGATION_KEY, FragmentGymDashboard.newInstance(), true, true);
                    }
                    break;
                case "Workouts":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, FragmentWorkout.newInstance(), true, true);
                    break;
                case "Nutrition":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, FragmentNutrition.newInstance(), true, true);
                    break;
                case "Training Programs":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, FragmentTrainingProgram.newInstance(), true, true);
                    break;
                case "Packages":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, PackageListFragment.newInstance(), true, true);
                    break;
                case "Profile":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, ProfileFragment.newInstance(), true, true);
                    break;
                case "Client Request":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, NotificationTypeFragment.newInstance(), true, true);
                    break;
                case "Chat":
                    Intent intent = new Intent(getApplicationContext(), ConversationActivity.class);
                    startActivity(intent);
                    break;
                case "Schedule":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new ScheduleFragment(), true, true);
                    break;
                case "Settings":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new SettingFragment(), true, true);
                    break;
                case "Gym Trainer":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new GymTrainerListFragment(), true, true);
                    break;
                case "My Clients":
                    this.position = position;
                    if (session.getRollId() == 2) {
                        pushFragments(AppConstants.NAVIGATION_KEY, new MyClientListFragment(), true, true);
                    } else {
                        pushFragments(AppConstants.NAVIGATION_KEY, GymClientListFragment.newInstance(0), true, true);
                    }
                    break;
                case "Gym Subscription":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, GymSubscriptionListFragment.newInstance(new Bundle()), true, true);
                    break;
                case "Shop":
                    /* if (session.getRollId() == 2) {*/
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new TrainerShopFragment(), true, true);
                    // }
                    break;
                case "Client Inquiries":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new ClientInquiryFragment(), true, true);
                    break;
                case "Gym Profile":
                    this.position = position;
                    FragmentTrainerProfile fragmentGymProfile = new FragmentTrainerProfile();
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("isGym", 1);
                    bundle1.putBoolean("isFromNav", true);
                    bundle1.putInt("trainerId", Integer.parseInt(session.getAuthorizedUser(this).getGym_id()));
                    fragmentGymProfile.setArguments(bundle1);
                    pushFragments(AppConstants.NAVIGATION_KEY, fragmentGymProfile, true, true);
                    break;
                case "Purchase":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new FragmentPurchaseHistory(), true, true);
                    break;
                case "Gym Inquiries":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new TrainerInquiriesFragment(), true, true);
                    break;

                case "Send Notification":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new SendNewNotificationFragment(), true, true);
                    break;

            }
        }

        drawerItemCustomAdapter.setPosition(this.position);
        if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setLogout() {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Logout Alert")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    callLogoutAPI();

                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
    }

    private void callLogoutAPI() {
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).logout()
                , getCompositeDisposable(), logout, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mStacks.get(AppConstants.NAVIGATION_KEY).size() == 0) {
            return;
        }
        /*Now current fragment on screen gets onActivityResult callback..*/
        mStacks.get(AppConstants.NAVIGATION_KEY).lastElement().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

            /* if (!((BaseFragment) mStacks.get(AppConstants.NAVIGATION_KEY).lastElement()).onBackPressed()) {

             *//*
             * top fragment in current tab doesn't handles back press, we can do our thing, which is
             *
             * if current tab has only one fragment in stack, ie first fragment is showing for this tab.
             *        finish the activity
             * else
             *        pop to previous fragment in stack for the same tab
             *
             *//*
                if (mStacks.get(AppConstants.NAVIGATION_KEY).size() == 1) {
                    //If current tab is not Home then set Home as current tab
                    finish();
                } else {
                    if (mStacks.get(AppConstants.NAVIGATION_KEY).lastElement() instanceof FragmentDashboard)
                        finish();
                    else
                        popFragments();
                }
            } else {
                //do nothing.. fragment already handled back button press.
                Logger.e("BackPress Manage By Fragment");
            }*/
            /*if (!((BaseFragment) mStacks.get(AppConstants.NAVIGATION_KEY).lastElement()).onBackPressed()) {
             *//*
             * top fragment in current tab doesn't handles back press, we can do our thing, which is
             *
             * if current tab has only one fragment in stack, ie first fragment is showing for this tab.
             *        finish the activity
             * else
             *        pop to previous fragment in stack for the same tab
             *
             *//*
                if (mStacks.get(AppConstants.NAVIGATION_KEY).size() == 1) {
                    //If current tab is not Home then set Home as current tab
                    if (!AppConstants.NAVIGATION_KEY.equals(AppConstants.TAB_BLENDER)) {
                        //setCurrentTab(0);
                    } else {
                        try {
                            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                                if (doubleBackToExitPressedOnce) {
                                    finish();
                                    return;
                                }
                                this.doubleBackToExitPressedOnce = true;
                                Toast.makeText(this, getString(R.string.prompt_exit), Toast.LENGTH_SHORT).show();
                                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
                            } else {
                                getSupportFragmentManager().popBackStack();
                            }
                        } catch (Exception e) {
                            super.onBackPressed();
                        }
                    }
                } else {
                    popFragments();
                }
            } else {
                //do nothing.. fragment already handled back button press.
                Logger.e("BackPress Manage By Fragment");
            }*/
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notification, menu);
        Utils.setMenuItemDrawable(this, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (!view.isSelected()) {
            position = -1;
            drawerItemCustomAdapter.setPosition(this.position);
            setCheckedImage(view.getId());
            switch (view.getId()) {
                case R.id.imgClient:
                    if (session.getRollId() == 2) {
                        pushFragments(AppConstants.NAVIGATION_KEY, new MyClientListFragment(), true, true);
                    } else {
                        pushFragments(AppConstants.NAVIGATION_KEY, GymClientListFragment.newInstance(1), true, true);
                    }
                    break;
                case R.id.txtClient:
                    if (session.getRollId() == 2) {
                        pushFragments(AppConstants.NAVIGATION_KEY, new MyClientListFragment(), true, true);
                    } else {
                        pushFragments(AppConstants.NAVIGATION_KEY, GymClientListFragment.newInstance(1), true, true);
                    }
                    break;
                case R.id.imgProgramTemplate:
                    pushFragments(AppConstants.NAVIGATION_KEY, FragmentTrainingProgram.newInstance(), true, true);
                    break;
                case R.id.txtProgramTemplate:
                    pushFragments(AppConstants.NAVIGATION_KEY, FragmentTrainingProgram.newInstance(), true, true);
                    break;
                case R.id.imgProfile:
                    pushFragments(AppConstants.NAVIGATION_KEY, ProfileFragment.newInstance(), true, true);
                    break;
                case R.id.txtProfile:
                    pushFragments(AppConstants.NAVIGATION_KEY, ProfileFragment.newInstance(), true, true);
                    break;
                case R.id.imgPackages:
                    pushFragments(AppConstants.NAVIGATION_KEY, PackageListFragment.newInstance(), true, true);
                    break;
                case R.id.txtPackages:
                    pushFragments(AppConstants.NAVIGATION_KEY, PackageListFragment.newInstance(), true, true);
                    break;
            }
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case logout:
                disableScreen(false);
                CommonApiResponse model = (CommonApiResponse) o;
                if (model.getStatus() == AppConstants.SUCCESS) {
                    session.setLogout();
                    pref.edit().clear().commit();
                    AccessToken accessToken = AccessToken.getCurrentAccessToken();
                    boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                    if (isLoggedIn)
                        LoginManager.getInstance().logOut();
                    startActivity(new Intent(NavigationMainActivity.this, SplashActivity.class));
                    finish();
                } else {
                    Toast.makeText(this, model.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        disableScreen(false);
    }
}

