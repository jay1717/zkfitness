package com.fitzoh.app.ui.activity;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ActivityWorkoutFinishBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutFinishActivity extends BaseActivity {

    ActivityWorkoutFinishBinding mBinding;

    public WorkoutFinishActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_workout_finish);
        Utils.setSwitchTint(this, mBinding.register.toggle);
//        Utils.getShapeGradient(this, mBinding.register.btnFinish);
        Utils.getBlueGreenGradient(ContextCompat.getColor(this, R.color.colorAccent), mBinding.register.btnCancel, true);
        Utils.getBlueGreenGradient(ContextCompat.getColor(this, R.color.colorPrimary), mBinding.register.btnFinish, false);

        mBinding.register.btnCancel.setOnClickListener(view -> {
            Intent intent = new Intent(this, NavigationClientMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, NavigationClientMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
