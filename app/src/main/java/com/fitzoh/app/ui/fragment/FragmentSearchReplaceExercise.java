package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ReplaceExerciseAdapter;
import com.fitzoh.app.databinding.FragmentSearchReplaceExerciseBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.EquipmentListModel;
import com.fitzoh.app.model.ExerciseListModel;
import com.fitzoh.app.model.MuscleListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.equipmentList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSearchReplaceExercise extends BaseFragment implements SingleCallback, AdapterView.OnItemSelectedListener, FragmentLifeCycle, SwipeRefreshLayout.OnRefreshListener {

    FragmentSearchReplaceExerciseBinding mBinding;
    private int type = 0;
    private String userId, userAccessToken;
    private ReplaceExerciseAdapter mAdapter;
    private List<MuscleListModel.DataBean> muscleDatas = new ArrayList<>();
    private List<EquipmentListModel.DataBean> equipmentData = new ArrayList<>();
    private int workout_id;

    public FragmentSearchReplaceExercise() {
        // Required empty public constructor
    }

    public static FragmentSearchReplaceExercise newInstance(int type, int workout_id) {
        FragmentSearchReplaceExercise fragment = new FragmentSearchReplaceExercise();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putSerializable("workout_id", workout_id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.add_exercise));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
            workout_id = getArguments().getInt("workout_id", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_search_replace_exercise, container, false);
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.imgMuscle});
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.imgEquipment});
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        getExerciseList();
/*
        mBinding.btnAddExercise.setOnClickListener(view -> {
            if (validation()) {
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());

                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignExercise(requestBody)
                        , getCompositeDisposable(), save, this);
            }
        });
*/
        return mBinding.getRoot();
    }

    /*private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("workout_id", dataBean.getId());
            jsonObject.put("exercise_id", new JSONArray(mAdapter.getData()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private boolean validation() {
        if (mAdapter == null || mAdapter.getData() == null || mAdapter.getData().size() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please select exercise to add", Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }*/

    private void getExerciseList() {
        setLayout();
        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mAdapter != null)
                    mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        callAPIList();
    }

    private void callAPIList() {
        if (type == 1) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).exerciseList(workout_id, null,
                    null,
                    null, "" + session.getAuthorizedUser(mActivity).getId())
                    , getCompositeDisposable(), single, this);
        } else if (type == 3) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).exerciseList(workout_id, null, true, null, "" + session.getAuthorizedUser(mActivity).getId())
                    , getCompositeDisposable(), single, this);
        } else if (type == 4) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).exerciseList(workout_id, null, null, true, "" + session.getAuthorizedUser(mActivity).getId())
                    , getCompositeDisposable(), single, this);

        }
    }

    private void getMuscles() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getMuscles()
                , getCompositeDisposable(), workoutList, this);
    }

    private void setLayout() {
        switch (type) {
            case 1:
                mBinding.edtSearch.setVisibility(View.VISIBLE);
                mBinding.spnMuscle.setVisibility(View.GONE);
                mBinding.imgMuscle.setVisibility(View.GONE);
                mBinding.imgEquipment.setVisibility(View.GONE);
                mBinding.spnEquipment.setVisibility(View.GONE);
                break;
            case 2:
                getMuscles();
                callEquipmentApi();
                mBinding.edtSearch.setVisibility(View.GONE);
                mBinding.spnMuscle.setVisibility(View.VISIBLE);
                mBinding.spnEquipment.setVisibility(View.VISIBLE);
                mBinding.imgEquipment.setVisibility(View.VISIBLE);
                mBinding.imgMuscle.setVisibility(View.VISIBLE);
                break;
            case 3:
                mBinding.layoutSearch.setVisibility(View.GONE);
                break;
            case 4:
                mBinding.layoutSearch.setVisibility(View.GONE);
                break;
        }
    }

    private void callEquipmentApi() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getEquipment()
                , getCompositeDisposable(), equipmentList, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ExerciseListModel exerciseListModel = (ExerciseListModel) o;
                if (exerciseListModel != null && exerciseListModel.getStatus() == AppConstants.SUCCESS && exerciseListModel.getData() != null && exerciseListModel.getData().size() > 0) {
                    setAdapter(exerciseListModel.getData());
                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                MuscleListModel muscleListModel = (MuscleListModel) o;
                List<String> list = new ArrayList<>();
                list.add("Select Muscles");
                if (muscleListModel != null && muscleListModel.getStatus() == AppConstants.SUCCESS && muscleListModel.getData() != null && muscleListModel.getData().size() > 0) {
                    muscleDatas = muscleListModel.getData();
                    for (int i = 0; i < muscleDatas.size(); i++) {
                        list.add(muscleDatas.get(i).getValue());
                    }
                    setSpinner(list, true);
                } else {
                    setSpinner(list, false);
                }
                break;
            case equipmentList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                EquipmentListModel equipmentListModel = (EquipmentListModel) o;
                List<String> listData = new ArrayList<>();
                listData.add("Select Equipment");
                if (equipmentListModel != null && equipmentListModel.getStatus() == AppConstants.SUCCESS && equipmentListModel.getData() != null && equipmentListModel.getData().size() > 0) {
                    equipmentData = equipmentListModel.getData();
                    for (int i = 0; i < equipmentData.size(); i++) {
                        listData.add(equipmentData.get(i).getEquipment_name());
                    }
                    setEquipmentSpinner(listData, true);
                } else {
                    setEquipmentSpinner(listData, false);
                }
                break;

        }
    }

    private void setSpinner(List<String> list, boolean isEnable) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_add_workout, list);
        mBinding.spnMuscle.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mBinding.spnMuscle.setEnabled(isEnable);
        mBinding.spnMuscle.setOnItemSelectedListener(this);
    }

    private void setEquipmentSpinner(List<String> list, boolean isEnable) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_add_workout, list);
        mBinding.spnEquipment.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mBinding.spnEquipment.setEnabled(isEnable);
        mBinding.spnEquipment.setOnItemSelectedListener(this);
    }


    private void setAdapter(List<ExerciseListModel.DataBean> data) {
        mAdapter = new ReplaceExerciseAdapter(mActivity, data);
        mBinding.recyclerView.setVisibility(View.VISIBLE);
        mBinding.imgNoData.setVisibility(View.GONE);
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spnMuscle:
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                int muscleId = mBinding.spnMuscle.getSelectedItemPosition() <= 0 ? 0 : muscleDatas.get(mBinding.spnMuscle.getSelectedItemPosition() - 1).getId();
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).exerciseList(workout_id, muscleId, null, null, "" + session.getAuthorizedUser(mActivity).getId())
                        , getCompositeDisposable(), single, this);
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        getExerciseList();
    }

    @Override
    public void onRefresh() {
        callAPIList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
