package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.AchievementAdapter;
import com.fitzoh.app.databinding.FragmentAchievementsBinding;
import com.fitzoh.app.model.AchivementModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getAchivement;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AchievementsFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    FragmentAchievementsBinding mBinding;
    String userId, userAccessToken;
    List<String> achivementModelList;
    AchievementAdapter achivementAdapter;

    public AchievementsFragment() {
        // Required empty public constructor
    }

    public static AchievementsFragment newInstance() {
        AchievementsFragment fragment = new AchievementsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_achievements, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        callAchivementsApi();
        return mBinding.getRoot();
    }

    private void callAchivementsApi() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getAchivement()
                    , getCompositeDisposable(), getAchivement, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getAchivement:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                achivementModelList = new ArrayList<>();
                AchivementModel achivementModel = (AchivementModel) o;
                if (achivementModel.getStatus() == AppConstants.SUCCESS) {

                    if (achivementModel != null) {
                        achivementModelList.addAll(achivementModel.getData());
                        achivementAdapter = new AchievementAdapter(getActivity(), achivementModelList);
                        mBinding.recyclerView.setAdapter(achivementAdapter);
                    } else
                        showSnackBar(mBinding.mainLayout, achivementModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        callAchivementsApi();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
