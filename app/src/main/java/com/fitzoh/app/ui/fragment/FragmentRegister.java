package com.fitzoh.app.ui.fragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentRegisterBinding;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.VarificationActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.register;


public class FragmentRegister extends BaseFragment implements View.OnClickListener, SingleCallback {

    View view;
    public static final int RECORD_REQUEST_CODE = 101;
    FragmentRegisterBinding mBinding;
    String providerId = "";
    int isSocialMedia = 0;
    int id;
    boolean isPhoneNo;
    String DOB = "";
    int roleId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            mBinding = DataBindingUtil.inflate(
                    inflater, R.layout.fragment_register, container, false);
            setThemeWidget();
            view = mBinding.getRoot();
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "");
            mBinding.register.edtName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            prepareLayout();
        }
        setClickEvents();
        makeRequest();
        setHasOptionsMenu(false);
        return view;
    }

    private void setThemeWidget() {
        Utils.getShapeGradient(mActivity, mBinding.register.btnRegister);
        Utils.setTextViewStartImage(mActivity, mBinding.register.edtEmail, R.drawable.ic_email, true);
        Utils.setTextViewStartImage(mActivity, mBinding.register.edtName, R.drawable.ic_user, true);
        Utils.setTextViewStartImage(mActivity, mBinding.register.edtDate, R.drawable.ic_calendar, true);
        Utils.setSpinnerArrow(mActivity, new ImageView[]{mBinding.register.btnDateDropdown});
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioClient);
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioFitnessCenter);
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioTrainer);
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioMale);
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioFemale);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    private void prepareLayout() {
        // Animate Layouts

        if (getArguments() != null && getArguments().containsKey("name") && getArguments().containsKey("providerId") && getArguments().containsKey("id") && getArguments().containsKey("isSocialMedia") && getArguments().containsKey("email")) {
            // mBinding.register.edtName.setText(getArguments().getString("name"));
            // mBinding.register.edtName.setEnabled(false);

            if (getArguments().getString("name") != null) {
                if (!getArguments().getString("name").isEmpty()) {
                    mBinding.register.edtName.setEnabled(false);
                    mBinding.register.edtName.setText(getArguments().getString("name"));
                } else {
                    mBinding.register.edtName.setEnabled(true);
                }
            }

            if (getArguments().getString("email") != null) {
                if (!getArguments().getString("email").isEmpty()) {
                    mBinding.register.edtEmail.setEnabled(false);
                    mBinding.register.edtEmail.setText(getArguments().getString("email"));
                } else {
                    mBinding.register.edtEmail.setEnabled(true);
                }
            }

            providerId = getArguments().getString("providerId");
            id = getArguments().getInt("id");

            isSocialMedia = getArguments().getInt("isSocialMedia");
        }
        String strColor = String.format("#%06X", 0xFFFFFF & ((BaseActivity) mActivity).res.getColor(R.color.colorPrimary));
        String text = "<font color=#000000>By submitting this form you agree with our</font> <font color=" + strColor + "> Terms & Conditions</font>";
        mBinding.register.txtTerms.setText(Html.fromHtml(text));
    }

    private void setClickEvents() {
        mBinding.register.btnRegister.setOnClickListener(this);
        mBinding.register.txtTerms.setOnClickListener(this);
        mBinding.register.edtDate.setOnClickListener(this);
        mBinding.register.edtDate.addTextChangedListener(new MyTaskWatcher(mBinding.register.edtDate));
        mBinding.register.edtName.addTextChangedListener(new MyTaskWatcher(mBinding.register.edtName));
        mBinding.register.edtEmail.addTextChangedListener(new MyTaskWatcher(mBinding.register.edtEmail));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {

                        callAPI();


                        //   startActivity(new Intent(getActivity(), CreateUserActivity.class));

                        /*    if (mBinding.register.chkServiceProvider.isChecked()) {
                         *//* session.setBooleanDataByKey(KEY_IS_LOGIN, true);
                            session.setBooleanDataByKey(KEY_IS_SP, true);*//*
                            JSONObject object = new JSONObject();
                            try {
                                object.put("FirstName", mBinding.register.edtUserFirstname.getText().toString());
                                object.put("LastName", mBinding.register.edtUserLastname.getText().toString());
                                object.put("Email", mBinding.register.edtEmail.getText().toString());
                                object.put("PhoneNo", mBinding.register.edtPhone.getText().toString());
                                object.put("Password", mBinding.register.edtPassword.getText().toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            ((LoginActivity) getActivity()).pushFragments(AppConstants.LOGIN_KEY, fragmentSeriviceProviderFeild, true, true);
                        } else {
//                            showProgressBar().show();
//                            callApi();
                        }*/
                        mBinding.register.txtErrorEmail.setVisibility(View.GONE);
                        mBinding.register.txtErrorDate.setVisibility(View.GONE);
                        mBinding.register.txtErrorName.setVisibility(View.GONE);
                    }
                } else {
                    Snackbar.make(mBinding.layoutRegister, R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_date_dropdown:

              /*  final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);



                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
                                Date newDate = null;
                                try {
                                    newDate = spf.parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                spf = new SimpleDateFormat("dd MMM yyyy");
                                date = spf.format(newDate);
                                if(date!=null)
                                    mBinding.register.edtDate.setText(date);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();*/
                showDatePickerDialog();
                break;
            case R.id.edtDate:
                showDatePickerDialog();
                break;

            case R.id.txtTerms:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://fitzoh.com/termsandcondtion"));
                startActivity(browserIntent);
                break;

        }
    }

    private void callAPI() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);


        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateFormatDOB = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = dateFormat.parse(mBinding.register.edtDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DOB = dateFormatDOB.format(date);

        if (mBinding.register.radiogroupLogin.getCheckedRadioButtonId() == R.id.radio_fitness_center) {
            roleId = 3;
        } else if (mBinding.register.radiogroupLogin.getCheckedRadioButtonId() == R.id.radio_trainer) {
            roleId = 2;
        } else {
            roleId = 1;
        }

        if(roleId==3) {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Fitzoh")
                    .setMessage("Are you sure to join as Fitness center?")
                    .setPositiveButton(R.string.yes, (dialog, whichButton) -> {
                        dialog.dismiss();
                        RegisterUser();

                    })
                    .setNegativeButton(R.string.no, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    })
                    .show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        }
        else if(roleId==2){
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Fitzoh")
                    .setMessage("Are you sure to join as Trainer?")
                    .setPositiveButton(R.string.yes, (dialog, whichButton) -> {
                        dialog.dismiss();
                        RegisterUser();

                    })
                    .setNegativeButton(R.string.no, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    })
                    .show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        }else {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Fitzoh")
                    .setMessage("Are you sure to join as Client?")
                    .setPositiveButton(R.string.yes, (dialog, whichButton) -> {
                        dialog.dismiss();
                        RegisterUser();

                    })
                    .setNegativeButton(R.string.no, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    })
                    .show();
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        }



       /* } else {
            if (isSocialMedia == 1) {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).signup(roleId,
                        mBinding.register.edtName.getText().toString(),
                        mBinding.register.edtEmail.getText().toString(),
                        DOB, mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "1" : "0", isSocialMedia, "", String.valueOf(id))
                        , getCompositeDisposable(), register, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).signup(roleId,
                        mBinding.register.edtName.getText().toString(),
                        mBinding.register.edtEmail.getText().toString(),
                        DOB, mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "1" : "0", isSocialMedia, "", null)
                        , getCompositeDisposable(), register, this);
            }
        }*/

    }

    public void  RegisterUser(){
        if (isSocialMedia == 1) {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).signup(roleId,
                    mBinding.register.edtEmail.getText().toString(),
                    mBinding.register.edtName.getText().toString(),
                    DOB, mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "m" : "f", isSocialMedia, mBinding.register.edtMobile.getText().toString(), "+" + mBinding.register.countryCode.getSelectedCountryCode(), String.valueOf(id))
                    , getCompositeDisposable(), register, this);
        } else {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).signup(roleId,
                    mBinding.register.edtName.getText().toString(),
                    mBinding.register.edtEmail.getText().toString(),
                    DOB, mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "m" : "f", isSocialMedia, mBinding.register.edtMobile.getText().toString(), "+" + mBinding.register.countryCode.getSelectedCountryCode(), null)
                    , getCompositeDisposable(), register, this);
        }
    }
    public boolean validation() {
        chkIsPhoneNo();
        boolean value = true;
        if (mBinding.register.edtName.getText().toString().length() == 0) {
            setEdittextError(mBinding.register.txtErrorName, getString(R.string.empty_userfirstname));
            value = false;
        }

        if (mBinding.register.edtEmail.getText().toString().length() == 0) {
            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.empty_username_register));
            value = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.register.edtEmail.getText().toString()).matches()) {
            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.invalid_email));
            value = false;
        }
        if (mBinding.register.edtMobile.getText().toString().length() > 0) {
            if (mBinding.register.edtMobile.getText().toString().length() != 10) {
                setEdittextError(mBinding.register.txtErrorMobileNo, getString(R.string.invalid_phone));
                value = false;
            } else {
                mBinding.register.txtErrorMobileNo.setText("");
                mBinding.register.txtErrorMobileNo.setVisibility(View.GONE);
            }

        }

        if (mBinding.register.edtDate.getText().toString().length() == 0) {
            setEdittextError(mBinding.register.txtErrorDate, getString(R.string.emapty_date));
            value = false;
        }

        return value;
    }

    private void chkIsPhoneNo() {
        if (mBinding.register.edtEmail.getText().toString().matches("[0-9]+")) {
            isPhoneNo = true;
        } else {
            isPhoneNo = false;
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case register:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);

//                Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG).show();
                RegisterModel registerModel = (RegisterModel) o;
//                if (registerModel.getStatus() == AppConstants.SUCCESS) {
                if (registerModel != null && registerModel.getStatus() == AppConstants.SUCCESS) {
                    Intent intent = new Intent(getActivity(), VarificationActivity.class);
                    if (isSocialMedia == 0) {
                        intent.putExtra("id", registerModel.getData().getId());
                    } else
                        intent.putExtra("id", id);
                    intent.putExtra("email", mBinding.register.edtEmail.getText().toString());
                    intent.putExtra("isRegister", "true");
                    startActivity(intent);
                    //((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, FragmentVerificationOTP.newInstance(registerModel.getData().getId(), mBinding.register.edtEmail.getText().toString()), true, true);
                } else {
                    showSnackBar(mBinding.layoutRegister, registerModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutRegister, "onFailure", Snackbar.LENGTH_LONG);
    }


    private class MyTaskWatcher implements TextWatcher {
        private View view;

        MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.edtEmail:
                    if (mBinding.register.edtEmail.hasFocus()) {
                        if (mBinding.register.edtEmail.getText().toString().length() == 0) {
                            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.empty_username));
                        } else if (mBinding.register.edtEmail.getText().toString().matches("[0-9]+")) {
                            if (mBinding.register.edtEmail.getText().toString().length() != 10) {
                                setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.invalid_phone));
                            } else {
                                mBinding.register.txtErrorEmail.setText("");
                                mBinding.register.txtErrorEmail.setVisibility(View.GONE);
                            }
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.register.edtEmail.getText().toString()).matches()) {
                            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.invalid_email));
                        } else {
                            mBinding.register.txtErrorEmail.setText("");
                            mBinding.register.txtErrorEmail.setVisibility(View.GONE);
                        }
                    }
                    break;
                case R.id.edtDate:
                    if (mBinding.register.edtDate.hasFocus()) {
                        if (mBinding.register.edtDate.getText().toString().length() == 0) {
                            setEdittextError(mBinding.register.txtErrorDate, getString(R.string.emapty_date));
                        } else {
                            mBinding.register.txtErrorDate.setText("");
                            mBinding.register.txtErrorDate.setVisibility(View.GONE);
                        }

                    }
                    break;
                case R.id.edtName:
                    if (mBinding.register.edtName.hasFocus()) {
                        if (mBinding.register.edtName.getText().toString().length() == 0) {
                            setEdittextError(mBinding.register.txtErrorName, getString(R.string.empty_userfirstname));
                        } else {
                            mBinding.register.txtErrorName.setText("");
                            mBinding.register.txtErrorName.setVisibility(View.GONE);
                        }
                    }
                    break;

            }

        }

    }

    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                (view, year, monthOfYear, dayOfMonth) -> {
                    c.set(Calendar.YEAR, year);
                    c.set(Calendar.MONTH, monthOfYear);
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    mBinding.register.edtDate.setText(dateFormat.format(c.getTime()));
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    protected void makeRequest() {
//        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RECORD_REQUEST_CODE);
        requestAppPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }


}
