package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientDocumentAdapter;
import com.fitzoh.app.databinding.FragmentClientDocumentBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GetDocumentModel;
import com.fitzoh.app.model.TrainerProfileAddModel;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.DOWNLOAD_SERVICE;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.editTrainerProfile;

public class ClientDocumentFragment extends BaseFragment implements View.OnClickListener, SingleCallback, ClientDocumentAdapter.DownloadListener {
    private static final int RECORD_REQUEST_CODE = 101;
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImage, filePdf;
    String userId, userAccessToken;
    TrainerProfileModel trainerProfileModel = null;
    List<String> uploadedImage = new ArrayList<>();
    List<String> downloadedFile = new ArrayList<>();
    int clientId;
    // ClientListModel.DataBean dataBean;

    private FragmentClientDocumentBinding mBinding;
    TrainerProfileAddModel trainerProfileAddModel;

    public ClientDocumentFragment() {
        // Required empty public constructor
    }


    public static ClientDocumentFragment newInstance(Bundle bundle) {
        ClientDocumentFragment fragment = new ClientDocumentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            clientId = getArguments().getInt("data");
            // dataBean = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Documents");

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_document, container, false);
        Utils.setImageBackground(mActivity, mBinding.imgAdd, R.drawable.ic_plus_training_program);
        Utils.setLines(mActivity, mBinding.view.view);
        Utils.setLines(mActivity, mBinding.view.view2);
        Utils.setLines(mActivity, mBinding.viewTwo.view);
        Utils.setLines(mActivity, mBinding.viewTwo.view2);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();

        makeRequest();
        setClickEvent();
        return mBinding.getRoot();
    }

    private void setClickEvent() {
        mBinding.imgAdd.setOnClickListener(this);
        setAdapter();
        getDocument();
        mActivity.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    mActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    callAPI("image");
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK && data != null) {
//            ArrayList<MediaFile> files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
            Log.e("onActivityResult: ", "" + data.getData());
            if (data.getData() != null && data.getData().toString().startsWith("content://com.google.android.apps.docs.storage")) {
                showSnackBar(mBinding.layoutCreateProfile, "Please select PDF from local storage", Snackbar.LENGTH_LONG);
            } else {
                filePdf = new File(Utils.getPath(mActivity, data.getData()));
                callAPI("pdf");
            }
        }
    }

    private void setAdapter() {
        mBinding.recyclerUpload.setAdapter(new ClientDocumentAdapter(mActivity, uploadedImage, false, null));
        mBinding.recyclerDownload.setAdapter(new ClientDocumentAdapter(mActivity, downloadedFile, true, this));
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        mActivity.startActivityForResult(getPickImageChooserIntent(), 200);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = mActivity.getPackageManager();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = mActivity.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = mActivity.getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(mActivity);
    }

    protected void makeRequest() {
        requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA}, RECORD_REQUEST_CODE, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgAdd:
                showPopupMenu(v);
                break;
        }
    }

    private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(mActivity, view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(R.menu.menu_gallery_option, popup.getMenu());
        popup.getMenu().getItem(1).setTitle("PDF");
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener());
        popup.show();
    }

    @Override
    public void download(String url) {
        Log.e("download: ", url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        DownloadManager downloadManager = (DownloadManager) mActivity.getSystemService(DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

        //Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        //Set whether this download may proceed over a roaming connection.
        request.setAllowedOverRoaming(false);
        //Set the title of this download, to be displayed in notifications (if enabled).
        request.setTitle("Downloading");
        //Set a description of this download, to be displayed in notifications (if enabled)
        request.setDescription("Downloading File");
        //Set the local destination for the downloaded file to a path within the application's external files directory
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        //Enqueue a new download and same the referenceId
        long downloadReference = downloadManager.enqueue(request);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.unregisterReceiver(onComplete);
    }

    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        MenuActionItemClickListener() {

        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_photo:
                    imgPick();
                    break;
                case R.id.action_video:
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("application/pdf");
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                    mActivity.startActivityForResult(intent, 100);
                    /*Intent intent = new Intent(mActivity, FilePickerActivity.class);
                    mActivity.startActivityForResult(intent, 100);*/
                    break;
            }
            return false;
        }
    }

    private BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            setAdapter();
        }
    };

    private void callAPI(String type) {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody reqFile;
        MultipartBody.Part photo;
        RequestBody client_id = RequestBody.create(MediaType.parse("text/plain"), "" + clientId);
        if (type.equalsIgnoreCase("image")) {
            byte[] uploadBytes = Utils.loadImageFromStorage(fileImage.getPath());
             reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
           // reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            photo = MultipartBody.Part.createFormData("image[0]", fileImage.getName(), reqFile);
        } else {
            reqFile = RequestBody.create(MediaType.parse("application/pdf"), filePdf);
            photo = MultipartBody.Part.createFormData("image[0]", filePdf.getName(), reqFile);
        }
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).uploadDocument(
                client_id,
                photo), getCompositeDisposable(), editTrainerProfile, this);
    }

    private void getDocument() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(false);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getDocument(clientId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerCertificates, this);
        } else {
            showSnackBar(mBinding.layoutCreateProfile, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getTrainerCertificates:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                GetDocumentModel getDocumentModel = (GetDocumentModel) o;
                if (getDocumentModel != null && getDocumentModel.getStatus() == AppConstants.SUCCESS && getDocumentModel.getData() != null) {
                    if (getDocumentModel.getData().getDownload_files() != null && getDocumentModel.getData().getDownload_files().size() > 0) {
                        downloadedFile = getDocumentModel.getData().getDownload_files();
                    }
                    if (getDocumentModel.getData().getUploaded_images() != null && getDocumentModel.getData().getUploaded_images().size() > 0) {
                        uploadedImage = getDocumentModel.getData().getUploaded_images();
                    }
                    setAdapter();
                }
                break;
            case editTrainerProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.layoutCreateProfile, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                    getDocument();
                } else {
                    showSnackBar(mBinding.layoutCreateProfile, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutCreateProfile, throwable.getMessage() == null ? getString(R.string.something_went_wrong) : throwable.getMessage(), Snackbar.LENGTH_LONG);
    }
}
