package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentAddFoodBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.NutritionValuesData;
import com.fitzoh.app.model.SearchDietPlanFoodData;
import com.fitzoh.app.model.ServingSizeData;
import com.fitzoh.app.model.ServingSizeUnitData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.SearchFoodActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addDietPlanFood;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getNutritionValues;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getServingSizeUnits;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getServingSizes;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddFoodFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddFoodFragment extends BaseFragment implements SingleCallback, AdapterView.OnItemSelectedListener, TextView.OnEditorActionListener {
    FragmentAddFoodBinding mBinding;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SearchDietPlanFoodData.DataBean searchData;
    String userId, userAccessToken;
    int dietPlanId, dietPlanMealId;
    List<ServingSizeData.DataBean> servingSizeList;
    List<ServingSizeUnitData.DataBean> servingSizeUnitList;
    boolean isNoOfServing, isServingSize;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public AddFoodFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddFoodFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddFoodFragment newInstance(String param1, String param2) {
        AddFoodFragment fragment = new AddFoodFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static AddFoodFragment newInstance() {
        AddFoodFragment fragment = new AddFoodFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Add Food Item");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_food, container, false);
        Utils.getShapeGradient(mActivity, mBinding.layoutEditWorkout.btnAddItem);
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.layoutEditWorkout.btnServingDropdown, mBinding.layoutEditWorkout.btnServingSizeDropdown});
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        setHasOptionsMenu(true);
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        if (getArguments() != null) {
            dietPlanId = getArguments().getInt("diet_plan_id");
            dietPlanMealId = getArguments().getInt("diet_plan_meal_id");
        }
        mBinding.layoutEditWorkout.txtSearchFood.setOnClickListener(view -> {
            mBinding.layoutEditWorkout.txtErrorSearchFood.setText("");
            mBinding.layoutEditWorkout.txtErrorSearchFood.setVisibility(View.GONE);
            mActivity.startActivityForResult(new Intent(mActivity, SearchFoodActivity.class), 10);
        });

        List<String> listServingUnit = new ArrayList<>();
        listServingUnit.add("Serving Size");
        setSpinner(mBinding.layoutEditWorkout.spnServingSize, listServingUnit, false);

        //  getServingSizes();
        mBinding.layoutEditWorkout.edtServingNo.setOnEditorActionListener(this);
        mBinding.layoutEditWorkout.btnAddItem.setOnClickListener(view -> {
            if (Utils.isOnline(mActivity)) {
                if (searchData != null)
                    addFood();
            } else
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

        });


      /*  mBinding.layoutEditWorkout..setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(EditText v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    return true;
                }
                return false;
            }
        });*/

        return mBinding.getRoot();
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            isNoOfServing = true;
            if (isNoOfServing) {
                if (mBinding.layoutEditWorkout.edtServingNo.getText().toString().length() <= 0) {
                    setEdittextError(mBinding.layoutEditWorkout.txtErrorNoOfServing, "Select Serving size");
                } else {
                    mBinding.layoutEditWorkout.txtErrorNoOfServing.setText("");
                    mBinding.layoutEditWorkout.txtErrorNoOfServing.setVisibility(View.GONE);
                    if (searchData == null) {
                        isNoOfServing = false;
                        // mBinding.layoutEditWorkout.spnNoOfServing.setSelection(0);
                        setEdittextError(mBinding.layoutEditWorkout.txtErrorSearchFood, "Please select food/drink");
                    } else
                        getNutritionValues();
                }
            } else {
                isNoOfServing = true;
            }
            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
        return false;
    }


   /* private void getServingSizes() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getServingSizes()
                , getCompositeDisposable(), getServingSizes, this);
    }*/

    private void getServingSizeUnits(String foodName) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getServingSizeUnits(foodName)
                , getCompositeDisposable(), getServingSizeUnits, this);
    }

    private void addFood() {
        if ( servingSizeUnitList != null) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addDietPlanFood(dietPlanMealId, searchData.getId(), mBinding.layoutEditWorkout.edtServingNo.getText().toString(), servingSizeUnitList.get(mBinding.layoutEditWorkout.spnServingSize.getSelectedItemPosition() - 1).getId(), servingSizeUnitList.get(mBinding.layoutEditWorkout.spnServingSize.getSelectedItemPosition() - 1).getName(), dietPlanId,session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), addDietPlanFood, this);
        }
    }

    private void getNutritionValues() {
        if (mBinding.layoutEditWorkout.edtServingNo.getText().toString().length() > 0 && servingSizeUnitList != null) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getNutritionValues(searchData.getId(), servingSizeUnitList.get(mBinding.layoutEditWorkout.spnServingSize.getSelectedItemPosition() - 1).getName(), servingSizeUnitList.get(mBinding.layoutEditWorkout.spnServingSize.getSelectedItemPosition() - 1).getId(), mBinding.layoutEditWorkout.edtServingNo.getText().toString())
                    , getCompositeDisposable(), getNutritionValues, this);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            searchData = (SearchDietPlanFoodData.DataBean) data.getSerializableExtra("searchData");
            if (searchData != null) {
                mBinding.layoutEditWorkout.layoutNutritionsFact.txtNoCaloriesValue.setText(searchData.getCalories());
                mBinding.layoutEditWorkout.layoutNutritionsFact.txtProteinValue.setText(searchData.getDishesProtien());
                mBinding.layoutEditWorkout.layoutNutritionsFact.txtCarbsValue.setText(searchData.getCarbs());
                mBinding.layoutEditWorkout.layoutNutritionsFact.txtFatValue.setText(searchData.getFat());
                mBinding.layoutEditWorkout.txtSearchFood.setText(searchData.getFoodName());
                getServingSizeUnits(searchData.getFoodName());
            }

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        mActivity.invalidateOptionsMenu();
    }

    private void setSpinner(Spinner spinner, List<String> list, boolean isEnable) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_add_workout, list);
        spinner.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setEnabled(isEnable);
        if (spinner.getId() == R.id.spnNoOfServing)
            mBinding.layoutEditWorkout.spnNoOfServing.setOnItemSelectedListener(this);
        if (spinner.getId() == R.id.spnServingSize)
            mBinding.layoutEditWorkout.spnServingSize.setOnItemSelectedListener(this);

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
           /* case getServingSizes:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ServingSizeData servingSizeData = (ServingSizeData) o;
                List<String> list = new ArrayList<>();
                list.add("No. of Serving");
                if (servingSizeData != null && servingSizeData.getStatus() == AppConstants.SUCCESS && servingSizeData.getData() != null && servingSizeData.getData().size() > 0) {
                    servingSizeList = servingSizeData.getData();
                    for (int i = 0; i < servingSizeList.size(); i++) {
                        list.add(String.valueOf(servingSizeList.get(i).getValue()));
                    }
                    setSpinner(mBinding.layoutEditWorkout.spnNoOfServing, list, true);
                } else {
                    setSpinner(mBinding.layoutEditWorkout.spnNoOfServing, list, false);
                }
                List<String> listServingUnit = new ArrayList<>();
                listServingUnit.add("Serving Size");
                setSpinner(mBinding.layoutEditWorkout.spnServingSize, listServingUnit, false);
                break;*/
            case getServingSizeUnits:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ServingSizeUnitData servingSizeUnitData = (ServingSizeUnitData) o;
                List<String> listUnit = new ArrayList<>();
                listUnit.add("Serving Size");
                isServingSize = false;
                if (servingSizeUnitData != null && servingSizeUnitData.getStatus() == AppConstants.SUCCESS && servingSizeUnitData.getData() != null && servingSizeUnitData.getData().size() > 0) {
                    servingSizeUnitList = servingSizeUnitData.getData();
                    for (int i = 0; i < servingSizeUnitList.size(); i++) {
                        listUnit.add(servingSizeUnitList.get(i).getName());
                    }
                    setSpinner(mBinding.layoutEditWorkout.spnServingSize, listUnit, true);
                } else {
                    setSpinner(mBinding.layoutEditWorkout.spnServingSize, listUnit, false);
                }
                isServingSize = false;
                isNoOfServing = false;
                if (searchData != null) {
                    mBinding.layoutEditWorkout.spnServingSize.setSelection(((ArrayAdapter) mBinding.layoutEditWorkout.spnServingSize.getAdapter()).getPosition(searchData.getServingSizeUnit()));
                    // mBinding.layoutEditWorkout.spnNoOfServing.setSelection(((ArrayAdapter) mBinding.layoutEditWorkout.spnNoOfServing.getAdapter()).getPosition(searchData.getServingSize()));
                    mBinding.layoutEditWorkout.edtServingNo.setText(searchData.getServingSize());
                }
                break;
            case addDietPlanFood:
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case getNutritionValues:
                NutritionValuesData nutritionValuesData = (NutritionValuesData) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (nutritionValuesData != null && nutritionValuesData.getStatus() == AppConstants.SUCCESS) {
                    mBinding.layoutEditWorkout.layoutNutritionsFact.txtNoCaloriesValue.setText(nutritionValuesData.getData().getCalories());
                    mBinding.layoutEditWorkout.layoutNutritionsFact.txtProteinValue.setText(nutritionValuesData.getData().getProtein());
                    mBinding.layoutEditWorkout.layoutNutritionsFact.txtCarbsValue.setText(nutritionValuesData.getData().getCarbs());
                    mBinding.layoutEditWorkout.layoutNutritionsFact.txtFatValue.setText(nutritionValuesData.getData().getFat());
                } else {
                    showSnackBar(mBinding.linear, nutritionValuesData.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
           /* case R.id.spnNoOfServing:
                if (isNoOfServing) {
                    if (mBinding.layoutEditWorkout.spnNoOfServing.getSelectedItemPosition() <= 0) {
                        setEdittextError(mBinding.layoutEditWorkout.txtErrorNoOfServing, "Select Serving size");
                    } else {
                        mBinding.layoutEditWorkout.txtErrorNoOfServing.setText("");
                        mBinding.layoutEditWorkout.txtErrorNoOfServing.setVisibility(View.GONE);
                        if (searchData == null) {
                            isNoOfServing = false;
                            mBinding.layoutEditWorkout.spnNoOfServing.setSelection(0);
                            setEdittextError(mBinding.layoutEditWorkout.txtErrorSearchFood, "Please select food/drink");
                        } else
                            getNutritionValues();
                    }
                } else {
                    isNoOfServing = true;
                }
                break;*/
            case R.id.spnServingSize:
                if (isServingSize) {
                    if (mBinding.layoutEditWorkout.spnServingSize.getSelectedItemPosition() <= 0) {
                        setEdittextError(mBinding.layoutEditWorkout.txtErrorServingSize, "Select Serving size unit");
                    } else {
                        mBinding.layoutEditWorkout.txtErrorServingSize.setText("");
                        mBinding.layoutEditWorkout.txtErrorServingSize.setVisibility(View.GONE);
                        if (searchData == null) {
                            isServingSize = false;
                            mBinding.layoutEditWorkout.spnServingSize.setSelection(0);
                            setEdittextError(mBinding.layoutEditWorkout.txtErrorSearchFood, "Please select food/drink");
                        } else
                            getNutritionValues();
                    }
                } else {
                    isServingSize = true;
                }
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
