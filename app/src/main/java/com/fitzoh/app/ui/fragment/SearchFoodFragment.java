package com.fitzoh.app.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.fitzoh.app.R;

import com.fitzoh.app.adapter.DietFoodSearchAdapter;
import com.fitzoh.app.databinding.FragmentSearchFoodBinding;
import com.fitzoh.app.model.SearchDietPlanFoodData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.searchDietPlanFood;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFoodFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFoodFragment extends BaseFragment implements SingleCallback, DietFoodSearchAdapter.onClickItem {
    FragmentSearchFoodBinding mBinding;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    String userId, userAccessToken;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public SearchFoodFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddFoodFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFoodFragment newInstance(String param1, String param2) {
        SearchFoodFragment fragment = new SearchFoodFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static SearchFoodFragment newInstance() {
        SearchFoodFragment fragment = new SearchFoodFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Search Food Item");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_food, container, false);
        setHasOptionsMenu(true);

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutSearchFood.recyclerSearch.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        mBinding.layoutSearchFood.edtSearchFood.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (Utils.isOnline(mContext)) {
                        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(mBinding.layoutSearchFood.edtSearchFood.getWindowToken(), 0);
                        callSearchApi(mBinding.layoutSearchFood.edtSearchFood.getText().toString());
                    } else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                    return true;
                }

                return false;
            }
        });

        return mBinding.getRoot();
    }

    private void callSearchApi(String searchText) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).searchDietPlanFood(searchText)
                , getCompositeDisposable(), searchDietPlanFood, this);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case searchDietPlanFood:
                SearchDietPlanFoodData dietPlanFoodData = (SearchDietPlanFoodData) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (dietPlanFoodData != null && dietPlanFoodData.getStatus() == AppConstants.SUCCESS && dietPlanFoodData.getData() != null) {
                    if (dietPlanFoodData.getData().size() == 0) {
                        mBinding.layoutSearchFood.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.layoutSearchFood.recyclerSearch.setVisibility(View.GONE);
                    } else {
                        mBinding.layoutSearchFood.imgNoData.setVisibility(View.GONE);
                        mBinding.layoutSearchFood.recyclerSearch.setVisibility(View.VISIBLE);
                        DietFoodSearchAdapter adapter = new DietFoodSearchAdapter(getActivity(), dietPlanFoodData.getData(), this);
                        mBinding.layoutSearchFood.recyclerSearch.setAdapter(adapter);
                    }
                } else {
                    showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onClick(SearchDietPlanFoodData.DataBean data) {
        Intent intent = new Intent();
        intent.putExtra("searchData", data);
        mActivity.setResult(Activity.RESULT_OK, intent);
        mActivity.finish();
    }
}
