package com.fitzoh.app.ui.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentSettingBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AppThemeSettingActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.notification;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends BaseFragment implements View.OnClickListener, SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentSettingBinding mBinding;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String userAccessToken, userId;
    String data = "";


    public SettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Settings");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        setClickListners();
        if (session.getAuthorizedUser(mContext).getRoleId().equals("1")) {
            mBinding.layoutSetting.txtAppTheme.setVisibility(View.GONE);
            mBinding.layoutSetting.viewAppTheme.setVisibility(View.GONE);

        } else {
            mBinding.layoutSetting.txtAppTheme.setVisibility(View.VISIBLE);
            mBinding.layoutSetting.viewAppTheme.setVisibility(View.VISIBLE);
        }
        userId = String.valueOf(session.getAuthorizedUser(getContext()).getId());
        userAccessToken = session.getAuthorizedUser(getContext()).getUserAccessToken();
        return mBinding.getRoot();
    }

    private void setClickListners() {
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setSwitchTint(mActivity, mBinding.layoutSetting.toggle);
        mBinding.layoutSetting.txtAppTheme.setOnClickListener(this);
        mBinding.layoutSetting.txtCheckFor.setOnClickListener(this);
        mBinding.layoutSetting.txtUnitTypeSelect.setOnClickListener(this);
        mBinding.layoutSetting.txtCurrencySelect.setText(session.getAuthorizedUser(mActivity).getCurrency());
        mBinding.layoutSetting.txtUnitTypeSelect.setText(session.getAuthorizedUser(mActivity).getWeight_type());
        mBinding.layoutSetting.toggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            setNotification(isChecked);
        });

    }


    public void setNotification(boolean isChecked) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).updateNotification(isChecked ? 1 : 0)
                , getCompositeDisposable(), notification, this);
    }

    private void callAPI() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
//            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new UnauthorizedNetworkInterceptor(mActivity)).create(WebserviceBuilder.class).checkVersion(BuildConfig.VERSION_NAME)
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new UnauthorizedNetworkInterceptor(mActivity)).create(WebserviceBuilder.class).checkVersion("1.0.1")
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtAppTheme:
                Intent intent = new Intent(getActivity(), AppThemeSettingActivity.class);
                startActivity(intent);
                break;
            case R.id.txtCheckFor:
                callAPI();
                break;
            case R.id.txtUnitTypeSelect:
                selectUniType();
                break;
        }
    }

    private void selectUniType() {
        String[] list = new String[]{"kg", "lbs"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Choose Unit");
        builder.setSingleChoiceItems(list, 0, (dialog, which) -> {
            data = list[which];
        });
        builder.setPositiveButton("OK", (dialog, which) -> {
            mBinding.layoutSetting.txtUnitTypeSelect.setText(data);
            setUnitAPI(data);
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
    }

    private void setUnitAPI(String data) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).updateWeightUnit(data)
                , getCompositeDisposable(), save, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case notification:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.mainLayout, "Notification updated successfully", Snackbar.LENGTH_LONG);
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.CHECK_UPDATE) {
                    ((BaseActivity) mActivity).showDialog(response.getMessage(), mActivity);
                } else {
                    showSnackBar(mBinding.mainLayout, "Your app is up to date", Snackbar.LENGTH_LONG);
                }
                break;
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse1 = (CommonApiResponse) o;
                if (commonApiResponse1 != null && commonApiResponse1.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.mainLayout, "Unit Change successfully", Snackbar.LENGTH_LONG);
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse1.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
