package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.ProfileUpdateFragment;
import com.fitzoh.app.ui.fragment.UploadPhotoFragment;

public class TransformationAddActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        ProfileUpdateFragment profileUpdateFragment=new ProfileUpdateFragment();
        profileUpdateFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent,profileUpdateFragment, "ProfileUpdateFragment")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
