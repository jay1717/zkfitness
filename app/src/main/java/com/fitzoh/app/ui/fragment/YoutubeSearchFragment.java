package com.fitzoh.app.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SearchYoutubeExerciseAdapter;
import com.fitzoh.app.databinding.FragmentYoutubeSearchBinding;
import com.fitzoh.app.model.YouTubeModel.Item;
import com.fitzoh.app.model.YouTubeModel.YoutubeSearchModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class YoutubeSearchFragment extends BaseFragment implements SingleCallback {

    FragmentYoutubeSearchBinding mBinding;
    SearchYoutubeExerciseAdapter mAdapter;

    public static YoutubeSearchFragment newInstance(Bundle bundle) {
        YoutubeSearchFragment fragment = new YoutubeSearchFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_youtube_search, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.search_youtube));
        setListner();
        return mBinding.getRoot();

    }

    private void setAdapter(List<Item> items) {
        mAdapter = new SearchYoutubeExerciseAdapter(mActivity,items);
        mBinding.recyclerView.setVisibility(View.VISIBLE);
//        mBinding.imgNoData.setVisibility(View.GONE);
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void setListner() {
        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mBinding.edtSearch.getText().toString().length() == 0) {
                } else {
                    callApi(mBinding.edtSearch.getText().toString().trim());

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void callApi(String input) {

        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getYoutubeClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).getYouTube("snippet", getResources().getString(R.string.toutube_search_api), "video", input, 50)
                , getCompositeDisposable(), single, this);
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case single:
                disableScreen(false);
                YoutubeSearchModel youtubeSearchModel = (YoutubeSearchModel) o;
                if (youtubeSearchModel != null && youtubeSearchModel.getItems() != null && youtubeSearchModel.getItems().size() > 0) {
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.imgNoData.setVisibility(View.GONE);
                    setAdapter(youtubeSearchModel.getItems());

                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }
}
