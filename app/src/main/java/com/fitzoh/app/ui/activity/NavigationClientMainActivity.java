package com.fitzoh.app.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.DrawerItemAdapter;
import com.fitzoh.app.databinding.ActivityNavigationClientMainBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DrawerItem;
import com.fitzoh.app.model.VerificationOTPModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.CheckInsDataFragment;
import com.fitzoh.app.ui.fragment.ClientShopFragment;
import com.fitzoh.app.ui.fragment.ClientWorkoutFragment;
import com.fitzoh.app.ui.fragment.FragmentClientNutrition;
import com.fitzoh.app.ui.fragment.FragmentPurchaseHistoryClient;
import com.fitzoh.app.ui.fragment.FragmentTrainerProfile;
import com.fitzoh.app.ui.fragment.FragmentTrainingProgramClient;
import com.fitzoh.app.ui.fragment.HomeClientFragment;
import com.fitzoh.app.ui.fragment.HomeClientSubscribeFragment;
import com.fitzoh.app.ui.fragment.ProfileFragment;
import com.fitzoh.app.ui.fragment.ReportCLientOptionFragment;
import com.fitzoh.app.ui.fragment.SettingFragment;
import com.fitzoh.app.ui.fragment.TrainerInquiriesFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.logout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.utils.SessionManager.KEY_IS_SP;

public class NavigationClientMainActivity extends BaseActivity implements DrawerItemAdapter.OnRecyclerViewClickListener, View.OnClickListener, SingleCallback {
    public ActivityNavigationClientMainBinding mBinding;
    private HashMap<String, Stack<Fragment>> mStacks;
    public ArrayList<DrawerItem> objectDrawerItems;
    private DrawerItemAdapter drawerItemCustomAdapter;
    boolean doubleBackToExitPressedOnce = false;
    boolean isSP = true;
    private static final int RECORD_REQUEST_CODE = 101;
    int rollId;
    private SharedPreferences pref;

    private int position = 0;
    private List<ImageView> imageViews = new ArrayList<>();
    private List<TextView> textViews = new ArrayList<>();
    String userId, userAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_navigation_client_main);
        prepareLayout();
        pref = getSharedPreferences("colors", 0);
        userId = String.valueOf(session.getAuthorizedUser(this).getId());
        userAccessToken = session.getAuthorizedUser(this).getUserAccessToken();
        makeRequest();
        Log.e("notification", "notification register");
        LocalBroadcastManager.getInstance(this.getApplicationContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("myFunction"));
        setTintOfBottomImage();
        initLiveClientSocket();
    }

    private void initLiveClientSocket() {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        String channelName = String.valueOf(session.getAuthorizedUser(this).getId());

        pubnub.addListener(new SubscribeCallback() {
                               @Override
                               public void status(PubNub pubnub, PNStatus status) {

                               }

                               @Override
                               public void message(PubNub pubnub, PNMessageResult message) {
                                   if (message.getMessage() != null) {
                                       /*setDataToLiveClient(message.getMessage());
                                       setAdapter();*/
                                       Log.e("message: ", message.getMessage().getAsString());
                                       if (message.getMessage().getAsString().equalsIgnoreCase("accepted")) {
                                           runOnUiThread(() -> callApi());
                                       }
                                   }

                               }

                               @Override
                               public void presence(PubNub pubnub, PNPresenceEventResult presence) {

                               }
                           }
        );
        pubnub.subscribe()
                .channels(Arrays.asList(channelName)) // subscribe to channels
                .execute();
        /*long currentMillis = System.currentTimeMillis();
        Log.e("currentMillis: ", "" + currentMillis);
        long fourHoursAgo = 4 * 60 * 60 * 1000L;
        Log.e("fourHoursAgo: ", "" + fourHoursAgo);
        long l = currentMillis - fourHoursAgo;
        Log.e("fourHoursLong: ", "" + l);
        pubnub.history().channel(channelName).async(new PNCallback<PNHistoryResult>() {
            @Override
            public void onResponse(PNHistoryResult result, PNStatus status) {
                if (!status.isError()) {
                    for (PNHistoryItemResult pnHistoryItemResult : result.getMessages()) {
                        // custom JSON structure for message
                        setDataToLiveClient(pnHistoryItemResult.getEntry());
                    }
                    setAdapter();
                }
            }
        });*/
    }

    public static Activity getActivity() {
        Class activityThreadClass = null;
        try {
            activityThreadClass = Class.forName("android.app.ActivityThread");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Object activityThread = null;
        try {
            activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Field activitiesField = null;
        try {
            activitiesField = activityThreadClass.getDeclaredField("mActivities");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        activitiesField.setAccessible(true);

        Map<Object, Object> activities = null;
        try {
            activities = (Map<Object, Object>) activitiesField.get(activityThread);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if (activities == null)
            return null;

        for (Object activityRecord : activities.values()) {
            Class activityRecordClass = activityRecord.getClass();
            Field pausedField = null;
            try {
                pausedField = activityRecordClass.getDeclaredField("paused");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            pausedField.setAccessible(true);
            try {
                if (!pausedField.getBoolean(activityRecord)) {
                    Field activityField = activityRecordClass.getDeclaredField("activity");
                    activityField.setAccessible(true);
                    Activity activity = (Activity) activityField.get(activityRecord);
                    return activity;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("notification", "notification received in class");
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(NavigationClientMainActivity.this);
            builder.setMessage("Trainer accepted request").setCancelable(
                    false).setPositiveButton("Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.e("notification", "apiCalling");
                            callApi();
                        }
                    });

            android.app.AlertDialog alert = builder.create();
            alert.show();
        }
    };

    private void callApi() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        Log.e("notification", "apiCalled");
        ObserverUtil.subscribeToSingle(ApiClient.getClient(NavigationClientMainActivity.this, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getRelogin(userId)
                , getCompositeDisposable(), single, this);
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE);
    }


    private void setTintOfBottomImage() {
        mBinding.relativeNavigation.setBackground(Utils.getShapeGradient(this, 0));
        Utils.setFabBackgroundWithDrawable(this, mBinding.uiNavigation.fab, R.drawable.home_icon);
        imageViews.add(mBinding.uiNavigation.imgProfile);
        imageViews.add(mBinding.uiNavigation.imgChat);
        tintButton(mBinding.uiNavigation.imgProfile);
        tintButton(mBinding.uiNavigation.imgChat);

        textViews.add(mBinding.uiNavigation.txtProfile);
        textViews.add(mBinding.uiNavigation.txtChat);
        mBinding.uiNavigation.txtProfile.setTextColor(makeSelector());
        mBinding.uiNavigation.txtChat.setTextColor(makeSelector());

        mBinding.uiNavigation.imgProfile.setOnClickListener(this);
        mBinding.uiNavigation.txtProfile.setOnClickListener(this);
        mBinding.uiNavigation.txtChat.setOnClickListener(this);
        mBinding.uiNavigation.imgChat.setOnClickListener(this);
    }

    public void tintButton(ImageView button) {
        ColorStateList colours = makeSelector();
        Drawable d = DrawableCompat.wrap(button.getDrawable());
        DrawableCompat.setTintList(d, colours);
        button.setImageDrawable(d);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mBinding.drawer.isDrawerOpen(GravityCompat.START);
        if (drawerOpen) {
            mBinding.txtUserName.setText(session.getAuthorizedUser(this).getName().toString());
            if (session.getAuthorizedUser(this).getPhoto() != null) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_user)
                        .error(R.drawable.ic_user);
                Glide.with(getApplicationContext())
                        .load(session.getAuthorizedUser(this).getPhoto())
                        .apply(options)
                        .into(mBinding.imgLogin);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }


    public ColorStateList makeSelector() {
        int[][] states = new int[][]{
                new int[]{android.R.attr.state_selected}, // selected
                new int[]{}};

        int[] colors = new int[]{
                ContextCompat.getColor(this, R.color.colorPrimary),
                Color.parseColor("#8a8a8a"),
        };
        ColorStateList myList = new ColorStateList(states, colors);
        return myList;
    }

    private void setCheckedImage(int id) {
        for (int i = 0; i < imageViews.size(); i++)
            if (id != 0 && imageViews.get(i).getId() == id) {
                imageViews.get(i).setSelected(true);
                textViews.get(i).setSelected(true);
            } else {
                imageViews.get(i).setSelected(false);
                textViews.get(i).setSelected(false);
            }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mBinding.txtUserName.setText(session.getAuthorizedUser(this).getName().toString());
        if (session.getAuthorizedUser(this).getPhoto() != null) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder);
            Glide.with(this)
                    .load(session.getAuthorizedUser(this).getPhoto())
                    .apply(options)
                    .into(mBinding.imgLogin);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this.getApplicationContext()).unregisterReceiver(mMessageReceiver);
    }

    private void prepareLayout() {
        isSP = session.getBooleanDataByKey(KEY_IS_SP);
        setTitle("");

        mStacks = new HashMap<>();
        mStacks.put(AppConstants.NAVIGATION_KEY, new Stack<>());
       /* if (rollId==1) {
            pushFragments(AppConstants.NAVIGATION_KEY, new HomeClientFragment(), true, true);
        } else {*/
        if (session.getAuthorizedUser(this).isClient_trainer() || session.getAuthorizedUser(this).isDirect_gym_client()) {
            pushFragments(AppConstants.NAVIGATION_KEY, new HomeClientSubscribeFragment(), true, true);
        } else {
            pushFragments(AppConstants.NAVIGATION_KEY, new HomeClientFragment(), true, true);
        }
        //}
        setData();
        mBinding.txtLogout.setOnClickListener(view -> {
            if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
                mBinding.drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
            setLogout();
        });
        mBinding.imgLogin.setOnClickListener(view -> {
            pushFragments(AppConstants.NAVIGATION_KEY, ProfileFragment.newInstance(), true, true);
            position = 2;
            drawerItemCustomAdapter.setPosition(2);
            if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
                mBinding.drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mBinding.drawer.openDrawer(GravityCompat.START);
                return true;

            case R.id.menu_notification:
                startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pushFragments(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.fragment_replace, fragment);
        ft.commit();
    }

    public void popFragments() {

        Fragment fragment = mStacks.get(AppConstants.NAVIGATION_KEY).elementAt(mStacks.get(AppConstants.NAVIGATION_KEY).size() - 2);
        /*pop current fragment from stack.. */
        mStacks.get(AppConstants.NAVIGATION_KEY).pop();

        /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }


    private void setData() {

        if (session.getAuthorizedUser(this).isClient_trainer() || session.getAuthorizedUser(this).isDirect_gym_client()) {
            objectDrawerItems = getObjectDrawerItemWithTrainer();
        } else {
            objectDrawerItems = getObjectDrawerItemWithoutTrainer();
        }
       /* if (session.getAuthorizedUser(this).getPhoto() != null)
        { Glide.with(this)
                .load(session.getAuthorizedUser(this).getPhoto())
                .into(mBinding.imgLogin);}*/

//        mBinding.txtLogin.setText(session.getAuthorizedUser(this).getUserName());
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mBinding.drawerList.setLayoutManager(lm);
        mBinding.drawerList.setNestedScrollingEnabled(false);
        drawerItemCustomAdapter = new DrawerItemAdapter(this, objectDrawerItems, this, position);
        mBinding.drawerList.setAdapter(drawerItemCustomAdapter);

        /*if (getIntent().hasExtra("order") && getIntent().getBooleanExtra("order", false) && !isSP) {
            pushFragments(AppConstants.NAVIGATION_KEY, FragmentBookingHistory.newInstance(), true, true);
            drawerItemCustomAdapter.setPosition(1);
        }
*/
    }

    private ArrayList<DrawerItem> getObjectDrawerItemWithoutTrainer() {
        ArrayList<DrawerItem> objectDrawerItems = new ArrayList<>();
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_find_trainer, R.drawable.ic_find_trainer, getString(R.string.search)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_workout_nav, R.drawable.ic_workout_nav, getString(R.string.workouts)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_apple_nav, R.drawable.ic_apple_nav, getString(R.string.nutrition)));
        //objectDrawerItems.add(new DrawerItem(R.drawable.ic_home, R.drawable.ic_home, getString(R.string.home)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_chat_line_nav, R.drawable.ic_chat_line_nav, getString(R.string.chat)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_settings_nav, R.drawable.ic_settings_nav, getString(R.string.settings)));
        return objectDrawerItems;
    }

    private ArrayList<DrawerItem> getObjectDrawerItemWithTrainer() {
        ArrayList<DrawerItem> objectDrawerItems = new ArrayList<>();
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_home, R.drawable.ic_home, getString(R.string.home)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_workout_nav, R.drawable.ic_workout_nav, getString(R.string.workouts)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_apple_nav, R.drawable.ic_apple_nav, getString(R.string.nutrition)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_training_program_nav, R.drawable.ic_training_program_nav, getString(R.string.training_program)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_reports, R.drawable.ic_reports, getString(R.string.reports)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_checkin, R.drawable.ic_checkin, getString(R.string.check_in)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_shop, R.drawable.ic_shop, "Shop"));
        if (session.getAuthorizedUser(this).isDirect_gym_client()) {
            objectDrawerItems.add(new DrawerItem(R.drawable.ic_gym_drawer, R.drawable.ic_gym_drawer, "Gym Profile"));
            if (session.getAuthorizedUser(this).isClient_trainer()) {
                objectDrawerItems.add(new DrawerItem(R.drawable.ic_user, R.drawable.ic_user, "Trainer Profile"));
            }
        } else {
            objectDrawerItems.add(new DrawerItem(R.drawable.ic_user, R.drawable.ic_user, "Trainer Profile"));
        }
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_chat_line_nav, R.drawable.ic_chat_line_nav, getString(R.string.chat)));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_find_trainer, R.drawable.ic_find_trainer, "Find a Trainer"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_client_enqriry, R.drawable.ic_client_enqriry, "Trainer/Gym Inquires"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_nav_purchase, R.drawable.ic_nav_purchase, "Purchase"));
        objectDrawerItems.add(new DrawerItem(R.drawable.ic_settings_nav, R.drawable.ic_settings_nav, getString(R.string.settings)));
        return objectDrawerItems;
    }

    @Override
    public void onItemClicked(int position, String name) {
        setCheckedImage(0);
        if (this.position != position) {
            switch (name) {
                case "Find a Trainer":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, HomeClientFragment.newInstance(), true, true);
                    break;
                case "Search":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, HomeClientFragment.newInstance(), true, true);
                    break;
                case "Check In":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, CheckInsDataFragment.newInstance(), true, true);
                    break;
                case "Home":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, HomeClientSubscribeFragment.newInstance(), true, true);
                    break;
                case "Workouts":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, ClientWorkoutFragment.newInstance(), true, true);
                    break;
                case "Training Programs":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new FragmentTrainingProgramClient(), true, true);
                    break;
                case "Profile":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new ProfileFragment(), true, true);
                    break;
                case "Settings":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new SettingFragment(), true, true);
                    break;

                case "Reports":
                    this.position = position;
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFromNav", true);
                    ReportCLientOptionFragment fragment = new ReportCLientOptionFragment();
                    fragment.setArguments(bundle);
                    pushFragments(AppConstants.NAVIGATION_KEY, fragment, true, true);
                    break;
                case "Chat":
                    Intent intent = new Intent(getApplicationContext(), ConversationActivity.class);
                    startActivity(intent);
                    break;
                case "Shop":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new ClientShopFragment(), true, true);
                    break;
                case "Nutrition":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new FragmentClientNutrition(), true, true);
                    break;
                case "Gym Profile":
                    this.position = position;
                    FragmentTrainerProfile fragmentGymProfile = new FragmentTrainerProfile();
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("isGym", 1);
                    bundle1.putBoolean("isFromNav", true);
                    bundle1.putInt("trainerId", Integer.parseInt(session.getAuthorizedUser(this).getDirect_gym_id()));
                    fragmentGymProfile.setArguments(bundle1);
                    pushFragments(AppConstants.NAVIGATION_KEY, fragmentGymProfile, true, true);
                    break;
                case "Trainer Profile":
                    this.position = position;
                    FragmentTrainerProfile fragmentTrainerProfile = new FragmentTrainerProfile();
                    Bundle bundleTrainer = new Bundle();
                    bundleTrainer.putInt("isGym", 0);
                    bundleTrainer.putBoolean("isFromNav", true);
                    bundleTrainer.putInt("trainerId", Integer.parseInt(session.getAuthorizedUser(this).getTrainer_id()));
                    fragmentTrainerProfile.setArguments(bundleTrainer);
                    pushFragments(AppConstants.NAVIGATION_KEY, fragmentTrainerProfile, true, true);
                    break;
                case "Trainer/Gym Inquires":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new TrainerInquiriesFragment(), true, true);
                    break;
                case "Purchase":
                    this.position = position;
                    pushFragments(AppConstants.NAVIGATION_KEY, new FragmentPurchaseHistoryClient(), true, true);
                    break;
            }
        }

        drawerItemCustomAdapter.setPosition(this.position);

        if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void setLogout() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Logout Alert")
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    callLogoutAPI();
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorAccent));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mStacks.get(AppConstants.NAVIGATION_KEY).size() == 0) {
            return;
        }
        /*Now current fragment on screen gets onActivityResult callback..*/
        mStacks.get(AppConstants.NAVIGATION_KEY).lastElement().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (mBinding.drawer.isDrawerOpen(GravityCompat.START)) {
            mBinding.drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

            /* if (!((BaseFragment) mStacks.get(AppConstants.NAVIGATION_KEY).lastElement()).onBackPressed()) {

             *//*
             * top fragment in current tab doesn't handles back press, we can do our thing, which is
             *
             * if current tab has only one fragment in stack, ie first fragment is showing for this tab.
             *        finish the activity
             * else
             *        pop to previous fragment in stack for the same tab
             *
             *//*
                if (mStacks.get(AppConstants.NAVIGATION_KEY).size() == 1) {
                    //If current tab is not Home then set Home as current tab
                    finish();
                } else {
                    if (mStacks.get(AppConstants.NAVIGATION_KEY).lastElement() instanceof FragmentDashboard)
                        finish();
                    else
                        popFragments();
                }
            } else {
                //do nothing.. fragment already handled back button press.
                Logger.e("BackPress Manage By Fragment");
            }*/
            /*if (!((BaseFragment) mStacks.get(AppConstants.NAVIGATION_KEY).lastElement()).onBackPressed()) {
             *//*
             * top fragment in current tab doesn't handles back press, we can do our thing, which is
             *
             * if current tab has only one fragment in stack, ie first fragment is showing for this tab.
             *        finish the activity
             * else
             *        pop to previous fragment in stack for the same tab
             *
             *//*
                if (mStacks.get(AppConstants.NAVIGATION_KEY).size() == 1) {
                    //If current tab is not Home then set Home as current tab
                    if (!AppConstants.NAVIGATION_KEY.equals(AppConstants.TAB_BLENDER)) {
                        //setCurrentTab(0);
                    } else {
                        try {
                            if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                                if (doubleBackToExitPressedOnce) {
                                    finish();
                                    return;
                                }
                                this.doubleBackToExitPressedOnce = true;
                                Toast.makeText(this, getString(R.string.prompt_exit), Toast.LENGTH_SHORT).show();
                                new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
                            } else {
                                getSupportFragmentManager().popBackStack();
                            }
                        } catch (Exception e) {
                            super.onBackPressed();
                        }
                    }
                } else {
                    popFragments();
                }
            } else {
                //do nothing.. fragment already handled back button press.
                Logger.e("BackPress Manage By Fragment");
            }*/
        }

    }

    @Override
    public void onClick(View view) {
        position = -1;
        drawerItemCustomAdapter.setPosition(this.position);
        setCheckedImage(view.getId());
        Intent intentChat = new Intent(getApplicationContext(), ConversationActivity.class);
        switch (view.getId()) {
            case R.id.imgProfile:
                pushFragments(AppConstants.NAVIGATION_KEY, new ProfileFragment(), true, true);
                break;
            case R.id.imgChat:
                startActivity(intentChat);
                break;
            case R.id.txtProfile:
                pushFragments(AppConstants.NAVIGATION_KEY, new ProfileFragment(), true, true);
                break;
            case R.id.txtChat:
                startActivity(intentChat);
                break;
        }
    }

    private void callLogoutAPI() {
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).logout()
                , getCompositeDisposable(), logout, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case logout:
                disableScreen(false);
                CommonApiResponse model = (CommonApiResponse) o;
                if (model.getStatus() == AppConstants.SUCCESS) {
                    session.setLogout();
                    pref.edit().clear().commit();
                    AccessToken accessToken = AccessToken.getCurrentAccessToken();
                    boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
                    if (isLoggedIn)
                        LoginManager.getInstance().logOut();
                    startActivity(new Intent(NavigationClientMainActivity.this, SplashActivity.class));
                    finish();
                } else {
                    Toast.makeText(this, model.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;

            case single:
                Log.e("notification", "success");
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                VerificationOTPModel verificationOTPModel = (VerificationOTPModel) o;
                if (verificationOTPModel != null && verificationOTPModel.getStatus() == AppConstants.SUCCESS) {
                    // session.setLogin(true);
                    session.setRollId(Integer.parseInt(verificationOTPModel.getData().getRoleId()));
                    session.saveAuthorizedUser(NavigationClientMainActivity.this, verificationOTPModel.getData());
                    Intent intent = new Intent(NavigationClientMainActivity.this, NavigationClientMainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    NavigationClientMainActivity.this.startActivity(intent);
                    NavigationClientMainActivity.this.finish();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
