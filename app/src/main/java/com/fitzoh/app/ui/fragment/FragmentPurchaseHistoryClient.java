package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.PurchaseHistoryClientAdapter;
import com.fitzoh.app.databinding.FragmentPurchaseHistoryClientBinding;
import com.fitzoh.app.model.PurchaseHistoryModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getPackageList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPurchaseHistoryClient extends BaseFragment implements SingleCallback {

    FragmentPurchaseHistoryClientBinding mBinding;
    String userId, userAccessToken;

    public FragmentPurchaseHistoryClient() {
        // Required empty public constructor
    }

    public static FragmentPurchaseHistoryClient newInstance() {
        FragmentPurchaseHistoryClient fragment = new FragmentPurchaseHistoryClient();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Purchase");
    }

    @Override
    public void onResume() {
        super.onResume();
        callPackageList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchase_history_client, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.cardLayout.setBackground(Utils.getShapeGradient(mActivity, 0));
        return mBinding.getRoot();
    }

    private void callPackageList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchasePlanHistory()
                    , getCompositeDisposable(), getPackageList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case getPackageList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                PurchaseHistoryModel model = (PurchaseHistoryModel) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null) {
                    setCurrentPlanDetail(model.getData());
                } else {
                    showSnackBar(mBinding.mainLayout, model.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
        }
    }

    private void setCurrentPlanDetail(PurchaseHistoryModel.DataBean model) {
        if (model.getCurrentPlan() != null) {
            PurchaseHistoryModel.DataBean.CurrentPlanBean planBean = model.getCurrentPlan();
            mBinding.packageName.setText(planBean.getPlan_name());
            mBinding.packageDescription.setText(planBean.getLimitation());
            mBinding.packageDate.setText(planBean.getStart_date());
            mBinding.packageRemainingDay.setText("Remaining Day : " + planBean.getRemaining_day());
            mBinding.packagePrice.setText("Price: " + planBean.getPrice());
        }
        if (model.getOwnPlan() != null) {
            List<PurchaseHistoryModel.DataBean.OwnPlanBean> ownPlan = model.getOwnPlan();
            if (ownPlan != null && ownPlan.size() > 0) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.imgNoData.setVisibility(View.GONE);
                mBinding.recyclerView.setAdapter(new PurchaseHistoryClientAdapter(ownPlan, mActivity));
            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
            }
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

}
