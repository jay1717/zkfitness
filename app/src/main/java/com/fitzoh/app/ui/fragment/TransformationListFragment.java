package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TransformationListAdapter;
import com.fitzoh.app.interfaces.OnRecyclerViewClickListener;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TransformationListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.databinding.FragmentTransformationListBinding;

import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.TransformationAddActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TransformationListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransformationListFragment extends BaseFragment implements SingleCallback, OnRecyclerViewClickListener, TransformationListAdapter.DeleteTransformation, SwipeRefreshLayout.OnRefreshListener {
    FragmentTransformationListBinding mBinding;
    List<TransformationListModel.DataBean> transformationList;
    int trainerId = 0;
    String userId, userAccessToken;
    boolean isFromNav = false;
    TransformationListAdapter transformationListAdapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TransformationListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TransformationListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TransformationListFragment newInstance() {
        TransformationListFragment fragment = new TransformationListFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transformation_list, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Transformation List");
        mBinding.layoutPackage.fab.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), TransformationAddActivity.class);
            intent.putExtra("IS_FROM_REGISTER", false);
            startActivity(intent);
        });
        mBinding.layoutPackage.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutPackage.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        getTransformationData();
        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getTransformationData();

    }

    private void getTransformationData() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTransformationDetails(session.getAuthorizedUser(mContext).getId())
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTransformation, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getTransformation:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                transformationList = new ArrayList<>();
                TransformationListModel transformationListModel = (TransformationListModel) o;
                if (transformationListModel.getStatus() == AppConstants.SUCCESS) {

                    if (transformationListModel != null) {
                        transformationList.addAll(transformationListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (transformationList.size() == 0) {
                            mBinding.layoutPackage.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutPackage.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutPackage.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutPackage.imgNoData.setVisibility(View.GONE);
                            transformationListAdapter = new TransformationListAdapter(getActivity(), transformationList, this, this);
                            mBinding.layoutPackage.recyclerView.setAdapter(transformationListAdapter);
                            /*ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
                            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mBinding.layoutPackage.recyclerView);*/

                        }

                    } else
                        showSnackBar(mBinding.linear, transformationListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

            case deleteTransformation:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                transformationList = new ArrayList<>();
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    //  transformationListAdapter.removeItem(viewHolder.getAdapterPosition());
                    getTransformationData();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }


                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

   /* @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof transformationListAdapter.DataViewHolder) {
            // get the removed item name to display it in snack bar
            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteTransformations(transformationList.get(viewHolder.getAdapterPosition()).getId())
                        , getCompositeDisposable(), WebserviceBuilder.ApiNames.deleteTransformation, this);
            } else {
                showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }

        }
    }*/

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getActivity(), TransformationAddActivity.class);
        intent.putExtra("IS_FROM_REGISTER", false);
        intent.putExtra("TRANSFORMATION_DATA", transformationList.get(position));
        startActivity(intent);
    }

    @Override
    public void delete(TransformationListModel.DataBean dietListModel) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteTransformations(dietListModel.getId())
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.deleteTransformation, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void deleteTransformationDialog(TransformationListModel.DataBean dietListModel) {
            AlertDialog alertDialog = new AlertDialog.Builder(mActivity)
                   // .setTitle("Are you sure you want to delte transformation")
                    .setMessage("Are you sure you want to delete this Training program?")
                    .setPositiveButton(R.string.delete, (dialog, whichButton) -> {
                        dialog.dismiss();

                    })
                    .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        // startActivity(new Intent(mActivity, NavigationMainActivity.class));
                    })
                    .show();
    }

    @Override
    public void onRefresh() {
        getTransformationData();
        mBinding.layoutPackage.swipeContainer.setRefreshing(false);
    }
}
