package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientNutritionListAdapter;
import com.fitzoh.app.databinding.FragmentClientNutritionBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditNutritionActivity;
import com.fitzoh.app.ui.activity.TrainingProgramDietActivity;
import com.fitzoh.app.ui.dialog.AddDietDialog;
import com.fitzoh.app.ui.dialog.AddWorkoutDialog;
import com.fitzoh.app.ui.dialog.RenameDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.DietList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteDiet;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentClientNutrition#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentClientNutrition extends BaseFragment implements SingleCallback, ClientNutritionListAdapter.UnAssign, AddDietDialog.DialogClickListener, SwipeRefreshLayout.OnRefreshListener, RenameDialog.DialogClickListener
{
    FragmentClientNutritionBinding mBinding;


    String userId, userAccessToken;
    ClientNutritionListAdapter nutritionListAdapter;

    public FragmentClientNutrition() {
    }

    public static FragmentClientNutrition newInstance() {
        FragmentClientNutrition fragment = new FragmentClientNutrition();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.nutrition_plans));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_nutrition, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        nutritionListAdapter = new ClientNutritionListAdapter(getActivity(), new ArrayList<>(), this);
        mBinding.recyclerView.setAdapter(nutritionListAdapter);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.fab.setOnClickListener(view -> {
            AddDietDialog dialog = new AddDietDialog();
            dialog.setListener(this);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isOnline(mActivity))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    private void callNutritionApi() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientDietList()
                , getCompositeDisposable(), DietList, this);
    }

    @Override
    public void edit(DietListModel.DataBean dietListModel) {
        //        ((BaseActivity) mActivity).session.editor.remove(CLIENT_ID).commit();
        session.setStringDataByKey(CLIENT_ID, "" + userId);
        if (dietListModel.getIs_assigned() == 1)
            startActivity(new Intent(mActivity, TrainingProgramDietActivity.class).putExtra("dietId", dietListModel.getId()).putExtra("dietName", dietListModel.getName()));
        else
            startActivity(new Intent(getActivity(), EditNutritionActivity.class).putExtra("dietId", dietListModel.getId()).putExtra("dietName", dietListModel.getName()));
    }

    @Override
    public void rename(DietListModel.DataBean dataBean) {
        RenameDialog dialogNote = new RenameDialog(dataBean.getId(), 0, 0, dataBean.getName());
        dialogNote.setListener(this);
        dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), RenameDialog.class.getSimpleName());
        dialogNote.setCancelable(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (Utils.isOnline(mContext))
                callNutritionApi();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case DietList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DietListModel dietListModel = (DietListModel) o;
                if (dietListModel != null && dietListModel.getStatus() == AppConstants.SUCCESS && dietListModel.getData() != null && dietListModel.getData().size() > 0) {
                    mBinding.imgNoData.setVisibility(View.GONE);
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setAdapter(new ClientNutritionListAdapter(getActivity(), dietListModel.getData(), this));
                } else {
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setVisibility(View.GONE);
                }
                break;
            case deleteDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deleteDiet = (CommonApiResponse) o;
                if (deleteDiet.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext()))
                        callNutritionApi();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteDiet.getMessage(), Snackbar.LENGTH_LONG);

                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
        mBinding.imgNoData.setVisibility(View.VISIBLE);
        mBinding.recyclerView.setVisibility(View.GONE);
    }


    @Override
    public void remove(DietListModel.DataBean dataBean) {
        if (dataBean != null) {
            if (Utils.isOnline(getActivity())) {
                callNutritionDelete(dataBean.getId());
            } else {
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        }
    }

    private void callNutritionDelete(int dietId) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteDietClient(dietId,userId)
                , getCompositeDisposable(), deleteDiet, this);
    }

    @Override
    public void setClick() {
        if (Utils.isOnline(mActivity))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(mActivity))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }

        mBinding.swipeContainer.setRefreshing(false);
    }
}
