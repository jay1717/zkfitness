package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewPagerAdapter;
import com.fitzoh.app.databinding.FragmentAddExerciseBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAddExercise extends BaseFragment {

    FragmentAddExerciseBinding mBinding;
    ViewPagerAdapter adapter;
    private WorkOutListModel.DataBean dataBean;

    public FragmentAddExercise() {
        // Required empty public constructor
    }

    public static FragmentAddExercise newInstance(Bundle bundle) {
        FragmentAddExercise fragment = new FragmentAddExercise();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (WorkOutListModel.DataBean) getArguments().get("dataBean");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.add_exercise));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_add_exercise, container, false);
        setupViewPager(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.add_exercise));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.toolbar.toolbar.setElevation(0f);
        }
        /*mBinding.tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        mBinding.tabs.setTabTextColors(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray));*/

        return mBinding.getRoot();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getFragmentManager());
        viewPager.addOnPageChangeListener(pageChangeListener);

        adapter.addFragment(FragmentSearchExercise.newInstance(1, dataBean), "Search  Exercise");
        adapter.addFragment(FragmentSearchExercise.newInstance(2, dataBean), "Muscle");
        adapter.addFragment(FragmentSearchExercise.newInstance(3, dataBean), "Recently Used");
        adapter.addFragment(FragmentSearchExercise.newInstance(4, dataBean), "Frequently Used");
        viewPager.setAdapter(adapter);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {

            FragmentLifeCycle fragmentToHide = (FragmentLifeCycle) adapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();

            FragmentLifeCycle fragmentToShow = (FragmentLifeCycle) adapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();
            currentPosition = newPosition;

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };
}
