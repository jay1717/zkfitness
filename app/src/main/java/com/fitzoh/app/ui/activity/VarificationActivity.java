package com.fitzoh.app.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ActivityVarificationBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.FragmentRegister;
import com.fitzoh.app.ui.fragment.FragmentVerificationOTP;

public class VarificationActivity extends BaseActivity {
    ActivityVarificationBinding mBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_varification);
        /*FragmentVerificationOTP fragmentVerificationOTP = new FragmentVerificationOTP();
        Bundle bundle = new Bundle();
        bundle=getIntent().getExtras();
        fragmentVerificationOTP.setArguments(bundle);*/
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.realtabcontent, FragmentVerificationOTP.newInstance(getIntent().getIntExtra("id",0),getIntent().getStringExtra("email"),getIntent().getStringExtra("isRegister")));
        ft.commit();
    }
}
