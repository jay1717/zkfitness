package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentWorkoutReportBinding;
import com.fitzoh.app.model.WorkoutReportModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class StrengthReportFragment extends BaseFragment implements SingleCallback {


    FragmentWorkoutReportBinding mBinding;
    private Integer clientId = null;
    //  private ClientListModel.DataBean data = null;
    String userId, userAccessToken;
    HashMap<Integer, String> numMapStrength;
    Typeface typeface;
    ArrayList<Entry> valuesStrength = new ArrayList<Entry>();
    String type;

    public StrengthReportFragment() {
        // Required empty public constructor
    }


    public static StrengthReportFragment newInstance() {
        StrengthReportFragment fragment = new StrengthReportFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.strength_report));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            //  data = (ClientListModel.DataBean) getArguments().getSerializable("data");
            clientId = getArguments().getInt("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_workout_report, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        typeface = ResourcesCompat.getFont(mContext, R.font.avenirnextltpro_medium);
        type = "weekly";
//        mBinding.toolbar.imgBack.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back));
//        mBinding.toolbar.imgBack.setOnClickListener(view -> getActivity().finish());
        //  mBinding.toolbar.imgAction.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_filter));
//        mBinding.toolbar.tvTitle.setText(R.string.strength_report);
        setHasOptionsMenu(true);
        callChartDataStrength();
        return mBinding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_reports, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_daily:
                type = "daily";
                callChartDataStrength();
                break;
            case R.id.action_weekly:
                type = "weekly";
                callChartDataStrength();
                break;
            case R.id.action_monthly:
                type = "monthly";
                callChartDataStrength();
                break;
            case R.id.action_training:
                type = "training program";
                callChartDataStrength();
                break;
        }
        return true;
    }

    private void callChartDataStrength() {
        if (com.fitzoh.app.utils.Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getStrengthReportData(clientId, type)
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setAllValuesStrength(List<WorkoutReportModel.DataBean> data) {
        numMapStrength = new HashMap<>();
        valuesStrength = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            if (data.size() == 1) {
                numMapStrength.put(i, "");
                numMapStrength.put(i + 1, data.get(i).getKey());
                valuesStrength.add(new Entry(i, 0, null));
                valuesStrength.add(new Entry(i + 1, data.get(i).getValue(), null));
            } else {
                numMapStrength.put(i, data.get(i).getKey());
                valuesStrength.add(new Entry(i, data.get(i).getValue(), null));
            }
        }
        setLineChartStrength();
    }

    private void setLineChartStrength() {
        mBinding.chartWorkout.setDrawGridBackground(false);
        mBinding.chartWorkout.setDrawBorders(false);
        mBinding.chartWorkout.getLegend().setEnabled(false);
        mBinding.chartWorkout.setAutoScaleMinMaxEnabled(false);
        mBinding.chartWorkout.setTouchEnabled(true);
        mBinding.chartWorkout.setDragEnabled(false);
        mBinding.chartWorkout.setScaleEnabled(false);
        mBinding.chartWorkout.setPinchZoom(false);
        mBinding.chartWorkout.setDoubleTapToZoomEnabled(false);
        mBinding.chartWorkout.getAxisRight().setEnabled(false);
        mBinding.chartWorkout.getDescription().setEnabled(false);

        final YAxis yAxis = mBinding.chartWorkout.getAxisLeft();
//        yAxis.setLabelCount(6, true);
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setValueFormatter(new DefaultAxisValueFormatter(1));
        yAxis.setTextColor(Color.GRAY);
        yAxis.setTextSize(5f); //not in your original but added
        yAxis.setTypeface(typeface);
        yAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));
        yAxis.setAxisLineColor(Color.TRANSPARENT);
        yAxis.setAxisMinimum(0); //not in your original but added

        final XAxis xAxis = mBinding.chartWorkout.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); //changed to match your spec
        xAxis.setTextColor(Color.GRAY);
        xAxis.disableGridDashedLine();
        xAxis.setDrawGridLines(true);
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));

        xAxis.setAxisLineColor(Color.TRANSPARENT);
        xAxis.setValueFormatter((value, axis) -> numMapStrength.get((int) value));
        xAxis.setLabelCount(valuesStrength.size());
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setLabelRotationAngle(-45);
        xAxis.setGranularityEnabled(true);
        setDataStrength();
    }

    private void setDataStrength() {
        LineDataSet set1;
        if (mBinding.chartWorkout.getData() != null &&
                mBinding.chartWorkout.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.chartWorkout.getData().getDataSetByIndex(0);
            set1.setValues(valuesStrength);
            mBinding.chartWorkout.getData().notifyDataChanged();
            mBinding.chartWorkout.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(valuesStrength, "DataSet 1");
//            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(0);
            set1.setDrawCircles(false);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            set1.setValueTextSize(5f);
            set1.setValueTypeface(typeface);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
//                Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.shape_gradient_chart);
                GradientDrawable gradientDrawable = com.fitzoh.app.utils.Utils.getShapeGradient(mActivity, 0);
                set1.setFillDrawable(gradientDrawable);
            } else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            mBinding.chartWorkout.setExtraBottomOffset(30);
            // set data
            mBinding.chartWorkout.setData(data);
            mBinding.chartWorkout.invalidate();
        }
    }

    public class CustomXAxisRenderer extends XAxisRenderer {
        public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
            super(viewPortHandler, xAxis, trans);
        }

        @Override
        protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
            Utils.drawXAxisValue(c, formattedLabel, x, y, mAxisLabelPaint, anchor, angleDegrees);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {

            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutReportModel strengthReportModel = (WorkoutReportModel) o;
                mBinding.chartWorkout.clear();
                if (strengthReportModel != null && strengthReportModel.getData() != null && strengthReportModel.getData().size() > 0) {
                    setAllValuesStrength(strengthReportModel.getData());
                }
                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }


}
