package com.fitzoh.app.ui.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainerShopAdapter;
import com.fitzoh.app.databinding.FragmentTrainerShopBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainerShopListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddTrainerProductActivity;
import com.fitzoh.app.ui.activity.EditTrainerProductActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainerShopFragment extends BaseFragment implements SingleCallback, TrainerShopAdapter.onAction {

    FragmentTrainerShopBinding mBinding;
    TrainerShopAdapter trainerShopAdapter;
    String userId, userAccessToken;
    List<TrainerShopListModel.DataBean> trainerListModel;

    public TrainerShopFragment() {
        // Required empty public constructor
    }

    public static TrainerShopFragment newInstance() {
        TrainerShopFragment fragment = new TrainerShopFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Shop");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trainer_shop, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setTextViewStartImage(mActivity, mBinding.edtSearch, R.drawable.ic_search, true);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        if (session.getAuthorizedUser(mActivity).isGym_trainer())
            mBinding.fab.hide();
        else
            mBinding.fab.show();
        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddTrainerProductActivity.class));
            }
        });
        trainerListModel = new ArrayList<>();
        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        trainerShopAdapter = new TrainerShopAdapter(getActivity(), trainerListModel, this);
        mBinding.recyclerView.setAdapter(trainerShopAdapter);


        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (trainerShopAdapter != null)
                    trainerShopAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callShoppingData();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callShoppingData() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getShoppingData()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainerListModel = new ArrayList<>();
                TrainerShopListModel trainerShopListModel = (TrainerShopListModel) o;
                if (trainerShopListModel.getStatus() == AppConstants.SUCCESS) {

                    if (trainerShopListModel != null) {
                        trainerListModel.addAll(trainerShopListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (trainerListModel.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            trainerShopAdapter = new TrainerShopAdapter(getActivity(), trainerListModel, this);
                            mBinding.recyclerView.setAdapter(trainerShopAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, trainerShopListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

            case deleteShoppingData:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext()))
                        callShoppingData();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);

                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void delete(TrainerShopListModel.DataBean dataBean) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                .setTitle("Delete Alert")
                .setMessage("Are you sure, you want to delete this product?")
                .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    if (Utils.isOnline(getActivity())) {
                        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteShoppingData(dataBean.getId())
                                , getCompositeDisposable(), WebserviceBuilder.ApiNames.deleteShoppingData, this);
                    } else {
                        showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
                    }

                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));

    }

    @Override
    public void edit(TrainerShopListModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), EditTrainerProductActivity.class).putExtra("data", dataBean));
    }

}
