package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientUpdateAdapter;
import com.fitzoh.app.adapter.LiveClientAdapter;
import com.fitzoh.app.adapter.ScheduleSummaryAdapter;
import com.fitzoh.app.databinding.FragmentDashboardGymBinding;
import com.fitzoh.app.model.ClientUpdateListData;
import com.fitzoh.app.model.LiveClientsModel;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.gson.JsonElement;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.history.PNHistoryResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.clientUpdateList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.liveClients;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingSession;


public class FragmentGymDashboard extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {

    FragmentDashboardGymBinding mBinding;
    String userId, userAccessToken;
    private List<LiveClientsModel.DataBean> liveClientsModels = new ArrayList<>();
    private LiveClientAdapter liveClientAdapter;
    private HashMap<Integer, LiveClientsModel.DataBean> hashMap = new HashMap<>();
    TimerTask timer;

    public FragmentGymDashboard() {
        // Required empty public constructor
    }

    public static FragmentGymDashboard newInstance() {
        return new FragmentGymDashboard();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.home));
        initLiveClientSocket(session.getAuthorizedUser(getActivity()).getId());


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_dashboard_gym, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutHome.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutHome.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        liveClientAdapter = new LiveClientAdapter(mActivity, hashMap, false);
        mBinding.layoutHome.layoutLiveClient.recyclerView.setAdapter(liveClientAdapter);
        liveClientAdapter.notifyDataSetChanged();
        if (Utils.isOnline(mActivity)) {
            getClientUpdate();
//            getLiveClients();
            getTrainingSession();
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
        mBinding.layoutHome.layoutTrainnig.heading.setText("Trainer Schedule Summary");
        Utils.setLines(mActivity, mBinding.layoutHome.layoutLiveClient.view.view);
        Utils.setLines(mActivity, mBinding.layoutHome.layoutLiveClient.view.view2);
        Utils.setLines(mActivity, mBinding.layoutHome.layoutClientUpdate.view.view);
        Utils.setLines(mActivity, mBinding.layoutHome.layoutClientUpdate.view.view2);
        Utils.setLines(mActivity, mBinding.layoutHome.layoutTrainnig.view.view);
        Utils.setLines(mActivity, mBinding.layoutHome.layoutTrainnig.view.view2);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

//        mBinding.layoutHome.layoutClientUpdate.recyclerView.setAdapter(new ClientUpdateAdapter(getActivity()));
        return mBinding.getRoot();

    }

    private void initLiveClientSocket(int trainer_id) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        String channelName = String.valueOf(trainer_id);

        pubnub.addListener(new SubscribeCallback() {
                               @Override
                               public void status(PubNub pubnub, PNStatus status) {

                               }

                               @Override
                               public void message(PubNub pubnub, PNMessageResult message) {
                                   if (message.getMessage() != null) {
                                       setDataToLiveClient(message.getMessage());
                                       setAdapter();
                                   }

                               }

                               @Override
                               public void presence(PubNub pubnub, PNPresenceEventResult presence) {

                               }
                           }
        );
        pubnub.subscribe()
                .channels(Arrays.asList(channelName)) // subscribe to channels
                .execute();
        long currentMillis = System.currentTimeMillis();
        Log.e("currentMillis: ", "" + currentMillis);
        long fourHoursAgo = 4 * 60 * 60 * 1000L;
        Log.e("fourHoursAgo: ", "" + fourHoursAgo);
        long l = currentMillis - fourHoursAgo;
        Log.e("fourHoursLong: ", "" + l);
        pubnub.history().channel(channelName).async(new PNCallback<PNHistoryResult>() {
            @Override
            public void onResponse(PNHistoryResult result, PNStatus status) {
                if (!status.isError()) {
                    /*for (PNHistoryItemResult pnHistoryItemResult : result.getMessages()) {
                        // custom JSON structure for message
                        setDataToLiveClient(pnHistoryItemResult.getEntry());
                    }*/
                    for (int i = 0; i < result.getMessages().size(); i++) {
                        setDataToLiveClient(result.getMessages().get(i).getEntry());
                        if (i == result.getMessages().size() - 1)
                            setAdapter();
                    }
                }
            }
        });
    }

    private void setAdapter() {
        if (hashMap.size() > 0) {
            liveClientAdapter = new LiveClientAdapter(mActivity, hashMap, false);
            mActivity.runOnUiThread(() -> {
                mBinding.layoutHome.layoutLiveClient.recyclerView.setAdapter(liveClientAdapter);
                mBinding.layoutHome.layoutLiveClient.txtEmptyClients.setVisibility(View.GONE);
                mBinding.layoutHome.layoutLiveClient.recyclerView.setVisibility(View.VISIBLE);

            });

            mBinding.layoutHome.layoutLiveClient.recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    liveClientAdapter.notifyDataSetChanged();
                }
            });

        } else {
            mActivity.runOnUiThread(() -> {
                mBinding.layoutHome.layoutLiveClient.txtEmptyClients.setVisibility(View.VISIBLE);
                mBinding.layoutHome.layoutLiveClient.recyclerView.setVisibility(View.GONE);
            });

        }
    }

    private void setDataToLiveClient(JsonElement message) {
        LiveClientsModel.DataBean dataBean = new LiveClientsModel.DataBean();
        try {
            JSONObject jsonObject = new JSONObject(message.toString());
            if (jsonObject.has("isStartedWorkout") && jsonObject.getBoolean("isStartedWorkout") && checkTime(jsonObject)) {
                dataBean.setEmail(jsonObject.has("email") ? jsonObject.getString("email") : "");
                dataBean.setId(jsonObject.getInt("id"));
                dataBean.setName(jsonObject.getString("name"));
                dataBean.setPhoto(jsonObject.getString("photo"));
                hashMap.put(jsonObject.getInt("id"), dataBean);
            } else if (jsonObject.has("isStartedWorkout") && !jsonObject.getBoolean("isStartedWorkout")) {
                hashMap.remove(jsonObject.getInt("id"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkTime(JSONObject jsonObject) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Log.e("checkTime: ", jsonObject.toString());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -4);
        if (jsonObject.has("date")) {
            try {
                String date = jsonObject.getString("date");
                Date sendDate = spf.parse(date);
                Calendar temp = Calendar.getInstance();
                temp.setTime(sendDate);
                Date currentDate = calendar.getTime();
                Log.e("checkTime:sendDate ", "" + temp.getTimeInMillis());
                Log.e("checkTime:currentDate ", "" + calendar.getTimeInMillis());
                return (temp.getTimeInMillis()) > (calendar.getTimeInMillis());
            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                Log.e("checkTime: ", "JSONException");
                return true;
            }
        }
        Log.e("checkTime: ", "OUT");
        return true;
    }

    private void getClientUpdate() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).gymClientUpdates()
                , getCompositeDisposable(), clientUpdateList, this);
    }


    private void getLiveClients() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getGymLiveClients(Integer.parseInt(userId))
                , getCompositeDisposable(), liveClients, this);
    }

    private void getTrainingSession() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).gymScheduleSummary()
                , getCompositeDisposable(), trainingSession, this);

    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case clientUpdateList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientUpdateListData clientUpdateListData = (ClientUpdateListData) o;
                if (clientUpdateListData != null && clientUpdateListData.getStatus() == AppConstants.SUCCESS && clientUpdateListData.getData() != null && clientUpdateListData.getData().size() > 0) {
                    mBinding.layoutHome.layoutClientUpdate.recyclerView.setAdapter(new ClientUpdateAdapter(getActivity(), clientUpdateListData.getData(), true));
                    mBinding.layoutHome.layoutClientUpdate.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.layoutHome.layoutClientUpdate.txtEmptyClients.setVisibility(View.GONE);
                } else {
                    mBinding.layoutHome.layoutClientUpdate.recyclerView.setVisibility(View.GONE);
                    mBinding.layoutHome.layoutClientUpdate.txtEmptyClients.setVisibility(View.VISIBLE);
                }
                break;
            case liveClients:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LiveClientsModel liveClientsModel = (LiveClientsModel) o;
                if (liveClientsModel != null && liveClientsModel.getStatus() == AppConstants.SUCCESS && liveClientsModel.getData() != null && liveClientsModel.getData().size() > 0) {
//                    mBinding.layoutHome.layoutLiveClient.recyclerView.setAdapter(new LiveClientAdapter(getActivity(), liveClientsModels, true));
                    mBinding.layoutHome.layoutLiveClient.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.layoutHome.layoutLiveClient.txtEmptyClients.setVisibility(View.GONE);
                } else {
                    mBinding.layoutHome.layoutLiveClient.recyclerView.setVisibility(View.GONE);
                    mBinding.layoutHome.layoutLiveClient.txtEmptyClients.setVisibility(View.VISIBLE);
                }
                break;
            case trainingSession:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainerListModel sessionModel = (TrainerListModel) o;
                if (sessionModel != null && sessionModel.getStatus() == AppConstants.SUCCESS && sessionModel.getData() != null && sessionModel.getData().size() > 0) {
                    mBinding.layoutHome.layoutTrainnig.recyclerView.setAdapter(new ScheduleSummaryAdapter(mActivity, sessionModel.getData()));
                    mBinding.layoutHome.layoutTrainnig.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.layoutHome.layoutTrainnig.txtEmptyClients.setVisibility(View.GONE);
                } else {
                    //no live clients
                    mBinding.layoutHome.layoutTrainnig.recyclerView.setVisibility(View.GONE);
                    mBinding.layoutHome.layoutTrainnig.txtEmptyClients.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case liveClients:
                mBinding.layoutHome.layoutLiveClient.recyclerView.setVisibility(View.GONE);
                mBinding.layoutHome.layoutLiveClient.txtEmptyClients.setVisibility(View.VISIBLE);
                break;
            case clientUpdateList:
                mBinding.layoutHome.layoutClientUpdate.recyclerView.setVisibility(View.GONE);
                mBinding.layoutHome.layoutClientUpdate.txtEmptyClients.setVisibility(View.VISIBLE);
                break;
            case trainingSession:
                mBinding.layoutHome.layoutTrainnig.recyclerView.setVisibility(View.GONE);
                mBinding.layoutHome.layoutTrainnig.txtEmptyClients.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onRefresh() {
        getClientUpdate();
        getTrainingSession();
        mBinding.layoutHome.swipeContainer.setRefreshing(false);
    }
}
