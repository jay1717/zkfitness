package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.NutritionDaysAdapter;
import com.fitzoh.app.databinding.DialogAssignClientBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;

import io.reactivex.disposables.CompositeDisposable;

@SuppressLint("ValidFragment")
public class AssignClientDialog extends AppCompatDialogFragment implements NutritionDaysAdapter.onSelect {

    DialogAssignClientBinding mBinding;
    DaySelection listener;
    String userId, userAccessToken;
    public SessionManager session;
    int dietId;
    ArrayList<String> daysName = new ArrayList<>();
    public CompositeDisposable compositeDisposable;
    int clientId;

    public AssignClientDialog(DaySelection listener, int clientId) {
        this.listener = listener;
        this.clientId = clientId;
    }

    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_assign_client, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            view = mBinding.getRoot();
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setView(view);
        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();
        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }

    private void prepareLayout() {
        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerDays.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        ArrayList drawableArray = new ArrayList<>(Arrays.asList(R.drawable.monday, R.drawable.tuesday, R.drawable.wednesday,
                R.drawable.thursday, R.drawable.friday, R.drawable.saturday, R.drawable.sunday));


        mBinding.recyclerDays.setAdapter(new NutritionDaysAdapter(drawableArray, getActivity(), this));

        mBinding.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
               // if (validation())
                    listener.setClick(daysName, clientId);
                    dismiss();
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
        mBinding.btnCancel.setOnClickListener(view -> {
            listener.onCancel();
            dismiss();
        });

    }

    private boolean validation() {
        if (daysName == null) {
            return false;
        }
        return true;
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }


    public void setListener(DaySelection listener) {
        this.listener = listener;
    }


    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void select(String name) {
        daysName.add(name);
    }

    public interface DaySelection {
        void setClick(ArrayList<String> days, int clientId);

        void onCancel();
    }
}
