package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ProfileSubscriptionListAdapter;
import com.fitzoh.app.databinding.FragmentSubscriptionBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.SubscriptionResultData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.BankDetailActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getSubscriptionOptionList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class ProfileSubscriptionFragment extends BaseFragment implements SingleCallback, PaymentResultListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    FragmentSubscriptionBinding mBinding;
    List<SubscriptionResultData.DataBean> subscriptionData;
    String userId, userAccessToken;
    ProfileSubscriptionListAdapter subscriptionListAdapter;
    private boolean isFromRegister = true;

    public ProfileSubscriptionFragment() {
        // Required empty public constructor
    }


    public static ProfileSubscriptionFragment newInstance() {
        ProfileSubscriptionFragment fragment = new ProfileSubscriptionFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_subscription, container, false);
        if (isFromRegister) {
            setupToolBar(mBinding.toolbar.toolbar, "Subscription");
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Subscription");
        }
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.getShapeGradient(mActivity, mBinding.layoutWorkout.btnPurchase);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.btnPurchase.setOnClickListener(view -> {
            setPayment();
        });
        subscriptionData = new ArrayList<>();
        callSubscriptionList();
        return mBinding.getRoot();
    }

    private void setPayment() {

        //   int price = Integer.parseInt(subscriptionData.get(subscriptionListAdapter.selectionPostition).getPrice());
        if (subscriptionListAdapter.selectionPostition == -1) {
            showSnackBar(mBinding.linear, "Select subscription plan", Snackbar.LENGTH_LONG);
        } else if (subscriptionData.get(subscriptionListAdapter.selectionPostition).getPrice() == 0) {
            purchaseSubscription("", "");
        } else {
            checkout();
        }
    }

    private void checkout() {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher);
        final Activity activity = mActivity;
        // int price = Integer.parseInt(subscriptionData.get(subscriptionListAdapter.selectionPostition).getPrice());


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Fitzoh");
            options.put("description", "Subscription charge");
            options.put("currency", "INR");
            options.put("amount", subscriptionData.get(subscriptionListAdapter.selectionPostition).getPrice() * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", session.getAuthorizedUser(mActivity).getEmail());
            preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());
//            preFill.put("contact", "+919784849848");

            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    private void callSubscriptionList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSubscriptionResult()
                    , getCompositeDisposable(), getSubscriptionOptionList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {

            case getSubscriptionOptionList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                subscriptionData = new ArrayList<>();
                SubscriptionResultData subscriptionTypeListModel = (SubscriptionResultData) o;
                if (subscriptionTypeListModel.getStatus() == AppConstants.SUCCESS) {

                    if (subscriptionTypeListModel.getData() != null) {
                        subscriptionData.addAll(subscriptionTypeListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (subscriptionData.size() == 0) {
                            //   mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            //  mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            subscriptionListAdapter = new ProfileSubscriptionListAdapter(mActivity, subscriptionData, session.getAuthorizedUser(mContext).getSubscription_id());
                           /* for (int i = 0; i < subscriptionData.size(); i++) {
                                if (subscriptionData.get(i).getId() == session.getAuthorizedUser(mContext).getSubscription_id()) {
                                    subscriptionListAdapter.selectionPostition = i;
                                }
                            }*/
                            subscriptionListAdapter.notifyDataSetChanged();
                            mBinding.layoutWorkout.recyclerView.setAdapter(subscriptionListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, subscriptionTypeListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    if (isFromRegister) {
                        session.setLogin(true);
                        showSnackBar(mBinding.linear, "subscription purchased successfully", Snackbar.LENGTH_LONG);
                        startActivity(new Intent(mActivity, BankDetailActivity.class));
                        getActivity().finish();
                    } else {
                        getActivity().finish();
                        showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                    }

                } else {

                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");
    }

    public void purchaseSubscription(String transaction_id, String payment_method) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchaseSubscription(
                subscriptionData.get(subscriptionListAdapter.selectionPostition).getId(), transaction_id, payment_method, userId)
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.linear, s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRefresh() {
        callSubscriptionList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }
}
