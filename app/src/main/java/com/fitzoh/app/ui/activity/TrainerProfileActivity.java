package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ActivityTrainerProfileBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.FragmentTrainerProfile;

public class TrainerProfileActivity extends BaseActivity {
    ActivityTrainerProfileBinding mBinding;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (view == null) {
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_trainer_profile);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.realtabcontent, FragmentTrainerProfile.newInstance(getIntent().getExtras()), "FragmentTrainerProfile")
                    .commit();
           /* mBinding.imgClose.setOnClickListener(view -> {
                finish();
            });*/
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
