package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.FitnessCenterImageAdapter;
import com.fitzoh.app.adapter.ImagesAdapter;
import com.fitzoh.app.adapter.ProductImageAdapter;
import com.fitzoh.app.adapter.TrainerBrandAutocompleteAdapter;
import com.fitzoh.app.adapter.TrainerCategoryAutocompleteAdapter;
import com.fitzoh.app.adapter.TrainerProductAutocompleteAdapter;
import com.fitzoh.app.databinding.FragmentTrainerAddProductBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GetFitzohProductModel;
import com.fitzoh.app.model.ShopBrandModel;
import com.fitzoh.app.model.ShopCategoryModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addProduct;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getBrands;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getCategories;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainerAddProductFragment extends BaseFragment implements FitnessCenterImageAdapter.RemoveImageListener, SingleCallback, ProductImageAdapter.RemoveImageListener {

    private FragmentTrainerAddProductBinding mBinding;
    private static int SELECT_IMAGE = 0;
    private FitnessCenterImageAdapter adapter;
    ArrayList<String> imagesEncodedList = new ArrayList<>();
    String userId, userAccessToken;
    ArrayList<GetFitzohProductModel.DataBean> trainerListModel = new ArrayList<>();
    ArrayList<String> productList = new ArrayList<>();
    ArrayList<String> categoryList = new ArrayList<>();
    ArrayList<String> brandList = new ArrayList<>();
    TrainerCategoryAutocompleteAdapter trainerCategoryAutocompleteAdapter = null;
    TrainerProductAutocompleteAdapter productAutocompleteAdapter = null;
    TrainerBrandAutocompleteAdapter trainerBrandAutocompleteAdapter = null;
    ArrayList<ShopCategoryModel.DataBean> category = new ArrayList<>();
    ArrayList<ShopBrandModel.DataBean> brand = new ArrayList<>();
    HashMap<Integer, String> spinnerMap;
    boolean isPhotoSelected = false;
    int productPostition = -1;
    int productId = 0, categopryId = 0, brandId = 0;
    ArrayList<String> getImages = new ArrayList<>();


    public TrainerAddProductFragment() {
        // Required empty public constructor
    }


    public static TrainerAddProductFragment newInstance() {
        TrainerAddProductFragment fragment = new TrainerAddProductFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trainer_add_product, container, false);
        Utils.setImageBackground(getActivity(), mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.imgBack.setOnClickListener(v -> getActivity().finish());
        mBinding.toolbar.tvTitle.setText("Add Product");
        adapter = new FitnessCenterImageAdapter(new ArrayList<>(), this);
        mBinding.txtCertificate.setTextColor(((BaseActivity) mContext).res.getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.imgSelect.setOnClickListener(v -> selectImage());

        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext())) {
                callShoppingData();
                callCategories();
                callBrands();
            } else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }

        mBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    callSubmitAPI();
                }
            }
        });

        mBinding.edtName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                enableAll();
            }
        });

        return mBinding.getRoot();
    }

    private void selectImage() {
        requestAppPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);

                getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
            }

            @Override
            public void onPermissionDenied(int requestCode) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {
            }
        });
    }

    private void callShoppingData() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getFitzohProduct()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callCategories() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getCategories()
                    , getCompositeDisposable(), getCategories, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callBrands() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getBrands()
                    , getCompositeDisposable(), getBrands, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callSubmitAPI() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);

        RequestBody productDataId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(productId));
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtPrice.getText().toString());
        RequestBody qty = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtQuantity.getText().toString());
        RequestBody discountedPrice = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtDiscountedPrice.getText().toString());
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtName.getText().toString());
        RequestBody categoryDataId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(categopryId));
        RequestBody category = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtCategory.getText().toString());
        RequestBody brandDataId = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(brandId));
        RequestBody brand = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtBrandName.getText().toString());
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtDescription.getText().toString());
        RequestBody ingredients = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtIncredient.getText().toString());
        RequestBody carbs = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtCarbs.getText().toString());
        RequestBody fat = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtFat.getText().toString());
        RequestBody protien = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtProtin.getText().toString());
        RequestBody caleroies = RequestBody.create(MediaType.parse("text/plain"), mBinding.edtCalories.getText().toString());

        List<MultipartBody.Part> parts = new ArrayList<>();


        if (isPhotoSelected) {
            for (int i = 0; i < imagesEncodedList.size(); i++) {

                File file = new File(imagesEncodedList.get(i));
                byte[] uploadBytes = Utils.loadImageFromStorage(imagesEncodedList.get(i));
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
                // RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part photo = MultipartBody.Part.createFormData("image[" + i + "]", file.getName(), reqFile);
                parts.add(photo);

            }
        }


        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addProduct(
                productDataId,
                price,
                qty,
                discountedPrice,
                name,
                categoryDataId, category, brandDataId, brand, description, ingredients, carbs, fat, protien, caleroies,
                parts), getCompositeDisposable(), addProduct, this);

    }

    public boolean validation() {

        double DiscountedPrice = 0.00;
        double price = 0.00;
        boolean value = true;
        if (mBinding.edtName.getText().toString().length() == 0) {
            mBinding.txtErrorName.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorName, "Name of supplements must not be null");
            value = false;
        } else {
            mBinding.txtErrorName.setText("");
            mBinding.txtErrorName.setVisibility(View.GONE);
        }
        if (mBinding.edtPrice.getText().toString().length() == 0) {
            mBinding.txtErrorPrice.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorPrice, "Price must not be null");
            value = false;
        } else {
            if (mBinding.edtPrice.getText().toString().length() == 0){
                price = 0.00;
            }else if (mBinding.edtDiscountedPrice.getText().toString().length() == 0){
                DiscountedPrice = 0.00;
            }else {
                price = Double.parseDouble(mBinding.edtPrice.getText().toString());
                DiscountedPrice = Double.parseDouble(mBinding.edtDiscountedPrice.getText().toString());
            }
            mBinding.txtErrorPrice.setText("");
            mBinding.txtErrorPrice.setVisibility(View.GONE);
        }


        if (price < DiscountedPrice) {
            mBinding.txtErrorDiscountedPrice.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorDiscountedPrice, "Discount price can not be more than actual price");
            value = false;
        } else {
            mBinding.txtErrorDiscountedPrice.setText("");
            mBinding.txtErrorDiscountedPrice.setVisibility(View.GONE);
        }
        if (mBinding.edtBrandName.getText().toString().length() == 0) {
            mBinding.txtErrorBrand.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorBrand, "Brand name must not be null");
            value = false;
        } else {
            mBinding.txtErrorBrand.setText("");
            mBinding.txtErrorBrand.setVisibility(View.GONE);
        }
        if (mBinding.edtCategory.getText().toString().length() == 0) {
            mBinding.txtErrorCategory.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorCategory, "Category name must not be null");
            value = false;
        } else {
            mBinding.txtErrorCategory.setText("");
            mBinding.txtErrorCategory.setVisibility(View.GONE);
        }
        if (mBinding.edtCalories.getText().toString().length() == 0) {
            mBinding.txtErrorCalories.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorCalories, "Calories must not be null");
            value = false;
        } else {
            mBinding.txtErrorCalories.setText("");
            mBinding.txtErrorCalories.setVisibility(View.GONE);
        }
        if (mBinding.edtProtin.getText().toString().length() == 0) {
            mBinding.txtErrorProtien.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorProtien, "Protine must not be null");
            value = false;
        } else {
            mBinding.txtErrorProtien.setText("");
            mBinding.txtErrorProtien.setVisibility(View.GONE);
        }
        if (mBinding.edtCarbs.getText().toString().length() == 0) {
            mBinding.txtErrorCarbs.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorCarbs, "Carbs must not be null");
            value = false;
        } else {
            mBinding.txtErrorCarbs.setText("");
            mBinding.txtErrorCarbs.setVisibility(View.GONE);
        }
        if (mBinding.edtFat.getText().toString().length() == 0) {
            mBinding.txtErrorFat.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorFat, "Fat must not be null");
            value = false;
        } else {
            mBinding.txtErrorFat.setText("");
            mBinding.txtErrorFat.setVisibility(View.GONE);
        }
        if (mBinding.edtQuantity.getText().toString().length() == 0) {
            mBinding.txtErrorQuantityPrice.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorQuantityPrice, "Quantity must not be null");
            value = false;
        } else {
            mBinding.txtErrorQuantityPrice.setText("");
            mBinding.txtErrorQuantityPrice.setVisibility(View.GONE);
        }
        if (mBinding.edtDescription.getText().toString().length() == 0) {
            mBinding.txtErrorDescription.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.txtErrorDescription, "Description must not be null");
            value = false;
        } else {
            mBinding.txtErrorDescription.setText("");
            mBinding.txtErrorDescription.setVisibility(View.GONE);
        }

        /*if (isPhotoSelected) {
            if (getImages != null && getImages.size() > 5) {
                showSnackBar(mBinding.getRoot(), getString(R.string.str_select_photos), Snackbar.LENGTH_LONG);
                value = false;
            }
        }*/
        return value;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_IMAGE) {
            isPhotoSelected = true;
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            trainerListModel = new ArrayList<>();
            if (resultCode == Activity.RESULT_OK) {
                if (data.getClipData() != null) {
                    adapter.arrayList = new ArrayList<>();
                    imagesEncodedList = new ArrayList<>();
                    ClipData mClipData = data.getClipData();
                    getImages = new ArrayList<>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        mBinding.txtCertificate.setVisibility(View.GONE);

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        Log.e("uri:upr", uri != null ? uri.toString() : "null6e");
                        adapter.arrayList.add(uri);
                        Cursor cursor = mActivity.getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);
                        Log.e("onActivityreturned:upr", imageEncoded != null ? imageEncoded : "null6e");
                        GetFitzohProductModel.DataBean imgModel = new GetFitzohProductModel.DataBean();
                        String imageName = uri.toString();
                        getImages.add(imageName);
                        imgModel.setImage(getImages);
                        trainerListModel.add(imgModel);
                        setAdapter();
                        cursor.close();
                    }
                } else if (data.getData() != null) {
                    adapter.arrayList = new ArrayList<>();
                    imagesEncodedList = new ArrayList<>();

                    Uri mImageUri = data.getData();

                    Log.e("mImageUri:", mImageUri != null ? mImageUri.toString() : "null6e");
                    // Get the cursor
                    Cursor cursor = mActivity.getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imageEncoded = cursor.getString(columnIndex);
                    Log.e("onActivityreturned:", imageEncoded != null ? imageEncoded : "null6e");

                    GetFitzohProductModel.DataBean imgModel = new GetFitzohProductModel.DataBean();
                    String imageName = mImageUri.toString();
                    getImages.add(imageName);
                    imgModel.setImage(getImages);
                    trainerListModel.add(imgModel);

                    adapter.arrayList.add(mImageUri);
                    imagesEncodedList.add(imageEncoded);

                    setAdapter();

                }
            }
        }
    }

    private void setAdapter() {

        mBinding.recyclerView.setVisibility(View.VISIBLE);
        mBinding.txtCertificate.setVisibility(View.GONE);
        mBinding.imgSelect.setVisibility(View.VISIBLE);
        ProductImageAdapter productImageAdapter = new ProductImageAdapter(getActivity(), trainerListModel, this);
        mBinding.recyclerView.setAdapter(productImageAdapter);

    }

    private void disableAll() {
        mBinding.edtCalories.setEnabled(false);
        mBinding.edtCarbs.setEnabled(false);
        mBinding.edtFat.setEnabled(false);
        mBinding.edtProtin.setEnabled(false);
        mBinding.edtIncredient.setEnabled(false);
        mBinding.edtBrandName.setEnabled(false);
        mBinding.edtCategory.setEnabled(false);
    }

    private void enableAll() {
        mBinding.edtCalories.setEnabled(true);
        mBinding.edtCarbs.setEnabled(true);
        mBinding.edtFat.setEnabled(true);
        mBinding.edtProtin.setEnabled(true);
        mBinding.edtIncredient.setEnabled(true);
        mBinding.edtBrandName.setEnabled(true);
        mBinding.edtCategory.setEnabled(true);

        mBinding.edtCalories.setText("");
        mBinding.edtCarbs.setText("");
        mBinding.edtFat.setText("");
        mBinding.edtProtin.setText("");
        mBinding.edtIncredient.setText("");
        mBinding.edtBrandName.setText("");
        mBinding.edtCategory.setText("");

        if (!isPhotoSelected) {
            mBinding.recyclerView.setVisibility(View.INVISIBLE);
            mBinding.txtCertificate.setVisibility(View.VISIBLE);
            mBinding.imgSelect.setVisibility(View.VISIBLE);
            getImages = new ArrayList<>();
            mBinding.recyclerView.setAdapter(new ImagesAdapter(getActivity(), getImages));
        }

        categopryId = 0;
        brandId = 0;
        productId = 0;
    }


    @Override
    public void removeImage(int position) {
        trainerListModel.remove(position);
        imagesEncodedList.remove(position);
        getImages.remove(position);
        mBinding.recyclerView.setVisibility(View.INVISIBLE);
        mBinding.txtCertificate.setVisibility(View.VISIBLE);
        mBinding.imgSelect.setVisibility(View.VISIBLE);
        setAdapter();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainerListModel = new ArrayList<>();
                GetFitzohProductModel trainerShopListModel = (GetFitzohProductModel) o;
                if (trainerShopListModel.getStatus() == AppConstants.SUCCESS) {

                    if (trainerShopListModel != null) {
                        trainerListModel.addAll(trainerShopListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (trainerListModel.size() == 0) {

                        } else {

                            productList = new ArrayList<>();
                            int size = trainerShopListModel.getData().size();
                            for (int i = 0; i < size; i++) {
                                productList.add(trainerShopListModel.getData().get(i).getName());

                            }
                            productAutocompleteAdapter = new TrainerProductAutocompleteAdapter(getActivity(), trainerListModel);
                            mBinding.edtName.setAdapter(productAutocompleteAdapter);
                            mBinding.edtName.setOnItemClickListener((parent, view, position, id) -> {
                                productPostition = position;
                                isPhotoSelected = false;
                                mBinding.edtCalories.setText(trainerListModel.get(position).getCalories());
                                mBinding.edtCarbs.setText(trainerListModel.get(position).getCarbs());
                                mBinding.edtFat.setText(trainerListModel.get(position).getFat());
                                mBinding.edtProtin.setText(trainerListModel.get(position).getProtein());
                                mBinding.edtIncredient.setText(trainerListModel.get(position).getIngredients());
                                mBinding.edtBrandName.setText(trainerListModel.get(position).getBrand());
                                mBinding.edtCategory.setText(trainerListModel.get(position).getCategory());
                                productId = trainerListModel.get(position).getId();
                                categopryId = Integer.parseInt(trainerListModel.get(position).getCategory_id());
                                brandId = trainerListModel.get(position).getBrand_id();
                                getImages.addAll(trainerListModel.get(position).getImage());
                                mBinding.recyclerView.setVisibility(View.VISIBLE);
                                mBinding.txtCertificate.setVisibility(View.GONE);
                                mBinding.imgSelect.setVisibility(View.GONE);
                                mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
                                mBinding.recyclerView.setAdapter(new ImagesAdapter(getActivity(), getImages));
                                disableAll();

                            });
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, trainerShopListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

            case getCategories:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ShopCategoryModel shopCategoryModel = (ShopCategoryModel) o;
                if (shopCategoryModel.getStatus() == AppConstants.SUCCESS) {

                    if (shopCategoryModel != null) {
                        category.addAll(shopCategoryModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (category.size() == 0) {

                        } else {

                            categoryList = new ArrayList<>();
                            int size = shopCategoryModel.getData().size();
                            for (int i = 0; i < size; i++) {
                                categoryList.add(shopCategoryModel.getData().get(i).getName());

                            }
                            trainerCategoryAutocompleteAdapter = new TrainerCategoryAutocompleteAdapter(getActivity(), category);
                            mBinding.edtCategory.setAdapter(trainerCategoryAutocompleteAdapter);
                            mBinding.edtCategory.setOnItemClickListener((parent, view, position, id) -> {
                                Object item = parent.getItemAtPosition(position);
                                if (item instanceof ShopCategoryModel) {
                                    ShopCategoryModel selectedData = (ShopCategoryModel) item;
                                    categopryId = selectedData.getData().get(position).getId();
                                }
                            });
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, shopCategoryModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

            case getBrands:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ShopBrandModel shopBrandModel = (ShopBrandModel) o;
                if (shopBrandModel.getStatus() == AppConstants.SUCCESS) {

                    if (shopBrandModel != null) {
                        brand.addAll(shopBrandModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (brand.size() == 0) {

                        } else {

                            brandList = new ArrayList<>();
                            int size = shopBrandModel.getData().size();
                            for (int i = 0; i < size; i++) {
                                brandList.add(shopBrandModel.getData().get(i).getName());

                            }
                            trainerBrandAutocompleteAdapter = new TrainerBrandAutocompleteAdapter(getActivity(), brand);
                            mBinding.edtBrandName.setAdapter(trainerBrandAutocompleteAdapter);
                            mBinding.edtBrandName.setOnItemClickListener((parent, view, position, id) -> {
                                Object item = parent.getItemAtPosition(position);
                                if (item instanceof ShopCategoryModel) {
                                    ShopCategoryModel selectedData = (ShopCategoryModel) item;
                                    brandId = selectedData.getData().get(position).getId();
                                }
                            });
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, shopBrandModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

            case addProduct:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getActivity().finish();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

}
