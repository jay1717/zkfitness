package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.BankDetailFragment;
import com.fitzoh.app.ui.fragment.SubscriptionFragment;

public class SubscriptionActivity extends BaseActivity implements PaymentResultListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suscription);

        Checkout.preload(getApplicationContext());

        SubscriptionFragment subscriptionFragment = SubscriptionFragment.newInstance();
        subscriptionFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, subscriptionFragment, "BankDetailFragment")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        ((SubscriptionFragment) fragment).onPaymentSuccess(s);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        ((SubscriptionFragment) fragment).onPaymentError(i, s);
    }
}
