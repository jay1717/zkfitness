package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.AddSetListAdapter;
import com.fitzoh.app.databinding.FragmentAddSetBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.ExerciseSetListModel;
import com.fitzoh.app.model.WorkoutParamModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.SelectSetUnitActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.mealList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReplaceExerciseSetFragment extends BaseFragment implements SingleCallback, AddSetListAdapter.AddSetInterface, SwipeRefreshLayout.OnRefreshListener {


    FragmentAddSetBinding mBinding;
    String userId, userAccessToken;
    private AddSetListAdapter adapter;
    private int param1_id = 0, param2_id = 0, param3_id = 0;
    String param1 = "", param2 = "", param3 = "", unit = "";
    private List<Integer> deletedList = new ArrayList<>();
    private List<WorkoutParamModel.DataBean> muscleDatas = new ArrayList<>();

    public ReplaceExerciseSetFragment() {
        // Required empty public constructor
    }


    public static ReplaceExerciseSetFragment newInstance(Bundle bundle) {
        ReplaceExerciseSetFragment fragment = new ReplaceExerciseSetFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            exerciseData = (WorkoutExerciseListModel.DataBean) getArguments().get("exercise");
//            workoutData = (WorkOutListModel.DataBean) getArguments().get("workout");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title = "Sets";
        if (getArguments() != null && getArguments().containsKey("data")) {
            String data = getArguments().getString("data", "");
            try {
                JSONObject jsonObject = new JSONObject(data);
                title = jsonObject.getString("exercise_name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, title);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_add_set, container, false);
        Utils.getShapeGradient(mActivity, mBinding.layoutSet.btnSave);
        Utils.setAddFabBackground(mActivity, mBinding.layoutSet.fab);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        setHasOptionsMenu(true);
        adapter = new AddSetListAdapter(mContext, this);
        mBinding.layoutSet.recyclerView.setAdapter(adapter);
//        getExerciseSetAPI();
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
       /* mBinding.layoutSet.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutSet.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));*/
        callParameterAPI();
        mBinding.layoutSet.fab.setOnClickListener(view -> {
            if (adapter.dataList.size() <= 0) {
                if (muscleDatas != null) {
                    addDataToAdapter(true);
                } else {
                    callParameterAPI();
                }
            } else {
                addDataToAdapter(true);
            }
        });
        mBinding.layoutSet.btnSave.setOnClickListener(view -> {
            JSONObject object = new JSONObject();
//            String json = new Gson().toJson(adapter.dataList);
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < adapter.dataList.size(); i++) {
                JSONArray array = new JSONArray();
                for (int j = 0; j < adapter.dataList.get(i).size(); j++) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("setno", i + 1);
                        obj.put("id", adapter.dataList.get(i).get(j).getId());
                        obj.put("parameter_id", adapter.dataList.get(i).get(j).getParameter_id());
                        obj.put("parameter_name", adapter.dataList.get(i).get(j).getParameter_name());
                        obj.put("weight_unit", adapter.dataList.get(i).get(j).getWeight_unit());
                        obj.put("set_value", adapter.dataList.get(i).get(j).getValue());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    array.put(obj);
                }
                jsonArray.put(array);
            }
            Log.e("data", jsonArray.toString());
            Intent intent = new Intent();
            intent.putExtra("main", getArguments().getInt("main", 0));
            intent.putExtra("sub", getArguments().getInt("sub", 0));
            if (getArguments() != null && getArguments().containsKey("data"))
                try {
                    object = new JSONObject(getArguments().getString("data"));
                    object.put("sets", jsonArray.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            intent.putExtra("data", object.toString());
            mActivity.setResult(Activity.RESULT_OK, intent);
            mActivity.finish();

        });
        return mBinding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            param1_id = data.getIntExtra("param1_id", 0);
            param1 = data.getStringExtra("param1");
            param2_id = data.getIntExtra("param2_id", 0);
            param2 = data.getStringExtra("param2");
            param3_id = data.getIntExtra("param3_id", 0);
            param3 = data.getStringExtra("param3");
            if (data.hasExtra("unit")) {
                unit = data.getStringExtra("unit");
            }
            addDataToAdapter(false);
        }

    }

    private void addDataToAdapter(boolean isAddNew) {
        if (adapter.dataList.size() <= 0) {
            mBinding.layoutSet.recyclerView.setVisibility(View.VISIBLE);
            mBinding.layoutSet.imgNoData.setVisibility(View.GONE);
        }
        if (!isAddNew) {
            if (adapter.dataList.size() > 0) {
                for (int i = 0; i < adapter.dataList.size(); i++) {
                    adapter.dataList.get(i).get(0).setParameter_name(param1);
                    adapter.dataList.get(i).get(1).setParameter_name(param2);
                    adapter.dataList.get(i).get(2).setParameter_name(param3);

                    adapter.dataList.get(i).get(0).setParameter_id(param1_id);
                    adapter.dataList.get(i).get(1).setParameter_id(param2_id);
                    adapter.dataList.get(i).get(2).setParameter_id(param3_id);

                    adapter.dataList.get(i).get(0).setValue("");
                    adapter.dataList.get(i).get(1).setValue("");
                    adapter.dataList.get(i).get(2).setValue("");

                    adapter.dataList.get(i).get(0).setWeight_unit(unit);
                    adapter.dataList.get(i).get(1).setWeight_unit(unit);
                    adapter.dataList.get(i).get(2).setWeight_unit(unit);

                    adapter.notifyDataSetChanged();
                }
            } else {
                adapter.addData(getNewData());
                mBinding.layoutSet.recyclerView.scrollToPosition(adapter.dataList.size() - 1);
            }
        } else {
            adapter.addData(getNewData());
            mBinding.layoutSet.recyclerView.scrollToPosition(adapter.dataList.size() - 1);
        }
    }

    private List<ExerciseSetListModel.DataBean> getNewData() {
        List<ExerciseSetListModel.DataBean> listValues = new ArrayList<>();
        ExerciseSetListModel.DataBean dataBean = new ExerciseSetListModel.DataBean();
        dataBean.setParameter_name(param1);
        dataBean.setParameter_id(param1_id);
        dataBean.setValue("");
        dataBean.setWeight_unit(unit);
        listValues.add(dataBean);

        dataBean = new ExerciseSetListModel.DataBean();
        dataBean.setParameter_name(param2);
        dataBean.setParameter_id(param2_id);
        dataBean.setValue("");
        dataBean.setWeight_unit(unit);
        listValues.add(dataBean);

        dataBean = new ExerciseSetListModel.DataBean();
        dataBean.setParameter_name(param3);
        dataBean.setParameter_id(param3_id);
        dataBean.setValue("");
        dataBean.setWeight_unit(unit);
        listValues.add(dataBean);
        return listValues;
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ExerciseSetListModel exerciseSetListModel = (ExerciseSetListModel) o;
                if (exerciseSetListModel != null && exerciseSetListModel.getStatus() == AppConstants.SUCCESS && exerciseSetListModel.getData() != null && exerciseSetListModel.getData().size() > 0) {
                    setParameterValue(exerciseSetListModel.getData().get(0));
                    mBinding.layoutSet.imgNoData.setVisibility(View.GONE);
                    mBinding.layoutSet.recyclerView.setVisibility(View.VISIBLE);
                    adapter.setAdapterData(exerciseSetListModel.getData());
                } else {
                    mBinding.layoutSet.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.layoutSet.recyclerView.setVisibility(View.GONE);
                    callParameterAPI();
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.onBackPressed();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case mealList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutParamModel workoutParamModel = (WorkoutParamModel) o;
                if (workoutParamModel != null && workoutParamModel.getStatus() == AppConstants.SUCCESS && workoutParamModel.getData() != null && workoutParamModel.getData().size() > 0) {
                    muscleDatas = workoutParamModel.getData();
                    for (int i = 0; i < muscleDatas.size(); i++) {
                        if (muscleDatas.get(i).getValue().equalsIgnoreCase("Weight")) {
                            param1 = muscleDatas.get(i).getValue();
                            param1_id = muscleDatas.get(i).getId();
                            unit = "1";
                        }
                        if (muscleDatas.get(i).getValue().equalsIgnoreCase("Reps")) {
                            param2 = muscleDatas.get(i).getValue();
                            param2_id = muscleDatas.get(i).getId();
                        }
                        if (muscleDatas.get(i).getValue().equalsIgnoreCase("Rest Period")) {
                            param3 = muscleDatas.get(i).getValue();
                            param3_id = muscleDatas.get(i).getId();
                        }
                    }
                    addDataToAdapter(true);
                }
                break;
        }

    }

    private void setParameterValue(List<ExerciseSetListModel.DataBean> dataBeans) {
        param1_id = dataBeans.get(0).getParameter_id();
        param1 = dataBeans.get(0).getParameter_name();

        param2_id = dataBeans.get(1).getParameter_id();
        param2 = dataBeans.get(1).getParameter_name();

        param3_id = dataBeans.get(2).getParameter_id();
        param3 = dataBeans.get(2).getParameter_name();
        if (param1.equalsIgnoreCase("Weight") && dataBeans.get(0).getWeight_unit() != null)
            unit = dataBeans.get(0).getWeight_unit();
        else if (param2.equalsIgnoreCase("Weight") && dataBeans.get(1).getWeight_unit() != null)
            unit = dataBeans.get(1).getWeight_unit();
        else if (param3.equalsIgnoreCase("Weight") && dataBeans.get(2).getWeight_unit() != null)
            unit = dataBeans.get(2).getWeight_unit();

    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case workoutList:
                mBinding.layoutSet.imgNoData.setVisibility(View.VISIBLE);
                mBinding.layoutSet.recyclerView.setVisibility(View.GONE);
//                addDataToAdapter(true);
                callParameterAPI();
                break;
            case mealList:
                break;
        }
    }

    private void callParameterAPI() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getParameters()
                , getCompositeDisposable(), mealList, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_set, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.action_edit), R.drawable.ic_edit);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                startSelectUnitActivity();
                break;
        }
        return true;
    }

    private void startSelectUnitActivity() {
        Intent intent = new Intent(mActivity, SelectSetUnitActivity.class);
        intent.putExtra("param1_id", param1_id);
        intent.putExtra("param1", param1);
        intent.putExtra("param2_id", param2_id);
        intent.putExtra("param2", param2);
        intent.putExtra("param3_id", param3_id);
        intent.putExtra("param3", param3);
        if (!unit.equalsIgnoreCase("")) {
            intent.putExtra("unit", unit);
        }
        mActivity.startActivityForResult(intent, 0);
    }

    @Override
    public void delete(int paramId) {
        if (paramId != 0)
            deletedList.add(paramId);

        if (adapter.dataList.size() <= 0) {
            mBinding.layoutSet.imgNoData.setVisibility(View.VISIBLE);
            mBinding.layoutSet.recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        callParameterAPI();
        //mBinding.layoutSet.swipeContainer.setRefreshing(false);
    }
}
