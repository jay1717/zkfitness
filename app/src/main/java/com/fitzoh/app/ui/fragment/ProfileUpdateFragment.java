package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.system.ErrnoException;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentProfileUpdateBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.model.TransformationListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AppThemeSettingActivity;
import com.fitzoh.app.ui.activity.NavigationClientMainActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.ui.activity.SubscriptionActivity;
import com.fitzoh.app.ui.activity.TransformationAddActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.fitzoh.app.vectorchildfinder.VectorChildFinder;
import com.fitzoh.app.vectorchildfinder.VectorDrawableCompat;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.editTransformation;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateClientProfile;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileUpdateFragment extends BaseFragment implements View.OnClickListener, SingleCallback {


    FragmentProfileUpdateBinding mBinding;
    private static final int RECORD_REQUEST_CODE = 101;
    boolean isBefore = false;
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImageBefore, fileImageAfter;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Calendar c;
    long beforeTime;
    long afterTime;
    private boolean isFromRegister = true;
    String userId, userAccessToken;
    TransformationListModel.DataBean transformationData;

    public ProfileUpdateFragment() {
        // Required empty public constructor
    }

    public static ProfileUpdateFragment newInstance(Bundle bundle) {
        ProfileUpdateFragment fragment = new ProfileUpdateFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("IS_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("IS_FROM_REGISTER", true);
        }
        if (getArguments() != null && getArguments().containsKey("TRANSFORMATION_DATA")) {
            transformationData = (TransformationListModel.DataBean) getArguments().getSerializable("TRANSFORMATION_DATA");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_profile_update, container, false);
        Utils.getShapeGradient(mActivity, mBinding.profile.btnSubmit);
        setTextViewStartImage(mBinding.profile.txtBeforeDate, R.drawable.ic_calendar, R.drawable.ic_down_arrow);
        setTextViewStartImage(mBinding.profile.txtAfterDate, R.drawable.ic_calendar, R.drawable.ic_down_arrow);
        mBinding.txtSkip.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (isFromRegister) {
            mBinding.txtSkip.setVisibility(View.VISIBLE);
            setupToolBar(mBinding.toolbar.toolbar, getString(R.string.transformation));
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.transformation));
            mBinding.txtSkip.setVisibility(View.GONE);
            if (transformationData != null) {
                setHasOptionsMenu(true);
                setData();
            }

        }
        c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        makeRequest();
        clickEvents();
        return mBinding.getRoot();
    }

    private void setData() {
        mBinding.profile.txtBeforeDate.setText(transformationData.getBefore_date());
        mBinding.profile.txtAfterDate.setText(transformationData.getAfter_date());
        mBinding.profile.txtDescription.setText(transformationData.getDescription());
        Utils.setImage(mContext, mBinding.profile.imgBefore, transformationData.getBefore_image());
        Utils.setImage(mContext, mBinding.profile.imgAfter, transformationData.getAfter_image());

    }

    private void clickEvents() {
        mBinding.txtSkip.setOnClickListener(this);
        mBinding.profile.btnSubmit.setOnClickListener(this);
        mBinding.profile.imgPickPhotoBefore.setOnClickListener(this);
        mBinding.profile.imgPickPhotoAfter.setOnClickListener(this);
        mBinding.profile.txtBeforeDate.setOnClickListener(this);
        mBinding.profile.txtAfterDate.setOnClickListener(this);
        mBinding.profile.fab.setOnClickListener(this);
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        getActivity().startActivityForResult(getPickImageChooserIntent(), 200);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                if (isBefore) fileImageBefore = new File(mImageUri.getPath());
                else fileImageAfter = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {

                    if (isBefore)
                        mBinding.profile.imgBefore.setImageURI(mImageUri);
                    else
                        mBinding.profile.imgAfter.setImageURI(mImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_pick_photo_before:
                isBefore = true;
                imgPick();
                break;
            case R.id.img_pick_photo_after:
                isBefore = false;
                imgPick();
                break;
            case R.id.fab:
                startActivity(new Intent(mContext, TransformationAddActivity.class));
                getActivity().overridePendingTransition(0, 0);
                getActivity().finish();
                break;
            case R.id.txt_before_date:
                DatePickerDialog datePickerDialogBefore = new DatePickerDialog(mContext,
                        (view, year, monthOfYear, dayOfMonth) -> {
                            c.set(Calendar.YEAR, year);
                            c.set(Calendar.MONTH, monthOfYear);
                            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                            beforeTime = c.getTimeInMillis();
                            mBinding.profile.txtBeforeDate.setText(dateFormat.format(c.getTime()));
                        }, mYear, mMonth, mDay);
                datePickerDialogBefore.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialogBefore.show();
                break;
            case R.id.txt_after_date:
                DatePickerDialog datePickerDialogAfter = new DatePickerDialog(mContext,
                        (view, year, monthOfYear, dayOfMonth) -> {
                            c.set(Calendar.YEAR, year);
                            c.set(Calendar.MONTH, monthOfYear);
                            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
                            mBinding.profile.txtAfterDate.setText(dateFormat.format(c.getTime()));
                            afterTime = c.getTimeInMillis();
                        }, mYear, mMonth, mDay);
                datePickerDialogAfter.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialogAfter.show();
                break;
            case R.id.btn_submit:
                if (validation()) {

                    callAPI();
//                    startActivity(new Intent(mActivity, NavigationMainActivity.class));
//                    getActivity().finish();
                }
                break;
            case R.id.txtSkip:
                if (session.getRollId() == 2 && session.getAuthorizedUser(mActivity).isGym_trainer()) {
                   // startActivity(new Intent(mActivity, AppThemeSettingActivity.class).putExtra("IS_LOGIN", true));
                    startActivity(new Intent(mActivity, NavigationMainActivity.class));
                    getActivity().finish();
                    return;
                }
                startActivity(new Intent(mActivity, SubscriptionActivity.class));
                getActivity().finish();
                break;
        }
    }

    private boolean validation() {
        if (transformationData == null) {
            if (fileImageBefore == null) {
                showSnackBar(mBinding.layoutProfile, "Please select before image", Snackbar.LENGTH_LONG);
                return false;
            } else if (fileImageAfter == null) {
                showSnackBar(mBinding.layoutProfile, "Please select after image", Snackbar.LENGTH_LONG);
                return false;
            }
        }
        if (mBinding.profile.txtBeforeDate.getText().toString().length() <= 0) {
            showSnackBar(mBinding.layoutProfile, "Please select before date", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.profile.txtAfterDate.getText().toString().length() <= 0) {
            showSnackBar(mBinding.layoutProfile, "Please select after date", Snackbar.LENGTH_LONG);
            return false;
        } else if (beforeTime > afterTime) {
            showSnackBar(mBinding.layoutProfile, getString(R.string.str_date), Snackbar.LENGTH_LONG);
            return false;

        } else if (mBinding.profile.txtDescription.getText().toString().length() == 0) {
            showSnackBar(mBinding.layoutProfile, getString(R.string.str_experience), Snackbar.LENGTH_LONG);
            return false;
        }

        return true;
    }

    private void addTransformationDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(mActivity)
                .setTitle("Add Image")
                .setMessage("Do you want to add another transformation?")
                .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    Intent intent = new Intent(getActivity(), TransformationAddActivity.class);
                    if (isFromRegister) {
                        getActivity().overridePendingTransition(0, 0);
                        getActivity().finish();
                    } else {
                        intent.putExtra("IS_FROM_REGISTER", false);
                        startActivity(intent);
                        getActivity().finish();

                    }
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    if (isFromRegister) {
                        if (session.getRollId() == 2 && session.getAuthorizedUser(mActivity).isGym_trainer()) {
                           /* startActivity(new Intent(mActivity, AppThemeSettingActivity.class).putExtra("IS_LOGIN", true));
                            getActivity().finish();*/
                            Intent intent;
                            if (session.getRollId() == 1) {
                                intent = new Intent(mActivity, NavigationClientMainActivity.class);
                            } else {
                                intent = new Intent(mActivity, NavigationMainActivity.class);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            mActivity.finish();
                            return;
                        }
                        startActivity(new Intent(mActivity, SubscriptionActivity.class));
                        getActivity().finish();
                    } else {
                        getActivity().finish();
                    }
                    // startActivity(new Intent(mActivity, NavigationMainActivity.class));
                })
                .show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
    }

    private void callAPI() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);

        RequestBody before = RequestBody.create(MediaType.parse("text/plain"), mBinding.profile.txtBeforeDate.getText().toString());
        RequestBody after = RequestBody.create(MediaType.parse("text/plain"), mBinding.profile.txtAfterDate.getText().toString());
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), mBinding.profile.txtDescription.getText().toString());
        MultipartBody.Part beforeImage = null, afterImage = null;
        if (fileImageBefore != null) {
            byte[] uploadBytes = Utils.loadImageFromStorage(fileImageBefore.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
          //  RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImageBefore);
            beforeImage = MultipartBody.Part.createFormData("before_image", fileImageBefore.getName(), reqFile);
        }
        if (fileImageAfter != null) {
            byte[] uploadBytes = Utils.loadImageFromStorage(fileImageAfter.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
           // RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImageAfter);
            afterImage = MultipartBody.Part.createFormData("after_image", fileImageAfter.getName(), reqFile);

        }
        if (transformationData != null) {
            RequestBody trasformation_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(transformationData.getId()));
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).editTransformations(
                    before, after, desc, beforeImage, afterImage, trasformation_id), getCompositeDisposable(), editTransformation, this);
        } else {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addTransformations(
                    before, after, desc, beforeImage, afterImage), getCompositeDisposable(), updateClientProfile, this);
        }


    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case updateClientProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                RegisterModel registerModel = (RegisterModel) o;
                if (registerModel.getStatus() == AppConstants.SUCCESS) {
                    //  mBinding.profile.fab.setVisibility(View.VISIBLE);
                    addTransformationDialog();
                } else {
                    showSnackBar(mBinding.layoutProfile, registerModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case editTransformation:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    //  mBinding.profile.fab.setVisibility(View.VISIBLE);
                    showSnackBar(mBinding.layoutProfile, "Training program Updated successfully", Snackbar.LENGTH_LONG);
                    getActivity().finish();


                } else {
                    showSnackBar(mBinding.layoutProfile, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutProfile, "onFailure", Snackbar.LENGTH_LONG);

    }

    public void setTextViewStartImage(TextView textView, int idStart, int idEnd) {
        int color = ContextCompat.getColor(mActivity, R.color.colorAccent);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) mActivity).res, idStart, textView);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        VectorDrawableCompat.VFullPath path3 = vector.findPathByName("path3");
        VectorDrawableCompat.VFullPath path4 = vector.findPathByName("path4");
        VectorDrawableCompat.VFullPath path5 = vector.findPathByName("path5");
        VectorDrawableCompat.VFullPath path6 = vector.findPathByName("path6");
        VectorDrawableCompat.VFullPath path7 = vector.findPathByName("path7");
        VectorDrawableCompat.VFullPath path8 = vector.findPathByName("path8");
        VectorDrawableCompat.VFullPath path9 = vector.findPathByName("path9");
        VectorDrawableCompat.VFullPath path10 = vector.findPathByName("path10");
        path1.setFillColor(color);
        if (path2 != null) path2.setFillColor(color);
        if (path3 != null) path3.setFillColor(color);
        if (path4 != null) path4.setFillColor(color);
        if (path5 != null) path5.setFillColor(color);
        if (path6 != null) path6.setFillColor(color);
        if (path7 != null) path7.setFillColor(color);
        if (path8 != null) path8.setFillColor(color);
        if (path9 != null) path9.setFillColor(color);
        if (path10 != null)
            path10.setFillColor(color);

        VectorChildFinder vector1 = new VectorChildFinder(((BaseActivity) mActivity).res, idEnd, textView);
        VectorDrawableCompat.VFullPath pathv1 = vector1.findPathByName("path1");
        pathv1.setFillColor(color);

        textView.setCompoundDrawablesWithIntrinsicBounds(vector.getDrawable(), null, vector1.getDrawable(), null);
    }
}
