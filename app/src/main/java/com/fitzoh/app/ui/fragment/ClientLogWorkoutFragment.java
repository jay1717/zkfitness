package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientLogWorkoutListAdapter;
import com.fitzoh.app.databinding.FragmentClientWorkoutBinding;
import com.fitzoh.app.model.ClientWorkoutTrackingModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.clientWorkoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientLogWorkoutFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {

    FragmentClientWorkoutBinding mBinding;
    String userId, userAccessToken;
    List<ClientWorkoutTrackingModel.DataBean> workoutList;
    ClientLogWorkoutListAdapter clientWorkoutListAdapter;

    public ClientLogWorkoutFragment() {
        // Required empty public constructor
    }

    public static ClientLogWorkoutFragment newInstance() {
        return new ClientLogWorkoutFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Log Workouts");

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_workout, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        mBinding.layoutWorkout.fab.hide();
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        workoutList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        clientWorkoutListAdapter = new ClientLogWorkoutListAdapter(mActivity, workoutList);
        mBinding.layoutWorkout.recyclerView.setAdapter(clientWorkoutListAdapter);
        callWorkoutList();
        return mBinding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            if (session.getAuthorizedUser(mActivity).getRoleId().equalsIgnoreCase("1")) {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClietWorkoutTracking()
                        , getCompositeDisposable(), clientWorkoutList, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerClietWorkoutTracking(session.getStringDataByKeyNull(CLIENT_ID))
                        , getCompositeDisposable(), clientWorkoutList, this);
            }
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case clientWorkoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                workoutList = new ArrayList<>();
                ClientWorkoutTrackingModel dataBean = (ClientWorkoutTrackingModel) o;
                if (dataBean.getStatus() == AppConstants.SUCCESS) {

                    if (dataBean != null) {
                        workoutList.addAll(dataBean.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (workoutList.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            clientWorkoutListAdapter = new ClientLogWorkoutListAdapter(mActivity, workoutList);
                            mBinding.layoutWorkout.recyclerView.setAdapter(clientWorkoutListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, dataBean.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        callWorkoutList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }
}
