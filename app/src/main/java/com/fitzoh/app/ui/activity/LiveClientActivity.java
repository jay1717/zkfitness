package com.fitzoh.app.ui.activity;

import android.os.Bundle;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.LiveClientsFragment;

public class LiveClientActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_client);


        LiveClientsFragment liveClientsFragment = LiveClientsFragment.newInstance();
        liveClientsFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, liveClientsFragment, "Live client")
                .commit();

    }
}
