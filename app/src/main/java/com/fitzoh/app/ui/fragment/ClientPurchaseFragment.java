package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientProfilePurchaseAdapter;
import com.fitzoh.app.adapter.PackagesListAdapter;
import com.fitzoh.app.databinding.FragmentClientPurchaseBinding;
import com.fitzoh.app.model.ClientProfilePackageList;
import com.fitzoh.app.model.PackageListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getPackageList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientPurchaseFragment extends BaseFragment implements SingleCallback, PackagesListAdapter.onAction, SwipeRefreshLayout.OnRefreshListener {

    FragmentClientPurchaseBinding mBinding;
    String userId, userAccessToken;
    List<ClientProfilePackageList.DataBean> dataBeanList;
    ClientProfilePurchaseAdapter packagesListAdapter;
    static int clientDataId = 0;

    public ClientPurchaseFragment() {
        // Required empty public constructor
    }

    public static ClientPurchaseFragment newInstance(int clientId) {
        ClientPurchaseFragment fragment = new ClientPurchaseFragment();
        clientDataId = clientId;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupToolBarWithBackArrow(mBinding.toolbar.too, "Arm Workout - Assign Clients");
    }

    @Override
    public void onResume() {
        super.onResume();
        callPackageList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_purchase, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        return mBinding.getRoot();
    }

    private void callPackageList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientProfilePurchaseList(clientDataId)
                    , getCompositeDisposable(), getPackageList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case getPackageList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                dataBeanList = new ArrayList<>();
                ClientProfilePackageList packageListModel = (ClientProfilePackageList) o;
                if (packageListModel != null) {
                    dataBeanList.addAll(packageListModel.getData());
                    // workoutListAdapter.notifyDataSetChanged();
                    if (packageListModel.getData().size() == 0) {
                        mBinding.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.recyclerView.setVisibility(View.VISIBLE);
                        mBinding.imgNoData.setVisibility(View.GONE);
                        packagesListAdapter = new ClientProfilePurchaseAdapter(dataBeanList, getActivity());
                        mBinding.recyclerView.setAdapter(packagesListAdapter);
                    }
                } else
                    showSnackBar(mBinding.mainLayout, packageListModel.getMessage(), Snackbar.LENGTH_LONG);

                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void edit(PackageListModel.DataBean packageListModel) {

    }

    @Override
    public void delete(PackageListModel.DataBean packageListModel) {

    }

    @Override
    public void onRefresh() {
        callPackageList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
