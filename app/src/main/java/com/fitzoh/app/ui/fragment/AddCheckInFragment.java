package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fitzoh.app.model.CheckinResponse;
import com.google.gson.Gson;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.CheckInFormLabelAdapter;
import com.fitzoh.app.adapter.CheckInFormListAdapter;
import com.fitzoh.app.adapter.FitnessCenterImageAdapter;
import com.fitzoh.app.databinding.FragmentAddCheckInBinding;
import com.fitzoh.app.model.CheckInFormListModel;
import com.fitzoh.app.model.CheckInLabelModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateClientProfile;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateImage;

/**
 * A simple {@link Fragment} subclass.
 */
public class
AddCheckInFragment extends BaseFragment implements View.OnClickListener, SingleCallback, FitnessCenterImageAdapter.RemoveImageListener, CheckInFormLabelAdapter.CheckInFormListener {

    private static final int RECORD_REQUEST_CODE = 101;
    FragmentAddCheckInBinding mBinding;
    public static final int PICK_IMAGES = 1;
    private ArrayList<String> imagesEncodedList = new ArrayList<>();
    private String userId, userAccessToken;
    private FitnessCenterImageAdapter adapter;
    CheckInFormListAdapter recyclerViewAdapter;
    List<CheckInLabelModel> checkInLabelModelList = new ArrayList<>();

    public AddCheckInFragment() {
        // Required empty public constructor
    }

    public static AddCheckInFragment newInstance(Bundle bundle) {
        AddCheckInFragment fragment = new AddCheckInFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_add_check_in, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.addCheckIn.txtEmptyText.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
       /* mBinding.addCheckIn.swipeContainer.setOnRefreshListener(this);
        mBinding.addCheckIn.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));*/
        Utils.setImageBackground(mActivity, mBinding.addCheckIn.imgSelect, R.drawable.ic_plus_icon);
        Utils.getShapeGradient(mActivity, mBinding.btnUpload);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        adapter = new FitnessCenterImageAdapter(new ArrayList<>(), this);
        recyclerViewAdapter = new CheckInFormListAdapter(mActivity, this);
        mBinding.addCheckIn.recyclerViewList.setAdapter(recyclerViewAdapter);
        makeRequest();
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Add Check In");
        clickEvent();
        getCheckInData();
        return mBinding.getRoot();
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE);
    }

    private void getCheckInData() {
    //    mBinding.addCheckIn.swipeContainer.setRefreshing(false);
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getCheckInForm()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.layoutProfile, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private boolean validation() {
        /*if (imagesEncodedList == null || imagesEncodedList.size() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please Select Gallery Images", Snackbar.LENGTH_LONG);
            return false;
        } else*/
        if (imagesEncodedList.size() > 5) {
            showSnackBar(mBinding.layoutProfile, "You can add maximum 5 photos.", Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private void clickEvent() {
        mBinding.btnUpload.setOnClickListener(this);
        mBinding.addCheckIn.imgSelect.setOnClickListener(this);
    }

    private void startImageSelection() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGES);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        if (requestCode == PICK_IMAGES) {
            if (resultCode == RESULT_OK) {
                if (data.getClipData() != null) {
                    adapter.arrayList = new ArrayList<>();
                    imagesEncodedList = new ArrayList<>();
                    ClipData mClipData = data.getClipData();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        adapter.arrayList.add(uri);
                        Cursor cursor = mActivity.getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);
                        cursor.close();
                    }
                    setAdapter();
                } else {
                    adapter.arrayList = new ArrayList<>();
                    Uri selectedImage = data.getData();

                    Cursor cursor = mContext.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    adapter.arrayList.add(selectedImage);
                    imagesEncodedList.add(picturePath);
                    setAdapter();
                }
            }
        }
    }

    private void setAdapter() {
        adapter = new FitnessCenterImageAdapter(adapter.arrayList, this);
        mBinding.addCheckIn.recyclerView.setAdapter(adapter);

    }

    private void callAPI() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonArray = new JSONArray(new Gson().toJson(checkInLabelModelList));
            jsonObject.put("form_details", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody data = RequestBody.create(MediaType.parse("text/plain"), jsonObject.toString());
        RequestBody delete_ids = RequestBody.create(MediaType.parse("text/plain"), "[]");

        List<MultipartBody.Part> parts = new ArrayList<>();
        if (imagesEncodedList != null) {
            for (int i = 0; i < imagesEncodedList.size(); i++) {
                File file = new File(imagesEncodedList.get(i));
                byte[] uploadBytes = Utils.loadImageFromStorage(imagesEncodedList.get(i));
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
                //RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part photo = MultipartBody.Part.createFormData("image[" + i + "]", file.getName(), reqFile);
                parts.add(photo);
            }
        }

        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveCheckInForm(data, delete_ids)
                , getCompositeDisposable(), updateClientProfile, this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSelect:
                mBinding.addCheckIn.txtEmptyText.setVisibility(View.GONE);
                startImageSelection();
                break;
            case R.id.btn_upload:
                if (validation()) {
                    callAPI();
                /*for (int i = 0; i < checkInLabelModelList.size(); i++) {
                    CheckInLabelModel check = checkInLabelModelList.get(i);
                    Log.e("CheckInLabelModel: ", "check" + check.getLable());
                    for (int j = 0; j < check.getSublable().size(); j++) {
                        Log.e("getSublable: ", "check" + check.getSublable().get(j).getLable());
                        Log.e("getAttributes0: ", "check" + check.getSublable().get(j).getAttributes().get(0).getValue());
                        Log.e("getAttributes1: ", "check" + check.getSublable().get(j).getAttributes().get(1).getValue());

                    }
                }*/
                }
                break;
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CheckInFormListModel checkInFormListModel = (CheckInFormListModel) o;
                if (checkInFormListModel != null && checkInFormListModel.getStatus() == AppConstants.SUCCESS && checkInFormListModel.getData() != null && checkInFormListModel.getData().getForm_details() != null && checkInFormListModel.getData().getForm_details().size() > 0) {
                    checkInLabelModelList = checkInFormListModel.getData().getForm_details();
                    recyclerViewAdapter.addDataToAdapter(checkInFormListModel.getData().getForm_details());
                } else {
                    setAdapter();
                }
                break;
            case updateClientProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CheckinResponse commonApiResponse = (CheckinResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    /*startActivity(new Intent(mActivity, TransformationAddActivity.class));
                    getActivity().finish();*/
                    callImageUploadApi(commonApiResponse.getData().getId());
                } else {
                    showSnackBar(mBinding.layoutProfile, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case updateImage:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonResponse = (CommonApiResponse) o;
                if (commonResponse.getStatus() == AppConstants.SUCCESS) {
                    /*startActivity(new Intent(mActivity, TransformationAddActivity.class));
                    getActivity().finish();*/
                    mActivity.onBackPressed();
                } else {
                    showSnackBar(mBinding.layoutProfile, commonResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    private void callImageUploadApi(int id) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody client_id = RequestBody.create(MediaType.parse("text/plain"), "" + id);

        List<MultipartBody.Part> parts = new ArrayList<>();
        if (imagesEncodedList != null) {
            for (int i = 0; i < imagesEncodedList.size(); i++) {
                File file = new File(imagesEncodedList.get(i));
                byte[] uploadBytes = Utils.loadImageFromStorage(imagesEncodedList.get(i));
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
                //RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part photo = MultipartBody.Part.createFormData("image[" + i + "]", file.getName(), reqFile);
                parts.add(photo);
            }
        }

        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).updateCheckinformImage(client_id, parts)
                , getCompositeDisposable(), updateImage, this);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutProfile, throwable.getMessage() == null ? "onFailure" : throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void removeImage(int position) {
        adapter.arrayList.remove(position);
        imagesEncodedList.remove(position);
        setAdapter();
    }

    @Override
    public void setData(int main, int position, EditText editText) {
        CheckInLabelModel check = checkInLabelModelList.get(main);
        List<CheckInLabelModel.SublableBean> sublableBeans = check.getSublable();
        List<CheckInLabelModel.SublableBean.AttributesBean> attributesBeans = sublableBeans.get(position).getAttributes();
        if (editText.getId() == R.id.edtLabelOne)
            attributesBeans.get(0).setValue(editText.getText().toString());
        if (editText.getId() == R.id.edtLabelTwo)
            attributesBeans.get(1).setValue(editText.getText().toString());
        CheckInLabelModel.SublableBean model = sublableBeans.get(position);
        model.setAttributes(attributesBeans);
        sublableBeans.set(position, model);
        check.setSublable(sublableBeans);
        checkInLabelModelList.set(main, check);
    }

  /*  @Override
    public void onRefresh() {
        getCheckInData();
    }*/
}
