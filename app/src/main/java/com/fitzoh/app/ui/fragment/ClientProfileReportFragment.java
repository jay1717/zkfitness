package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentClientProfileReportBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.WorkoutReportModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityAchievement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addDiet;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.reportWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientProfileReportFragment extends BaseFragment implements SingleCallback {


    FragmentClientProfileReportBinding mBinding;
    Typeface typeface;
    String userId, userAccessToken;
    HashMap<Integer, String> numMap;
    HashMap<Integer, String> numMapDiet;
    HashMap<Integer, String> numMapStrength;
    ArrayList<Entry> valuesWorkout = new ArrayList<Entry>();
    ArrayList<Entry> valuesDiet = new ArrayList<Entry>();
    ArrayList<Entry> valuesStrength = new ArrayList<Entry>();
    private static int clientId;
    private ClientListModel.DataBean data = null;

    //   ArrayList<Entry> valuesWorkout;


    public static ClientProfileReportFragment newInstance() {
        ClientProfileReportFragment fragment = new ClientProfileReportFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.reports));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            data = (ClientListModel.DataBean) getArguments().getSerializable("data");
            clientId = data.getId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_profile_report, container, false);
        typeface = ResourcesCompat.getFont(mContext, R.font.avenirnextltpro_medium);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        callChartDataWorkout(clientId);
        callChartDataDiet();
        callChartDataStrength();
//        setLineChartWorkout();
        mBinding.layoutReport.txtAchivements.setOnClickListener(view -> startActivity(new Intent(getActivity(), ActivityAchievement.class)));
        return mBinding.getRoot();

    }

    private void callChartDataWorkout(int clientId) {
        if (com.fitzoh.app.utils.Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getReportData(clientId, "daily")
                    , getCompositeDisposable(), reportWorkout, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callChartDataDiet() {
        if (com.fitzoh.app.utils.Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getDietReportData(clientId, "daily")
                    , getCompositeDisposable(), addDiet, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callChartDataStrength() {
        if (com.fitzoh.app.utils.Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getStrengthReportData(clientId, "daily")
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setLineChartWorkout() {
       /* numMap = new HashMap<>();
        numMap.put(0, "");
        numMap.put(1, "exe2");
        numMap.put(2, "exe3");*/
        /*numMap.put(3, "exe4");
        numMap.put(4, "exe5");
        numMap.put(5, "exe6");
        numMap.put(6, "exe7");
        numMap.put(7, "exe8");
        numMap.put(8, "exe9");
        numMap.put(9, "exe10");
        numMap.put(10, "exe11");*/


        mBinding.layoutReport.chartWorkout.setDrawGridBackground(false);
        mBinding.layoutReport.chartWorkout.setDrawBorders(false);
        mBinding.layoutReport.chartWorkout.getLegend().setEnabled(false);
        mBinding.layoutReport.chartWorkout.setAutoScaleMinMaxEnabled(false);
        mBinding.layoutReport.chartWorkout.setTouchEnabled(true);
        mBinding.layoutReport.chartWorkout.setDragEnabled(false);
        mBinding.layoutReport.chartWorkout.setScaleEnabled(false);
        mBinding.layoutReport.chartWorkout.setPinchZoom(false);
        mBinding.layoutReport.chartWorkout.setDoubleTapToZoomEnabled(false);
        mBinding.layoutReport.chartWorkout.getAxisRight().setEnabled(false);
        mBinding.layoutReport.chartWorkout.getDescription().setEnabled(false);
        mBinding.layoutReport.chartWorkout.setXAxisRenderer(new CustomXAxisRenderer(mBinding.layoutReport.chartWorkout.getViewPortHandler(), mBinding.layoutReport.chartWorkout.getXAxis(), mBinding.layoutReport.chartWorkout.getTransformer(YAxis.AxisDependency.LEFT)));
        final YAxis yAxis = mBinding.layoutReport.chartWorkout.getAxisLeft();
//        yAxis.setLabelCount(6, true);
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setValueFormatter(new DefaultAxisValueFormatter(1));
        yAxis.setTextColor(Color.GRAY);
        yAxis.setTextSize(5f); //not in your original but added
        yAxis.setTypeface(typeface);
        yAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));
        yAxis.setAxisLineColor(Color.TRANSPARENT);
        yAxis.setAxisMinimum(0); //not in your original but added

        final XAxis xAxis = mBinding.layoutReport.chartWorkout.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); //changed to match your spec
        xAxis.setTextColor(Color.GRAY);
        xAxis.disableGridDashedLine();
        xAxis.setDrawGridLines(true);
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));

        xAxis.setAxisLineColor(Color.TRANSPARENT);
        xAxis.setValueFormatter((value, axis) -> numMap.get((int) value));
        xAxis.setLabelCount(valuesWorkout.size());
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setGranularityEnabled(true);
        setDataWrokout();
        xAxis.setLabelRotationAngle(-45);
        //xAxis.setSpaceMin(10f); //DON'T NEED THIS!!
        // setDataWrokout(10, 16);
    }

    public class CustomXAxisRenderer extends XAxisRenderer {
        public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
            super(viewPortHandler, xAxis, trans);
        }

        @Override
        protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
            Utils.drawXAxisValue(c, formattedLabel, x, y, mAxisLabelPaint, anchor, angleDegrees);
        }
    }

    private void setDataWrokout() {


        /*valuesWorkout = new ArrayList<Entry>();
        valuesWorkout.add(new Entry(0, 0.0f, null));*/
        // valuesWorkout.add(new Entry(count, range, null));

        /*valuesWorkout = new ArrayList<Entry>();
        valuesWorkout.add(new Entry(0, 2.5f, null));
        valuesWorkout.add(new Entry(1, 2.3f, null));
        valuesWorkout.add(new Entry(2, 3f, null));*/
        /*valuesWorkout.add(new Entry(3, 3.8f, null));
        valuesWorkout.add(new Entry(4, 4.2f, null));
        valuesWorkout.add(new Entry(5, 3.8f, null));
        valuesWorkout.add(new Entry(6, 3.2f, null));
        valuesWorkout.add(new Entry(7, 3.8f, null));
        valuesWorkout.add(new Entry(8, 4.8f, null));
        valuesWorkout.add(new Entry(9, 4.2f, null));
        valuesWorkout.add(new Entry(10, 4f, null));*/

        LineDataSet set1;
        Log.e("valuesWorkout: ", "size" + valuesWorkout.size());
        Log.e("numMap: ", "size" + numMap.size());
        if (mBinding.layoutReport.chartWorkout.getData() != null &&
                mBinding.layoutReport.chartWorkout.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.layoutReport.chartWorkout.getData().getDataSetByIndex(0);
            set1.setValues(valuesWorkout);
            mBinding.layoutReport.chartWorkout.getData().notifyDataChanged();
            mBinding.layoutReport.chartWorkout.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(valuesWorkout, "DataSet 1");
//            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(0);
            set1.setDrawCircles(false);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            set1.setValueTextSize(5f);
            set1.setValueTypeface(typeface);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
//                Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.shape_gradient_chart);
                GradientDrawable gradientDrawable = com.fitzoh.app.utils.Utils.getShapeGradient(mActivity, 0);
                set1.setFillDrawable(gradientDrawable);
            } else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            mBinding.layoutReport.chartWorkout.setExtraBottomOffset(30);
            // set data
            mBinding.layoutReport.chartWorkout.setData(data);
            mBinding.layoutReport.chartWorkout.invalidate();
        }


    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case reportWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutReportModel workoutReportModel = (WorkoutReportModel) o;
                if (workoutReportModel != null && workoutReportModel.getData() != null && workoutReportModel.getData().size() > 0) {
                    setAllValues(workoutReportModel.getData());
                }
                break;
            case addDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutReportModel dietModel = (WorkoutReportModel) o;
                if (dietModel != null && dietModel.getData() != null && dietModel.getData().size() > 0) {
                    setAllValuesDiet(dietModel.getData());
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutReportModel strengthReportModel = (WorkoutReportModel) o;
                if (strengthReportModel != null && strengthReportModel.getData() != null && strengthReportModel.getData().size() > 0) {
                    setAllValuesStrength(strengthReportModel.getData());
                }
                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    private void setAllValues(List<WorkoutReportModel.DataBean> data) {
        numMap = new HashMap<>();
        valuesWorkout = new ArrayList<Entry>();
        for (int i = 0; i < data.size(); i++) {
            if (data.size() == 1) {
                numMap.put(i, "");
                numMap.put(i + 1, data.get(i).getKey());
                valuesWorkout.add(new Entry(i, 0, null));
                valuesWorkout.add(new Entry(i + 1, data.get(i).getValue(), null));
            } else {
                numMap.put(i, data.get(i).getKey());
                valuesWorkout.add(new Entry(i, data.get(i).getValue(), null));
            }
        }
        setLineChartWorkout();
    }

    private void setAllValuesDiet(List<WorkoutReportModel.DataBean> data) {
        numMapDiet = new HashMap<>();
        valuesDiet = new ArrayList<Entry>();
        for (int i = 0; i < data.size(); i++) {
            if (data.size() == 1) {
                numMapDiet.put(i, "");
                numMapDiet.put(i + 1, data.get(i).getKey());
                valuesDiet.add(new Entry(i, 0, null));
                valuesDiet.add(new Entry(i + 1, data.get(i).getValue(), null));
            } else {
                numMapDiet.put(i, data.get(i).getKey());
                valuesDiet.add(new Entry(i, data.get(i).getValue(), null));
            }
        }
        setLineChartDiet();
    }

    private void setAllValuesStrength(List<WorkoutReportModel.DataBean> data) {
        numMapStrength = new HashMap<>();
        valuesStrength = new ArrayList<Entry>();
        for (int i = 0; i < data.size(); i++) {
            if (data.size() == 1) {
                numMapStrength.put(i, "");
                numMapStrength.put(i + 1, data.get(i).getKey());
                valuesStrength.add(new Entry(i,0, null));
                valuesStrength.add(new Entry(i + 1, data.get(i).getValue(), null));
            } else {
                numMapStrength.put(i, data.get(i).getKey());
                valuesStrength.add(new Entry(i, data.get(i).getValue(), null));
            }
        }
        setLineChartStrength();
    }


    private void setLineChartDiet() {
        mBinding.layoutReport.chartDiet.setDrawGridBackground(false);
        mBinding.layoutReport.chartDiet.setDrawBorders(false);
        mBinding.layoutReport.chartDiet.getLegend().setEnabled(false);
        mBinding.layoutReport.chartDiet.setAutoScaleMinMaxEnabled(false);
        mBinding.layoutReport.chartDiet.setTouchEnabled(true);
        mBinding.layoutReport.chartDiet.setDragEnabled(false);
        mBinding.layoutReport.chartDiet.setScaleEnabled(false);
        mBinding.layoutReport.chartDiet.setPinchZoom(false);
        mBinding.layoutReport.chartDiet.setDoubleTapToZoomEnabled(false);
        mBinding.layoutReport.chartDiet.getAxisRight().setEnabled(false);
        mBinding.layoutReport.chartDiet.getDescription().setEnabled(false);

        final YAxis yAxis = mBinding.layoutReport.chartDiet.getAxisLeft();
//        yAxis.setLabelCount(6, true);
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setValueFormatter(new DefaultAxisValueFormatter(1));
        yAxis.setTextColor(Color.GRAY);
        yAxis.setTextSize(5f); //not in your original but added
        yAxis.setTypeface(typeface);
        yAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));
        yAxis.setAxisLineColor(Color.TRANSPARENT);
        yAxis.setAxisMinimum(0); //not in your original but added

        final XAxis xAxis = mBinding.layoutReport.chartDiet.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); //changed to match your spec
        xAxis.setTextColor(Color.GRAY);
        xAxis.disableGridDashedLine();
        xAxis.setDrawGridLines(true);
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));

        xAxis.setAxisLineColor(Color.TRANSPARENT);
        xAxis.setValueFormatter((value, axis) -> numMapDiet.get((int) value));
        xAxis.setLabelCount(valuesDiet.size());
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setLabelRotationAngle(-45);
        xAxis.setGranularityEnabled(true);
        setDataDiet();
    }

    private void setDataDiet() {
        LineDataSet set1;
        if (mBinding.layoutReport.chartDiet.getData() != null &&
                mBinding.layoutReport.chartDiet.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.layoutReport.chartDiet.getData().getDataSetByIndex(0);
            set1.setValues(valuesDiet);
            mBinding.layoutReport.chartDiet.getData().notifyDataChanged();
            mBinding.layoutReport.chartDiet.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(valuesDiet, "DataSet 1");
//            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(0);
            set1.setDrawCircles(false);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            set1.setValueTextSize(5f);
            set1.setValueTypeface(typeface);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
//                Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.shape_gradient_chart);
                GradientDrawable gradientDrawable = com.fitzoh.app.utils.Utils.getShapeGradient(mActivity, 0);
                set1.setFillDrawable(gradientDrawable);
            } else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            mBinding.layoutReport.chartDiet.setExtraBottomOffset(30);
            // set data
            mBinding.layoutReport.chartDiet.setData(data);
            mBinding.layoutReport.chartDiet.invalidate();
        }


    }

    private void setLineChartStrength() {
        mBinding.layoutReport.chartStrength.setDrawGridBackground(false);
        mBinding.layoutReport.chartStrength.setDrawBorders(false);
        mBinding.layoutReport.chartStrength.getLegend().setEnabled(false);
        mBinding.layoutReport.chartStrength.setAutoScaleMinMaxEnabled(false);
        mBinding.layoutReport.chartStrength.setTouchEnabled(true);
        mBinding.layoutReport.chartStrength.setDragEnabled(false);
        mBinding.layoutReport.chartStrength.setScaleEnabled(false);
        mBinding.layoutReport.chartStrength.setPinchZoom(false);
        mBinding.layoutReport.chartStrength.setDoubleTapToZoomEnabled(false);
        mBinding.layoutReport.chartStrength.getAxisRight().setEnabled(false);
        mBinding.layoutReport.chartStrength.getDescription().setEnabled(false);

        final YAxis yAxis = mBinding.layoutReport.chartStrength.getAxisLeft();
//        yAxis.setLabelCount(6, true);
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setValueFormatter(new DefaultAxisValueFormatter(1));
        yAxis.setTextColor(Color.GRAY);
        yAxis.setTextSize(5f); //not in your original but added
        yAxis.setTypeface(typeface);
        yAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));
        yAxis.setAxisLineColor(Color.TRANSPARENT);
        yAxis.setAxisMinimum(0); //not in your original but added

        final XAxis xAxis = mBinding.layoutReport.chartStrength.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); //changed to match your spec
        xAxis.setTextColor(Color.GRAY);
        xAxis.disableGridDashedLine();
        xAxis.setDrawGridLines(true);
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));

        xAxis.setAxisLineColor(Color.TRANSPARENT);
        xAxis.setValueFormatter((value, axis) -> numMapStrength.get((int) value));
        xAxis.setLabelCount(valuesStrength.size());
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setLabelRotationAngle(-45);
        setDataStrength();
    }

    private void setDataStrength() {
        LineDataSet set1;
        if (mBinding.layoutReport.chartStrength.getData() != null &&
                mBinding.layoutReport.chartStrength.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.layoutReport.chartStrength.getData().getDataSetByIndex(0);
            set1.setValues(valuesStrength);
            mBinding.layoutReport.chartStrength.getData().notifyDataChanged();
            mBinding.layoutReport.chartStrength.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(valuesStrength, "DataSet 1");
//            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(0);
            set1.setDrawCircles(false);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            set1.setValueTextSize(5f);
            set1.setValueTypeface(typeface);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
//                Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.shape_gradient_chart);
                GradientDrawable gradientDrawable = com.fitzoh.app.utils.Utils.getShapeGradient(mActivity, 0);
                set1.setFillDrawable(gradientDrawable);
            } else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            mBinding.layoutReport.chartStrength.setExtraBottomOffset(30);
            // set data
            mBinding.layoutReport.chartStrength.setData(data);
            mBinding.layoutReport.chartStrength.invalidate();
        }
    }
}
