package com.fitzoh.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentSubscriptionDetailBinding;
import com.fitzoh.app.model.ProfileGymSubscriptionModel;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.SessionManager;

import org.json.JSONObject;

public class SubscriptionDetailActivity extends BaseActivity implements PaymentResultListener {

    FragmentSubscriptionDetailBinding mBinding;
    ProfileGymSubscriptionModel.DataBean dataBean;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    String userId, userAccessToken, photoPath;
    private Snackbar snackbar;
    public SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.fragment_subscription_detail);

        Intent intent = getIntent();
        if (null != intent) {

            dataBean = (ProfileGymSubscriptionModel.DataBean) getIntent().getSerializableExtra("dataBeanClass");
            photoPath = getIntent().getStringExtra("photoPath");
            session = new SessionManager(getApplicationContext());

            mBinding.layout.txtPackageNameValue.setText(dataBean.getName());
            mBinding.layout.txtPackageDescriptionValue.setText(dataBean.getAbout_subscriptions());
            mBinding.layout.txtPackageDiscountValue.setText("$" + String.valueOf(dataBean.getDiscount_price()));
            mBinding.layout.txtPackagePriceValue.setText("$" + String.valueOf(dataBean.getPrice()));
            mBinding.layout.txtPackagePriceValue.setPaintFlags(mBinding.layout.txtPackagePriceValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mBinding.layout.txtPackageWeekValue.setText(String.valueOf(dataBean.getNo_of_months()));
            mBinding.toolbar.tvTitle.setTextColor(getResources().getColor(R.color.gray));
            mBinding.toolbar.tvTitle.setText(dataBean.getName());

            if (photoPath.equals("") || photoPath == null || photoPath.isEmpty())

                mBinding.layout.imgMain.setImageDrawable(getResources().getDrawable(R.drawable.workout_placeholder));

        } else {
            Glide.with(getApplicationContext())
                    .load(photoPath)
                    .into(mBinding.layout.imgMain);
        }
        mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        mBinding.toolbar.imgBack.setOnClickListener(view -> onBackPressed());
        mBinding.layout.btnPurchase.setOnClickListener(view -> checkout());


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    private void checkout() {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher);
        Double price = Double.valueOf(dataBean.getDiscount_price());
        final Activity activity = this;


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Fitzoh");
            options.put("description", "Subscription charge");
            options.put("currency", "INR");
            options.put("amount", (price.intValue()) * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("email", session.getAuthorizedUser(activity).getEmail());

            preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());

            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    public void disableScreen(boolean disable) {
        if (disable) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");
    }

    public void purchaseSubscription(String transaction_id, String payment_method) {
       /* mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getApplicationContext(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchaseSubscription(
                subscriptionData.get(subscriptionListAdapter.selectionPostition).getId(), transaction_id, payment_method, userId)
                , getCompositeDisposable(), single, this);*/
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.getRoot(), s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    public void showSnackBar(View view, String msg, int LENGTH) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        snackbar.show();
    }
}
