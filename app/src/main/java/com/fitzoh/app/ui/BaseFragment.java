package com.fitzoh.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.interfaces.OnSnackBarActionListener;
import com.fitzoh.app.ui.activity.NavigationClientMainActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import io.reactivex.disposables.CompositeDisposable;


public class BaseFragment extends Fragment {

    public Context mContext;
    public Activity mActivity;
    public CompositeDisposable compositeDisposable;
    public TextView title;
    public SessionManager session;
    private Snackbar snackbar;

    //    private ProgressDialog dialog;
    public setPermissionListener permissionListener;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mActivity = getActivity();
        session = new SessionManager(mContext);
    }

    public void setupToolBar(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(Title != null ? Title : "");
    }

    public void setupToolBarWithMenu(Toolbar toolbar) {
        setupToolBarWithMenu(toolbar, null);
    }

    public void setupToolBarWithMenu(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(Utils.getBackgroundDrawable(mActivity, new TextView(mActivity), R.drawable.ic_menu));
        }
        toolbar.setNavigationOnClickListener(view -> {
            if (session.getRollId() == 1) {
                ((NavigationClientMainActivity) mActivity).mBinding.drawer.openDrawer(GravityCompat.START);
            } else
                ((NavigationMainActivity) mActivity).mBinding.drawer.openDrawer(GravityCompat.START);
        });
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(Title != null ? Title : "");
    }

    public void setupWhiteToolBarWithMenu(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white);
        }
        toolbar.setNavigationOnClickListener(view -> {
            if (session.getRollId() == 1) {
                ((NavigationClientMainActivity) mActivity).mBinding.drawer.openDrawer(GravityCompat.START);
            } else
                ((NavigationMainActivity) mActivity).mBinding.drawer.openDrawer(GravityCompat.START);
        });
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(Title != null ? Title : "");
    }


    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    public void setupToolBarWithBackArrow(Toolbar toolbar) {
        setupToolBarWithBackArrow(toolbar, null);
    }

    public void setupToolBarWithBackArrow(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(Utils.getBackgroundDrawable(mActivity, new TextView(mActivity), R.drawable.ic_back));
        }
        toolbar.setNavigationOnClickListener(view -> mActivity.onBackPressed());
        title = toolbar.findViewById(R.id.tvTitle);
        title.setText(Title != null ? Title : "");
    }

    public void setupToolBarWithCloseWhite(Toolbar toolbar, @Nullable String Title) {
        ActionBar actionBar;

        ((BaseActivity) mActivity).setSupportActionBar(toolbar);
        actionBar = ((BaseActivity) mActivity).getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_vector_close_white);
        }
        toolbar.setNavigationOnClickListener(view -> mActivity.onBackPressed());
        title = toolbar.findViewById(R.id.tvTitle);
        title.setTextColor(getResources().getColor(R.color.white));
        title.setText(Title != null ? Title : "");
    }

    public void updateToolBarTitle(@Nullable String Title) {
        title.setText(Title != null ? Title : mContext.getResources().getString(R.string.app_name));
    }

    // Set false for Tab Back press management. It will manage from MainActivity
    public boolean onBackPressed() {
        return false;
    }

    public void showSnackBar(View view, String msg, int LENGTH) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        snackbar.show();
    }

    public void showSnackBar(View view, String msg, int LENGTH, String action, final OnSnackBarActionListener actionListener) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        snackbar.setActionTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        if (actionListener != null) {
            snackbar.setAction(action, view1 -> {
                snackbar.dismiss();
                actionListener.onAction();
            });
        }
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        snackbar.show();
    }

    public void setEdittextError(TextView textView, String string) {
        Animation animationFadeIn = AnimationUtils.loadAnimation(mActivity, R.anim.fade_in);
        textView.startAnimation(animationFadeIn);
        textView.setTextColor(Color.RED);
        textView.setText(string);
        textView.setVisibility(View.VISIBLE);
        textView.requestFocus();
    }

    public void showSoftKeyboard(EditText editText) {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void hideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            if (mActivity.getWindow().getCurrentFocus() != null)
                imm.hideSoftInputFromWindow(mActivity.getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    public ProgressDialog showProgressBar() {
        return showProgressBar(null);
    }

    public ProgressDialog showProgressBar(String message) {
        if (dialog == null) dialog = new ProgressDialog(mContext, message);
        return dialog;
    }

    public void hideProgressBar() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
*/


    public void showPermissionSettingDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.need_permission);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.app_settings, (dialog, which) -> {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + mContext.getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            startActivity(intent);
            dialog.dismiss();
        });
        builder.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss());
        builder.create().show();
    }

    public void requestAppPermissions(final String[] requestedPermissions, final int requestCode,
                                      BaseFragment.setPermissionListener listener) {
        this.permissionListener = listener;
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (String permission : requestedPermissions) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(mActivity, permission);
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(requestedPermissions, requestCode);
        } else {
            if (permissionListener != null) permissionListener.onPermissionGranted(requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(mContext, permission) == PackageManager.PERMISSION_GRANTED) {
                if (permissionListener != null) permissionListener.onPermissionGranted(requestCode);
                break;
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
                if (permissionListener != null) permissionListener.onPermissionDenied(requestCode);
                break;
            } else {
                if (permissionListener != null) {
                    permissionListener.onPermissionNeverAsk(requestCode);
                    break;
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (snackbar != null && snackbar.isShown()) snackbar.dismiss();
    }

    public interface setPermissionListener {
        void onPermissionGranted(int requestCode);

        void onPermissionDenied(int requestCode);

        void onPermissionNeverAsk(int requestCode);
    }
}