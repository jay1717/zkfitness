package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.history.PNHistoryItemResult;
import com.pubnub.api.models.consumer.history.PNHistoryResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.LiveClientStatusAdapter;
import com.fitzoh.app.databinding.FragmentLiveClientsBinding;
import com.fitzoh.app.model.LiveClientDetailModel;
import com.fitzoh.app.model.LiveClientsModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class LiveClientsFragment extends BaseFragment implements SingleCallback {


    String userId, userAccessToken;
    FragmentLiveClientsBinding mBinding;
    LiveClientsModel.DataBean dataBean;

    List<LiveClientDetailModel.DataBean> liveClientModel;
    LiveClientStatusAdapter liveClientAdapter;
    private boolean isGym = false;

    public LiveClientsFragment() {
        // Required empty public constructor
    }

    public static LiveClientsFragment newInstance() {
        return new LiveClientsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (LiveClientsModel.DataBean) getArguments().getSerializable("data");
            isGym = getArguments().getBoolean("isGym", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_live_clients, container, false);

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        initLiveClientSocket(session.getAuthorizedUser(getActivity()).getId());
        setClickEvents();
        prepareLayout();

        return mBinding.getRoot();

    }

/*
    private void getLiveClientDetails() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).trainingSession(Integer.parseInt(userId))
                , getCompositeDisposable(), liveClientdetail, this);

    }*/

    private void prepareLayout() {
        Utils.setImageBackground(mActivity, mBinding.layoutLiveClient.imgCall, R.drawable.ic_call_icon);
        Utils.setImageBackground(mActivity, mBinding.layoutLiveClient.imgMenu, R.drawable.ic_actionmenu_icon);
        Utils.setImageBackground(mActivity, mBinding.layoutLiveClient.imgMsg, R.drawable.ic_chat_line);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.toolbar.tvTitle.setText(dataBean.getName());
        Utils.setImage(getActivity(), mBinding.layoutLiveClient.imgUser, dataBean.getPhoto());

//        mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_white));
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.tvTitle.setTextColor(getResources().getColor(R.color.white));
        //   setupToolBarWithBackArrowWhite(mBinding.toolbar.toolbar, "John Doe");
        liveClientModel = new ArrayList<>();
        mBinding.layoutLiveClient.recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.layoutLiveClient.recyclerview.setAdapter(new LiveClientStatusAdapter(getActivity(), liveClientModel));

        mBinding.layoutLiveClient.imgMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getActivity(), ConversationActivity.class);
                intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(dataBean.getId()));
                intent1.putExtra(ConversationUIService.DISPLAY_NAME, dataBean.getName()); //put it for displaying the title.
                intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
                getActivity().startActivity(intent1);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callInquiryList();
            else {
                showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }*/
    }

    private void callInquiryList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.layoutLiveClient.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            if (isGym)
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getGymLiveClientDetail(dataBean.getId())
                        , getCompositeDisposable(), single, this);
            else
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getLiveClientDetail(dataBean.getId())
                        , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.layoutliveClients, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setClickEvents() {

        mBinding.toolbar.imgBack.setOnClickListener(v -> getActivity().onBackPressed());


        mBinding.layoutLiveClient.imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mBinding.layoutLiveClient.imgCall.getVisibility() == View.INVISIBLE) {
                    mBinding.layoutLiveClient.imgCall.setVisibility(View.VISIBLE);
                    Animation animFadeInCall = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                    mBinding.layoutLiveClient.imgCall.startAnimation(animFadeInCall);

                    mBinding.layoutLiveClient.imgMsg.setVisibility(View.VISIBLE);
                    Animation animFadeInMsg = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
                    mBinding.layoutLiveClient.imgMsg.startAnimation(animFadeInMsg);
                } else {

                    Animation animFadeInCall = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
                    mBinding.layoutLiveClient.imgCall.startAnimation(animFadeInCall);
                    Animation animFadeInMsg = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
                    mBinding.layoutLiveClient.imgMsg.startAnimation(animFadeInMsg);

                    mBinding.layoutLiveClient.imgMsg.setVisibility(View.INVISIBLE);
                    mBinding.layoutLiveClient.imgCall.setVisibility(View.INVISIBLE);
                }

            }
        });

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.layoutLiveClient.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                liveClientModel = new ArrayList<>();
                LiveClientDetailModel liveClientDetailModel = (LiveClientDetailModel) o;
                if (liveClientDetailModel.getStatus() == AppConstants.SUCCESS) {

                    if (liveClientDetailModel != null) {
                        liveClientModel.addAll(liveClientDetailModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (liveClientModel.size() == 0) {
                            mBinding.layoutLiveClient.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutLiveClient.recyclerview.setVisibility(View.GONE);
                            mBinding.layoutLiveClient.view.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutLiveClient.recyclerview.setVisibility(View.VISIBLE);
                            mBinding.layoutLiveClient.imgNoData.setVisibility(View.GONE);
                            liveClientAdapter = new LiveClientStatusAdapter(getActivity(), liveClientModel);
                            mBinding.layoutLiveClient.recyclerview.setAdapter(liveClientAdapter);
                        }

                    } else
                        showSnackBar(mBinding.layoutliveClients, liveClientDetailModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.layoutLiveClient.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    private void initLiveClientSocket(int trainer_id) {
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        String channelName = String.valueOf(trainer_id);

        pubnub.addListener(new SubscribeCallback() {
                               @Override
                               public void status(PubNub pubnub, PNStatus status) {

                               }

                               @Override
                               public void message(PubNub pubnub, PNMessageResult message) {

                                   String messagePublisher = message.getPublisher();
                                   /*System.out.println("Message 2publisher: " + messagePublisher);
                                   System.out.println("Message 2Payload: " + message.getMessage());
                                   System.out.println("Message 2Subscription: " + message.getSubscription());
                                   System.out.println("Message 2Channel: " + message.getChannel());
                                   System.out.println("Message 2timetoken: " + message.getTimetoken());*/
                                   System.out.println("Message 2publisher: " + messagePublisher);

                                   if (message.getMessage() != null) {
                                       setDataToLiveClient(message.getMessage());
                                       Log.e("message: ", "else");
                                       setAdapter();
                                   }

                               }

                               @Override
                               public void presence(PubNub pubnub, PNPresenceEventResult presence) {

                               }
                           }
        );
        pubnub.subscribe()
                .channels(Arrays.asList(channelName)) // subscribe to channels
                .execute();
        pubnub.history().channel(channelName).async(new PNCallback<PNHistoryResult>() {
            @Override
            public void onResponse(PNHistoryResult result, PNStatus status) {
                if (!status.isError()) {
                    Log.e("size: ", "" + result.getMessages().size());
                    for (PNHistoryItemResult pnHistoryItemResult : result.getMessages()) {
                        // custom JSON structure for message
                        setDataToLiveClient(pnHistoryItemResult.getEntry());
                    }
                    setAdapter();
                }
            }
        });
    }

    private void setAdapter() {

    }

    private void setDataToLiveClient(JsonElement message) {
        try {
            JSONObject jsonObject = new JSONObject(message.toString());
            Log.e("jsonObject: ", "" + message.toString());
            if (jsonObject.has("email") && jsonObject.has("id") && jsonObject.has("exercises") && jsonObject.getInt("id") == dataBean.getId()) {
                JSONArray jsonArray = jsonObject.getJSONArray("exercises");

                Type listType = new TypeToken<List<LiveClientDetailModel.DataBean>>() {
                }.getType();
                List<LiveClientDetailModel.DataBean> yourList = new Gson().fromJson(jsonArray.toString(), listType);

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (yourList.size() == 0) {
                            mBinding.layoutLiveClient.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutLiveClient.recyclerview.setVisibility(View.GONE);
                            mBinding.layoutLiveClient.view.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutLiveClient.recyclerview.setVisibility(View.VISIBLE);
                            mBinding.layoutLiveClient.imgNoData.setVisibility(View.GONE);
                            liveClientAdapter = new LiveClientStatusAdapter(getActivity(), yourList);
                            mBinding.layoutLiveClient.recyclerview.setAdapter(liveClientAdapter);
                        }
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
