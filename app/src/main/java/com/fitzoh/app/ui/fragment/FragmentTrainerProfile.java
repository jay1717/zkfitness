package com.fitzoh.app.ui.fragment;

import android.content.Context;
import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.contact.AppContactService;
import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.applozic.mobicommons.people.contact.Contact;
import com.bumptech.glide.Glide;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewPagerAdapter;
import com.fitzoh.app.databinding.TrainerProfileBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ClientDocumentViewActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.sendClientRequest;

public class FragmentTrainerProfile extends BaseFragment implements SingleCallback, View.OnClickListener {

    TrainerProfileBinding mBinding;
    String userId, userAccessToken;
    ViewPagerAdapter viewPagerAdapter;
    int trainerId, isGym;
    TrainerProfileModel trainerProfileModel = null;

    public FragmentTrainerProfile() {
        // Required empty public constructor
    }


    public static FragmentTrainerProfile newInstance(Bundle bundle) {
        FragmentTrainerProfile profile = new FragmentTrainerProfile();
        profile.setArguments(bundle);
        return profile;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("isFromNav") && getArguments().getBoolean("isFromNav", false)) {
            mBinding.txtRequest.setVisibility(View.GONE);
            setupWhiteToolBarWithMenu(mBinding.toolbar.toolbar, "");
        } else
            setupToolBarWithCloseWhite(mBinding.toolbar.toolbar, "");

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.trainer_profile, container, false);
        mBinding.mainView.setBackground(Utils.getShapeGradient(mActivity, 90));
        Utils.setImageBackground(mActivity, mBinding.imgChat, R.drawable.ic_chat_icon_profile);
        Utils.setImageBackground(mActivity, mBinding.imgCall, R.drawable.ic_call_white_icon);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

//        Intent intent = getActivity().getIntent();
        if (null != getArguments()) {
            trainerId = getArguments().getInt("trainerId");
            isGym = getArguments().getInt("isGym");
            Log.e("trainerId: ", "" + trainerId);
            Log.e("isGym: ", "" + isGym);
            if (isGym == 0) {
                getTrainerProfile();
//                setUpViewPageCertification(mBinding.viewpager);
//                mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);
            } else {
                getGymProfile();
//                setUpViewPager(mBinding.viewpager);
//                mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);
            }

        }
        mBinding.txtRequest.setOnClickListener(v -> sendRequest());
        mBinding.imgUser.setOnClickListener(this);
        mBinding.imgCall.setOnClickListener(view -> {
            Utils.makeCall(this, trainerProfileModel.getData().getMobile_number());
        });
        mBinding.imgChat.setOnClickListener(view -> {
            Intent intent1 = new Intent(getActivity(), ConversationActivity.class);
            intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(trainerId));
            intent1.putExtra(ConversationUIService.DISPLAY_NAME, trainerProfileModel.getData().getName()); //put it for displaying the title.
            intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
            getActivity().startActivity(intent1);
        });

        return mBinding.getRoot();

    }

    private void sendRequest() {
        if (Utils.isOnline(getActivity())) {
            // mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            //   disableScreen(true);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).sendRequestToTrainer(trainerId)
                    , getCompositeDisposable(), sendClientRequest, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void getTrainerProfile() {
        if (Utils.isOnline(getActivity())) {
            // mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            //   disableScreen(true);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerProfile(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerProfile, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void getGymProfile() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerProfileGym(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerProfile, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setUpViewPager(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragment(FragmentTrainerAboutUs.newInstance(trainerId, isGym), getString(R.string.about));
        viewPagerAdapter.addFragment(FragmentTrainerTransformation.newInstance(trainerId, false), getString(R.string.transformation));
        viewPagerAdapter.addFragment(FragmentTrainerGallery.newInstance(trainerId), getString(R.string.gallery));
        viewPagerAdapter.addFragment(ProfilePackageListFragment.newInstance(trainerId), getString(R.string.gym_packages));
        viewPagerAdapter.addFragment(ProfileGymSubListFragment.newInstance(trainerId, trainerProfileModel.getData().getPhoto()), getString(R.string.request_gym_subscription));
        viewpager.setAdapter(viewPagerAdapter);
    }

    private void setUpViewPageCertification(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragment(FragmentTrainerAboutUs.newInstance(trainerId, isGym), getString(R.string.about));
        viewPagerAdapter.addFragment(FragmentTrainerTransformation.newInstance(trainerId, false), getString(R.string.transformation));
        viewPagerAdapter.addFragment(FragmentTrainerGallery.newInstance(trainerId), getString(R.string.gallery));
        viewPagerAdapter.addFragment(CertificationFragment.newInstance(trainerId, isGym), getString(R.string.certification));
        viewPagerAdapter.addFragment(ProfilePackageListFragment.newInstance(trainerId), getString(R.string.trainer_packages));
        viewpager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case sendClientRequest:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mBinding.txtRequest.setText(R.string.pending);
                    mBinding.txtRequest.setTextColor(getResources().getColor(R.color.start_yellow));
                    mBinding.txtRequest.setClickable(false);
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case getTrainerProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainerProfileModel = (TrainerProfileModel) o;
                if (trainerProfileModel != null) {
                    if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {
                        if (isGym == 0) {
                            setUpViewPageCertification(mBinding.viewpager);
                            mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);
                        } else {
                            setUpViewPager(mBinding.viewpager);
                            mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);
                        }

//                        mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);
                        mBinding.txtTrainerName.setText(trainerProfileModel.getData().getName());
                        mBinding.txtTrainerAddress.setText(trainerProfileModel.getData().getAddress());
                        mBinding.txtTrainerEmail.setText(trainerProfileModel.getData().getEmail());
                      /*  if(trainer.is_request_sent == 0)
                        {
                            if trainer.client_request_status == 2 {
                            btnRequest.setTitle("Request again", for: .normal)
                            btnRequest.isUserInteractionEnabled = true;
                        }else{
                            btnRequest.setTitle("Request", for: .normal)
                            btnRequest.isUserInteractionEnabled = true;
                        }


                        }else if(trainer.client_request_status == 0)
                        {
                            btnRequest.isUserInteractionEnabled = false;
                            btnRequest.setTitle("Pending", for: .normal)
                        }
                        else if(trainer.client_request_status == 1)
                        {
                            btnRequest.setTitle("Accept", for: .normal)
                            btnRequest.isUserInteractionEnabled = false;
                        }
                        else if(trainer.client_request_status == 2)
                        {
                            btnRequest.setTitle("Reject", for: .normal)
                            btnRequest.isUserInteractionEnabled = false;
                        }*/

                        if (trainerProfileModel.getData().getIs_request_sent().equals("0")) {
                            if(trainerProfileModel.getData().getClient_request_status().equals("2")){
                                mBinding.txtRequest.setText(R.string.str_request_again);
                                mBinding.txtRequest.setClickable(true);
                            }
                            else {
                                mBinding.txtRequest.setText(R.string.send_request);
                                mBinding.txtRequest.setClickable(true);
                            }


                        } else if (trainerProfileModel.getData().getClient_request_status().equals("0")){
                            mBinding.txtRequest.setText(R.string.pending);
                            mBinding.txtRequest.setClickable(false);
                            mBinding.txtRequest.setTextColor(getResources().getColor(R.color.start_yellow));

                        }else if (trainerProfileModel.getData().getClient_request_status().equals("1")){
                            mBinding.txtRequest.setText(R.string.accepted);
                            mBinding.txtRequest.setClickable(false);
                            mBinding.txtRequest.setTextColor(getResources().getColor(R.color.color_green));

                        }
                        else if (trainerProfileModel.getData().getClient_request_status().equals("2")){
                            mBinding.txtRequest.setText(R.string.declined);
                            mBinding.txtRequest.setClickable(false);
                            mBinding.txtRequest.setTextColor(getResources().getColor(R.color.color_green));

                        }

                            /*switch (trainerProfileModel.getData().getRequest_status()) {
                                case "0":
                                    mBinding.txtRequest.setText(R.string.pending);
                                    mBinding.txtRequest.setClickable(false);
                                    mBinding.txtRequest.setTextColor(getResources().getColor(R.color.start_yellow));
                                    mBinding.imgCall.setClickable(false);
                                    mBinding.imgCall.setVisibility(View.INVISIBLE);
                                    mBinding.imgChat.setClickable(false);
                                    mBinding.imgChat.setVisibility(View.INVISIBLE);
                                    break;

                                case "1":
                                    mBinding.txtRequest.setText(R.string.accepted);
                                    mBinding.txtRequest.setClickable(false);
                                    mBinding.txtRequest.setTextColor(getResources().getColor(R.color.color_green));
                                    mBinding.imgCall.setClickable(true);
                                    mBinding.imgCall.setVisibility(View.VISIBLE);
                                    mBinding.imgChat.setClickable(true);
                                    mBinding.imgChat.setVisibility(View.VISIBLE);
                                    break;

                                case "2":
                                    mBinding.txtRequest.setText(R.string.declined);
                                    mBinding.txtRequest.setClickable(false);
                                    mBinding.txtRequest.setTextColor(getResources().getColor(R.color.red));
                                    mBinding.imgCall.setClickable(false);
                                    mBinding.imgCall.setVisibility(View.INVISIBLE);
                                    mBinding.imgChat.setClickable(false);
                                    mBinding.imgChat.setVisibility(View.INVISIBLE);
                                    break;
                                default:
                                    mBinding.txtRequest.setText(R.string.send_request);
                                    mBinding.txtRequest.setClickable(true);
                                    mBinding.imgCall.setClickable(false);
                                    mBinding.imgCall.setVisibility(View.INVISIBLE);
                                    mBinding.imgChat.setClickable(false);
                                    mBinding.imgChat.setVisibility(View.INVISIBLE);
                                    break;

                            }*/

                        }

                       /* if (trainerProfileModel.getData().getPhoto() != null)
                            mBinding.txtTrainerEmail.setText(trainerProfileModel.getData().getEmail());*/
                        if (trainerProfileModel.getData().getPhoto() == null || trainerProfileModel.getData().getPhoto().equals("") || trainerProfileModel.getData().getPhoto() == "") {
                            mBinding.imgUser.setImageDrawable(getResources().getDrawable(R.drawable.ic_user_placeholder));
                        } else {
                            Glide.with(getActivity())
                                    .load(trainerProfileModel.getData().getPhoto())
                                    .into(mBinding.imgUser);
                        }

                    }
                    break;
                }
        }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_user:
                if (trainerProfileModel != null && trainerProfileModel.getData() != null && trainerProfileModel.getData().getPhoto() != null) {
                    startActivity(new Intent(getContext(), ClientDocumentViewActivity.class).putExtra("data", trainerProfileModel.getData().getPhoto()));
                }
                break;
        }

    }

/*
    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getTrainerProfile:

                TrainerProfileModel trainerProfileModel = (TrainerProfileModel) o;
                if (trainerProfileModel != null) {
                    if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {

                        switch (apiNames) {
                            case getTrainerProfile:
                                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                trainerProfileModel = (TrainerProfileModel) o;
                                if (trainerProfileModel != null) {
                                    if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {
                     *//*   setUpViewPager(mBinding.viewpager);
                        mBinding.tabLayout.setupWithViewPager(mBinding.viewpager);*//*
                                        mBinding.txtTrainerName.setText(trainerProfileModel.getData().getName());
                                        mBinding.txtTrainerAddress.setText(trainerProfileModel.getData().getCity() + "," + trainerProfileModel.getData().getState());
                                        mBinding.txtTrainerAddress.setText(trainerProfileModel.getData().getEmail());
                                        if (trainerProfileModel.getData().getPhoto() != null)
                                            mBinding.txtTrainerEmail.setText(trainerProfileModel.getData().getEmail());
                                        if (trainerProfileModel.getData().getPhoto() != null)
                                            Glide.with(getActivity())
                                                    .load(trainerProfileModel.getData().getPhoto())
                                                    .into(mBinding.imgUser);

                                    }
                                }

                                break;
                        }

                    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }*/
}
