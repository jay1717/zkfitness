package com.fitzoh.app.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogAddTrainingProgramBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addTrainingProgram;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.editTraining;

public class AddTrainingProgram extends AppCompatDialogFragment implements SingleCallback {

    View view;
    DialogAddTrainingProgramBinding mBinding;
    public SessionManager session;
    private AddDietDialog.DialogClickListener dialogClickListener;
    String userId, userAccessToken;
    public CompositeDisposable compositeDisposable;
    boolean isEdit = false;
    String title;
    int trainingId;

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_training_program, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnCreate, false);
            view = mBinding.getRoot();
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            if (getArguments() != null) {
                isEdit = getArguments().getBoolean("isEdit");
                title = getArguments().getString("trainingTitle");
                trainingId = getArguments().getInt("trainingId");

                if (isEdit) {
                    mBinding.edtTrainingTitle.setText(title);
                    mBinding.btnCreate.setText(getString(R.string.save));
                } else {
                    mBinding.btnCreate.setText(getString(R.string.create));
                }
            }

            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();

        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }

    private void prepareLayout() {


        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });
        mBinding.btnCreate.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    if (!isEdit)
                        addTraining();
                    else
                        editTraining();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
    }

    public void disableScreen(boolean disable) {
        if (getActivity() != null){
            if (disable) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            } else {
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        }

    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }


    private void addTraining() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addTrainingProgram(mBinding.edtTrainingTitle.getText().toString())
                , getCompositeDisposable(), addTrainingProgram, this);
    }

    private void editTraining() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).editTrainingProgram(mBinding.edtTrainingTitle.getText().toString(), trainingId)
                , getCompositeDisposable(), editTraining, this);
    }


    private boolean validation() {
        if (TextUtils.isEmpty(mBinding.edtTrainingTitle.getText().toString())) {
            showSnackBar("Please Enter Training Program.");
            return false;
        }
        return true;
    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        CommonApiResponse commonApiResponse = (CommonApiResponse) o;
        switch (apiNames) {

            case addTrainingProgram:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent());
                    dismiss();
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
            case editTraining:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent());
                    dismiss();
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
