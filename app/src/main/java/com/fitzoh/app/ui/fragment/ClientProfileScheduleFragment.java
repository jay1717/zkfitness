package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientProfileScheduleAdapter;
import com.fitzoh.app.databinding.FragmentClientProfileScheduleBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.ScheduleListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.dialog.AddClientScheduleDialog;
import com.fitzoh.app.ui.dialog.AddScheduleDialog;
import com.fitzoh.app.ui.dialog.ClientProfileAssignDialog;
import com.fitzoh.app.ui.dialog.StartWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignSchedule;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getSchedualList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.unAssignWorkout;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientProfileScheduleFragment extends BaseFragment implements SingleCallback, ClientProfileScheduleAdapter.onScheduleUnassign, SwipeRefreshLayout.OnRefreshListener {


    FragmentClientProfileScheduleBinding mBinding;
    String userId, userAccessToken;
    List<ScheduleListModel.DataBean> schedualList;
    List<ScheduleListModel.DataBean> assignToUserModel;
    ClientProfileScheduleAdapter scheduleAdapter;
    private static int clientId;
    //  private ClientListModel.DataBean data = null;


    public ClientProfileScheduleFragment() {
        // Required empty public constructor
    }

    public static ClientProfileScheduleFragment newInstance() {
        ClientProfileScheduleFragment fragment = new ClientProfileScheduleFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // data = (ClientListModel.DataBean) getArguments().getSerializable("data");
            clientId = getArguments().getInt("data");
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.schedule));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_profile_schedule, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        mBinding.fab.setOnClickListener(v -> {
            AddClientScheduleDialog dialogSchedule = new AddClientScheduleDialog(clientId);
            //dialogNote.setListener(this);
            dialogSchedule.setTargetFragment(this, 0);
            dialogSchedule.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddScheduleDialog.class.getSimpleName());
            dialogSchedule.setCancelable(false);
        });
        if (Utils.isOnline(getContext()))
            callSchedualList();
        else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
        return mBinding.getRoot();
    }

    private void callSchedualList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientProfileScheduleList(clientId)
                    , getCompositeDisposable(), getSchedualList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callAllScheduleList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSchedualList()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callSchedualList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            int position = data.getIntExtra("data", 0);
            assignSchedule(assignToUserModel.get(position).getId());
        }
    }

    private void assignSchedule(int id) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson(id));
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignSchedule(requestBody)
                    , getCompositeDisposable(), assignSchedule, this);


        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

    }

    private String getClientJson(int id) {
        JSONArray jsonArray = new JSONArray();
        JSONObject obj = new JSONObject();

        JSONObject jsonObject = new JSONObject();
        try {
            obj.put("user_id", clientId);
            obj.put("is_assigned", 1);
            jsonArray.put(obj);
            jsonObject.put("client_ids", jsonArray);
            jsonObject.put("client_group_ids", new JSONArray());
            jsonObject.put("schedule_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {

            case deleteSchedual:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callSchedualList();
                }
                break;
            case assignSchedule:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response.getStatus() == AppConstants.SUCCESS) {
                    callSchedualList();
                }
                break;


            case unAssignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponseUnassign = (CommonApiResponse) o;
                if (commonApiResponseUnassign.getStatus() == AppConstants.SUCCESS) {
                    callSchedualList();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponseUnassign.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case getSchedualList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                schedualList = new ArrayList<>();
                ScheduleListModel scheduleListModel = (ScheduleListModel) o;
                if (scheduleListModel.getStatus() == AppConstants.SUCCESS) {

                    if (scheduleListModel != null) {
                        schedualList.addAll(scheduleListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (schedualList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            scheduleAdapter = new ClientProfileScheduleAdapter(getActivity(), schedualList, (ClientProfileScheduleAdapter.onScheduleUnassign) this);
                            mBinding.recyclerView.setAdapter(scheduleAdapter);
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, scheduleListModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ScheduleListModel model = (ScheduleListModel) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null && model.getData().size() > 0) {
                    assignToUserModel = model.getData();
                    ArrayList<String> workout_names = new ArrayList<>();
                    for (int i = 0; i < assignToUserModel.size(); i++) {
                        workout_names.add(assignToUserModel.get(i).getDay() + "\n" + "Start Time : " + assignToUserModel.get(i).getStart_time());
                    }
                    ClientProfileAssignDialog dialog = new ClientProfileAssignDialog();
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("data", workout_names);
                    dialog.setArguments(bundle);
                    dialog.setTargetFragment(this, 100);
                    dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), StartWorkoutDialog.class.getSimpleName());
                    dialog.setCancelable(false);
                } else {
                    showSnackBar(mBinding.mainLayout, "No schedule found.", Snackbar.LENGTH_LONG);
                }
                break;


        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

        showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    @Override
    public void unAssign(ScheduleListModel.DataBean dataBean) {

        if (dataBean != null) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", clientId);
                jsonObject.put("is_assigned", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();

            }
            JSONObject jsonObjectUnassign = new JSONObject();
            try {
                jsonObjectUnassign.put("client_ids", jsonArray);
                jsonObjectUnassign.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
                jsonObjectUnassign.put("schedule_id", dataBean.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObjectUnassign.toString());

            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).unAssignSchedule(requestBody)
                        , getCompositeDisposable(), unAssignWorkout, this);
            } else {
                showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        }
    }

    @Override
    public void onRefresh() {
        callSchedualList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
