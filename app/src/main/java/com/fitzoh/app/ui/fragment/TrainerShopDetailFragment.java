package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smarteist.autoimageslider.SliderView;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentTrainerProductDetailBinding;
import com.fitzoh.app.model.ClientShopListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainerShopListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityClientCartList;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainerShopDetailFragment extends BaseFragment implements SingleCallback {


    FragmentTrainerProductDetailBinding mBinding;
    TrainerShopListModel.DataBean dataBean;
    String userId, userAccessToken;

    public TrainerShopDetailFragment() {
        // Required empty public constructor
    }

    public static TrainerShopDetailFragment newInstance() {
        TrainerShopDetailFragment fragment = new TrainerShopDetailFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (TrainerShopListModel.DataBean) getArguments().getSerializable("data");
            // clientId = getArguments().getInt("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Product Details");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trainer_product_detail, container, false);
        // mBinding.toolbar.imgBack.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back));

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();


        if (dataBean != null) {

            mBinding.txtNameValue.setText(dataBean.getName());
            mBinding.txtPrice.setText("\u20B9" + dataBean.getPrice());
            mBinding.txtDescTitle.setText(dataBean.getDescription());
            mBinding.txtCarbsValue.setText(String.valueOf(dataBean.getCarbs()));
            mBinding.txtCarloriesValue.setText(String.valueOf(dataBean.getCalories()));
            mBinding.txtFatValue.setText(String.valueOf(dataBean.getFat()));
            mBinding.txtProtienValue.setText(String.valueOf(dataBean.getProtein()));

            if (dataBean.getImage().size() > 0) {
                mBinding.imgProductPlaceholder.setVisibility(View.INVISIBLE);
                int size = dataBean.getImage().size();
                for (int i = 0; i < size; i++) {

                    SliderView sliderView = new SliderView(getActivity());
                    sliderView.setImageUrl(dataBean.getImage().get(i));
                    sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);
                    sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(SliderView sliderView) {
                            //  context.startActivity(new Intent(context, DetailCheckInActivity.class).putExtra("checkin_id", checkInLabelModelList.get(position).getId()));

                        }
                    });

                    mBinding.imgProduct.addSliderView(sliderView);
                }
            } else {
                mBinding.imgProductPlaceholder.setVisibility(View.VISIBLE);
            }
        }
        if (session.getAuthorizedUser(mContext).isGym_trainer())
            mBinding.btnInquiry.setVisibility(View.VISIBLE);
        else
            mBinding.btnInquiry.setVisibility(View.GONE);

        mBinding.btnInquiry.setOnClickListener(v -> callAddToCart());

        return mBinding.getRoot();
    }

    private void callAddToCart() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addToCart(dataBean.getId())
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext())) {
                        startActivity(new Intent(getActivity(), ActivityClientCartList.class));
                        getActivity().finish();
                    } else {
                        showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);

                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
