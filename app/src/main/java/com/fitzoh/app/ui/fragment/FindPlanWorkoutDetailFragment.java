package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramExerciseMainAdapter;
import com.fitzoh.app.databinding.FragmentFindPlanWorkoutDetailBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.FindPlanWorkoutModel;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindPlanWorkoutDetailFragment extends BaseFragment implements SingleCallback, PaymentResultListener, SwipeRefreshLayout.OnRefreshListener {


    FragmentFindPlanWorkoutDetailBinding mBinding;
    String userId, userAccessToken;
    FindPlanWorkoutModel.DataBean dataBean;
    //  FindPlanWorkoutDetailAdapter findPlanWorkoutDetailAdapter;
    private List<List<WorkoutExerciseListModel.DataBean>> listDatas = new ArrayList<>();

    public FindPlanWorkoutDetailFragment() {
        // Required empty public constructor
    }

    public static FindPlanWorkoutDetailFragment newInstance() {
        FindPlanWorkoutDetailFragment fragment = new FindPlanWorkoutDetailFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (FindPlanWorkoutModel.DataBean) getArguments().getSerializable("data");
            // clientId = getArguments().getInt("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.find_a_plan));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_find_plan_workout_detail, container, false);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setHasOptionsMenu(true);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerView.setAdapter(new TrainingProgramExerciseMainAdapter(getContext(), new ArrayList<>()));
        callExerciseList();
        return mBinding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        menu.findItem(R.id.menu_notification).setVisible(false);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        textView.setText(dataBean.getPrice() == 0 ? "Add" : "Purchase");
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> purchase(dataBean.getPrice() == 0 ? "Add" : "Purchase"));
    }

    private void purchase(String type) {
        if (dataBean.getIs_purchased() == 1)
            showSnackBar(mBinding.mainLayout, "you have already purchased this nutrition plan.", Snackbar.LENGTH_LONG);
        else {
            if (type.equalsIgnoreCase("Add")) {
                //direct API
                purchaseSubscription("", "");
            } else {
                //razorpay api
                checkout();
            }
        }
    }

    private void checkout() {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher);
        final Activity activity = mActivity;


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Fitzoh");
            options.put("description", "Subscription charge");
            options.put("currency", "INR");
            options.put("amount", dataBean.getPrice() * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());
            preFill.put("email", session.getAuthorizedUser(mActivity).getEmail());
//            preFill.put("contact", "+919784849848");

            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    private void callExerciseList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkoutFindPlanDetail(dataBean.getId())
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutExerciseListModel workoutExerciseListModel = (WorkoutExerciseListModel) o;
                if (workoutExerciseListModel.getStatus() == AppConstants.SUCCESS) {
                    listDatas = workoutExerciseListModel.getData();
                    if (listDatas != null) {
                        if (listDatas.size() == 0) {
                            mBinding.recyclerView.setVisibility(View.GONE);
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                        } else {
                            mBinding.imgNoData.setVisibility(View.GONE);
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setAdapter(new TrainingProgramExerciseMainAdapter(mActivity, listDatas));
                        }
                    } else
                        showSnackBar(mBinding.mainLayout, workoutExerciseListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.onBackPressed();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");

    }

    private void purchaseSubscription(String s, String razorpay) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchase_plan("workout",
                dataBean.getId(), s, razorpay)
                , getCompositeDisposable(), save, this);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.mainLayout, s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);

    }

    @Override
    public void onRefresh() {
        callExerciseList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
