package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SelectNutritionPlanAdapter;
import com.fitzoh.app.databinding.FragmentSelectWorkoutBinding;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditNutritionActivity;
import com.fitzoh.app.ui.dialog.AddDietDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.DietList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectNutritionPlantFragment extends BaseFragment implements SingleCallback, AddDietDialog.DialogClickListener {


    FragmentSelectWorkoutBinding mBinding;
    String userId, userAccessToken;
    SelectNutritionPlanAdapter recyclerViewAdapter;

    public SelectNutritionPlantFragment() {
        // Required empty public constructor
    }

    public static SelectNutritionPlantFragment newInstance(Bundle bundle) {
        SelectNutritionPlantFragment fragment = new SelectNutritionPlantFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_select_workout, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.select_nutrition_plan));
//        Utils.getShapeGradient(mActivity, mBinding.layoutWorkout.btnSelect);
        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        mBinding.layoutWorkout.fab.setOnClickListener(view -> {
            AddDietDialog dialog = new AddDietDialog();
            dialog.setListener(this);
            dialog.setTargetFragment(this, 0);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddDietDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.layoutWorkout.recyclerView.setLayoutManager(recyclerLayoutManager);

        //RecyclerView adapater
        recyclerViewAdapter = new
                SelectNutritionPlanAdapter(mActivity, new ArrayList<>());
        mBinding.layoutWorkout.recyclerView.setAdapter(recyclerViewAdapter);
        mBinding.layoutWorkout.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (recyclerViewAdapter != null)
                    recyclerViewAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        callNutritionApi();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            DietListModel.DataBean dataBean = (DietListModel.DataBean) data.getSerializableExtra("data");
            startActivity(new Intent(getActivity(), EditNutritionActivity.class).putExtra("dietId", dataBean.getId()).putExtra("dietName", dataBean.getName()));
        }
    }

    private void callNutritionApi() {
        if (Utils.isOnline(getActivity())) {
            mBinding.layoutWorkout.edtSearch.setText("");
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getDietList()
                    , getCompositeDisposable(), DietList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case DietList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DietListModel dietListModel = (DietListModel) o;
                if (dietListModel != null && dietListModel.getStatus() == AppConstants.SUCCESS && dietListModel.getData() != null && dietListModel.getData().size() > 0) {
                    mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                    mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                    recyclerViewAdapter = new
                            SelectNutritionPlanAdapter(mActivity, dietListModel.getData());
                    mBinding.layoutWorkout.recyclerView.setAdapter(recyclerViewAdapter);
                } else {
                    mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
        mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
        mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void setClick() {

    }
}
