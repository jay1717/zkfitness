package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientWorkoutListAdapter;
import com.fitzoh.app.databinding.FragmentClientWorkoutBinding;
import com.fitzoh.app.model.DeleteWorkOutModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ClientLogWorkoutActivity;
import com.fitzoh.app.ui.dialog.AddWorkoutClientDialog;
import com.fitzoh.app.ui.dialog.RenameDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.clientWorkoutList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientWorkoutFragment extends BaseFragment implements SingleCallback, ClientWorkoutListAdapter.DeleteWorkoutListener, SwipeRefreshLayout.OnRefreshListener, RenameDialog.DialogClickListener {

    FragmentClientWorkoutBinding mBinding;
    String userId, userAccessToken;
    List<WorkOutListModel.DataBean> workoutList;
    ClientWorkoutListAdapter clientWorkoutListAdapter;

    public ClientWorkoutFragment() {
        // Required empty public constructor
    }

    public static ClientWorkoutFragment newInstance() {
        return new ClientWorkoutFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Workouts");

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_workout, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        setHasOptionsMenu(true);
        mBinding.layoutWorkout.fab.setOnClickListener(view -> {
            AddWorkoutClientDialog dialog = new AddWorkoutClientDialog();
            dialog.setTargetFragment(this, 0);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutClientDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        workoutList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        clientWorkoutListAdapter = new ClientWorkoutListAdapter(mActivity, workoutList, this);
        mBinding.layoutWorkout.recyclerView.setAdapter(clientWorkoutListAdapter);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        callWorkoutList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        textView.setText("Logged");
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> {
            session.editor.remove(CLIENT_ID).commit();
            startActivity(new Intent(getActivity(), ClientLogWorkoutActivity.class));
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkOutList()
                    , getCompositeDisposable(), clientWorkoutList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case clientWorkoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                workoutList = new ArrayList<>();
                WorkOutListModel dataBean = (WorkOutListModel) o;
                if (dataBean.getStatus() == AppConstants.SUCCESS) {

                    if (dataBean != null) {
                        workoutList.addAll(dataBean.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (workoutList.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            clientWorkoutListAdapter = new ClientWorkoutListAdapter(mActivity, workoutList, this);
                            mBinding.layoutWorkout.recyclerView.setAdapter(clientWorkoutListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, dataBean.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel commonApiResponse = (DeleteWorkOutModel) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callWorkoutList();
                } else {
                    showSnackBar(mBinding.linear, Objects.requireNonNull(commonApiResponse).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void delete(WorkOutListModel.DataBean dataBean) {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(dataBean.getId())
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void rename(WorkOutListModel.DataBean dataBean) {
        RenameDialog dialogNote = new RenameDialog(0, dataBean.getId(), 0, dataBean.getName());
        dialogNote.setListener(this);
        dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), RenameDialog.class.getSimpleName());
        dialogNote.setCancelable(false);
    }

    @Override
    public void onRefresh() {
        callWorkoutList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }

    @Override
    public void setClick() {
        callWorkoutList();
    }
}
