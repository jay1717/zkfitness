package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentClientprofilePuchasesBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClientProfilePurchasesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClientProfilePurchasesFragment extends BaseFragment {

    FragmentClientprofilePuchasesBinding mBinding;

    // TODO: Rename and change types of parameters
  //  private ClientListModel.DataBean data = null;
    int clientId;
    private String mParam2;


    public ClientProfilePurchasesFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ClientProfilePurchasesFragment newInstance() {
        ClientProfilePurchasesFragment fragment = new ClientProfilePurchasesFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clientId = getArguments().getInt("data");
          // data = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupToolBarWithBackArrow(mBinding.toolbar.too, "Arm Workout - Assign Clients");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_clientprofile_puchases, container, false);
        setupViewPager(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Purchases/Inquiries");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.toolbar.toolbar.setElevation(0f);
        }

        mBinding.tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        //  mBinding.tabs.setIndicator("",getResources().getDrawable(R.drawable.tab1));
        mBinding.tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        mBinding.tabs.setTabTextColors(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray));
        return mBinding.getRoot();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(ClientPurchaseFragment.newInstance(clientId), "Purchases");
        adapter.addFragment(FragmentProfilePurchases.newInstance(), "Inquiries");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
