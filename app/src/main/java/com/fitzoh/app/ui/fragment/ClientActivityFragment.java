package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientActivityListAdapter;
import com.fitzoh.app.databinding.FragmentClientActivityBinding;
import com.fitzoh.app.model.ClientProgressModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getProgressList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientActivityFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    FragmentClientActivityBinding mBinding;
    String userId, userAccessToken;
    // ClientListModel.DataBean data;
    int clientId;
    List<ClientProgressModel.DataBean> clientListData;

    public ClientActivityFragment() {
        // Required empty public constructor
    }

    public static ClientActivityFragment newInstance() {
        ClientActivityFragment fragment = new ClientActivityFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //   data = (ClientListModel.DataBean) getArguments().getSerializable("data");
            clientId = getArguments().getInt("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupToolBarWithBackArrow(mBinding.toolbar.too, "Arm Workout - Assign Clients");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_activity, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutActivity.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutActivity.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.toolbar.tvTitle.setText(R.string.client_activities);
        Utils.setImageBackground(getActivity(), mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.imgBack.setOnClickListener(view -> getActivity().finish());
        mBinding.layoutActivity.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callProgressList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callProgressList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getProgressData(clientId)
                    , getCompositeDisposable(), getProgressList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getProgressList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                clientListData = new ArrayList<>();
                ClientProgressModel clientProgressModel = (ClientProgressModel) o;
                if (clientProgressModel.getStatus() == AppConstants.SUCCESS) {
                    if (clientProgressModel != null) {
                        clientListData.addAll(clientProgressModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (clientListData.size() == 0) {
                            mBinding.layoutActivity.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutActivity.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutActivity.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutActivity.imgNoData.setVisibility(View.GONE);
                            mBinding.layoutActivity.recyclerView.setAdapter(new ClientActivityListAdapter(getActivity(), clientListData));
                        }

                    } else
                        showSnackBar(mBinding.linear, clientProgressModel.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        callProgressList();
        mBinding.layoutActivity.swipeContainer.setRefreshing(false);
    }
}
