package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ExerciseInstructionAdapter;
import com.fitzoh.app.databinding.ActivityDetailExcersiceBinding;
import com.fitzoh.app.model.ExerciseDetailData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.Res;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;
import com.fitzoh.app.vectorchildfinder.VectorChildFinder;
import com.fitzoh.app.vectorchildfinder.VectorDrawableCompat;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.utils.Utils.extractYTId;

public class DetailExcersiceActivity extends YouTubeBaseActivity implements SingleCallback, YouTubePlayer.OnInitializedListener {
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    String userId, userAccessToken;
    String DEVELOPER_KEY = "AIzaSyAQNKNP2qdX6AgwfxVyKGPsvEJkmp1hmEI";
    //        String url = "https://www.youtube.com/watch?v=dOoNRzddLC0";
    String url = "";
    ActivityDetailExcersiceBinding mBinding;
    private SessionManager session;
    public CompositeDisposable compositeDisposable;
    private int exerciseId = 0;
    public Res res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail_excersice);
        session = new SessionManager(this);
        if (getIntent().hasExtra("id")) {
            exerciseId = getIntent().getIntExtra("id", 0);
        }

        mBinding.layoutExerciseDetail.recyclerViewInstructions.setVisibility(View.GONE);
        mBinding.layoutExerciseDetail.recyclerViewInstructions.setAdapter(new ExerciseInstructionAdapter());
        mBinding.toolbar.tvTitle.setText("detail");
        mBinding.layoutExerciseDetail.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        setImageBackgroundDrawable();
        mBinding.toolbar.imgBack.setOnClickListener(v -> onBackPressed());
        mBinding.layoutExerciseDetail.view.view.setBackgroundColor(res.getColor(R.color.colorAccent));
        mBinding.layoutExerciseDetail.view.view2.setBackgroundColor(res.getColor(R.color.colorAccent));

        getExerciseDetail();
    }

    public void setImageBackgroundDrawable() {
        VectorChildFinder vector = new VectorChildFinder(res, R.drawable.ic_back, mBinding.toolbar.imgBack);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(ContextCompat.getColor(this, R.color.colorAccent));
    }

    @Override
    public Resources getResources() {
        if (res == null) {
            res = new Res(this, super.getResources());
        }
        return res;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getExerciseDetail() {
        userId = String.valueOf(session.getAuthorizedUser(this).getId());
        userAccessToken = session.getAuthorizedUser(this).getUserAccessToken();

        mBinding.layoutExerciseDetail.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).exerciseDetail(exerciseId)
                , getCompositeDisposable(), single, this);
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.layoutExerciseDetail.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ExerciseDetailData exerciseDetailData = (ExerciseDetailData) o;
                if (exerciseDetailData != null && exerciseDetailData.getStatus() == AppConstants.SUCCESS && exerciseDetailData.getData() != null) {
                    mBinding.setItem(exerciseDetailData.getData());
                    mBinding.layoutExerciseDetail.setItem(exerciseDetailData.getData());
                    mBinding.toolbar.tvTitle.setText(exerciseDetailData.getData().getExercise_name());
                    if (exerciseDetailData.getData().getInstructions() != null)
                        mBinding.layoutExerciseDetail.txtInstruction.setText(exerciseDetailData.getData().getInstructions());
                    else
                        mBinding.layoutExerciseDetail.txtInstruction.setText("N/A");

                    if (exerciseDetailData.getData().getVideo_url() != null && !exerciseDetailData.getData().getVideo_url().equalsIgnoreCase("")) {
                        url = exerciseDetailData.getData().getVideo_url();
                        mBinding.youtubeView.initialize(DEVELOPER_KEY, this);
                        mBinding.noVideoAvailable.setVisibility(View.GONE);
                        mBinding.youtubeView.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.noVideoAvailable.setVisibility(View.VISIBLE);
                        mBinding.youtubeView.setVisibility(View.GONE);
                    }
                } else {
                    Snackbar.make(mBinding.getRoot(), getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.layoutExerciseDetail.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        Snackbar.make(mBinding.getRoot(), getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {

            String videoId = "";
            try {
                if (URLUtil.isValidUrl(url)) {
                    if (url.contains("https://youtu.be/")) {
                        String currentString = url;
                        String[] separated = currentString.split("youtu.be/");
                        videoId = separated[1];

                    } else {
                        String currentString = url;
                        String[] separated = currentString.split("v=");
                        videoId = separated[1];
                    }
                } else {
                    String finalUrl = "https://www.youtube.com/watch?v=" + url;
                    videoId = extractYTId(finalUrl);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                mBinding.noVideoAvailable.setVisibility(View.VISIBLE);
                mBinding.youtubeView.setVisibility(View.GONE);
            }
            if (videoId.equals("")) {
                mBinding.noVideoAvailable.setVisibility(View.VISIBLE);
                mBinding.youtubeView.setVisibility(View.GONE);
                Toast.makeText(this, "Invalid URL", Toast.LENGTH_SHORT).show();
            } else {
                mBinding.noVideoAvailable.setVisibility(View.GONE);
                mBinding.youtubeView.setVisibility(View.VISIBLE);

                Log.e("onInitializationSucce", videoId);
                youTubePlayer.cueVideo(videoId);
                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        mBinding.noVideoAvailable.setVisibility(View.VISIBLE);
        mBinding.youtubeView.setVisibility(View.GONE);
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            mBinding.youtubeView.initialize(DEVELOPER_KEY, this);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }
}
