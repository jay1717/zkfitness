package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogNutritionPlanBinding;
import com.fitzoh.app.model.TotalNutritionModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getNutrition;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

@SuppressLint("ValidFragment")
public class NutritionPlanDialog extends AppCompatDialogFragment implements SingleCallback {

    DialogNutritionPlanBinding mBinding;
    AddMealDialog.DialogClickListener listener;
    String userId, userAccessToken;
    public SessionManager session;
    public CompositeDisposable compositeDisposable;
    int dietId;
    View view;

    public NutritionPlanDialog(int dietId) {
        this.dietId = dietId;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_nutrition_plan, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnCancel, false);
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            view = mBinding.getRoot();
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setView(view);
        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();
        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();


        return dialog;
    }

    private void getNutrition() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getToalNutrition(dietId, session.getStringDataByKeyNull(CLIENT_ID))
                , getCompositeDisposable(), getNutrition, this);
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }


    private void prepareLayout() {
        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (Utils.isOnline(getContext()))
            getNutrition();
        mBinding.btnCancel.setOnClickListener(view -> dismiss());
    }

    public void setListener(AddMealDialog.DialogClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getNutrition:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TotalNutritionModel totalNutritionModel = (TotalNutritionModel) o;
                if (totalNutritionModel != null && totalNutritionModel.getStatus() == AppConstants.SUCCESS) {
                    //dialogClickListener.setClick();
                    mBinding.txtNoMealsValue.setText(String.valueOf(totalNutritionModel.getData().getNo_of_meals()));
                    mBinding.txtNoCaloriesValue.setText(String.valueOf(totalNutritionModel.getData().getCalories()));
                    mBinding.txtNoCarbValue.setText(String.valueOf(totalNutritionModel.getData().getCarbs()));
                    mBinding.txtNoProteinValue.setText(String.valueOf(totalNutritionModel.getData().getProtein()));
                    mBinding.txtNoFatValue.setText(String.valueOf(totalNutritionModel.getData().getFat()));
                } else {
                    showSnackBar(totalNutritionModel.getMessage());
                }
                break;
        }
    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    public interface DialogClickListener {
        public void setClick();
    }
}
