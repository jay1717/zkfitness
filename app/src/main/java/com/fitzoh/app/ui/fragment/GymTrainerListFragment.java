package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GymTrainerClientAdapter;
import com.fitzoh.app.databinding.FragmentGymTrainerListBinding;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddTrainerActivity;
import com.fitzoh.app.ui.activity.GymAssignClientActivity;
import com.fitzoh.app.ui.activity.GymAssignClientDetailsActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


/**
 * A simple {@link Fragment} subclass.
 */
public class GymTrainerListFragment extends BaseFragment implements SingleCallback, GymTrainerClientAdapter.onDataModeified, SwipeRefreshLayout.OnRefreshListener {


    FragmentGymTrainerListBinding mBinding;
    String userId, userAccessToken;
    List<TrainerListModel.DataBean> trainerList;
    GymTrainerClientAdapter gymTrainerClientAdapter;


    public GymTrainerListFragment() {
        // Required empty public constructor
    }

    public static GymTrainerListFragment newInstance() {
        GymTrainerListFragment fragment = new GymTrainerListFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Gym Trainer");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_gym_trainer_list, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        trainerList = new ArrayList<>();
        mBinding.fab.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), AddTrainerActivity.class));
        });
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        gymTrainerClientAdapter = new GymTrainerClientAdapter(getActivity(), trainerList, this);
        mBinding.recyclerView.setAdapter(gymTrainerClientAdapter);

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callTrainerList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callTrainerList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerList()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainerList = new ArrayList<>();
                TrainerListModel trainerListModel = (TrainerListModel) o;
                if (trainerListModel.getStatus() == AppConstants.SUCCESS) {

                    if (trainerListModel != null) {
                        trainerList.addAll(trainerListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (trainerList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            gymTrainerClientAdapter = new GymTrainerClientAdapter(getActivity(), trainerList, this);
                            mBinding.recyclerView.setAdapter(gymTrainerClientAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, trainerListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void chat(TrainerListModel.DataBean dataBean) {
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        intent.putExtra(ConversationUIService.USER_ID, String.valueOf(dataBean.getId()));
        intent.putExtra(ConversationUIService.DISPLAY_NAME, dataBean.getName()); //put it for displaying the title.
        intent.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
        getActivity().startActivity(intent);
    }

    @Override
    public void assign(TrainerListModel.DataBean dataBean) {
        mActivity.startActivityForResult(new Intent(getActivity(), GymAssignClientActivity.class).putExtra("id", dataBean.getId()), 1);
    }

    @Override
    public void getData(TrainerListModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), GymAssignClientDetailsActivity.class).putExtra("trainerId", dataBean.getId()));
    }

    @Override
    public void onRefresh() {
        callTrainerList();
    }
}
