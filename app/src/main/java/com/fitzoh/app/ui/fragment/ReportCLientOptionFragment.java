package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentReportClientOptionBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityAchievement;
import com.fitzoh.app.ui.activity.ReportGraphTypectivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportCLientOptionFragment extends BaseFragment implements View.OnClickListener {


    FragmentReportClientOptionBinding mBinding;
    private Integer clientId = null;
    private ClientListModel.DataBean data = null;

    public ReportCLientOptionFragment() {
        // Required empty public constructor
    }

    public static ReportCLientOptionFragment newInstance() {
        ReportCLientOptionFragment fragment = new ReportCLientOptionFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("isFromNav") && getArguments().getBoolean("isFromNav"))
            setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.reports));
        else
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.reports));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            // data = (ClientListModel.DataBean) getArguments().getSerializable("data");
            clientId = getArguments().getInt("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_client_option, container, false);
//        mBinding.toolbar.imgBack.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back));
//        mBinding.toolbar.tvTitle.setText(R.string.reports);
//        mBinding.toolbar.imgBack.setOnClickListener(this);
        mBinding.txtWorkout.setOnClickListener(this);
        mBinding.txtDiet.setOnClickListener(this);
        mBinding.txtAchivements.setOnClickListener(this);
        mBinding.txtCheckin.setOnClickListener(this);
        mBinding.txtStrength.setOnClickListener(this);
        return mBinding.getRoot();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), ReportGraphTypectivity.class);
        switch (view.getId()) {

            case R.id.txt_workout:
                intent.putExtra("graphType", "workout");
                if (clientId != null)
                    intent.putExtra("data", clientId);
                startActivity(intent);
                break;

            case R.id.txt_diet:
                intent.putExtra("graphType", "report");
                if (clientId != null)
                    intent.putExtra("data", clientId);
                startActivity(intent);
                break;

            case R.id.txt_achivements:
                startActivity(new Intent(getActivity(), ActivityAchievement.class));
                break;

            case R.id.txt_checkin:

                break;

            case R.id.txt_strength:
                intent.putExtra("graphType", "strength");
                if (clientId != null)
                    intent.putExtra("data", clientId);
                startActivity(intent);
                break;

          /*  case R.id.img_back:
                getActivity().finish();
                break;*/
        }

    }
}
