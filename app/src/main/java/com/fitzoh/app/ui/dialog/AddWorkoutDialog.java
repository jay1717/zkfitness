package com.fitzoh.app.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogAddWorkoutBinding;
import com.fitzoh.app.model.AddWorkoutResponse;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class AddWorkoutDialog extends AppCompatDialogFragment implements AdapterView.OnItemSelectedListener, SingleCallback {
    DialogAddWorkoutBinding mBinding;
    View view;
    public CompositeDisposable compositeDisposable;
    private List<WorkOutListModel.DataBean> workoutList = new ArrayList<>();
    String userId, userAccessToken;
    public SessionManager session;

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_workout, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.btnTempleteDropdown, mBinding.btnTempleteDropdownNew});
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            view = mBinding.getRoot();
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();

        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }


    private void prepareLayout() {

        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        getWorkouts();

        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });
        mBinding.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    addWorkout();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
    }

    private void getWorkouts() {
        mBinding.spnTemplete.setVisibility(View.GONE);
        mBinding.btnTempleteDropdown.setVisibility(View.GONE);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkoutListDialog("alphabetical")
                , getCompositeDisposable(), single, this);

    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    private boolean validation() {
        if (TextUtils.isEmpty(mBinding.edtWorkoutName.getText().toString())) {
            showSnackBar("Enter Workout name.");
            return false;
        } else if (mBinding.spnTemplete.getSelectedItemPosition() < 0) {
            showSnackBar("Select Workout template.");
            return false;
        } else if (mBinding.spnTempleteNew.getVisibility() == View.VISIBLE && mBinding.spnTempleteNew.getSelectedItemPosition() < 0) {
            showSnackBar("Select existing workout template.");
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 1) {
            if (workoutList != null && workoutList.size() > 0) {
                mBinding.spnTempleteNew.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdownNew.setVisibility(View.VISIBLE);
            } else {
                mBinding.spnTempleteNew.setVisibility(View.GONE);
                mBinding.btnTempleteDropdownNew.setVisibility(View.GONE);
            }
        } else {
            mBinding.spnTempleteNew.setVisibility(View.GONE);
            mBinding.btnTempleteDropdownNew.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                mBinding.spnTemplete.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdown.setVisibility(View.VISIBLE);
                WorkOutListModel workoutListModel = (WorkOutListModel) o;
                if (workoutListModel != null && workoutListModel.getStatus() == AppConstants.SUCCESS && workoutListModel.getData() != null && workoutListModel.getData().size() > 0) {
                    workoutList = workoutListModel.getData();
                    setSpinner();
                } else {
                    createSpinnerTemplate();
                }
                break;
            case addWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);

                AddWorkoutResponse commonApiResponse = (AddWorkoutResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent().putExtra("data", commonApiResponse.getData()));
                    dismiss();
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }

    private void createSpinnerTemplate() {
        List<String> list = new ArrayList<>();
        list.add("New Workout");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, list);
        mBinding.spnTemplete.setAdapter(adapter1);
        adapter1.setDropDownViewResource(R.layout.spinner_item);
    }


    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void addWorkout() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        int id = mBinding.spnTemplete.getSelectedItemPosition() == 0 ? 0 : workoutList.get(mBinding.spnTempleteNew.getSelectedItemPosition()).getId();
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addWorkout(mBinding.edtWorkoutName.getText().toString(), id)
                , getCompositeDisposable(), addWorkout, this);
    }

    private void setSpinner() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < workoutList.size(); i++) {
            list.add(workoutList.get(i).getName());
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, list);
        mBinding.spnTempleteNew.setAdapter(adapter1);
        adapter1.setDropDownViewResource(R.layout.spinner_item);

        String[] templates = getResources().getStringArray(R.array.workout_template_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, templates);
        mBinding.spnTemplete.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mBinding.spnTemplete.setOnItemSelectedListener(this);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case single:
                mBinding.spnTemplete.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdown.setVisibility(View.VISIBLE);
                createSpinnerTemplate();
                break;
        }
        showSnackBar(getString(R.string.something_went_wrong));
    }


    public interface DialogClickListener {
        public void setClick();
    }
}
