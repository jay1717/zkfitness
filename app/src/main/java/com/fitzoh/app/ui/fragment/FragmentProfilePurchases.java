package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.InquiryAdapter;
import com.fitzoh.app.adapter.ProfileWorkoutAdapter;
import com.fitzoh.app.adapter.WorkoutListAdapter;
import com.fitzoh.app.databinding.FragmentProfilePurchasesBinding;
import com.fitzoh.app.model.InquiryModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityClientInquiryDetail;
import com.fitzoh.app.ui.activity.ActivityinquiryDetail;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentProfilePurchases#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProfilePurchases extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, SingleCallback, InquiryAdapter.dataPassing {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentProfilePurchasesBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<InquiryModel.DataBean> inquiryList;
    InquiryAdapter inquiryAdapter;



    public FragmentProfilePurchases() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentWorkout.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentProfilePurchases newInstance() {
        FragmentProfilePurchases fragment = new FragmentProfilePurchases();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.workouts));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_profile_purchases, container, false);

        inquiryList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.fab.setVisibility(View.GONE);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
//        profileWorkoutAdapter = new ProfileWorkoutAdapter(getActivity(), (ProfileWorkoutAdapter.unAssignClient) this, workOutListData);
        mBinding.layoutWorkout.recyclerView.setAdapter(inquiryAdapter);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callInquiryList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callInquiryList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getInquiryList()
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callUnassignClient(int clientID) {
        /*if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                    , getCompositeDisposable(), deleteWorkout, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }*/
        showSnackBar(mBinding.linear, "Client ID " + clientID, Snackbar.LENGTH_LONG);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callInquiryList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }


    @Override
    public void onRefresh() {
        callInquiryList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames){
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                inquiryList = new ArrayList<>();
                InquiryModel inquiryModel = (InquiryModel) o;
                if (inquiryModel.getStatus() == AppConstants.SUCCESS) {

                    if (inquiryModel != null) {
                        inquiryList.addAll(inquiryModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (inquiryList.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            inquiryAdapter = new InquiryAdapter(inquiryList, getActivity(), this);
                            mBinding.layoutWorkout.recyclerView.setAdapter(inquiryAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, inquiryModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void data(InquiryModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), ActivityinquiryDetail.class).putExtra("data", dataBean));
    }
}
