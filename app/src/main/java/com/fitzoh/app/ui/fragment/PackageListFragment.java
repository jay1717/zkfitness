package com.fitzoh.app.ui.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.PackagesListAdapter;
import com.fitzoh.app.databinding.FragmentPackageListBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.PackageListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreatePackageActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class PackageListFragment extends BaseFragment implements SingleCallback, PackagesListAdapter.onAction, SwipeRefreshLayout.OnRefreshListener {

    FragmentPackageListBinding mBinding;
    String userId, userAccessToken;
    List<PackageListModel.DataBean> dataBeanList;
    PackagesListAdapter packagesListAdapter;

    public PackageListFragment() {
        // Required empty public constructor
    }

    public static PackageListFragment newInstance() {
        PackageListFragment fragment = new PackageListFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_package_list, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Your Packages");
        mBinding.layoutPackage.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutPackage.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.setAddFabBackground(mActivity, mBinding.layoutPackage.fab);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutPackage.fab.setOnClickListener(view -> {
            startCreatePackageActivity();
        });
        return mBinding.getRoot();
    }

    private void startCreatePackageActivity() {
        Intent intent = new Intent(mActivity, CreatePackageActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        callPackageList();
    }


    private void callPackageList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.layoutPackage.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getPackageData()
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getPackageList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case getPackageList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                dataBeanList = new ArrayList<>();
                PackageListModel packageListModel = (PackageListModel) o;
                if (packageListModel != null) {
                    dataBeanList.addAll(packageListModel.getData());
                    // workoutListAdapter.notifyDataSetChanged();
                    if (packageListModel.getData().size() == 0) {
                        mBinding.layoutPackage.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.layoutPackage.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.layoutPackage.recyclerView.setVisibility(View.VISIBLE);
                        mBinding.layoutPackage.imgNoData.setVisibility(View.GONE);
                        packagesListAdapter = new PackagesListAdapter(dataBeanList, getActivity(), this);
                        mBinding.layoutPackage.recyclerView.setAdapter(packagesListAdapter);
                    }
                } else
                    showSnackBar(mBinding.linear, packageListModel.getMessage(), Snackbar.LENGTH_LONG);

                break;

            case deletePackage:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deletePackage = (CommonApiResponse) o;
                if (deletePackage.getStatus() == AppConstants.SUCCESS)
                    callPackageList();
                else
                    showSnackBar(mBinding.linear, deletePackage.getMessage(), Snackbar.LENGTH_LONG);
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void edit(PackageListModel.DataBean packageListModel) {
        startActivity(new Intent(mActivity, CreatePackageActivity.class).putExtra("isFromEdit", true).putExtra("data", packageListModel));
    }

    @Override
    public void delete(PackageListModel.DataBean packageListModel) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deletePackage(packageListModel.getId())
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.deletePackage, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onRefresh() {
        callPackageList();
    }
}
