package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.CertificationAdapter;
import com.fitzoh.app.databinding.FragmentCertificationBinding;
import com.fitzoh.app.model.CertificationModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CertificationFragment extends BaseFragment implements SingleCallback {

    FragmentCertificationBinding mBinding;
    View view;
    int trainerId = 0;
    String userId, userAccessToken;
    List<CertificationModel.DataBean> certificationDataList;
    CertificationAdapter certificationAdapter;


    public CertificationFragment() {
        // Required empty public constructor
    }

    public static CertificationFragment newInstance(int trainerId, int gymId) {
        CertificationFragment fragment = new CertificationFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("data", trainerId);
        bundle.putInt("gymId", gymId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trainerId = getArguments().getInt("data");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_certification, container, false);

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        getCertification();
        return mBinding.getRoot();
    }

    private void getCertification() {

        if (Utils.isOnline(getActivity())) {
            if (getArguments() != null && getArguments().containsKey("data")) {
                trainerId = getArguments().getInt("data");
            }
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(false);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getCertification(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerCertificates, this);
        } else {
            showSnackBar(mBinding.linerMain, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getTrainerCertificates:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                certificationDataList = new ArrayList<>();
                CertificationModel certificationModel = (CertificationModel) o;
                if (certificationModel.getStatus() == AppConstants.SUCCESS) {
                    if (certificationModel.getData() != null) {
                        certificationDataList.addAll(certificationModel.getData());
                        if (certificationDataList.size() <= 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            certificationAdapter = new CertificationAdapter(getActivity(), certificationDataList);
                            mBinding.recyclerView.setAdapter(certificationAdapter);
                        }

                    }
                }

                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
