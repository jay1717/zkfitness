package com.fitzoh.app.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewPagerAdapter;
import com.fitzoh.app.databinding.FragmentMyClientProfileBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;

public class MyClientProfileFragment extends BaseFragment implements SingleCallback {

    FragmentMyClientProfileBinding mBinding;
    String userId, userAccessToken;
    ViewPagerAdapter viewPagerAdapter;
    ClientListModel.DataBean dataBean;

    public MyClientProfileFragment() {
        // Required empty public constructor
    }


    public static MyClientProfileFragment newInstance(Bundle bundle) {
        MyClientProfileFragment fragment = new MyClientProfileFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (ClientListModel.DataBean) getArguments().get("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Client Profile");

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_my_client_profile, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        getClientProfile();
        setUpViewPageCertification(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        return mBinding.getRoot();

    }


    private void getClientProfile() {
        if (getArguments() != null) {
            dataBean = (ClientListModel.DataBean) getArguments().get("data");

            Utils.setImage(mContext, mBinding.imgUser, dataBean.getPhoto());
            mBinding.txtClientName.setText(dataBean.getName());
            mBinding.txtClientEmail.setText(dataBean.getEmail());
        }
    }

    private void setUpViewPageCertification(ViewPager viewpager) {
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        viewPagerAdapter.addFragment(MyClientCheckInListFragment.newInstance(dataBean), "Check In");
        viewpager.setAdapter(viewPagerAdapter);

        mBinding.tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        mBinding.tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case sendClientRequest:
                break;
        }
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
