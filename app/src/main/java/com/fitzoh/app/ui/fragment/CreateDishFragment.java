package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentCreateDishBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.ServingSizeUnitData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getServingSizeUnits;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateDishFragment extends BaseFragment implements View.OnClickListener, SingleCallback, AdapterView.OnItemSelectedListener {


    FragmentCreateDishBinding mBinding;
    String userId, userAccessToken;
    List<ServingSizeUnitData.DataBean> sizeUnitList = new ArrayList<>();
    private int diet_plan_id = 0, diet_plan_meal_id = 0;

    public CreateDishFragment() {
        // Required empty public constructor
    }

    public static CreateDishFragment newInstance(Bundle bundle) {
        CreateDishFragment createDishFragment = new CreateDishFragment();
        createDishFragment.setArguments(bundle);
        return createDishFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            diet_plan_id = getArguments().getInt("diet_plan_id", 0);
            diet_plan_meal_id = getArguments().getInt("diet_plan_meal_id", 0);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_create_dish, container, false);
        Utils.getShapeGradient(mActivity, mBinding.createDish.btnCreate);
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.createDish.btnUnitDropdown});
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setClickEvent();
        prepareLayout();

        return mBinding.getRoot();
    }

    private void prepareLayout() {
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.imgBack.setOnClickListener(v -> mActivity.onBackPressed());
        mBinding.toolbar.tvTitle.setText("Create Dish");

        callServingSizeUnit();
    }

    private void callServingSizeUnit() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getServingSizeUnits("")
                    , getCompositeDisposable(), getServingSizeUnits, this);
        } else {
            showSnackBar(mBinding.layoutPackage, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    private void setClickEvent() {
        mBinding.createDish.spinnerUnit.setOnItemSelectedListener(this);
        mBinding.createDish.btnCreate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_create:
                if (Utils.isOnline(mActivity)) {
                    if (validation())
                        createPackage();
                } else {
                    showSnackBar(mBinding.layoutPackage, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }


    private void createPackage() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                createDish(mBinding.createDish.edtDishName.getText().toString(),
                        mBinding.createDish.spinnerUnit.getSelectedItem().toString(),
                        mBinding.createDish.edtServingSize.getText().toString(),
                        sizeUnitList.get(mBinding.createDish.spinnerUnit.getSelectedItemPosition() - 1).getId(),
                        mBinding.createDish.edtCalories.getText().toString(),
                        mBinding.createDish.edtFat.getText().toString(),
                        mBinding.createDish.edtCarbs.getText().toString(),
                        mBinding.createDish.edtProtein.getText().toString(), diet_plan_meal_id, diet_plan_id), getCompositeDisposable(), single, this);
    }

    private boolean validation() {
        boolean isValid = true;
        if (mBinding.createDish.edtDishName.getText().toString().length() <= 0) {
            setEdittextError(mBinding.createDish.txtErrorName, "Please enter dish name");
            isValid = false;
        } else {
            mBinding.createDish.txtErrorName.setVisibility(View.GONE);
        }
        if (mBinding.createDish.spinnerUnit.getSelectedItemPosition() <= 0) {
            setEdittextError(mBinding.createDish.txtErrorUnit, "Please select unit");
            isValid = false;
        } else {
            mBinding.createDish.txtErrorUnit.setVisibility(View.GONE);
        }
        if (mBinding.createDish.edtServingSize.getText().toString().length() <= 0) {
            setEdittextError(mBinding.createDish.txtServingSize, "Please enter serving size");
            isValid = false;
        } else {
            mBinding.createDish.txtServingSize.setVisibility(View.GONE);
        }
        if (mBinding.createDish.edtCalories.getText().toString().length() <= 0) {
            setEdittextError(mBinding.createDish.txtErrorCalories, "Please enter calories");
            isValid = false;
        } else {
            mBinding.createDish.txtErrorCalories.setVisibility(View.GONE);
        }
        if (mBinding.createDish.edtFat.getText().toString().length() <= 0) {
            setEdittextError(mBinding.createDish.txtErrorFat, "Please enter fat");
            isValid = false;
        } else {
            mBinding.createDish.txtErrorFat.setVisibility(View.GONE);
        }
        if (mBinding.createDish.edtCarbs.getText().toString().length() <= 0) {
            setEdittextError(mBinding.createDish.txtErrorCarbs, "Please enter carbs");
            isValid = false;
        } else {
            mBinding.createDish.txtErrorCarbs.setVisibility(View.GONE);
        }
        if (mBinding.createDish.edtProtein.getText().toString().length() <= 0) {
            setEdittextError(mBinding.createDish.txtErrorProtein, "Please enter protein");
            isValid = false;
        } else {
            mBinding.createDish.txtErrorProtein.setVisibility(View.GONE);
        }
        return isValid;
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.layoutPackage, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case getServingSizeUnits:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ServingSizeUnitData response = (ServingSizeUnitData) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS && response.getData() != null && response.getData().size() > 0) {
                    sizeUnitList = response.getData();
                    List<String> list = new ArrayList<>();
                    list.add("Unit");
                    for (int i = 0; i < sizeUnitList.size(); i++) {
                        list.add(sizeUnitList.get(i).getName());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity, R.layout.spinner_search_item, list);
                    adapter.setDropDownViewResource(R.layout.spinner_search_item);
                    mBinding.createDish.spinnerUnit.setAdapter(adapter);

                } else {
                    showSnackBar(mBinding.layoutPackage, Objects.requireNonNull(response).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            mBinding.createDish.txtErrorUnit.setVisibility(View.GONE);
            mBinding.createDish.txtErrorUnit.setText("");
        } else {
            mBinding.createDish.txtErrorUnit.setVisibility(View.GONE);
            mBinding.createDish.txtErrorUnit.setText("");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


}
