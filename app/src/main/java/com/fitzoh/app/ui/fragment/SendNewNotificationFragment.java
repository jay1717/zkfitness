package com.fitzoh.app.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SendNotificationAllAdpter;
import com.fitzoh.app.databinding.FragmentSendNewNotificationBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.Singleton;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;

public class SendNewNotificationFragment extends BaseFragment implements SingleCallback{

    FragmentSendNewNotificationBinding mBinding;
    ViewPagerAdapter viewPagerAdapter;
    boolean setCheck = false;
    WorkOutListModel.DataBean dataBean;
    SendNotificationAllAdpter adpter;
    private String userId, userAccessToken, type = "trainer";
    Singleton x = Singleton.getInstance();


    public SendNewNotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_send_new_notification, container, false);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Send Notification");
        Utils.getShapeGradient(mActivity, mBinding.btnSave);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        mBinding.idSelectTab.viewpager.setAdapter(viewPagerAdapter);
        mBinding.idSelectTab.tabLayout.setupWithViewPager(mBinding.idSelectTab.viewpager);
        mBinding.btnSave.setOnClickListener(view -> {
            if (validation()) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJsonClient());
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).sendGymNotification(requestBody)
                        , getCompositeDisposable(), save, this);
            }
        });

        mBinding.idSelectTab.checkboxClientAssign.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    assert getFragmentManager() != null;
                    if (mBinding.idSelectTab.viewpager.getCurrentItem() == 0) {
                        AllSendNotificationFragment.selectAll();
                        type = "all";
                    } else if (mBinding.idSelectTab.viewpager.getCurrentItem() == 1) {
                        TrainerSendNotificationFragment.selectAll();
                        type = "trainer";
                    } else if (mBinding.idSelectTab.viewpager.getCurrentItem() == 2) {
                        ClientSendNotificationFragment.selectAll();
                        type = "client";
                    }
                } else {
                    AllSendNotificationFragment.deselectAll();
                    TrainerSendNotificationFragment.deselectAll();
                    ClientSendNotificationFragment.deselectAll();
                }
            }
        });

        mBinding.idSelectTab.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int currentPosition = 0;

            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int newPosition) {
                mBinding.idSelectTab.checkboxClientAssign.setChecked(false);

                FragmentLifeCycle fragmentToHide = (FragmentLifeCycle) viewPagerAdapter.getItem(currentPosition);
                fragmentToHide.onPauseFragment();

                FragmentLifeCycle fragmentToShow = (FragmentLifeCycle) viewPagerAdapter.getItem(newPosition);
                fragmentToShow.onResumeFragment();
                currentPosition = newPosition;

                if (newPosition == 0) {
                    type = "all";
                } else if (newPosition == 1) {
                    type = "trainer";
                } else {
                    type = "client";
                }
            }
            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                mBinding.sendNotiField.edtNote.setText("");
                AllSendNotificationFragment.deselectAll();
                TrainerSendNotificationFragment.deselectAll();
                ClientSendNotificationFragment.deselectAll();
                mBinding.idSelectTab.checkboxClientAssign.setChecked(false);
                Toast.makeText(getActivity(), "Notification Send Successfully", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.getRoot(), "onFailure", Snackbar.LENGTH_LONG);

    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("ischeck", setCheck);
                fragment = new AllSendNotificationFragment();
                fragment.setArguments(bundle);

            } else if (position == 1) {
                fragment = new TrainerSendNotificationFragment();

            } else if (position == 2) {
                fragment = new ClientSendNotificationFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = "All";
            } else if (position == 1) {
                title = "Trainer";
            } else if (position == 2) {
                title = "Client";
            }
            return title;
        }
    }

    public List<Integer> getTabsData() {
        if (mBinding.idSelectTab.viewpager.getCurrentItem() == 0) {
            return AllSendNotificationFragment.getData();
        } else if (mBinding.idSelectTab.viewpager.getCurrentItem() == 1) {
            return TrainerSendNotificationFragment.getData();
        } else if (mBinding.idSelectTab.viewpager.getCurrentItem() == 2) {
            return ClientSendNotificationFragment.getData();
        }
        return null;
    }

    private String getClientJsonClient() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("message", mBinding.sendNotiField.edtNote.getText().toString().trim());
            jsonObject.put("type", type);
            jsonObject.put("ids", new JSONArray(getTabsData()));
            Log.e("JSON", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private boolean validation() {
        if (mBinding.sendNotiField.edtNote.getText().toString().length() == 0) {
            setEdittextError(mBinding.sendNotiField.txtErrorNotes, getString(R.string.empty_notes));
            return false;
        }

        if (getTabsData() == null || getTabsData().size() <= 0) {
            showSnackBar(mBinding.container, "Please select user", Snackbar.LENGTH_LONG);
            return false;
        }else {
            mBinding.sendNotiField.txtErrorNotes.setText("");
            mBinding.sendNotiField.txtErrorNotes.setVisibility(View.GONE);
        }
        return true;
    }

}
