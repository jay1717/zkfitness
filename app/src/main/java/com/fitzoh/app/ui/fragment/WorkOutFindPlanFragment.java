package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.FindPlanWorkoutAdapter;
import com.fitzoh.app.databinding.FragmentWorkOutFindPlanBinding;
import com.fitzoh.app.model.FindPlanWorkoutModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityFindPlanDetail;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkOutFindPlanFragment extends BaseFragment implements FindPlanWorkoutAdapter.onDataModeified, SingleCallback {


    FragmentWorkOutFindPlanBinding mBinding;
    String userId, userAccessToken;
    List<FindPlanWorkoutModel.DataBean> findPlanList;
    FindPlanWorkoutAdapter findPlanWorkoutAdapter;

    public WorkOutFindPlanFragment() {
        // Required empty public constructor
    }

    public static WorkOutFindPlanFragment newInstance() {
        WorkOutFindPlanFragment fragment = new WorkOutFindPlanFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.find_a_plan));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_work_out_find_plan, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setTextViewStartImage(mActivity, mBinding.edtSearch, R.drawable.ic_search, true);
        findPlanList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        findPlanWorkoutAdapter = new FindPlanWorkoutAdapter(getActivity(), findPlanList, this);
        mBinding.recyclerView.setAdapter(findPlanWorkoutAdapter);

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (findPlanWorkoutAdapter != null)
                    findPlanWorkoutAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return mBinding.getRoot();
    }
    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getFindPlanList("workout")
                    , getCompositeDisposable(), workoutList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void getData(FindPlanWorkoutModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), ActivityFindPlanDetail.class).putExtra("type", "workout").putExtra("data", dataBean));
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                findPlanList = new ArrayList<>();
                FindPlanWorkoutModel workOutListModel = (FindPlanWorkoutModel) o;
                if (workOutListModel.getStatus() == AppConstants.SUCCESS) {

                    if (workOutListModel != null) {
                        findPlanList.addAll(workOutListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (findPlanList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            findPlanWorkoutAdapter = new FindPlanWorkoutAdapter(getActivity(), findPlanList, this);
                            mBinding.recyclerView.setAdapter(findPlanWorkoutAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, workOutListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
