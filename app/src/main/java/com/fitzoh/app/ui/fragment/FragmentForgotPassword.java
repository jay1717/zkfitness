package com.fitzoh.app.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentForgotPasswordBinding;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;


public class FragmentForgotPassword extends BaseFragment implements View.OnClickListener {

    FragmentForgotPasswordBinding mBinding;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            mBinding = DataBindingUtil.inflate(
                    inflater, R.layout.fragment_forgot_password, container, false);
            view = mBinding.getRoot();

            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "");

            prepareLayout();
        }
        setClickEvents();
        setHasOptionsMenu(false);
        return view;
    }

    private void prepareLayout() {
        // Animate Layouts
//        mBinding.touchzenMedia.tvRawvanaVersion.setText(mContext.getResources().getString(R.string.rawvana_version, BuildConfig.VERSION_NAME));
    }

    private void setClickEvents() {
        //Rawvana Social URLs
        mBinding.btnRetrievePwd.setOnClickListener(this);
        mBinding.edtEmail.addTextChangedListener(new MyTaskWatcher(mBinding.edtEmail));

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRetrievePwd:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
//                        showProgressBar().show();
                    }
                } else {
                    Toast.makeText(mContext, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }


    public boolean validation() {

        boolean value = true;
        if (mBinding.edtEmail.getText().toString().length() == 0) {
            setEdittextError(mBinding.txtErrorEmail, getString(R.string.empty_email));
            value = false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(mBinding.edtEmail.getText().toString()).matches()) {
            setEdittextError(mBinding.txtErrorEmail, getString(R.string.invalid_email));
            value = false;
        }

        return value;
    }


    private class MyTaskWatcher implements TextWatcher {
        private View view;

        public MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.edtEmail:
                    if (TextUtils.isEmpty(text)) {
                        setEdittextError(mBinding.txtErrorEmail, getString(R.string.empty_email));
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
                        setEdittextError(mBinding.txtErrorEmail, getString(R.string.invalid_email));
                    } else {
                        mBinding.txtErrorEmail.setText("");
                        mBinding.txtErrorEmail.setVisibility(View.GONE);
                    }
                    break;
            }
        }

    }
}
