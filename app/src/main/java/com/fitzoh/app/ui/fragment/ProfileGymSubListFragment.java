package com.fitzoh.app.ui.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ProfileGymSubListAdapter;
import com.fitzoh.app.databinding.FragmentProfilePackageListBinding;
import com.fitzoh.app.model.ProfileGymSubscriptionModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreatePackageActivity;
import com.fitzoh.app.ui.activity.SubscriptionDetailActivity;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProfileGymSubListFragment extends BaseFragment implements SingleCallback, ProfileGymSubListAdapter.onDataPass, SwipeRefreshLayout.OnRefreshListener {

    FragmentProfilePackageListBinding mBinding;
    String userId, userAccessToken, PhotoPath;
    int trainerId = 0;
    List<ProfileGymSubscriptionModel.DataBean> dataBeanList;
    ProfileGymSubListAdapter profileGymSubListAdapter;

    public ProfileGymSubListFragment() {
        // Required empty public constructor
    }

    public static ProfileGymSubListFragment newInstance(int trainerId, String PhotoPath) {
        ProfileGymSubListFragment fragment = new ProfileGymSubListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("data", trainerId);
        bundle.putString("photopath", PhotoPath);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trainerId = getArguments().getInt("data");
            PhotoPath = getArguments().getString("photopath");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_package_list, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutPackage.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutPackage.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        if (trainerId != 0)
            callSubscriptionList();
        setHasOptionsMenu(true);
        return mBinding.getRoot();
    }

    private void startCreatePackageActivity() {
        Intent intent = new Intent(mActivity, CreatePackageActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        callSubscriptionList();
    }

    private void callSubscriptionList() {
        if (Utils.isOnline(getActivity())) {

            if (getArguments() != null && getArguments().containsKey("data")) {
                trainerId = getArguments().getInt("data");
            }
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSubscriptionList(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getProfileGymSubList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getProfileGymSubList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                dataBeanList = new ArrayList<>();
                ProfileGymSubscriptionModel profileGymSubscriptionModel = (ProfileGymSubscriptionModel) o;
                if (profileGymSubscriptionModel != null) {
                    dataBeanList.addAll(profileGymSubscriptionModel.getData());
                    // workoutListAdapter.notifyDataSetChanged();
                    if (profileGymSubscriptionModel.getData().size() == 0) {
                        mBinding.layoutPackage.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.layoutPackage.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.layoutPackage.recyclerView.setVisibility(View.VISIBLE);
                        mBinding.layoutPackage.imgNoData.setVisibility(View.GONE);
                        profileGymSubListAdapter = new ProfileGymSubListAdapter(dataBeanList, getActivity(), this);
                        mBinding.layoutPackage.recyclerView.setAdapter(profileGymSubListAdapter);
                    }
                } else
                    showSnackBar(mBinding.linear, profileGymSubscriptionModel.getMessage(), Snackbar.LENGTH_LONG);

                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void pass(ProfileGymSubscriptionModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), SubscriptionDetailActivity.class).putExtra("dataBeanClass", dataBean).putExtra("photoPath", PhotoPath));
    }

    @Override
    public void onRefresh() {
        callSubscriptionList();
        mBinding.layoutPackage.swipeContainer.setRefreshing(false);
    }

    /*@Override
    public void pass(ProfileGymSubListAdapter.DataBean dataBean) {
        startActivity(new Intent(getActivity(), PackageDetailActivity.class).putExtra("dataBeanClass", dataBean));
    }*/
}
