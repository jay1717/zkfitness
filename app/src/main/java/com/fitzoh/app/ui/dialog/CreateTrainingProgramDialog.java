package com.fitzoh.app.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogCreateTrainingProgramBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;

public class CreateTrainingProgramDialog extends AppCompatDialogFragment implements SingleCallback {
    DialogCreateTrainingProgramBinding mBinding;
    View view;
    public CompositeDisposable compositeDisposable;
    String userId, userAccessToken;
    public SessionManager session;
    private boolean isEdit = false;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_create_training_program, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            view = mBinding.getRoot();
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        AlertDialog dialog = alertBuilder.create();
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();

        return dialog;
    }


    private void prepareLayout() {
        if (getArguments() != null && getArguments().containsKey("isEdit")) {
            isEdit = getArguments().getBoolean("isEdit");
        }
        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.edtTime.setVisibility(isEdit ? View.VISIBLE : View.GONE);
        mBinding.txtTitle.setText(isEdit ? getString(R.string.training_program_detail) : getString(R.string.training_program));
        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });
        mBinding.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    addWorkout();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
    }


    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    private boolean validation() {
        if (TextUtils.isEmpty(mBinding.edtTrainingProgramName.getText().toString())) {
            showSnackBar("Enter training program name.");
            return false;
        } else if (isEdit && TextUtils.isEmpty(mBinding.edtTime.getText().toString())) {
            showSnackBar("Enter time.");
            return false;
        }
        return true;
    }


    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case addWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent());
                    dismiss();
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }


    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void addWorkout() {
        /*mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        int id = mBinding.spnTemplete.getSelectedItemPosition() == 0 ? 0 : workoutList.get(mBinding.spnTempleteNew.getSelectedItemPosition()).getId();
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addWorkout(mBinding.edtWorkoutName.getText().toString(), id)
                , getCompositeDisposable(), addWorkout, this);*/
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(getString(R.string.something_went_wrong));
    }
}
