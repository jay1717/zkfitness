package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramListAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityFindPlan;
import com.fitzoh.app.ui.activity.AssignTrainingProgramActivity;
import com.fitzoh.app.ui.activity.EditTrainingProgramActivity;
import com.fitzoh.app.ui.dialog.AddTrainingProgram;
import com.fitzoh.app.ui.dialog.RenameDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;


public class FragmentTrainingProgram extends BaseFragment implements TrainingProgramListAdapter.TrainingProgramListener, SingleCallback, SwipeRefreshLayout.OnRefreshListener, RenameDialog.DialogClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentTrainingProgramBinding mBinding;

    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<TrainingProgramList.DataBean> trainingProgramListData = new ArrayList<>();
    TrainingProgramListAdapter trainingProgramListAdapter;


    public FragmentTrainingProgram() {
    }

    public static FragmentTrainingProgram newInstance() {
        return new FragmentTrainingProgram();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.training_program));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_training_program, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.layoutTrainingProgram.fab);
        mBinding.layoutTrainingProgram.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutTrainingProgram.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.layoutTrainingProgram.fab.setOnClickListener(view -> {
            AddTrainingProgram dialog = new AddTrainingProgram();
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", false);
            bundle.putString("trainingTitle", "");
            bundle.putInt("trainingId", 0);
            dialog.setArguments(bundle);
            dialog.setTargetFragment(this, 0);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddTrainingProgram.class.getSimpleName());
            dialog.setCancelable(false);
        });
//        mBinding.toolbar.btnFindplan.setVisibility(View.VISIBLE);
//        mBinding.toolbar.btnFindplan.setOnClickListener(view -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "training_program")));
        setHasOptionsMenu(true);
        trainingProgramListData = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        mBinding.layoutTrainingProgram.recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        trainingProgramListAdapter = new TrainingProgramListAdapter(mActivity, this, trainingProgramListData);
        mBinding.layoutTrainingProgram.recyclerView.setAdapter(trainingProgramListAdapter);
        return mBinding.getRoot();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                break;
        }
        return true;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "training_program")));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(mActivity))
                callTrainingList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callTrainingList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.layoutTrainingProgram.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainingList()
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callTrainingDelete(int trainingId) {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteTrainingProgram(trainingId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.deleteTraining, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callTrainingList();
            else {
                showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }


    @Override
    public void add(TrainingProgramList.DataBean dataBean) {
        mActivity.startActivityForResult(new Intent(mActivity, AssignTrainingProgramActivity.class).putExtra("data", dataBean), 0);
    }

    @Override
    public void edit(TrainingProgramList.DataBean dataBean) {
        /*AddTrainingProgram dialog = new AddTrainingProgram();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEdit", true);
        bundle.putString("trainingTitle", dataBean.getTitle());
        bundle.putInt("trainingId", dataBean.getId());
        dialog.setArguments(bundle);
        dialog.setTargetFragment(this, 0);
        dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddTrainingProgram.class.getSimpleName());
        dialog.setCancelable(false);*/
        session.editor.remove(CLIENT_ID).commit();
        startActivity(new Intent(mActivity, EditTrainingProgramActivity.class).putExtra("data", dataBean));
    }

    @Override
    public void rename(TrainingProgramList.DataBean dataBean) {
        RenameDialog dialogNote = new RenameDialog(0, 0, dataBean.getId(), dataBean.getTitle());
        dialogNote.setListener(this);
        dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), RenameDialog.class.getSimpleName());
        dialogNote.setCancelable(false);
    }


    @Override
    public void delete(TrainingProgramList.DataBean trainingModel) {
        if (trainingModel != null)
            callTrainingDelete(trainingModel.getId());
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainingProgramListData = new ArrayList<>();
                TrainingProgramList trainingProgramList = (TrainingProgramList) o;
                if (trainingProgramList != null && trainingProgramList.getStatus() == AppConstants.SUCCESS && trainingProgramList.getData() != null && trainingProgramList.getData().size() > 0) {
                    trainingProgramListData.addAll(trainingProgramList.getData());
                    mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.GONE);
                    trainingProgramListAdapter = new TrainingProgramListAdapter(mActivity, this, trainingProgramListData);
                    mBinding.layoutTrainingProgram.recyclerView.setAdapter(trainingProgramListAdapter);
                } else {
                    trainingProgramListData.addAll(new ArrayList<>());
                    mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.GONE);
                }
                break;
            case deleteTraining:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deleteTraining = (CommonApiResponse) o;
                if (deleteTraining.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(mActivity))
                        callTrainingList();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteTraining.getMessage(), Snackbar.LENGTH_LONG);
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                trainingProgramListData.addAll(new ArrayList<>());
                mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.VISIBLE);
                mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.GONE);
                break;
        }
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onRefresh() {
        callTrainingList();
    }

    @Override
    public void setClick() {
        callTrainingList();
    }
}
