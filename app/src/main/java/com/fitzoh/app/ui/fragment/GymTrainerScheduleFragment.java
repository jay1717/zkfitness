package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GymTrainerScheduleAdapter;
import com.fitzoh.app.databinding.FragmentScheduleBinding;
import com.fitzoh.app.model.GymTrainerScheduleListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.Objects;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getSchedualList;

/**
 * A simple {@link Fragment} subclass.
 */
public class GymTrainerScheduleFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    FragmentScheduleBinding mBinding;
    String userId, userAccessToken;
    private int trainer_id = 0;

    public GymTrainerScheduleFragment() {
        // Required empty public constructor
    }

    public static GymTrainerScheduleFragment newInstance(Bundle bundle) {
        GymTrainerScheduleFragment fragment = new GymTrainerScheduleFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trainer_id = getArguments().getInt("trainer_id", 0);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.schedule));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_schedule, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.fab.hide();
        if (Utils.isOnline(mActivity))
            callScheduleList();
        else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
        return mBinding.getRoot();
    }

    private void callScheduleList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerScheduleList(trainer_id)
                    , getCompositeDisposable(), getSchedualList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callScheduleList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getSchedualList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                GymTrainerScheduleListModel scheduleListModel = (GymTrainerScheduleListModel) o;
                if (scheduleListModel != null && scheduleListModel.getStatus() == AppConstants.SUCCESS && scheduleListModel.getData() != null && scheduleListModel.getData().size() > 0) {
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.imgNoData.setVisibility(View.GONE);
                    mBinding.recyclerView.setAdapter(new GymTrainerScheduleAdapter(getActivity(), scheduleListModel.getData()));
                } else {
                    showSnackBar(mBinding.mainLayout, Objects.requireNonNull(scheduleListModel).getMessage(), Snackbar.LENGTH_LONG);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setVisibility(View.GONE);
                }
                break;
        }
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.imgNoData.setVisibility(View.VISIBLE);
        mBinding.recyclerView.setVisibility(View.GONE);
        showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onRefresh() {
        callScheduleList();
    }
}
