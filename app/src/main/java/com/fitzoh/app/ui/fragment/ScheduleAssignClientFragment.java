package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientAssignScheduleAdapter;
import com.fitzoh.app.databinding.FragmentScheduleAssignClientBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignSchedule;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleAssignClientFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    List<String> modelList;
    FragmentScheduleAssignClientBinding mBinding;
    String userId, userAccessToken;
    private int id = 0;
    ClientAssignScheduleAdapter adapter;
    private List<ClientListModel.DataBean> data = new ArrayList<>();

    public ScheduleAssignClientFragment() {
        // Required empty public constructor
    }

    public static ScheduleAssignClientFragment newInstance(int id) {
        ScheduleAssignClientFragment ScheduleAssignClientFragment = new ScheduleAssignClientFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("scheduleId", id);
        ScheduleAssignClientFragment.setArguments(bundle);
        return ScheduleAssignClientFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("scheduleId", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_schedule_assign_client, container, false);
        Utils.getShapeGradient(mActivity, mBinding.schedule.btnAssign);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.schedule.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.schedule.recyclerDays.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        mBinding.schedule.swipeContainer.setOnRefreshListener(this);
        mBinding.schedule.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.schedule.btnAssign.setOnClickListener(view -> {
            if (validation()) {
                assignSchedule();
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Utils.isOnline(getActivity()))
            getClient();
        else
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    private List<String> getClient() {
     /*   modelList = new ArrayList<String>();
        modelList.add("Johnny Depp");
        modelList.add("Nick");
        modelList.add("Tom");
        modelList.add("Johnny Depp");
        modelList.add("Nick");
        modelList.add("Tom");
        modelList.add("Johnny Depp");
        modelList.add("Nick");
        modelList.add("Tom");

        mBinding.nutrition.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.nutrition.recyclerView.setAdapter(new ClientAssignSelectoinAdapter(getActivity(), modelList));*/
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientList(null, null, null, id,null)
                , getCompositeDisposable(), single, this);


       /* ArrayList drawableArray = new ArrayList<>(Arrays.asList(R.drawable.monday, R.drawable.tuesday, R.drawable.wednesday,
                R.drawable.thursday, R.drawable.friday, R.drawable.saturday, R.drawable.sunday));


        mBinding.nutrition.recyclerDays.setAdapter(new NutritionDaysAdapter(drawableArray, getActivity()));
*/

        return null;
    }

    private boolean validation() {
        if (data == null || data.size() <= 0) {
            showSnackBar(mBinding.layoutNutrition, "No Data Available", Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private void assignSchedule() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignSchedule(requestBody)
                    , getCompositeDisposable(), assignSchedule, this);


        } else {
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

    }

    private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("client_ids", adapter.getData());
            jsonObject.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
            jsonObject.put("schedule_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                ClientListModel clientListModel = (ClientListModel) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (clientListModel != null && clientListModel.getStatus() == AppConstants.SUCCESS && clientListModel.getData() != null) {
                    data = clientListModel.getData();
                    if (data.size() == 0) {
                        mBinding.schedule.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.schedule.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.schedule.imgNoData.setVisibility(View.GONE);
                        mBinding.schedule.recyclerView.setVisibility(View.VISIBLE);
                        adapter = new ClientAssignScheduleAdapter(getActivity(), data);
                        mBinding.schedule.recyclerView.setAdapter(adapter);
                    }
                } else {
                    showSnackBar(mBinding.layoutNutrition, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
            case assignSchedule:
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.layoutNutrition, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutNutrition, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(getActivity()))
            getClient();
        else
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

        mBinding.schedule.swipeContainer.setRefreshing(false);
    }
}
