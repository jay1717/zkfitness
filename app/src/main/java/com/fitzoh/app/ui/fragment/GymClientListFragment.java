package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GymClientListAdapter;
import com.fitzoh.app.adapter.MyClientListAdapter;
import com.fitzoh.app.databinding.FragmentGymClientListBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.GymClientListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddClientActivity;
import com.fitzoh.app.ui.activity.AddClientGroupActivity;
import com.fitzoh.app.ui.activity.GymAssignTrainerToClientActivity;
import com.fitzoh.app.ui.activity.GymClientProfileActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.liveClients;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class GymClientListFragment extends BaseFragment implements GymClientListAdapter.onDataModeified, SingleCallback, MyClientListAdapter.ClientActivateListener {

    FragmentGymClientListBinding mBinding;
    String userId, userAccessToken;
    List<GymClientListModel.DataBean> clientList;
    GymClientListAdapter gymClientAdapter;
    private int type = 0;

    public GymClientListFragment() {
        // Required empty public constructor
    }

    public static GymClientListFragment newInstance(int type) {
        GymClientListFragment fragment = new GymClientListFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.clients));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_gym_client_list, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        clientList = new ArrayList<>();
        mBinding.fab.setOnClickListener(view -> {
            if(type==0)
            startActivity(new Intent(getActivity(), AddClientActivity.class));
            else {
                showPopUpMenu(mBinding.fab);
            }
        });

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        gymClientAdapter = new GymClientListAdapter(getActivity(), clientList, this);
        mBinding.recyclerView.setAdapter(gymClientAdapter);

        /*if (type == 0)
            mBinding.fab.show();
        else
            mBinding.fab.hide();*/
        return mBinding.getRoot();
    }

    private void showPopUpMenu(View view) {
        PopupMenu popup = new PopupMenu(mActivity, view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(R.menu.menu_my_client, popup.getMenu());
        popup.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_add_client:
                    startActivity(new Intent(getActivity(), AddClientActivity.class));
                    break;
                case R.id.action_add_group:
                    startActivity(new Intent(getActivity(), AddClientGroupActivity.class));
                    break;
            }
            return false;
        });
        popup.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (type == 0)
            callClientList();
        else
            getLiveClients();
    }

    private void getLiveClients() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerClients()
                    , getCompositeDisposable(), liveClients, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callClientList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getGymClientList()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void chat(GymClientListModel.DataBean dataBean) {
        Intent intent = new Intent(getActivity(), ConversationActivity.class);
        intent.putExtra(ConversationUIService.USER_ID, String.valueOf(dataBean.getId()));
        intent.putExtra(ConversationUIService.DISPLAY_NAME, dataBean.getName()); //put it for displaying the title.
        intent.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
        getActivity().startActivity(intent);
    }

    @Override
    public void getData(GymClientListModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), GymClientProfileActivity.class).putExtra("data", dataBean));
    }

    @Override
    public void assign(GymClientListModel.DataBean dataBean) {
        mActivity.startActivityForResult(new Intent(getActivity(), GymAssignTrainerToClientActivity.class).putExtra("id", dataBean.getId()), 1);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                clientList = new ArrayList<>();
                GymClientListModel clientListModel = (GymClientListModel) o;
                if (clientListModel.getStatus() == AppConstants.SUCCESS) {

                    if (clientListModel != null) {
                        clientList.addAll(clientListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (clientList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            gymClientAdapter = new GymClientListAdapter(getActivity(), clientList, this);
                            mBinding.recyclerView.setAdapter(gymClientAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, clientListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case liveClients:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientListModel liveClientsModel = (ClientListModel) o;
                if (liveClientsModel != null && liveClientsModel.getStatus() == AppConstants.SUCCESS && liveClientsModel.getData() != null && liveClientsModel.getData().size() > 0) {
                    mBinding.recyclerView.setAdapter(new MyClientListAdapter(getActivity(), liveClientsModel.getData(), this));
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.imgNoData.setVisibility(View.GONE);
                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void setAssign(int id, int isAssign) {

    }
}
