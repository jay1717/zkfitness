package com.fitzoh.app.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogAddDietBinding;
import com.fitzoh.app.model.AddDietResponse;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addDiet;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class AddDietDialog extends AppCompatDialogFragment implements AdapterView.OnItemSelectedListener, SingleCallback {
    DialogAddDietBinding mBinding;
    View view;
    public CompositeDisposable compositeDisposable;
    private List<DietListModel.DataBean> dietList = new ArrayList<>();
    String userId, userAccessToken;
    public SessionManager session;
    private DialogClickListener dialogClickListener;

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_diet, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.btnTempleteDropdown, mBinding.btnTempleteDropdownNew});
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            view = mBinding.getRoot();

            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();

        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }


    private void prepareLayout() {

        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        getWorkouts();

        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });
        mBinding.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    addDietPlan();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
    }

    private void getWorkouts() {
        mBinding.spnTemplete.setVisibility(View.GONE);
        mBinding.btnTempleteDropdown.setVisibility(View.GONE);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getDietList()
                , getCompositeDisposable(), single, this);

    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    private boolean validation() {
        if (TextUtils.isEmpty(mBinding.edtDietName.getText().toString())) {
            showSnackBar("Enter Diet name.");
            return false;
        } else if (mBinding.spnTemplete.getSelectedItemPosition() < 0) {
            showSnackBar("Select Diet template.");
            return false;
        } else if (mBinding.spnTempleteNew.getVisibility() == View.VISIBLE && mBinding.spnTempleteNew.getSelectedItemPosition() < 0) {
            showSnackBar("Select existing diet template.");
            return false;
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 1) {
            if (dietList != null && dietList.size() > 0) {
                mBinding.spnTempleteNew.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdownNew.setVisibility(View.VISIBLE);
            } else {
                mBinding.spnTempleteNew.setVisibility(View.GONE);
                mBinding.btnTempleteDropdownNew.setVisibility(View.GONE);
            }
        } else {
            mBinding.spnTempleteNew.setVisibility(View.GONE);
            mBinding.btnTempleteDropdownNew.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if(session.getAuthorizedUser(getContext()).getRoleId().equals("1")){
                    mBinding.spnTemplete.setVisibility(View.GONE);
                    mBinding.btnTempleteDropdown.setVisibility(View.GONE);
                }
                else {
                    mBinding.spnTemplete.setVisibility(View.VISIBLE);
                    mBinding.btnTempleteDropdown.setVisibility(View.VISIBLE);
                }
               /* mBinding.spnTemplete.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdown.setVisibility(View.VISIBLE);*/
                DietListModel dietListModel = (DietListModel) o;
                if (dietListModel != null && dietListModel.getStatus() == AppConstants.SUCCESS && dietListModel.getData() != null && dietListModel.getData().size() > 0) {
                    dietList = dietListModel.getData();
                    setSpinner();
                } else {
                    createSpinnerTemplate();
                }
                break;
            case addDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);

                AddDietResponse commonApiResponse = (AddDietResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    if (getTargetFragment() != null)
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent().putExtra("data", commonApiResponse.getData()));
                    dialogClickListener.setClick();
                    dismiss();
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }

    private void createSpinnerTemplate() {
        List<String> list = new ArrayList<>();
        list.add("New Nutrition Plan");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, list);
        mBinding.spnTemplete.setAdapter(adapter1);
        adapter1.setDropDownViewResource(R.layout.spinner_item);
    }


    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void addDietPlan() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        int id = mBinding.spnTemplete.getSelectedItemPosition() == 0 ? 0 : dietList.get(mBinding.spnTempleteNew.getSelectedItemPosition()).getId();
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addDiet(mBinding.edtDietName.getText().toString(), id, session.getRollId() == 1 ? userId : null)
                , getCompositeDisposable(), addDiet, this);
    }

    private void setSpinner() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < dietList.size(); i++) {
            list.add(dietList.get(i).getName());
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, list);
        mBinding.spnTempleteNew.setAdapter(adapter1);
        adapter1.setDropDownViewResource(R.layout.spinner_item);

        String[] templates = getResources().getStringArray(R.array.diet_template_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, templates);
        mBinding.spnTemplete.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mBinding.spnTemplete.setOnItemSelectedListener(this);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);


        switch (apiNames) {
            case single:
                mBinding.spnTemplete.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdown.setVisibility(View.VISIBLE);
                createSpinnerTemplate();
                break;
        }
        showSnackBar(getString(R.string.something_went_wrong));
    }

    public void setListener(DialogClickListener listener) {
        dialogClickListener = listener;
    }

    public interface DialogClickListener {
        public void setClick();
    }
}
