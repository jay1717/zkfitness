package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GalleryImageAdapter;
import com.fitzoh.app.adapter.MultipleImageAdapter;
import com.fitzoh.app.databinding.FragmentUploadPhotoBinding;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.model.TrainerGalleryModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.TransformationAddActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateClientProfile;

/**
 * A simple {@link Fragment} subclass.
 */
public class UploadPhotoFragment extends BaseFragment implements View.OnClickListener, SingleCallback, GalleryImageAdapter.RemoveImageListener {


    FragmentUploadPhotoBinding mBinding;
    public static final int PICK_IMAGES = 1;
    private boolean isFromRegister = true;
    List<TrainerGalleryModel.DataBean> trainerGalleryData = new ArrayList<>();
    String detleteIds = "";
    GalleryImageAdapter trainerProfileGalleryAdapter;

    private ArrayList<String> imagesEncodedList = new ArrayList<>();
    private String userId, userAccessToken;
    private MultipleImageAdapter adapter;

    public UploadPhotoFragment() {
        // Required empty public constructor
    }

    public static UploadPhotoFragment newInstance(Bundle bundle) {
        UploadPhotoFragment fragment = new UploadPhotoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_upload_photo, container, false);
        Utils.getShapeGradient(mActivity, mBinding.uploadPhoto.btnUpload);
        Utils.setAddFabBackground(mActivity, mBinding.uploadPhoto.fab);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.uploadPhoto.txtEmptyText.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        mBinding.txtSkip.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
//        adapter = new MultipleImageAdapter(new ArrayList<>(), (MultipleImageAdapter.RemoveImageListener) this);
        makeRequest();
        if (isFromRegister) {
            mBinding.txtSkip.setVisibility(View.VISIBLE);
            setupToolBar(mBinding.toolbar.toolbar, "Gallery");
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
        } else {
            mBinding.txtSkip.setVisibility(View.GONE);
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Gallery");
            getTrainerGallery();
        }
        mBinding.uploadPhoto.recycler.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        mBinding.txtSkip.setOnClickListener(this);
        mBinding.uploadPhoto.btnUpload.setOnClickListener(this);
        clickEvent();
        return mBinding.getRoot();
    }

    protected void makeRequest() {
//        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RECORD_REQUEST_CODE);
        requestAppPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION}, 100, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }

    private void getTrainerGallery() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerGallery(session.getAuthorizedUser(mContext).getId())
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerGallery, this);
        } else {
            showSnackBar(mBinding.layoutProfile, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private boolean validation() {
        if (trainerGalleryData == null || trainerGalleryData.size() <= 0) {
            showSnackBar(mBinding.layoutProfile, getString(R.string.str_gallery_img), Snackbar.LENGTH_LONG);
            return false;
        } else if (trainerGalleryData.size() > 15) {
            showSnackBar(mBinding.layoutProfile, getString(R.string.validation_max_15_photos), Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private void clickEvent() {
        mBinding.uploadPhoto.fab.setOnClickListener(this);
    }

    private void startImageSelection() {/*
        Intent intent = new Intent();
        intent.setType("image/*");*/
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGES);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        if (requestCode == PICK_IMAGES) {
            if (resultCode == RESULT_OK) {
                mBinding.uploadPhoto.recycler.setVisibility(View.VISIBLE);
                mBinding.uploadPhoto.txtEmptyText.setVisibility(View.GONE);
                if (data.getClipData() != null) {
                    //  adapter.arrayList = new ArrayList<>();
                    imagesEncodedList = new ArrayList<>();
                    ClipData mClipData = data.getClipData();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        // adapter.arrayList.add(uri);
                        Cursor cursor = mActivity.getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();


                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        String imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);

                        TrainerGalleryModel.DataBean imgModel = new TrainerGalleryModel.DataBean();
                        imgModel.setFromGallery(true);
                        imgModel.setImage(uri.toString());
                        imgModel.setPicturePath(imageEncoded);

                        trainerGalleryData.add(imgModel);

                        cursor.close();
                    }
                    setAdapter();
                } else {
                    //  adapter.arrayList = new ArrayList<>();
                    Uri selectedImage = data.getData();


                    Cursor cursor = mContext.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    //  adapter.arrayList.add(selectedImage);
                    imagesEncodedList.add(picturePath);

                    TrainerGalleryModel.DataBean imgModel = new TrainerGalleryModel.DataBean();
                    imgModel.setFromGallery(true);
                    imgModel.setImage(selectedImage.toString());
                    imgModel.setPicturePath(picturePath);

                    trainerGalleryData.add(imgModel);
                    setAdapter();
                }
            }
        }
    }

    private void setAdapter() {
      /*  adapter = new MultipleImageAdapter(adapter.arrayList, this);
        mBinding.uploadPhoto.recycler.setAdapter(adapter);*/

        trainerProfileGalleryAdapter = new GalleryImageAdapter(getActivity(), trainerGalleryData, this);
        mBinding.uploadPhoto.recycler.setAdapter(trainerProfileGalleryAdapter);

    }

    private void callAPI() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);

        List<MultipartBody.Part> parts = new ArrayList<>();
        RequestBody deleteIds = RequestBody.create(MediaType.parse("text/plain"), detleteIds);

        for (int i = 0; i < trainerGalleryData.size(); i++) {



            if (trainerGalleryData.get(i).isFromGallery() && trainerGalleryData.get(i).getPicturePath() != null) {
                File file = new File(trainerGalleryData.get(i).getPicturePath());
                byte[] uploadBytes = Utils.loadImageFromStorage(trainerGalleryData.get(i).getPicturePath());
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);

              //  RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part photo = MultipartBody.Part.createFormData("image[" + i + "]", file.getName(), reqFile);
                parts.add(photo);
            }

        }
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addGalleryImages(parts, deleteIds)
                , getCompositeDisposable(), updateClientProfile, this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                mBinding.uploadPhoto.txtEmptyText.setVisibility(View.GONE);
                startImageSelection();
                break;
            case R.id.btn_upload:
                if (validation()) {
                    callAPI();
                }
                break;
            case R.id.txtSkip:
                startActivity(new Intent(mActivity, TransformationAddActivity.class));
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case updateClientProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                RegisterModel registerModel = (RegisterModel) o;
                if (registerModel.getStatus() == AppConstants.SUCCESS) {
                    if (isFromRegister) {
                        startActivity(new Intent(mActivity, TransformationAddActivity.class));
                        getActivity().finish();
                    } else {
                        showSnackBar(mBinding.layoutProfile, registerModel.getMessage(), Snackbar.LENGTH_LONG);

                    }

                } else {
                    showSnackBar(mBinding.layoutProfile, registerModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case getTrainerGallery:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainerGalleryModel trainerGalleryModel = (TrainerGalleryModel) o;
                if (trainerGalleryModel.getData() != null) {
                    if (trainerGalleryModel.getStatus() == AppConstants.SUCCESS) {
                        trainerGalleryData.addAll(trainerGalleryModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (trainerGalleryModel.getData().size() == 0) {
                            mBinding.uploadPhoto.txtEmptyText.setVisibility(View.VISIBLE);
                            mBinding.uploadPhoto.recycler.setVisibility(View.GONE);
                        } else {
                            mBinding.uploadPhoto.recycler.setVisibility(View.VISIBLE);
                            mBinding.uploadPhoto.txtEmptyText.setVisibility(View.GONE);
                            setAdapter();

                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutProfile, throwable.getMessage() == null ? "onFailure" : throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void removeImage(int position) {
        // adapter.arrayList.remove(position);
        // imagesEncodedList.remove(position);
        if (!trainerGalleryData.get(position).isFromGallery()) {
            if (detleteIds.isEmpty()) {
                detleteIds = "" + trainerGalleryData.get(position).getId();
            } else
                detleteIds = detleteIds + "," + trainerGalleryData.get(position).getId();
        }
        trainerGalleryData.remove(position);
        setAdapter();
    }
}
