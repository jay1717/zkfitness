package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.WorkoutListAdapter;
import com.fitzoh.app.databinding.FragmentWorkoutBinding;
import com.fitzoh.app.model.DeleteWorkOutModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityFindPlan;
import com.fitzoh.app.ui.activity.AssignClientActivity;
import com.fitzoh.app.ui.activity.EditWorkoutActivity;
import com.fitzoh.app.ui.dialog.AddWorkoutDialog;
import com.fitzoh.app.ui.dialog.RenameDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentWorkout#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentWorkout extends BaseFragment implements WorkoutListAdapter.onAddClient, SingleCallback, WorkoutListAdapter.DeleteWorkOut, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, RenameDialog.DialogClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentWorkoutBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<WorkOutListModel.DataBean> workOutListData;
    WorkoutListAdapter workoutListAdapter;


    public FragmentWorkout() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentWorkout.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentWorkout newInstance() {
        FragmentWorkout fragment = new FragmentWorkout();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                break;
        }
        return true;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "workout")));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.workouts));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_workout, container, false);

        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.layoutWorkout.fab.setOnClickListener(view -> {
            AddWorkoutDialog dialog = new AddWorkoutDialog();
            dialog.setTargetFragment(this, 0);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
//        mBinding.toolbar.btnFindplan.setVisibility(View.VISIBLE);
//        mBinding.toolbar.btnFindplan.setOnClickListener(view -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "workout")));
        setHasOptionsMenu(true);
        workOutListData = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
      /*  mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        workoutListAdapter = new WorkoutListAdapter(getActivity(), this, workOutListData, this);
        mBinding.layoutWorkout.recyclerView.setAdapter(workoutListAdapter);*/
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOuList()
                    , getCompositeDisposable(), workoutList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callWorkoutDelete(int workoutId) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                    , getCompositeDisposable(), deleteWorkout, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void add(WorkOutListModel.DataBean dataBean) {
//        Toast.makeText(getActivity(), "clicked...!", Toast.LENGTH_SHORT).show();
        mActivity.startActivityForResult(new Intent(getActivity(), AssignClientActivity.class).putExtra("data", dataBean), 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    @Override
    public void edit(WorkOutListModel.DataBean dataBean) {
        session.editor.remove(CLIENT_ID).commit();
        mActivity.startActivityForResult(new Intent(getActivity(), EditWorkoutActivity.class).putExtra("data", dataBean), 0);
    }

    @Override
    public void rename(WorkOutListModel.DataBean dataBean) {
        RenameDialog dialogNote = new RenameDialog(0, dataBean.getId(), 0, dataBean.getName());
        dialogNote.setListener(this);
        dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), RenameDialog.class.getSimpleName());
        dialogNote.setCancelable(false);
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                workOutListData = new ArrayList<>();
                WorkOutListModel workOutListModel = (WorkOutListModel) o;
                if (workOutListModel.getStatus() == AppConstants.SUCCESS) {

                    if (workOutListModel != null) {
                        workOutListData.addAll(workOutListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (workOutListData.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            workoutListAdapter = new WorkoutListAdapter(getActivity(), this, workOutListData, this);
                            mBinding.layoutWorkout.recyclerView.setAdapter(workoutListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, workOutListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case deleteWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel deleteWorkOutModel = (DeleteWorkOutModel) o;
                if (deleteWorkOutModel.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext()))
                        callWorkoutList();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteWorkOutModel.getMessage(), Snackbar.LENGTH_LONG);

                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void delete(WorkOutListModel.DataBean workOutListModel) {
        if (workOutListModel != null)
            callWorkoutDelete(workOutListModel.getId());
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(mActivity, "dfsfd", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRefresh() {
        callWorkoutList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }

    @Override
    public void setClick() {
        callWorkoutList();
    }
}
