package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ProfileTrainingAdapter;
import com.fitzoh.app.databinding.FragmentClientProfileTrainingprogramBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditTrainingProgramActivity;
import com.fitzoh.app.ui.dialog.AssignToClientDialog;
import com.fitzoh.app.ui.dialog.ClientProfileAssignDialog;
import com.fitzoh.app.ui.dialog.StartWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.unAssignTraining;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClientProfileTrainingProgramFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClientProfileTrainingProgramFragment extends BaseFragment implements SingleCallback, ProfileTrainingAdapter.unAssignClient, AssignToClientDialog.onSave, SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentClientProfileTrainingprogramBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int clientId;
    String userId, userAccessToken;
    List<TrainingProgramList.DataBean> trainingAssignModel;
    List<TrainingProgramList.DataBean> assignToUserModel = new ArrayList<>();
    ProfileTrainingAdapter profileWorkoutAdapter;
    //  ClientListModel.DataBean data;
    List<WorkOutListModel.DataBean> workOutListData;
    ClientListModel.DataBean dataBean;

    private int training_program_id = 0;

    public ClientProfileTrainingProgramFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentWorkout.
     */
    // TODO: Rename and change types and number of parameters
    public static ClientProfileTrainingProgramFragment newInstance() {
        ClientProfileTrainingProgramFragment fragment = new ClientProfileTrainingProgramFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clientId = getArguments().getInt("data");
            // data = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.workouts));
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.training_program));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_profile_trainingprogram, container, false);

        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        trainingAssignModel = new ArrayList<>();
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        profileWorkoutAdapter = new ProfileTrainingAdapter(getActivity(), this, trainingAssignModel);
        mBinding.layoutWorkout.recyclerView.setAdapter(profileWorkoutAdapter);
        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        mBinding.layoutWorkout.fab.show();
        mBinding.layoutWorkout.fab.setOnClickListener(v -> {
            getAllTrainingPrograms();
        });
        return mBinding.getRoot();
    }

    private void getAllTrainingPrograms() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainingList(clientId, 1)
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callTrainingList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callTrainingList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientAssignTrainingList(clientId)
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callUnassignClient(int clientID) {
        /*if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                    , getCompositeDisposable(), deleteWorkout, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }*/
        showSnackBar(mBinding.linear, "Client ID " + clientID, Snackbar.LENGTH_LONG);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callTrainingList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            int position = data.getIntExtra("data", 0);
            training_program_id = assignToUserModel.get(position).getId();
            AssignToClientDialog dialog = new AssignToClientDialog(clientId, 1, this);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AssignToClientDialog.class.getSimpleName());
            dialog.setCancelable(false);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainingAssignModel = new ArrayList<>();
                TrainingProgramList trainingList = (TrainingProgramList) o;
                if (trainingList.getStatus() == AppConstants.SUCCESS) {

                    if (trainingList != null) {
                        trainingAssignModel.addAll(trainingList.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (trainingAssignModel.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            profileWorkoutAdapter = new ProfileTrainingAdapter(getActivity(), this, trainingAssignModel);
                            mBinding.layoutWorkout.recyclerView.setAdapter(profileWorkoutAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, trainingList.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case unAssignTraining:

                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callTrainingList();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainingProgramList model = (TrainingProgramList) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null && model.getData().size() > 0) {
                    assignToUserModel = model.getData();
                    ArrayList<String> workout_names = new ArrayList<>();
                    for (int i = 0; i < assignToUserModel.size(); i++) {
                        workout_names.add(assignToUserModel.get(i).getTitle());
                    }
                    ClientProfileAssignDialog dialog = new ClientProfileAssignDialog();
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("data", workout_names);
                    dialog.setArguments(bundle);
                    dialog.setTargetFragment(this, 100);
                    dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), StartWorkoutDialog.class.getSimpleName());
                    dialog.setCancelable(false);
                } else {
                    showSnackBar(mBinding.linear, "No training program found.", Snackbar.LENGTH_LONG);
                }
                break;
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS) {
                    callTrainingList();
                } else {
                    showSnackBar(mBinding.linear, response.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void startActivity(TrainingProgramList.DataBean dataBean) {
        session.setStringDataByKey(CLIENT_ID, "" + clientId);
        startActivity(new Intent(mActivity, EditTrainingProgramActivity.class).putExtra("data", dataBean));
    }

    @Override
    public void unAssign(TrainingProgramList.DataBean dataBean) {
        if (dataBean != null) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", clientId);
                jsonObject.put("is_assigned", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();

            }
            JSONObject jsonObjectUnassign = new JSONObject();
            try {
                jsonObjectUnassign.put("client_id", clientId);
                jsonObjectUnassign.put("client_ids", jsonArray);
                jsonObjectUnassign.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
                jsonObjectUnassign.put("training_program_id", dataBean.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObjectUnassign.toString());

            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).unAssignTraining(requestBody)
                        , getCompositeDisposable(), unAssignTraining, this);
            } else {
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        }
    }

    @Override
    public void save(int id, String date, int isAllowed, int isAssign) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONObject object = new JSONObject();
        try {
            object.put("user_id", id);
            object.put("is_assigned", isAssign);
            object.put("start_date", date);
            object.put("allowed_to_change_date", isAllowed);
            jsonArray.put(object);
            jsonObject.put("client_ids", jsonArray);

            jsonObject.put("client_group_ids", new JSONArray());
            jsonObject.put("training_program_id", training_program_id);
            jsonObject.put("start_date", dateFormat.format(Calendar.getInstance().getTime()));
            jsonObject.put("allowed_to_change_date", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignTrainingProgram(requestBody)
                    , getCompositeDisposable(), assignWorkout, this);


        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onRefresh() {
        callTrainingList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }
}
