package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SelectFacilitiesAdapter;
import com.fitzoh.app.databinding.FragmentSelectFacilitiesBinding;
import com.fitzoh.app.model.FacilitiesListModel;
import com.fitzoh.app.model.FitnessValuesModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.custom_ui.ChipsViewFacilites;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SelectFacilitiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectFacilitiesFragment extends BaseFragment implements SingleCallback {
    public FragmentSelectFacilitiesBinding mBinding;
    public static SelectFacilitiesFragment facilitiesFragment;
    String userId, userAccessToken;
    SelectFacilitiesAdapter mAdapter;
    List<FitnessValuesModel.DataBean> facilitiesDataList = new ArrayList<>();

    public HashMap<String, FitnessValuesModel.DataBean> selectedFitneeList = new HashMap<String, FitnessValuesModel.DataBean>();


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public SelectFacilitiesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SelectFitnessValueFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SelectFacilitiesFragment newInstance() {
        SelectFacilitiesFragment fragment = new SelectFacilitiesFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_facilities, container, false);
        Utils.getShapeGradient(mActivity, mBinding.btnRegister);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        facilitiesFragment = this;
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        callFitnessValueApi();
        setChipsView();
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Select Facilities Values");
        searchFitnessValue();
        mBinding.btnRegister.setOnClickListener(view -> {
            Intent resultIntent = new Intent();
// TODO Add extras or a data URI to this intent as appropriate.
            resultIntent.putExtra("Selected Data", selectedFitneeList);
            getActivity().setResult(Activity.RESULT_OK, resultIntent);
            getActivity().finish();
        });
    }

    private void searchFitnessValue() {

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mAdapter != null)
                    mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setChipsView() {
        mBinding.chipsView.setTypeface(ResourcesCompat.getFont(getActivity(), R.font.avenirnextltpro_regular));
        // change EditText config
        mBinding.chipsView.getEditText().setCursorVisible(false);
        mBinding.chipsView.getEditText().setTextIsSelectable(false);
        mBinding.chipsView.getEditText().setClickable(false);

        mBinding.chipsView.setChipsValidator(new ChipsViewFacilites.ChipValidator() {
            @Override
            public boolean isValid(FitnessValuesModel.DataBean contact) {
                /*if (contact.getDisplayName().equals("asd@qwe.de")) {
                    return false;
                }*/
                return true;
            }
        });

        mBinding.chipsView.setChipsListener(new ChipsViewFacilites.ChipsListener() {
            @Override
            public void onChipAdded(ChipsViewFacilites.Chip chip) {
                for (ChipsViewFacilites.Chip chipItem : mBinding.chipsView.getChips()) {
                    Log.d("ChipList", "chip: " + chipItem.toString());
                }
            }

            @Override
            public void onChipDeleted(ChipsViewFacilites.Chip chip) {
                selectedFitneeList.remove(String.valueOf(chip.getContact().getId()));
                for (int i = 0; i <facilitiesDataList.size() ; i++) {
                    if(facilitiesDataList.get(i).getId()==chip.getContact().getId()){
                        facilitiesDataList.get(i).setSelected(false);
                        mAdapter.disSelectItem(i);
                    }
                }
                /*if (facilitiesDataList.contains(chip.getContact())) {
                    facilitiesDataList.get(facilitiesDataList.indexOf(chip.getContact())).setSelected(false);
                    mAdapter.disSelectItem(facilitiesDataList.indexOf(chip.getContact()));
                }*/
            }

            @Override
            public void onTextChanged(CharSequence text) {
//                mAdapter.filterItems(text);
            }

            @Override
            public boolean onInputNotRecognized(String text) {

                return false;
            }
        });



    }


    private void callFitnessValueApi() {
        if (Utils.isOnline(getContext())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getFitnessValue()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }


   /* public void addTextListener() {

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();
                String description = query.toString();

                // List<DiaryObject> mDiaryList = mDBhelper.getDiaryInfoSearch(description);

                if (fitnessDataList != null) {
                }
                int sizeOfdata = fitnessDataList.size();
                mAdapter = new SelectFitnessAdapter();
                HashMap<String, ArrayList<FitnessValuesModel.DataBean>> hashMapFitnessData = new HashMap<String, ArrayList<FitnessValuesModel.DataBean>>();

                HashMap<Integer, String> hashCategory = mDBhelper.getCategoryHashMap();

                for (int i = 0; i < sizeOfdata; i++) {
                    FitnessValuesModel.DataBean fitnessObject = fitnessDataList.get(i);
                    String catName = hashCategory.get(fitnessObject.get());
                    ArrayList<DiaryObject> arrDiary = hashMapDiary.get(catName);
                    if (arrDiary != null && arrDiary.size() > 0) {
                        arrDiary.add(diaryObject);
                    } else {
                        arrDiary = new ArrayList<DiaryObject>();
                        arrDiary.add(diaryObject);
                    }
                    hashMapDiary.put(catName, arrDiary);
                    *//*
                    List<DiaryObject> arrDiary = mDBhelper.getDiarytList(diaryCategoryObject.getId());
                   *//*

                }
                if (hashMapDiary != null && hashMapDiary.size() > 0) {


                    Iterator myVeryOwnIterator = hashMapDiary.keySet().iterator();
                    while (myVeryOwnIterator.hasNext()) {
                        String key = (String) myVeryOwnIterator.next();
                        ArrayList<DiaryObject> value = (ArrayList<DiaryObject>) hashMapDiary.get(key);
                        linAddCategory.setVisibility(View.GONE);
                        sectionAdapter.addSection(new ContactsSection(key, value));

                        // Toast.makeText(ctx, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                    }


                }
                recyclerView.setAdapter(sectionAdapter);
// data set changed
            }
        });
    }
*/

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                FitnessValuesModel facilitiesListModel = (FitnessValuesModel) o;
                if (facilitiesListModel != null && facilitiesListModel.getData() != null && facilitiesListModel.getData().size() > 0) {
                    facilitiesDataList.clear();
                    facilitiesDataList = facilitiesListModel.getData();
                    setPrevious();
                    mBinding.recyclerPeople.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mAdapter = new SelectFacilitiesAdapter(getContext(), facilitiesDataList);
                    mBinding.recyclerPeople.setAdapter(mAdapter);
                    for (int i = 0; i < facilitiesListModel.getData().size(); i++) {
                      /*  KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(fitnessValuesModel.getData().get(i).getId());
                        h.setName(fitnessValuesModel.getData().get(i).getValue());
                        h.setSelected(false);
                        listArraySpinner.add(h);*/
                    }

                }
                break;
        }
    }

    private void setPrevious() {
        if (getArguments() != null && getArguments().containsKey("peopleList")) {
            selectedFitneeList = (HashMap<String, FitnessValuesModel.DataBean>) getArguments().getSerializable("peopleList");
            if (selectedFitneeList.size() > 0) {
                Iterator myVeryOwnIterator = selectedFitneeList.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    FitnessValuesModel.DataBean value = selectedFitneeList.get(key);
                    if(facilitiesDataList!=null && facilitiesDataList.size()>0){
                        for (int i = 0; i < facilitiesDataList.size(); i++) {
                            if (value.getId()==facilitiesDataList.get(i).getId()){
                                value.setSelected(true);
                                facilitiesDataList.get(i).setSelected(true);
                                mBinding.chipsView.addChip(value.getValue(), "", value, false);

                            }
                        }
                    }

                }
            }
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, "onFailure", Snackbar.LENGTH_LONG);

    }
}
