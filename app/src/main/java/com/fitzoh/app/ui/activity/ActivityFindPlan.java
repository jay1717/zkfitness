package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.AchievementsFragment;
import com.fitzoh.app.ui.fragment.DietFindPlanFragment;
import com.fitzoh.app.ui.fragment.TrainingFindPlanFragment;
import com.fitzoh.app.ui.fragment.WorkOutFindPlanFragment;

public class ActivityFindPlan extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        Intent intent = getIntent();
        if(null!=intent){
            String intentData = intent.getExtras().getString("type");
            switch (intentData){
                case "workout" :
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, WorkOutFindPlanFragment.newInstance(), "WorkoutFindPlan")
                            .commit();
                    break;
                case "diet_plan":
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, DietFindPlanFragment.newInstance(), "DietFindPlan")
                            .commit();
                    break;
                case "training_program" :
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, TrainingFindPlanFragment.newInstance(), "TrainerFindPlan")
                            .commit();
                    break;
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
