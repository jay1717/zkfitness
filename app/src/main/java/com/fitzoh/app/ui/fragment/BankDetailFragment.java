package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentBankDetailBinding;
import com.fitzoh.app.model.BankDetailModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.IFSCDetail;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AppThemeSettingActivity;
import com.fitzoh.app.ui.activity.NavigationClientMainActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class BankDetailFragment extends BaseFragment implements AdapterView.OnItemSelectedListener, SingleCallback {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String userId, userAccessToken;
    FragmentBankDetailBinding mBinding;
    boolean isFromRegister = true;
    boolean isEditable = false;
    MenuItem menuItem;

    public BankDetailFragment() {
        // Required empty public constructor
    }


    public static BankDetailFragment newInstance() {
        BankDetailFragment fragment = new BankDetailFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_bank_detail, container, false);
        Utils.getShapeGradient(mActivity, mBinding.layoutBankDetail.btnSaveDetail);
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER");
        }
        if (isFromRegister) {
            mBinding.txtSkip.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
            setupToolBar(mBinding.toolbar.toolbar, "Setup Bank Detail");
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
            mBinding.txtSkip.setVisibility(View.VISIBLE);
            setHasOptionsMenu(false);
            mBinding.layoutBankDetail.btnSaveDetail.setVisibility(View.VISIBLE);
        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Setup Bank Detail");
            setHasOptionsMenu(false);
            callBankDetailList();
            mBinding.layoutBankDetail.btnSaveDetail.setVisibility(View.VISIBLE);

        }


        mBinding.layoutBankDetail.edtBankName.setKeyListener(null);
        mBinding.layoutBankDetail.edtBranchCode.setKeyListener(null);


        setClickListener();
        return mBinding.getRoot();
    }

    private void callBankDetailList() {

        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getBankDetail()
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getBankDetail, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setClickListener() {
        mBinding.layoutBankDetail.btnSaveDetail.setOnClickListener(view -> {
            if (validation()) {
                callSaveAPI();
            }
        });
        mBinding.layoutBankDetail.edtIFSCCode.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchData();
                return true;
            }
            return false;
        });


        mBinding.txtSkip.setOnClickListener(view -> {
            if (session.getAuthorizedUser(getActivity()).isGym_trainer()) {
                Intent intent;
                if (session.getRollId() == 1) {
                    intent = new Intent(mActivity, NavigationClientMainActivity.class);
                } else {
                    intent = new Intent(mActivity, NavigationMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                mActivity.finish();
            } else {
                Intent intent = new Intent(mActivity, AppThemeSettingActivity.class);
                intent.putExtra("IS_LOGIN", true);
                startActivity(intent);
                getActivity().finish();
            }
        });

        mBinding.layoutBankDetail.btnDropdownIfsc.setOnClickListener(view -> {
            searchData();
        });
    }

    private void searchData() {
        if (mBinding.layoutBankDetail.edtIFSCCode.getText().length() > 0) {
            WebserviceBuilder builder = new Retrofit.Builder()
                    .baseUrl("https://ifsc.razorpay.com/")
                    .addConverterFactory(GsonConverterFactory.create()).
                            addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                            build().create(WebserviceBuilder.class);
            ObserverUtil.subscribeToSingle(builder.getBankDetailFromIFSC(mBinding.layoutBankDetail.edtIFSCCode.getText().toString())
                    , getCompositeDisposable(), single, this);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_set, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.action_edit), R.drawable.ic_edit);
        mActivity.invalidateOptionsMenu();
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuItem = item;
        switch (item.getItemId()) {
            case R.id.action_edit:
                isEditable = !isEditable;
                setEditTextEnable(isEditable);
                break;
        }
        return true;
    }

    private void setEditTextEnable(boolean isEditable) {
        mBinding.layoutBankDetail.edtAccountHolderName.setEnabled(isEditable);
        mBinding.layoutBankDetail.edtAccountNumber.setEnabled(isEditable);
        mBinding.layoutBankDetail.edtIFSCCode.setEnabled(isEditable);
        if (isEditable) {
            mBinding.layoutBankDetail.btnSaveDetail.setVisibility(View.VISIBLE);
            Utils.setMenuItemDrawable(mActivity, menuItem, R.drawable.ic_cancel);
        } else {
            mBinding.layoutBankDetail.btnSaveDetail.setVisibility(View.GONE);
            Utils.setMenuItemDrawable(mActivity, menuItem, R.drawable.ic_edit);
        }

    }*/


    private void callSaveAPI() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveBankDetails(mBinding.layoutBankDetail.edtBankName.getText().toString(), mBinding.layoutBankDetail.edtAccountHolderName.getText().toString(),
                    mBinding.layoutBankDetail.edtAccountNumber.getText().toString(), mBinding.layoutBankDetail.edtIFSCCode.getText().toString(), mBinding.layoutBankDetail.edtBranchCode.getText().toString())
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.saveBankDetail, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private boolean validation() {
        if (mBinding.layoutBankDetail.edtIFSCCode.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorIFSCCode, "IFSC Code must not be empty");
            return false;
        }
        if (mBinding.layoutBankDetail.edtBankName.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorBankName, "Bank name must not be empty");
            return false;

        }
        if (mBinding.layoutBankDetail.edtAccountHolderName.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorName, "Account holder name must not be empty");
            return false;

        }
        if (mBinding.layoutBankDetail.edtAccountNumber.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorAccountNumber, "Account number must not be empty");
            return false;

        } else if (mBinding.layoutBankDetail.edtAccountNumber.getText().toString().length() < 5) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorAccountNumber, "Account number can not be less than 5 digits");
            return false;

        } else if (mBinding.layoutBankDetail.edtAccountNumber.getText().toString().length() > 25) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorAccountNumber, "Account number can not be more than 25 digits");
            return false;

        }
        if (mBinding.layoutBankDetail.edtBranchCode.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutBankDetail.txtErrorBranchCode, "Branch code must not be empty");
            return false;

        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                IFSCDetail ifscDetail = (IFSCDetail) o;
                if (ifscDetail != null) {
                    mBinding.layoutBankDetail.edtBranchCode.setText(ifscDetail.getBRANCH());
                    mBinding.layoutBankDetail.edtBankName.setText(ifscDetail.getBANK());
                } else {
                    mBinding.layoutBankDetail.edtBranchCode.setText("");
                    mBinding.layoutBankDetail.edtBankName.setText("");
                }
                break;

            case saveBankDetail:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse saveBankDetail = (CommonApiResponse) o;
                if (saveBankDetail.getStatus() == AppConstants.SUCCESS) {
                    if (isFromRegister) {
                        if (session.getAuthorizedUser(getActivity()).isGym_trainer()) {
                            Intent intent;
                            if (session.getRollId() == 1) {
                                intent = new Intent(mActivity, NavigationClientMainActivity.class);
                            } else {
                                intent = new Intent(mActivity, NavigationMainActivity.class);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            mActivity.finish();
                        } else {
                            Intent intent = new Intent(mActivity, AppThemeSettingActivity.class);
                            intent.putExtra("IS_LOGIN", true);
                            startActivity(intent);
                            getActivity().finish();
                        }
                    } else {
                        getActivity().finish();
                        showSnackBar(mBinding.linear, "Bank detail updated successfully.", Snackbar.LENGTH_LONG);
                    }
                } else {
                    showSnackBar(mBinding.linear, saveBankDetail.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case getBankDetail:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                BankDetailModel bankDetailModel = (BankDetailModel) o;
                if (bankDetailModel.getStatus() == AppConstants.SUCCESS && bankDetailModel.getData() != null) {
                    mBinding.layoutBankDetail.edtIFSCCode.setText(bankDetailModel.getData().getIfscCode());
                    mBinding.layoutBankDetail.edtAccountNumber.setText(bankDetailModel.getData().getAccountNumber());
                    mBinding.layoutBankDetail.edtBranchCode.setText(bankDetailModel.getData().getBranch());
                    mBinding.layoutBankDetail.edtAccountHolderName.setText(bankDetailModel.getData().getAccountHolderName());
                    mBinding.layoutBankDetail.edtBankName.setText(bankDetailModel.getData().getBankName());

                } else {
                    showSnackBar(mBinding.linear, bankDetailModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case single:
                mBinding.layoutBankDetail.edtBranchCode.setText("");
                mBinding.layoutBankDetail.edtBankName.setText("");
                break;

        }
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
