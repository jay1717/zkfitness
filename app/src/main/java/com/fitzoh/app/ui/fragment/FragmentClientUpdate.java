package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientUpdateDetailAdapter;
import com.fitzoh.app.databinding.FragmentClientUpdateBinding;
import com.fitzoh.app.model.ClientUpdateDetailData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentClientUpdate#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentClientUpdate extends BaseFragment implements SingleCallback, AdapterView.OnItemSelectedListener {

    FragmentClientUpdateBinding mBinding;
    private Typeface typeface;
    String userId, userAccessToken;
    HashMap<Integer, String> numMap;
    ArrayList<Entry> valuesWorkout = new ArrayList<Entry>();
    private int clientID = 0;
    boolean isGym = false;

    public FragmentClientUpdate() {
        // Required empty public constructor
    }


    public static FragmentClientUpdate newInstance(Bundle bundle) {
        FragmentClientUpdate fragmentClientUpdate = new FragmentClientUpdate();
        fragmentClientUpdate.setArguments(bundle);
        return fragmentClientUpdate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clientID = getArguments().getInt("clientID", 0);
            isGym = getArguments().getBoolean("isGym", false);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.client_update));


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_update, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        typeface = ResourcesCompat.getFont(mContext, R.font.avenirnextltpro_medium);
        mBinding.layoutClientReport.recyclerView.setAdapter(new ClientUpdateDetailAdapter(mActivity, new ArrayList<>()));
        mBinding.layoutClientReport.txtProgress.setText(Html.fromHtml("0<small>%</small>"));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.toolbar.tvTitle.setText(getString(R.string.client_update));
//        mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        com.fitzoh.app.utils.Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.imgBack.setOnClickListener(v -> getActivity().onBackPressed());
        mBinding.layoutClientReport.txtProgress.setTextColor(((BaseActivity) mContext).res.getColor(R.color.colorAccent));
        com.fitzoh.app.utils.Utils.createProgressBackground(mActivity, mBinding.layoutClientReport.progressBar);
        com.fitzoh.app.utils.Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        com.fitzoh.app.utils.Utils.setImageBackground(mActivity, mBinding.imgCall, R.drawable.ic_call_icon);
        com.fitzoh.app.utils.Utils.setImageBackground(mActivity, mBinding.layoutClientReport.imgChat, R.drawable.ic_chat_line);
        getClientUpdate();
        return mBinding.getRoot();

    }

    private void getClientUpdate() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        if (isGym)
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).gymClientUpdateDetail(clientID)
                    , getCompositeDisposable(), single, this);
        else
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).clientUpdateDetail(clientID)
                    , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientUpdateDetailData data = (ClientUpdateDetailData) o;
                if (data != null && data.getStatus() == AppConstants.SUCCESS && data.getData() != null) {
                    ClientUpdateDetailData.DataBean dataBean = data.getData();
                    mBinding.layoutClientReport.txtClientName.setText(dataBean.getName());
                    mBinding.layoutClientReport.txtProgress.setText(dataBean.getPercentage_taken() + "" + Html.fromHtml("<small>%</small>"));
                    mBinding.layoutClientReport.progressBar.setProgress((int) dataBean.getPercentage_taken());

                    mBinding.imgCall.setOnClickListener(view -> {
                        if(dataBean.getMobile_number()!=null)
                        com.fitzoh.app.utils.Utils.makeCall(this, dataBean.getDate());
                    });
                    mBinding.layoutClientReport.imgChat.setOnClickListener(view -> {
                        Intent intent1 = new Intent(getActivity(), ConversationActivity.class);
                        intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(clientID));
                        intent1.putExtra(ConversationUIService.DISPLAY_NAME, dataBean.getName()); //put it for displaying the title.
                        intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
                        getActivity().startActivity(intent1);
                    });
                    if (dataBean.getTodays_workout() != null && dataBean.getTodays_workout().size() > 0)
                        setSpinner(dataBean);
                    else {
                        createSpinnerTemplate();
                    }
                    //  mBinding.layoutClientReport.txtWorkoutName.setText(dataBean.getWorkout_name());
                    mBinding.layoutClientReport.txtDate.setText(dataBean.getDate());

                    if (dataBean.getPhoto() != null && !dataBean.getPhoto().equalsIgnoreCase(""))
                        com.fitzoh.app.utils.Utils.setImage(mActivity, mBinding.layoutClientReport.imgClient, dataBean.getPhoto());
                    if (dataBean.getTodays_diet() != null && dataBean.getTodays_diet().size() > 0) {
                        mBinding.layoutClientReport.recyclerView.setVisibility(View.VISIBLE);
                        mBinding.layoutClientReport.recyclerView.setAdapter(new ClientUpdateDetailAdapter(mActivity, dataBean.getTodays_diet()));
                    } else {
                        mBinding.layoutClientReport.recyclerView.setVisibility(View.GONE);
                    }
                  /*  if (dataBean.getGraph() != null && dataBean.getGraph().size() > 0) {
                        setGraphValues(dataBean.getGraph());
                    } else {
                        mBinding.layoutClientReport.chart1.clear();
                    }*/

                }
                break;
        }
    }

    private void setSpinner(ClientUpdateDetailData.DataBean dataBean) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < dataBean.getTodays_workout().size(); i++) {
            list.add(dataBean.getTodays_workout().get(i).getWorkout_name());
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, list);
        mBinding.layoutClientReport.spnWorkout.setAdapter(adapter1);
        adapter1.setDropDownViewResource(R.layout.spinner_item);
        mBinding.layoutClientReport.spnWorkout.setOnItemSelectedListener(this);
        mBinding.layoutClientReport.ratingBar.setRating(dataBean.getTodays_workout().get(0).getRating());
        if (dataBean.getTodays_workout().get(0).getGraph() != null && dataBean.getTodays_workout().get(0).getGraph().size() > 0) {
            setGraphValues(dataBean.getTodays_workout().get(0).getGraph());
        } else {
            mBinding.layoutClientReport.chart1.clear();
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        createSpinnerTemplate();
    }

    private void createSpinnerTemplate() {
        List<String> list = new ArrayList<>();
        list.add("Workout Name");
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, list);
        mBinding.layoutClientReport.spnWorkout.setAdapter(adapter1);
        adapter1.setDropDownViewResource(R.layout.spinner_item);
    }

    private void setGraphValues(List<ClientUpdateDetailData.DataBean.TodaysWorkoutBean.GraphBean> data) {
        numMap = new HashMap<>();
        valuesWorkout = new ArrayList<Entry>();
        for (int i = 0; i < data.size(); i++) {
            if (i == 0) {
                numMap.put(i, "");
                numMap.put(i + 1, data.get(i).getKey());
                valuesWorkout.add(new Entry(i, 0, null));
                valuesWorkout.add(new Entry(i + 1, Float.parseFloat(data.get(i).getValue()), null));
            } else {
                numMap.put(i + 1, data.get(i).getKey());
                valuesWorkout.add(new Entry(i + 1, Float.parseFloat(data.get(i).getValue()), null));
            }
        }
        setLineChart();
    }

    private void setLineChart() {
        mBinding.layoutClientReport.chart1.setDrawGridBackground(false);
        mBinding.layoutClientReport.chart1.setDrawBorders(false);
        mBinding.layoutClientReport.chart1.getLegend().setEnabled(false);
        mBinding.layoutClientReport.chart1.setAutoScaleMinMaxEnabled(false);
        mBinding.layoutClientReport.chart1.setTouchEnabled(true);
        mBinding.layoutClientReport.chart1.setDragEnabled(false);
        mBinding.layoutClientReport.chart1.setScaleEnabled(false);
        mBinding.layoutClientReport.chart1.setPinchZoom(false);
        mBinding.layoutClientReport.chart1.setDoubleTapToZoomEnabled(false);
        mBinding.layoutClientReport.chart1.getAxisRight().setEnabled(false);
        mBinding.layoutClientReport.chart1.getDescription().setEnabled(false);
        mBinding.layoutClientReport.chart1.setXAxisRenderer(new CustomXAxisRenderer(mBinding.layoutClientReport.chart1.
                getViewPortHandler(), mBinding.layoutClientReport.chart1.getXAxis(),
                mBinding.layoutClientReport.chart1.getTransformer(YAxis.AxisDependency.LEFT)));
        final YAxis yAxis = mBinding.layoutClientReport.chart1.getAxisLeft();
//        yAxis.setLabelCount(6, true);
        yAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yAxis.setValueFormatter(new DefaultAxisValueFormatter(1));
        yAxis.setTextColor(Color.GRAY);
        yAxis.setTextSize(5f); //not in your original but added
        yAxis.setTypeface(typeface);
        yAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));
        yAxis.setAxisLineColor(Color.TRANSPARENT);
        yAxis.setAxisMinimum(0); //not in your original but added

        final XAxis xAxis = mBinding.layoutClientReport.chart1.getXAxis();
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); //changed to match your spec
        xAxis.setTextColor(Color.GRAY);
        xAxis.disableGridDashedLine();
        xAxis.setDrawGridLines(true);
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.light_gray_field));

        xAxis.setAxisLineColor(Color.TRANSPARENT);
        xAxis.setValueFormatter((value, axis) -> numMap.get((int) value));
        xAxis.setLabelCount(valuesWorkout.size());
        //xAxis.setAvoidFirstLastClipping(true);
        setData();
       xAxis.setLabelRotationAngle(-45);
        mBinding.layoutClientReport.chart1.getLegend().setWordWrapEnabled(true);


    }

    private void setData() {
        LineDataSet set1;
        if (mBinding.layoutClientReport.chart1.getData() != null &&
                mBinding.layoutClientReport.chart1.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.layoutClientReport.chart1.getData().getDataSetByIndex(0);
            set1.setValues(valuesWorkout);
            mBinding.layoutClientReport.chart1.getData().notifyDataChanged();
            mBinding.layoutClientReport.chart1.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(valuesWorkout, "DataSet 1");
            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(0);
            set1.setDrawCircles(false);
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);

            set1.setValueTextSize(5f);
            set1.setValueTypeface(typeface);
            set1.setDrawValues(false);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormSize(15.f);

            if (Utils.getSDKInt() >= 18) {
                GradientDrawable gradientDrawable = com.fitzoh.app.utils.Utils.getShapeGradient(mActivity, 0);
                set1.setFillDrawable(gradientDrawable);
            } else {
                set1.setFillColor(Color.BLACK);
            }

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);
            mBinding.layoutClientReport.chart1.setExtraBottomOffset(60);
            mBinding.layoutClientReport.chart1.getLegend().setWordWrapEnabled(true);
            // set data
            mBinding.layoutClientReport.chart1.setData(data);
            mBinding.layoutClientReport.chart1.invalidate();
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class CustomXAxisRenderer extends XAxisRenderer {

        CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
            super(viewPortHandler, xAxis, trans);
        }

        @Override
        protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
            Utils.drawXAxisValue(c, formattedLabel, x, y, mAxisLabelPaint, anchor, angleDegrees);
        }
    }

}

