package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ProfileWorkoutAdapter;
import com.fitzoh.app.databinding.FragmentWorkoutAssignBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DeleteWorkOutModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditWorkoutActivity;
import com.fitzoh.app.ui.dialog.ClientProfileAssignDialog;
import com.fitzoh.app.ui.dialog.StartWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.unAssignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentProfileAssignedWorkout#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProfileAssignedWorkout extends BaseFragment implements SingleCallback, ProfileWorkoutAdapter.unAssignClient, SwipeRefreshLayout.OnRefreshListener {

    private static int clientId;
    FragmentWorkoutAssignBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<WorkOutListModel.DataBean> workOutListData;
    List<WorkOutListModel.DataBean> assignWorkOutListData = new ArrayList<>();
    ProfileWorkoutAdapter profileWorkoutAdapter;
    ClientListModel.DataBean dataBean;

    public FragmentProfileAssignedWorkout() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FragmentProfileAssignedWorkout newInstance(int id) {
        FragmentProfileAssignedWorkout fragment = new FragmentProfileAssignedWorkout();

        clientId = id;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //   dataBean = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.workouts));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_workout_assign, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        workOutListData = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        profileWorkoutAdapter = new ProfileWorkoutAdapter(getActivity(), this, workOutListData, true);
        mBinding.layoutWorkout.recyclerView.setAdapter(profileWorkoutAdapter);
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        mBinding.layoutWorkout.fab.show();
        mBinding.layoutWorkout.fab.setOnClickListener(v -> {
            getWorkouts();
        });
        return mBinding.getRoot();
    }

    private void getWorkouts() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOuList(1, String.valueOf(clientId))
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(mActivity))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkoutList(clientId)
                    , getCompositeDisposable(), workoutList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callUnassignClient(int clientID) {
      /* if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                    , getCompositeDisposable(), unAssignWorkout, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
        showSnackBar(mBinding.getRoot(), "Client ID " + clientID, Snackbar.LENGTH_LONG);*/

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                assignWorkout(data.getIntExtra("data", 0));
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void assignWorkout(int data) {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson(data));
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignWorkout(requestBody)
                    , getCompositeDisposable(), assignWorkout, this);


        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private String getClientJson(int id) {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject obj = new JSONObject();
            obj.put("user_id", clientId);
            obj.put("is_assigned", 1);
            jsonArray.put(obj);
            jsonObject.put("client_ids", jsonArray);
            jsonObject.put("client_group_ids", new JSONArray());
            jsonObject.put("workout_id", assignWorkOutListData.get(id).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                workOutListData = new ArrayList<>();
                WorkOutListModel profileWorkoutModel = (WorkOutListModel) o;
                if (profileWorkoutModel.getStatus() == AppConstants.SUCCESS) {

                    if (profileWorkoutModel != null) {
                        int size = profileWorkoutModel.getData().size();
                        for (int i = 0; i < size; i++) {
                            if (profileWorkoutModel.getData().get(i).getIs_assigned() == 1) {
                                workOutListData.add(profileWorkoutModel.getData().get(i));
                            }
                        }
                        // workoutListAdapter.notifyDataSetChanged();
                        if (workOutListData.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            profileWorkoutAdapter = new ProfileWorkoutAdapter(getActivity(), this, workOutListData, true);
                            mBinding.layoutWorkout.recyclerView.setAdapter(profileWorkoutAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, profileWorkoutModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case deleteWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel deleteWorkOutModel = (DeleteWorkOutModel) o;
                if (deleteWorkOutModel.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext()))
                        callWorkoutList();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteWorkOutModel.getMessage(), Snackbar.LENGTH_LONG);

                break;

            case unAssignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callWorkoutList();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkOutListModel listModel = (WorkOutListModel) o;
                if (listModel != null && listModel.getStatus() == AppConstants.SUCCESS /*&& listModel.getData() != null && listModel.getData().size() > 0*/) {
                    assignWorkOutListData = listModel.getData();
                    ArrayList<String> workout_names = new ArrayList<>();
                    if( assignWorkOutListData != null && assignWorkOutListData.size() > 0){
                        for (int i = 0; i < assignWorkOutListData.size(); i++) {
                            workout_names.add(assignWorkOutListData.get(i).getName());
                        }
                    }

                    ClientProfileAssignDialog dialog = new ClientProfileAssignDialog();
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("data", workout_names);
                    dialog.setArguments(bundle);
                    dialog.setTargetFragment(this, 100);
                    dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), StartWorkoutDialog.class.getSimpleName());
                    dialog.setCancelable(false);
                } else {
                    showSnackBar(mBinding.linear, listModel.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS) {
                    callWorkoutList();
                } else {
                    showSnackBar(mBinding.linear, response.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void unAssign(WorkOutListModel.DataBean dataBean) {
        if (dataBean != null) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", clientId);
                jsonObject.put("is_assigned", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject jsonObjectUnassign = new JSONObject();
            try {
                jsonObjectUnassign.put("client_id", clientId);
                jsonObjectUnassign.put("client_ids", jsonArray);
                jsonObjectUnassign.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
                jsonObjectUnassign.put("workout_id", dataBean.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObjectUnassign.toString());

            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).unAssignWorkout(requestBody)
                        , getCompositeDisposable(), unAssignWorkout, this);
            } else {
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        }
    }

    @Override
    public void startWorkout(WorkOutListModel.DataBean dataBean) {
//        session.editor.remove(CLIENT_ID).commit();
        session.setStringDataByKey(CLIENT_ID, "" + clientId);
        startActivity(new Intent(mActivity, EditWorkoutActivity.class).putExtra("data", dataBean).putExtra("is_workout_client",true));
    }

    @Override
    public void onRefresh() {
        callWorkoutList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }
}
