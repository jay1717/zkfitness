package com.fitzoh.app.ui.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentLoginBinding;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.LoginActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.ui.activity.VarificationActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.login;


public class FragmentLogin extends BaseFragment implements View.OnClickListener, SingleCallback {

    FragmentLoginBinding mBinding;
    View view;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 1001;
    private static final String EMAIL = "email";
    CallbackManager callbackManager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
            view = mBinding.getRoot();
            FacebookSdk.sdkInitialize(getActivity());

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
//            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "");
            prepareLayout();
        }


        callbackManager = CallbackManager.Factory.create();
        mBinding.login.loginButtonFacebook.setReadPermissions("email");
        mBinding.login.loginButtonFacebook.setFragment(this);
        mBinding.login.loginButtonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);

//                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        String firstName = null, lastName = null, email = null, id = null;
                        // int id = 0;
                        Bundle bFacebookData = getFacebookData(object);
                        try {
                            firstName = object.getString("first_name");
                            lastName = object.getString("last_name");
                            email = object.getString("email");
                            id = object.getString("id");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
//                Toast.makeText(getActivity(), "cancel", Toast.LENGTH_SHORT).show();
                Log.i("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("LoginActivity", exception.toString());
            }
        });

        calculateHashKey("com.fitzoh.app");
        setClickEvents();
        setHasOptionsMenu(false);
        return view;
    }

    private void calculateHashKey(String yourPackageName) {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    yourPackageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void prepareLayout() {
        // Animate Layouts
//        mBinding.touchzenMedia.tvRawvanaVersion.setText(mContext.getResources().getString(R.string.rawvana_version, BuildConfig.VERSION_NAME));
        String text = "<font color=#000000>Don't have an account?</font> <font color=#309DD6> Create</font>";
        mBinding.login.txtRegister.setText(Html.fromHtml(text));

        /*AddWorkoutDialog dialog = new AddWorkoutDialog();
        dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutDialog.class.getSimpleName());
        ((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, new FragmentMedicalForm(), true, true);
        */
    }

    private void setClickEvents() {
        mBinding.login.btnLogin.setOnClickListener(this);
        mBinding.login.txtRegister.setOnClickListener(this);
        mBinding.login.btnFacebook.setOnClickListener(this);
        mBinding.login.btnGoogle.setOnClickListener(this);
        mBinding.login.edtEmail.addTextChangedListener(new MyTaskWatcher(mBinding.login.edtEmail));
    }

    //   mBinding.login.edtEmail.addTextChangedListener(new MyTaskWatcher(mBinding.login.edtEmail));


    @Override
    public void onResume() {
        super.onResume();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (account != null) {
            //Go to next Activity
            startActivity(new Intent(getActivity(), NavigationMainActivity.class));
            Log.e("GoogleLoging already", "" + account.getEmail() + " ," + account.getId() + " ," + account.getIdToken());

        }


    }

    @Override
    public void onClick(View v) {
        Bundle bundle;
        switch (v.getId()) {
            case R.id.btnLogin:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        if (mBinding.login.edtEmail.getText().toString().matches("[0-9]+")) {
//                            Toast.makeText(mContext, "With Phone", Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(mContext, "With Email", Toast.LENGTH_SHORT).show();
                        }
                        callAPI();



                    }
                } else {
                    Toast.makeText(mContext, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.txtRegister:
                FragmentRegister fragmentRegister = new FragmentRegister();
                bundle = new Bundle();
                fragmentRegister.setArguments(bundle);
                ((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, fragmentRegister, true, true);
                break;

            case R.id.btnFacebook:
                mBinding.login.loginButtonFacebook.performClick();
                break;

            case R.id.btnGoogle:
                signIn();
                break;


        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            //  updateUI(account);
            startActivity(new Intent(getActivity(), NavigationMainActivity.class));
            // Toast.makeText(getActivity(), "Signed in Successfully using", Toast.LENGTH_SHORT).show();
            Log.e("GoogleLoging using", "" + account.getEmail() + " ," + account.getId() + " ," + account.getIdToken());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            // Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //  updateUI(null);
            //Toast.makeText(getActivity(), "Fail to login ", Toast.LENGTH_SHORT).show();

        }
    }

    private void callAPI() {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).login(mBinding.login.edtEmail.getText().toString())
                , getCompositeDisposable(), login, this);
    }

    public boolean validation() {
        boolean value = true;
        if (mBinding.login.edtEmail.getText().toString().length() == 0) {
            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.empty_username));
            value = false;
        } else if (mBinding.login.edtEmail.getText().toString().matches("[0-9]+")) {
            if (mBinding.login.edtEmail.getText().toString().length() != 10) {
                setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_phone));
                value = false;
            } else {
                mBinding.login.txtErrorUsername.setText("");
                mBinding.login.txtErrorUsername.setVisibility(View.GONE);
            }
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.login.edtEmail.getText().toString()).matches()) {
            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_email));
            value = false;
        }
        return value;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case login:
                RegisterModel registerModel = (RegisterModel) o;
                if (registerModel != null && registerModel.getStatus() == AppConstants.SUCCESS) {
                    Intent intent = new Intent(getActivity(), VarificationActivity.class);
                    intent.putExtra("id", registerModel.getData().getId());
                    intent.putExtra("email", mBinding.login.edtEmail.getText().toString());
                    intent.putExtra("isRegister", "false");
                    startActivity(intent);
                    // ((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, FragmentVerificationOTP.newInstance(registerModel.getData().getId(), mBinding.login.edtEmail.getText().toString()), true, true);
                } else {
                    showSnackBar(mBinding.LoginFrame, registerModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        showSnackBar(mBinding.LoginFrame, "onFailure", Snackbar.LENGTH_LONG);
    }


    private class MyTaskWatcher implements TextWatcher {
        private View view;

        MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edtEmail:
                    if (mBinding.login.edtEmail.hasFocus()) {
                        if (mBinding.login.edtEmail.getText().toString().length() == 0) {
                            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.empty_username));
                        } else if (mBinding.login.edtEmail.getText().toString().matches("[0-9]+")) {
                            if (mBinding.login.edtEmail.getText().toString().length() != 10) {
                                setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_phone));
                            } else {
                                mBinding.login.txtErrorUsername.setText("");
                                mBinding.login.txtErrorUsername.setVisibility(View.GONE);
                            }
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.login.edtEmail.getText().toString()).matches()) {
                            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_email));
                        } else {
                            mBinding.login.txtErrorUsername.setText("");
                            mBinding.login.txtErrorUsername.setVisibility(View.GONE);
                        }
                    }
                    break;

            }
        }

    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = null;
            try {
                id = object.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                bundle.putString("profile_pic", profile_pic.toString());
                Log.i("profile_pic", profile_pic + "");

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));


            return bundle;


        } catch (JSONException e) {
            //  Log.d(TAG,"Error parsing JSON");
        }
        return null;
    }
}
