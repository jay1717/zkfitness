package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.MyClientListAdapter;
import com.fitzoh.app.databinding.FragmentMyClientListBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddClientActivity;
import com.fitzoh.app.ui.activity.AddClientGroupActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.Objects;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class MyClientListFragment extends BaseFragment implements SingleCallback, MyClientListAdapter.ClientActivateListener, SwipeRefreshLayout.OnRefreshListener {

    FragmentMyClientListBinding mBinding;
    String userId, userAccessToken;

    public MyClientListFragment() {

    }


    public static MyClientListFragment newInstance() {
        return new MyClientListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "My Clients");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_my_client_list, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        Utils.setAddFabBackground(mActivity, mBinding.layoutMyClients.fab);
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutMyClients.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutMyClients.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        if (session.getAuthorizedUser(mActivity).isGym_trainer()) {
            mBinding.layoutMyClients.fab.hide();
        }
        mBinding.layoutMyClients.fab.setOnClickListener(this::showPopUpMenu);
        return mBinding.getRoot();
        }

    private void showPopUpMenu(View view) {
        PopupMenu popup = new PopupMenu(mActivity, view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(R.menu.menu_my_client, popup.getMenu());
        popup.setOnMenuItemClickListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.action_add_client:
                    startActivity(new Intent(getActivity(), AddClientActivity.class));
                    break;
                case R.id.action_add_group:
                    startActivity(new Intent(getActivity(), AddClientGroupActivity.class));
                    break;
            }
            return false;
        });
        popup.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isOnline(mActivity)) {
            getLiveClients();
        } else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    private void getLiveClients() {
        mBinding.layoutMyClients.swipeContainer.setRefreshing(false);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerClients()
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientListModel liveClientsModel = (ClientListModel) o;
                if (liveClientsModel != null && liveClientsModel.getStatus() == AppConstants.SUCCESS && liveClientsModel.getData() != null && liveClientsModel.getData().size() > 0) {
                    mBinding.layoutMyClients.recyclerView.setAdapter(new MyClientListAdapter(getActivity(), liveClientsModel.getData(), this));
                    mBinding.layoutMyClients.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.layoutMyClients.imgNoData.setVisibility(View.GONE);
                } else {
                    mBinding.layoutMyClients.recyclerView.setVisibility(View.GONE);
                    mBinding.layoutMyClients.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getLiveClients();
                } else {
                    showSnackBar(mBinding.linear, Objects.requireNonNull(commonApiResponse).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case single:
                mBinding.layoutMyClients.recyclerView.setVisibility(View.GONE);
                mBinding.layoutMyClients.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void setAssign(int id, int isAssign) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).activateDeactivateClient(id, isAssign,null)
                , getCompositeDisposable(), assignWorkout, this);
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(mActivity)) {
            getLiveClients();
        } else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }
}
