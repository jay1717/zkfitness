package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewPagerAdapter;
import com.fitzoh.app.databinding.FragmentNotificationTypeBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationTypeFragment extends BaseFragment {

    FragmentNotificationTypeBinding mBinding;
    ViewPagerAdapter adapter;

    public NotificationTypeFragment() {
        // Required empty public constructor
    }

    public static NotificationTypeFragment newInstance() {
        NotificationTypeFragment fragment = new NotificationTypeFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_type, container, false);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.client_request));
        setupViewPager(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        mBinding.tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        return mBinding.getRoot();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getFragmentManager());
        viewPager.addOnPageChangeListener(pageChangeListener);
        adapter.addFragment(new NotificationFragment(), "Pending");
        adapter.addFragment(new NotificationAcceptedFragment(), getActivity().getResources().getString(R.string.accepted));
        adapter.addFragment(new NotificationRejectedFragment(), getActivity().getResources().getString(R.string.declined));
        viewPager.setAdapter(adapter);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {
            FragmentLifeCycle fragmentToHide = (FragmentLifeCycle) adapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();
            FragmentLifeCycle fragmentToShow = (FragmentLifeCycle) adapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();
            currentPosition = newPosition;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };
}
