package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramDietAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramDietBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.MealListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.dialog.AddMealDialog;
import com.fitzoh.app.ui.dialog.NutritionPlanDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.mealList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainingProgramDietFragment extends BaseFragment implements View.OnClickListener, SingleCallback, AddMealDialog.DialogClickListener, SwipeRefreshLayout.OnRefreshListener {


    FragmentTrainingProgramDietBinding mBinding;
    int dietId = 0;
    String dietName;
    private String userId, userAccessToken;
    List<MealListModel.DataBean> mealData;

    public TrainingProgramDietFragment() {
        // Required empty public constructor
    }

    public static TrainingProgramDietFragment newInstance(Bundle bundle) {
        TrainingProgramDietFragment fragment = new TrainingProgramDietFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.nutrition_plan));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_training_program_diet, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mealData = new ArrayList<>();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerView.setAdapter(new TrainingProgramDietAdapter(getActivity(), mealData));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.setImageBackground(mActivity, mBinding.imgEye, R.drawable.ic_view);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Intent intent = getActivity().getIntent();
        if (null != intent) {
            dietId = intent.getExtras().getInt("dietId");
            dietName = intent.getExtras().getString("dietName");
            if (dietName != null)
                mBinding.txtTitle.setText(dietName);
            getNutririonList();
        }
        setClickEvents();
        return mBinding.getRoot();
    }

    private void getNutririonList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getMealList(dietId, session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), mealList, this);
        }
        else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setClickEvents() {
        mBinding.imgEye.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_eye:
                NutritionPlanDialog dialog = new NutritionPlanDialog(dietId);
                dialog.setListener(this);
                dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), NutritionPlanDialog.class.getSimpleName());
                dialog.setCancelable(false);
                break;
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case mealList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                mealData = new ArrayList<>();
                MealListModel mealListModel = (MealListModel) o;
                if (mealListModel.getStatus() == AppConstants.SUCCESS) {

                    if (mealListModel.getData() != null) {
                        mealData.addAll(mealListModel.getData());
                        if (mealData.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.imgNoData.setVisibility(View.GONE);
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setAdapter(new TrainingProgramDietAdapter(getActivity(), mealData));
                        }
                    } else {
                        showSnackBar(mBinding.mainLayout, mealListModel.getMessage(), Snackbar.LENGTH_LONG);
                    }

                }
                break;
            case deleteDietPlan:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS)
                    getNutririonList();

                else
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            getNutririonList();
        }
    }

    @Override
    public void refreshData() {
        if (Utils.isOnline(getActivity()))
            getNutririonList();
        else
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    @Override
    public void onRefresh() {
        getNutririonList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
