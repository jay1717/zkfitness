package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GymClientTrainerAssignAdapter;
import com.fitzoh.app.databinding.FragmentAssignClientToGymTrainerBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignSchedule;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssignTrainerToClientGymFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    FragmentAssignClientToGymTrainerBinding mBinding;
    String userId, userAccessToken;
    private int id = 0;
    GymClientTrainerAssignAdapter adapter;
    private List<TrainerListModel.DataBean> data = new ArrayList<>();

    public AssignTrainerToClientGymFragment() {
        // Required empty public constructor
    }

    public static AssignTrainerToClientGymFragment newInstance() {
        return new AssignTrainerToClientGymFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("id", 0);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Assign Clients");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_assign_client_to_gym_trainer, container, false);

        mBinding.schedule.swipeContainer.setOnRefreshListener(this);
        mBinding.schedule.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.getShapeGradient(mActivity, mBinding.schedule.btnAssign);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.schedule.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mBinding.schedule.btnAssign.setOnClickListener(view -> {
            if (validation()) {
                assignClient();
            }
        });

        return mBinding.getRoot();
    }

    private boolean validation() {
        if (data == null || data.size() <= 0) {
            showSnackBar(mBinding.layoutNutrition, "No Data Available", Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private void assignClient() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignClientToTrainer(requestBody)
                    , getCompositeDisposable(), assignSchedule, this);


        } else {
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

    }

    private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("trainer_id", adapter.getData());
            List<Integer> list = new ArrayList<>();
            list.add(id);
            JSONArray jsonArray = new JSONArray(list);
            jsonObject.put("client_ids", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Utils.isOnline(getActivity()))
            getTrainer();
        else
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    private void getTrainer() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerList()
                , getCompositeDisposable(), single, this);

    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                TrainerListModel clientListModel = (TrainerListModel) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (clientListModel != null && clientListModel.getStatus() == AppConstants.SUCCESS && clientListModel.getData() != null) {
                    data = clientListModel.getData();
                    if (data.size() == 0) {
                        mBinding.schedule.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.schedule.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.schedule.imgNoData.setVisibility(View.GONE);
                        mBinding.schedule.recyclerView.setVisibility(View.VISIBLE);
                        adapter = new GymClientTrainerAssignAdapter(mActivity, data);
                        mBinding.schedule.recyclerView.setAdapter(adapter);
                    }
                } else {
                    showSnackBar(mBinding.layoutNutrition, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
            case assignSchedule:
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.getRoot(), commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(getActivity()))
            getTrainer();
        else
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

        mBinding.schedule.swipeContainer.setRefreshing(false);
    }
}
