package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewPagerAdapter;
import com.fitzoh.app.databinding.FragmentPurchaseHistoryBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.PurchaseHistoryTrainerModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.TimeZone;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getPackageList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPurchaseHistory extends BaseFragment implements SingleCallback {

    FragmentPurchaseHistoryBinding mBinding;
    ViewPagerAdapter adapter;
    String userId, userAccessToken;

    public FragmentPurchaseHistory() {
        // Required empty public constructor
    }

    public static FragmentPurchaseHistory newInstance() {
        FragmentPurchaseHistory fragment = new FragmentPurchaseHistory();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchase_history, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.cardLayout.setBackground(Utils.getShapeGradient(mActivity,0));
        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Purchase");
        setupViewPager(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        callPackageList();
    }

    private void callPackageList() {
        if (Utils.isOnline(mActivity)) {
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchasePlanHistoryTrainer()
                    , getCompositeDisposable(), getPackageList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getFragmentManager());
        viewPager.addOnPageChangeListener(pageChangeListener);
        adapter.addFragment(new FragmentPurchaseHistoryOwn(), "Own");
        adapter.addFragment(new FragmentPurchaseHistoryTrainerClient(), "Client");
        viewPager.setAdapter(adapter);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {

            FragmentLifeCycle fragmentToHide = (FragmentLifeCycle) adapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();

            FragmentLifeCycle fragmentToShow = (FragmentLifeCycle) adapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();
            currentPosition = newPosition;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getPackageList:
                disableScreen(false);
                PurchaseHistoryTrainerModel model = (PurchaseHistoryTrainerModel) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null) {
                    setCurrentPlanDetail(model.getData());
                } else {
                    showSnackBar(mBinding.mainLayout, model.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
        }
    }

    private void setCurrentPlanDetail(PurchaseHistoryTrainerModel.DataBean model) {
        if (model.getCurrentPlan() != null) {
            PurchaseHistoryTrainerModel.DataBean.CurrentPlanBean planBean = model.getCurrentPlan();
            mBinding.packageName.setText(planBean.getPlan_name());
            mBinding.packageDescription.setText(planBean.getLimitation());
            mBinding.packageDate.setText(planBean.getStart_date());
            mBinding.packageRemainingDay.setText("Remaining Day : " + planBean.getRemaining_day());
            mBinding.packagePrice.setText("Price: " + planBean.getPrice());
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }
}
