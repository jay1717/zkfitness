package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;

import com.fitzoh.app.adapter.TrainerInquiryListAdapter;
import com.fitzoh.app.databinding.FragmentTrainerInquiriesBinding;
import com.fitzoh.app.model.ClientInquiryHistoryModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainerInquiriesFragment extends BaseFragment implements TrainerInquiryListAdapter.dataPassing, SingleCallback, SwipeRefreshLayout.OnRefreshListener {

    FragmentTrainerInquiriesBinding mBinding;
    String userId, userAccessToken;
    List<ClientInquiryHistoryModel.DataBean> clientInquiryModel;
    TrainerInquiryListAdapter trainerInquiryListAdapter;


    public TrainerInquiriesFragment() {
        // Required empty public constructor
    }
    public static TrainerInquiriesFragment newInstance() {
        TrainerInquiriesFragment fragment = new TrainerInquiriesFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.inquiry));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_trainer_inquiries, container, false);
        clientInquiryModel = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        trainerInquiryListAdapter = new TrainerInquiryListAdapter(getActivity(), this,clientInquiryModel);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.recyclerView.setAdapter(trainerInquiryListAdapter);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callInquiryList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callInquiryList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientHistory()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void data(ClientInquiryHistoryModel.DataBean dataBean) {

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                clientInquiryModel = new ArrayList<>();
                ClientInquiryHistoryModel clientInquiryHistoryModel = (ClientInquiryHistoryModel) o;
                if (clientInquiryHistoryModel.getStatus() == AppConstants.SUCCESS) {

                    if (clientInquiryHistoryModel != null) {
                        clientInquiryModel.addAll(clientInquiryHistoryModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (clientInquiryModel.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            trainerInquiryListAdapter = new TrainerInquiryListAdapter(getActivity(), this, clientInquiryModel);
                            mBinding.recyclerView.setAdapter(trainerInquiryListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, clientInquiryHistoryModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        callInquiryList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
