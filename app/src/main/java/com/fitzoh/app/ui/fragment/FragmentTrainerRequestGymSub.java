package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentFragmentTrainerRequestGymSubBinding;
import com.fitzoh.app.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTrainerRequestGymSub extends BaseFragment {

    FragmentFragmentTrainerRequestGymSubBinding mBinding;


    public FragmentTrainerRequestGymSub() {
        // Required empty public constructor
    }

    public static FragmentTrainerRequestGymSub newInstance() {
        return new FragmentTrainerRequestGymSub();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_fragment_trainer_request_gym_sub, container, false);
        return mBinding.getRoot();
    }

}
