package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogReloginBinding;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class ReloginActivity extends BaseActivity implements SingleCallback {

    public DialogReloginBinding mBinding;
    String userId, userAccessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.dialog_relogin);
        userId = String.valueOf(session.getAuthorizedUser(getApplicationContext()).getId());
        userAccessToken = session.getAuthorizedUser(getApplicationContext()).getUserAccessToken();
        mBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi();
            }
        });

    }

    private void callApi() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getApplicationContext(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getRelogin(userId)
                , getCompositeDisposable(), single, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() != null) {
           /* txtOfferTitle.setText(intent.getExtras().getString("title"));
            txtOfferDesc.setText(intent.getExtras().getString("message"));*/
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames){
            case single:

                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }
}
