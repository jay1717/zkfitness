package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentSelectSetUnitBinding;
import com.fitzoh.app.model.WorkoutParamModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

public class SelectSetUnitFragment extends BaseFragment implements SingleCallback, AdapterView.OnItemSelectedListener {
    FragmentSelectSetUnitBinding mBinding;
    private List<WorkoutParamModel.DataBean> muscleDatas;
    String userId, userAccessToken;
    String param1 = "", param2 = "", param3 = "", unit = "";
    private int param1_id = 0, param2_id = 0, param3_id = 0;
    int position1 = 0, position2 = 0, position3 = 0;
    String data = "";

    public SelectSetUnitFragment() {
    }

    public static SelectSetUnitFragment newInstance(Bundle args) {
        SelectSetUnitFragment fragment = new SelectSetUnitFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            param1_id = getArguments().getInt("param1_id", 0);
            param1 = getArguments().getString("param1");
            param2_id = getArguments().getInt("param2_id", 0);
            param2 = getArguments().getString("param2");
            param3_id = getArguments().getInt("param3_id", 0);
            param3 = getArguments().getString("param3");
            if (getArguments().containsKey("unit")) {
                unit = getArguments().getString("unit");
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Select Unit");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_set_unit, container, false);
        Utils.getShapeGradient(mActivity, mBinding.btnAdd);
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.imgParam1, mBinding.imgParam2, mBinding.imgParam3, mBinding.imgParam4});
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        getParameters();
        mBinding.spnParam4.setAdapter(new ArrayAdapter<>(mContext, R.layout.spinner_item_add_workout, new String[]{"KG", "LBS"}));
        mBinding.btnAdd.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.putExtra("param1_id", muscleDatas.get(mBinding.spnParam1.getSelectedItemPosition()).getId());
            intent.putExtra("param1", muscleDatas.get(mBinding.spnParam1.getSelectedItemPosition()).getValue());
            intent.putExtra("param2_id", muscleDatas.get(mBinding.spnParam2.getSelectedItemPosition()).getId());
            intent.putExtra("param2", muscleDatas.get(mBinding.spnParam2.getSelectedItemPosition()).getValue());
            intent.putExtra("param3_id", muscleDatas.get(mBinding.spnParam3.getSelectedItemPosition()).getId());
            intent.putExtra("param3", muscleDatas.get(mBinding.spnParam3.getSelectedItemPosition()).getValue());
            if (mBinding.spnParam4.getVisibility() == View.VISIBLE) {
                intent.putExtra("unit", mBinding.spnParam4.getSelectedItemPosition() == 0 ? "1" : "2");
            }
            mActivity.setResult(Activity.RESULT_OK, intent);
            mActivity.finish();
        });
        return mBinding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        mActivity.invalidateOptionsMenu();
    }

    private void getParameters() {
        mBinding.spnParam1.setEnabled(false);
        mBinding.spnParam2.setEnabled(false);
        mBinding.spnParam3.setEnabled(false);


        mBinding.imgParam1.setOnClickListener(view -> {
            List<String> list = getListData(mBinding.spnParam2.getSelectedItem().toString(), mBinding.spnParam3.getSelectedItem().toString());
            showDialog(mBinding.spnParam1, list.toArray(new String[list.size()]), list.indexOf(mBinding.spnParam1.getSelectedItem().toString()));
        });
        mBinding.imgParam2.setOnClickListener(view -> {
            List<String> list = getListData(mBinding.spnParam1.getSelectedItem().toString(), mBinding.spnParam3.getSelectedItem().toString());
            showDialog(mBinding.spnParam2, list.toArray(new String[list.size()]), list.indexOf(mBinding.spnParam2.getSelectedItem().toString()));
        });
        mBinding.imgParam3.setOnClickListener(view -> {
            List<String> list = getListData(mBinding.spnParam1.getSelectedItem().toString(), mBinding.spnParam2.getSelectedItem().toString());
            showDialog(mBinding.spnParam3, list.toArray(new String[list.size()]), list.indexOf(mBinding.spnParam3.getSelectedItem().toString()));
        });
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getParameters()
                , getCompositeDisposable(), workoutList, this);
    }

    private List<String> getListData(String first, String second) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < muscleDatas.size(); i++) {
            if (!muscleDatas.get(i).getValue().equalsIgnoreCase(first) && !muscleDatas.get(i).getValue().equalsIgnoreCase(second)) {
                list.add(muscleDatas.get(i).getValue());
            }
        }
        return list;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutParamModel workoutParamModel = (WorkoutParamModel) o;
                List<String> list = new ArrayList<>();
                if (workoutParamModel != null && workoutParamModel.getStatus() == AppConstants.SUCCESS && workoutParamModel.getData() != null && workoutParamModel.getData().size() > 0) {
                    muscleDatas = workoutParamModel.getData();
                    for (int i = 0; i < muscleDatas.size(); i++) {
                        list.add(muscleDatas.get(i).getValue());
                        if (muscleDatas.get(i).getId() == param1_id) {
                            position1 = i;
                        }
                        if (muscleDatas.get(i).getId() == param2_id) {
                            position2 = i;
                        }
                        if (muscleDatas.get(i).getId() == param3_id) {
                            position3 = i;
                        }
                    }

                    setSpinner(mBinding.spnParam1, list);
                    setSpinner(mBinding.spnParam2, list);
                    setSpinner(mBinding.spnParam3, list);

                    if ((position1 == position2) && (position1 == position3)) {
                        mBinding.spnParam1.setSelection(((ArrayAdapter<String>) mBinding.spnParam1.getAdapter()).getPosition("Weight"));
                        mBinding.spnParam2.setSelection(((ArrayAdapter<String>) mBinding.spnParam2.getAdapter()).getPosition("Reps"));
                        mBinding.spnParam3.setSelection(((ArrayAdapter<String>) mBinding.spnParam3.getAdapter()).getPosition("Rest Period"));
                    } else {
                        mBinding.spnParam1.setSelection(position1);
                        mBinding.spnParam2.setSelection(position2);
                        mBinding.spnParam3.setSelection(position3);
                    }
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }

    private void setSpinner(Spinner spinner, List<String> list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_add_workout, list);
        spinner.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        setWeight();
        switch (adapterView.getId()) {
            case R.id.spnParam1:
                position1 = i;
                break;
            case R.id.spnParam2:
                position2 = i;
                break;
            case R.id.spnParam3:
                position3 = i;
                break;
        }
    }

    private void setWeight() {
        if (mBinding.spnParam1.getSelectedItem().toString().equalsIgnoreCase("Weight") || mBinding.spnParam2.getSelectedItem().toString().equalsIgnoreCase("Weight") || mBinding.spnParam3.getSelectedItem().toString().equalsIgnoreCase("Weight")) {
            mBinding.spnParam4.setVisibility(View.VISIBLE);
            mBinding.imgParam4.setVisibility(View.VISIBLE);
            if (!unit.equalsIgnoreCase("")) {
                mBinding.spnParam4.setSelection(unit.equalsIgnoreCase("1") ? 0 : 1);
            }
        } else {
            mBinding.spnParam4.setVisibility(View.GONE);
            mBinding.imgParam4.setVisibility(View.GONE);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void showDialog(Spinner spinner, String[] list, int position) {
        data = list[position];
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Choose Unit");
        builder.setSingleChoiceItems(list, position, (dialog, which) -> data = list[which]);
        builder.setPositiveButton("OK", (dialog, which) -> spinner.setSelection(((ArrayAdapter<String>) spinner.getAdapter()).getPosition(data)));
        builder.setNegativeButton("Cancel", null);
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
    }
}
