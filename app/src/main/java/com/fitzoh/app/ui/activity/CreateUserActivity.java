package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.CreateFitnessCenterFragment;
import com.fitzoh.app.ui.fragment.CreateTrainerProfileFragment;
import com.fitzoh.app.ui.fragment.UserCreateProfileFragment;

public class CreateUserActivity extends BaseActivity {

    String rollId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        Intent intent = getIntent();
        if (null != intent) {
            rollId = intent.getExtras().getString("rollId");
            if (rollId.equals("1")) {
                UserCreateProfileFragment userCreateProfileFragment = new UserCreateProfileFragment();
                userCreateProfileFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.realtabcontent, userCreateProfileFragment, "UserCreateFragment")
                        .commit();
            } else if (rollId.equals("2")) {
                CreateTrainerProfileFragment createTrainerProfileFragment = new CreateTrainerProfileFragment();
                createTrainerProfileFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.realtabcontent, createTrainerProfileFragment, "TrainerCreateProfileFragment")
                        .commit();
            } else if (rollId.equals("3")) {
                CreateFitnessCenterFragment createFitnessCenterFragment = new CreateFitnessCenterFragment();
                createFitnessCenterFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.realtabcontent, createFitnessCenterFragment, "CreateFitnessCenterFragment")
                        .commit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
