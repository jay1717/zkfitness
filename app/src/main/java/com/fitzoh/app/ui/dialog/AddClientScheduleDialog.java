package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogAddScheduleBinding;
import com.fitzoh.app.model.AddClientSchedule;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.ScheduleListModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

@SuppressLint("ValidFragment")
public class AddClientScheduleDialog extends AppCompatDialogFragment implements AdapterView.OnItemSelectedListener, SingleCallback {
    DialogAddScheduleBinding mBinding;
    View view;
    public CompositeDisposable compositeDisposable;
    String userId, userAccessToken;
    public SessionManager session;
    private int startHour, startMinute, endHour, endMinute;
    String seletedDay;
    int clientId;

    public AddClientScheduleDialog(int clientId) {
        this.clientId = clientId;
    }


    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_schedule, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.btnDays, mBinding.btnStartTime, mBinding.btnEndTime});
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            view = mBinding.getRoot();
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();

        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }


    private void prepareLayout() {

        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        List<String> days = new ArrayList<String>();
        days.add("Select Day");
        days.add("Monday");
        days.add("Tuesday");
        days.add("Wednesday");
        days.add("Thursday");
        days.add("Friday");
        days.add("Saturday");
        days.add("Sunday");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item_add_workout, days);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mBinding.spnDays.setAdapter(dataAdapter);

        mBinding.spnDays.setOnItemSelectedListener(this);

        mBinding.btnStartTime.setOnClickListener(view -> {
            final Calendar c = Calendar.getInstance();
            startHour = c.get(Calendar.HOUR_OF_DAY);
            startMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            mBinding.spnStartTime.setText(hourOfDay + ":" + minute);



                        }
                    }, startHour, startMinute, false);
            timePickerDialog.show();
        });

        mBinding.btnEndTime.setOnClickListener(view -> {
            final Calendar c = Calendar.getInstance();
            endHour = c.get(Calendar.HOUR_OF_DAY);
            endMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {

                            mBinding.spnEndTime.setText(hourOfDay + ":" + minute);

                        }
                    }, endHour, endMinute, false);
            timePickerDialog.show();
        });

        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });
        mBinding.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    saveShedule();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
        mBinding.btnDays.setOnClickListener(view -> mBinding.spnDays.performClick());
    }



    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    private boolean validation() {
        if (mBinding.spnDays.getSelectedItemPosition() <= 0) {
            showSnackBar("Select Day.");
            return false;
        } else if (mBinding.spnStartTime.getText().toString().length() <= 0) {
            showSnackBar("Select Start Time.");
            return false;
        } else if (mBinding.spnEndTime.getText().toString().length() <= 0) {
            showSnackBar("Select End Time");
            return false;
        } else if (!checkDate()) {
            showSnackBar("Start time should not greater or equal to end time");
            return false;
        }
        return true;
    }

    private boolean checkDate() {
        String startDate = mBinding.spnStartTime.getText().toString();
        String endDate = mBinding.spnEndTime.getText().toString();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            Log.e("checkDate:", spf.format(simpleDateFormat.parse(startDate)));
            Log.e("checkDate:", spf.format(simpleDateFormat.parse(endDate)));
            if (simpleDateFormat.parse(startDate).after(simpleDateFormat.parse(endDate)) || simpleDateFormat.parse(startDate).equals(simpleDateFormat.parse(endDate))) {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }


    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }


    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void saveShedule() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveClientSchedule(seletedDay, mBinding.spnStartTime.getText().toString(), mBinding.spnEndTime.getText().toString())
                , getCompositeDisposable(), single, this);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i != 0) {
            seletedDay = adapterView.getItemAtPosition(i).toString();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                AddClientSchedule commonApiResponse = (AddClientSchedule) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    assignWorkout(commonApiResponse.getData().getId());
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }

                break;

            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse1 = (CommonApiResponse) o;
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent().putExtra("data", commonApiResponse1.getStatus()));
                dismiss();
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    private void assignWorkout(int id) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson(id));
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).unAssignSchedule(requestBody)
                    , getCompositeDisposable(), assignWorkout, this);
        } else {
        }

    }

    private String getClientJson(int id) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("client_ids", getData());
            jsonObject.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
            jsonObject.put("schedule_id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public JSONArray getData() {
        JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", clientId);
                jsonObject.put("is_assigned", 1);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        return jsonArray;

    }


        public interface DialogClickListener {
        public void setClick();
    }
}
