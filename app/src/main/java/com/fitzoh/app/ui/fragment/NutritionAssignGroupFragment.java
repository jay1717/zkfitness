package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientAssignGroupAdapter;
import com.fitzoh.app.databinding.FragmentGroupAssignBinding;
import com.fitzoh.app.model.ClientGroupListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignDiet;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class NutritionAssignGroupFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {

    private FragmentGroupAssignBinding mBinding;
    private String userId, userAccessToken;
    private List<ClientGroupListModel.DataBean> data = new ArrayList<>();
    private ClientAssignGroupAdapter adapter;
    private int dietId = 0;

    public NutritionAssignGroupFragment() {
        // Required empty public constructor
    }

    public static NutritionAssignGroupFragment newInstance(int id) {
        NutritionAssignGroupFragment fragment = new NutritionAssignGroupFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("dietId", id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dietId = getArguments().getInt("dietId");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_group_assign, container, false);
        Utils.getShapeGradient(mActivity, mBinding.client.btnAssign);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        mBinding.client.swipeContainer.setOnRefreshListener(this);
        mBinding.client.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        if (Utils.isOnline(mActivity)) {
            getClientGroups();
        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
        mBinding.client.btnAssign.setOnClickListener(view -> {
            if (validation()) {
                assignDiet();
            }
        });

        return mBinding.getRoot();
    }

    private void assignDiet() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignDiet(requestBody)
                , getCompositeDisposable(), assignDiet, this);

    }

    private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("client_ids", new JSONArray());
            jsonObject.put("client_group_ids", adapter.getData());
            jsonObject.put("diet_plan_id", dietId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    private void getClientGroups() {
        mBinding.client.swipeContainer.setRefreshing(false);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientGroups(dietId)
                , getCompositeDisposable(), single, this);
    }

    private boolean validation() {
        if (data == null || data.size() <= 0) {
            showSnackBar(mBinding.layoutClient, "No Data Available", Snackbar.LENGTH_LONG);
            return false;
        } /*else if (adapter.getData() == null || adapter.getData().size() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please select client to assign", Snackbar.LENGTH_LONG);
            return false;
        }*/
        return true;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientGroupListModel clientGroupListModel = (ClientGroupListModel) o;
                if (clientGroupListModel != null && clientGroupListModel.getStatus() == AppConstants.SUCCESS && clientGroupListModel.getData() != null && clientGroupListModel.getData().size() > 0) {
                    mBinding.client.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.client.imgNoData.setVisibility(View.GONE);
                    data = clientGroupListModel.getData();
                    adapter = new ClientAssignGroupAdapter(mActivity, data);
                    mBinding.client.recyclerView.setAdapter(adapter);
                } else {
                    mBinding.client.recyclerView.setVisibility(View.GONE);
                    mBinding.client.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case assignDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.layoutClient, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.client.recyclerView.setVisibility(View.GONE);
                mBinding.client.imgNoData.setVisibility(View.VISIBLE);
        }
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutClient, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(mActivity)) {
            getClientGroups();
        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }
}
