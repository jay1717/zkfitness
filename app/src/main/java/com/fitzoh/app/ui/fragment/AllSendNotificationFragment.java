package com.fitzoh.app.ui.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GymClientListAdapter;
import com.fitzoh.app.adapter.GymSubscriptionListAdapter;
import com.fitzoh.app.adapter.SendNotificationAllAdpter;
import com.fitzoh.app.adapter.WorkoutListAdapter;
import com.fitzoh.app.databinding.FragmentAllSendNotificationBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.GymSubscriptionModel;
import com.fitzoh.app.model.SendNotiGetUserModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class AllSendNotificationFragment extends BaseFragment implements SingleCallback{

    public static SendNotificationAllAdpter sendNotificationAllAdpter;
    FragmentAllSendNotificationBinding mBinding;
    private String userId, userAccessToken, type = "all";
    List<SendNotiGetUserModel.DatumSendNoti> dataBeans;
    private boolean ischeck;

    public AllSendNotificationFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ischeck = getArguments().getBoolean("ischeck");
            Log.e("Is Check", ">> " + ischeck);
        }
    }

    public static void selectAll() {
        if (sendNotificationAllAdpter != null) {
            SendNotificationAllAdpter.selectAll();
            sendNotificationAllAdpter.notifyDataSetChanged();
        }
    }

    public static void deselectAll() {
        if (sendNotificationAllAdpter != null) {
            SendNotificationAllAdpter.deselectAll();
            sendNotificationAllAdpter.notifyDataSetChanged();
        }
    }

    public static List<Integer> getData() {
        if (sendNotificationAllAdpter != null) {
            return SendNotificationAllAdpter.getData();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_all_send_notification, container, false);

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        getUserData();
        setLayoutManager();

        return mBinding.getRoot();
    }

    private void setLayoutManager() {
        try {
            mBinding.rcyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
            /*sendNotificationAllAdpter = new SendNotificationAllAdpter(dataBeans,getActivity());
            mBinding.rcyclerview.setAdapter(sendNotificationAllAdpter);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserData() {
        try {
            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getUserData(type)
                        , getCompositeDisposable(), WebserviceBuilder.ApiNames.single, this);
            } else {
                showSnackBar(mBinding.layoutNotification, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
//                dataBeans = new ArrayList<>();
                SendNotiGetUserModel sendNotiGetUserModel = (SendNotiGetUserModel) o;
                if (sendNotiGetUserModel != null) {
                    if (sendNotiGetUserModel.getData().size() == 0) {
                        mBinding.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.rcyclerview.setVisibility(View.GONE);
                    } else {
                        mBinding.rcyclerview.setVisibility(View.VISIBLE);
                        mBinding.imgNoData.setVisibility(View.GONE);
                        sendNotificationAllAdpter = new SendNotificationAllAdpter(sendNotiGetUserModel.getData(), getActivity(), ischeck);
                        mBinding.rcyclerview.setAdapter(sendNotificationAllAdpter);
                    }
                } else {
                    showSnackBar(mBinding.layoutNotification, ((ClientListModel) o).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        showSnackBar(mBinding.layoutNotification, throwable.getMessage(), Snackbar.LENGTH_LONG);
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
    }
}
