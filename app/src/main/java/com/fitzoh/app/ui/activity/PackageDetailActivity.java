package com.fitzoh.app.ui.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentPackageDetailBinding;
import com.fitzoh.app.model.ProfilePackageModel;
import com.fitzoh.app.utils.SessionManager;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

public class PackageDetailActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, PaymentResultListener, SingleCallback {

    FragmentPackageDetailBinding mBinding;
    ProfilePackageModel.DataBean dataBean;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    String userId, userAccessToken;
    private Snackbar snackbar;
    String DEVELOPER_KEY = "AIzaSyAQNKNP2qdX6AgwfxVyKGPsvEJkmp1hmEI";
    public SessionManager session;
    private int mYear, mMonth, mDay;
    public CompositeDisposable compositeDisposable;
    String date = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.fragment_package_detail);

        Intent intent = getIntent();
        if (null != intent) {

            dataBean = (ProfilePackageModel.DataBean) getIntent().getSerializableExtra("dataBeanClass");
            session = new SessionManager(getApplicationContext());
            userId = String.valueOf(session.getAuthorizedUser(this).getId());
            userAccessToken = session.getAuthorizedUser(this).getUserAccessToken();
            mBinding.layout.txtPackageNameValue.setText(dataBean.getPackage_name());
            mBinding.layout.txtPackageDescriptionValue.setText(dataBean.getDesc());
            mBinding.layout.txtPackageDiscountValue.setText("$" + String.valueOf(dataBean.getDiscounted_price()));
            mBinding.layout.txtPackagePriceValue.setText("$" + String.valueOf(dataBean.getPrice()));
            mBinding.layout.txtPackagePriceValue.setPaintFlags( mBinding.layout.txtPackagePriceValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mBinding.layout.txtPackageWeekValue.setText(String.valueOf(dataBean.getWeeks()));
            mBinding.toolbar.tvTitle.setTextColor(getResources().getColor(R.color.gray));
            mBinding.toolbar.tvTitle.setText(dataBean.getPackage_name());

            if (dataBean.getMedia_type().equals("Image")) {
                mBinding.layout.imgMain.setVisibility(View.VISIBLE);
                mBinding.layout.youtubeView.setVisibility(View.GONE);
                mBinding.layout.simpleVideoView.setVisibility(View.GONE);
                Glide.with(getApplicationContext())
                        .load(dataBean.getMedia())
                        .into(mBinding.layout.imgMain);
            } else if (dataBean.getMedia_type().equals("Youtube")) {
                mBinding.layout.imgMain.setVisibility(View.GONE);
                mBinding.layout.youtubeView.setVisibility(View.VISIBLE);
                mBinding.layout.simpleVideoView.setVisibility(View.GONE);
                mBinding.layout.youtubeView.initialize(DEVELOPER_KEY, this);
            } else if (dataBean.getMedia_type().equals("Video")) {
                mBinding.layout.imgMain.setVisibility(View.GONE);
                mBinding.layout.youtubeView.setVisibility(View.GONE);
                mBinding.layout.simpleVideoView.setVisibility(View.VISIBLE);
                mBinding.layout.simpleVideoView.setVideoURI(Uri.parse(dataBean.getMedia()));
                mBinding.layout.simpleVideoView.start();
            } else {
                mBinding.layout.imgMain.setVisibility(View.VISIBLE);
                mBinding.layout.youtubeView.setVisibility(View.GONE);
                mBinding.layout.simpleVideoView.setVisibility(View.GONE);
            }
        }
        mBinding.layout.txtDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              openDatePicker();
            }

        });
        mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        mBinding.toolbar.imgBack.setOnClickListener(view -> onBackPressed());
        mBinding.layout.btnPurchase.setOnClickListener(view -> checkout());

    }

    private void openDatePicker(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                        mBinding.layout.txtDatePicker.setText(dateFormat.format(c.getTime()));

                        String inputDate=dateFormat.format(c.getTime());
                        SimpleDateFormat spf=new SimpleDateFormat("dd/MM/yyyy");
                        Date newDate= null;
                        try {
                            newDate = spf.parse(inputDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        spf= new SimpleDateFormat("yyyy-MM-dd");
                        date = spf.format(newDate);
                        System.out.println(date);
                  //      mBinding.layout.txtDatePicker.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mBinding.layout.simpleVideoView.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBinding.layout.simpleVideoView.stopPlayback();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mBinding.layout.simpleVideoView.resume();
    }

    public String extractYTId(String ytUrl) throws MalformedURLException {
        String vId = "";
        Pattern pattern = Pattern.compile(
                "(?:https?:/{2})?(?:w{3}.)?youtu(?:be)?.(?:com|be)(?:/watch?v=|/)([^s&]+)",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
            if (vId.startsWith("watch?v=")) {
                vId = vId.replace("watch?v=", "");
            } else if (vId.equals("")) {
                vId = extractYoutubeId(ytUrl);
            }
        }
        return vId;
    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = "";
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            mBinding.layout.youtubeView.initialize(DEVELOPER_KEY, this);
        }
    }

    private void checkout() {

        if(!date.equals("")){

            Checkout checkout = new Checkout();
            checkout.setImage(R.mipmap.ic_launcher);
            Double price = Double.valueOf(dataBean.getDiscounted_price());
            final Activity activity = this;


            try {
                JSONObject options = new JSONObject();
                options.put("name", "Fitzoh");
                options.put("description", "Subscription charge");
                options.put("currency", "INR");
                options.put("amount", (price.intValue()) * 100);

                JSONObject preFill = new JSONObject();
                preFill.put("email", session.getAuthorizedUser(activity).getEmail());

                preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());

                options.put("prefill", preFill);

                checkout.open(activity, options);
            } catch (Exception e) {
                Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                        .show();
                e.printStackTrace();
            }
        }
        else {
            Toast.makeText(this, "Please Select Date", Toast.LENGTH_LONG).show();
        }

    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {

            String videoId = "";
            try {
                if(URLUtil.isValidUrl(dataBean.getYoutube_url())) {
                    videoId = extractYTId(dataBean.getYoutube_url());
                }
                else {
                    String url = "https://www.youtube.com/watch?v=" + dataBean.getYoutube_url();
                    videoId = extractYTId(url);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if (videoId.equals("")) {
                Toast.makeText(this, "Invalid URL", Toast.LENGTH_SHORT).show();
            } else {
                Log.e("onInitializationSucce", videoId);
                youTubePlayer.cueVideo(videoId);
                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
            }
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");
    }

    public void purchaseSubscription(String transaction_id, String payment_method) {
        /*mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getApplicationContext(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchaseSubscription(
                subscriptionData.get(subscriptionListAdapter.selectionPostition).getId(), transaction_id, payment_method, userId)
                , getCompositeDisposable(), single, this);*/

        if (Utils.isOnline(this)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).savePurchaseMethod(dataBean.getId(),transaction_id,"razorpay",1, date, Integer.parseInt(dataBean.getOwner_id()))
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.layoutPackage, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.getRoot(), s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    public void showSnackBar(View view, String msg, int LENGTH) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames){
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    finish();
                } else {
                    showSnackBar(mBinding.layoutPackage, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
