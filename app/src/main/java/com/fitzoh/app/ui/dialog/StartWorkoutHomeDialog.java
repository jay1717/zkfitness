package com.fitzoh.app.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogStartWorkoutHomeBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GetClientDetailModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.activity.ViewWorkoutExerciseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class StartWorkoutHomeDialog extends AppCompatDialogFragment implements SingleCallback {


    DialogStartWorkoutHomeBinding mBinding;
    View view;
    String userId, userAccessToken;
    public SessionManager session;
    ArrayList<GetClientDetailModel.DataBean.TodaysWorkoutBean> workoutBeans = new ArrayList<>();
    ArrayList<GetClientDetailModel.DataBean.TodaysWorkoutBean> workoutFollowBeans = new ArrayList<>();
    String dateString = "", timeString = "";
    public CompositeDisposable compositeDisposable;
    private String workout_name = "";
    private int workout_id = 0, training_program_id = 0, weekday_id = 0;
    private boolean is_am_workout = false;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_start_workout_home, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnAdd, false);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.btnTempleteDropdown, mBinding.btnTempleteDropdownTwo});
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            Utils.setSwitchTint(getActivity(), mBinding.toggle);
            view = mBinding.getRoot();
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        AlertDialog dialog = alertBuilder.create();
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();

        return dialog;
    }

    private void prepareLayout() {
        if (getArguments() != null) {
            training_program_id = getArguments().getInt("training_program_id", 0);
            workout_id = getArguments().getInt("workout_id", 0);
            workout_name = getArguments().getString("workout_name", "");
            is_am_workout = getArguments().getBoolean("is_am_workout", false);
            weekday_id = getArguments().getInt("weekday_id", 0);
            /*if (getArguments().getParcelable("data") != null) {
                todaysWorkoutBean = getArguments().getParcelable("data");
            }*/
            if (getArguments().containsKey("data") && getArguments().getParcelableArrayList("data") != null) {
                workoutBeans = getArguments().getParcelableArrayList("data");
                setSpinnerData();
            }
            if (getArguments().containsKey("dataFollow") && getArguments().getParcelableArrayList("dataFollow") != null) {
                workoutFollowBeans = getArguments().getParcelableArrayList("dataFollow");
                setSpinnerDataNew();
            }
        }
        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.toggle.setOnCheckedChangeListener((compoundButton, b) -> {
            if (!b) {
                if (workoutFollowBeans != null && workoutFollowBeans.size() > 0) {
                    mBinding.spnTemplete.setVisibility(View.GONE);
                    mBinding.btnTempleteDropdown.setVisibility(View.GONE);
                    mBinding.spnTempleteTwo.setVisibility(View.VISIBLE);
                    mBinding.btnTempleteDropdownTwo.setVisibility(View.VISIBLE);
                } else {
                    mBinding.toggle.setChecked(false);
                }
            } else {
                mBinding.spnTemplete.setVisibility(View.VISIBLE);
                mBinding.btnTempleteDropdown.setVisibility(View.VISIBLE);
                mBinding.spnTempleteTwo.setVisibility(View.GONE);
                mBinding.btnTempleteDropdownTwo.setVisibility(View.GONE);
            }
        });
        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time = new SimpleDateFormat("hh:mm a");

        dateString = date.format(calendar.getTime());
        timeString = time.format(calendar.getTime());

        mBinding.txtDate.setText("Date : " + dateString);
        mBinding.txtTime.setText("Time : " + timeString);

        mBinding.btnAdd.setOnClickListener(view -> {
            if (Utils.isOnline(getActivity())) {
                if (validation()) {
                    startWorkout();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
    }

    private void setSpinnerDataNew() {
        String[] templates = new String[workoutFollowBeans.size()];
        for (int i = 0; i < workoutFollowBeans.size(); i++) {
            templates[i] = workoutFollowBeans.get(i).getWorkout_name();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, templates);
        mBinding.spnTempleteTwo.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
    }

    private void setSpinnerData() {
        String[] templates;
        templates = new String[workoutBeans.size()];
        for (int i = 0; i < workoutBeans.size(); i++) {
            templates[i] = workoutBeans.get(i).getWorkout_name();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_add_workout, templates);
        mBinding.spnTemplete.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
    }

    private void startWorkout() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        workout_id = !mBinding.toggle.isChecked() ? workoutFollowBeans.get(mBinding.spnTempleteTwo.getSelectedItemPosition()).getId() : workoutBeans.get(mBinding.spnTemplete.getSelectedItemPosition()).getId();
        workout_name = !mBinding.toggle.isChecked() ? workoutFollowBeans.get(mBinding.spnTempleteTwo.getSelectedItemPosition()).getWorkout_name() : workoutBeans.get(mBinding.spnTemplete.getSelectedItemPosition()).getWorkout_name();
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).startWorkout(dateString, timeString, workout_id, is_am_workout)
                , getCompositeDisposable(), single, this);
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    private boolean validation() {
       /* if (TextUtils.isEmpty(mBinding.edtTrainingProgramName.getText().toString())) {
            showSnackBar("Enter training program name.");
            return false;
        } else if (isEdit && TextUtils.isEmpty(mBinding.edtTime.getText().toString())) {
            showSnackBar("Enter time.");
            return false;
        }*/
        return true;
    }


    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent());
                    dismiss();
                    startActivity(new Intent(getActivity(), ViewWorkoutExerciseActivity.class).putExtra("workout_id", workout_id).putExtra("workout_name", workout_name).putExtra("training_program_id", training_program_id).putExtra("is_am_workout", is_am_workout).putExtra("weekday_id", weekday_id));
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
