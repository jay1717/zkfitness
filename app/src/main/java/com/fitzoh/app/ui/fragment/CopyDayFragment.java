
package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.CopyDayAdapter;
import com.fitzoh.app.databinding.FragmentCopyDayBinding;
import com.fitzoh.app.model.TrainingProgramWeek;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditWorkoutActivity;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CopyDayFragment extends BaseFragment {


    FragmentCopyDayBinding mBinding;
    String userId, userAccessToken;
    CopyDayAdapter recyclerViewAdapter;
    ArrayList<TrainingProgramWeek> weeks = new ArrayList<>();
    int week, day;

    public CopyDayFragment() {
        // Required empty public constructor
    }

    public static CopyDayFragment newInstance(Bundle bundle) {
        CopyDayFragment fragment = new CopyDayFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_copy_day, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Copy Day");
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        //RecyclerView adapater
        week = mActivity.getIntent().getIntExtra("week", 0);
        day = mActivity.getIntent().getIntExtra("day", 0);
        recyclerViewAdapter = new CopyDayAdapter(mActivity, new ArrayList<>(), listener, -1,day,week);
        mBinding.layoutCopyDay.recyclerView.setAdapter(recyclerViewAdapter);
        mBinding.layoutCopyDay.radioWeek.setOnCheckedChangeListener((radioGroup, i) -> {
            if (weeks.get(i) != null && weeks.get(i).getWeekdays() != null && weeks.get(i).getWeekdays().size() > 0) {
                mBinding.layoutCopyDay.recyclerView.setVisibility(View.VISIBLE);
                mBinding.layoutCopyDay.imgNoData.setVisibility(View.GONE);
                mBinding.btnSave.setVisibility(View.VISIBLE);
                recyclerViewAdapter = new CopyDayAdapter(mActivity, weeks.get(i).getWeekdays(), listener, i,day,week);
                mBinding.layoutCopyDay.recyclerView.setAdapter(recyclerViewAdapter);
            } else {
                mBinding.layoutCopyDay.recyclerView.setVisibility(View.GONE);
                mBinding.layoutCopyDay.imgNoData.setVisibility(View.VISIBLE);
                mBinding.btnSave.setVisibility(View.GONE);
                recyclerViewAdapter = new CopyDayAdapter(mActivity, new ArrayList<>(), listener, -1,day,week);
                mBinding.layoutCopyDay.recyclerView.setAdapter(recyclerViewAdapter);
            }
        });
        setRadioGroup();
        mBinding.btnSave.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("data", weeks);
            intent.putExtra("week", mActivity.getIntent().getIntExtra("week", 0));
            intent.putExtra("day", mActivity.getIntent().getIntExtra("day", 0));
            mActivity.setResult(Activity.RESULT_OK, intent);
            mActivity.finish();
        });
    }

    private void setRadioGroup() {

        if (getArguments() != null) {
            weeks = getArguments().getParcelableArrayList("data");
        }
        mBinding.layoutCopyDay.radioWeek.removeAllViews();
        for (int i = 0; i < weeks.size(); i++) {
            RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.custom_radiobutton, null);//initialize and set content
            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.leftMargin = 10;
            layoutParams.rightMargin = 10;
            radioButton.setLayoutParams(layoutParams);
            Utils.setRadioButtonDrawable(mActivity, radioButton);
            radioButton.setTextSize(12);
            radioButton.setText("Week " + (i + 1));
            radioButton.setId(i);
            mBinding.layoutCopyDay.radioWeek.addView(radioButton);
            if (i == 0)
                radioButton.setChecked(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        callWorkoutList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            //session.editor.remove(CLIENT_ID).commit();
            mActivity.startActivityForResult(new Intent(getActivity(), EditWorkoutActivity.class).putExtra("data", data.getSerializableExtra("data")), 0);
        }
    }


    CopyDayAdapter.CopyDayListener listener = (week, day, isChecked) -> {
        if (week != -1) weeks.get(week).getWeekdays().get(day).setSelected(isChecked ? 1 : 0);
    };
}
