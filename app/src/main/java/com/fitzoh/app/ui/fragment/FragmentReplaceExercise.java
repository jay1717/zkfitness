package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewPagerAdapter;
import com.fitzoh.app.databinding.FragmentAddExerciseBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentReplaceExercise extends BaseFragment {

    FragmentAddExerciseBinding mBinding;
    ViewPagerAdapter adapter;
    private int workout_id = 0;

    public FragmentReplaceExercise() {
        // Required empty public constructor
    }

    public static FragmentReplaceExercise newInstance(Bundle bundle) {
        FragmentReplaceExercise fragment = new FragmentReplaceExercise();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            workout_id = getArguments().getInt("workout_id");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.replace_exercise));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_add_exercise, container, false);
        setupViewPager(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.add_exercise));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.toolbar.toolbar.setElevation(0f);
        }
        /*mBinding.tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        mBinding.tabs.setTabTextColors(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray));*/

        return mBinding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK && data != null) {
            mActivity.setResult(Activity.RESULT_OK, data);
            mActivity.finish();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.addOnPageChangeListener(pageChangeListener);
        adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(FragmentSearchReplaceExercise.newInstance(1, workout_id), "Search  Exercise");
        adapter.addFragment(FragmentSearchReplaceExercise.newInstance(2, workout_id), "Muscle");
        adapter.addFragment(FragmentSearchReplaceExercise.newInstance(3, workout_id), "Recently Used");
        adapter.addFragment(FragmentSearchReplaceExercise.newInstance(4, workout_id), "Frequently Used");
        viewPager.setAdapter(adapter);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        int currentPosition = 0;

        @Override
        public void onPageSelected(int newPosition) {

            FragmentLifeCycle fragmentToHide = (FragmentLifeCycle) adapter.getItem(currentPosition);
            fragmentToHide.onPauseFragment();

            FragmentLifeCycle fragmentToShow = (FragmentLifeCycle) adapter.getItem(newPosition);
            fragmentToShow.onResumeFragment();
            currentPosition = newPosition;

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    };
}
