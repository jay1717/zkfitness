package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentPackageDetailBinding;
import com.fitzoh.app.model.ProfilePackageModel;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class PackageDetailFragment extends BaseFragment implements YouTubePlayer.OnInitializedListener {

    FragmentPackageDetailBinding mBinding;
    ProfilePackageModel.DataBean dataBean;

    public PackageDetailFragment() {
        // Required empty public constructor
    }

    public static PackageDetailFragment newInstance() {
        PackageDetailFragment fragment = new PackageDetailFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_package_detail, container, false);
//        mBinding.toolbar.imgBack.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back));
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        // mBinding.toolbar.tvTitle.setText(R.string.packages);
        Intent intent = getActivity().getIntent();
        if (null != intent) {

            dataBean = (ProfilePackageModel.DataBean) getActivity().getIntent().getSerializableExtra("dataBeanClass");

            mBinding.layout.txtPackageNameValue.setText(dataBean.getPackage_name());
            mBinding.layout.txtPackageDescriptionValue.setText(dataBean.getDesc());
            mBinding.layout.txtPackageDiscountValue.setText(String.valueOf(dataBean.getDiscounted_price()));
            mBinding.layout.txtPackagePriceValue.setText(String.valueOf(dataBean.getPrice()));
            mBinding.layout.txtPackageWeekValue.setText(String.valueOf(dataBean.getWeeks()));
            mBinding.toolbar.tvTitle.setTextColor(getActivity().getResources().getColor(R.color.gray));
            mBinding.toolbar.tvTitle.setText(dataBean.getPackage_name());

            if (dataBean.getMedia_type().equals("Image")) {
                mBinding.layout.imgMain.setVisibility(View.VISIBLE);
                mBinding.layout.youtubeView.setVisibility(View.GONE);
                Glide.with(getActivity())
                        .load(dataBean.getMedia())
                        .into(mBinding.layout.imgMain);
            } else if (dataBean.getMedia_type().equals("Youtube")) {
                mBinding.layout.imgMain.setVisibility(View.GONE);
                mBinding.layout.youtubeView.setVisibility(View.VISIBLE);
            }


        }
        mBinding.toolbar.imgBack.setOnClickListener(view -> getActivity().onBackPressed());

        return mBinding.getRoot();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
