package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.AddFoodFragment;
import com.fitzoh.app.ui.fragment.SearchFoodFragment;

public class AddFoodActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_client);
        AddFoodFragment addFoodFragment = AddFoodFragment.newInstance();
        addFoodFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, addFoodFragment, "addFoodFragment")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
