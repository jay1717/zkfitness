package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientCreatedNutritionPlanAdapter;
import com.fitzoh.app.adapter.NutritionPlanAdapter;
import com.fitzoh.app.databinding.FragmentNutritionPlanBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.MealListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddFoodActivity;
import com.fitzoh.app.ui.activity.CreateDishActivity;
import com.fitzoh.app.ui.dialog.AddDietDialog;
import com.fitzoh.app.ui.dialog.AddMealDialog;
import com.fitzoh.app.ui.dialog.AddNoteDialog;
import com.fitzoh.app.ui.dialog.AddWorkoutDialog;
import com.fitzoh.app.ui.dialog.NutritionPlanDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteDietPlan;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.mealList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientCreatedNutritionPlanFragment extends BaseFragment implements View.OnClickListener, AddWorkoutDialog.DialogClickListener, AddMealDialog.DialogClickListener, AddDietDialog.DialogClickListener, SingleCallback, ClientCreatedNutritionPlanAdapter.onAddMeal, AddNoteDialog.DialogClickListener, SwipeRefreshLayout.OnRefreshListener {


    FragmentNutritionPlanBinding mBinding;
    int dietId = 0;
    String dietName;
    private String userId, userAccessToken;
    List<MealListModel.DataBean> mealData;

    public ClientCreatedNutritionPlanFragment() {
        // Required empty public constructor
    }

    public static ClientCreatedNutritionPlanFragment newInstance() {
        ClientCreatedNutritionPlanFragment fragment = new ClientCreatedNutritionPlanFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_nutrition_plan, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.nutririon.fab);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mealData = new ArrayList<>();
        mBinding.nutririon.recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.nutririon.swipeContainer.setOnRefreshListener(this);
        mBinding.nutririon.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.nutririon.recycler.setAdapter(new ClientCreatedNutritionPlanAdapter(getActivity(), mealData, this, userId, userAccessToken));
        mBinding.toolbar.tvTitle.setText(R.string.nutrition_plan);
        Utils.setImageBackground(mActivity, mBinding.nutririon.imgEye, R.drawable.ic_view);
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgAction, R.drawable.ic_bell);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Intent intent = getActivity().getIntent();
        if (null != intent) {
            dietId = intent.getExtras().getInt("dietId");
            dietName = intent.getExtras().getString("dietName");
            if (dietName != null)
                mBinding.nutririon.txtTitle.setText(dietName);
            getNutririonList();
        }
        mBinding.nutririon.imgNote.setVisibility(View.GONE);
        mBinding.nutririon.fab.hide();

        setClickEvents();
        return mBinding.getRoot();
    }

    private void getNutririonList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.nutririon.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getMealList(dietId, session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), mealList, this);
        }
        else {
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setClickEvents() {
        Utils.setAddFabBackground(mActivity, mBinding.nutririon.fab);
        mBinding.toolbar.imgBack.setOnClickListener(this);
        mBinding.nutririon.imgEye.setOnClickListener(this);
        mBinding.nutririon.fab.setOnClickListener(this);
        mBinding.nutririon.imgNote.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().onBackPressed();
                break;
            case R.id.img_eye:
                NutritionPlanDialog dialog = new NutritionPlanDialog(dietId);
                dialog.setListener(this);
                dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), NutritionPlanDialog.class.getSimpleName());
                dialog.setCancelable(false);
                break;
            case R.id.fab:
                AddMealDialog dialogMeal = new AddMealDialog(dietId, this);
                dialogMeal.setListener(this);
                dialogMeal.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddMealDialog.class.getSimpleName());
                dialogMeal.setCancelable(false);
                break;
            case R.id.img_note:
                AddNoteDialog dialogNote = new AddNoteDialog(dietId);
                dialogNote.setListener(this);
                dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddNoteDialog.class.getSimpleName());
                dialogNote.setCancelable(false);
                break;

        }
    }

    @Override
    public void setClick() {
        // getNutririonList();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case mealList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                mealData = new ArrayList<>();
                MealListModel mealListModel = (MealListModel) o;
                if (mealListModel.getStatus() == AppConstants.SUCCESS) {

                    if (mealListModel.getData() != null) {
                        mealData.addAll(mealListModel.getData());
                        if (mealData.size() == 0) {
                            mBinding.nutririon.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.nutririon.recycler.setVisibility(View.GONE);
                        } else {
                            mBinding.nutririon.imgNoData.setVisibility(View.GONE);
                            mBinding.nutririon.recycler.setVisibility(View.VISIBLE);
                            mBinding.nutririon.recycler.setAdapter(new ClientCreatedNutritionPlanAdapter(getActivity(), mealData, this, userId, userAccessToken));
                        }
                    } else {
                        showSnackBar(mBinding.layoutNutrition, mealListModel.getMessage(), Snackbar.LENGTH_LONG);
                    }

                }
                break;
            case deleteDietPlan:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS)
                    getNutririonList();

                else
                    showSnackBar(mBinding.layoutNutrition, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void add(MealListModel.DataBean dataBean) {
        mActivity.startActivityForResult(new Intent(mActivity, AddFoodActivity.class).putExtra("diet_plan_id", dietId).putExtra("diet_plan_meal_id", dataBean.getId()), 10);
    }

    @Override
    public void edit(MealListModel.DataBean dataBean, int position) {
        AddMealDialog dialogMeal = new AddMealDialog(dataBean.getTime(), dataBean.getName(), dataBean.getDiet_plan_id(), dataBean.getId(), this);
        dialogMeal.setListener(this);
        dialogMeal.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddMealDialog.class.getSimpleName());
        dialogMeal.setCancelable(false);
    }

    @Override
    public void delete(MealListModel.DataBean dataBean) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteMealDelete(dataBean.getId(), session.getStringDataByKeyNull(CLIENT_ID))
                , getCompositeDisposable(), deleteDietPlan, this);
    }

    @Override
    public void create(MealListModel.DataBean dataBean) {
        //create dish
        mActivity.startActivityForResult(new Intent(mActivity, CreateDishActivity.class).putExtra("diet_plan_id", dietId).putExtra("diet_plan_meal_id", dataBean.getId()), 10);
    }

    @Override
    public void onRefresh() {
        getNutririonList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            getNutririonList();
        }
    }

    @Override
    public void refreshData() {
        if (Utils.isOnline(getActivity()))
            getNutririonList();
        else
            showSnackBar(mBinding.layoutNutrition, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }
}
