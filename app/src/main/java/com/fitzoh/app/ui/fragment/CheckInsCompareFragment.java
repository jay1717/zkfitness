package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.CheckInsDataAdapter;
import com.fitzoh.app.databinding.FragmentCheckInsDataBinding;
import com.fitzoh.app.model.CheckInDataModel;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddCheckInActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInsCompareFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {

    FragmentCheckInsDataBinding mBinding;
    String userId, userAccessToken;
    List<CheckInDataModel.DataBean> checkInList = new ArrayList<>();
    CheckInsDataAdapter checkInsDataAdapter;
    private String sortBy = "ascending";

    public CheckInsCompareFragment() {
        // Required empty public constructor
    }

    public static CheckInsCompareFragment newInstance(Bundle bundle) {
        CheckInsCompareFragment fragment = new CheckInsCompareFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Check In List");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_check_in, menu);
        menu.findItem(R.id.action_ascending).setVisible(false);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.action_ascending);
        if (sortBy.equalsIgnoreCase("descending"))
            menuItem.setIcon(Utils.getBackgroundDrawable(mActivity, new TextView(mActivity), R.drawable.ic_descending));
        else
            menuItem.setIcon(Utils.getBackgroundDrawable(mActivity, new TextView(mActivity), R.drawable.ic_ascending));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_ascending:
                sortBy = sortBy.equalsIgnoreCase("ascending") ? "descending" : "ascending";
                mActivity.invalidateOptionsMenu();
//                callCheckInList();
                return false;
            case R.id.action_filter:
                mActivity.onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_check_ins_data, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.layoutCheckin.fab);
        mBinding.layoutCheckin.fab.setVisibility(View.GONE);
        mBinding.layoutCheckin.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutCheckin.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.layoutCheckin.fab.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), AddCheckInActivity.class));
        });
        setHasOptionsMenu(true);
        checkInList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutCheckin.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        checkInsDataAdapter = new CheckInsDataAdapter(checkInList, mActivity);
        mBinding.layoutCheckin.recyclerView.setAdapter(checkInsDataAdapter);
//        callCheckInList();
        getData();
        return mBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null) {
            if (getArguments().getSerializable("data1") != null) {
                checkInList.add((CheckInDataModel.DataBean) getArguments().getSerializable("data1"));
            }
            if (getArguments().getSerializable("data2") != null) {
                checkInList.add((CheckInDataModel.DataBean) getArguments().getSerializable("data2"));
            }
            checkInsDataAdapter = new CheckInsDataAdapter(checkInList, mActivity);
            mBinding.layoutCheckin.recyclerView.setAdapter(checkInsDataAdapter);
        }
    }

   /* private void callCheckInList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getCheckInData(sortBy)
                    , getCompositeDisposable(), checkInForms, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }*/

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case checkInForms:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                checkInList = new ArrayList<>();
                CheckInDataModel CheckinData = (CheckInDataModel) o;
                if (CheckinData.getStatus() == AppConstants.SUCCESS) {

                    if (CheckinData != null) {
                        checkInList.addAll(CheckinData.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (checkInList.size() == 0) {
                            mBinding.layoutCheckin.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutCheckin.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutCheckin.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutCheckin.imgNoData.setVisibility(View.GONE);
                            checkInsDataAdapter = new CheckInsDataAdapter(checkInList, mActivity);
                            mBinding.layoutCheckin.recyclerView.setAdapter(checkInsDataAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, CheckinData.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        mBinding.layoutCheckin.swipeContainer.setRefreshing(false);
    }
}
