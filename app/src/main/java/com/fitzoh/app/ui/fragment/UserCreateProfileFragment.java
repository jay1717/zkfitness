package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.system.ErrnoException;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.ApplozicClient;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.applozic.mobicomkit.api.account.user.PushNotificationTask;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;
import com.applozic.mobicomkit.uiwidgets.ApplozicSetting;
import com.fitzoh.app.ApplicationClass;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentUserDetailSignupBinding;
import com.fitzoh.app.interfaces.MyLocationChangeListner;
import com.fitzoh.app.model.ClientProfileAddModel;
import com.fitzoh.app.model.FitnessValuesModel;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.model.VerificationOTPModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreateUserActivity;
import com.fitzoh.app.ui.activity.MedicalFormActivity;
import com.fitzoh.app.ui.activity.SelectFitnessValueActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getFitnessValue;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateClientProfile;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserCreateProfileFragment extends BaseFragment implements View.OnClickListener, PlaceSelectionListener, GoogleMap.OnInfoWindowClickListener, MyLocationChangeListner, SingleCallback {

    FragmentUserDetailSignupBinding mBinding;
    private static final int RECORD_REQUEST_CODE = 101;
    public Uri mImageUri;
    TrainerProfileModel trainerProfileModel = null;
    Bitmap bitmap;
    byte[] byteImage;
    VerificationOTPModel clientModel;
    File fileImage;
    String userId, userAccessToken;
    MenuItem menuItem;
    boolean isEditable;
    HashMap<String, FitnessValuesModel.DataBean> selectedFitneeList;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    //    Location location;
    LatLng latLng;
    String locationAPI;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int REQUEST_SELECT_PLACE = 1000;
    private static final int REQUEST_SELECT_FITNESS_VELUE = 1001;
    private String countryName = "", cityName = "", stateName = "";
    MultipartBody.Part body;
    String edtString = "";
    private UserLoginTask mAuthTask = null;
    String stringName = "";
    boolean isFromRegister = true;
    ClientProfileAddModel clientProfileAddModel;

    public UserCreateProfileFragment() {
        // Required empty public constructor
    }

    public static UserCreateProfileFragment newInstance() {
        return new UserCreateProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_user_detail_signup, container, false);
        Utils.getShapeGradient(mActivity, mBinding.createprofile.btnNext);
        Utils.setImageAddBackgroundImage(mActivity, mBinding.createprofile.imgPickPhoto);
        Utils.setTextViewEndImage(mActivity, mBinding.createprofile.edtFitness, R.drawable.ic_down_arrow);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setClickEvent();
        prepareLayout();
        makeRequest();
        //callFitnessValueApi();
        return mBinding.getRoot();


    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.createprofile.edtAddress.setClickable(true);

    }

    private void setClickEvent() {
        mBinding.createprofile.btnNext.setOnClickListener(this);
        mBinding.createprofile.imgPickPhoto.setOnClickListener(this);
        mBinding.createprofile.edtAddress.setOnClickListener(this);
        mBinding.createprofile.btnNext.setOnClickListener(this);
        mBinding.createprofile.edtFitness.setKeyListener(null);
        mBinding.createprofile.edtFitness.setOnClickListener(this);
    }

    public void attemptLogin(User.AuthenticationType authenticationType) {

        UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {

            @Override
            public void onSuccess(RegistrationResponse registrationResponse, final Context context) {
                mAuthTask = null;
                ApplozicClient.getInstance(context).setContextBasedChat(true).setHandleDial(true);

                Map<ApplozicSetting.RequestCode, String> activityCallbacks = new HashMap<ApplozicSetting.RequestCode, String>();
                activityCallbacks.put(ApplozicSetting.RequestCode.USER_LOOUT, CreateUserActivity.class.getName());
                ApplozicSetting.getInstance(context).setActivityCallbacks(activityCallbacks);
                MobiComUserPreference.getInstance(context).setUserRoleType(registrationResponse.getRoleType());


                PushNotificationTask.TaskListener pushNotificationTaskListener = new PushNotificationTask.TaskListener() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {

                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
//
                    }
                };
                PushNotificationTask pushNotificationTask = new PushNotificationTask(Applozic.getInstance(context).getDeviceRegistrationId(), pushNotificationTaskListener, context);
                pushNotificationTask.execute((Void) null);


            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                mAuthTask = null;
                try {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(exception.getMessage().toString());
                    alertDialog.setMessage(exception.toString());
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok_alert),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        User user = new User();
        user.setUserId(String.valueOf(session.getAuthorizedUser(getActivity()).getId()));
        user.setEmail(session.getAuthorizedUser(getActivity()).getEmail());
        user.setPassword("123456");
        user.setDisplayName(session.getAuthorizedUser(getActivity()).getName());
        //  user.setContactNumber(session.getAuthorizedUser(getActivity()).getMobile_number());
        user.setImageLink(clientProfileAddModel.getData().getPhoto());
        user.setAuthenticationTypeId(authenticationType.getValue());

        mAuthTask = new UserLoginTask(user, listener, ApplicationClass.getAppContext());
        mAuthTask.execute((Void) null);
    }

    private void prepareLayout() {

        String text = "<font color=#4b4b4b>What is your main goal?</font> <font color=#8a8a8a> (max 140 characters)</font>";
        mBinding.createprofile.edtGoal.setHint(Html.fromHtml(text));
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER");
        }

        if (getArguments() != null && getArguments().containsKey("TRAINER_MODEL")) {
            trainerProfileModel = (TrainerProfileModel) getArguments().getSerializable("TRAINER_MODEL");
        }
        if (isFromRegister) {
            setupToolBar(mBinding.toolbar.toolbar, "");
            setHasOptionsMenu(false);
            mBinding.createprofile.btnNext.setText(R.string.next);
            mBinding.createprofile.btnNext.setVisibility(View.VISIBLE);
        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Profile");
            setHasOptionsMenu(true);
            mBinding.createprofile.btnNext.setText("Save");
            mBinding.createprofile.btnNext.setVisibility(View.GONE);
            mBinding.createprofile.edtAddress.setEnabled(false);
            mBinding.createprofile.edtAddress.setClickable(false);
            mBinding.createprofile.edtFitness.setEnabled(false);
            mBinding.createprofile.edtFitness.setClickable(false);
            mBinding.createprofile.edtFitness.setFocusable(false);
            mBinding.createprofile.edtGoal.setEnabled(false);
            mBinding.createprofile.edtRoutine.setEnabled(false);
            mBinding.createprofile.edtLandmark.setEnabled(false);
            mBinding.createprofile.imgPickPhoto.setClickable(false);
            mBinding.createprofile.imgPickPhoto.setEnabled(false);
            callGetApi();
        }


    }

    private void callGetApi() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(false);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientProfileDetail(session.getAuthorizedUser(getContext()).getId())
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.layoutCreateProfile, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setData() {
        if (trainerProfileModel != null) {
            mBinding.createprofile.edtGoal.setText(trainerProfileModel.getData().getAbout());
            mBinding.createprofile.edtAddress.setText(trainerProfileModel.getData().getAddress() + " " + trainerProfileModel.getData().getCity() + " " + trainerProfileModel.getData().getState() + " " + trainerProfileModel.getData().getCountry());
            if (trainerProfileModel.getData().getPhoto() != null) {
                Utils.setImage(mContext, mBinding.createprofile.imgUser, trainerProfileModel.getData().getPhoto());
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_set, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.action_edit), R.drawable.ic_edit);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuItem = item;
        switch (item.getItemId()) {
            case R.id.action_edit:
                isEditable = !isEditable;
                setEditTextEnable(isEditable);
                break;
        }
        return true;
    }

    private void setEditTextEnable(boolean isEditable) {
        mBinding.createprofile.edtAddress.setEnabled(isEditable);
        mBinding.createprofile.edtAddress.setClickable(isEditable);
        mBinding.createprofile.edtGoal.setEnabled(isEditable);
        mBinding.createprofile.edtRoutine.setEnabled(isEditable);
        mBinding.createprofile.edtLandmark.setEnabled(isEditable);
        mBinding.createprofile.edtFitness.setClickable(isEditable);
        mBinding.createprofile.edtFitness.setEnabled(isEditable);
        mBinding.createprofile.edtFitness.setFocusable(isEditable);
        mBinding.createprofile.imgPickPhoto.setClickable(isEditable);
        mBinding.createprofile.imgPickPhoto.setEnabled(isEditable);
        if (isEditable) {
            mBinding.createprofile.btnNext.setVisibility(View.VISIBLE);
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_cancel));
        } else {
            mBinding.createprofile.btnNext.setVisibility(View.GONE);
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_edit));

        }
    }

    /*  private void getLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                getCurrentLocation();

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            getCurrentLocation();
        }
    }*/


    public void performSearchClick() {


        if (Utils.isOnline(mContext)) {
            try {
                AutocompleteFilter filter = new AutocompleteFilter.Builder()
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                        .build();
                Intent intent = new PlaceAutocomplete.IntentBuilder
                        (PlaceAutocomplete.MODE_FULLSCREEN)
                        .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                        .setFilter(filter)
                        .build(mActivity);
                mActivity.startActivityForResult(intent, REQUEST_SELECT_PLACE);
                mBinding.createprofile.edtAddress.setClickable(false);

            } catch (GooglePlayServicesRepairableException |
                    GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), R.string.no_network_available, Toast.LENGTH_LONG).show();
        }
    }

    public boolean validation() {

        boolean value = true;
        if (mBinding.createprofile.edtAddress.getText().toString().equals(getString(R.string.what_is_your_address))) {
            mBinding.createprofile.txtErrorAddress.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorAddress, getString(R.string.address_empty));
            value = false;
        }
        if (mBinding.createprofile.edtGoal.getText().toString().length() == 0) {
            mBinding.createprofile.txtErrorGoal.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorGoal, getString(R.string.empty_goal));
            value = false;
        }

        if (mBinding.createprofile.edtRoutine.getText().toString().length() == 0) {
            mBinding.createprofile.txtErrorRoutine.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorRoutine, getString(R.string.empty_routine));
            value = false;
        }

        if (mBinding.createprofile.edtFitness.getText().toString().length() == 0) {
            mBinding.createprofile.txtErrorFitness.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorFitness, getString(R.string.empty_fitness));
            value = false;
        }
        return value;


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_SELECT_PLACE) {
                Place place = PlaceAutocomplete.getPlace(mContext, data);
                this.onPlaceSelected(place);
            } else if (requestCode == REQUEST_CHECK_SETTINGS) {
                // getLocation();
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(mContext, data);
            this.onError(status);
        }
        if (requestCode == REQUEST_SELECT_FITNESS_VELUE) {
            if (resultCode == RESULT_OK) {
                Log.d("Result", "");
                if (data != null) {
                    selectedFitneeList = (HashMap<String, FitnessValuesModel.DataBean>) data.getExtras().getSerializable("Selected Data");
                    setIntrestedEditText();

                }
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    mBinding.createprofile.imgUser.setImageURI(mImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }

    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        getActivity().startActivityForResult(getPickImageChooserIntent(), 200);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    protected void makeRequest() {
        requestAppPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA}, RECORD_REQUEST_CODE, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_pick_photo:
                imgPick();
                break;

            case R.id.edt_address:
                performSearchClick();
                break;

            case R.id.edt_fitness:
                Intent intent = new Intent(getActivity(), SelectFitnessValueActivity.class);
                if (selectedFitneeList != null)
                    intent.putExtra("peopleList", selectedFitneeList);
                getActivity().startActivityForResult(intent, REQUEST_SELECT_FITNESS_VELUE);
                break;

            case R.id.btn_next:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        mBinding.createprofile.txtErrorRoutine.setVisibility(View.GONE);
                        mBinding.createprofile.txtErrorAddress.setVisibility(View.GONE);
                        mBinding.createprofile.txtErrorGoal.setVisibility(View.GONE);
                        mBinding.createprofile.txtErrorFitness.setVisibility(View.GONE);

                        callAPI();

                    }
                } else {
                    showSnackBar(mBinding.layoutCreateProfile, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }

                break;
        }

    }

    private void callAPI() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);


        /*RequestBody name = RequestBody.create(MediaType.parse("text/plain"), firstNameField.getText().toString());*/

        RequestBody goal = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtGoal.getText().toString());
        RequestBody fitness = RequestBody.create(MediaType.parse("text/plain"), edtString);
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtAddress.getText().toString());
        RequestBody routine = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtRoutine.getText().toString());
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(session.getAuthorizedUser(getActivity()).getId()));
        //  RequestBody address = RequestBody.create(MediaType.parse("text/plain"), addressName);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), cityName);
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), stateName);
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), countryName);
        RequestBody location;
        if (!isFromRegister && latLng == null) {
            location = RequestBody.create(MediaType.parse("text/plain"), locationAPI);
        } else {
            location = RequestBody.create(MediaType.parse("text/plain"), latLng.latitude + "," + latLng.longitude);
        }
        RequestBody landmark = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtLandmark.getText().toString());

        if (fileImage != null && mImageUri != null) {
            byte[] uploadBytes = Utils.loadImageFromStorage(mImageUri.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
            //  RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            MultipartBody.Part bodyEmail = MultipartBody.Part.createFormData("photo", fileImage.getName(), reqFile);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).create_user_profile(
                    goal,
                    fitness,
                    address,
                    routine,
                    id, city, state, country, location, landmark, bodyEmail), getCompositeDisposable(), updateClientProfile, this);
        } else {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).create_user_profile_without_image(
                    goal,
                    fitness,
                    address,
                    routine,
                    city,
                    state,
                    country,
                    location,
                    landmark,
                    id), getCompositeDisposable(), updateClientProfile, this);
        }
    }

    private void getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress.getCountryName() != null) {
                    countryName = returnedAddress.getCountryName();
                }
                if (returnedAddress.getLocality() != null) {
                    cityName = returnedAddress.getLocality();
                }
                if (returnedAddress.getAdminArea() != null) {
                    stateName = returnedAddress.getAdminArea();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.e("on Place Selected", "Place Selected: " + place.getAddress() + "" + place.getLatLng().latitude + "" + place.getLatLng().longitude);
        storeLocationDetails(place.getLatLng().latitude, place.getLatLng().longitude, place.getAddress());
        getCompleteAddressString(place.getLatLng().latitude, place.getLatLng().longitude);
        mBinding.createprofile.edtAddress.setText(place.getAddress());
        mBinding.createprofile.edtAddress.setClickable(true);

//        mGoogleMap.clear();
        latLng = place.getLatLng();
        // setUpMap(mGoogleMap);

    }

    private void storeLocationDetails(double latitude, double longitude, CharSequence address) {
    }

    @Override
    public void onError(Status status) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onLocationChange(Location location) {

    }

    private void callFitnessValueApi() {
        if (Utils.isOnline(getContext())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getFitnessValue()
                    , getCompositeDisposable(), getFitnessValue, this);
        } else {
            showSnackBar(mBinding.layoutCreateProfile, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                clientModel = (VerificationOTPModel) o;
                if (clientModel != null && clientModel.getStatus() == AppConstants.SUCCESS && clientModel.getData() != null) {
                    mBinding.createprofile.edtAddress.setText(clientModel.getData().getAddress());
                    mBinding.createprofile.edtGoal.setText(clientModel.getData().getGoal());
                    mBinding.createprofile.edtLandmark.setText(clientModel.getData().getLandmark());
                    mBinding.createprofile.edtRoutine.setText(clientModel.getData().getLife_style());
                    cityName = clientModel.getData().getCity();
                    countryName = clientModel.getData().getCountry();
                    stateName = clientModel.getData().getState();
                    locationAPI = clientModel.getData().getLocation();
                    if (clientModel.getData().getFitness_intrested() != null && clientModel.getData().getFitness_intrested().size() > 0) {
                        callFitnessValueApi();
                    }

                    if (clientModel.getData().getPhoto() != null) {
                        Utils.setImage(mContext, mBinding.createprofile.imgUser, clientModel.getData().getPhoto());
                    }
                } else {
                    showSnackBar(mBinding.layoutCreateProfile, Objects.requireNonNull(clientModel).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case updateClientProfile:
                clientProfileAddModel = (ClientProfileAddModel) o;
                if (clientProfileAddModel.getStatus() == AppConstants.SUCCESS) {
                    attemptLogin(User.AuthenticationType.APPLOZIC);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isFromRegister) {
                                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                startActivity(new Intent(mActivity, MedicalFormActivity.class));
                                getActivity().finish();

                            } else {
                                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                getActivity().finish();
                            }

                        }
                    }, 2000);

                } else {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    showSnackBar(mBinding.layoutCreateProfile, clientProfileAddModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case getFitnessValue:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                FitnessValuesModel fitnessValuesModel = (FitnessValuesModel) o;
                if (fitnessValuesModel != null && fitnessValuesModel.getData() != null && fitnessValuesModel.getData().size() > 0) {
                    selectedFitneeList = new HashMap<String, FitnessValuesModel.DataBean>();
                    for (int i = 0; i < fitnessValuesModel.getData().size(); i++) {
                        for (int j = 0; j < clientModel.getData().getFitness_intrested().size(); j++) {
                            if (clientModel.getData().getFitness_intrested().get(j).getId() == fitnessValuesModel.getData().get(i).getId()) {
                                selectedFitneeList.put(String.valueOf(fitnessValuesModel.getData().get(i).getId()), fitnessValuesModel.getData().get(i));
                            }
                        }
                    }
                    setIntrestedEditText();

                }
                break;
        }
    }


    private void setIntrestedEditText() {

        if (selectedFitneeList != null) {
            stringName = "";
            edtString = "";
            for (String key : selectedFitneeList.keySet()) {
                stringName = stringName + "" + selectedFitneeList.get(key).getValue() + ",";
                edtString = edtString + "" + String.valueOf(selectedFitneeList.get(key).getId()) + ",";
            }
            if (edtString != "" && edtString.length() > 0 && edtString.endsWith(",")) {
                edtString = edtString.substring(0, edtString.length() - 1);
            }
            if (stringName != "" && stringName.length() > 0 && stringName.endsWith(",")) {
                stringName = stringName.substring(0, stringName.length() - 1);
            }
            Log.e("id", edtString);
            mBinding.createprofile.edtFitness.setText(stringName);
        }

    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutCreateProfile, "on Failure", Snackbar.LENGTH_LONG);
    }
}
