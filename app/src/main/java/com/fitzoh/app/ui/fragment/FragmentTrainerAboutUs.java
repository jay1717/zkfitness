package com.fitzoh.app.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainerAboutUsAdapter;
import com.fitzoh.app.databinding.FragmentTrainerAboutBinding;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

public class FragmentTrainerAboutUs extends BaseFragment implements SingleCallback {
    FragmentTrainerAboutBinding mBinding;

    String userId, userAccessToken;
    int trainerId = 0, gymId = 0;
    TrainerProfileModel trainerProfileModel = null;

    public FragmentTrainerAboutUs() {
    }

    public static FragmentTrainerAboutUs newInstance(int trainerId, int gymId) {
        FragmentTrainerAboutUs fragment = new FragmentTrainerAboutUs();
        Bundle bundle = new Bundle();
        bundle.putInt("data", trainerId);
        bundle.putInt("gymId", gymId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trainerId = getArguments().getInt("data");
            gymId = getArguments().getInt("gymId");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getTrainerProfile() {
        if (Utils.isOnline(getActivity())) {
            if (getArguments() != null && getArguments().containsKey("data")) {
                trainerId = getArguments().getInt("data");
            }
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerProfile(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerProfile, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void getGymProfile() {
        if (Utils.isOnline(getActivity())) {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerProfileGym(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getGymProfile, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_trainer_about, container, false);

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        if (gymId == 0)
            getTrainerProfile();
        else
            getGymProfile();

        return mBinding.getRoot();

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getTrainerProfile:
                trainerProfileModel = (TrainerProfileModel) o;
                if (trainerProfileModel != null) {
                    if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {
                        mBinding.txtAboutUs.setText(trainerProfileModel.getData().getAbout());
                        if (!trainerProfileModel.getData().getSpeciality().equals("")) {
                            mBinding.txtFacilities.setText("Speciality :");
                            mBinding.txtSpeciality.setVisibility(View.VISIBLE);
                            mBinding.txtSpeciality.setText(trainerProfileModel.getData().getSpeciality());
                        }

                    }
                }

                break;

            case getGymProfile:
                trainerProfileModel = (TrainerProfileModel) o;
                if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {
                    mBinding.txtAboutUs.setText(trainerProfileModel.getData().getAbout());
                    if (trainerProfileModel.getData().getFacilities().size() != 0) {
                        mBinding.txtFacilities.setText("Facilities :");
                        mBinding.txtSpeciality.setVisibility(View.GONE);
                        mBinding.txtSpeciality.setText("");
                        mBinding.recyclerView.setAdapter(new TrainerAboutUsAdapter(getActivity(), trainerProfileModel.getData().getFacilities()));
                    }
                }

                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }
}
