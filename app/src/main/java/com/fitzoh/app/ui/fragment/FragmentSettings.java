package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentSettingsBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.NavigationClientMainActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.ui.dialog.SelectColorDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSettings extends BaseFragment implements SingleCallback {

    FragmentSettingsBinding mBinding;
    private SharedPreferences pref;
    int redPrimary = 0, greenPrimary = 0, bluePrimary = 0, redAccent = 0, greenAccent = 0, blueAccent = 0;
    boolean isFromLogin = false;
    private String userId, userAccessToken;

    public FragmentSettings() {
        // Required empty public constructor
    }

    public static FragmentSettings newInstance(Bundle bundle) {
        FragmentSettings fragment = new FragmentSettings();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //  setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.settings));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_settings, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutSettings.txtReset.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        mBinding.layoutSettings.imgReset.setColorFilter(ContextCompat.getColor(mActivity, R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        if (getArguments() != null && getArguments().containsKey("IS_LOGIN") && getArguments().getBoolean("IS_LOGIN")) {
            isFromLogin = true;
            setupToolBar(mBinding.toolbar.toolbar, getString(R.string.app_theme));
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
            mBinding.txtSkip.setVisibility(View.VISIBLE);
        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.app_theme));
            mBinding.txtSkip.setVisibility(View.GONE);
        }
        mBinding.txtSkip.setOnClickListener(view->{
            Intent intent;
            if (session.getRollId() == 1) {
                intent = new Intent(mActivity, NavigationClientMainActivity.class);
            } else {
                intent = new Intent(mActivity, NavigationMainActivity.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            mActivity.finish();
        });
        Utils.getShapeGradient(mActivity, mBinding.layoutSettings.btnSelect);
        setListener();
        return mBinding.getRoot();
    }

    private void setListener() {
        pref = mContext.getSharedPreferences("colors", 0);
        redPrimary = pref.getInt("redPrimary", 21);
        bluePrimary = pref.getInt("bluePrimary", 215);
        greenPrimary = pref.getInt("greenPrimary", 153);

        redAccent = pref.getInt("redAccent", 58);
        blueAccent = pref.getInt("blueAccent", 209);
        greenAccent = pref.getInt("greenAccent", 190);

        mBinding.layoutSettings.imgPrimary.setBackgroundColor(Color.rgb(redPrimary, greenPrimary, bluePrimary));
        mBinding.layoutSettings.imgAccent.setBackgroundColor(Color.rgb(redAccent, greenAccent, blueAccent));

        mBinding.layoutSettings.imgPrimary.setOnClickListener(view -> {
            showDialog(0);
        });
        mBinding.layoutSettings.imgAccent.setOnClickListener(view -> {
            showDialog(100);
        });
        mBinding.layoutSettings.btnSelect.setOnClickListener(view -> {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("redPrimary", redPrimary);
            editor.putInt("bluePrimary", bluePrimary);
            editor.putInt("greenPrimary", greenPrimary);
            editor.putInt("redAccent", redAccent);
            editor.putInt("blueAccent", blueAccent);
            editor.putInt("greenAccent", greenAccent);
            editor.apply();
            String primaryColor = redPrimary + "," + greenPrimary + "," + bluePrimary;
            String secondaryColor = redAccent + "," + greenAccent + "," + blueAccent;
            saveColor(primaryColor, secondaryColor);
        });
        mBinding.layoutSettings.layoutReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pref.edit().clear().commit();
                saveColor("","");
            }
        });
    }

    private void saveColor(String primaryColor, String secondaryColor) {
        JSONObject object = new JSONObject();
        try {
            object.put("primary_color_code", primaryColor);
            object.put("secondary_color_code", secondaryColor);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), object.toString());
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).settings(requestBody)
                , getCompositeDisposable(), single, this);
    }

    private void showDialog(int code) {
        SelectColorDialog dialog = new SelectColorDialog();
        dialog.setTargetFragment(this, code);
        dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), SelectColorDialog.class.getSimpleName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            redPrimary = data.getIntExtra("red", 0);
            greenPrimary = data.getIntExtra("green", 0);
            bluePrimary = data.getIntExtra("blue", 0);
            mBinding.layoutSettings.imgPrimary.setBackgroundColor(Color.rgb(redPrimary, greenPrimary, bluePrimary));
        } else if (requestCode == 100 && resultCode == Activity.RESULT_OK && data != null) {
            redAccent = data.getIntExtra("red", 0);
            greenAccent = data.getIntExtra("green", 0);
            blueAccent = data.getIntExtra("blue", 0);
            mBinding.layoutSettings.imgAccent.setBackgroundColor(Color.rgb(redAccent, greenAccent, blueAccent));
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS) {
                    Intent intent;
                    if (session.getRollId() == 1) {
                        intent = new Intent(mActivity, NavigationClientMainActivity.class);
                    } else {
                        intent = new Intent(mActivity, NavigationMainActivity.class);
                    }
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    mActivity.finish();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }
}
