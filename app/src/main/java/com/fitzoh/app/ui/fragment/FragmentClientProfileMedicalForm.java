package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientProfileMedicalFormAdapter;
import com.fitzoh.app.databinding.FragmentClientProfileMedicalFormBinding;
import com.fitzoh.app.model.MedicalFormModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentClientProfileMedicalForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentClientProfileMedicalForm extends BaseFragment implements SingleCallback {

    FragmentClientProfileMedicalFormBinding mBinding;
    String userId, userAccessToken;
    private List<MedicalFormModel.DataBean> modelList;
    private ClientProfileMedicalFormAdapter medicalFormAdapter;
    boolean isFromRegister = true;
    int clientId;


    public FragmentClientProfileMedicalForm() {
        // Required empty public constructor
    }


    public static FragmentClientProfileMedicalForm newInstance() {
        return new FragmentClientProfileMedicalForm();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clientId = getArguments().getInt("data");
            //  data = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_profile_medical_form, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
//        mBinding.medicalForm.recyclerView.setAdapter(new MedicalFormAdapter(getAdapterData()));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.medical_info));

        if (Utils.isOnline(getContext())) {
            getMedicalFormData();
        } else
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);

        return mBinding.getRoot();

    }


    private void getMedicalFormData() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientProileMedicalForm(clientId)
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                MedicalFormModel medicalFormModel = (MedicalFormModel) o;
                if (medicalFormModel != null && medicalFormModel.getStatus() == AppConstants.SUCCESS && medicalFormModel.getData() != null && medicalFormModel.getData().size() > 0) {
                    modelList = medicalFormModel.getData();
                    medicalFormAdapter = new ClientProfileMedicalFormAdapter(mActivity, modelList, true);
                    mBinding.medicalForm.recyclerView.setAdapter(medicalFormAdapter);
                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }
}
