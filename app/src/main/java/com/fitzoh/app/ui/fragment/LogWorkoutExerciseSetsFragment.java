package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.LogWorkoutSetsAdapter;
import com.fitzoh.app.databinding.FragmentLogWorkoutExerciseSetsBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class LogWorkoutExerciseSetsFragment extends BaseFragment implements View.OnClickListener, SingleCallback {


    FragmentLogWorkoutExerciseSetsBinding mBinding;
    private String userId, userAccessToken;
    LogWorkoutSetsAdapter recyclerViewAdapter;
    List<LogWorkoutDataModel.DataBean> logWorkoutDataModels = new ArrayList<>();
    private LogWorkoutDataModel.DataBean dataBean;
    private int workout_id = 0;

    public LogWorkoutExerciseSetsFragment() {
        // Required empty public constructor
    }

    public static LogWorkoutExerciseSetsFragment newInstance(Bundle bundle) {
        LogWorkoutExerciseSetsFragment fragment = new LogWorkoutExerciseSetsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            workout_id = getArguments().getInt("workout_id", 0);
            if (getArguments().containsKey("data")) {
                dataBean = getArguments().getParcelable("data");
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_log_workout_exercise_sets, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.getShapeGradient(mActivity, mBinding.btnSave);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (dataBean.getSets() != null && dataBean.getSets().size() > 0) {
            recyclerViewAdapter = new LogWorkoutSetsAdapter(mActivity, new ArrayList<>(), 0, 0, null, false);
            mBinding.layoutLogWorkout.recyclerView.setAdapter(recyclerViewAdapter);
            mBinding.layoutLogWorkout.recyclerView.setVisibility(View.VISIBLE);
            mBinding.layoutLogWorkout.imgNoData.setVisibility(View.GONE);
        } else {
            mBinding.layoutLogWorkout.recyclerView.setVisibility(View.GONE);
            mBinding.layoutLogWorkout.imgNoData.setVisibility(View.VISIBLE);
        }
        /*mBinding.btnSave.setOnClickListener(view -> {
            List<LogWorkoutDataModel.DataBean.SetsList> data = recyclerViewAdapter.getData();
            for (int i = 0; i < data.size(); i++) {
                Log.e("onCreateView: ", "" + data.get(i).getIsChecked());
                for (int j = 0; j < data.get(i).size(); j++) {
                    Log.e("onCreateViewKG: ", "" + data.get(i).getEnter_kg());
                    Log.e("onCreateViewREPS: ", "" + data.get(i).getEnter_reps());

                }
            }
        });*/
        setHasOptionsMenu(true);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Log Workout");
        return mBinding.getRoot();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LogWorkoutDataModel checkInFormListModel = (LogWorkoutDataModel) o;
                if (checkInFormListModel != null && checkInFormListModel.getStatus() == AppConstants.SUCCESS && checkInFormListModel.getData() != null && checkInFormListModel.getData().size() > 0) {
//                    logWorkoutDataModels = checkInFormListModel.getData();
//                    recyclerViewAdapter.addDataToAdapter(logWorkoutDataModels);
                } else {
                    showSnackBar(mBinding.mainLayout, checkInFormListModel.getMessage(), Snackbar.LENGTH_LONG);
                    mActivity.onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, throwable.getMessage() == null ? "onFailure" : throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_log_workout, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.action_add), R.drawable.ic_plus_training_program);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notes:
                break;
            case R.id.action_add:
                break;
        }
        return true;
    }
}
