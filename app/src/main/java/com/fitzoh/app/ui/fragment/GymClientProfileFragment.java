package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentClientProfileBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GymClientListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityOfClientActivity;
import com.fitzoh.app.ui.activity.ClientDocumentActivity;
import com.fitzoh.app.ui.activity.ClientProfileMedicalFormActivity;
import com.fitzoh.app.ui.activity.ClientProfileNutritionActivity;
import com.fitzoh.app.ui.activity.ClientProfilePurchaseActivity;
import com.fitzoh.app.ui.activity.ClientProfileReportActivity;
import com.fitzoh.app.ui.activity.ClientProfileSchedulesActivity;
import com.fitzoh.app.ui.activity.ClientProfileTrainingProgramActivity;
import com.fitzoh.app.ui.activity.ClientProfileWorkoutActivity;
import com.fitzoh.app.ui.activity.TrainerClientBasicInfoActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.ui.fragment.ClientProfileFragment.setImageBackground;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GymClientProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GymClientProfileFragment extends BaseFragment implements View.OnClickListener, SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentClientProfileBinding mBinding;
    View view;
    GymClientListModel.DataBean dataBean;
    String userId, userAccessToken;

    public GymClientProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ClientProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GymClientProfileFragment newInstance(Bundle ClientData) {
        GymClientProfileFragment fragment = new GymClientProfileFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(ClientData);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            dataBean = (GymClientListModel.DataBean) getArguments().get("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.str_client_profile));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_profile, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        getClientProfile();
        setClicks();
        return mBinding.getRoot();
    }

    private void setClicks() {
        Utils.setSwitchTint(mContext, mBinding.layoutClientProfile.toggle);
        mBinding.layoutClientProfile.toggle.setChecked(dataBean.getIs_active() == 1);
        mBinding.layoutClientProfile.toggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            setAssign(isChecked);
        });
        mBinding.layoutClientProfile.linActivity.setOnClickListener(this);
        mBinding.layoutClientProfile.linWorkout.setOnClickListener(this);
        mBinding.layoutClientProfile.linNutritions.setOnClickListener(this);
        mBinding.layoutClientProfile.linTrainingProgram.setOnClickListener(this);
        mBinding.layoutClientProfile.linSchedules.setOnClickListener(this);
        mBinding.layoutClientProfile.linReports.setOnClickListener(this);
        mBinding.layoutClientProfile.linDocuments.setOnClickListener(this);
        mBinding.layoutClientProfile.linPurchase.setOnClickListener(this);
        mBinding.layoutClientProfile.linMedical.setOnClickListener(this);
        mBinding.layoutClientProfile.linBasicInfo.setOnClickListener(this);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgActivity, R.drawable.ic_client_profile_activity_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgWorkout, R.drawable.ic_client_profile_workout_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgNutrition, R.drawable.ic_client_profile_nutrition_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgTrainigProgram, R.drawable.ic_client_profile_training_program_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgCheckin, R.drawable.ic_client_profile_checkin_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgReports, R.drawable.ic_client_profile_reports_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgNotes, R.drawable.ic_client_profile_document_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgPurchase, R.drawable.ic_client_profile_purchase_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgMedical, R.drawable.ic_client_profile_medical_info_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgBasicInfo, R.drawable.ic_client_profile_basic_info_vector);
    }

    public void setAssign(boolean isChecked) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).activateDeactivateClient(dataBean.getId(), isChecked ? 1 : 0,dataBean.getTrainer_id())
                , getCompositeDisposable(), assignWorkout, this);
    }

    private void getClientProfile() {
        if (getArguments() != null) {
            dataBean = (GymClientListModel.DataBean) getArguments().get("data");
            Utils.setImage(mContext, mBinding.layoutClientProfile.imgClient, dataBean.getPhoto());
            mBinding.layoutClientProfile.name.setText(dataBean.getName());
            mBinding.layoutClientProfile.txtEmail.setText(dataBean.getEmail());
            mBinding.layoutClientProfile.toggle.setChecked((dataBean.getIs_active() == 1));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linActivity:
                startActivity(new Intent(getActivity(), ActivityOfClientActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linWorkout:
                startActivity(new Intent(getActivity(), ClientProfileWorkoutActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linNutritions:
                startActivity(new Intent(getActivity(), ClientProfileNutritionActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linTrainingProgram:
                startActivity(new Intent(getActivity(), ClientProfileTrainingProgramActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linSchedules:
                startActivity(new Intent(getActivity(), ClientProfileSchedulesActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linReports:
                startActivity(new Intent(getActivity(), ClientProfileReportActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linDocuments:
                startActivity(new Intent(getActivity(), ClientDocumentActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linPurchase:
                startActivity(new Intent(getActivity(), ClientProfilePurchaseActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linMedical:
                startActivity(new Intent(getActivity(), ClientProfileMedicalFormActivity.class).putExtra("data", dataBean.getId()));
                break;

            case R.id.linBasicInfo:
                startActivity(new Intent(getActivity(), TrainerClientBasicInfoActivity.class).putExtra("client_id", dataBean.getId()));
                break;

        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.linear, "active changed successfully", Snackbar.LENGTH_LONG);
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
