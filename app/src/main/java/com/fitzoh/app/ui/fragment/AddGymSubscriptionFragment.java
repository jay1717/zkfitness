package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentAddGymSubscriptionBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GymSubscriptionModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addPackage;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddGymSubscriptionFragment extends BaseFragment implements View.OnClickListener, SingleCallback {


    FragmentAddGymSubscriptionBinding mBinding;
    public static final int RECORD_REQUEST_CODE = 101;
    String userId, userAccessToken;
    private boolean isFromEdit = false;
    GymSubscriptionModel.DataBean packageListModel;

    public AddGymSubscriptionFragment() {
        // Required empty public constructor
    }

    public static AddGymSubscriptionFragment newInstance(Bundle bundle) {
        AddGymSubscriptionFragment fragment = new AddGymSubscriptionFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("isFromEdit")) {
            isFromEdit = getArguments().getBoolean("isFromEdit", false);
            packageListModel = (GymSubscriptionModel.DataBean) getArguments().getSerializable("data");

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, !isFromEdit ? "Add Gym Subscription" : "Edit Gym Subscription");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_add_gym_subscription, container, false);
        Utils.getShapeGradient(mActivity, mBinding.layoutAddGym.btnCreate);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        mBinding.layoutAddGym.btnCreate.setOnClickListener(this);
        mBinding.layoutAddGym.btnCreate.setText(!isFromEdit ? "Add Gym Subscription" : "Edit Gym Subscription");
        if (isFromEdit) {
            callEditTraining();
        }


    }

    private void callEditTraining() {
        mBinding.layoutAddGym.edtPackageName.setText(packageListModel.getName());
        mBinding.layoutAddGym.edtAboutTrainingProgram.setText(packageListModel.getAbout_subscriptions());
        mBinding.layoutAddGym.edtWeeks.setText(packageListModel.getNo_of_months());
        mBinding.layoutAddGym.edtPrice.setText(packageListModel.getPrice());
        mBinding.layoutAddGym.edtDiscountPrice.setText(packageListModel.getDiscount_price());

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create:
                if (Utils.isOnline(mActivity)) {
                    if (validation())
                        if (!isFromEdit)
                            createPackage();
                        else editPackage();
                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    private void editPackage() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                updateGymSubscription(packageListModel.getId(),
                        mBinding.layoutAddGym.edtPackageName.getText().toString(),
                        mBinding.layoutAddGym.edtWeeks.getText().toString(),
                        mBinding.layoutAddGym.edtAboutTrainingProgram.getText().toString(),
                        mBinding.layoutAddGym.edtPrice.getText().toString(),
                        mBinding.layoutAddGym.edtDiscountPrice.getText().toString()
                ), getCompositeDisposable(), addPackage, this);
    }

    private void createPackage() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                addGymSubscription(mBinding.layoutAddGym.edtPackageName.getText().toString(),
                        mBinding.layoutAddGym.edtWeeks.getText().toString(),
                        mBinding.layoutAddGym.edtAboutTrainingProgram.getText().toString(),
                        mBinding.layoutAddGym.edtPrice.getText().toString(),
                        mBinding.layoutAddGym.edtDiscountPrice.getText().toString()
                ), getCompositeDisposable(), addPackage, this);
    }

    private boolean validation() {

        if (mBinding.layoutAddGym.edtPackageName.getText().toString().length() <= 0) {
            showSnackBar(mBinding.mainLayout, "Please enter name", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.layoutAddGym.edtWeeks.getText().toString().length() <= 0) {
            showSnackBar(mBinding.mainLayout, "Please enter no of months", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.layoutAddGym.edtAboutTrainingProgram.getText().toString().isEmpty()) {
            showSnackBar(mBinding.mainLayout, "Please enter description", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.layoutAddGym.edtPrice.getText().toString().length() <= 0) {
            showSnackBar(mBinding.mainLayout, "Please enter price", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.layoutAddGym.edtDiscountPrice.getText().toString().length() <= 0) {
            showSnackBar(mBinding.mainLayout, "Please enter discount price", Snackbar.LENGTH_LONG);
            return false;
        }
        if(Integer.parseInt(mBinding.layoutAddGym.edtPrice.getText().toString()) < Integer.parseInt(mBinding.layoutAddGym.edtDiscountPrice.getText().toString())){
            mBinding.layoutAddGym.txtErrorDiscount.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.layoutAddGym.txtErrorDiscount, "Discount price can not be more than actual price");
            return  false;
        }
        else {
            mBinding.layoutAddGym.txtErrorDiscount.setText("");
            mBinding.layoutAddGym.txtErrorDiscount.setVisibility(View.GONE);
        }
        return true;
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case addPackage:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getActivity().finish();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
