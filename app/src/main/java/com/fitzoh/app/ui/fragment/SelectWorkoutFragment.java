package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SelectWorkoutListAdapter;
import com.fitzoh.app.databinding.FragmentSelectWorkoutBinding;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditWorkoutActivity;
import com.fitzoh.app.ui.dialog.AddWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectWorkoutFragment extends BaseFragment implements SingleCallback {


    FragmentSelectWorkoutBinding mBinding;
    String userId, userAccessToken;
    SelectWorkoutListAdapter recyclerViewAdapter;

    public SelectWorkoutFragment() {
        // Required empty public constructor
    }

    public static SelectWorkoutFragment newInstance(Bundle bundle) {
        SelectWorkoutFragment fragment = new SelectWorkoutFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_select_workout, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.select_workout));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
//        Utils.getShapeGradient(mActivity, mBinding.layoutWorkout.btnSelect);
        Utils.setAddFabBackground(mActivity, mBinding.layoutWorkout.fab);
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        mBinding.layoutWorkout.fab.setOnClickListener(view -> {
            AddWorkoutDialog dialog = new AddWorkoutDialog();
            dialog.setTargetFragment(this, 0);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.layoutWorkout.recyclerView.setLayoutManager(recyclerLayoutManager);

        //RecyclerView adapater
        recyclerViewAdapter = new
                SelectWorkoutListAdapter(getActivity(), new ArrayList<>());
        mBinding.layoutWorkout.recyclerView.setAdapter(recyclerViewAdapter);
        mBinding.layoutWorkout.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (recyclerViewAdapter != null)
                    recyclerViewAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        callWorkoutList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            mActivity.startActivityForResult(new Intent(getActivity(), EditWorkoutActivity.class).putExtra("data", data.getSerializableExtra("data")), 0);
        }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.layoutWorkout.edtSearch.setText("");
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkoutListDialog("alphabetical")
                    , getCompositeDisposable(), workoutList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkOutListModel workOutListModel = (WorkOutListModel) o;
                if (workOutListModel != null && workOutListModel.getStatus() == AppConstants.SUCCESS && workOutListModel.getData() != null && workOutListModel.getData().size() > 0) {
                    mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                    mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                    recyclerViewAdapter = new
                            SelectWorkoutListAdapter(getActivity(), workOutListModel.getData());
                    mBinding.layoutWorkout.recyclerView.setAdapter(recyclerViewAdapter);
                } else {
                    mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
        mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
        mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
    }
}
