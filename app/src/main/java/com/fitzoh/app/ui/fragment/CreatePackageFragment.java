package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.system.ErrnoException;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentCreatePackageBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.PackageListModel;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addPackage;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatePackageFragment extends BaseFragment implements View.OnClickListener, SingleCallback, AdapterView.OnItemSelectedListener {


    FragmentCreatePackageBinding mBinding;
    public static final int RECORD_REQUEST_CODE = 101;
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImage;
    File fileVideo;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    Activity activityMain;
    private static final int REQUEST_VIDEO = 0x01;
    public static final int SELECT_CATEGORY = 100;
    String videoUri;
    HashMap<Integer, String> spinnerMap;
    String userId, userAccessToken;
    List<TrainingProgramList.DataBean> trainingProgramListData = new ArrayList<>();
    int trainingNameId;
    private boolean isFromEdit = false;
    MediaPlayer mediaPlayer;

    public CreatePackageFragment() {
        // Required empty public constructor
    }

    public static CreatePackageFragment newInstance() {
        CreatePackageFragment fragment = new CreatePackageFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("isFromEdit")) {
            isFromEdit = getArguments().getBoolean("isFromEdit", false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_create_package, container, false);
        Utils.getShapeGradient(mActivity, mBinding.createPackage.btnCreate);
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.createPackage.btnDateDropdown});
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setClickEvent();
        prepareLayout();
        makeRequest();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
//        mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.imgBack.setOnClickListener(v -> mActivity.onBackPressed());
        mBinding.toolbar.tvTitle.setText(!isFromEdit ? getString(R.string.create_package) : "Update Package");
        activityMain = this.getActivity();
        callTrainingList();
    }

    private void callEditTraining() {
        PackageListModel.DataBean packageListModel = (PackageListModel.DataBean) getArguments().getSerializable("data");
        mBinding.createPackage.edtPackageName.setText(packageListModel.getPackage_name());
        mBinding.createPackage.edtAboutTrainingProgram.setText(packageListModel.getDesc());
        mBinding.createPackage.edtWeeks.setText(packageListModel.getWeeks());
        mBinding.createPackage.edtPrice.setText(packageListModel.getPrice());
        mBinding.createPackage.edtDiscountPrice.setText(packageListModel.getDiscounted_price());
        mBinding.createPackage.spinnerTrainingName.setSelection(((ArrayAdapter<String>) mBinding.createPackage.spinnerTrainingName.getAdapter()).getPosition(packageListModel.getTraining_program_name()));

        if (packageListModel.getMedia_type().equalsIgnoreCase("Youtube")) {
            mBinding.createPackage.imgYoutube.performClick();
            mBinding.createPackage.edtVideoLink.setText(packageListModel.getYoutube_url());
        } else if (packageListModel.getMedia_type().equalsIgnoreCase("Image")) {
            mBinding.createPackage.imgUpload.setVisibility(View.VISIBLE);
            if (packageListModel.getMedia() != null && !packageListModel.getMedia().equalsIgnoreCase("")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder);
                Glide.with(this)
                        .load(packageListModel.getMedia())
                        .apply(options)
                        .into(mBinding.createPackage.imgUpload);
            }
        } else if (packageListModel.getMedia_type().equalsIgnoreCase("Video")) {
            mBinding.createPackage.imgUpload.setVisibility(View.VISIBLE);
            mBinding.createPackage.imgUpload.setImageResource(R.drawable.placeholder);

        }
    }

    private void callTrainingList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainingList()
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    private void setClickEvent() {
        mBinding.createPackage.imgGallary.setOnClickListener(this);
        mBinding.createPackage.imgYoutube.setOnClickListener(this);
        mBinding.createPackage.spinnerTrainingName.setOnItemSelectedListener(this);

        mBinding.createPackage.imgGallary.setOnClickListener(v -> {
            mBinding.createPackage.edtVideoLink.setText("");
            mBinding.createPackage.imgUpload.setVisibility(View.VISIBLE);
            mBinding.createPackage.edtVideoLink.setVisibility(View.GONE);
            showPopupMenu(mBinding.createPackage.imgGallary);
        });
        mBinding.createPackage.btnCreate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_gallary:
                mBinding.createPackage.edtVideoLink.setText("");
                mBinding.createPackage.imgUpload.setVisibility(View.VISIBLE);
                mBinding.createPackage.edtVideoLink.setVisibility(View.GONE);
                scrollToPosition(mBinding.createPackage.imgUpload);
                break;

            case R.id.img_youtube:
                fileImage = null;
                fileVideo = null;
                mBinding.createPackage.imgUpload.setImageResource(R.drawable.placeholder_workout_large);
                scrollToPosition(mBinding.createPackage.edtVideoLink);
                mBinding.createPackage.imgUpload.setVisibility(View.GONE);
                mBinding.createPackage.edtVideoLink.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_create:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                      /*  if (!isEdit)
                            addTraining();
                        else
                            editTraining();*/
                        if (!isFromEdit)
                            createPackage();
                        else editPackage();
                    }
                } else {
                    showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    private void scrollToPosition(View view) {
        mBinding.createPackage.scrollView.post(() -> mBinding.createPackage.scrollView.scrollTo(0, view.getBottom()));
    }

    private void editPackage() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        PackageListModel.DataBean packageListModel = (PackageListModel.DataBean) getArguments().getSerializable("data");

        RequestBody package_id = RequestBody.create(MediaType.parse("text/plain"), "" + packageListModel.getId());
        RequestBody package_name = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtPackageName.getText().toString());
        RequestBody training_program_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(trainingNameId));
        RequestBody no_of_weeks = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtWeeks.getText().toString());
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtAboutTrainingProgram.getText().toString());
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtPrice.getText().toString());
        RequestBody discounted_price = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtDiscountPrice.getText().toString());
        RequestBody mediaType;
        RequestBody youTubeLink = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtVideoLink.getText().toString());
        MultipartBody.Part img = null;
        if (fileImage != null && mImageUri != null) {
            byte[] uploadBytes = Utils.loadImageFromStorage(mImageUri.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
            // RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            mediaType = RequestBody.create(MediaType.parse("text/plain"), "Image");
            img = MultipartBody.Part.createFormData("media", fileImage.getName(), reqFile);
        } else if (fileVideo != null) {
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), fileVideo);
            mediaType = RequestBody.create(MediaType.parse("text/plain"), "Video");
            img = MultipartBody.Part.createFormData("media", fileVideo.getName(), videoBody);
        } else if (mBinding.createPackage.edtVideoLink.getVisibility() == View.VISIBLE) {
            mediaType = RequestBody.create(MediaType.parse("text/plain"), "Youtube");
            img = null;
        } else {
            mediaType = RequestBody.create(MediaType.parse("text/plain"), "");
            img = null;
        }
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                        editPackage(package_id, package_name, training_program_id, no_of_weeks, desc, price, discounted_price, mediaType, img, youTubeLink),
                getCompositeDisposable(), addPackage, this);
    }

    private void createPackage() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody package_name = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtPackageName.getText().toString());
        RequestBody training_program_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(trainingNameId));
        RequestBody no_of_weeks = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtWeeks.getText().toString());
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtAboutTrainingProgram.getText().toString());
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtPrice.getText().toString());
        RequestBody discounted_price = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtDiscountPrice.getText().toString());
        RequestBody mediaType;
        RequestBody youTubeLink = RequestBody.create(MediaType.parse("text/plain"), mBinding.createPackage.edtVideoLink.getText().toString());


        if (fileImage != null && mImageUri != null) {
            byte[] uploadBytes = Utils.loadImageFromStorage(mImageUri.getPath());
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
            // RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);

            mediaType = RequestBody.create(MediaType.parse("text/plain"), "Image");
            MultipartBody.Part img = MultipartBody.Part.createFormData("media", fileImage.getName(), reqFile);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                    addPackageData(package_name, training_program_id, no_of_weeks, desc, price, discounted_price, mediaType, img), getCompositeDisposable(), addPackage, this);

        } else if (fileVideo != null) {
            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), fileVideo);
            mediaType = RequestBody.create(MediaType.parse("text/plain"), "Video");
            MultipartBody.Part vFile = MultipartBody.Part.createFormData("media", fileVideo.getName(), videoBody);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                    addPackageData(package_name, training_program_id, no_of_weeks, desc, price, discounted_price, mediaType, vFile), getCompositeDisposable(), addPackage, this);

        } else {
            mediaType = RequestBody.create(MediaType.parse("text/plain"), "Youtube");
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).
                    addPackageDataWithoutImg(package_name, training_program_id, no_of_weeks, desc, price, discounted_price, youTubeLink, mediaType), getCompositeDisposable(), addPackage, this);
        }


    }

    private boolean validation() {
       /* if (fileImage == null) {
            showSnackBar(mBinding.getRoot(), "Please select image", Snackbar.LENGTH_LONG);
            return false;
        } else */
        if (mBinding.createPackage.edtPackageName.getText().toString().length() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please enter package name", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.createPackage.edtWeeks.getText().toString().length() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please enter no of weeks", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.createPackage.edtAboutTrainingProgram.getText().toString().isEmpty()) {
            showSnackBar(mBinding.getRoot(), "Please enter description", Snackbar.LENGTH_LONG);
            return false;
        } else if (mBinding.createPackage.edtPrice.getText().toString().length() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please enter price", Snackbar.LENGTH_LONG);
            return false;
        }
        else if (mBinding.createPackage.edtDiscountPrice.getText().toString().length() > 0) {
            if (Double.valueOf(mBinding.createPackage.edtPrice.getText().toString()) < Double.valueOf(mBinding.createPackage.edtDiscountPrice.getText().toString())) {
                showSnackBar(mBinding.getRoot(), "Discount price can not be more than actual price", Snackbar.LENGTH_LONG);
                return false;
            }
        }

        return true;
    }

    private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(getActivity(), view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(R.menu.menu_gallery_option, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener());
        popup.show();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case addPackage:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                /*if (commonApiResponse.getStatus() == AppConstants.SUCCESS)*/
            {
                showSnackBar(mBinding.getRoot(), commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                getActivity().finish();
            }/* else*/
            {
                showSnackBar(mBinding.getRoot(), commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
            }
            break;

            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainingProgramListData = new ArrayList<>();
                TrainingProgramList trainingProgramList = (TrainingProgramList) o;
              /*  List<String> list = new ArrayList<>();
                list.add("Name of Training Program");*/
                if (trainingProgramList != null && trainingProgramList.getStatus() == AppConstants.SUCCESS && trainingProgramList.getData() != null && trainingProgramList.getData().size() > 0) {
                    this.trainingProgramListData = trainingProgramList.getData();
                    String[] spinnerArray = new String[trainingProgramList.getData().size() + 1];
                    spinnerArray[0] = "Name of Training Program";
                    spinnerMap = new HashMap<Integer, String>();
                    for (int i = 0; i < trainingProgramList.getData().size(); i++) {
                        spinnerMap.put(i, String.valueOf(trainingProgramList.getData().get(i).getId()));
                        spinnerArray[i + 1] = trainingProgramList.getData().get(i).getTitle();
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_search_item, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_search_item);
                    mBinding.createPackage.spinnerTrainingName.setAdapter(adapter);
                    if (isFromEdit)
                        callEditTraining();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(mActivity)
                            .setTitle("Create Package Alert")
                            .setMessage("You need to create training program before creating package")
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                                dialog.dismiss();
                                mActivity.onBackPressed();
                            })
                            .show();
                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position == 0) {
            trainingNameId = 0;
        }
        if (position != 0) {
//            String item = parent.getItemAtPosition(position).toString();
            String name = mBinding.createPackage.spinnerTrainingName.getSelectedItem().toString();

//            fitnessCenterId = Integer.parseInt(spinnerMap.get(mBinding.home.spinnerFitnessCenter.getSelectedItemPosition()));
            trainingNameId = Integer.parseInt(spinnerMap.get(mBinding.createPackage.spinnerTrainingName.getSelectedItemPosition() - 1));
            mBinding.createPackage.edtWeeks.setText("" + trainingProgramListData.get(mBinding.createPackage.spinnerTrainingName.getSelectedItemPosition() - 1).getWeeks());
//            Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();
        } else {
            mBinding.createPackage.edtWeeks.setText("");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        MenuActionItemClickListener() {

        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_photo:
                    //onAddClient.edit(workoutList.get(position));
                    imgPick();
                    break;
                case R.id.action_video:
                    // onAddClient.rename(position);
                    showDialogForVideo();
                    break;
            }
            return false;
        }
    }

    protected void makeRequest() {
        requestAppPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, RECORD_REQUEST_CODE, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        getActivity().startActivityForResult(getPickImageChooserIntent(), 200);
    }

    private void showDialogForVideo() {

        new AlertDialog.Builder(mActivity)
                .setTitle("Select video")
                .setMessage("Please choose video selection type")
                .setPositiveButton("CAMERA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        openVideoCapture();
                    }
                })
                .setNegativeButton("GALLERY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickFromGallery();
                    }
                })
                .show();
    }

    private void openVideoCapture() {
        Intent videoCapture = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(videoCapture, REQUEST_VIDEO);
    }

    private void pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale), REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(intent, REQUEST_VIDEO);
            /*Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            intent.setTypeAndNormalize("video/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO);*/
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setTitle(getString(R.string.permission_title_rationale));
            builder.setMessage(rationale);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(mActivity, new String[]{permission}, requestCode);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.show();
        } else {
            ActivityCompat.requestPermissions(mActivity, new String[]{permission}, requestCode);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {

                    // mBinding.createprofile.imgUser.setImageURI(mImageUri);
                    mBinding.createPackage.imgUpload.setImageURI(mImageUri);
                    mBinding.createPackage.imgUpload.setVisibility(View.VISIBLE);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();


                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }

        if (requestCode == REQUEST_VIDEO && resultCode == RESULT_OK) {
            Uri selectedUri = data.getData();
            String path = "";
            if (selectedUri != null) {
                videoUri = String.valueOf(selectedUri);
                String[] projection = {MediaStore.Video.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(selectedUri, projection, null, null, null);
                //Cursor cursor = getActivity().getContentResolver().query(selectedUri, null, null, null, null);
                if (cursor == null) {
                    path = selectedUri.getPath();
                } else {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                    path = cursor.getString(idx);
                }
                if (path != null) {
                    fileVideo = new File(path);
                }
            } else {
                Toast.makeText(mActivity, "Cannot retrieve selected video", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void loadVideo() {
        try {
            mediaPlayer.setDataSource(videoUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

}
