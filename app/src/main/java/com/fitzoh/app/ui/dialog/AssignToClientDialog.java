package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogAssignToClientBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@SuppressLint("ValidFragment")
public class AssignToClientDialog extends AppCompatDialogFragment {

    View view;
    DialogAssignToClientBinding mBinding;
    public SessionManager session;
    private AddDietDialog.DialogClickListener dialogClickListener;
    String userId, userAccessToken;
    private onSave onSave;
    int clientId;
    int isAssign, isAllow;
    String date;

    public AssignToClientDialog(int clientId, int isAssign, onSave onSave) {
        this.clientId = clientId;
        this.isAssign = isAssign;
        this.onSave = onSave;
    }

    @Nullable
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_assign_to_client, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            Utils.setSwitchTint(getActivity(), mBinding.toggle);
            view = mBinding.getRoot();
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setView(view);

        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();

        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }

    private void prepareLayout() {

        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    isAllow = 1;
                else
                    isAllow = 0;
            }
        });
        mBinding.edtStartDate.setKeyListener(null);
        mBinding.edtStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });
        mBinding.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    // addDietPlan();
                    onSave.save(clientId, date, isAllow, isAssign);
                    dismiss();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
    }

    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                (view, year, monthOfYear, dayOfMonth) -> {
                    c.set(Calendar.YEAR, year);
                    c.set(Calendar.MONTH, monthOfYear);
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                    mBinding.edtStartDate.setText(dateFormat.format(c.getTime()));

                    String inputDate=dateFormat.format(c.getTime());
                    SimpleDateFormat spf=new SimpleDateFormat("dd/MM/yyyy");
                    Date newDate= null;
                    try {
                        newDate = spf.parse(inputDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    spf= new SimpleDateFormat("yyyy-MM-dd");
                    date = spf.format(newDate);
                    System.out.println(date);

                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    private boolean validation() {
        if (TextUtils.isEmpty(mBinding.edtStartDate.getText().toString())) {
            showSnackBar("Please select date.");
            return false;
        }
        return true;
    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    public interface onSave {
        void save(int id, String date, int isAllowed, int isAssign);
    }


}
