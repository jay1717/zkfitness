package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.PurchaseHistoryOwnAdapter;
import com.fitzoh.app.databinding.FragmentPurchaseHistoryTypeBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.PurchaseHistoryTrainerModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getPackageList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPurchaseHistoryOwn extends BaseFragment implements SingleCallback, FragmentLifeCycle, SwipeRefreshLayout.OnRefreshListener {

    FragmentPurchaseHistoryTypeBinding mBinding;
    String userId, userAccessToken;

    public FragmentPurchaseHistoryOwn() {
        // Required empty public constructor
    }

    public static FragmentPurchaseHistoryOwn newInstance() {
        FragmentPurchaseHistoryOwn fragment = new FragmentPurchaseHistoryOwn();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setupToolBarWithMenu(mBinding.toolbar.toolbar, "Purchase");
    }

    @Override
    public void onResume() {
        super.onResume();
        callPackageList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_purchase_history_type, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        return mBinding.getRoot();
    }

    private void callPackageList() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchasePlanHistoryTrainer()
                    , getCompositeDisposable(), getPackageList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case getPackageList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                PurchaseHistoryTrainerModel model = (PurchaseHistoryTrainerModel) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null) {
                    setCurrentPlanDetail(model.getData());
                } else {
                    showSnackBar(mBinding.mainLayout, model.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
        }
    }

    private void setCurrentPlanDetail(PurchaseHistoryTrainerModel.DataBean model) {

        if (model.getOwnPlan() != null) {
            List<PurchaseHistoryTrainerModel.DataBean.OwnPlanBean> ownPlan = model.getOwnPlan();
            if (ownPlan != null && ownPlan.size() > 0) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.imgNoData.setVisibility(View.GONE);
                mBinding.recyclerView.setAdapter(new PurchaseHistoryOwnAdapter(ownPlan, mActivity));
            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
            }
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        callPackageList();
    }

    @Override
    public void onRefresh() {
        callPackageList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
