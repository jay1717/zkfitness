package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ActivityLoginFeildBinding;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.model.SocialLoginModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.login;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.resend;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class LoginFeildActivity extends BaseActivity implements View.OnClickListener, SingleCallback {
    ActivityLoginFeildBinding mBinding;
    View view;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 1001;
    private static final String EMAIL = "email";
    CallbackManager callbackManager;
    String email;
    String firstName;
    String lastName;
    String name;
    String providerId;
    String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (view == null) {

            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_feild);
            view = mBinding.getRoot();
            FacebookSdk.sdkInitialize(this);

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
//            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "");
            prepareLayout();
            Utils.getShapeGradient(this, mBinding.login.btnLogin);
            Utils.setTextViewStartImage(this, mBinding.login.edtEmail, R.drawable.ic_email, true);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        }
        callbackManager = CallbackManager.Factory.create();
        mBinding.login.loginButtonFacebook.setReadPermissions("email");
        // mBinding.login.loginButtonFacebook.setFragment(getApplicationContext());
        mBinding.login.loginButtonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);
//                Toast.makeText(LoginFeildActivity.this, "Success", Toast.LENGTH_SHORT).show();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        // int id = 0;
                        Bundle bFacebookData = getFacebookData(object);
                        try {
                            firstName = object.getString("first_name");
                            lastName = object.getString("last_name");
                            name = object.getString("first_name") + "" + object.getString("last_name");
                            email = object.getString("email");
                            providerId = object.getString("id");
                            provider = "0";
                            if (Utils.isOnline(getApplicationContext())) {
                                callAPI();
                            } else {
                                showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG, LoginFeildActivity.this);
                            }
                            /*Intent intent = new Intent(LoginFeildActivity.this, NavigationMainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
//                Toast.makeText(LoginFeildActivity.this, "cancel", Toast.LENGTH_SHORT).show();
                Log.i("LoginActivity", "cancel");
            }


            @Override
            public void onError(FacebookException exception) {
//                Toast.makeText(LoginFeildActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("LoginActivity", exception.toString());
            }
        });

        calculateHashKey("com.fitzoh.app");
        setClickEvents();
    }

    private void callLoginAPI() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new UnauthorizedNetworkInterceptor(this)).create(WebserviceBuilder.class).login(mBinding.login.edtEmail.getText().toString().trim())
                , getCompositeDisposable(), login, this);
    }

    private void callAPI() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
//        Toast.makeText(this, "API call", Toast.LENGTH_SHORT).show();
        ObserverUtil.subscribeToSingle(ApiClient.getClient(LoginFeildActivity.this, new UnauthorizedNetworkInterceptor(this)).create(WebserviceBuilder.class).socialSignIn(email, "", provider, providerId)
                , getCompositeDisposable(), single, this);
    }

    private void calculateHashKey(String yourPackageName) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    yourPackageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void prepareLayout() {
        // Animate Layouts
//        mBinding.touchzenMedia.tvRawvanaVersion.setText(mContext.getResources().getString(R.string.rawvana_version, BuildConfig.VERSION_NAME));
        String strColor = String.format("#%06X", 0xFFFFFF & res.getColor(R.color.colorPrimary));
        String text = "<font color=#000000>Don't have an account?</font> <font color=" + strColor + "> Sign up </font>";
        mBinding.login.txtRegister.setText(Html.fromHtml(text));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // ...
                        }
                    });
        }

          /*  name=account.getDisplayName();
            email=account.getEmail();
            providerId=account.getId();
            provider="1";
            callAPI();
            Log.e("GoogleLoging already", "" + account.getEmail() + " ," + account.getId() + " ," + account.getIdToken());*/

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            //  updateUI(account);
            name = account.getDisplayName();
            email = account.getEmail();
            providerId = account.getId();
            provider = "1";
            if (Utils.isOnline(getApplicationContext())) {
                callAPI();
            } else {
                showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG, LoginFeildActivity.this);
            }
//            switchToRegister();
            // Toast.makeText(getActivity(), "Signed in Successfully using", Toast.LENGTH_SHORT).show();
            Log.e("GoogleLoging using", "" + account.getEmail() + " ," + account.getId() + " ," + account.getIdToken());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("ERROR", "signInResult:failed code=" + e.getStatusCode());
            //  updateUI(null);
            //Toast.makeText(getActivity(), "Fail to login ", Toast.LENGTH_SHORT).show();

        }
    }

    private void switchToRegister(int id) {
        Intent intent = new Intent(LoginFeildActivity.this, RegisterActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("providerId", providerId);
        bundle.putString("name", name);
        bundle.putString("email", email);
        bundle.putInt("id", id);
        bundle.putInt("isSocialMedia", 1);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public boolean validation() {
        boolean value = true;
        if (mBinding.login.edtEmail.getText().toString().length() == 0) {
            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.empty_email));
            value = false;
        } else if (mBinding.login.edtEmail.getText().toString().matches("[0-9]+")) {
            if (mBinding.login.edtEmail.getText().toString().length() != 10) {
                setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_phone));
                value = false;
            } else {
                mBinding.login.txtErrorUsername.setText("");
                mBinding.login.txtErrorUsername.setVisibility(View.GONE);
            }
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.login.edtEmail.getText().toString()).matches()) {
            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_email));
            value = false;
        }
        return value;
    }

    public void setEdittextError(TextView textView, String string) {
        Animation animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        textView.startAnimation(animationFadeIn);
        textView.setTextColor(Color.RED);
        textView.setText(string);
        textView.setVisibility(View.VISIBLE);
        textView.requestFocus();
    }

    private void setClickEvents() {
        mBinding.login.btnLogin.setOnClickListener(this);
        mBinding.login.txtRegister.setOnClickListener(this);
        mBinding.login.btnFacebook.setOnClickListener(this);
        mBinding.login.btnGoogle.setOnClickListener(this);
        mBinding.login.edtEmail.addTextChangedListener(new MyTaskWatcher(mBinding.login.edtEmail));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (Utils.isOnline(getApplicationContext())) {
                    if (validation()) {
                        callLoginAPI();
                    }
                } else {
                    showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG, this);
                }


                /*if (Utils.isOnline(this)) {
                    if (validation()) {
                        showProgressBar().show();
                        callAPI();
                        if (mBinding.login.edtEmail.getText().toString().matches("[0-9]+")) {
                            Toast.makeText(LoginFeildActivity.this, "With Phone", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(LoginFeildActivity.this, "With Email", Toast.LENGTH_SHORT).show();
                        }
//                        startActivity(new Intent(getActivity(), NavigationMainActivity.class));

                        startActivity(new Intent(LoginFeildActivity.this, VarificationActivity.class));

                    }
                } else {
                    Toast.makeText(LoginFeildActivity.this, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
                }*/
                break;

            case R.id.txtRegister:
                startActivity(new Intent(LoginFeildActivity.this, RegisterActivity.class));
                /* FragmentRegister fragmentRegister = new FragmentRegister();
                bundle = new Bundle();
                fragmentRegister.setArguments(bundle);
                ((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, fragmentRegister, true, true); */
                break;

            case R.id.btnFacebook:
                mBinding.login.loginButtonFacebook.performClick();
                break;

            case R.id.btnGoogle:
                signIn();
                break;
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SocialLoginModel socialLoginModel = (SocialLoginModel) o;
                if (socialLoginModel != null && socialLoginModel.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.getRoot(), socialLoginModel.getMessage(), Snackbar.LENGTH_LONG, this);
                    if (checkColor(socialLoginModel.getData().getPrimary_color_code()) && checkColor(socialLoginModel.getData().getSecondary_color_code())) {
                        List<String> primaryColor = Arrays.asList(socialLoginModel.getData().getPrimary_color_code().split(","));
                        List<String> secondary = Arrays.asList(socialLoginModel.getData().getSecondary_color_code().split(","));
                        SharedPreferences pref = getSharedPreferences("colors", 0);
                        SharedPreferences.Editor editor = pref.edit();
                        double redP = Double.parseDouble(primaryColor.get(0));
                        double greenP = Double.parseDouble(primaryColor.get(1));
                        double blueP = Double.parseDouble(primaryColor.get(2));
                        double redS = Double.parseDouble(secondary.get(0));
                        double greenS = Double.parseDouble(secondary.get(1));
                        double blueS = Double.parseDouble(secondary.get(2));
                        editor.putInt("redPrimary", (int) redP);
                        editor.putInt("bluePrimary", (int) blueP);
                        editor.putInt("greenPrimary", (int) greenP);
                        editor.putInt("redAccent", (int) redS);
                        editor.putInt("blueAccent", (int) blueS);
                        editor.putInt("greenAccent", (int) greenS);
                        editor.apply();
                    }
                    if (socialLoginModel.getData().isIsAlreadyExist()) {
                        session.saveAuthorizedUser(this, socialLoginModel.getData());
                        if (socialLoginModel.getData().isIs_verified() == 1) {
                            Intent intent = null;
                            //  session.setLogin(true);
                            //   session.saveAuthorizedUser(this, socialLoginModel.getData());
                            if (socialLoginModel.getData().getIsProfileCompleted() == 0) {
                                intent = new Intent(this, CreateUserActivity.class);
                                intent.putExtra("rollId", socialLoginModel.getData().getRoleId());
                            } else if (socialLoginModel.getData().getIsMedicalFormCompleted() == 0 && socialLoginModel.getData().getRoleId().equals("1")) {
                                intent = new Intent(this, MedicalFormActivity.class);
                            } else if (socialLoginModel.getData().getRoleId().equals("1")) {
                                session.setRollId(1);
                                session.setLogin(true);
                                intent = new Intent(LoginFeildActivity.this, NavigationClientMainActivity.class);
                            } else if (socialLoginModel.getData().isIs_subscription() == 0 && !socialLoginModel.getData().getRoleId().equals("1")) {
                                intent = new Intent(LoginFeildActivity.this, SubscriptionActivity.class);
                            } else if (socialLoginModel.getData().getRoleId().equals("2")) {
                                session.setLogin(true);
                                session.setRollId(2);
                                intent = new Intent(LoginFeildActivity.this, NavigationMainActivity.class);
                            } else if (socialLoginModel.getData().getRoleId().equals("3")) {
                                session.setLogin(true);
                                session.setRollId(3);
                                intent = new Intent(LoginFeildActivity.this, NavigationMainActivity.class);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(LoginFeildActivity.this, VarificationActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("id", socialLoginModel.getData().getId());
                            intent.putExtra("email", socialLoginModel.getData().getEmail());
                            intent.putExtra("isRegister", "true");
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        switchToRegister(socialLoginModel.getData().getId());
                    }
                    // callSocialLoginAPI();
                }
                break;
            case resend:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                break;

            case login:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                RegisterModel registerModel = (RegisterModel) o;
                if (registerModel != null && registerModel.getStatus() == AppConstants.SUCCESS) {
                    Intent intent = new Intent(LoginFeildActivity.this, VarificationActivity.class);
                    intent.putExtra("id", registerModel.getData().getId());
                    intent.putExtra("email", mBinding.login.edtEmail.getText().toString());
                    intent.putExtra("isRegister", "false");
                    startActivity(intent);
                    // ((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, FragmentVerificationOTP.newInstance(registerModel.getData().getId(), mBinding.login.edtEmail.getText().toString()), true, true);
                } else {
                    showSnackBar(mBinding.getRoot(), registerModel.getMessage(), Snackbar.LENGTH_LONG, this);
                }
                break;
        }
    }

    private boolean checkColor(String color_code) {
        if (color_code == null || color_code.equalsIgnoreCase("")) {
            return false;
        } else return Arrays.asList(color_code.split(",")).size() == 3;

    }

    private void callSocialLoginAPI() {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(LoginFeildActivity.this, new UnauthorizedNetworkInterceptor(this)).create(WebserviceBuilder.class).socialLogin(email, "", providerId)
                , getCompositeDisposable(), resend, this);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.getRoot(), throwable.getMessage() != null ? throwable.getMessage() : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG, this);
    }

    private class MyTaskWatcher implements TextWatcher {
        private View view;

        MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edtEmail:
                    if (mBinding.login.edtEmail.hasFocus()) {
                        if (mBinding.login.edtEmail.getText().toString().length() == 0) {
                        /*    setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.empty_username));
                        } else if (mBinding.login.edtEmail.getText().toString().matches("[0-9]+")) {
                            if (mBinding.login.edtEmail.getText().toString().length() != 10) {
                                setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_phone));
                            } else {
                                mBinding.login.txtErrorUsername.setText("");
                                mBinding.login.txtErrorUsername.setVisibility(View.GONE);
                            }
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.login.edtEmail.getText().toString()).matches()) {
                            setEdittextError(mBinding.login.txtErrorUsername, getString(R.string.invalid_email));*/
                        } else {
                            mBinding.login.txtErrorUsername.setText("");
                            mBinding.login.txtErrorUsername.setVisibility(View.GONE);
                        }
                    }
                    break;

            }
        }

    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = null;
            try {
                id = object.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                bundle.putString("profile_pic", profile_pic.toString());
                Log.i("profile_pic", profile_pic + "");

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));


            return bundle;


        } catch (JSONException e) {
            //  Log.d(TAG,"Error parsing JSON");
        }
        return null;
    }
}
