package com.fitzoh.app.ui.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;

import com.fitzoh.app.adapter.ProfilePackagesListAdapter;
import com.fitzoh.app.databinding.FragmentProfilePackageListBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.ProfilePackageModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreatePackageActivity;
import com.fitzoh.app.ui.activity.PackageDetailActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProfilePackageListFragment extends BaseFragment implements SingleCallback, ProfilePackagesListAdapter.onDataPass, SwipeRefreshLayout.OnRefreshListener {

    FragmentProfilePackageListBinding mBinding;
    String userId, userAccessToken;
    int trainerId = 0;
    List<ProfilePackageModel.DataBean> dataBeanList;
    ProfilePackagesListAdapter packagesListAdapter;

    public ProfilePackageListFragment() {
        // Required empty public constructor
    }

    public static ProfilePackageListFragment newInstance(int trainerId) {
        ProfilePackageListFragment fragment = new ProfilePackageListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("data", trainerId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trainerId = getArguments().getInt("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_package_list, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutPackage.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutPackage.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
       /* if(trainerId!=0)
            callPackageList();*/
//        callPackageList();
//        setHasOptionsMenu(true);
        return mBinding.getRoot();
    }

    private void startCreatePackageActivity() {
        Intent intent = new Intent(mActivity, CreatePackageActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(trainerId!=0)
            callPackageList();
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        mActivity.invalidateOptionsMenu();
    }
*/
    private void callPackageList() {
        if (Utils.isOnline(getActivity())) {
            if (getArguments() != null && getArguments().containsKey("data")) {
                trainerId = getArguments().getInt("data");
            }mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getPackageLst(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getPackageList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case getPackageList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                dataBeanList = new ArrayList<>();
                ProfilePackageModel packageListModel = (ProfilePackageModel) o;
                if (packageListModel != null) {
                    dataBeanList.addAll(packageListModel.getData());
                    // workoutListAdapter.notifyDataSetChanged();
                    if (packageListModel.getData().size() == 0) {
                        mBinding.layoutPackage.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.layoutPackage.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.layoutPackage.recyclerView.setVisibility(View.VISIBLE);
                        mBinding.layoutPackage.imgNoData.setVisibility(View.GONE);
                        packagesListAdapter = new ProfilePackagesListAdapter(dataBeanList, getActivity(), this);
                        mBinding.layoutPackage.recyclerView.setAdapter(packagesListAdapter);
                    }
                } else
                    showSnackBar(mBinding.linear, packageListModel.getMessage(), Snackbar.LENGTH_LONG);

                break;

            case deletePackage:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deletePackage = (CommonApiResponse) o;
                if (deletePackage.getStatus() == AppConstants.SUCCESS)
                    callPackageList();
                else
                    showSnackBar(mBinding.linear, deletePackage.getMessage(), Snackbar.LENGTH_LONG);
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void pass(ProfilePackageModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), PackageDetailActivity.class).putExtra("dataBeanClass", dataBean));
    }

    @Override
    public void onRefresh() {
        callPackageList();
        mBinding.layoutPackage.swipeContainer.setRefreshing(false);
    }
}
