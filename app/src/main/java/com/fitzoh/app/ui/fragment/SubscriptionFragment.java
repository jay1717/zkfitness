package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.listners.AlLoginHandler;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SubscriptionListAdapter;
import com.fitzoh.app.databinding.FragmentSubscriptionBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.SubscriptionTypeListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.BankDetailActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getSubscriptionOptionList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getTrainerProfile;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class SubscriptionFragment extends BaseFragment implements SingleCallback, PaymentResultListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    FragmentSubscriptionBinding mBinding;
    List<SubscriptionTypeListModel.DataBean> subscriptionData;
    String userId, userAccessToken;
    SubscriptionListAdapter subscriptionListAdapter;
    private boolean isFromRegister = true;
    private boolean isDisCountinue;

    public SubscriptionFragment() {
        // Required empty public constructor
    }


    public static SubscriptionFragment newInstance() {
        SubscriptionFragment fragment = new SubscriptionFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER", false);
        }
        if (getArguments() != null && getArguments().containsKey("IS_DISCOUNTINUE")) {
            isDisCountinue = getArguments().getBoolean("IS_DISCOUNTINUE", false);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_subscription, container, false);
        if (isFromRegister) {
            setupToolBar(mBinding.toolbar.toolbar, "Subscription");
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
        } else {
            if (isDisCountinue) {
                setupToolBar(mBinding.toolbar.toolbar, "Subscription");
                mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);
            } else
                setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Subscription");
        }
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.getShapeGradient(mActivity, mBinding.layoutWorkout.btnPurchase);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.btnPurchase.setOnClickListener(view -> {
            setPayment();
        });
        subscriptionData = new ArrayList<>();
        callSubscriptionList();
        return mBinding.getRoot();
    }

    private void setPayment() {
        if (subscriptionListAdapter.selectionPostition == -1) {
            showSnackBar(mBinding.linear, "Select subscription plan", Snackbar.LENGTH_LONG);
        } else if (subscriptionData.get(subscriptionListAdapter.selectionPostition).getPrice() == 0) {
            purchaseSubscription("", "");
        } else {
            checkout();
        }
    }

    private void checkout() {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher);
        final Activity activity = mActivity;


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Fitzoh");
            options.put("description", "Subscription charge");
            options.put("currency", "INR");
            options.put("amount", subscriptionData.get(subscriptionListAdapter.selectionPostition).getPrice() * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());
            preFill.put("email", session.getAuthorizedUser(mActivity).getEmail());
//            preFill.put("contact", "+919784849848");

            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    private void callSubscriptionList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSubscriptionOption()
                    , getCompositeDisposable(), getSubscriptionOptionList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {

            case getSubscriptionOptionList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                subscriptionData = new ArrayList<>();
                SubscriptionTypeListModel subscriptionTypeListModel = (SubscriptionTypeListModel) o;
                if (subscriptionTypeListModel.getStatus() == AppConstants.SUCCESS) {

                    if (subscriptionTypeListModel.getData() != null) {
                        subscriptionData.addAll(subscriptionTypeListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (subscriptionData.size() == 0) {
                            //   mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            //  mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            subscriptionListAdapter = new SubscriptionListAdapter(mActivity, subscriptionData, session.getAuthorizedUser(mContext).getSubscription_id());
                            for (int i = 0; i < subscriptionData.size(); i++) {
                                if (subscriptionData.get(i).getId() == session.getAuthorizedUser(mContext).getSubscription_id()) {
                                    subscriptionListAdapter.selectionPostition = i;
                                }
                            }
                            subscriptionListAdapter.notifyDataSetChanged();
                            mBinding.layoutWorkout.recyclerView.setAdapter(subscriptionListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, subscriptionTypeListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    if (isFromRegister) {
                        session.setLogin(true);
                        showSnackBar(mBinding.linear, "subscription purchased successfully", Snackbar.LENGTH_LONG);
                        startActivity(new Intent(mActivity, BankDetailActivity.class));
                        getActivity().finish();
                    } else {
                        if (isDisCountinue) {
                            callAPI();
                        } else {
                            getActivity().finish();
                            showSnackBar(mBinding.linear, "subscription purchased successfully", Snackbar.LENGTH_LONG);
                        }

                    }

                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case getTrainerProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainerProfileModel trainerProfileModel = (TrainerProfileModel) o;
                if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {
                    session.getAuthorizedUser(mContext).setIs_subscription(trainerProfileModel.getData().getIs_subscription());
                    session.getAuthorizedUser(mContext).setSubscription_continue(trainerProfileModel.getData().getSubscription_continue());
                    session.getAuthorizedUser(mContext).setSubscription_id(trainerProfileModel.getData().getSubscription_id());

                    session.setLogin(true);
                    User user = new User();
                    user.setUserId(String.valueOf(trainerProfileModel.getData().getId())); //userId it can be any unique user identifier NOTE : +,*,? are not allowed chars in userId.
                    user.setDisplayName(trainerProfileModel.getData().getName()); //displayName is the name of the user which will be shown in chat messages
                    user.setEmail(trainerProfileModel.getData().getEmail()); //optional
                    user.setAuthenticationTypeId(User.AuthenticationType.APPLOZIC.getValue());  //User.AuthenticationType.APPLOZIC.getValue() for password verification from Applozic server and User.AuthenticationType.CLIENT.getValue() for access Token verification from your server set access token as password
                    user.setPassword("123456");
                    if (trainerProfileModel.getData().getPhoto() != null)//optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
                        user.setImageLink(trainerProfileModel.getData().getPhoto());//optional, set your image link if you have

                    Applozic.connectUser(getActivity(), user, new AlLoginHandler() {
                        @Override
                        public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            Intent intent = new Intent(mActivity, NavigationMainActivity.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(intent);
                            mActivity.finish();
                        }

                        @Override
                        public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                            // If any failure in registration the callback  will come here
                            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    });
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");
    }

    public void purchaseSubscription(String transaction_id, String payment_method) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchaseSubscription(
                subscriptionData.get(subscriptionListAdapter.selectionPostition).getId(), transaction_id, payment_method, userId)
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.linear, s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRefresh() {
        callSubscriptionList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }

    private void callAPI() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            if (session.getAuthorizedUser(mContext).getRoleId().equals("2")) {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).getTrainerProfile(Integer.parseInt(userId))
                        , getCompositeDisposable(), getTrainerProfile, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).getTrainerProfileGym(Integer.parseInt(userId))
                        , getCompositeDisposable(), getTrainerProfile, this);
            }

        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }
}
