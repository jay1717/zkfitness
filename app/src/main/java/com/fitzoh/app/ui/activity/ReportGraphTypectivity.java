package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.ClientProfileFragment;
import com.fitzoh.app.ui.fragment.DietReportFragment;
import com.fitzoh.app.ui.fragment.ReportCLientOptionFragment;
import com.fitzoh.app.ui.fragment.StrengthReportFragment;
import com.fitzoh.app.ui.fragment.WorkoutReportFragment;

public class ReportGraphTypectivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        Intent intent = getIntent();
        if (null != intent) {
            String intentData = intent.getExtras().getString("graphType");
            switch (intentData) {
                case "workout":
                    WorkoutReportFragment clientProfileReportFragment = WorkoutReportFragment.newInstance();
                    clientProfileReportFragment.setArguments(getIntent().getExtras());
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, clientProfileReportFragment, "Workout")
                            .commit();
                    break;

                case "report":
                    DietReportFragment dietReportFragment = DietReportFragment.newInstance();
                    dietReportFragment.setArguments(getIntent().getExtras());
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, dietReportFragment, "Diet")
                            .commit();
                    break;
                case "strength":
                    StrengthReportFragment strengthReportFragment = StrengthReportFragment.newInstance();
                    strengthReportFragment.setArguments(getIntent().getExtras());
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, strengthReportFragment, "Strength")
                            .commit();
                    break;
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
