package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.GymClientAssigedAdapter;
import com.fitzoh.app.databinding.FragmentGymTrainersClientBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GymClientListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.GymClientProfileActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class GymTrainersClientFragment extends BaseFragment implements SingleCallback, GymClientAssigedAdapter.onDataModeified, SwipeRefreshLayout.OnRefreshListener {

    FragmentGymTrainersClientBinding mBinding;
    String userId, userAccessToken;
    private int id = 0;
    GymClientAssigedAdapter adapter;
    private List<GymClientListModel.DataBean> data = new ArrayList<>();

    public GymTrainersClientFragment() {
        // Required empty public constructor
    }

    public static GymTrainersClientFragment newInstance() {
        GymTrainersClientFragment fragment = new GymTrainersClientFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Trainer's Client");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("trainerId", 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_gym_trainers_client, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Utils.isOnline(getActivity()))
            getClient();
        else
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    private void getClient() {
        mBinding.swipeContainer.setRefreshing(false);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getGymTrainerClient(1, id)
                , getCompositeDisposable(), single, this);

    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                GymClientListModel clientListModel = (GymClientListModel) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (clientListModel != null && clientListModel.getStatus() == AppConstants.SUCCESS && clientListModel.getData() != null) {
                    data = clientListModel.getData();
                    if (data.size() == 0) {
                        mBinding.imgNoData.setVisibility(View.VISIBLE);
                        mBinding.recyclerView.setVisibility(View.GONE);
                    } else {
                        mBinding.imgNoData.setVisibility(View.GONE);
                        mBinding.recyclerView.setVisibility(View.VISIBLE);
                        adapter = new GymClientAssigedAdapter(getActivity(), data, this);
                        mBinding.recyclerView.setAdapter(adapter);
                    }
                } else {
                    showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
            case assignSchedule:
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void getData(GymClientListModel.DataBean dataBean) {
        startActivity(new Intent(getActivity(), GymClientProfileActivity.class).putExtra("data", dataBean));
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(getActivity()))
            getClient();
        else
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
    }
}
