package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramClientAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramClientBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainingProgramClientList;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;


public class FragmentTrainingProgramClient extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentTrainingProgramClientBinding mBinding;

    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<TrainingProgramClientList.DataBean> trainingProgramListData = new ArrayList<>();
    TrainingProgramClientAdapter trainingProgramListAdapter;


    public FragmentTrainingProgramClient() {
    }

    public static FragmentTrainingProgramClient newInstance() {
        return new FragmentTrainingProgramClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.training_program));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_training_program_client, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        trainingProgramListData = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        trainingProgramListAdapter = new TrainingProgramClientAdapter(mActivity, trainingProgramListData);
        mBinding.recyclerView.setAdapter(trainingProgramListAdapter);
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(mActivity))
                callTrainingList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callTrainingList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientTrainingProgram()
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callTrainingDelete(int trainingId) {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteTrainingProgram(trainingId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.deleteTraining, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callTrainingList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainingProgramListData = new ArrayList<>();
                TrainingProgramClientList trainingProgramList = (TrainingProgramClientList) o;
                if (trainingProgramList != null && trainingProgramList.getStatus() == AppConstants.SUCCESS && trainingProgramList.getData() != null && trainingProgramList.getData().size() > 0) {
                    trainingProgramListData.addAll(trainingProgramList.getData());
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.imgNoData.setVisibility(View.GONE);
                    trainingProgramListAdapter = new TrainingProgramClientAdapter(mActivity, trainingProgramListData);
                    mBinding.recyclerView.setAdapter(trainingProgramListAdapter);
                } else {
                    trainingProgramListData.addAll(new ArrayList<>());
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setVisibility(View.GONE);
                }
                break;
            case deleteTraining:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deleteTraining = (CommonApiResponse) o;
                if (deleteTraining.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(mActivity))
                        callTrainingList();
                    else {
                        showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.mainLayout, deleteTraining.getMessage(), Snackbar.LENGTH_LONG);
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                trainingProgramListData.addAll(new ArrayList<>());
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setVisibility(View.GONE);
                break;
        }
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onRefresh() {
        callTrainingList();
    }
}
