package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientAssignScheduleAdapter;
import com.fitzoh.app.databinding.FragmentClientAssignToGroupBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class ClientAssignToGroupFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    FragmentClientAssignToGroupBinding mBinding;
    String userId, userAccessToken;
    private int group_id = 0;
    private String group_name = "";
    ClientAssignScheduleAdapter adapter;
    private List<ClientListModel.DataBean> data = new ArrayList<>();

    public ClientAssignToGroupFragment() {
        // Required empty public constructor
    }

    public static ClientAssignToGroupFragment newInstance(Bundle bundle) {
        ClientAssignToGroupFragment clientAssignFragment = new ClientAssignToGroupFragment();
        clientAssignFragment.setArguments(bundle);
        return clientAssignFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, group_name);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            group_id = getArguments().getInt("data");
            group_name = getArguments().getString("group_name", "");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_assign_to_group, container, false);
        Utils.getShapeGradient(mActivity, mBinding.client.btnAssign);
        mBinding.client.swipeContainer.setOnRefreshListener(this);
        mBinding.client.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        getClients();
        mBinding.client.btnAssign.setOnClickListener(view -> {
            if (validation()) {
                assignWorkout();
            }
        });

        return mBinding.getRoot();
    }

    private boolean validation() {
        if (data == null || data.size() <= 0) {
            showSnackBar(mBinding.layoutClient, "No Data Available", Snackbar.LENGTH_LONG);
            return false;
        } /*else if (adapter.getData() == null || adapter.getData().size() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please select client to assign", Snackbar.LENGTH_LONG);
            return false;
        }*/
        return true;
    }

    private void getClients() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientList(null, null, null, null, group_id)
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

    }

    private void assignWorkout() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addClientGroupUser(requestBody)
                    , getCompositeDisposable(), assignWorkout, this);
        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

    }

    private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("client_ids", adapter.getData());
            jsonObject.put("client_group_id", group_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientListModel clientListModel = (ClientListModel) o;
                if (clientListModel != null && clientListModel.getStatus() == AppConstants.SUCCESS && clientListModel.getData() != null && clientListModel.getData().size() > 0) {
                    mBinding.client.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.client.imgNoData.setVisibility(View.GONE);
                    data = clientListModel.getData();
                    adapter = new ClientAssignScheduleAdapter(getActivity(), data);
                    mBinding.client.recyclerView.setAdapter(adapter);
                } else {
                    mBinding.client.recyclerView.setVisibility(View.GONE);
                    mBinding.client.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.layoutClient, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case single:
                mBinding.client.recyclerView.setVisibility(View.GONE);
                mBinding.client.imgNoData.setVisibility(View.VISIBLE);
        }
        showSnackBar(mBinding.layoutClient, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }


    @Override
    public void onRefresh() {
        getClients();
        mBinding.client.swipeContainer.setRefreshing(false);
    }
}
