package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.EditWorkoutAdapter;
import com.fitzoh.app.databinding.FragmentWorkoutEditBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DeleteWorkOutModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AddExerciseActivity;
import com.fitzoh.app.ui.activity.AddSetActivity;
import com.fitzoh.app.ui.activity.CreateExerciseActivity;
import com.fitzoh.app.ui.activity.DetailExcersiceActivity;
import com.fitzoh.app.ui.activity.SelectExerciseActivity;
import com.fitzoh.app.ui.dialog.AddWorkoutNoteDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.resend;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;


public class WorkoutEditFragment extends BaseFragment implements EditWorkoutAdapter.onAddClient, SingleCallback, SwipeRefreshLayout.OnRefreshListener {
    FragmentWorkoutEditBinding mBinding;
    String userId, userAccessToken;
    private WorkOutListModel.DataBean dataBean;
    private List<List<WorkoutExerciseListModel.DataBean>> listDatas = new ArrayList<>();
    boolean is_workout_client = false;

    public WorkoutEditFragment() {
    }


    public static WorkoutEditFragment newInstance(Bundle bundle) {
        WorkoutEditFragment fragment = new WorkoutEditFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (WorkOutListModel.DataBean) getArguments().getSerializable("data");
        }
        if (getArguments() != null && getArguments().containsKey("is_workout_client")) {
            is_workout_client = getArguments().getBoolean("is_workout_client");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // setupToolBarWithBackArrow(mBinding.toolbar.toolbar, dataBean.getName());

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_workout_edit, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.layoutEditWorkout.fab);
        Utils.getShapeGradient(mActivity, mBinding.layoutEditWorkout.txtCreate);
        Utils.getShapeGradient(mActivity, mBinding.layoutEditWorkout.txtAdd);
        mBinding.layoutEditWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutEditWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);


        mBinding.toolbar.imgBack.setOnClickListener(view -> setBackpress());


        mBinding.toolbar.tvTitle.setText(dataBean.getName());
        mBinding.toolbar.imgAction.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_comment));
        mBinding.toolbar.imgAction.setOnClickListener(view -> {
            AddWorkoutNoteDialog dialogNote = new AddWorkoutNoteDialog(dataBean.getId());
            //dialogNote.setListener(this);
            dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutNoteDialog.class.getSimpleName());
            dialogNote.setCancelable(false);
        });
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

//        setHasOptionsMenu(true);
        mBinding.layoutEditWorkout.recyclerView.setAdapter(new EditWorkoutAdapter(getContext(), this, new ArrayList<>()));
        mBinding.layoutEditWorkout.fab.setOnClickListener(view -> setClickImage());
        mBinding.layoutEditWorkout.txtCreate.setOnClickListener(view -> {
            mBinding.layoutEditWorkout.fab.performClick();
            mActivity.startActivityForResult(new Intent(mActivity, CreateExerciseActivity.class).putExtra("dataBean", dataBean), 0);
        });

        mBinding.layoutEditWorkout.txtAdd.setOnClickListener(view -> {
            mBinding.layoutEditWorkout.fab.performClick();
//            mActivity.startActivityForResult(intent, 0);
            mActivity.startActivityForResult(new Intent(mActivity, AddExerciseActivity.class).putExtra("dataBean", dataBean), 0);
        });
        callExerciseList();
        return mBinding.getRoot();
    }

    private void setClickImage() {
        if (mBinding.layoutEditWorkout.txtCreate.getVisibility() == View.INVISIBLE) {
            mBinding.layoutEditWorkout.txtCreate.setVisibility(View.VISIBLE);
            Animation animFadeInCall = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            mBinding.layoutEditWorkout.txtCreate.startAnimation(animFadeInCall);

            mBinding.layoutEditWorkout.txtAdd.setVisibility(View.VISIBLE);
            Animation animFadeInMsg = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            mBinding.layoutEditWorkout.txtAdd.startAnimation(animFadeInMsg);
        } else {
            Animation animFadeInCall = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            mBinding.layoutEditWorkout.txtCreate.startAnimation(animFadeInCall);
            Animation animFadeInMsg = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            mBinding.layoutEditWorkout.txtAdd.startAnimation(animFadeInMsg);
            mBinding.layoutEditWorkout.txtAdd.setVisibility(View.INVISIBLE);
            mBinding.layoutEditWorkout.txtCreate.setVisibility(View.INVISIBLE);
        }
    }

    private void setBackpress() {

        String set = "0";
        String name = "";
        String msg = "";
        for (List<WorkoutExerciseListModel.DataBean> beans : listDatas) {
            for (WorkoutExerciseListModel.DataBean dataBean : beans) {

                if (dataBean.getNo_of_sets().equals(set)) {
                    name = name + dataBean.getExercise_name() + ", ";
                } else {
                }
            }
        }
        if (name == null && name.length() <= 0) {
            getActivity().finish();
        } else {
            Log.e("Message", msg);
            msg = name.substring(0, name.length() - 2);
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Please click on the exercise and add sets");
            alertDialog.setMessage(msg);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            });

            alertDialog.show();
        }
    }

    public String method(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == 'x') {
            str = str.substring(0, str.length() - 2);
        }
        return str;
    }

    @Override
    public void view(int id) {
        startActivity(new Intent(getActivity(), DetailExcersiceActivity.class).putExtra("id", id));
    }

    @Override
    public void edit(WorkoutExerciseListModel.DataBean dataBean) {
        Intent intent = new Intent(getActivity(), AddSetActivity.class);
        intent.putExtra("exercise", (Parcelable) dataBean);
        intent.putExtra("workout", this.dataBean);
        startActivity(intent);

    }

    @Override
    public void superGiant(WorkoutExerciseListModel.DataBean dataBean, String data) {
        if (data.equalsIgnoreCase("super")) {
            Intent intent = new Intent(getActivity(), SelectExerciseActivity.class);
            intent.putExtra("data", (Parcelable) dataBean);
            intent.putExtra("id", this.dataBean.getId());
            mActivity.startActivityForResult(intent, 0);
        } else {
            callBreakApartAPI(dataBean.getExercise_set_id());
        }
    }

    private void callBreakApartAPI(int exercise_set_id) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).breakSuperSet(exercise_set_id, session.getStringDataByKeyNull(CLIENT_ID))
                , getCompositeDisposable(), save, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callExerciseList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    @Override
    public void delete(int exerciseId, int exerciseSetId) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteSetExercise(exerciseId, exerciseSetId, session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

        }
    }

    @Override
    public void deleteExercise(int exerciseId) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOutExercise(dataBean.getId(), exerciseId, session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), resend, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

        }
    }

    private void callExerciseList() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        if (is_workout_client) {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOutExerciseListdetail(dataBean.getId(), session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), workoutList, this);
        } else
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOutExerciseList(dataBean.getId(), session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), workoutList, this);


    }

    private void deleteWorkout() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(dataBean.getId())
                    , getCompositeDisposable(), deleteWorkout, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutExerciseListModel workoutExerciseListModel = (WorkoutExerciseListModel) o;
                if (workoutExerciseListModel.getStatus() == AppConstants.SUCCESS) {
                    listDatas = workoutExerciseListModel.getData();
                    if (listDatas != null) {
                        if (listDatas.size() == 0) {
                            mBinding.layoutEditWorkout.recyclerView.setVisibility(View.GONE);
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                        } else {
                            mBinding.imgNoData.setVisibility(View.GONE);
                            mBinding.layoutEditWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutEditWorkout.recyclerView.setAdapter(new EditWorkoutAdapter(getContext(), this, listDatas));
                        }
                    } else
                        showSnackBar(mBinding.mainLayout, workoutExerciseListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel deleteExercise = (DeleteWorkOutModel) o;
                if (deleteExercise.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(mContext)) {
                        callExerciseList();
                    } else {
                        showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.mainLayout, deleteExercise.getMessage(), Snackbar.LENGTH_LONG);
                break;
            case resend:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel delete = (DeleteWorkOutModel) o;
                if (delete.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.mainLayout, "Exercise deleted successfully", Snackbar.LENGTH_LONG);
                    if (Utils.isOnline(mContext)) {
                        callExerciseList();
                    } else {
                        showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                } else
                    showSnackBar(mBinding.mainLayout, delete.getMessage(), Snackbar.LENGTH_LONG);
                break;
            case deleteWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel deleteWorkOutModel = (DeleteWorkOutModel) o;
                if (deleteWorkOutModel.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(mContext)) {
                        mActivity.setResult(Activity.RESULT_OK);
                        mActivity.finish();
                    } else {
                        showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.mainLayout, deleteWorkOutModel.getMessage(), Snackbar.LENGTH_LONG);
                break;
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    if (Utils.isOnline(getActivity()))
                        callExerciseList();
                    else
                        showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case workoutList:
                mBinding.layoutEditWorkout.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onRefresh() {
        callExerciseList();
        mBinding.layoutEditWorkout.swipeContainer.setRefreshing(false);
    }
}
