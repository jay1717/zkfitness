package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.YoutubeSearchFragment;

public class YoutubeSearchActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_search);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, YoutubeSearchFragment.newInstance(getIntent().getExtras()), "YoutubeSearchFragment")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
