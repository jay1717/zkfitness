package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogSelectColorBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import io.reactivex.disposables.CompositeDisposable;

@SuppressLint("ValidFragment")
public class SelectColorDialog extends AppCompatDialogFragment implements SingleCallback {

    DialogSelectColorBinding mBinding;
    String userId, userAccessToken;
    public SessionManager session;
    public CompositeDisposable compositeDisposable;
    private int red = 0, green = 0, blue = 0;
    private SharedPreferences pref;

    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_color, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnAdd, false);
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            view = mBinding.getRoot();
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setView(view);
        AlertDialog dialog = alertBuilder.create();
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 40);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }

    private void prepareLayout() {
        pref = getActivity().getSharedPreferences("colors", 0);
        if (getTargetRequestCode() == 0) {
            red = pref.getInt("redPrimary", 21);
            blue = pref.getInt("bluePrimary", 215);
            green = pref.getInt("greenPrimary", 153);
        } else {
            red = pref.getInt("redAccent", 58);
            blue = pref.getInt("blueAccent", 209);
            green = pref.getInt("greenAccent", 190);
        }
        setColorFinal();

        mBinding.seekbarRed.setOnSeekbarFinalValueListener(value -> {
            red = value.intValue();
            setColorFinal();
        });
        mBinding.seekbarGreen.setOnSeekbarFinalValueListener(value -> {
            green = value.intValue();
            setColorFinal();
        });
        mBinding.seekbarBlue.setOnSeekbarFinalValueListener(value -> {
            blue = value.intValue();
            setColorFinal();
        });

        mBinding.seekbarRed.setMinStartValue(red).apply();
        mBinding.seekbarGreen.setMinStartValue(green).apply();
        mBinding.seekbarBlue.setMinStartValue(blue).apply();

        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.btnAdd.setOnClickListener(view -> {
            /*if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    addNote();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }*/
            if (validation()) {
                Intent intent = new Intent();
                intent.putExtra("red", red);
                intent.putExtra("green", green);
                intent.putExtra("blue", blue);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
            } else {
                showSnackBar("Please select dark color");
            }
        });
        mBinding.btnCancel.setOnClickListener(view -> dismiss());

    }

    private boolean validation() {
        double darkness = 1 - (0.299 * Color.red(red) + 0.587 * Color.green(green) + 0.114 * Color.blue(blue)) / 255;
        return !(darkness < 0.9);
    }

    private void setColorFinal() {
        mBinding.viewColor.setBackgroundColor(Color.rgb(red, green, blue));
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    /*private void addNote() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveDietNotes(dietId, mBinding.edtNote.getText().toString())
                , getCompositeDisposable(), addNotes, this);
    }
*/
    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case addNotes:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);

                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    //dialogClickListener.setClick();
                    dismiss();
                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

}
