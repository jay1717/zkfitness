package com.fitzoh.app.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentClientDocumentViewBinding;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.Utils;

public class ClientDocumentViewFragment extends BaseFragment {

    private String data;
    private FragmentClientDocumentViewBinding mBinding;

    public static ClientDocumentViewFragment newInstance(Bundle bundle) {
        ClientDocumentViewFragment fragment = new ClientDocumentViewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            data = getArguments().getString("data", "");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Documents");

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_document_view, container, false);

        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        if (data != null && !data.equalsIgnoreCase("")) {
            String type = data.substring(data.lastIndexOf(".") + 1);
            if (type.equalsIgnoreCase("pdf")) {
                mBinding.webView.setVisibility(View.VISIBLE);
                mBinding.imageView.setVisibility(View.GONE);
                mBinding.webView.getSettings().setJavaScriptEnabled(true);
                String googleDocs = "https://docs.google.com/viewer?url=";

                mBinding.webView.loadUrl(googleDocs + data);
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                mBinding.webView.setWebViewClient(new WebViewClient() {

                    public void onPageFinished(WebView view, String url) {
                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                        // do your stuff here
                    }

                    @Override
                    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                        super.onReceivedError(view, request, error);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            Log.e("onReceivedError: ", "code" + error.getErrorCode());
                        }
                    }
                });

                Log.e("prepareLayout: ", data);
            } else if (type.equalsIgnoreCase("jpg") || type.equalsIgnoreCase("jpeg") || type.equalsIgnoreCase("gif") || type.equalsIgnoreCase("png")) {
                mBinding.webView.setVisibility(View.GONE);
                mBinding.imageView.setVisibility(View.VISIBLE);
                Utils.setImagePlaceHolder(mContext, mBinding.imageView, data);
            } else {
                mBinding.txtNoDocument.setVisibility(View.VISIBLE);
                mBinding.webView.setVisibility(View.GONE);
                mBinding.imageView.setVisibility(View.GONE);
            }
        }
    }


}
