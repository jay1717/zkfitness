package com.fitzoh.app.ui.fragment;


import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentAddClientBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addTrainer;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddClientFragment extends BaseFragment implements View.OnClickListener, SingleCallback {


    FragmentAddClientBinding mBinding;
    String userId, userAccessToken;

    public AddClientFragment() {
        // Required empty public constructor
    }

    public static AddClientFragment newInstance() {
        AddClientFragment fragment = new AddClientFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, session.getRollId() == 2 ? "Add Trainer Client" : getString(R.string.add_gym_client));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_add_client, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setThemeWidget();
        setClickEvents();
        return mBinding.getRoot();
    }

    public boolean validation() {
        boolean value = true;
        if (mBinding.edtName.getText().toString().length() == 0) {
            setEdittextError(mBinding.txtErrorName, getString(R.string.empty_userfirstname));
            value = false;
        }

        if (mBinding.edtEmail.getText().toString().length() == 0) {
            setEdittextError(mBinding.txtErrorEmail, getString(R.string.empty_username_register));
            value = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.edtEmail.getText().toString()).matches()) {
            setEdittextError(mBinding.txtErrorEmail, getString(R.string.invalid_email));
            value = false;
        }
        if (mBinding.edtMobile.getText().toString().length() > 0) {
            if (mBinding.edtMobile.getText().toString().length() != 10) {
                setEdittextError(mBinding.txtErrorMobileNo, getString(R.string.invalid_phone));
                value = false;
            } else {
                mBinding.txtErrorMobileNo.setText("");
                mBinding.txtErrorMobileNo.setVisibility(View.GONE);
            }

        }

        if (mBinding.edtDate.getText().toString().length() == 0) {
            setEdittextError(mBinding.txtErrorDate, getString(R.string.emapty_date));
            value = false;
        }

        return value;
    }

    private void setThemeWidget() {
        Utils.getShapeGradient(mActivity, mBinding.btnRegister);
        Utils.setTextViewStartImage(mActivity, mBinding.edtEmail, R.drawable.ic_email, true);
        Utils.setTextViewStartImage(mActivity, mBinding.edtName, R.drawable.ic_user, true);
        Utils.setTextViewStartImage(mActivity, mBinding.edtDate, R.drawable.ic_calendar, true);
        Utils.setSpinnerArrow(mActivity, new ImageView[]{mBinding.btnDateDropdown});
        Utils.setRadioButtonSelectors(mActivity, mBinding.radioMale);
        Utils.setRadioButtonSelectors(mActivity, mBinding.radioFemale);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    private void setClickEvents() {
        mBinding.btnRegister.setOnClickListener(this);
        mBinding.edtDate.setOnClickListener(this);
        mBinding.edtDate.addTextChangedListener(new MyTaskWatcher(mBinding.edtDate));
        mBinding.edtName.addTextChangedListener(new MyTaskWatcher(mBinding.edtName));
        mBinding.edtEmail.addTextChangedListener(new MyTaskWatcher(mBinding.edtEmail));

    }

    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                (view, year, monthOfYear, dayOfMonth) -> {
                    c.set(Calendar.YEAR, year);
                    c.set(Calendar.MONTH, monthOfYear);
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    mBinding.edtDate.setText(dateFormat.format(c.getTime()));
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        if (Utils.isOnline(getActivity())) {
                            callAPI(session.getRollId() == 2);
                        } else {
                            showSnackBar(mBinding.frame, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
                        }

                        //   startActivity(new Intent(getActivity(), CreateUserActivity.class));

                        /*    if (mBinding.register.chkServiceProvider.isChecked()) {
                         *//* session.setBooleanDataByKey(KEY_IS_LOGIN, true);
                            session.setBooleanDataByKey(KEY_IS_SP, true);*//*
                            JSONObject object = new JSONObject();
                            try {
                                object.put("FirstName", mBinding.register.edtUserFirstname.getText().toString());
                                object.put("LastName", mBinding.register.edtUserLastname.getText().toString());
                                object.put("Email", mBinding.register.edtEmail.getText().toString());
                                object.put("PhoneNo", mBinding.register.edtPhone.getText().toString());
                                object.put("Password", mBinding.register.edtPassword.getText().toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            ((LoginActivity) getActivity()).pushFragments(AppConstants.LOGIN_KEY, fragmentSeriviceProviderFeild, true, true);
                        } else {
//                            showProgressBar().show();
//                            callApi();
                        }*/
                        mBinding.txtErrorEmail.setVisibility(View.GONE);
                        mBinding.txtErrorDate.setVisibility(View.GONE);
                        mBinding.txtErrorName.setVisibility(View.GONE);
                    }
                } else {
                    Snackbar.make(mBinding.frame, R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_date_dropdown:

              /*  final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);



                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
                                Date newDate = null;
                                try {
                                    newDate = spf.parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                spf = new SimpleDateFormat("dd MMM yyyy");
                                date = spf.format(newDate);
                                if(date!=null)
                                    mBinding.register.edtDate.setText(date);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();*/
                showDatePickerDialog();
                break;
            case R.id.edtDate:
                showDatePickerDialog();
                break;

        }
    }

    private void callAPI(boolean isTrainer) {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);

        String DOB = "";
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateFormatDOB = new SimpleDateFormat("yyyy/MM/dd");
        Date date = null;
        try {
            date = dateFormat.parse(mBinding.edtDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DOB = dateFormatDOB.format(date);
        if (isTrainer)
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addClientFromTrainer(mBinding.edtName.getText().toString(),
                    mBinding.edtEmail.getText().toString(),
                    mBinding.edtMobile.getText().toString(), DOB, mBinding.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "m" : "f", mBinding.countryCode.getSelectedCountryCode())
                    , getCompositeDisposable(), addTrainer, this);
        else
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveClientData(mBinding.edtName.getText().toString(),
                    mBinding.edtEmail.getText().toString(),
                    mBinding.edtMobile.getText().toString(), DOB, mBinding.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "m" : "f", mBinding.countryCode.getSelectedCountryCode())
                    , getCompositeDisposable(), addTrainer, this);
       /* } else {
            if (isSocialMedia == 1) {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).signup(roleId,
                        mBinding.register.edtName.getText().toString(),
                        mBinding.register.edtEmail.getText().toString(),
                        DOB, mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "1" : "0", isSocialMedia, "", String.valueOf(id))
                        , getCompositeDisposable(), register, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).signup(roleId,
                        mBinding.register.edtName.getText().toString(),
                        mBinding.register.edtEmail.getText().toString(),
                        DOB, mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "1" : "0", isSocialMedia, "", null)
                        , getCompositeDisposable(), register, this);
            }
        }*/
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case addTrainer:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {

                    getActivity().finish();
                } else {
                    showSnackBar(mBinding.frame, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    private class MyTaskWatcher implements TextWatcher {
        private View view;

        MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.edtEmail:
                    if (mBinding.edtEmail.hasFocus()) {
                        if (mBinding.edtEmail.getText().toString().length() == 0) {
                            setEdittextError(mBinding.txtErrorEmail, getString(R.string.empty_username));
                        } else if (mBinding.edtEmail.getText().toString().matches("[0-9]+")) {
                            if (mBinding.edtEmail.getText().toString().length() != 10) {
                                setEdittextError(mBinding.txtErrorEmail, getString(R.string.invalid_phone));
                            } else {
                                mBinding.txtErrorEmail.setText("");
                                mBinding.txtErrorEmail.setVisibility(View.GONE);
                            }
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.edtEmail.getText().toString()).matches()) {
                            setEdittextError(mBinding.txtErrorEmail, getString(R.string.invalid_email));
                        } else {
                            mBinding.txtErrorEmail.setText("");
                            mBinding.txtErrorEmail.setVisibility(View.GONE);
                        }
                    }
                    break;
                case R.id.edtDate:
                    if (mBinding.edtDate.hasFocus()) {
                        if (mBinding.edtDate.getText().toString().length() == 0) {
                            setEdittextError(mBinding.txtErrorDate, getString(R.string.emapty_date));
                        } else {
                            mBinding.txtErrorDate.setText("");
                            mBinding.txtErrorDate.setVisibility(View.GONE);
                        }

                    }
                    break;
                case R.id.edtName:
                    if (mBinding.edtName.hasFocus()) {
                        if (mBinding.edtName.getText().toString().length() == 0) {
                            setEdittextError(mBinding.txtErrorName, getString(R.string.empty_userfirstname));
                        } else {
                            mBinding.txtErrorName.setText("");
                            mBinding.txtErrorName.setVisibility(View.GONE);
                        }
                    }
                    break;

            }

        }

    }

}
