package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramWeekClientAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramClientBinding;
import com.fitzoh.app.model.TrainingProgramClientList;
import com.fitzoh.app.model.TrainingProgramDetailClientData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.LogWorkoutExerciseListActivity;
import com.fitzoh.app.ui.activity.TrainingProgramExerciseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailTrainingProgramClientFragment extends BaseFragment implements SingleCallback, TrainingProgramWeekClientAdapter.RedirectToDetailListener, SwipeRefreshLayout.OnRefreshListener {


    public FragmentTrainingProgramClientBinding mBinding;
    public TrainingProgramWeekClientAdapter recyclerViewAdapter;
    String userId, userAccessToken;
    TrainingProgramClientList.DataBean dataBean;
    private List<Integer> deletedDaysList = new ArrayList<>();
    private List<Integer> deletedWeekList = new ArrayList<>();

    public DetailTrainingProgramClientFragment() {
        // Required empty public constructor
    }


    public static DetailTrainingProgramClientFragment newInstance(Bundle bundle) {
        DetailTrainingProgramClientFragment fragment = new DetailTrainingProgramClientFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (TrainingProgramClientList.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_training_program_client, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, dataBean.getTitle());
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        prepareLayout();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        recyclerViewAdapter = new TrainingProgramWeekClientAdapter(mActivity, new ArrayList<>(), this);
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(mActivity);
        mBinding.recyclerView.setLayoutManager(recyclerLayoutManager);
        mBinding.recyclerView.setAdapter(recyclerViewAdapter);
        callTrainingProgram();
    }


    private void callTrainingProgram() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainingProgramDetail(dataBean.getTraining_program_id())
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainingProgramDetailClientData editModel = (TrainingProgramDetailClientData) o;
                if (editModel != null && editModel.getStatus() == AppConstants.SUCCESS && editModel.getData() != null && editModel.getData().getTraining_weeks().size() > 0) {
                    recyclerViewAdapter = new TrainingProgramWeekClientAdapter(mActivity, editModel.getData().getTraining_weeks(), this);
                    mBinding.recyclerView.setAdapter(recyclerViewAdapter);
                    setAdapter();

                } else {
                    setAdapter();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.getRoot(), throwable.getMessage(), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case trainingList:
                setAdapter();
                break;
            case single:
                mActivity.finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
        }
    }


    private void setAdapter() {
        if (recyclerViewAdapter.weekList.size() > 0) {
            mBinding.recyclerView.setVisibility(View.VISIBLE);
            mBinding.imgNoData.setVisibility(View.GONE);
        } else {
            mBinding.recyclerView.setVisibility(View.GONE);
            mBinding.imgNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void openExerciseDetail(int workout_id, String workout_name, boolean isAM, int day_id) {
        Intent intent = new Intent(mActivity, TrainingProgramExerciseActivity.class);
        intent.putExtra("workout_id", workout_id);
        intent.putExtra("workout_name", workout_name);
        intent.putExtra("training_program_id", Integer.parseInt(dataBean.getTraining_program_id()));
        intent.putExtra("is_am_workout", isAM);
        intent.putExtra("weekday_id", day_id);
        if (dataBean.getIs_active() == 0) {
            intent.putExtra("isActive", dataBean.getIs_active());
        }

        startActivity(intent);
    }

    @Override
    public void viewLogWorkout(int workout_id, String workout_name, boolean isAM, int day_id) {
        startActivity(new Intent(mActivity, LogWorkoutExerciseListActivity.class).putExtra("workout_id", workout_id).putExtra("workout_name", workout_name).putExtra("training_program_id", Integer.parseInt(dataBean.getTraining_program_id())).putExtra("is_am_workout", isAM).putExtra("isSetEditable", false).putExtra("weekday_id", day_id).putExtra("is_show_log", 0));
    }

    @Override
    public void onRefresh() {
        callTrainingProgram();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
