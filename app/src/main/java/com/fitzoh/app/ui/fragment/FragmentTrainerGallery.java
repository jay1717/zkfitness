package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainerProfileGalleryAdapter;
import com.fitzoh.app.databinding.FragmentTrainerGalleryBinding;
import com.fitzoh.app.model.TrainerGalleryModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentTrainerGallery#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTrainerGallery extends BaseFragment implements SingleCallback {
    FragmentTrainerGalleryBinding mBinding;
    int trainerId = 0;
    String userId, userAccessToken;
    List<TrainerGalleryModel.DataBean> trainerGalleryData;
    TrainerProfileGalleryAdapter trainerProfileGalleryAdapter;

    public FragmentTrainerGallery() {
        // Required empty public constructor
    }


    public static FragmentTrainerGallery newInstance(int trainerId) {
        FragmentTrainerGallery fragment = new FragmentTrainerGallery();
        Bundle bundle = new Bundle();
        bundle.putInt("data", trainerId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            trainerId = getArguments().getInt("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
      //  setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.gallery_title));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_trainer_gallery, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (trainerId != 0)
            getTrainerGallery();
        return mBinding.getRoot();

    }

    private void getTrainerGallery() {
        if (Utils.isOnline(getActivity())) {
            if (getArguments() != null && getArguments().containsKey("data")) {
                trainerId = getArguments().getInt("data");
            } mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerGallery(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerGallery, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getTrainerGallery:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainerGalleryData = new ArrayList<>();
                TrainerGalleryModel trainerGalleryModel = (TrainerGalleryModel) o;
                if (trainerGalleryModel.getData() != null) {
                    if (trainerGalleryModel.getStatus() == AppConstants.SUCCESS) {
                        trainerGalleryData.addAll(trainerGalleryModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (trainerGalleryModel.getData().size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            trainerProfileGalleryAdapter = new TrainerProfileGalleryAdapter(getActivity(), trainerGalleryData);
                            mBinding.recyclerView.setAdapter(trainerProfileGalleryAdapter);
                        }
                    }
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
