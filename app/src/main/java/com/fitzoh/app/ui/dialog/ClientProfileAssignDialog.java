package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.DialogToAssignAdapter;
import com.fitzoh.app.databinding.DialogSelectToAssignBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

@SuppressLint("ValidFragment")
public class ClientProfileAssignDialog extends AppCompatDialogFragment {

    DialogSelectToAssignBinding mBinding;
    String userId, userAccessToken;
    public SessionManager session;
    public CompositeDisposable compositeDisposable;

    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_select_to_assign, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnSave, false);
            view = mBinding.getRoot();
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

            if (getArguments() != null && getArguments().getStringArrayList("data") != null && getArguments().getStringArrayList("data").size() > 0) {
                mBinding.imgNoData.setVisibility(View.GONE);
                mBinding.recyclerDays.setVisibility(View.VISIBLE);
                mBinding.recyclerDays.setAdapter(new DialogToAssignAdapter(getActivity(), getArguments().getStringArrayList("data")));
            } else {
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                mBinding.recyclerDays.setVisibility(View.INVISIBLE);
                //showSnackBar("No data found");
               // dismiss();
            }
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setView(view);
        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();
        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }

    private void prepareLayout() {
        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        mBinding.btnSave.setOnClickListener(view -> {
            if(mBinding.recyclerDays.getVisibility()==View.VISIBLE){
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent().putExtra("data", ((DialogToAssignAdapter) mBinding.recyclerDays.getAdapter()).selectionPostition));
                        dismiss();
                    }
                } else {
                    showSnackBar(getString(R.string.network_unavailable));
                }
            }



        });
        mBinding.btnCancel.setOnClickListener(view -> {
            dismiss();
        });

    }

    private boolean validation() {
        if (((DialogToAssignAdapter) mBinding.recyclerDays.getAdapter()).selectionPostition == -1) {
            showSnackBar("Please select workout to assign");
            return false;
        }
        return true;
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }


    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }


    public interface DaySelection {
        void setClick(ArrayList<String> days, int clientId);

        void onCancel();
    }
}
