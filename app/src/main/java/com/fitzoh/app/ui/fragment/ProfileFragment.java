package com.fitzoh.app.ui.fragment;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentProfileBinding;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.model.UserDataModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.BankDetailActivity;
import com.fitzoh.app.ui.activity.BasicInfoActivity;
import com.fitzoh.app.ui.activity.ClientDocumentActivity;
import com.fitzoh.app.ui.activity.ClientDocumentViewActivity;
import com.fitzoh.app.ui.activity.CreateUserActivity;
import com.fitzoh.app.ui.activity.MedicalFormActivity;
import com.fitzoh.app.ui.activity.ProfileSubscriptionActivity;
import com.fitzoh.app.ui.activity.TransformationListActivity;
import com.fitzoh.app.ui.activity.UploadPhotoActivity;
import com.fitzoh.app.ui.dialog.AddAmountDialog;
import com.fitzoh.app.ui.dialog.AddMealDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener, SingleCallback, AddAmountDialog.AddAmount {
    FragmentProfileBinding mBinding;
    String rollId;
    String userId, userAccessToken;
    TrainerProfileModel trainerProfileModel = null;
    String myUrl;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
      /*  Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupWhiteToolBarWithMenu(mBinding.toolbar.toolbar, "Profile");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        setHasOptionsMenu(false);
        rollId = session.getAuthorizedUser(mContext).getRoleId();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        Utils.getShapeGradient(mActivity, mBinding.layoutTrainer.txtClientCount);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setTextViewEndImage(mActivity, mBinding.layoutProfileAction.txtUrl, R.drawable.ic_copy);
        getTrainerProfile();
        setLayout();
        mBinding.toolbar.tvTitle.setTextColor(Color.WHITE);
        setOnClick();


        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        getTrainerProfile();
    }

    private void getTrainerProfile() {
        if (Utils.isOnline(getActivity())) {
            // mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            //   disableScreen(true);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            if (rollId.equals("3")) {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerProfileGym(session.getAuthorizedUser(mContext).getId())
                        , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerProfile, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainerProfile(session.getAuthorizedUser(mContext).getId())
                        , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTrainerProfile, this);
            }


        } else {
            showSnackBar(mBinding.constMain, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void setLayout() {
        if (rollId.equals("1")) {
            mBinding.layoutProfileAction.txtSubscription.setVisibility(View.GONE);
            mBinding.layoutProfileAction.txtBankDetail.setVisibility(View.GONE);
            mBinding.layoutProfileAction.txtGallery.setVisibility(View.GONE);
            mBinding.layoutProfileAction.txtTransformation.setVisibility(View.GONE);
            mBinding.layoutProfileAction.txtUrl.setVisibility(View.GONE);
            mBinding.layoutProfileAction.txtMedicalForm.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.txtSubProfile.setText("Client Profile");
            mBinding.layoutProfileAction.viewTxtUrl.setVisibility(View.GONE);
            mBinding.layoutTrainer.txtClient.setVisibility(View.GONE);
            mBinding.layoutTrainer.txtClientCount.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewBankDetail.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewGallery.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewTransformation.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewTxtUrl.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewtxtMedicalForm.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.txtDocument.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.viewDocument.setVisibility(View.VISIBLE);

        } else {
            mBinding.layoutProfileAction.txtSubscription.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.viewTxtUrl.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.txtBankDetail.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.txtGallery.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.txtTransformation.setVisibility(View.VISIBLE);

            mBinding.layoutTrainer.txtClient.setVisibility(View.VISIBLE);
            mBinding.layoutTrainer.txtClientCount.setVisibility(View.VISIBLE);

            mBinding.layoutProfileAction.viewSubProfile.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.viewBankDetail.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.viewGallery.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.viewTransformation.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.viewTxtUrl.setVisibility(View.VISIBLE);
            mBinding.layoutProfileAction.txtDocument.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewDocument.setVisibility(View.GONE);

            mBinding.layoutProfileAction.txtMedicalForm.setVisibility(View.GONE);
            mBinding.layoutProfileAction.viewtxtMedicalForm.setVisibility(View.GONE);

            if (rollId.equals("2")) {
                if (session.getAuthorizedUser(mActivity).isGym_trainer()) {
                    mBinding.layoutProfileAction.txtSubscription.setVisibility(View.GONE);
                    mBinding.layoutProfileAction.txtBankDetail.setVisibility(View.GONE);
                    mBinding.layoutProfileAction.txtSubscription.setVisibility(View.GONE);
                    mBinding.layoutProfileAction.viewBankDetail.setVisibility(View.GONE);
                }
                mBinding.layoutProfileAction.txtSubProfile.setText("Trainer Profile");
            } else if (rollId.equals("3")) {
                mBinding.layoutProfileAction.txtSubProfile.setText("Fitness Center Profile");
            }
        }
    }

    private void setOnClick() {
        mBinding.layoutTrainer.imgUser.setOnClickListener(this);
        mBinding.layoutProfileAction.txtProfile.setOnClickListener(this);
        mBinding.layoutProfileAction.txtSubProfile.setOnClickListener(this);
        mBinding.layoutProfileAction.txtTransformation.setOnClickListener(this);
        mBinding.layoutProfileAction.txtGallery.setOnClickListener(this);
        mBinding.layoutProfileAction.txtMedicalForm.setOnClickListener(this);
        mBinding.layoutProfileAction.txtBankDetail.setOnClickListener(this);
        mBinding.layoutProfileAction.txtSubscription.setOnClickListener(this);
        mBinding.layoutProfileAction.txtUrl.setOnClickListener(this);
        mBinding.layoutProfileAction.txtDocument.setOnClickListener(this);


    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification_white, menu);

    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtProfile:
                Intent intentBasicProfile = new Intent(getActivity(), BasicInfoActivity.class);
                intentBasicProfile.putExtra("TRAINER_MODEL", trainerProfileModel);
                startActivity(intentBasicProfile);
                break;

            case R.id.txtMedicalForm:
                Intent intentMedicalProfile = new Intent(getActivity(), MedicalFormActivity.class);
                intentMedicalProfile.putExtra("Is_FROM_REGISTER", false);
                startActivity(intentMedicalProfile);
                break;
            case R.id.txtSubProfile:
                Intent intentProfile = new Intent(getActivity(), CreateUserActivity.class);
                intentProfile.putExtra("Is_FROM_REGISTER", false);
                intentProfile.putExtra("rollId", rollId);
                intentProfile.putExtra("TRAINER_MODEL", trainerProfileModel);
                startActivity(intentProfile);

                break;
            case R.id.txtTransformation:
                Intent intentTransformation = new Intent(getActivity(), TransformationListActivity.class);
                startActivity(intentTransformation);
                break;
            case R.id.txtGallery:
                Intent intentGallery = new Intent(getActivity(), UploadPhotoActivity.class);
                intentGallery.putExtra("Is_FROM_REGISTER", false);
                startActivity(intentGallery);
                break;
            case R.id.txtBankDetail:
                Intent intentBankDetail = new Intent(getActivity(), BankDetailActivity.class);
                intentBankDetail.putExtra("Is_FROM_REGISTER", false);
                startActivity(intentBankDetail);
                break;
            case R.id.txtSubscription:
                Intent intent = new Intent(getActivity(), ProfileSubscriptionActivity.class);
                intent.putExtra("Is_FROM_REGISTER", false);
                startActivity(intent);
                break;
            case R.id.txtUrl:
                AddAmountDialog dialog = new AddAmountDialog();
                dialog.setListener(this);
                dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddMealDialog.class.getSimpleName());
                dialog.setCancelable(false);
                break;

            case R.id.imgUser:
                if(trainerProfileModel!=null && trainerProfileModel.getData()!=null && trainerProfileModel.getData().getPhoto()!=null){
                    startActivity(new Intent(getContext(), ClientDocumentViewActivity.class).putExtra("data", trainerProfileModel.getData().getPhoto()));
                }
                break;

            case R.id.txtDocument :
                startActivity(new Intent(getActivity(), ClientDocumentActivity.class).putExtra("data", trainerProfileModel.getData().getId()));
                break;

        }

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getTrainerProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                trainerProfileModel = (TrainerProfileModel) o;
                if (trainerProfileModel != null) {
                    if (trainerProfileModel.getStatus() == AppConstants.SUCCESS) {
                        setDataToSession();
                        setDataToView();
                    }
                }
                break;
        }
    }

    private void setDataToView() {
        mBinding.layoutTrainer.name.setText(trainerProfileModel.getData().getName());
        mBinding.layoutTrainer.txtTag.setText(trainerProfileModel.getData().getEmail());
        mBinding.layoutTrainer.txtDescription.setText(trainerProfileModel.getData().getAbout());
        mBinding.layoutTrainer.txtClientCount.setText(String.valueOf(trainerProfileModel.getData().getRemain_days()));
        if (trainerProfileModel.getData().getPhoto() != null) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder);
            Glide.with(this)
                    .load(trainerProfileModel.getData().getPhoto())
                    .apply(options)
                    .into(mBinding.layoutTrainer.imgUser);
        }
    }

    private void setDataToSession() {
        UserDataModel userDataModel = session.getAuthorizedUser(mContext);
        userDataModel.setName(trainerProfileModel.getData().getName());
        userDataModel.setMobile_number(trainerProfileModel.getData().getMobile_number());
        userDataModel.setBirthDate(trainerProfileModel.getData().getBirth_date());
        userDataModel.setCountry_code(trainerProfileModel.getData().getCountry_code());
        userDataModel.setGender(trainerProfileModel.getData().getGender());
        userDataModel.setPhoto(trainerProfileModel.getData().getPhoto());
        session.saveAuthorizedUser(mContext, userDataModel);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void addUrl(String url) {
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("PaymentUrl", url);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, url, Toast.LENGTH_LONG).show();
    }
}
