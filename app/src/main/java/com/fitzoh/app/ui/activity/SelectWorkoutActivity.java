package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.SelectNutritionPlantFragment;
import com.fitzoh.app.ui.fragment.SelectWorkoutFragment;

public class SelectWorkoutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_workout);
        if (getIntent().getIntExtra("select", 1) == 1 || getIntent().getIntExtra("select", 1) == 2)
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.realtabcontent, SelectWorkoutFragment.newInstance(getIntent().getExtras()), "SelectWorkoutFragment")
                    .commit();
        else
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.realtabcontent, SelectNutritionPlantFragment.newInstance(getIntent().getExtras()), "SelectNutritionPlantFragment")
                    .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
