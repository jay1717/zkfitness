package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.system.ErrnoException;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.ApplozicClient;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.applozic.mobicomkit.api.account.user.PushNotificationTask;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;
import com.applozic.mobicomkit.uiwidgets.ApplozicSetting;
import com.fitzoh.app.model.FitnessValuesModel;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentCreateFitnessCenterBinding;
import com.fitzoh.app.interfaces.MyLocationChangeListner;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainerProfileAddModel;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreateUserActivity;
import com.fitzoh.app.ui.activity.SelectFacilitiesActivity;
import com.fitzoh.app.ui.activity.UploadPhotoActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.editTrainerProfile;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateClientProfile;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateFitnessCenterFragment extends BaseFragment implements View.OnClickListener, PlaceSelectionListener, GoogleMap.OnInfoWindowClickListener, MyLocationChangeListner, SingleCallback {
    private static final int RECORD_REQUEST_CODE = 101;
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImage;
    TrainerProfileModel trainerProfileModel = null;

    String userId, userAccessToken;
    HashMap<String, FitnessValuesModel.DataBean> selectedFitneeList;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    //    Location location;
    LatLng latLng;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int REQUEST_SELECT_PLACE = 1000;
    private static final int REQUEST_SELECT_FITNESS_VELUE = 1001;
    String edtString = "";
    String stringName = "";

    MultipartBody.Part body;
    private FragmentCreateFitnessCenterBinding mBinding;
    private String countryName = "", cityName = "", stateName = "";
    MenuItem menuItem;
    boolean isEditable;
    boolean isFromRegister = true;
    private UserLoginTask mAuthTask = null;
    TrainerProfileAddModel trainerProfileAddModel;

    public CreateFitnessCenterFragment() {
        // Required empty public constructor
    }


    public static CreateFitnessCenterFragment newInstance() {
        return new CreateFitnessCenterFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.createprofile.edtAddress.setClickable(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_fitness_center, container, false);
        Utils.getShapeGradient(mActivity, mBinding.createprofile.btnNext);
        Utils.setTextViewEndImage(mActivity, mBinding.createprofile.edtFitness, R.drawable.ic_down_arrow);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        prepareLayout();
        makeRequest();
        setClickEvent();
        return mBinding.getRoot();
    }


    private void setClickEvent() {
        mBinding.createprofile.btnNext.setOnClickListener(this);
        mBinding.createprofile.imgPickPhoto.setOnClickListener(this);
        mBinding.createprofile.edtAddress.setOnClickListener(this);
        mBinding.createprofile.btnNext.setOnClickListener(this);
        mBinding.createprofile.edtFitness.setKeyListener(null);
        mBinding.createprofile.edtFitness.setOnClickListener(this);
    }

    public void attemptLogin(User.AuthenticationType authenticationType) {

        UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {

            @Override
            public void onSuccess(RegistrationResponse registrationResponse, final Context context) {
                mAuthTask = null;


                ApplozicClient.getInstance(context).setContextBasedChat(true).setHandleDial(true);

                Map<ApplozicSetting.RequestCode, String> activityCallbacks = new HashMap<ApplozicSetting.RequestCode, String>();
                activityCallbacks.put(ApplozicSetting.RequestCode.USER_LOOUT, CreateUserActivity.class.getName());
                ApplozicSetting.getInstance(context).setActivityCallbacks(activityCallbacks);
                MobiComUserPreference.getInstance(context).setUserRoleType(registrationResponse.getRoleType());


                PushNotificationTask.TaskListener pushNotificationTaskListener = new PushNotificationTask.TaskListener() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {

                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
//
                    }
                };
                PushNotificationTask pushNotificationTask = new PushNotificationTask(Applozic.getInstance(context).getDeviceRegistrationId(), pushNotificationTaskListener, context);
                pushNotificationTask.execute((Void) null);


            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                mAuthTask = null;

                try {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(exception.getMessage().toString());
                    alertDialog.setMessage(exception.toString());
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok_alert),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        User user = new User();
        user.setUserId(String.valueOf(session.getAuthorizedUser(getActivity()).getId()));
        user.setEmail(session.getAuthorizedUser(getActivity()).getEmail());
        user.setPassword("123456");
        user.setDisplayName(session.getAuthorizedUser(getActivity()).getName());
        user.setImageLink(trainerProfileAddModel.getData().getProfile_image());
        //  user.setContactNumber(session.getAuthorizedUser(getActivity()).getMobile_number());
        user.setAuthenticationTypeId(authenticationType.getValue());

        mAuthTask = new UserLoginTask(user, listener, getActivity());
        mAuthTask.execute((Void) null);
    }

    private void prepareLayout() {

        String text = "<font color=#4b4b4b>About your profile?</font> <font color=#8a8a8a> (max 140 characters)</font>";
        mBinding.createprofile.edtGoal.setHint(Html.fromHtml(text));
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER");
        }
        if (getArguments() != null && getArguments().containsKey("TRAINER_MODEL")) {
            trainerProfileModel = (TrainerProfileModel) getArguments().getSerializable("TRAINER_MODEL");
        }
        if (isFromRegister) {
            setupToolBar(mBinding.toolbar.toolbar, "");
            setHasOptionsMenu(false);
            mBinding.createprofile.btnNext.setText(R.string.next);
            mBinding.createprofile.btnNext.setVisibility(View.VISIBLE);
        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Profile");
            setHasOptionsMenu(true);
            if (trainerProfileModel != null) {
                mBinding.createprofile.edtGoal.setText(trainerProfileModel.getData().getAbout());
                mBinding.createprofile.edtAddress.setText(trainerProfileModel.getData().getAddress() + " " + trainerProfileModel.getData().getCity() + " " + trainerProfileModel.getData().getState() + " " + trainerProfileModel.getData().getCountry());
                mBinding.createprofile.edtLandmark.setText(trainerProfileModel.getData().getLandmark());
                if (trainerProfileModel.getData().getPhoto() != null) {
                    Utils.setImage(mContext, mBinding.createprofile.imgUser, trainerProfileModel.getData().getPhoto());
                }
                if (trainerProfileModel.getData().getFacilities() != null) {
                    setSelectedFacilityData();
                    String facilitied = "";
                    for (int i = 0; i < trainerProfileModel.getData().getFacilities().size(); i++) {
                        //selectedFitneeList.put(String.valueOf(trainerProfileModel.getData().getFacilities().get(i).getId()),trainerProfileModel.getData().getFacilities().get(i));
                        if (i == (trainerProfileModel.getData().getFacilities().size() - 1))
                            facilitied = facilitied + trainerProfileModel.getData().getFacilities().get(i).getName();
                        else {
                            facilitied = facilitied + trainerProfileModel.getData().getFacilities().get(i).getName() + " , ";
                        }
                    }
                    mBinding.createprofile.edtFitness.setText(facilitied);
                }

            }
            mBinding.createprofile.btnNext.setText("Save");
            mBinding.createprofile.btnNext.setVisibility(View.GONE);
            mBinding.createprofile.edtAddress.setEnabled(false);
            mBinding.createprofile.imgPickPhoto.setEnabled(false);
            mBinding.createprofile.edtFitness.setEnabled(false);
            mBinding.createprofile.edtAddress.setClickable(false);
            mBinding.createprofile.edtGoal.setEnabled(false);
        }

    }

    private void setSelectedFacilityData() {
        callFitnessValueApi();
    }


    private void callFitnessValueApi() {
        if (Utils.isOnline(getContext())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getFitnessValue()
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }


  /*  private void getLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                getCurrentLocation();

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            getCurrentLocation();
        }
    }*/


    public void performSearchClick() {


        if (Utils.isOnline(mContext)) {
            try {
                AutocompleteFilter filter = new AutocompleteFilter.Builder()
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                        .build();
                Intent intent = new PlaceAutocomplete.IntentBuilder
                        (PlaceAutocomplete.MODE_FULLSCREEN)
                        .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                        .setFilter(filter)
                        .build(mActivity);
                mActivity.startActivityForResult(intent, REQUEST_SELECT_PLACE);
                mBinding.createprofile.edtAddress.setClickable(false);

            } catch (GooglePlayServicesRepairableException |
                    GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), R.string.no_network_available, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_set, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.action_edit), R.drawable.ic_edit);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuItem = item;
        switch (item.getItemId()) {
            case R.id.action_edit:
                isEditable = !isEditable;
                setEditTextEnable(isEditable);
                break;
        }
        return true;
    }

    private void setEditTextEnable(boolean isEditable) {
        mBinding.createprofile.edtAddress.setEnabled(isEditable);
        mBinding.createprofile.edtAddress.setClickable(isEditable);
        mBinding.createprofile.imgPickPhoto.setEnabled(isEditable);
        mBinding.createprofile.edtFitness.setEnabled(isEditable);
        mBinding.createprofile.edtGoal.setEnabled(isEditable);
        if (isEditable) {
            mBinding.createprofile.btnNext.setVisibility(View.VISIBLE);
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_cancel));
        } else {
            mBinding.createprofile.btnNext.setVisibility(View.GONE);
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_edit));

        }

    }

    public boolean validation() {

        boolean value = true;
        if (mBinding.createprofile.edtAddress.getText().toString().equals(getString(R.string.what_is_your_address))) {
            mBinding.createprofile.txtErrorAddress.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorAddress, getString(R.string.address_empty));
            value = false;
        }
        if (mBinding.createprofile.edtGoal.getText().toString().length() == 0) {
            mBinding.createprofile.txtErrorGoal.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorGoal, getString(R.string.empty_about_us));
            value = false;
        }

        if (mBinding.createprofile.edtFitness.getText().toString().length() == 0) {
            mBinding.createprofile.txtErrorFitness.setVisibility(View.VISIBLE);
            setEdittextError(mBinding.createprofile.txtErrorFitness, getString(R.string.empty_facilities));
            value = false;
        }
       /* if (fileImage == null) {
            showSnackBar(mBinding.getRoot(), "Please Select Profile picture", Snackbar.LENGTH_LONG);
            value = false;
        }*/

        return value;


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_SELECT_PLACE) {
                Place place = PlaceAutocomplete.getPlace(mContext, data);
                this.onPlaceSelected(place);
            } else if (requestCode == REQUEST_CHECK_SETTINGS) {
                // getLocation();
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(mContext, data);
            this.onError(status);
        }
        if (requestCode == REQUEST_SELECT_FITNESS_VELUE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    selectedFitneeList = (HashMap<String, FitnessValuesModel.DataBean>) data.getExtras().getSerializable("Selected Data");
                    setFitnessEditText();

                }
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {

                    mBinding.createprofile.imgUser.setImageURI(mImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();


                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }

    }

    private void setFitnessEditText() {
        if(selectedFitneeList!=null){
            stringName = "";
            edtString = "";
            for (String key : selectedFitneeList.keySet()) {
                stringName = stringName + "" + selectedFitneeList.get(key).getValue() + ",";
                edtString = edtString + "" + String.valueOf(selectedFitneeList.get(key).getId()) + ",";
            }
            if (edtString != "" && edtString.length() > 0 && edtString.endsWith(",")) {
                edtString = edtString.substring(0, edtString.length() - 1);
            }
            if (stringName != "" && stringName.length() > 0 && stringName.endsWith(",")) {
                stringName = stringName.substring(0, stringName.length() - 1);
            }
            Log.e("id", edtString);
            mBinding.createprofile.edtFitness.setText(stringName);
        }

    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        getActivity().startActivityForResult(getPickImageChooserIntent(), 200);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_pick_photo:
                imgPick();
                break;

            case R.id.edt_address:
                performSearchClick();
                break;

            case R.id.edt_fitness:
                Intent intent = new Intent(getActivity(), SelectFacilitiesActivity.class);
                if (selectedFitneeList != null)
                    intent.putExtra("peopleList", selectedFitneeList);
                getActivity().startActivityForResult(intent, REQUEST_SELECT_FITNESS_VELUE);
                break;

            case R.id.btn_next:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        mBinding.createprofile.txtErrorAddress.setVisibility(View.GONE);
                        mBinding.createprofile.txtErrorGoal.setVisibility(View.GONE);
                        mBinding.createprofile.txtErrorFitness.setVisibility(View.GONE);

                        callAPI();

                    }
                } else {
                    showSnackBar(mBinding.getRoot(), getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }

                break;
        }

    }

    private void callAPI() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);

        RequestBody about = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtGoal.getText().toString());
        RequestBody facilities = RequestBody.create(MediaType.parse("text/plain"), edtString);
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtAddress.getText().toString());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), cityName);
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), stateName);
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), countryName);
        RequestBody location = null;
        if (latLng != null) {
            location = RequestBody.create(MediaType.parse("text/plain"), latLng.latitude + "," + latLng.longitude);
        } else if (trainerProfileModel != null) {
            location = RequestBody.create(MediaType.parse("text/plain"), trainerProfileModel.getData().getLocation());
        }
        RequestBody landmark = RequestBody.create(MediaType.parse("text/plain"), mBinding.createprofile.edtLandmark.getText().toString());
        if (isFromRegister) {
            if (fileImage != null && mImageUri!=null) {
                byte[] uploadBytes = Utils.loadImageFromStorage(mImageUri.getPath());
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
                MultipartBody.Part photo = MultipartBody.Part.createFormData("photo", fileImage.getName(), reqFile);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addTrainerProfile(
                        about,
                        facilities,
                        location,
                        address,
                        city,
                        state,
                        country, photo,
                        null,
                        landmark), getCompositeDisposable(), updateClientProfile, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addTrainerProfileWithoutImage(
                        about,
                        facilities,
                        location,
                        address,
                        city,
                        state,
                        country,
                        landmark,
                        null), getCompositeDisposable(), updateClientProfile, this);
            }
        } else {

            if (fileImage != null && mImageUri!=null) {
                byte[] uploadBytes = Utils.loadImageFromStorage(mImageUri.getPath());
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
                MultipartBody.Part photo = MultipartBody.Part.createFormData("photo", fileImage.getName(), reqFile);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).EditTrainerProfile(
                        about,
                        facilities,
                        location,
                        address,
                        city,
                        state,
                        country, landmark, null, photo, null), getCompositeDisposable(), editTrainerProfile, this);
            } else {
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).EditTrainerProfileWithoutImage(
                        about,
                        facilities,
                        location,
                        address,
                        city,
                        state,
                        country, landmark, null, null), getCompositeDisposable(), editTrainerProfile, this);
            }
        }

    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.e("on Place Selected", "Place Selected: " + place.getAddress() + "" + place.getLatLng().latitude + "" + place.getLatLng().longitude);
        getCompleteAddressString(place.getLatLng().latitude, place.getLatLng().longitude);
        mBinding.createprofile.edtAddress.setText(place.getAddress());
        mBinding.createprofile.edtAddress.setClickable(true);

//        mGoogleMap.clear();
        latLng = place.getLatLng();
        // setUpMap(mGoogleMap);

    }

    private void getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null && addresses.size() > 0) {
                Address returnedAddress = addresses.get(0);
                if (returnedAddress.getCountryName() != null) {
                    countryName = returnedAddress.getCountryName();
                }
                if (returnedAddress.getLocality() != null) {
                    cityName = returnedAddress.getLocality();
                }
                if (returnedAddress.getFeatureName() != null) {
                    stateName = returnedAddress.getAdminArea();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(Status status) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onLocationChange(Location location) {

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case updateClientProfile:

                trainerProfileAddModel = (TrainerProfileAddModel) o;
                if (trainerProfileAddModel.getStatus() == AppConstants.SUCCESS) {
                    attemptLogin(User.AuthenticationType.APPLOZIC);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            startActivity(new Intent(mActivity, UploadPhotoActivity.class));
                            getActivity().finish();
                        }
                    }, 2000);
                } else {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    showSnackBar(mBinding.getRoot(), trainerProfileAddModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case editTrainerProfile:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.getRoot(), commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                    getActivity().finish();
                } else {
                    showSnackBar(mBinding.getRoot(), commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                FitnessValuesModel facilitiesListModel = (FitnessValuesModel) o;
                if (facilitiesListModel != null && facilitiesListModel.getData() != null && facilitiesListModel.getData().size() > 0) {
                    selectedFitneeList = new HashMap<String, FitnessValuesModel.DataBean>();
                    for (int i = 0; i < facilitiesListModel.getData().size(); i++) {
                        for (int j = 0; j < trainerProfileModel.getData().getFacilities().size(); j++) {
                            if (trainerProfileModel.getData().getFacilities().get(j).getId() == facilitiesListModel.getData().get(i).getId()) {
                                selectedFitneeList.put(String.valueOf(facilitiesListModel.getData().get(i).getId()), facilitiesListModel.getData().get(i));
                            }
                        }
                    }
                    setFitnessEditText();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.getRoot(), "onFailure", Snackbar.LENGTH_LONG);

    }
}
