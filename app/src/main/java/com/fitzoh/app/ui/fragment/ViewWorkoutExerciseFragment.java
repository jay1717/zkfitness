package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ViewWorkoutMainAdapter;
import com.fitzoh.app.databinding.FragmentLogWorkoutExerciseListBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.LogWorkoutExerciseListActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;


public class ViewWorkoutExerciseFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {
    FragmentLogWorkoutExerciseListBinding mBinding;
    String userId, userAccessToken;
    private List<List<LogWorkoutDataModel.DataBean>> listDatas = new ArrayList<>();
    private int workout_id = 0, training_program_id = 0, weekday_id = 0;
    private String workout_name = "";
    private boolean is_am_workout = false;
    private PubNub pubnub;
    private List<String> channels;

    public ViewWorkoutExerciseFragment() {
    }


    public static ViewWorkoutExerciseFragment newInstance(Bundle bundle) {
        ViewWorkoutExerciseFragment fragment = new ViewWorkoutExerciseFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            workout_id = getArguments().getInt("workout_id", 0);
            training_program_id = getArguments().getInt("training_program_id", 0);
            weekday_id = getArguments().getInt("weekday_id", 0);
            workout_name = getArguments().getString("workout_name", "");
            is_am_workout = getArguments().getBoolean("is_am_workout", is_am_workout);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, workout_name);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_log_workout_exercise_list, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.getShapeGradient(mActivity, mBinding.btnSave);

        mBinding.btnSave.setText("Track");
        mBinding.btnSave.setOnClickListener(view -> {

            startActivity(new Intent(getActivity(), LogWorkoutExerciseListActivity.class).putExtra("workout_id", workout_id).putExtra("workout_name", workout_name).putExtra("training_program_id", training_program_id).putExtra("is_am_workout", is_am_workout).putExtra("isSetEditable", true).putExtra("weekday_id", weekday_id).putExtra("is_show_log", 0));
            initLiveClientSocket();


        });
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.recyclerView.setAdapter(new ViewWorkoutMainAdapter(mActivity, new ArrayList<>()));
        callExerciseList();
        return mBinding.getRoot();
    }

    private void initLiveClientSocket() {
        channels = new ArrayList<>();
        if (session.getAuthorizedUser(mActivity).isDirect_gym_client()) {
            channels.add(session.getAuthorizedUser(mActivity).getDirect_gym_id());
            if (session.getAuthorizedUser(mActivity).isClient_trainer()) {
                channels.add(session.getAuthorizedUser(mActivity).getTrainer_id());
            }
        } else
            channels.add(session.getAuthorizedUser(mActivity).getTrainer_id());


        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);


        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // create message payload using Gson
        JsonObject messageJsonObject = new JsonObject();
        messageJsonObject.addProperty("id", session.getAuthorizedUser(mActivity).getId());
        messageJsonObject.addProperty("name", session.getAuthorizedUser(mActivity).getName());
        messageJsonObject.addProperty("photo", session.getAuthorizedUser(mActivity).getPhoto());
        messageJsonObject.addProperty("email", session.getAuthorizedUser(mActivity).getEmail());
        messageJsonObject.addProperty("date", "" + spf.format(Calendar.getInstance().getTime()));
        messageJsonObject.addProperty("isStartedWorkout", true);
        JsonArray jsonArray = new JsonArray();

        for (int i = 0; i < listDatas.size(); i++) {
            for (int j = 0; j < listDatas.get(i).size(); j++) {
                JsonObject object = new JsonObject();
                object.addProperty("exercise_name", listDatas.get(i).get(j).getExercise_name());
                object.addProperty("id", listDatas.get(i).get(j).getId());
                object.addProperty("rating", listDatas.get(i).get(j).getRating());
                object.addProperty("timestamp", listDatas.get(i).get(j).getTimeStamp());
                jsonArray.add(object);
            }
        }
        messageJsonObject.add("exercises", jsonArray);

        System.out.println("Message to send: " + messageJsonObject.toString());

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost
//                    Toast.makeText(mActivity, "connectivity is lost", Toast.LENGTH_LONG).show();
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

//                    Toast.makeText(mActivity, "Connect event", Toast.LENGTH_LONG).show();
                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
                        for (String channelName : channels)
                            pubnub.publish().channel(channelName).message(messageJsonObject).async(new PNCallback<PNPublishResult>() {
                                @Override
                                public void onResponse(PNPublishResult result, PNStatus status) {
                                    // Check whether request successfully completed or not.
//                                    Toast.makeText(mActivity, "request successfully completed", Toast.LENGTH_LONG).show();

                                    if (!status.isError()) {

//                                        Toast.makeText(mActivity, "Message successfully published to specified channel", Toast.LENGTH_LONG).show();

                                        // Message successfully published to specified channel.
                                    }
                                    // Request processing failed.
                                    else {

//                                        Toast.makeText(mActivity, "Handle message publish error", Toast.LENGTH_LONG).show();

                                        status.retry();
                                        // Handle message publish error. Check 'category' property to find out possible issue
                                        // because of which request did fail.
                                        //
                                        // Request can be resent using: [status retry];
                                    }
                                }
                            });
                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {

                // Handle new message stored in message.message
                if (message.getChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscription()
                }

                /*JsonElement receivedMessageObject = message.getMessage();
                System.out.println("Received message content: " + receivedMessageObject.toString());
                // extract desired parts of the payload, using Gson
                String msg = message.getMessage().getAsJsonObject().get("msg").getAsString();
                System.out.println("msg content: " + msg);
                System.out.println("msg subscription: " + message.getSubscription());
                System.out.println("msg time token: " + message.getTimetoken());*/


                                    /*
                                        log the following items with your favorite logger
                                            - message.getMessage()
                                            - message.getSubscription()
                                            - message.getTimetoken()
                                    */
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });
        pubnub.subscribe().channels(channels).execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pubnub != null)
            pubnub.unsubscribe()
                    .channels(channels)
                    .execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callExerciseList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }


    private void callExerciseList() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkoutDetails(workout_id, training_program_id, (is_am_workout) ? 1 : 0, weekday_id, null, null,1)
                    , getCompositeDisposable(), workoutList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LogWorkoutDataModel workoutExerciseListModel = (LogWorkoutDataModel) o;
                if (workoutExerciseListModel.getStatus() == AppConstants.SUCCESS && workoutExerciseListModel.getData() != null && workoutExerciseListModel.getData().size() > 0) {
                    listDatas = workoutExerciseListModel.getData();
                    mBinding.imgNoData.setVisibility(View.GONE);
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setAdapter(new ViewWorkoutMainAdapter(mActivity, listDatas));

                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case workoutList:
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onRefresh() {
        callExerciseList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
