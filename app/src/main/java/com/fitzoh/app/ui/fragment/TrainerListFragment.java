package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainerListAdapter;
import com.fitzoh.app.databinding.FragmentTrainerListBinding;
import com.fitzoh.app.model.GetTrainerModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.TrainerProfileActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainerListFragment extends BaseFragment implements SingleCallback, TrainerListAdapter.onDataPass, SwipeRefreshLayout.OnRefreshListener {

    FragmentTrainerListBinding mBinding;

    String userId, userAccessToken;
    String latLong, speciality, roll_id;
    int fitnessCenterId;

    public TrainerListFragment() {
        // Required empty public constructor
    }

    public static TrainerListFragment newInstance() {
        TrainerListFragment fragment = new TrainerListFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.select_trainer));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_trainer_list, container, false);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Intent intent = getActivity().getIntent();
        if (null != intent) {
            latLong = intent.getExtras().getString("latLong");
            //   latLong = "23.72";
            fitnessCenterId = intent.getExtras().getInt("fitnessCenterId");
            speciality = intent.getExtras().getString("speciality");
            roll_id = intent.getExtras().getString("role_id");

            Log.e("trList", "" + latLong + fitnessCenterId + speciality);
        }

        return mBinding.getRoot();
    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (Utils.isOnline(getContext())) {
            mBinding.imgNoData.setVisibility(View.GONE);
            getTrainerList();
        } else {
            mBinding.imgNoData.setImageDrawable(getResources().getDrawable(R.drawable.no_internet_connecion));
            mBinding.imgNoData.setVisibility(View.VISIBLE);
            showSnackBar(mBinding.layoutTrainer, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        prepareLayout();
    }

    private void getTrainerList() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTrainer(fitnessCenterId, speciality, latLong, roll_id)
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        GetTrainerModel getTrainerModel = (GetTrainerModel) o;
        if (getTrainerModel.getStatus() == AppConstants.SUCCESS && getTrainerModel.getData() != null && getTrainerModel.getData().size() > 0) {

            if (getTrainerModel.getData().size() == 1) {
                startActivity(new Intent(getActivity(), TrainerProfileActivity.class).putExtra("trainerId", getTrainerModel.getData().get(0).getId()).putExtra("isGym", getTrainerModel.getData().get(0).getIs_gym()));
                getActivity().finish();
            } else {
                mBinding.imgNoData.setVisibility(View.GONE);
                setAdapter(getTrainerModel.getData());
            }
        } else {
            mBinding.imgNoData.setImageDrawable(getResources().getDrawable(R.drawable.no_data_found));
            mBinding.imgNoData.setVisibility(View.VISIBLE);
        }
    }

    private void setAdapter(List<GetTrainerModel.DataBean> data) {
        mBinding.recyclerView.setAdapter(new TrainerListAdapter(getActivity(), data, this));
    }
    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.layoutTrainer, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void pass(GetTrainerModel.DataBean dataBean) {
        dataBean.getId();
        startActivity(new Intent(getActivity(), TrainerProfileActivity.class).putExtra("trainerId",dataBean.getId()).putExtra("isGym", dataBean.getIs_gym()));
    }

    @Override
    public void onRefresh() {
        prepareLayout();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
