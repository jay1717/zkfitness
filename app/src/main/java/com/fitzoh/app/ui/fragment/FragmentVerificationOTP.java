package com.fitzoh.app.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.listners.AlLoginHandler;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentVerificationOtpBinding;
import com.fitzoh.app.model.RegisterModel;
import com.fitzoh.app.model.VerificationOTPModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreateUserActivity;
import com.fitzoh.app.ui.activity.MedicalFormActivity;
import com.fitzoh.app.ui.activity.NavigationClientMainActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.ui.activity.SubscriptionActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.Arrays;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.resend;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class FragmentVerificationOTP extends BaseFragment implements View.OnClickListener, SingleCallback {

    private static String ARG_PARAM1 = "id";
    private static String ARG_PARAM2 = "email";
    FragmentVerificationOtpBinding mBinding;
    View view;
    private int id = 0;
    private String email = "";
    String isRegister;
    Intent intent;

    public static FragmentVerificationOTP newInstance(int id, String email, String isRegister) {
        FragmentVerificationOTP fragment = new FragmentVerificationOTP();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, id);
        args.putString(ARG_PARAM2, email);
        args.putString("isRegister", isRegister);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt(ARG_PARAM1);
            email = getArguments().getString(ARG_PARAM2);
            isRegister = getArguments().getString("isRegister");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_verification_otp, container, false);
            Utils.getShapeGradient(mActivity, mBinding.verificationLayout.btnVerify);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            view = mBinding.getRoot();
            // setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Verification");
//            mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
            Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
            mBinding.toolbar.tvTitle.setText(R.string.verification);
            prepareLayout();
        }
        setClickEvents();
        setHasOptionsMenu(false);
        return view;
    }

    private void prepareLayout() {
        // Animate Layouts
        String strColor = String.format("#%06X", 0xFFFFFF & ((BaseActivity) mActivity).res.getColor(R.color.colorPrimary));
        String text = "<font color=#8a8a8a>If You don't receive code!</font> <font color=" + strColor + ">Resend</font>";
        mBinding.verificationLayout.txtResend.setText(Html.fromHtml(text));
    }

    private void setClickEvents() {
        mBinding.verificationLayout.edtOne.addTextChangedListener(new GenericTextWatcher(mBinding.verificationLayout.edtOne));
        mBinding.verificationLayout.edtTwo.addTextChangedListener(new GenericTextWatcher(mBinding.verificationLayout.edtTwo));
        mBinding.verificationLayout.edtThree.addTextChangedListener(new GenericTextWatcher(mBinding.verificationLayout.edtThree));
        mBinding.verificationLayout.edtFour.addTextChangedListener(new GenericTextWatcher(mBinding.verificationLayout.edtFour));
        mBinding.verificationLayout.edtFive.addTextChangedListener(new GenericTextWatcher(mBinding.verificationLayout.edtFive));

        mBinding.verificationLayout.edtOne.setOnKeyListener(onKeyListener);
        mBinding.verificationLayout.edtTwo.setOnKeyListener(onKeyListener);
        mBinding.verificationLayout.edtThree.setOnKeyListener(onKeyListener);
        mBinding.verificationLayout.edtFour.setOnKeyListener(onKeyListener);
        mBinding.verificationLayout.edtFive.setOnKeyListener(onKeyListener);

        mBinding.verificationLayout.edtOne.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        mBinding.verificationLayout.edtTwo.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        mBinding.verificationLayout.edtThree.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        mBinding.verificationLayout.edtFour.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        mBinding.verificationLayout.edtFive.setTextColor(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));

        mBinding.verificationLayout.btnVerify.setOnClickListener(this);
        mBinding.verificationLayout.txtResend.setOnClickListener(this);

        mBinding.toolbar.imgBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnVerify:

                if (Utils.isOnline(getApplicationContext())) {
                    if (validation()) {
                        callAPI();
                    }
                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }
                break;
            case R.id.txtResend:

                if (Utils.isOnline(getApplicationContext())) {
                    callAPIResend();

                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                }
                break;

            case R.id.img_back:
                getActivity().onBackPressed();
                break;

        }
    }

    private void callAPI() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);

            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).varifyotp(id, getOTP())
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callAPIResend() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new UnauthorizedNetworkInterceptor(getActivity())).create(WebserviceBuilder.class).resendotp(email)
                    , getCompositeDisposable(), resend, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                VerificationOTPModel verificationOTPModel = (VerificationOTPModel) o;
                if (verificationOTPModel != null && verificationOTPModel.getStatus() == AppConstants.SUCCESS) {
                    // session.setLogin(true);
                    session.setRollId(Integer.parseInt(verificationOTPModel.getData().getRoleId()));
                    session.saveAuthorizedUser(mContext, verificationOTPModel.getData());

                    if (isRegister.equals("true")) {
                      /*  if (verificationOTPModel.getData().getRoleId().equals("1")) {
                            Intent intent = new Intent(mActivity, CreateUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            mActivity.finish();
                        } else if (verificationOTPModel.getData().getRoleId().equals("2")||verificationOTPModel.getData().getRoleId().equals("3")) {
                            Intent intent = new Intent(mActivity, CreateTrainerProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("rollId", verificationOTPModel.getData().getRoleId());
                            startActivity(intent);
                            mActivity.finish();
                        } else {
                            startActivity(new Intent(mActivity, NavigationMainActivity.class));
                        }*/
                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Intent intent = new Intent(mActivity, CreateUserActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("rollId", verificationOTPModel.getData().getRoleId());
                        startActivity(intent);
                        mActivity.finish();
                    } else {
                        if (checkColor(verificationOTPModel.getData().getPrimary_color_code()) && checkColor(verificationOTPModel.getData().getSecondary_color_code())) {
                            List<String> primaryColor = Arrays.asList(verificationOTPModel.getData().getPrimary_color_code().split(","));
                            List<String> secondary = Arrays.asList(verificationOTPModel.getData().getSecondary_color_code().split(","));
                            SharedPreferences pref = mContext.getSharedPreferences("colors", 0);
                            SharedPreferences.Editor editor = pref.edit();
                            double redP = Double.parseDouble(primaryColor.get(0));
                            double greenP = Double.parseDouble(primaryColor.get(1));
                            double blueP = Double.parseDouble(primaryColor.get(2));
                            double redS = Double.parseDouble(secondary.get(0));
                            double greenS = Double.parseDouble(secondary.get(1));
                            double blueS = Double.parseDouble(secondary.get(2));
                            editor.putInt("redPrimary", (int) redP);
                            editor.putInt("bluePrimary", (int) blueP);
                            editor.putInt("greenPrimary", (int) greenP);
                            editor.putInt("redAccent", (int) redS);
                            editor.putInt("blueAccent", (int) blueS);
                            editor.putInt("greenAccent", (int) greenS);
                            editor.apply();
                        }

                        if (verificationOTPModel.getData().getIsProfileCompleted() == 0) {
                            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            intent = new Intent(mActivity, CreateUserActivity.class);
                            intent.putExtra("rollId", verificationOTPModel.getData().getRoleId());
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            mActivity.finish();
                        } else if (verificationOTPModel.getData().getIsMedicalFormCompleted() == 0 && verificationOTPModel.getData().getRoleId().equals("1")) {
                            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            intent = new Intent(mActivity, MedicalFormActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            mActivity.finish();
                        } else if (verificationOTPModel.getData().isIs_subscription() == 0 && verificationOTPModel.getData().getRoleId().equals("2") && verificationOTPModel.getData().isGym_trainer()) {

                            session.setLogin(true);

                            User user = new User();
                            user.setUserId(String.valueOf(verificationOTPModel.getData().getId())); //userId it can be any unique user identifier NOTE : +,*,? are not allowed chars in userId.
                            user.setDisplayName(verificationOTPModel.getData().getName()); //displayName is the name of the user which will be shown in chat messages
                            user.setEmail(verificationOTPModel.getData().getEmail()); //optional
                            user.setAuthenticationTypeId(User.AuthenticationType.APPLOZIC.getValue());  //User.AuthenticationType.APPLOZIC.getValue() for password verification from Applozic server and User.AuthenticationType.CLIENT.getValue() for access Token verification from your server set access token as password
                            user.setPassword("123456");
                            if (verificationOTPModel.getData().getPhoto() != null)//optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
                                user.setImageLink(verificationOTPModel.getData().getPhoto());//optional, set your image link if you have

                            Applozic.connectUser(getActivity(), user, new AlLoginHandler() {
                                @Override
                                public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                    Log.e("Responce Success", registrationResponse.getMessage());

                                    disableScreen(false);
                                    intent = new Intent(mActivity, NavigationMainActivity.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                    startActivity(intent);
                                    mActivity.finish();
                                }

                                @Override
                                public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                                    // If any failure in registration the callback  will come here
                                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                    disableScreen(false);
                                    Log.e("Responce Error", exception.getMessage());

                                }
                            });
                        } else if (verificationOTPModel.getData().isIs_subscription() == 0 && !verificationOTPModel.getData().getRoleId().equals("1")) {
                            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            intent = new Intent(mActivity, SubscriptionActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            mActivity.finish();
                        } else if (verificationOTPModel.getData().getRoleId().equals("1")) {
                            session.setLogin(true);
                            User user = new User();
                            user.setUserId(String.valueOf(verificationOTPModel.getData().getId())); //userId it can be any unique user identifier NOTE : +,*,? are not allowed chars in userId.
                            user.setDisplayName(verificationOTPModel.getData().getName()); //displayName is the name of the user which will be shown in chat messages
                            user.setEmail(verificationOTPModel.getData().getEmail()); //optional
                            user.setAuthenticationTypeId(User.AuthenticationType.APPLOZIC.getValue());  //User.AuthenticationType.APPLOZIC.getValue() for password verification from Applozic server and User.AuthenticationType.CLIENT.getValue() for access Token verification from your server set access token as password
                            user.setPassword("123456");
                            if (verificationOTPModel.getData().getPhoto() != null)//optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
                                user.setImageLink(verificationOTPModel.getData().getPhoto());//optional, set your image link if you have

                            Applozic.connectUser(getActivity(), user, new AlLoginHandler() {
                                @Override
                                public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                    disableScreen(false);
                                    intent = new Intent(mActivity, NavigationClientMainActivity.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                    startActivity(intent);
                                    mActivity.finish();
                                }

                                @Override
                                public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                                    // If any failure in registration the callback  will come here
                                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                    disableScreen(false);
                                }
                            });
                        } else {
                            if (verificationOTPModel.getData().isIs_subscription() == 0 || verificationOTPModel.getData().getSubscription_continue()==0) {
                                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                intent = new Intent(mActivity, SubscriptionActivity.class).putExtra("IS_DISCOUNTINUE",true);
                                intent.putExtra("IS_FROM_REGISTER", false);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                mActivity.finish();
                            } else {
                                session.setLogin(true);
                                User user = new User();
                                user.setUserId(String.valueOf(verificationOTPModel.getData().getId())); //userId it can be any unique user identifier NOTE : +,*,? are not allowed chars in userId.
                                user.setDisplayName(verificationOTPModel.getData().getName()); //displayName is the name of the user which will be shown in chat messages
                                user.setEmail(verificationOTPModel.getData().getEmail()); //optional
                                user.setAuthenticationTypeId(User.AuthenticationType.APPLOZIC.getValue());  //User.AuthenticationType.APPLOZIC.getValue() for password verification from Applozic server and User.AuthenticationType.CLIENT.getValue() for access Token verification from your server set access token as password
                                user.setPassword("123456");
                                if (verificationOTPModel.getData().getPhoto() != null)//optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
                                    user.setImageLink(verificationOTPModel.getData().getPhoto());//optional, set your image link if you have

                                Applozic.connectUser(getActivity(), user, new AlLoginHandler() {
                                    @Override
                                    public void onSuccess(RegistrationResponse registrationResponse, Context context) {
                                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                        intent = new Intent(mActivity, NavigationMainActivity.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        mActivity.finish();
                                    }

                                    @Override
                                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                                        // If any failure in registration the callback  will come here
                                        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    }
                                });
                            }

                        }
                    }
                } else {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    showSnackBar(mBinding.mainLayout, verificationOTPModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case resend:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                RegisterModel resend = (RegisterModel) o;
                if (resend != null && resend.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.mainLayout, resend.getMessage(), Snackbar.LENGTH_LONG);
                } else {
                    showSnackBar(mBinding.mainLayout, resend.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    private boolean checkColor(String color_code) {
        if (color_code == null || color_code.equalsIgnoreCase("")) {
            return false;
        } else return Arrays.asList(color_code.split(",")).size() == 3;

    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, "onFailure", Snackbar.LENGTH_LONG);
    }

    private String getOTP() {
        return mBinding.verificationLayout.edtOne.getText().toString() + mBinding.verificationLayout.edtTwo.getText().toString() +
                mBinding.verificationLayout.edtThree.getText().toString() + mBinding.verificationLayout.edtFour.getText().toString()
                + mBinding.verificationLayout.edtFive.getText().toString();
    }

    public boolean validation() {
        boolean value = true;
        if (mBinding.verificationLayout.edtOne.getText().toString().length() == 0) {
            showSnackBar(mBinding.mainLayout, "Please Enter OTP", Snackbar.LENGTH_LONG);
            value = false;
        } else if (mBinding.verificationLayout.edtTwo.getText().toString().length() == 0) {
            showSnackBar(mBinding.mainLayout, "Please Enter OTP", Snackbar.LENGTH_LONG);
            value = false;
        } else if (mBinding.verificationLayout.edtThree.getText().toString().length() == 0) {
            showSnackBar(mBinding.mainLayout, "Please Enter OTP", Snackbar.LENGTH_LONG);
            value = false;
        } else if (mBinding.verificationLayout.edtFour.getText().toString().length() == 0) {
            showSnackBar(mBinding.mainLayout, "Please Enter OTP", Snackbar.LENGTH_LONG);
            value = false;
        } else if (mBinding.verificationLayout.edtFive.getText().toString().length() == 0) {
            showSnackBar(mBinding.mainLayout, "Please Enter OTP", Snackbar.LENGTH_LONG);
            value = false;
        }
        return value;
    }



 /*   @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        hideProgressBar();
        Snackbar.make(getActivity().findViewById(android.R.id.content), getString(R.string.common_error_msg), Snackbar.LENGTH_LONG)
                .show();
    }*/


    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.edtOne:
                    if (text.length() == 1)
                        mBinding.verificationLayout.edtTwo.requestFocus();
                    break;
                case R.id.edtTwo:
                    if (text.length() == 1)
                        mBinding.verificationLayout.edtThree.requestFocus();
                    break;
                case R.id.edtThree:
                    if (text.length() == 1)
                        mBinding.verificationLayout.edtFour.requestFocus();
                    break;
                case R.id.edtFour:
                    if (text.length() == 1)
                        mBinding.verificationLayout.edtFive.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (i == KeyEvent.KEYCODE_DEL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                switch (view.getId()) {
                    case R.id.edtOne:
                        break;
                    case R.id.edtTwo:
                        mBinding.verificationLayout.edtOne.requestFocus();
                        break;
                    case R.id.edtThree:
                        mBinding.verificationLayout.edtTwo.requestFocus();
                        break;
                    case R.id.edtFour:
                        mBinding.verificationLayout.edtThree.requestFocus();
                        break;
                    case R.id.edtFive:
                        mBinding.verificationLayout.edtFour.requestFocus();
                        break;
                }
            }
            return false;
        }
    };

}
