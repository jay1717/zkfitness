package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientProfileNutritionListAdapter;
import com.fitzoh.app.databinding.ClientAssignedNutritionBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.EditNutritionActivity;
import com.fitzoh.app.ui.dialog.AddDietDialog;
import com.fitzoh.app.ui.dialog.AssignClientDialog;
import com.fitzoh.app.ui.dialog.ClientProfileAssignDialog;
import com.fitzoh.app.ui.dialog.StartWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.DietList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignDiet;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.unAssignDiet;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClientProfilAssignedNutritionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClientProfilAssignedNutritionFragment extends BaseFragment implements SingleCallback, AddDietDialog.DialogClickListener, ClientProfileNutritionListAdapter.UnAssign, AssignClientDialog.DaySelection, SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ClientAssignedNutritionBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<DietListModel.DataBean> dietList;
    List<DietListModel.DataBean> assignDietList = new ArrayList<>();
    ClientProfileNutritionListAdapter nutritionListAdapter;
    // ClientListModel.DataBean data;
    private int clientId;
    private int is_assigned;
    private int diet_plan_id = 0;

    public ClientProfilAssignedNutritionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentWorkout.
     */
    // TODO: Rename and change types and number of parameters
    public static ClientProfilAssignedNutritionFragment newInstance(int id, int createdId) {
        ClientProfilAssignedNutritionFragment fragment = new ClientProfilAssignedNutritionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, id);
        args.putInt(ARG_PARAM2, createdId);
        fragment.setArguments(args);
       /* clientId = id;
        is_assigned = createdId;*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clientId = getArguments().getInt(ARG_PARAM1, 0);
            is_assigned = getArguments().getInt(ARG_PARAM2, 0);
            //  data = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.nutrition_plans));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.client_assigned_nutrition, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.layoutNutrition.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutNutrition.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        dietList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutNutrition.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        nutritionListAdapter = new ClientProfileNutritionListAdapter(getActivity(), dietList, is_assigned, this);
        mBinding.layoutNutrition.recyclerView.setAdapter(nutritionListAdapter);
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(mActivity))
                callNutritionApi();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        Utils.setAddFabBackground(mActivity, mBinding.layoutNutrition.fab);
        mBinding.layoutNutrition.fab.show();
        mBinding.layoutNutrition.fab.setOnClickListener(v -> {
            getAllDietPlan();
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isOnline(mActivity))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    private void callNutritionApi() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientDietList(clientId, 1)
                , getCompositeDisposable(), DietList, this);
    }

    /*private void callWorkoutDelete(int workoutId) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                , getCompositeDisposable(), deleteWorkout, this);
    }*/


    @Override
    public void edit(int id, String name) {
        session.setStringDataByKey(CLIENT_ID, "" + clientId);
        startActivity(new Intent(getActivity(), EditNutritionActivity.class).putExtra("dietId", id).putExtra("dietName", name));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (Utils.isOnline(mContext))
                callNutritionApi();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            int position = data.getIntExtra("data", 0);
            diet_plan_id = assignDietList.get(position).getId();
            AssignClientDialog dialogNote = new AssignClientDialog(this, clientId);
            //dialogNote.setListener(this);
            dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AssignClientDialog.class.getSimpleName());
            dialogNote.setCancelable(false);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case DietList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                dietList = new ArrayList<>();
                DietListModel dietListModel = (DietListModel) o;
                if (dietListModel.getStatus() == AppConstants.SUCCESS) {

                    if (dietListModel.getData() != null) {
                        dietList.addAll(dietListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        List<DietListModel.DataBean> dietList = new ArrayList<>();
                        for (DietListModel.DataBean dataBean : this.dietList) {
                            if (dataBean.getIs_assigned() == is_assigned) {
                                dietList.add(dataBean);
                            }
                        }
                        if (dietList.size() == 0) {
                            mBinding.layoutNutrition.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutNutrition.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutNutrition.imgNoData.setVisibility(View.GONE);
                            mBinding.layoutNutrition.recyclerView.setVisibility(View.VISIBLE);
                            nutritionListAdapter = new ClientProfileNutritionListAdapter(getActivity(), dietList, is_assigned, this);
                            mBinding.layoutNutrition.recyclerView.setAdapter(nutritionListAdapter);
                        }


                    } else
                        showSnackBar(mBinding.linear, dietListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case deleteDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deleteDiet = (CommonApiResponse) o;
                if (deleteDiet.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(mActivity))
                        callNutritionApi();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteDiet.getMessage(), Snackbar.LENGTH_LONG);

                break;

            case unAssignDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callNutritionApi();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DietListModel model = (DietListModel) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null && model.getData().size() > 0) {
                    assignDietList = model.getData();
                    ArrayList<String> workout_names = new ArrayList<>();
                    for (int i = 0; i < assignDietList.size(); i++) {
                        workout_names.add(assignDietList.get(i).getName());
                    }
                    ClientProfileAssignDialog dialog = new ClientProfileAssignDialog();
                    Bundle bundle = new Bundle();
                    bundle.putStringArrayList("data", workout_names);
                    dialog.setArguments(bundle);
                    dialog.setTargetFragment(this, 100);
                    dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), StartWorkoutDialog.class.getSimpleName());
                    dialog.setCancelable(false);
                } else {
                    showSnackBar(mBinding.linear, "", Snackbar.LENGTH_LONG);
                }
                break;
            case assignDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS) {
                    callNutritionApi();
                } else {
                    showSnackBar(mBinding.linear, response.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }


    private void getAllDietPlan() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getDietList()
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void setClick() {
        if (Utils.isOnline(mActivity))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void remove(DietListModel.DataBean dataBean) {
        if (dataBean != null) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", clientId);
                jsonObject.put("is_assigned", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();

            }
            JSONObject jsonObjectUnassign = new JSONObject();
            try {
                jsonObjectUnassign.put("client_id", clientId);
                jsonObjectUnassign.put("client_ids", jsonArray);
                jsonObjectUnassign.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
                jsonObjectUnassign.put("diet_plan_id", dataBean.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObjectUnassign.toString());

            if (Utils.isOnline(mActivity)) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).unAssignDiet(requestBody)
                        , getCompositeDisposable(), unAssignDiet, this);
            } else {
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        }
    }

    @Override
    public void setClick(ArrayList<String> days, int clientId) {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject object = new JSONObject();
            try {
                object.put("user_id", clientId);
                JSONArray list = new JSONArray(days);
                object.put("day", list);
                object.put("is_assigned", 1);
                jsonArray.put(object);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            jsonObject.put("client_ids", jsonArray);
            jsonObject.put("client_group_ids", new JSONArray());
            jsonObject.put("diet_plan_id", diet_plan_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignDiet(requestBody)
                    , getCompositeDisposable(), assignDiet, this);

        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(mActivity))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }

        mBinding.layoutNutrition.swipeContainer.setRefreshing(false);
    }
}
