package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ExerciseInstructionAdapter;
import com.fitzoh.app.databinding.FragmentExerciseDetailBinding;
import com.fitzoh.app.model.ExerciseDetailData;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentExerciseDetail#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentExerciseDetail extends BaseFragment implements SingleCallback, YouTubePlayer.OnInitializedListener {
    FragmentExerciseDetailBinding mBinding;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    //    private String videoUrl = "https://r2---sn-un57en7s.googlevideo.com/videoplayback?dur=244.204&requiressl=yes&pl=21&ratebypass=yes&c=WEB&expire=1536413439&source=youtube&id=o-AF9fzqndbjnWFmGj4Du7ESKC6WYL-aCSk8-jFcbeYM8R&sparams=dur,ei,expire,id,ip,ipbits,ipbypass,itag,lmt,mime,mip,mm,mn,ms,mv,pl,ratebypass,requiressl,source&fvip=2&lmt=1522960451860848&signature=6E3E46202DC2D4818540BAB6963A1CAD43835DF4.65A93F32FA2BCE032FC8E81307483DCF2D4FB1A9&ei=n3qTW5qVHcPj8wSemJO4Dw&ipbits=0&mime=video%2Fmp4&ip=54.167.143.187&key=cms1&itag=22&rm=sn-p5qkl7l&fexp=23755740&req_id=4ff8222fddcca3ee&ipbypass=yes&mip=106.201.238.169&cm2rm=sn-ci5gup-qxas7k,sn-cvhly7l&redirect_counter=3&cms_redirect=yes&mm=34&mn=sn-un57en7s&ms=ltu&mt=1536391765&mv=m";
//    private String videoUrl = "https://www.youtube.com/watch?v=XVnP_ROKNKk";
    String userId, userAccessToken;
    String DEVELOPER_KEY = "AIzaSyAKxCe1rF5MHnLeIEZl57rzigG_b38qzkM";
    String url = "https://www.youtube.com/watch?v=dOoNRzddLC0";

    public FragmentExerciseDetail() {
        // Required empty public constructor
    }


    public static FragmentExerciseDetail newInstance() {
        return new FragmentExerciseDetail();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //   setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.exercise_detail_title));

        mBinding.youtubeView.initialize(DEVELOPER_KEY, this);
        getExerciseDetail();
    }

    private void getExerciseDetail() {
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

//        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).exerciseDetail(2)
                , getCompositeDisposable(), single, this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_exercise_detail, container, false);
        mBinding.layoutExerciseDetail.recyclerViewInstructions.setAdapter(new ExerciseInstructionAdapter());
//        mBinding.layoutExerciseDetail.videoView.setVideoURI(Uri.parse(videoUrl));
        mBinding.toolbar.tvTitle.setText(getString(R.string.exercise_detail_title));
//        mBinding.toolbar.imgBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.toolbar.imgBack.setOnClickListener(v -> getActivity().onBackPressed());
//        mBinding.layoutExerciseDetail.videoView.start();
        return mBinding.getRoot();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
//                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ExerciseDetailData exerciseDetailData = (ExerciseDetailData) o;
                if (exerciseDetailData != null && exerciseDetailData.getStatus() == AppConstants.SUCCESS && exerciseDetailData.getData() != null) {
                    mBinding.setItem(exerciseDetailData.getData());
                    mBinding.layoutExerciseDetail.setItem(exerciseDetailData.getData());

                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
//        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {

            // loadVideo() will auto play video
            // Use cueVideo() method, if you don't want to play it automatically
            String videoId = "";
            try {
                videoId = extractYoutubeId(url);
                Log.e("videoId", videoId);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if (videoId.equals("")) {
                mBinding.txtNoVideo.setVisibility(View.VISIBLE);
                Toast.makeText(mContext, "Invalid URL", Toast.LENGTH_SHORT).show();
            } else {
//                player.loadVideo(videoId);
                mBinding.txtNoVideo.setVisibility(View.VISIBLE);
                youTubePlayer.cueVideo(videoId);

            }
            // Hiding player controls
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(mActivity, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(mActivity, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

}
