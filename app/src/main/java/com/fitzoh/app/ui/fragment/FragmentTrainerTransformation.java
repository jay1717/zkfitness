package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainerProfileTransformationAdapter;
import com.fitzoh.app.databinding.FragmentTrainerTransformationBinding;
import com.fitzoh.app.model.TransformationListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.TransformationAddActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentTrainerTransformation extends BaseFragment implements SingleCallback {


    FragmentTrainerTransformationBinding mBinding;
    List<TransformationListModel.DataBean> transformationList;
    int trainerId = 0;
    String userId, userAccessToken;
    boolean isFromNav = false;
    TrainerProfileTransformationAdapter trainerProfileTransformationAdapter;


    public FragmentTrainerTransformation() {
        // Required empty public constructor
    }

    public static FragmentTrainerTransformation newInstance(int trainerId, boolean isFromNav) {
        FragmentTrainerTransformation fragment = new FragmentTrainerTransformation();
        Bundle bundle = new Bundle();
        bundle.putInt("data", trainerId);
        bundle.putBoolean("isFromNav", isFromNav);
        Log.e("trainerId: ", "" + trainerId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            trainerId = getArguments().getInt("data");
        } else if (getArguments() != null && getArguments().containsKey("isFromNav")) {
            isFromNav = getArguments().getBoolean("isFromNav");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_trainer_transformation, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (isFromNav)
            mBinding.fab.setVisibility(View.VISIBLE);
        else
            mBinding.fab.setVisibility(View.GONE);

        mBinding.fab.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), TransformationAddActivity.class);
            intent.putExtra("IS_FROM_REGISTER", false);
            startActivity(intent);
        });
        getTransformationData();
        //mBinding.recyclerView.setAdapter(new TrainerProfileTransformationAdapter(getActivity()));

        return mBinding.getRoot();
    }

    private void getTransformationData() {
        if (Utils.isOnline(getActivity())) {
            if (getArguments() != null && getArguments().containsKey("data")) {
                trainerId = getArguments().getInt("data");
            }
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getTransformationDetails(trainerId)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.getTransformation, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getTransformation:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                transformationList = new ArrayList<>();
                TransformationListModel transformationListModel = (TransformationListModel) o;
                if (transformationListModel.getStatus() == AppConstants.SUCCESS) {

                    if (transformationListModel != null) {
                        transformationList.addAll(transformationListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (transformationList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            trainerProfileTransformationAdapter = new TrainerProfileTransformationAdapter(getActivity(), transformationList, null);
                            mBinding.recyclerView.setAdapter(trainerProfileTransformationAdapter);
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, transformationListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
