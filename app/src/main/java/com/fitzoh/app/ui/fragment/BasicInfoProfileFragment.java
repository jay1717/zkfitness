package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.app.DatePickerDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.system.ErrnoException;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentBasicInfoProfileBinding;
import com.fitzoh.app.model.EditBasicTrainerModel;
import com.fitzoh.app.model.TrainerProfileModel;
import com.fitzoh.app.model.UserDataModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateBasicInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicInfoProfileFragment extends BaseFragment implements View.OnClickListener, SingleCallback {


    // FragmentBasicInfoProfileBinding mBinding;

    FragmentBasicInfoProfileBinding mBinding;
    String userId, userAccessToken;
    boolean isEditable = false;
    MenuItem menuItem;
    TrainerProfileModel trainerProfileModel = null;

    private static final int RECORD_REQUEST_CODE = 101;
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImage;

    public BasicInfoProfileFragment() {
        // Required empty public constructor
    }

    public static BasicInfoProfileFragment newInstance() {
        BasicInfoProfileFragment fragment = new BasicInfoProfileFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_basic_info_profile, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.str_basic_profile));
        if (getArguments() != null && getArguments().containsKey("TRAINER_MODEL")) {
            trainerProfileModel = (TrainerProfileModel) getArguments().getSerializable("TRAINER_MODEL");
        }

        setThemeWidget();
        setHasOptionsMenu(true);
        setClickEvents();
        setData();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        return mBinding.getRoot();
    }

    private void setData() {
        mBinding.register.edtEmail.setEnabled(false);
        mBinding.register.edtName.setEnabled(false);
        mBinding.register.edtDate.setEnabled(false);
        mBinding.register.btnDateDropdown.setClickable(false);
        mBinding.register.countryCode.setCcpClickable(false);
        mBinding.register.imgPickPhoto.setClickable(false);
        mBinding.register.btnRegister.setVisibility(View.GONE);
        mBinding.register.edtName.setText(session.getAuthorizedUser(mContext).getName().toString());
        mBinding.register.edtEmail.setText(session.getAuthorizedUser(mContext).getEmail());
        mBinding.register.edtMobile.setText(session.getAuthorizedUser(mContext).getMobile_number());
        mBinding.register.edtDate.setText(session.getAuthorizedUser(mContext).getBirthDate().toString());
        if (session.getAuthorizedUser(mContext).getCountry_code() != null && !session.getAuthorizedUser(mContext).getCountry_code().isEmpty())
            mBinding.register.countryCode.setCountryForPhoneCode(Integer.parseInt(session.getAuthorizedUser(mContext).getCountry_code()));
        if (session.getAuthorizedUser(mContext).getGender().equalsIgnoreCase("m")) {
            mBinding.register.radioMale.setChecked(true);
        } else {
            mBinding.register.radioFemale.setChecked(true);
        }
        if (!session.getAuthorizedUser(mContext).getBirthDate().toString().isEmpty()) {
            String date = session.getAuthorizedUser(mContext).getBirthDate().toString();
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = null;
            try {
                newDate = spf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            spf = new SimpleDateFormat("dd/MM/yyyy");
            date = spf.format(newDate);
            mBinding.register.edtDate.setText(date);
        }

        if (session.getAuthorizedUser(mContext).getPhoto() != null) {
            if (!session.getAuthorizedUser(mContext).getPhoto().equals("")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_user_placeholder)
                        .error(R.drawable.ic_user_placeholder);
                Glide.with(mContext)
                        .load(session.getAuthorizedUser(mContext).getPhoto())
                        .apply(options)
                        .into(mBinding.register.imgLogo);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit_set, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.action_edit), R.drawable.ic_edit);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        menuItem = item;
        switch (item.getItemId()) {
            case R.id.action_edit:
                isEditable = !isEditable;
                setEditTextEnable(isEditable);
                break;
        }
        return true;
    }

    private void setEditTextEnable(boolean isEditable) {
        mBinding.register.edtMobile.setEnabled(isEditable);
        mBinding.register.edtDate.setEnabled(isEditable);
        mBinding.register.edtDate.setClickable(isEditable);
        mBinding.register.countryCode.setCcpClickable(isEditable);
        mBinding.register.imgPickPhoto.setClickable(isEditable);
        mBinding.register.edtName.setEnabled(isEditable);
        mBinding.register.radioMale.setEnabled(isEditable);
        mBinding.register.radioFemale.setEnabled(isEditable);
        mBinding.register.countryCode.setEnabled(isEditable);

        mBinding.register.radiogroupGender.setActivated(isEditable);
        if (isEditable) {
            mBinding.register.btnRegister.setVisibility(View.VISIBLE);
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_cancel));
        } else {
            mBinding.register.btnRegister.setVisibility(View.GONE);
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_edit));

        }
    }

    public boolean validation() {
        boolean value = true;
        if (mBinding.register.edtName.getText().toString().length() == 0) {
            setEdittextError(mBinding.register.txtErrorName, getString(R.string.empty_userfirstname));
            value = false;
        }

        if (mBinding.register.edtEmail.getText().toString().length() == 0) {
            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.empty_username_register));
            value = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.register.edtEmail.getText().toString()).matches()) {
            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.invalid_email));
            value = false;
        }
        if (mBinding.register.edtMobile.getText().toString().length() > 0) {
            if (mBinding.register.edtMobile.getText().toString().length() != 10) {
                setEdittextError(mBinding.register.txtErrorMobileNo, getString(R.string.invalid_phone));
                value = false;
            } else {
                mBinding.register.txtErrorMobileNo.setText("");
                mBinding.register.txtErrorMobileNo.setVisibility(View.GONE);
            }

        }

        if (mBinding.register.edtDate.getText().toString().length() == 0) {
            setEdittextError(mBinding.register.txtErrorDate, getString(R.string.emapty_date));
            value = false;
        }

        return value;
    }

    private void setThemeWidget() {
        Utils.getShapeGradient(mActivity, mBinding.register.btnRegister);
        Utils.setTextViewStartImage(mActivity, mBinding.register.edtEmail, R.drawable.ic_email, true);
        Utils.setTextViewStartImage(mActivity, mBinding.register.edtName, R.drawable.ic_user, true);
        Utils.setTextViewStartImage(mActivity, mBinding.register.edtDate, R.drawable.ic_calendar, true);
        Utils.setSpinnerArrow(mActivity, new ImageView[]{mBinding.register.btnDateDropdown});
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioMale);
        Utils.setRadioButtonSelectors(mActivity, mBinding.register.radioFemale);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.setImageBackground(mActivity, mBinding.register.imgPickPhoto, R.drawable.ic_plus_icon);
    }


    private void setClickEvents() {
        mBinding.register.countryCode.setOnClickListener(this);
        mBinding.register.btnRegister.setOnClickListener(this);
        mBinding.register.imgPickPhoto.setOnClickListener(this);
        mBinding.register.edtDate.setOnClickListener(this);
        mBinding.register.edtDate.addTextChangedListener(new BasicInfoProfileFragment.MyTaskWatcher(mBinding.register.edtDate));
        mBinding.register.edtName.addTextChangedListener(new BasicInfoProfileFragment.MyTaskWatcher(mBinding.register.edtName));
        mBinding.register.edtEmail.addTextChangedListener(new BasicInfoProfileFragment.MyTaskWatcher(mBinding.register.edtEmail));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_pick_photo:
                imgPick();
                break;

            case R.id.btnRegister:
                if (Utils.isOnline(getContext())) {
                    if (validation()) {
                        callAPI();

                        //   startActivity(new Intent(getActivity(), CreateUserActivity.class));

                        /*    if (mBinding.register.chkServiceProvider.isChecked()) {
                         *//* session.setBooleanDataByKey(KEY_IS_LOGIN, true);
                            session.setBooleanDataByKey(KEY_IS_SP, true);*//*
                            JSONObject object = new JSONObject();
                            try {
                                object.put("FirstName", mBinding.register.edtUserFirstname.getText().toString());
                                object.put("LastName", mBinding.register.edtUserLastname.getText().toString());
                                object.put("Email", mBinding.register.edtEmail.getText().toString());
                                object.put("PhoneNo", mBinding.register.edtPhone.getText().toString());
                                object.put("Password", mBinding.register.edtPassword.getText().toString());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            ((LoginActivity) getActivity()).pushFragments(AppConstants.LOGIN_KEY, fragmentSeriviceProviderFeild, true, true);
                        } else {
//                            showProgressBar().show();
//                            callApi();
                        }*/
                        mBinding.register.txtErrorEmail.setVisibility(View.GONE);
                        mBinding.register.txtErrorDate.setVisibility(View.GONE);
                        mBinding.register.txtErrorName.setVisibility(View.GONE);
                    }
                } else {
                    Snackbar.make(mBinding.linear, R.string.no_internet_connection, Snackbar.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_date_dropdown:

              /*  final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);



                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
                                Date newDate = null;
                                try {
                                    newDate = spf.parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                spf = new SimpleDateFormat("dd MMM yyyy");
                                date = spf.format(newDate);
                                if(date!=null)
                                    mBinding.register.edtDate.setText(date);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();*/
                showDatePickerDialog();
                break;
            case R.id.edtDate:
                showDatePickerDialog();
                break;


        }
    }


    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        getActivity().startActivityForResult(getPickImageChooserIntent(), 200);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    mBinding.register.imgLogo.setImageURI(mImageUri);
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }

    }

    private void callAPI() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), mBinding.register.edtName.getText().toString());


        String DOB = "";
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateFormatDOB = new SimpleDateFormat("yyyy/MM/dd");
        Date dateNew = null;
        try {
            dateNew = dateFormat.parse(mBinding.register.edtDate.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DOB = dateFormatDOB.format(dateNew);

        RequestBody date = RequestBody.create(MediaType.parse("text/plain"), DOB);
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), mBinding.register.edtMobile.getText().toString());
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "m" : "f");
        RequestBody country_code = RequestBody.create(MediaType.parse("text/plain"), mBinding.register.countryCode.getSelectedCountryCode());

        if (fileImage != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            MultipartBody.Part photo = MultipartBody.Part.createFormData("photo", fileImage.getName(), reqFile);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addBasicTrainerProfile(
                    name,
                    gender,
                    mobile,
                    country_code,
                    date,
                    photo), getCompositeDisposable(), updateBasicInfo, this);
        } else {
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addBasicTrainerProfileWithoutImage(
                    name,
                    gender,
                    mobile,
                    country_code,
                    date), getCompositeDisposable(), updateBasicInfo, this);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {

            case updateBasicInfo:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);

//                Toast.makeText(getActivity(), "success", Toast.LENGTH_LONG).show();
                EditBasicTrainerModel editBasicTrainerModel = (EditBasicTrainerModel) o;
//                if (registerModel.getStatus() == AppConstants.SUCCESS) {
                if (editBasicTrainerModel != null && editBasicTrainerModel.getStatus() == AppConstants.SUCCESS) {
                    UserDataModel userDataModel = session.getAuthorizedUser(mContext);
                    userDataModel.setName(mBinding.register.edtName.getText().toString().trim());
                    userDataModel.setPhoto(editBasicTrainerModel.getData().getPhoto());
                  /*  userDataModel.setMobile_number(mBinding.register.edtMobile.getText().toString());
                    userDataModel.setBirthDate(mBinding.register.edtDate.getText().toString());
                    userDataModel.setCountry_code(mBinding.register.countryCode.getSelectedCountryCode());
                    userDataModel.setGender(mBinding.register.radiogroupGender.getCheckedRadioButtonId() == R.id.radio_male ? "m" : "f");*/
                    session.saveAuthorizedUser(mContext, userDataModel);
                    /*setData();
                    setEditTextEnable(false);*/
                    getActivity().finish();
                   /* Intent intent = new Intent(getActivity(), VarificationActivity.class);
                    if (isSocialMedia == 0) {
                        intent.putExtra("id", registerModel.getData().getId());
                    } else
                        intent.putExtra("id", id);
                    intent.putExtra("email", mBinding.register.edtEmail.getText().toString());
                    intent.putExtra("isRegister", "true");
                    startActivity(intent);*/
                    //((LoginActivity) mActivity).pushFragments(AppConstants.LOGIN_KEY, FragmentVerificationOTP.newInstance(registerModel.getData().getId(), mBinding.register.edtEmail.getText().toString()), true, true);
                } else {
                    showSnackBar(mBinding.linear, editBasicTrainerModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, "onFailure", Snackbar.LENGTH_LONG);
    }

    private class MyTaskWatcher implements TextWatcher {
        private View view;

        MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.edtEmail:
                    if (mBinding.register.edtEmail.hasFocus()) {
                        if (mBinding.register.edtEmail.getText().toString().length() == 0) {
                            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.empty_username));
                        } else if (mBinding.register.edtEmail.getText().toString().matches("[0-9]+")) {
                            if (mBinding.register.edtEmail.getText().toString().length() != 10) {
                                setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.invalid_phone));
                            } else {
                                mBinding.register.txtErrorEmail.setText("");
                                mBinding.register.txtErrorEmail.setVisibility(View.GONE);
                            }
                        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.register.edtEmail.getText().toString()).matches()) {
                            setEdittextError(mBinding.register.txtErrorEmail, getString(R.string.invalid_email));
                        } else {
                            mBinding.register.txtErrorEmail.setText("");
                            mBinding.register.txtErrorEmail.setVisibility(View.GONE);
                        }
                    }
                    break;
                case R.id.edtDate:
                    if (mBinding.register.edtDate.hasFocus()) {
                        if (mBinding.register.edtDate.getText().toString().length() == 0) {
                            setEdittextError(mBinding.register.txtErrorDate, getString(R.string.emapty_date));
                        } else {
                            mBinding.register.txtErrorDate.setText("");
                            mBinding.register.txtErrorDate.setVisibility(View.GONE);
                        }
                    }
                    break;
                case R.id.edtName:
                    if (mBinding.register.edtName.hasFocus()) {
                        if (mBinding.register.edtName.getText().toString().length() == 0) {
                            setEdittextError(mBinding.register.txtErrorName, getString(R.string.empty_userfirstname));
                        } else {
                            mBinding.register.txtErrorName.setText("");
                            mBinding.register.txtErrorName.setVisibility(View.GONE);
                        }
                    }
                    break;

            }

        }

    }

    private void showDatePickerDialog() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                (view, year, monthOfYear, dayOfMonth) -> {
                    c.set(Calendar.YEAR, year);
                    c.set(Calendar.MONTH, monthOfYear);
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    mBinding.register.edtDate.setText(dateFormat.format(c.getTime()));
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }
}
