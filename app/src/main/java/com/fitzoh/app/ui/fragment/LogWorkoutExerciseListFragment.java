package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.edit;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.LogWorkoutExerciseAdapter;
import com.fitzoh.app.adapter.LogWorkoutMainAdapter;
import com.fitzoh.app.databinding.FragmentLogWorkoutExerciseListBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.LogWorkoutDataModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.WorkoutFinishActivity;
import com.fitzoh.app.ui.dialog.AddNoteDialog;
import com.fitzoh.app.ui.dialog.ViewWorkoutNoteDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;


public class LogWorkoutExerciseListFragment extends BaseFragment implements SingleCallback, LogWorkoutMainAdapter.StartWorkoutListener, SwipeRefreshLayout.OnRefreshListener {
    FragmentLogWorkoutExerciseListBinding mBinding;
    String userId, userAccessToken;
    private List<List<LogWorkoutDataModel.DataBean>> listDatas = new ArrayList<>();
    private int workout_id = 0, training_program_id = 0, weekday_id = 0;
    private String workout_name = "";
    boolean is_finish_flag;
    boolean isFromTP = false;
    private boolean is_am_workout = false, isSetEditable = false;
    private List<String> channels;
    int is_show_log;

    public LogWorkoutExerciseListFragment() {
    }


    public static LogWorkoutExerciseListFragment newInstance(Bundle bundle) {
        LogWorkoutExerciseListFragment fragment = new LogWorkoutExerciseListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            training_program_id = getArguments().getInt("training_program_id", 0);
            workout_id = getArguments().getInt("workout_id", 0);
            workout_name = getArguments().getString("workout_name", "");
            is_am_workout = getArguments().getBoolean("is_am_workout", false);
            isSetEditable = getArguments().getBoolean("isSetEditable", false);
            weekday_id = getArguments().getInt("weekday_id", 0);
            if (getArguments().containsKey("isFromTP"))
                isFromTP = getArguments().getBoolean("isFromTP", false);
            is_show_log = getArguments().getInt("is_show_log", 0);
        }
        if (isFromTP) {
            initLiveClientSocket();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, workout_name);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_log_workout_exercise_list, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        Utils.getShapeGradient(mActivity, mBinding.btnSave);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.toolbar.toolbar.setNavigationOnClickListener(view -> callSaveAPIFromWorkout(false));
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.btnSave.setOnClickListener(view -> {
            if (listDatas != null && listDatas.size() > 0) {
                if (getArguments() != null && getArguments().containsKey("isFromWorkout") && getArguments().getBoolean("isFromWorkout", false))
                    callSaveAPIFromWorkout(true);
                else
                    callSaveAPI(true);

            } else {
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    mBinding.swipeContainer.setRefreshing(false);
                } else {
                    // Scrolling down
                }
            }
        });


        setHasOptionsMenu(true);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.btnSave.setVisibility(isSetEditable ? View.VISIBLE : View.GONE);
        mBinding.recyclerView.setAdapter(new LogWorkoutExerciseAdapter(mActivity, new ArrayList<>(), workout_id, this, isSetEditable));
        callExerciseList();


        return mBinding.getRoot();
    }

    public void callSaveAPI(boolean is_finish) {
        is_finish_flag = is_finish;
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < listDatas.size(); i++) {
                JSONArray array = new JSONArray();
                for (int j = 0; j < listDatas.get(i).size(); j++) {
                    JSONObject objectMain = new JSONObject();
                    objectMain.put("id", listDatas.get(i).get(j).getId());
                    objectMain.put("exercise_name", listDatas.get(i).get(j).getExercise_name());
                    objectMain.put("image", listDatas.get(i).get(j).getImage());
                    objectMain.put("no_of_sets", listDatas.get(i).get(j).getNo_of_sets());
                    objectMain.put("rating", listDatas.get(i).get(j).getRating());
                    if (!listDatas.get(i).get(j).getTimeStamp().equalsIgnoreCase(""))
                        objectMain.put("timestamp", listDatas.get(i).get(j).getTimeStamp());
                    objectMain.put("workout_id", workout_id);
                    objectMain.put("training_program_id", training_program_id);
                    JSONArray sets = new JSONArray();
                    for (int k = 0; k < listDatas.get(i).get(j).getSets().size(); k++) {
                        JSONArray arraySet = new JSONArray();
                        for (int l = 0; l < listDatas.get(i).get(j).getSets().get(k).size(); l++) {
                            JSONObject objectSet = new JSONObject();
                            objectSet.put("setno", listDatas.get(i).get(j).getSets().get(k).get(l).getSetno());
                            objectSet.put("id", listDatas.get(i).get(j).getSets().get(k).get(l).getId());
                            objectSet.put("parameter_id", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_id());
                            objectSet.put("parameter_name", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_name());
                            objectSet.put("value", listDatas.get(i).get(j).getSets().get(k).get(l).getValue());
                            objectSet.put("set_value", listDatas.get(i).get(j).getSets().get(k).get(l).getSet_value());
                            objectSet.put("weight_unit", listDatas.get(i).get(j).getSets().get(k).get(l).getWeight_unit());
                            arraySet.put(objectSet);
                        }
                        sets.put(arraySet);
                    }
                    objectMain.put("sets", sets);
                    array.put(objectMain);
                }
                jsonArray.put(array);
            }
            jsonObject.put("data", jsonArray);
            jsonObject.put("is_finish", 1);
        } catch (JSONException e) {
            e.printStackTrace();
            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
            disableScreen(false);
        }
        initLiveClientSocket(false);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveLogWorkoutDetails(requestBody)
                , getCompositeDisposable(), single, this);
    }

    public void callSaveAPIEdit(boolean is_finish) {
        is_finish_flag = is_finish;
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < listDatas.size(); i++) {
                JSONArray array = new JSONArray();
                for (int j = 0; j < listDatas.get(i).size(); j++) {
                    JSONObject objectMain = new JSONObject();
                    objectMain.put("id", listDatas.get(i).get(j).getId());
                    objectMain.put("exercise_name", listDatas.get(i).get(j).getExercise_name());
                    objectMain.put("image", listDatas.get(i).get(j).getImage());
                    objectMain.put("no_of_sets", listDatas.get(i).get(j).getNo_of_sets());
                    objectMain.put("rating", listDatas.get(i).get(j).getRating());
                    if (!listDatas.get(i).get(j).getTimeStamp().equalsIgnoreCase(""))
                        objectMain.put("timestamp", listDatas.get(i).get(j).getTimeStamp());
                    objectMain.put("workout_id", workout_id);
                    objectMain.put("training_program_id", training_program_id);
                    JSONArray sets = new JSONArray();
                    for (int k = 0; k < listDatas.get(i).get(j).getSets().size(); k++) {
                        JSONArray arraySet = new JSONArray();
                        for (int l = 0; l < listDatas.get(i).get(j).getSets().get(k).size(); l++) {
                            JSONObject objectSet = new JSONObject();
                            objectSet.put("setno", listDatas.get(i).get(j).getSets().get(k).get(l).getSetno());
                            objectSet.put("id", listDatas.get(i).get(j).getSets().get(k).get(l).getId());
                            objectSet.put("parameter_id", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_id());
                            objectSet.put("parameter_name", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_name());
                            objectSet.put("value", listDatas.get(i).get(j).getSets().get(k).get(l).getValue());
                            objectSet.put("set_value", listDatas.get(i).get(j).getSets().get(k).get(l).getSet_value());
                            objectSet.put("weight_unit", listDatas.get(i).get(j).getSets().get(k).get(l).getWeight_unit());
                            arraySet.put(objectSet);
                        }
                        sets.put(arraySet);
                    }
                    objectMain.put("sets", sets);
                    array.put(objectMain);
                }
                jsonArray.put(array);
            }
            jsonObject.put("data", jsonArray);
            jsonObject.put("is_finish", 1);
        } catch (JSONException e) {
            e.printStackTrace();
            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
            disableScreen(false);
        }
        initLiveClientSocket(false);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveLogWorkoutDetails(requestBody)
                , getCompositeDisposable(), edit, this);
    }

    public void callSaveAPIFromWorkoutEdit(boolean is_finish) {
        is_finish_flag = is_finish;
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonObject.put("workout_id", workout_id);
            jsonObject.put("is_finish", is_finish);
            for (int i = 0; i < listDatas.size(); i++) {
                JSONArray array = new JSONArray();
                for (int j = 0; j < listDatas.get(i).size(); j++) {
                    JSONObject objectMain = new JSONObject();
                    objectMain.put("id", listDatas.get(i).get(j).getId());
                    objectMain.put("exercise_name", listDatas.get(i).get(j).getExercise_name());
                    objectMain.put("image", listDatas.get(i).get(j).getImage());
                    objectMain.put("no_of_sets", listDatas.get(i).get(j).getNo_of_sets());
                    objectMain.put("rating", listDatas.get(i).get(j).getRating());
                    if (!listDatas.get(i).get(j).getTimeStamp().equalsIgnoreCase(""))
                        objectMain.put("timestamp", listDatas.get(i).get(j).getTimeStamp());
                    objectMain.put("workout_id", workout_id);
                    JSONArray sets = new JSONArray();
                    for (int k = 0; k < listDatas.get(i).get(j).getSets().size(); k++) {
                        JSONArray arraySet = new JSONArray();
                        for (int l = 0; l < listDatas.get(i).get(j).getSets().get(k).size(); l++) {
                            JSONObject objectSet = new JSONObject();
                            objectSet.put("setno", listDatas.get(i).get(j).getSets().get(k).get(l).getSetno());
                            objectSet.put("id", listDatas.get(i).get(j).getSets().get(k).get(l).getId());
                            objectSet.put("parameter_id", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_id());
                            objectSet.put("parameter_name", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_name());
                            objectSet.put("value", listDatas.get(i).get(j).getSets().get(k).get(l).getValue());
                            objectSet.put("set_value", listDatas.get(i).get(j).getSets().get(k).get(l).getSet_value());
                            objectSet.put("weight_unit", listDatas.get(i).get(j).getSets().get(k).get(l).getWeight_unit());
                            arraySet.put(objectSet);
                        }
                        sets.put(arraySet);
                    }
                    objectMain.put("sets", sets);
                    array.put(objectMain);
                }
                jsonArray.put(array);
            }
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
            disableScreen(false);
        }
        if (is_finish) initLiveClientSocket(false);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Log.e("jSOn", jsonObject.toString());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addClientWorkoutTracking(requestBody)
                , getCompositeDisposable(), edit, this);
    }


    @Override
    public boolean onBackPressed() {
        callSaveAPIFromWorkout(false);
        return super.onBackPressed();

    }

    public void callSaveAPIFromWorkout(boolean is_finish) {
        is_finish_flag = is_finish;
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            jsonObject.put("workout_id", workout_id);
            jsonObject.put("is_finish", is_finish);
            for (int i = 0; i < listDatas.size(); i++) {
                JSONArray array = new JSONArray();
                for (int j = 0; j < listDatas.get(i).size(); j++) {
                    JSONObject objectMain = new JSONObject();
                    objectMain.put("id", listDatas.get(i).get(j).getId());
                    objectMain.put("exercise_name", listDatas.get(i).get(j).getExercise_name());
                    objectMain.put("image", listDatas.get(i).get(j).getImage());
                    objectMain.put("no_of_sets", listDatas.get(i).get(j).getNo_of_sets());
                    objectMain.put("rating", listDatas.get(i).get(j).getRating());
                    if (!listDatas.get(i).get(j).getTimeStamp().equalsIgnoreCase(""))
                        objectMain.put("timestamp", listDatas.get(i).get(j).getTimeStamp());
                    objectMain.put("workout_id", workout_id);
                    JSONArray sets = new JSONArray();
                    for (int k = 0; k < listDatas.get(i).get(j).getSets().size(); k++) {
                        JSONArray arraySet = new JSONArray();
                        for (int l = 0; l < listDatas.get(i).get(j).getSets().get(k).size(); l++) {
                            JSONObject objectSet = new JSONObject();
                            objectSet.put("setno", listDatas.get(i).get(j).getSets().get(k).get(l).getSetno());
                            objectSet.put("id", listDatas.get(i).get(j).getSets().get(k).get(l).getId());
                            objectSet.put("parameter_id", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_id());
                            objectSet.put("parameter_name", listDatas.get(i).get(j).getSets().get(k).get(l).getParameter_name());
                            objectSet.put("value", listDatas.get(i).get(j).getSets().get(k).get(l).getValue());
                            objectSet.put("set_value", listDatas.get(i).get(j).getSets().get(k).get(l).getSet_value());
                            objectSet.put("weight_unit", listDatas.get(i).get(j).getSets().get(k).get(l).getWeight_unit());
                            arraySet.put(objectSet);
                        }
                        sets.put(arraySet);
                    }
                    objectMain.put("sets", sets);
                    array.put(objectMain);
                }
                jsonArray.put(array);
            }
            jsonObject.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
            mBinding.loadingBar.progressBar.setVisibility(View.GONE);
            disableScreen(false);
        }
        if (is_finish) initLiveClientSocket(false);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Log.e("jSOn", jsonObject.toString());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addClientWorkoutTracking(requestBody)
                , getCompositeDisposable(), single, this);
    }

    private void initLiveClientSocket() {
        channels = new ArrayList<>();
        if (session.getAuthorizedUser(mActivity).isDirect_gym_client()) {
            channels.add(session.getAuthorizedUser(mActivity).getDirect_gym_id());
            if (session.getAuthorizedUser(mActivity).isClient_trainer()) {
                channels.add(session.getAuthorizedUser(mActivity).getTrainer_id());
            }
        } else
            channels.add(session.getAuthorizedUser(mActivity).getTrainer_id());


        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);


        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // create message payload using Gson
        JsonObject messageJsonObject = new JsonObject();
        messageJsonObject.addProperty("id", session.getAuthorizedUser(mActivity).getId());
        messageJsonObject.addProperty("name", session.getAuthorizedUser(mActivity).getName());
        messageJsonObject.addProperty("photo", session.getAuthorizedUser(mActivity).getPhoto());
        messageJsonObject.addProperty("email", session.getAuthorizedUser(mActivity).getEmail());
        messageJsonObject.addProperty("date", "" + spf.format(Calendar.getInstance().getTime()));
        messageJsonObject.addProperty("isStartedWorkout", true);
        JsonArray jsonArray = new JsonArray();

        for (int i = 0; i < listDatas.size(); i++) {
            for (int j = 0; j < listDatas.get(i).size(); j++) {
                JsonObject object = new JsonObject();
                object.addProperty("exercise_name", listDatas.get(i).get(j).getExercise_name());
                object.addProperty("id", listDatas.get(i).get(j).getId());
                object.addProperty("rating", listDatas.get(i).get(j).getRating());
                object.addProperty("timestamp", listDatas.get(i).get(j).getTimeStamp());
                jsonArray.add(object);
            }
        }
        messageJsonObject.add("exercises", jsonArray);

        System.out.println("Message to send: " + messageJsonObject.toString());

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost
//                    Toast.makeText(mActivity, "connectivity is lost", Toast.LENGTH_LONG).show();
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

//                    Toast.makeText(mActivity, "Connect event", Toast.LENGTH_LONG).show();
                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
                        for (String channelName : channels)
                            pubnub.publish().channel(channelName).message(messageJsonObject).async(new PNCallback<PNPublishResult>() {
                                @Override
                                public void onResponse(PNPublishResult result, PNStatus status) {
                                    // Check whether request successfully completed or not.
//                                    Toast.makeText(mActivity, "request successfully completed", Toast.LENGTH_LONG).show();

                                    if (!status.isError()) {

//                                        Toast.makeText(mActivity, "Message successfully published to specified channel", Toast.LENGTH_LONG).show();

                                        // Message successfully published to specified channel.
                                    }
                                    // Request processing failed.
                                    else {

//                                        Toast.makeText(mActivity, "Handle message publish error", Toast.LENGTH_LONG).show();

                                        status.retry();
                                        // Handle message publish error. Check 'category' property to find out possible issue
                                        // because of which request did fail.
                                        //
                                        // Request can be resent using: [status retry];
                                    }
                                }
                            });
                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {

                // Handle new message stored in message.message
                if (message.getChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscription()
                }

                /*JsonElement receivedMessageObject = message.getMessage();
                System.out.println("Received message content: " + receivedMessageObject.toString());
                // extract desired parts of the payload, using Gson
                String msg = message.getMessage().getAsJsonObject().get("msg").getAsString();
                System.out.println("msg content: " + msg);
                System.out.println("msg subscription: " + message.getSubscription());
                System.out.println("msg time token: " + message.getTimetoken());*/


                                    /*
                                        log the following items with your favorite logger
                                            - message.getMessage()
                                            - message.getSubscription()
                                            - message.getTimetoken()
                                    */
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });
        pubnub.subscribe().channels(channels).execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callExerciseList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK && data != null) {
            int main = data.getIntExtra("main", 0);
            int sub = data.getIntExtra("sub", 0);
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(data.getStringExtra("data"));
                listDatas.get(main).get(sub).setExercise_name(jsonObject.getString("exercise_name"));
                listDatas.get(main).get(sub).setId(jsonObject.getInt("id"));
                listDatas.get(main).get(sub).setImage(jsonObject.getString("image"));
                List<List<LogWorkoutDataModel.DataBean.SetsBean>> sets = new Gson().fromJson(jsonObject.getString("sets"), new TypeToken<List<List<LogWorkoutDataModel.DataBean.SetsBean>>>() {
                }.getType());
                listDatas.get(main).get(sub).setSets(sets);
                if (sets != null && sets.size() > 0) {
                    listDatas.get(main).get(sub).setNo_of_sets("" + sets.size());
                } else {
                    listDatas.get(main).get(sub).setNo_of_sets("0");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mBinding.recyclerView.setAdapter(new LogWorkoutMainAdapter(mActivity, listDatas, this, isSetEditable));
        }
    }


    private void callExerciseList() {
        if (getArguments() != null && getArguments().containsKey("isFromWorkout") && getArguments().getBoolean("isFromWorkout", false)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkoutDetails(workout_id, userId)
                    , getCompositeDisposable(), workoutList, this);
        } else {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            int client_log = 0;
            if (session.getAuthorizedUser(mContext).getRoleId().equals("1")) {
                client_log = 0;
            } else {
                client_log = 1;
            }
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkoutDetails(workout_id, training_program_id, (is_am_workout) ? 1 : 0, weekday_id, client_log, session.getStringDataByKeyNull(CLIENT_ID), is_show_log)
                    , getCompositeDisposable(), workoutList, this);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LogWorkoutDataModel workoutExerciseListModel = (LogWorkoutDataModel) o;
                if (workoutExerciseListModel.getStatus() == AppConstants.SUCCESS && workoutExerciseListModel.getData() != null && workoutExerciseListModel.getData().size() > 0) {
                    listDatas = workoutExerciseListModel.getData();
                    mBinding.imgNoData.setVisibility(View.GONE);
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.recyclerView.setAdapter(new LogWorkoutMainAdapter(mActivity, listDatas, this, isSetEditable));
                    if (isSetEditable)
                        initLiveClientSocket(true);
                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case single:
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
//                    mActivity.finish();
                    if (is_finish_flag)
                        startActivity(new Intent(mActivity, WorkoutFinishActivity.class));
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        //Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        switch (apiNames) {
            case workoutList:
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void setRating(int main, int sub, int rating) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        listDatas.get(main).get(sub).setTimeStamp(spf.format(Calendar.getInstance().getTime()));
        listDatas.get(main).get(sub).setRating(rating);
        initLiveClientSocket(true);
    }

    @Override
    public void setWeight(int main, int sub, int set, String value) {
        listDatas.get(main).get(sub).getSets().get(set).get(0).setValue(value);

        if (getArguments() != null && getArguments().containsKey("isFromWorkout") && getArguments().getBoolean("isFromWorkout", false))
            callSaveAPIFromWorkoutEdit(false);
        else
            callSaveAPIEdit(false);


    }

    @Override
    public void setReps(int main, int sub, int set, String value) {
        listDatas.get(main).get(sub).getSets().get(set).get(1).setValue(value);
        if (getArguments() != null && getArguments().containsKey("isFromWorkout") && getArguments().getBoolean("isFromWorkout", false))
            callSaveAPIFromWorkoutEdit(false);
        else
            callSaveAPIEdit(false);


    }

    @Override
    public void setRepetations(int main, int sub, int set, String value) {
        listDatas.get(main).get(sub).getSets().get(set).get(2).setValue(value);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.menu_log_workout, menu);
        menu.findItem(R.id.action_add).setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_notes:
                ViewWorkoutNoteDialog dialogNote = new ViewWorkoutNoteDialog(workout_id);
                dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddNoteDialog.class.getSimpleName());
                dialogNote.setCancelable(false);
                break;
            case R.id.action_add:
                break;
        }
        return true;
    }

    private void initLiveClientSocket(boolean isStart) {
        List<String> strings = new ArrayList<>();
        if (session.getAuthorizedUser(mActivity).isDirect_gym_client()) {
            strings.add(session.getAuthorizedUser(mActivity).getDirect_gym_id());
            if (session.getAuthorizedUser(mActivity).isClient_trainer()) {
                strings.add(session.getAuthorizedUser(mActivity).getTrainer_id());
            }
        } else
            strings.add(session.getAuthorizedUser(mActivity).getTrainer_id());


        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);


        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // create message payload using Gson
        JsonObject messageJsonObject = new JsonObject();
        messageJsonObject.addProperty("id", session.getAuthorizedUser(mActivity).getId());
        messageJsonObject.addProperty("name", session.getAuthorizedUser(mActivity).getName());
        messageJsonObject.addProperty("photo", session.getAuthorizedUser(mActivity).getPhoto());
        messageJsonObject.addProperty("email", session.getAuthorizedUser(mActivity).getEmail());
        messageJsonObject.addProperty("date", "" + spf.format(Calendar.getInstance().getTime()));
        messageJsonObject.addProperty("isStartedWorkout", isStart);
        JsonArray jsonArray = new JsonArray();
        if (isStart) {
            mBinding.swipeContainer.setRefreshing(false);
            for (int i = 0; i < listDatas.size(); i++) {
                for (int j = 0; j < listDatas.get(i).size(); j++) {
                    JsonObject object = new JsonObject();
                    object.addProperty("exercise_name", listDatas.get(i).get(j).getExercise_name());
                    object.addProperty("id", listDatas.get(i).get(j).getId());
                    object.addProperty("rating", listDatas.get(i).get(j).getRating());
                    object.addProperty("timestamp", listDatas.get(i).get(j).getTimeStamp());
                    jsonArray.add(object);
                }
            }
            messageJsonObject.add("exercises", jsonArray);
        }

        System.out.println("Message to send: " + messageJsonObject.toString());

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost
//                    Toast.makeText(mActivity, "connectivity is lost", Toast.LENGTH_LONG).show();
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

//                    Toast.makeText(mActivity, "Connect event", Toast.LENGTH_LONG).show();
                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
                        for (String channelName : strings)
                            pubnub.publish().channel(channelName).message(messageJsonObject).async(new PNCallback<PNPublishResult>() {
                                @Override
                                public void onResponse(PNPublishResult result, PNStatus status) {
                                    // Check whether request successfully completed or not.
//                                    Toast.makeText(mActivity, "request successfully completed", Toast.LENGTH_LONG).show();

                                    if (!status.isError()) {

//                                        Toast.makeText(mActivity, "Message successfully published to specified channel", Toast.LENGTH_LONG).show();

                                        // Message successfully published to specified channel.
                                    }
                                    // Request processing failed.
                                    else {

//                                        Toast.makeText(mActivity, "Handle message publish error", Toast.LENGTH_LONG).show();

                                        status.retry();
                                        // Handle message publish error. Check 'category' property to find out possible issue
                                        // because of which request did fail.
                                        //
                                        // Request can be resent using: [status retry];
                                    }
                                }
                            });
                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {

                // Handle new message stored in message.message
                if (message.getChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscription()
                }

                /*JsonElement receivedMessageObject = message.getMessage();
                System.out.println("Received message content: " + receivedMessageObject.toString());
                // extract desired parts of the payload, using Gson
                String msg = message.getMessage().getAsJsonObject().get("msg").getAsString();
                System.out.println("msg content: " + msg);
                System.out.println("msg subscription: " + message.getSubscription());
                System.out.println("msg time token: " + message.getTimetoken());*/


                                    /*
                                        log the following items with your favorite logger
                                            - message.getMessage()
                                            - message.getSubscription()
                                            - message.getTimetoken()
                                    */
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });
        pubnub.subscribe().channels(strings).execute();
    }

    @Override
    public void onRefresh() {
        callExerciseList();
    }
}
