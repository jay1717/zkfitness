package com.fitzoh.app.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.DialogAddMealBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.SessionManager;
import com.fitzoh.app.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.addMeal;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

@SuppressLint("ValidFragment")
public class AddMealDialog extends AppCompatDialogFragment implements SingleCallback {

    DialogAddMealBinding mBinding;
    DialogClickListener listener;
    String userId, userAccessToken;
    public SessionManager session;
    int dietId, dietMealId;
    String mealTIme, mealName;
    int mHour, mMinute;
    private DialogClickListener dialogClickListener;
    public CompositeDisposable compositeDisposable;

    public AddMealDialog(int dietId, DialogClickListener dialogClickListener) {
        this.dietId = dietId;
        this.dialogClickListener = dialogClickListener;
    }

    public AddMealDialog(String mealTIme, String mealName, int dietId, int mealId, DialogClickListener dialogClickListener) {
        this.mealTIme = mealTIme;
        this.mealName = mealName;
        this.dietId = dietId;
        this.dietMealId = mealId;
        this.dialogClickListener = dialogClickListener;
    }

    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (view == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_add_meal, null, false);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorAccent), mBinding.btnCancel, true);
            Utils.getBlueGreenGradient(ContextCompat.getColor(getActivity(), R.color.colorPrimary), mBinding.btnAdd, false);
            view = mBinding.getRoot();
            Utils.setLines(getActivity(), mBinding.view.view);
            Utils.setLines(getActivity(), mBinding.view.view2);
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) getActivity()).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            prepareLayout();
        }

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setView(view);
        // Create the alert dialog
        AlertDialog dialog = alertBuilder.create();
        // Inner Spacing
//        dialog.setView(mBinding.getRoot(), 100, 0, 100, 0);

        // Outer Spacing
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        InsetDrawable inset = new InsetDrawable(back, 80);
        dialog.getWindow().setBackgroundDrawable(inset);

        dialog.show();

        return dialog;
    }

    private void prepareLayout() {
      /*  SessionManager sessionManager=new SessionManager(getContext());
        if(sessionManager.getBooleanDataByKey(KEY_IS_SP)){
            mBinding.chkFavSp.setVisibility(View.GONE);
        }
        else {
            mBinding.chkFavSp.setVisibility(View.VISIBLE);
            isFav=mBinding.chkFavSp.isChecked();
        }

        order = (BookingListModel.DataBean) getArguments().getSerializable("OderderData");

        mBinding.btnSend.setOnClickListener(view -> {
            listener.onDismissCalled(mBinding.edtReview.getText().toString(), String.valueOf(mBinding.ratingBar.getRating()),mBinding.chkFavSp.isChecked(),order);
            dismiss();
        });
*/
        session = new SessionManager(getActivity());
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.btnAdd.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                    if (mealName != null && mealTIme != null)
                        editMeal();
                    else
                        addMeal();
                }
            } else {
                showSnackBar(getString(R.string.network_unavailable));
            }

        });
        mBinding.edtMealTime.setKeyListener(null);
        mBinding.edtMealTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(),
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                String selectedTime = hourOfDay + ":" + minute;
                                SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm");
                                SimpleDateFormat outFormat = new SimpleDateFormat("hh:mm a");
                                try {
                                    String time = outFormat.format(inFormat.parse(selectedTime));
                                    mBinding.edtMealTime.setText(time);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        mBinding.btnCancel.setOnClickListener(view -> {
            hideKeyboard();
            dismiss();
        });
        if (mealTIme != null) {
            mBinding.edtMealTime.setText(mealTIme);
            mBinding.txtTitle.setText("Edit Meal");
        }
        if (mealName != null)
            mBinding.edtMealName.setText(mealName);

    }

    private boolean validation() {
        if (TextUtils.isEmpty(mBinding.edtMealName.getText().toString())) {
            showSnackBar("Enter Meal name.");
            return false;
        } else if (TextUtils.isEmpty(mBinding.edtMealTime.getText().toString())) {
            showSnackBar("Enter Meal time.");
            return false;
        }
        return true;
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    private void addMeal() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addMeal(mBinding.edtMealName.getText().toString(), mBinding.edtMealTime.getText().toString(), dietId, session.getStringDataByKeyNull(CLIENT_ID))
                , getCompositeDisposable(), addMeal, this);
    }

    private void editMeal() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).editMeal(mBinding.edtMealName.getText().toString(), mBinding.edtMealTime.getText().toString(), dietId, dietMealId, session.getStringDataByKeyNull(CLIENT_ID))
                , getCompositeDisposable(), addMeal, this);
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void setListener(DialogClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case addMeal:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);

                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    hideKeyboard();
                    dialogClickListener.refreshData();
                    dismiss();

                } else {
                    showSnackBar(commonApiResponse.getMessage());
                }
                break;
        }
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    public void showSnackBar(String msg) {
        if (view == null) return;
        Snackbar snackbar = Snackbar.make(mBinding.getRoot(), msg, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        snackbar.show();
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    public interface DialogClickListener {
        void refreshData();
    }
}
