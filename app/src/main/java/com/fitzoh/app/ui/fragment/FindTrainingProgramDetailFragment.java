package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.FindTrainingProgramWeekAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramClientBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.FindPlanTrainingProgramModel;
import com.fitzoh.app.model.TrainingProgramEditModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.LogWorkoutExerciseListActivity;
import com.fitzoh.app.ui.activity.TrainingProgramExerciseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindTrainingProgramDetailFragment extends BaseFragment implements SingleCallback, FindTrainingProgramWeekAdapter.RedirectToDetailListener, PaymentResultListener, SwipeRefreshLayout.OnRefreshListener {


    public FragmentTrainingProgramClientBinding mBinding;
    public FindTrainingProgramWeekAdapter recyclerViewAdapter;
    String userId, userAccessToken;
    FindPlanTrainingProgramModel.DataBean dataBean;

    public FindTrainingProgramDetailFragment() {
        // Required empty public constructor
    }


    public static FindTrainingProgramDetailFragment newInstance(Bundle bundle) {
        FindTrainingProgramDetailFragment fragment = new FindTrainingProgramDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (FindPlanTrainingProgramModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_training_program_client, container, false);
        setHasOptionsMenu(true);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, dataBean.getName());
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        recyclerViewAdapter = new FindTrainingProgramWeekAdapter(mActivity, new ArrayList<>(), this);
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(mActivity);
        mBinding.recyclerView.setLayoutManager(recyclerLayoutManager);
        mBinding.recyclerView.setAdapter(recyclerViewAdapter);
        callTrainingProgram();
    }


    private void callTrainingProgram() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWeekWiseTrainingProgram(dataBean.getId(), null)
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainingProgramEditModel editModel = (TrainingProgramEditModel) o;
                if (editModel != null && editModel.getStatus() == AppConstants.SUCCESS && editModel.getData() != null && editModel.getData().size() > 0) {
                    recyclerViewAdapter = new FindTrainingProgramWeekAdapter(mActivity, editModel.getData(), this);
                    mBinding.recyclerView.setAdapter(recyclerViewAdapter);
                    setAdapter();

                } else {
                    setAdapter();
                }
                break;
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS) {
                    mActivity.onBackPressed();
                } else {
                    showSnackBar(mBinding.mainLayout, response.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, throwable.getMessage(), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case trainingList:
                setAdapter();
                break;
            case single:
                mActivity.finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
        }
    }


    private void setAdapter() {
        if (recyclerViewAdapter.weekList.size() > 0) {
            mBinding.recyclerView.setVisibility(View.VISIBLE);
            mBinding.imgNoData.setVisibility(View.GONE);
        } else {
            mBinding.recyclerView.setVisibility(View.GONE);
            mBinding.imgNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void openExerciseDetail(int workout_id, String workout_name, boolean isAM, int day_id) {
        Intent intent = new Intent(mActivity, TrainingProgramExerciseActivity.class);
        intent.putExtra("workout_id", workout_id);
        intent.putExtra("workout_name", workout_name);
        intent.putExtra("training_program_id", dataBean.getId());
        intent.putExtra("is_am_workout", isAM);
        intent.putExtra("weekday_id", day_id);
        startActivity(intent);
    }

    @Override
    public void viewLogWorkout(int workout_id, String workout_name, boolean isAM, int day_id) {
        startActivity(new Intent(mActivity, LogWorkoutExerciseListActivity.class).putExtra("workout_id", workout_id).putExtra("workout_name", workout_name).putExtra("training_program_id", dataBean.getId()).putExtra("is_am_workout", isAM).putExtra("isSetEditable", false).putExtra("weekday_id", day_id).putExtra("is_show_log", 0));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        menu.findItem(R.id.menu_notification).setVisible(false);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        textView.setText(dataBean.getPrice() == 0 ? "Add" : "Purchase");
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> purchase(dataBean.getPrice() == 0 ? "Add" : "Purchase"));
    }

    private void purchase(String type) {
        if (dataBean.getIs_purchased() == 1)
            showSnackBar(mBinding.mainLayout, "you have already purchased this training program.", Snackbar.LENGTH_LONG);
        else {
            if (type.equalsIgnoreCase("Add")) {
                //direct API
                purchaseSubscription("", "");
            } else {
                //razorpay api
                checkout();
            }
        }

    }

    private void checkout() {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher);
        final Activity activity = mActivity;


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Fitzoh");
            options.put("description", "training program charge");
            options.put("currency", "INR");
            options.put("amount", dataBean.getPrice() * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());
            preFill.put("email", session.getAuthorizedUser(mActivity).getEmail());
//            preFill.put("contact", "+919784849848");

            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");

    }

    private void purchaseSubscription(String s, String razorpay) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchase_plan("training_program",
                dataBean.getId(), s, razorpay)
                , getCompositeDisposable(), save, this);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.mainLayout, s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);

    }

    @Override
    public void onRefresh() {
        callTrainingProgram();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
