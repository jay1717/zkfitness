package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientGroupListAdapter;
import com.fitzoh.app.databinding.FragmentClientGroupListBinding;
import com.fitzoh.app.model.AddClientGroupListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ClientAssignToGroupActivity;
import com.fitzoh.app.ui.dialog.AddClientGroupDialog;
import com.fitzoh.app.ui.dialog.AddWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.Objects;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteTraining;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class ClientGroupListFragment extends BaseFragment implements SingleCallback, ClientGroupListAdapter.ClientActivateListener, SwipeRefreshLayout.OnRefreshListener {

    FragmentClientGroupListBinding mBinding;
    String userId, userAccessToken;

    public ClientGroupListFragment() {

    }


    public static ClientGroupListFragment newInstance() {
        return new ClientGroupListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Client Groups");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_group_list, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);

        mBinding.fab.setOnClickListener(v -> {
            AddClientGroupDialog dialog = new AddClientGroupDialog();
            dialog.setTargetFragment(this, 0);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
        return mBinding.getRoot();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            //
            startActivity(new Intent(mActivity, ClientAssignToGroupActivity.class).putExtra("data", data.getIntExtra("data", 0)).putExtra("group_name", data.getStringExtra("group_name")));
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isOnline(mActivity)) {
            getClientsGroup();
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    private void getClientsGroup() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientGroupsWithUser()
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                AddClientGroupListModel liveClientsModel = (AddClientGroupListModel) o;
                if (liveClientsModel != null && liveClientsModel.getStatus() == AppConstants.SUCCESS && liveClientsModel.getData() != null && liveClientsModel.getData().size() > 0) {
                    mBinding.recyclerView.setAdapter(new ClientGroupListAdapter(getActivity(), liveClientsModel.getData(), this));
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.imgNoData.setVisibility(View.GONE);
                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case deleteTraining:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getClientsGroup();
                } else {
                    showSnackBar(mBinding.mainLayout, Objects.requireNonNull(commonApiResponse).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case single:
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onSelect(AddClientGroupListModel.DataBean dataBean) {
        startActivity(new Intent(mActivity, ClientAssignToGroupActivity.class).putExtra("data", dataBean.getId()).putExtra("group_name", dataBean.getName()));
    }

    @Override
    public void delete(AddClientGroupListModel.DataBean dataBean) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteClientGroup(dataBean.getId())
                , getCompositeDisposable(), deleteTraining, this);
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(mActivity)) {
            getClientsGroup();
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }

        mBinding.swipeContainer.setRefreshing(false);
    }
}
