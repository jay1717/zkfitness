package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ClientInquiryDetailistAdapter;

import com.fitzoh.app.databinding.FragmentClientInquiryDetailBinding;
import com.fitzoh.app.model.TrainerProductInquiryListing;
import com.fitzoh.app.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientInquiryDetailFragment extends BaseFragment {

    FragmentClientInquiryDetailBinding mBinding;
    TrainerProductInquiryListing.DataBean dataBean;
    List<TrainerProductInquiryListing.DataBean.InquiriesBean> inquiryListing = new ArrayList<>();
    ClientInquiryDetailistAdapter clientInquiryDetailistAdapter;

    public ClientInquiryDetailFragment() {
        // Required empty public constructor
    }

    public static ClientInquiryDetailFragment newInstance() {
        ClientInquiryDetailFragment fragment = new ClientInquiryDetailFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (TrainerProductInquiryListing.DataBean) getArguments().getSerializable("data");
            inquiryListing = dataBean.getInquiries();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Inquiry details");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_inquiry_detail, container, false);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if(inquiryListing!=null && inquiryListing.size()>0){
            clientInquiryDetailistAdapter = new ClientInquiryDetailistAdapter(getActivity(), inquiryListing);
            mBinding.recyclerView.setAdapter(clientInquiryDetailistAdapter);
        }
        return mBinding.getRoot();
    }
}
