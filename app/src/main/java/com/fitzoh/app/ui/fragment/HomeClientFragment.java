package com.fitzoh.app.ui.fragment;


import android.Manifest;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentHomeClientBinding;
import com.fitzoh.app.interfaces.MyLocationChangeListner;
import com.fitzoh.app.model.FitnessCenterListModel;
import com.fitzoh.app.model.SpecialityListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.SelectSpecialityValueActivity;
import com.fitzoh.app.ui.activity.TrainerListActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.HashMap;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.fitnessCenterList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.specialityList;
import static com.fitzoh.app.ui.fragment.UserCreateProfileFragment.REQUEST_CHECK_SETTINGS;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeClientFragment extends BaseFragment implements PlaceSelectionListener, MyLocationChangeListner, View.OnClickListener, SingleCallback, AdapterView.OnItemSelectedListener {


    FragmentHomeClientBinding mBinding;
    String userId, userAccessToken;
    LatLng latLng;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));
    private static final int REQUEST_SELECT_PLACE = 1000;
    private static final int RECORD_REQUEST_CODE = 101;
    String latitude, longitude;
    String rollOfUser;
    private static final int REQUEST_SELECT_FITNESS_VELUE = 1001;
    HashMap<Integer, String> spinnerMap;
    int fitnessCenterId = 0;
    String latLongString;
    LatLngBounds latLngBounds;
    GeoDataClient mDataClient;
    private View view;

    public HomeClientFragment() {
        // Required empty public constructor
    }

    public static HomeClientFragment newInstance() {
        HomeClientFragment fragment = new HomeClientFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.search_trainer));
        this.view = view;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_home_client, container, false);
        setThemeWidget();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        makeRequest();
        if (Utils.isOnline(getActivity())) {
            getFitnessCenterList();
            getSpeciality(0);
        } else {
            showSnackBar(mBinding.layoutClientHome.findViewById(android.R.id.content), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
        setClickEvent();
        mBinding.home.radiogroupTrainer
                .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        Log.d("chk", "id" + checkedId);

                        mBinding.home.edtSearch.setText(getString(R.string.search_location));
                        getFitnessCenterList();
                        if (checkedId == R.id.radio_all) {
                            //some code
                            rollOfUser = "all";
                            mBinding.home.edtSpeciality.setVisibility(View.VISIBLE);
                            mBinding.home.btnSpecialityDropdown.setVisibility(View.VISIBLE);
                        } else if (checkedId == R.id.radio_trainer) {
                            rollOfUser = "2";
                            mBinding.home.edtSpeciality.setVisibility(View.VISIBLE);
                            mBinding.home.btnSpecialityDropdown.setVisibility(View.VISIBLE);
                        } else if (checkedId == R.id.radio_gym) {
                            rollOfUser = "3";
                            mBinding.home.edtSpeciality.setVisibility(View.GONE);
                            mBinding.home.btnSpecialityDropdown.setVisibility(View.GONE);
                        }

                    }

                });
        return mBinding.getRoot();
    }

    private void setThemeWidget() {
        Utils.getShapeGradient(mContext, mBinding.home.btnSearch);
        Utils.getShapeGradient(mContext, mBinding.home.btnReset);
        Utils.setTextViewStartImage(mContext, mBinding.home.edtSearch, R.drawable.ic_search, true);
        Utils.setTextViewStartImage(mContext, mBinding.home.edtSpeciality, R.drawable.ic_speciality, true);
        Utils.setImageBackground(mContext, mBinding.home.imgGym, R.drawable.ic_gym);
        Utils.setSpinnerArrow(getActivity(), new ImageView[]{mBinding.home.btnFitnessDropdown, mBinding.home.btnSpecialityDropdown});
        Utils.setRadioButtonSelectors(mActivity, mBinding.home.radioAll);
        Utils.setRadioButtonSelectors(mActivity, mBinding.home.radioGym);
        Utils.setRadioButtonSelectors(mActivity, mBinding.home.radioTrainer);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    private void setClickEvent() {

        mBinding.home.edtSearch.setOnClickListener(this);
        mBinding.home.btnSearch.setOnClickListener(this);
        //  mBinding.home.edtFitnessCenter.setKeyListener(null);
//        mBinding.home.edtFitnessCenter.setOnClickListener(this);

        mBinding.home.edtSpeciality.setKeyListener(null);
        mBinding.home.edtFitnessCenter.setOnItemSelectedListener(this);
        mBinding.home.spinnerFitnessCenter.setOnItemSelectedListener(this);
        mBinding.home.edtSpeciality.setOnClickListener(this);
        mBinding.home.btnFitnessDropdown.setOnClickListener(this);
        mBinding.home.btnSpecialityDropdown.setOnClickListener(this);
        mBinding.home.btnReset.setOnClickListener(this);

    }


    public void performSearchClick() {
        if (Utils.isOnline(mContext)) {
            try {
                AutocompleteFilter filter = new AutocompleteFilter.Builder()
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                        .build();
                Intent intent = new PlaceAutocomplete.IntentBuilder
                        (PlaceAutocomplete.MODE_FULLSCREEN)
                        .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                        .setFilter(filter)
                        .build(mActivity);
                mActivity.startActivityForResult(intent, REQUEST_SELECT_PLACE);
            } catch (GooglePlayServicesRepairableException |
                    GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        } else {
            showSnackBar(mBinding.layoutClientHome, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        latitude = String.valueOf(place.getLatLng().latitude);
        longitude = String.valueOf(place.getLatLng().longitude);
        storeLocationDetails(place.getLatLng().latitude, place.getLatLng().longitude, place.getAddress());
        mBinding.home.edtSearch.setText(place.getAddress());



     /*  latLngBounds = new LatLngBounds(
               new LatLng(-33.880490, 151.184363),
               new LatLng(-33.858754, 151.229596));

     *//*   AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS)
                .build();*//*

        Task<AutocompletePredictionBufferResponse> results =
                mDataClient.getAutocompletePredictions("test", latLngBounds,
                        typeFilter);

        Log.e("places", "" + results);*/

//        mGoogleMap.clear();
        latLng = place.getLatLng();
        getFitnessCenterList();
    }

    @Override
    public void onError(Status status) {

    }

    private void storeLocationDetails(double latitude, double longitude, CharSequence address) {
    }

    @Override
    public void onLocationChange(Location location) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.edt_search:
                makeRequest();
                performSearchClick();

                break;
            case R.id.btn_search:
                if (Utils.isOnline(getActivity())) {
                    Log.e("search", "" + latLongString + fitnessCenterId + mBinding.home.edtSpeciality.getText().toString());
                    startActivity(new Intent(getActivity(), TrainerListActivity.class).putExtra("latLong", latLongString).putExtra("fitnessCenterId", fitnessCenterId).putExtra("speciality", mBinding.home.edtSpeciality.getText().toString()).putExtra("role_id", rollOfUser));
                } else {
                    showSnackBar(mBinding.layoutClientHome, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
                }
                break;

            case R.id.edt_fitness_center:
                getFitnessCenterList();
                break;

            case R.id.edt_speciality:
                if (Utils.isOnline(getActivity())) {
                    getActivity().startActivityForResult(new Intent(getActivity(), SelectSpecialityValueActivity.class).putExtra("fitnessCenterId", fitnessCenterId), REQUEST_SELECT_FITNESS_VELUE);

                } else {
                    showSnackBar(mBinding.layoutClientHome, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
                }
                break;

            case R.id.btn_fitness_dropdown:
//                mBinding.home.edtFitnessCenter.performClick();
                mBinding.home.edtFitnessCenter.onTouch(view, MotionEvent.obtain(1, 1, MotionEvent.ACTION_UP, 1, 1, 1));
                break;

            case R.id.btn_speciality_dropdown:
                if (Utils.isOnline(getActivity())) {
                    mBinding.home.edtSpeciality.performClick();

                } else {
                    showSnackBar(mBinding.layoutClientHome, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
                }

                break;

            case R.id.btn_reset:
                mBinding.home.edtSearch.setText(getActivity().getResources().getString(R.string.search_location));
                mBinding.home.radioAll.setSelected(true);
                mBinding.home.radioAll.setChecked(true);
                latitude = null;
                longitude = null;
                latLongString = "";
                mBinding.home.edtSpeciality.setText("");
                getFitnessCenterList();
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_SELECT_PLACE) {
                Place place = PlaceAutocomplete.getPlace(mContext, data);
                this.onPlaceSelected(place);
            } else if (requestCode == REQUEST_CHECK_SETTINGS) {
                // getLocation();
            }
        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(mContext, data);
            this.onError(status);
        }
        if (requestCode == REQUEST_SELECT_FITNESS_VELUE) {
            if (resultCode == RESULT_OK) {
                Log.d("Result", "");
                if (data != null) {
                    HashMap<String, SpecialityListModel.DataBean> selectedFitneeList = (HashMap<String, SpecialityListModel.DataBean>) data.getExtras().getSerializable("Selected Data");
                    String edtString = "";
                    for (String key : selectedFitneeList.keySet()) {
                        edtString = edtString + "" + selectedFitneeList.get(key).getName() + ",";
                    }
                    mBinding.home.edtSpeciality.setText(edtString.substring(0, edtString.length() - 2));

                }
            }
        }

    }

    protected void makeRequest() {
//        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE);
        requestAppPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }

    private void getSpeciality(int specialityId) {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSpecialityList(specialityId)
                , getCompositeDisposable(), specialityList, this);
    }

    private void getFitnessCenterList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            if (mBinding.home.radiogroupTrainer.getCheckedRadioButtonId() == R.id.radio_all) {
                rollOfUser = "all";
            } else if (mBinding.home.radiogroupTrainer.getCheckedRadioButtonId() == R.id.radio_trainer) {
                rollOfUser = "2";
            } else {
                rollOfUser = "3";
            }
            if (latitude != null && longitude != null)
                latLongString = latitude + "," + longitude;
            else
                latLongString = "";

            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getFitnessCenterList(latLongString, rollOfUser)
                    , getCompositeDisposable(), fitnessCenterList, this);
        } else {
            showSnackBar(mBinding.layoutClientHome, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case fitnessCenterList:

                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                FitnessCenterListModel fitnessCenterListModel = (FitnessCenterListModel) o;
                if (fitnessCenterListModel.getStatus() == AppConstants.SUCCESS) {
                    String[] spinnerArray = new String[fitnessCenterListModel.getData().size() + 1];
                    spinnerArray[0] = getString(R.string.select_fitness_center_fitness_trainer);
                    spinnerMap = new HashMap<Integer, String>();
                    for (int i = 0; i < fitnessCenterListModel.getData().size(); i++) {
                        spinnerMap.put(i, String.valueOf(fitnessCenterListModel.getData().get(i).getId()));
                        spinnerArray[i + 1] = fitnessCenterListModel.getData().get(i).getName();
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_search_item, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_item_add_workout);
                    mBinding.home.edtFitnessCenter.setAdapter(adapter);

                }


                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Log.e("onitem", "onItemSelected: " + position);
        if (position == 0) {
            fitnessCenterId = 0;
        }
        if (position != 0) {
//            String item = parent.getItemAtPosition(position).toString();
            String name = mBinding.home.edtFitnessCenter.getSelectedItem().toString();
//            fitnessCenterId = Integer.parseInt(spinnerMap.get(mBinding.home.spinnerFitnessCenter.getSelectedItemPosition()));
            fitnessCenterId = Integer.parseInt(spinnerMap.get(mBinding.home.edtFitnessCenter.getSelectedItemPosition() - 1));

            if (Utils.isOnline(getActivity()))
                getSpeciality(fitnessCenterId);
            else
                showSnackBar(mBinding.layoutClientHome, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
//            Toast.makeText(getActivity(), name, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
