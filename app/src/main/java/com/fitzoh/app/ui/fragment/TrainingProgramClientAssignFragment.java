package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainerAssignSelectoinAdapter;
import com.fitzoh.app.databinding.FragmentClientAssignBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.dialog.AssignToClientDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class TrainingProgramClientAssignFragment extends BaseFragment implements SingleCallback, TrainerAssignSelectoinAdapter.onChecked, AssignToClientDialog.onSave, SwipeRefreshLayout.OnRefreshListener {


    FragmentClientAssignBinding mBinding;
    String userId, userAccessToken;
    private TrainingProgramList.DataBean dataBean = null;
    TrainerAssignSelectoinAdapter adapter;
    private List<ClientListModel.DataBean> data = new ArrayList<>();
    JsonArray jArrayId, jArrayIsAssign, jArrayIsAllow, jArrayDate;
    JsonObject jObjAssignData;

    JSONArray jsonArray;
    JSONObject jsonObject;

    public TrainingProgramClientAssignFragment() {
        // Required empty public constructor
    }

    public static TrainingProgramClientAssignFragment newInstance(TrainingProgramList.DataBean dataBean) {
        TrainingProgramClientAssignFragment clientAssignFragment = new TrainingProgramClientAssignFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", dataBean);
        clientAssignFragment.setArguments(bundle);
        return clientAssignFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (TrainingProgramList.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_client_assign, container, false);
        Utils.getShapeGradient(mActivity, mBinding.client.btnAssign);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.client.swipeContainer.setOnRefreshListener(this);
        mBinding.client.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        getClients();
        jArrayDate = new JsonArray();
        jArrayIsAssign = new JsonArray();
        jArrayIsAllow = new JsonArray();
        jArrayId = new JsonArray();
        jObjAssignData = new JsonObject();

        jsonArray = new JSONArray();
        jsonObject = new JSONObject();
        mBinding.client.btnAssign.setOnClickListener(view -> {
            if (validation()) {
                assignWorkout();
            }
        });

        return mBinding.getRoot();
    }

    private boolean validation() {
        if (data == null || data.size() <= 0) {
            showSnackBar(mBinding.layoutClient, "No Data Available", Snackbar.LENGTH_LONG);
            return false;
        } /*else if (adapter.getData() == null || adapter.getData().size() <= 0) {
            showSnackBar(mBinding.getRoot(), "Please select client to assign", Snackbar.LENGTH_LONG);
            return false;
        }*/
        return true;
    }

    private void getClients() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientList(null, null, dataBean.getId(), null, null)
                    , getCompositeDisposable(), single, this);
        }
        else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void assignWorkout() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getClientJson());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).assignTrainingProgram(requestBody)
                    , getCompositeDisposable(), assignWorkout, this);


        } else {
            showSnackBar(mBinding.layoutClient, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }

    }

    private String getClientJson() {
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            jsonObject.put("client_ids", jsonArray);
            jsonObject.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
            jsonObject.put("training_program_id", dataBean.getId());
        /*    jsonObject.put("start_date", dateFormat.format(Calendar.getInstance().getTime()));
            jsonObject.put("allowed_to_change_date", 1);*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                ClientListModel clientListModel = (ClientListModel) o;
                if (clientListModel != null && clientListModel.getStatus() == AppConstants.SUCCESS && clientListModel.getData() != null && clientListModel.getData().size() > 0) {
                    mBinding.client.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.client.imgNoData.setVisibility(View.GONE);
                    data = clientListModel.getData();
                    adapter = new TrainerAssignSelectoinAdapter(getActivity(), data, this);
                    mBinding.client.recyclerView.setAdapter(adapter);
                } else {
                    mBinding.client.recyclerView.setVisibility(View.GONE);
                    mBinding.client.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.layoutClient, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case single:
                mBinding.client.recyclerView.setVisibility(View.GONE);
                mBinding.client.imgNoData.setVisibility(View.VISIBLE);
        }
        showSnackBar(mBinding.layoutClient, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    @Override
    public void check(ClientListModel.DataBean dataBean, boolean isChecked) {
        if (isChecked) {
            AssignToClientDialog dialog = new AssignToClientDialog(dataBean.getId(), dataBean.getIs_assigned(), this);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AssignToClientDialog.class.getSimpleName());
            dialog.setCancelable(false);
        } else {
            try {
                jsonObject = new JSONObject();
                jsonObject.put("user_id", dataBean.getId());
                jsonObject.put("is_assigned", 0);
                jsonObject.put("start_date", "");
                jsonObject.put("allowed_to_change_date", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(int id, String date, int isAllowed, int isAssign) {
     /*   jArrayDate.add(date);
        jArrayIsAllow.add(isAllowed);
        jArrayId.add(id);
        jArrayIsAssign.add(isAssign);
        jObjAssignData.add("start_date", jArrayDate);
        jObjAssignData.add("allowed_to_change_date", jArrayIsAllow);
        jObjAssignData.add("user_id", jArrayId);
        jObjAssignData.add("is_assigned", jArrayIsAssign);*/
        try {
            jsonObject = new JSONObject();
            jsonObject.put("user_id", id);
            jsonObject.put("is_assigned", isAssign);
            jsonObject.put("start_date", date);
            jsonObject.put("allowed_to_change_date", isAllowed);
            jsonArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
     getClients();
        mBinding.client.swipeContainer.setRefreshing(false);
    }
}
