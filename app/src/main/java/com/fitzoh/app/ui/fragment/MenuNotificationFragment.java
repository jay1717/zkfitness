package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.MenuNotificationListAdapter;
import com.fitzoh.app.databinding.FragmentMenuNotificationBinding;
import com.fitzoh.app.model.MenuNotificationResponseModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getTrainerProfile;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MenuNotificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuNotificationFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentMenuNotificationBinding mBinding;
    String userId, userAccessToken;

    public MenuNotificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MenuNotificationListAdapter.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuNotificationFragment newInstance() {
        MenuNotificationFragment fragment = new MenuNotificationFragment();
      /*  Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu_notification, container, false);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.notificaion));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        callApi();
    }

    private void callApi() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getNotification(session.getAuthorizedUser(getContext()).getId())
                    , getCompositeDisposable(), single, this);

        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        MenuNotificationResponseModel response = (MenuNotificationResponseModel) o;
        if (response.getStatus() == AppConstants.SUCCESS) {
            if (response.getData() != null && response.getData().size() > 0) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.imgNoData.setVisibility(View.GONE);
                mBinding.recyclerView.setAdapter(new MenuNotificationListAdapter(response.getData(), getContext()));
            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);

            }
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onRefresh() {
        callApi();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
