package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TodaysDietAdapter;
import com.fitzoh.app.adapter.TodaysWorkoutAdapter;
import com.fitzoh.app.databinding.FragmentHomeClientSubscribeBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GetClientDetailModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ClientDocumentViewActivity;
import com.fitzoh.app.ui.dialog.StartWorkoutDialog;
import com.fitzoh.app.ui.dialog.StartWorkoutHomeDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.gson.JsonObject;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.updateDate;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;


public class HomeClientSubscribeFragment extends BaseFragment implements SingleCallback, TodaysWorkoutAdapter.TodaysWorkoutListener, TodaysDietAdapter.SelectExerciseListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    FragmentHomeClientSubscribeBinding mBinding;

    GetClientDetailModel.DataBean dataBean;
    String userId, userAccessToken;
    private PubNub pubnub;
    private List<String> channels;

    public static final String inputFormat = "HH:mm";
    private ArrayList<GetClientDetailModel.DataBean.TodaysDietBean> todays_diet = new ArrayList<>();
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);


    private Date date;
    private Date dateCompareTwo;

    public HomeClientSubscribeFragment() {
    }

    public static HomeClientSubscribeFragment newInstance() {
        HomeClientSubscribeFragment fragment = new HomeClientSubscribeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupWhiteToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.home));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_client_subscribe, container, false);
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        setHasOptionsMenu(true);
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.layoutHome.layoutTodayDiet.recyclerView.setAdapter(new TodaysDietAdapter(mActivity, new ArrayList<>(), this));
        mBinding.layoutHome.layoutTodayWorkout.recyclerView.setAdapter(new TodaysWorkoutAdapter(mActivity, new ArrayList<>(), this));
        mBinding.toolbar.tvTitle.setGravity(Gravity.LEFT);
        mBinding.layoutTrainer.imgTrainer.setOnClickListener(this);

        getClientDetail();
    }

    private void getClientDetail() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientDetails()
                , getCompositeDisposable(), workoutList, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_notification, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        mActivity.invalidateOptionsMenu();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                GetClientDetailModel model = (GetClientDetailModel) o;
                if (model.getStatus() == AppConstants.SUCCESS) {
                    dataBean = model.getData();
                    todays_diet = dataBean.getTodays_diet();

                    setClientDetails();

                    for (int i = 0; i < todays_diet.size(); i++) {
                        focusOnView(todays_diet.get(i).getTime(), i);

                    }
                } else {
                    showSnackBar(mBinding.mainLayout, model.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse data = (CommonApiResponse) o;
                if (data.getStatus() == AppConstants.SUCCESS) {
                    getClientDetail();
                } else {
                    showSnackBar(mBinding.mainLayout, data.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case updateDate:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    getClientDetail();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

        }
    }

    private void focusOnView(String compareStringTwo, int i) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                compareDates(compareStringTwo, i);
            }
        });
    }

    private void compareDates(String compareStringTwo, int i) {
        Calendar now = Calendar.getInstance();

        int hour = now.get(Calendar.HOUR);
        int minute = now.get(Calendar.MINUTE);

        date = parseDate(hour + ":" + minute);
        dateCompareTwo = parseDate(compareStringTwo);
        if (dateCompareTwo.after(date)) {
            mBinding.layoutHome.layoutTodayDiet.recyclerView.smoothScrollToPosition(i);
        }

    }

    private Date parseDate(String date) {
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }



    private void setClientDetails() {
        mBinding.layoutTrainer.name.setText("Hi, " + dataBean.getName());
        mBinding.layoutTrainer.txtTag.setText(dataBean.getEmail());
        mBinding.layoutTrainer.txtDescription.setText(dataBean.getAbout());
        Utils.setImage(mActivity.getApplicationContext(), mBinding.layoutTrainer.imgTrainer, session.getAuthorizedUser(mContext).getPhoto());
        Utils.setImage(mActivity.getApplicationContext(), mBinding.layoutTrainer.imgBanner, dataBean.getTrainer_photo());
        mBinding.layoutHome.layoutTodayDiet.txtTotalMeal.setText("Total Meals: " + dataBean.getTotal_meals());

        String dateType = dataBean.getAllow_to_change_date();
        if (dateType.equals("1"))
            changeDate(dataBean.getTodays_workout().get(0).getTraining_program_id());
        if (dataBean.getTodays_diet() != null && dataBean.getTodays_diet().size() > 0)
            mBinding.layoutHome.layoutTodayDiet.recyclerView.setAdapter(new TodaysDietAdapter(mActivity, dataBean.getTodays_diet(), this));
        else
            mBinding.layoutHome.layoutTodayDiet.recyclerView.setAdapter(new TodaysDietAdapter(mActivity, new ArrayList<>(), this));
        if (dataBean.getTodays_workout() != null && dataBean.getTodays_workout().size() > 0)
            mBinding.layoutHome.layoutTodayWorkout.recyclerView.setAdapter(new TodaysWorkoutAdapter(mActivity, dataBean.getTodays_workout(), this));
        else
            mBinding.layoutHome.layoutTodayWorkout.recyclerView.setAdapter(new TodaysWorkoutAdapter(mActivity, new ArrayList<>(), this));
        if (dataBean.getIs_assign() == 1) {
            if ((dataBean.getTodays_diet() != null && dataBean.getTodays_diet().size() > 0) || (dataBean.getTodays_workout() != null && dataBean.getTodays_workout().size() > 0)) {
                mBinding.layoutHome.getRoot().setVisibility(View.VISIBLE);
                mBinding.imgRestDay.setVisibility(View.GONE);
                mBinding.txtRestDay.setVisibility(View.GONE);
                if (dataBean.getTodays_diet() != null && dataBean.getTodays_diet().size() > 0)
                    mBinding.layoutHome.layoutTodayDiet.recyclerView.setAdapter(new TodaysDietAdapter(mActivity, dataBean.getTodays_diet(), this));
                else
                    mBinding.layoutHome.layoutTodayDiet.recyclerView.setAdapter(new TodaysDietAdapter(mActivity, new ArrayList<>(), this));
                if (dataBean.getTodays_workout() != null && dataBean.getTodays_workout().size() > 0)
                    mBinding.layoutHome.layoutTodayWorkout.recyclerView.setAdapter(new TodaysWorkoutAdapter(mActivity, dataBean.getTodays_workout(), this));
                else
                    mBinding.layoutHome.layoutTodayWorkout.recyclerView.setAdapter(new TodaysWorkoutAdapter(mActivity, new ArrayList<>(), this));

            } else {
                mBinding.layoutHome.getRoot().setVisibility(View.GONE);
                mBinding.imgRestDay.setVisibility(View.VISIBLE);
                mBinding.txtRestDay.setVisibility(View.VISIBLE);
                mBinding.txtRestDay.setText("Today is Rest day");

            }
        } else {
            mBinding.layoutHome.getRoot().setVisibility(View.GONE);
            mBinding.imgRestDay.setVisibility(View.VISIBLE);
            mBinding.txtRestDay.setVisibility(View.VISIBLE);
            mBinding.txtRestDay.setText("For training program, Please contact to Trainer or Gym...");
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onStart(GetClientDetailModel.DataBean.TodaysWorkoutBean workoutBean) {
        StartWorkoutHomeDialog dialog = new StartWorkoutHomeDialog();
        Bundle bundle = new Bundle();
        bundle.putString("workout_name", workoutBean.getWorkout_name());
        bundle.putInt("workout_id", workoutBean.getId());
        bundle.putInt("training_program_id", workoutBean.getTraining_program_id());
        bundle.putBoolean("is_am_workout", workoutBean.isIs_am_workout());
        bundle.putInt("weekday_id", 0);
//        bundle.putParcelable("data", workoutBean);
        bundle.putParcelableArrayList("data", dataBean.getTodays_workout());
        bundle.putParcelableArrayList("dataFollow", dataBean.getClients_workout());
        dialog.setArguments(bundle);
        dialog.setTargetFragment(this, 100);
        dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), StartWorkoutDialog.class.getSimpleName());
        dialog.setCancelable(false);
    }

    private void initLiveClientSocket() {
        channels = new ArrayList<>();
        if (session.getAuthorizedUser(mActivity).isDirect_gym_client()) {
            channels.add(session.getAuthorizedUser(mActivity).getDirect_gym_id());
            if (session.getAuthorizedUser(mActivity).isClient_trainer()) {
                channels.add(session.getAuthorizedUser(mActivity).getTrainer_id());
            }
        } else
            channels.add(session.getAuthorizedUser(mActivity).getTrainer_id());


        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        pubnub = new PubNub(pnConfiguration);

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // create message payload using Gson
        JsonObject messageJsonObject = new JsonObject();
        messageJsonObject.addProperty("id", session.getAuthorizedUser(mActivity).getId());
        messageJsonObject.addProperty("name", session.getAuthorizedUser(mActivity).getName());
        messageJsonObject.addProperty("photo", session.getAuthorizedUser(mActivity).getPhoto());
        messageJsonObject.addProperty("email", session.getAuthorizedUser(mActivity).getEmail());
        messageJsonObject.addProperty("date", "" + spf.format(Calendar.getInstance().getTime()));
        messageJsonObject.addProperty("isStartedWorkout", true);

        System.out.println("Message to send: " + messageJsonObject.toString());

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost
//                    Toast.makeText(mActivity, "connectivity is lost", Toast.LENGTH_LONG).show();
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

//                    Toast.makeText(mActivity, "Connect event", Toast.LENGTH_LONG).show();
                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
                        for (String channelName : channels)
                            pubnub.publish().channel(channelName).message(messageJsonObject).async(new PNCallback<PNPublishResult>() {
                                @Override
                                public void onResponse(PNPublishResult result, PNStatus status) {
                                    // Check whether request successfully completed or not.
//                                    Toast.makeText(mActivity, "request successfully completed", Toast.LENGTH_LONG).show();

                                    if (!status.isError()) {

//                                        Toast.makeText(mActivity, "Message successfully published to specified channel", Toast.LENGTH_LONG).show();

                                        // Message successfully published to specified channel.
                                    }
                                    // Request processing failed.
                                    else {

//                                        Toast.makeText(mActivity, "Handle message publish error", Toast.LENGTH_LONG).show();

                                        status.retry();
                                        // Handle message publish error. Check 'category' property to find out possible issue
                                        // because of which request did fail.
                                        //
                                        // Request can be resent using: [status retry];
                                    }
                                }
                            });
                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {

                // Handle new message stored in message.message
                if (message.getChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscription()
                }

                /*JsonElement receivedMessageObject = message.getMessage();
                System.out.println("Received message content: " + receivedMessageObject.toString());
                // extract desired parts of the payload, using Gson
                String msg = message.getMessage().getAsJsonObject().get("msg").getAsString();
                System.out.println("msg content: " + msg);
                System.out.println("msg subscription: " + message.getSubscription());
                System.out.println("msg time token: " + message.getTimetoken());*/


                                    /*
                                        log the following items with your favorite logger
                                            - message.getMessage()
                                            - message.getSubscription()
                                            - message.getTimetoken()
                                    */
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });
        pubnub.subscribe().channels(channels).execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            getClientDetail();
        }
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
            // initLiveClientSocket();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pubnub != null)
            pubnub.unsubscribe()
                    .channels(channels)
                    .execute();
    }

    @Override
    public void selected(List<Integer> listChecked, List<Integer> listUnChecked, int id) {
        if (listChecked != null) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm a");

            String dateString = date.format(calendar.getTime());
            String timeString = time.format(calendar.getTime());
            StringBuilder idsCheked = new StringBuilder();
            StringBuilder idsUnChecked = new StringBuilder();
            for (int data : listChecked)
                idsCheked.append(data).append(",");
            for (int data : listUnChecked)
                idsUnChecked.append(data).append(",");

            String idsTrue = idsCheked.substring(0, idsCheked.length() - 1);
            String idsFalse = listUnChecked.size() > 0 ? idsUnChecked.substring(0, idsUnChecked.length() - 1) : "";

            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);

                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).startDiet(dateString, timeString, idsTrue, idsFalse, id)
                        , getCompositeDisposable(), single, this);
            } else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        } else {
            showSnackBar(mBinding.mainLayout, "Please select diet to start", Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgTrainer:
                if (dataBean != null && dataBean.getPhoto() != null) {
                    startActivity(new Intent(getContext(), ClientDocumentViewActivity.class).putExtra("data", dataBean.getPhoto()));
                }
                break;

        }
    }

    private void changeDate(int training_program_id) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Fitzoh")
                .setMessage("Do you want to change training program date?")
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, whichButton) -> {
                    dialog.dismiss();
                    showDatePickerDialog(training_program_id);

                })
                .setNegativeButton(R.string.no, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");

                    String dateString = date.format(calendar.getTime());
                    mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).updateWorkoutDate(dateString, training_program_id)
                            , getCompositeDisposable(), updateDate, this);

                })
                .show();

        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
    }

    private void showDatePickerDialog(int training_program_id) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                (view, year, monthOfYear, dayOfMonth) -> {
                    c.set(Calendar.YEAR, year);
                    c.set(Calendar.MONTH, monthOfYear);
                    c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String inputDate = dateFormat.format(c.getTime());
                    SimpleDateFormat spf = new SimpleDateFormat("dd/MM/yyyy");
                    Date newDate = null;
                    try {
                        newDate = spf.parse(inputDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    spf = new SimpleDateFormat("dd-MM-yyyy");
                    String date = spf.format(newDate);
                    System.out.println(date);

                    updateDate(training_program_id, date);

                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    private void updateDate(int training_program_id, String date) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).updateWorkoutDate(date, training_program_id)
                , getCompositeDisposable(), updateDate, this);

    }

    @Override
    public void onRefresh() {
        getClientDetail();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
