package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.MedicalFormAdapter;
import com.fitzoh.app.databinding.FragmentMedicalFormBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.MedicalFormModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AppThemeSettingActivity;
import com.fitzoh.app.ui.activity.NavigationClientMainActivity;
import com.fitzoh.app.ui.activity.NavigationMainActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.editMedicalForm;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentMedicalForm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMedicalForm extends BaseFragment implements SingleCallback {
    FragmentMedicalFormBinding mBinding;
    String userId, userAccessToken;
    private List<MedicalFormModel.DataBean> modelList;
    private MedicalFormAdapter medicalFormAdapter;
    boolean isFromRegister = true;

    public FragmentMedicalForm() {
        // Required empty public constructor
    }


    public static FragmentMedicalForm newInstance() {
        return new FragmentMedicalForm();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_medical_form, container, false);
        Utils.getShapeGradient(mActivity, mBinding.medicalForm.btnLetsGo);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
//        mBinding.medicalForm.recyclerView.setAdapter(new MedicalFormAdapter(getAdapterData()));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        if (getArguments() != null && getArguments().containsKey("Is_FROM_REGISTER")) {
            isFromRegister = getArguments().getBoolean("Is_FROM_REGISTER");
        }
        if (isFromRegister) {
            setupToolBar(mBinding.toolbar.toolbar, getString(R.string.medical_form_toolbar_title));
            mBinding.toolbar.tvTitle.setGravity(Gravity.CENTER);

        } else {
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.medical_form_toolbar_title));
        }
        if (Utils.isOnline(getContext())) {
            if (isFromRegister)
                getMedicalFormData();
            else
                getMedicalFormDataEdit();

        } else
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        mBinding.medicalForm.btnLetsGo.setOnClickListener(view -> {
            if (Utils.isOnline(getContext())) {
                if (validation()) {
                    callSaveAPI();
                }
            } else
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);

        });
        return mBinding.getRoot();

    }

    private void getMedicalFormDataEdit() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientmedicalform()
                , getCompositeDisposable(), editMedicalForm, this);
    }

    private void callSaveAPI() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getJsonString());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).submitMedicalFormData(requestBody)
                , getCompositeDisposable(), save, this);
    }

    private String getJsonString() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < medicalFormAdapter.getData().size(); i++) {
            JSONObject jsonObject = new JSONObject();
            MedicalFormModel.DataBean bean = medicalFormAdapter.getData().get(i);
            try {
                jsonObject.put("question_id", bean.getId());
                if (bean.getType() == 2) {
                    if (bean.getDropdown_values() != null && bean.getSelectedPosition() >= 0) {
                        jsonObject.put("answer_id", bean.getDropdown_values().get(bean.getSelectedPosition()).getId());
                    } else {
                        jsonObject.put("answer_id", 0);
                    }
                } else {
                    jsonObject.put("answer", bean.getSelectedPosition() == 1);
                }
                if (!bean.getNotes().equalsIgnoreCase(""))
                    jsonObject.put("notes", bean.getNotes());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray.toString();
    }

    private boolean validation() {
        List<MedicalFormModel.DataBean> dataBeans = medicalFormAdapter.getData();

        for (int i = 0; i < dataBeans.size(); i++) {
            MedicalFormModel.DataBean data = dataBeans.get(i);
        }
        return true;
    }

    private void getMedicalFormData() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).viewMedicalFormFields()
                , getCompositeDisposable(), single, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                MedicalFormModel medicalFormModel = (MedicalFormModel) o;
                if (medicalFormModel != null && medicalFormModel.getStatus() == AppConstants.SUCCESS && medicalFormModel.getData() != null && medicalFormModel.getData().size() > 0) {
                    modelList = medicalFormModel.getData();
                    medicalFormAdapter = new MedicalFormAdapter(mActivity, modelList, true);
                    mBinding.medicalForm.recyclerView.setAdapter(medicalFormAdapter);
                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    if (isFromRegister) {
                        session.setLogin(true);
                        Intent intent = new Intent(mActivity, NavigationClientMainActivity.class);
                        intent.putExtra("IS_LOGIN", true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                        getActivity().finish();
                    }


                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case editMedicalForm:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                MedicalFormModel medicalFormModelEdit = (MedicalFormModel) o;
                if (medicalFormModelEdit != null && medicalFormModelEdit.getStatus() == AppConstants.SUCCESS && medicalFormModelEdit.getData() != null && medicalFormModelEdit.getData().size() > 0) {
                    modelList = medicalFormModelEdit.getData();
                    medicalFormAdapter = new MedicalFormAdapter(mActivity, modelList, false);
                    mBinding.medicalForm.recyclerView.setAdapter(medicalFormAdapter);
                } else {
                    showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }
}
