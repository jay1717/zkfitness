package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.NutritionListAdapter;
import com.fitzoh.app.databinding.FragmentNutritionBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityFindPlan;
import com.fitzoh.app.ui.activity.AssignNutritionActivity;
import com.fitzoh.app.ui.activity.EditNutritionActivity;
import com.fitzoh.app.ui.dialog.AddDietDialog;
import com.fitzoh.app.ui.dialog.AddWorkoutDialog;
import com.fitzoh.app.ui.dialog.RenameDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.DietList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteDiet;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentNutrition#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentNutrition extends BaseFragment implements NutritionListAdapter.onAddClient, SingleCallback, NutritionListAdapter.DeleteWorkOut, AddDietDialog.DialogClickListener, SwipeRefreshLayout.OnRefreshListener, RenameDialog.DialogClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    FragmentNutritionBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<DietListModel.DataBean> dietList;
    NutritionListAdapter nutritionListAdapter;


    public FragmentNutrition() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentWorkout.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentNutrition newInstance() {
        FragmentNutrition fragment = new FragmentNutrition();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.nutrition_plans));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_nutrition, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.layoutNutrition.fab);
        mBinding.layoutNutrition.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutNutrition.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.layoutNutrition.fab.setOnClickListener(view -> {
            AddDietDialog dialog = new AddDietDialog();
            dialog.setListener(this);
            dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddWorkoutDialog.class.getSimpleName());
            dialog.setCancelable(false);
        });
        setHasOptionsMenu(true);
//        mBinding.toolbar.btnFindplan.setVisibility(View.VISIBLE);
//        mBinding.toolbar.btnFindplan.setOnClickListener(view -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "diet_plan")));
        dietList = new ArrayList<>();
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutNutrition.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        nutritionListAdapter = new NutritionListAdapter(getActivity(), this, dietList, this);
        mBinding.layoutNutrition.recyclerView.setAdapter(nutritionListAdapter);
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callNutritionApi();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }

        return mBinding.getRoot();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_notification:
                break;
        }
        return true;

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> startActivity(new Intent(getActivity(), ActivityFindPlan.class).putExtra("type", "diet_plan")));
    }


    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isOnline(getContext()))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    private void callNutritionApi() {
        mBinding.layoutNutrition.swipeContainer.setRefreshing(false);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getDietList()
                , getCompositeDisposable(), DietList, this);
    }

    /*private void callWorkoutDelete(int workoutId) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                , getCompositeDisposable(), deleteWorkout, this);
    }*/

    @Override
    public void add(int id) {
//        Toast.makeText(getActivity(), "clicked...!", Toast.LENGTH_SHORT).show();
        mActivity.startActivityForResult(new Intent(getActivity(), AssignNutritionActivity.class).putExtra("dietId", id), 1);
    }

    @Override
    public void edit(int id, String name) {
        session.editor.remove(CLIENT_ID).commit();
        startActivity(new Intent(getActivity(), EditNutritionActivity.class).putExtra("dietId", id).putExtra("dietName", name));

    }

    @Override
    public void rename(DietListModel.DataBean dataBean) {
        RenameDialog dialogNote = new RenameDialog(dataBean.getId(), 0, 0, dataBean.getName());
        dialogNote.setListener(this);
        dialogNote.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), RenameDialog.class.getSimpleName());
        dialogNote.setCancelable(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            if (Utils.isOnline(mContext))
                callNutritionApi();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case DietList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                dietList = new ArrayList<>();
                DietListModel dietListModel = (DietListModel) o;
                if (dietListModel.getStatus() == AppConstants.SUCCESS) {

                    if (dietListModel.getData() != null) {
                        dietList.addAll(dietListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (dietList.size() == 0) {
                            mBinding.layoutNutrition.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutNutrition.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutNutrition.imgNoData.setVisibility(View.GONE);
                            mBinding.layoutNutrition.recyclerView.setVisibility(View.VISIBLE);
                            nutritionListAdapter = new NutritionListAdapter(getActivity(), this, dietList, this);
                            mBinding.layoutNutrition.recyclerView.setAdapter(nutritionListAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, dietListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case deleteDiet:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse deleteDiet = (CommonApiResponse) o;
                if (deleteDiet.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext()))
                        callNutritionApi();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteDiet.getMessage(), Snackbar.LENGTH_LONG);

                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    @Override
    public void delete(DietListModel.DataBean dietListModel) {
        if (dietListModel != null) {
            if (Utils.isOnline(getActivity())) {
                callWorkoutDelete(dietListModel.getId());
            } else {
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
        }
    }

    private void callWorkoutDelete(int dietId) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteDiet(dietId)
                , getCompositeDisposable(), deleteDiet, this);
    }

    @Override
    public void setClick() {
        if (Utils.isOnline(getContext()))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onRefresh() {
        if (Utils.isOnline(getContext()))
            callNutritionApi();
        else {
            showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }
}
