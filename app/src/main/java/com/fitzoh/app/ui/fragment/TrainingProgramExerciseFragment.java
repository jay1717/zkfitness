package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramExerciseMainAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramExerciseBinding;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.LogWorkoutExerciseListActivity;
import com.fitzoh.app.ui.dialog.StartWorkoutDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;


public class TrainingProgramExerciseFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {
    FragmentTrainingProgramExerciseBinding mBinding;
    String userId, userAccessToken;
    private List<List<WorkoutExerciseListModel.DataBean>> listDatas = new ArrayList<>();
    private int workout_id = 0, training_program_id = 0, weekday_id = 0;
    private String workout_name = "";
    private boolean is_am_workout = false;

    public TrainingProgramExerciseFragment() {
    }


    public static TrainingProgramExerciseFragment newInstance(Bundle bundle) {
        TrainingProgramExerciseFragment fragment = new TrainingProgramExerciseFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            workout_id = getArguments().getInt("workout_id", 0);
            workout_name = getArguments().getString("workout_name", "");
            training_program_id = getArguments().getInt("training_program_id", 0);
            is_am_workout = getArguments().getBoolean("is_am_workout", false);
            weekday_id = getArguments().getInt("weekday_id", 0);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, workout_name);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_training_program_exercise, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();

        mBinding.layoutTrainingProgram.recyclerView.setAdapter(new TrainingProgramExerciseMainAdapter(getContext(), new ArrayList<>()));
        Utils.getShapeGradient(mActivity, mBinding.btnSave);
        mBinding.layoutTrainingProgram.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutTrainingProgram.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        if (getArguments() != null && getArguments().containsKey("isActive") && getArguments().getInt("isActive") == 0)
            mBinding.btnSave.setVisibility(View.VISIBLE);
        mBinding.btnSave.setOnClickListener(view -> {
            if (getArguments() != null && getArguments().containsKey("isFromWorkout") && getArguments().getBoolean("isFromWorkout", false)) {
                startActivity(new Intent(mActivity, LogWorkoutExerciseListActivity.class).putExtra("workout_id", workout_id).putExtra("workout_name", workout_name).putExtra("isFromWorkout", true).putExtra("isSetEditable", true).putExtra("is_show_log", false).putExtra("is_show_log", 0));
            } else {
                StartWorkoutDialog dialog = new StartWorkoutDialog();
                Bundle bundle = new Bundle();
                bundle.putString("workout_name", workout_name);
                bundle.putInt("workout_id", workout_id);
                bundle.putInt("training_program_id", training_program_id);
                bundle.putBoolean("is_am_workout", is_am_workout);
                bundle.putInt("weekday_id", weekday_id);
                bundle.putBoolean("is_training_program_workout",true);
                dialog.setArguments(bundle);
                dialog.setTargetFragment(this, 0);
                dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), StartWorkoutDialog.class.getSimpleName());
                dialog.setCancelable(false);
            }
        });
        callExerciseList();
        return mBinding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callExerciseList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }


    private void callExerciseList() {
        if (Utils.isOnline(mActivity)) {
            if (getArguments() != null && getArguments().containsKey("isFromWorkout") && getArguments().getBoolean("isFromWorkout", false)) {
                mBinding.btnSave.setText("Track Workout");
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOutExerciseListClient(workout_id)
                        , getCompositeDisposable(), workoutList, this);
            } else {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOutExerciseList(workout_id, session.getStringDataByKeyNull(CLIENT_ID))
                        , getCompositeDisposable(), workoutList, this);
            }
        }
        else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutExerciseListModel workoutExerciseListModel = (WorkoutExerciseListModel) o;
                if (workoutExerciseListModel.getStatus() == AppConstants.SUCCESS) {
                    listDatas = workoutExerciseListModel.getData();
                    if (listDatas != null) {
                        if (listDatas.size() == 0) {
                            mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.GONE);
                            mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.VISIBLE);
                        } else {
                            mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.GONE);
                            mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutTrainingProgram.recyclerView.setAdapter(new TrainingProgramExerciseMainAdapter(mActivity, listDatas));
                        }
                    } else
                        showSnackBar(mBinding.mainLayout, workoutExerciseListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case workoutList:
                mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.GONE);
                mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onRefresh() {
        callExerciseList();
        mBinding.layoutTrainingProgram.swipeContainer.setRefreshing(false);
    }
}
