package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.CheckInsDataAdapter;
import com.fitzoh.app.databinding.FragmentMyClientCheckInListBinding;
import com.fitzoh.app.model.CheckInDataModel;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.liveClients;


public class MyClientCheckInListFragment extends BaseFragment implements SingleCallback, SwipeRefreshLayout.OnRefreshListener {

    FragmentMyClientCheckInListBinding mBinding;
    String userId, userAccessToken;
    private ClientListModel.DataBean dataBean;

    public MyClientCheckInListFragment() {

    }


    public static MyClientCheckInListFragment newInstance(ClientListModel.DataBean dataBean) {
        MyClientCheckInListFragment fragment = new MyClientCheckInListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", dataBean);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (ClientListModel.DataBean) getArguments().get("data");

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_my_client_check_in_list, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        if (Utils.isOnline(mActivity)) {
            getLiveClients();
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        return mBinding.getRoot();

    }

    private void getLiveClients() {
        mBinding.swipeContainer.setRefreshing(false);
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getCheckInsList(dataBean.getId())
                , getCompositeDisposable(), liveClients, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case liveClients:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CheckInDataModel liveClientsModel = (CheckInDataModel) o;
                if (liveClientsModel != null && liveClientsModel.getStatus() == AppConstants.SUCCESS && liveClientsModel.getData() != null && liveClientsModel.getData().size() > 0) {
                    mBinding.recyclerView.setAdapter(new CheckInsDataAdapter(liveClientsModel.getData(), mActivity));
                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                    mBinding.imgNoData.setVisibility(View.GONE);
                } else {
                    mBinding.recyclerView.setVisibility(View.GONE);
                    mBinding.imgNoData.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case liveClients:
                mBinding.recyclerView.setVisibility(View.GONE);
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onRefresh() {
        if (Utils.isOnline(mActivity)) {
            getLiveClients();
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }
}
