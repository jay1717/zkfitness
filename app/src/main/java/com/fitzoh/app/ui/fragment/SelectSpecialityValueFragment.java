package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SelectSpecialityAdapter;
import com.fitzoh.app.databinding.FragmentSelectSpecialityBinding;
import com.fitzoh.app.model.SpecialityListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.custom_ui.ChipsViewSpeciality;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.specialityList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SelectSpecialityValueFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SelectSpecialityValueFragment extends BaseFragment implements SingleCallback {


    public FragmentSelectSpecialityBinding mBinding;
    public static SelectSpecialityValueFragment fitnessFragment;
    String userId, userAccessToken;
    SelectSpecialityAdapter mAdapter;
    List<SpecialityListModel.DataBean> fitnessDataList = new ArrayList<>();
    int fitnessCenterId = 0;
    public HashMap<String, SpecialityListModel.DataBean> selectedFitneeList = new HashMap<String, SpecialityListModel.DataBean>();


    public SelectSpecialityValueFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static SelectSpecialityValueFragment newInstance() {
        SelectSpecialityValueFragment fragment = new SelectSpecialityValueFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_speciality, container, false);
        Utils.getShapeGradient(mActivity, mBinding.btnRegister);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        fitnessFragment = this;
        Intent intent = getActivity().getIntent();
        if (null != intent) {
            fitnessCenterId = intent.getExtras().getInt("fitnessCenterId");
        }
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        prepareLayout();
        return mBinding.getRoot();
    }

    private void prepareLayout() {
        callFitnessValueApi();
        setChipsView();
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Select Speciality");
        searchFitnessValue();
        mBinding.btnRegister.setOnClickListener(view -> {
            if (selectedFitneeList.size() > 0) {
                Intent resultIntent = new Intent();
// TODO Add extras or a data URI to this intent as appropriate.
                resultIntent.putExtra("Selected Data", selectedFitneeList);
                getActivity().setResult(Activity.RESULT_OK, resultIntent);
                getActivity().finish();
            } else {
                getActivity().onBackPressed();
            }

        });
    }

    private void searchFitnessValue() {

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mAdapter != null)
                    mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setChipsView() {
        mBinding.chipsView.setTypeface(ResourcesCompat.getFont(getActivity(), R.font.avenirnextltpro_regular));
        // change EditText config
        mBinding.chipsView.getEditText().setCursorVisible(false);
        mBinding.chipsView.getEditText().setTextIsSelectable(false);
        mBinding.chipsView.getEditText().setClickable(false);

        mBinding.chipsView.setChipsValidator(new ChipsViewSpeciality.ChipValidator() {


            @Override
            public boolean isValid(SpecialityListModel.DataBean contact) {
                return false;
            }
        });

        mBinding.chipsView.setChipsListener(new ChipsViewSpeciality.ChipsListener() {
            @Override
            public void onChipAdded(ChipsViewSpeciality.Chip chip) {
                for (ChipsViewSpeciality.Chip chipItem : mBinding.chipsView.getChips()) {
                    Log.d("ChipList", "chip: " + chipItem.toString());
                }
            }

            @Override
            public void onChipDeleted(ChipsViewSpeciality.Chip chip) {
                selectedFitneeList.remove(String.valueOf(chip.getContact().getId()));
                if (fitnessDataList.contains(chip.getContact())) {
                    fitnessDataList.get(fitnessDataList.indexOf(chip.getContact())).setSelected(false);
                    mAdapter.disSelectItem(fitnessDataList.indexOf(chip.getContact()));
                }
            }

            @Override
            public void onTextChanged(CharSequence text) {
//                mAdapter.filterItems(text);
            }

            @Override
            public boolean onInputNotRecognized(String text) {

                return false;
            }
        });

        if (getArguments() != null) {
            selectedFitneeList = (HashMap<String, SpecialityListModel.DataBean>) getArguments().getSerializable("peopleList");
            if (selectedFitneeList.size() > 0) {
                Iterator myVeryOwnIterator = selectedFitneeList.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    SpecialityListModel.DataBean value = selectedFitneeList.get(key);
                    value.setSelected(false);
                    mBinding.chipsView.addChip(value.getName(), "", value, false);
                }
            }
        }

    }


    private void callFitnessValueApi() {
        if (Utils.isOnline(getContext())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSpecialityList(fitnessCenterId)
                    , getCompositeDisposable(), specialityList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }


   /* public void addTextListener() {

        mBinding.edtSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                query = query.toString().toLowerCase();
                String description = query.toString();

                // List<DiaryObject> mDiaryList = mDBhelper.getDiaryInfoSearch(description);

                if (fitnessDataList != null) {
                }
                int sizeOfdata = fitnessDataList.size();
                mAdapter = new SelectFitnessAdapter();
                HashMap<String, ArrayList<FitnessValuesModel.DataBean>> hashMapFitnessData = new HashMap<String, ArrayList<FitnessValuesModel.DataBean>>();

                HashMap<Integer, String> hashCategory = mDBhelper.getCategoryHashMap();

                for (int i = 0; i < sizeOfdata; i++) {
                    FitnessValuesModel.DataBean fitnessObject = fitnessDataList.get(i);
                    String catName = hashCategory.get(fitnessObject.get());
                    ArrayList<DiaryObject> arrDiary = hashMapDiary.get(catName);
                    if (arrDiary != null && arrDiary.size() > 0) {
                        arrDiary.add(diaryObject);
                    } else {
                        arrDiary = new ArrayList<DiaryObject>();
                        arrDiary.add(diaryObject);
                    }
                    hashMapDiary.put(catName, arrDiary);
                    *//*
                    List<DiaryObject> arrDiary = mDBhelper.getDiarytList(diaryCategoryObject.getId());
                   *//*

                }
                if (hashMapDiary != null && hashMapDiary.size() > 0) {


                    Iterator myVeryOwnIterator = hashMapDiary.keySet().iterator();
                    while (myVeryOwnIterator.hasNext()) {
                        String key = (String) myVeryOwnIterator.next();
                        ArrayList<DiaryObject> value = (ArrayList<DiaryObject>) hashMapDiary.get(key);
                        linAddCategory.setVisibility(View.GONE);
                        sectionAdapter.addSection(new ContactsSection(key, value));

                        // Toast.makeText(ctx, "Key: "+key+" Value: "+value, Toast.LENGTH_LONG).show();
                    }


                }
                recyclerView.setAdapter(sectionAdapter);
// data set changed
            }
        });
    }
*/

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case specialityList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SpecialityListModel specialityListModel = (SpecialityListModel) o;
                if (specialityListModel != null && specialityListModel.getData() != null && specialityListModel.getData().size() > 0) {
                    fitnessDataList.clear();
                    fitnessDataList = specialityListModel.getData();
                    mBinding.recyclerPeople.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mAdapter = new SelectSpecialityAdapter(getContext(), fitnessDataList);
                    mBinding.recyclerPeople.setAdapter(mAdapter);
                    for (int i = 0; i < specialityListModel.getData().size(); i++) {
                      /*  KeyPairBoolData h = new KeyPairBoolData();
                        h.setId(fitnessValuesModel.getData().get(i).getId());
                        h.setName(fitnessValuesModel.getData().get(i).getValue());
                        h.setSelected(false);
                        listArraySpinner.add(h);*/
                    }

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, "onFailure", Snackbar.LENGTH_LONG);

    }
}
