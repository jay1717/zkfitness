package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.EditTrainingProgramDayAdapter;
import com.fitzoh.app.adapter.EditTrainingProgramWeekAdapter;
import com.fitzoh.app.adapter.FindTrainingProgramWeekAdapter;
import com.fitzoh.app.databinding.FragmentEditTrainingProgramBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.TrainingProgramDay;
import com.fitzoh.app.model.TrainingProgramEditModel;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.model.TrainingProgramWeek;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.LogWorkoutExerciseListActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.trainingList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditTrainingProgramFragment extends BaseFragment implements SingleCallback, EditTrainingProgramDayAdapter.TrainingProgramListener, SwipeRefreshLayout.OnRefreshListener, FindTrainingProgramWeekAdapter.RedirectToDetailListener {


    public FragmentEditTrainingProgramBinding mBinding;
    public EditTrainingProgramWeekAdapter recyclerViewAdapter;
    String userId, userAccessToken;
    TrainingProgramList.DataBean dataBean;
    private List<Integer> deletedDaysList = new ArrayList<>();
    private List<Integer> deletedWeekList = new ArrayList<>();

    public EditTrainingProgramFragment() {
        // Required empty public constructor
    }


    public static EditTrainingProgramFragment newInstance(Bundle bundle) {
        EditTrainingProgramFragment fragment = new EditTrainingProgramFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (TrainingProgramList.DataBean) getArguments().getSerializable("data");
        }
        setRetainInstance(true);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mBinding == null) {
            mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_edit_training_program, container, false);
            setupToolBarWithBackArrow(mBinding.toolbar.toolbar, dataBean.getTitle() + "Training Program");
            Utils.getShapeGradient(mActivity, mBinding.btnSave);
            Utils.setAddFabBackground(mActivity, mBinding.fab);
            mBinding.btnSave.setVisibility(View.GONE);
            mBinding.layoutTrainingProgram.swipeContainer.setOnRefreshListener(this);
            mBinding.layoutTrainingProgram.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
            mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            Utils.setAddFabBackground(mActivity, mBinding.fab);
            prepareLayout();

        }

        return mBinding.getRoot();
    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        recyclerViewAdapter = new EditTrainingProgramWeekAdapter(mActivity, this,this);
        LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(mActivity);
        mBinding.layoutTrainingProgram.recyclerView.setLayoutManager(recyclerLayoutManager);
        mBinding.layoutTrainingProgram.recyclerView.setAdapter(recyclerViewAdapter);
        mBinding.fab.setOnClickListener(view -> {
            recyclerViewAdapter.addNewWeek();
        });
        mBinding.btnSave.setOnClickListener(view -> {
            saveTrainingProgram();
        });
        callTrainingProgram();
    }


    private void callTrainingProgram() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWeekWiseTrainingProgram(dataBean.getId(), session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), trainingList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void saveTrainingProgram() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getJsonData());
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).addWeekWiseTrainingProgram(requestBody)
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private String getJsonData() {
        JSONObject finalObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            finalObject.put("client_id", session.getStringDataByKeyNull(CLIENT_ID));
            finalObject.put("deleted_week_id", new JSONArray(deletedWeekList));
            finalObject.put("deleted_day_id", new JSONArray(deletedDaysList));

            for (int i = 0; i < recyclerViewAdapter.weekList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("training_program_id", dataBean.getId());
                jsonObject.put("week_number", i + 1);
                jsonObject.put("id", recyclerViewAdapter.weekList.get(i).getId());
                JSONArray array = new JSONArray();
                List<TrainingProgramDay> weekDays = recyclerViewAdapter.weekList.get(i).getWeekdays();
                for (int j = 0; j < weekDays.size(); j++) {
                    JSONObject object = new JSONObject();
                    object.put("day_number", (j + 1));
                    object.put("id", weekDays.get(j).getId());
                    object.put("isRestfulDay", weekDays.get(j).getIs_restfull_day());
                    object.put("workoutAMId", weekDays.get(j).getWorkout_am_id());
                    object.put("workoutPMId", weekDays.get(j).getWorkout_pm_id());
                    object.put("dietPlanId", weekDays.get(j).getDiet_plan_id());
                    object.put("is_checkin", weekDays.get(j).getIs_checkin());
                    array.put(object);
                }
                jsonObject.put("days", array);
                jsonArray.put(jsonObject);
            }
            finalObject.put("weeks", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return finalObject.toString();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case trainingList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TrainingProgramEditModel editModel = (TrainingProgramEditModel) o;
                if (editModel != null && editModel.getStatus() == AppConstants.SUCCESS && editModel.getData() != null && editModel.getData().size() > 0) {
                    recyclerViewAdapter.addDataToAdapter(editModel.getData());
                } else {
                    setAdapter();
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
//                    mActivity.onBackPressed();
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                    mActivity.finish();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, throwable.getMessage(), Snackbar.LENGTH_LONG);
        switch (apiNames) {
            case trainingList:
                setAdapter();
                break;
            case single:
                mActivity.finish();
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            switch (requestCode) {
                case 0:
                    recyclerViewAdapter.setAMWorkout(data);
                    break;
                case 100:
                    recyclerViewAdapter.setPMWorkout(data);
                    break;
                case 500:
                    recyclerViewAdapter.setNutrition(data);
                    break;
                case 200:
//                    Log.e("onActivityResult: ", "" + 200);
                    recyclerViewAdapter.setCopyDay(data);
                    break;
            }
        }
    }

    @Override
    public void delete(String name, int id) {
        if (id != 0)
            switch (name) {
                case "day":
                    deletedDaysList.add(id);
                    break;
                case "week":
                    deletedWeekList.add(id);
                    break;
            }
    }

    @Override
    public void refresh(int id) {
        if (id != -1)
            recyclerViewAdapter.notifyItemChanged(id);
        setAdapter();
    }

    @Override
    public void showPopup(int id, View view) {
        HashMap<String, Integer> list = new HashMap<>();
        PopupMenu menu = new PopupMenu(mActivity, view);
        for (int i = 0; i < recyclerViewAdapter.weekList.size(); i++) {
            if (i != id) {
                list.put("Week " + (i + 1), i);
                menu.getMenu().add("Week " + (i + 1));
            }
        }
        if (list.size() > 0)
            menu.show();
        menu.setOnMenuItemClickListener(menuItem -> {
            String title = (String) menuItem.getTitle();
            int selectedId = list.get(title);
            TrainingProgramWeek week = recyclerViewAdapter.weekList.get(selectedId);
            TrainingProgramWeek weekSelected = recyclerViewAdapter.weekList.get(id);
            recyclerViewAdapter.copyWeek(id, selectedId);
            Toast.makeText(getContext(), R.string.week_copied_successfully, Toast.LENGTH_LONG).show();
            return false;
        });


    }

    private void setAdapter() {
        if (recyclerViewAdapter.weekList.size() > 0) {
            mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.VISIBLE);
            mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.GONE);
        } else {
            mBinding.layoutTrainingProgram.recyclerView.setVisibility(View.GONE);
            mBinding.layoutTrainingProgram.imgNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        callTrainingProgram();
        mBinding.layoutTrainingProgram.swipeContainer.setRefreshing(false);
    }

    @Override
    public void openExerciseDetail(int workout_id, String workout_name, boolean isAM, int day_id) {

    }

    @Override
    public void viewLogWorkout(int workout_id, String workout_name, boolean isAM, int day_id) {
        startActivity(new Intent(mActivity, LogWorkoutExerciseListActivity.class)
                .putExtra("workout_id", workout_id)
                .putExtra("workout_name", workout_name)
                .putExtra("training_program_id", dataBean.getId())
                .putExtra("is_am_workout", isAM)
                .putExtra("isSetEditable", false)
                .putExtra("weekday_id", day_id));
    }
}
