package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.SelectExerciseAdapter;
import com.fitzoh.app.databinding.FragmentSelectExerciseBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;


public class SelectExerciseFragment extends BaseFragment implements SingleCallback, SelectExerciseAdapter.SelectExerciseListener, SwipeRefreshLayout.OnRefreshListener {
    private FragmentSelectExerciseBinding mBinding;
    private String userId, userAccessToken;
    private WorkoutExerciseListModel.DataBean dataBean;
    private List<List<WorkoutExerciseListModel.DataBean>> listDatas = new ArrayList<>();
    private SelectExerciseAdapter selectExerciseAdapter;
    private int workoutId = 0;

    public SelectExerciseFragment() {
    }


    public static SelectExerciseFragment newInstance(Bundle bundle) {
        SelectExerciseFragment fragment = new SelectExerciseFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (WorkoutExerciseListModel.DataBean) getArguments().get("data");
            workoutId = getArguments().getInt("id", 0);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.super_giant));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_exercise, container, false);
       // Utils.getShapeGradient(mActivity, mBinding.layoutSelectExercise.btnSave);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();
        mBinding.layoutSelectExercise.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutSelectExercise.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.layoutSelectExercise.itemSelectExercise.checkboxClientAssign.setVisibility(View.GONE);
        mBinding.layoutSelectExercise.btnSave.setOnClickListener(view -> {
            if (Utils.isOnline(mActivity)) {
                if (validation()) {
                    if (session.getRollId() == 1) saveSuperSetClient();
                    else saveSuperSet();
                }
            } else
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        });
        mBinding.layoutSelectExercise.btnSave.setVisibility(View.GONE);

        if (Utils.isOnline(mActivity))
            callExerciseList();
        else
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        return mBinding.getRoot();
    }

    private void saveSuperSetClient() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getJsonStringClient());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).createSuperSet(requestBody)
                , getCompositeDisposable(), single, this);
    }

    private boolean validation() {
        if (selectExerciseAdapter.getData().size() <= 0) {
            showSnackBar(mBinding.linear, "Please select exercise to create set", Snackbar.LENGTH_LONG);
            return false;
        }
        return true;
    }

    private void saveSuperSet() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), getJsonString());
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).createSuperSet(requestBody)
                , getCompositeDisposable(), single, this);
    }

    private void callExerciseList() {
        if (session.getRollId() == 1) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOutExerciseListClient(workoutId)
                    , getCompositeDisposable(), workoutList, this);
        } else {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getWorkOutExerciseList(workoutId, session.getStringDataByKeyNull(CLIENT_ID))
                    , getCompositeDisposable(), workoutList, this);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                WorkoutExerciseListModel model = (WorkoutExerciseListModel) o;
                if (model != null && model.getStatus() == AppConstants.SUCCESS && model.getData() != null && model.getData().size() > 0) {
                    listDatas = model.getData();
                    setModelData();
                } else {
                    showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
                }
                break;
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse apiResponse = (CommonApiResponse) o;
                if (apiResponse != null && apiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.linear, apiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }

                break;
        }
    }

    private void setModelData() {
        List<WorkoutExerciseListModel.DataBean> tempDatas = new ArrayList<>();
        for (List<WorkoutExerciseListModel.DataBean> datas : listDatas) {
            if (datas.size() == 1 && datas.get(0).getId() != dataBean.getId()) {
                tempDatas.add(datas.get(0));
            }
        }
        selectExerciseAdapter = new SelectExerciseAdapter(mActivity, tempDatas, this);
        mBinding.layoutSelectExercise.recyclerView.setAdapter(selectExerciseAdapter);
        mBinding.layoutSelectExercise.itemSelectExercise.setItem(dataBean);

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    public String getJsonString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("workout_id", workoutId);
            jsonObject.put("client_id", session.getStringDataByKeyNull(CLIENT_ID));
            List<Integer> list = new ArrayList<>();
            list.add(dataBean.getId());
            list.addAll(selectExerciseAdapter.getData());
            jsonObject.putOpt("exercises", new JSONArray(list));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public String getJsonStringClient() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("workout_id", workoutId);
            jsonObject.put("client_id", userId);
            List<Integer> list = new ArrayList<>();
            list.add(dataBean.getId());
            list.addAll(selectExerciseAdapter.getData());
            jsonObject.putOpt("exercises", new JSONArray(list));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public void selected() {
        if (selectExerciseAdapter.getData().size() == 1) {
            mBinding.layoutSelectExercise.btnSave.setVisibility(View.VISIBLE);
            mBinding.layoutSelectExercise.btnSave.setText("Create Super Set");
        } else if (selectExerciseAdapter.getData().size() >= 2) {
            mBinding.layoutSelectExercise.btnSave.setText("Create Giant Set");
        } else {
            mBinding.layoutSelectExercise.btnSave.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        callExerciseList();
        mBinding.layoutSelectExercise.swipeContainer.setRefreshing(false);
        mBinding.layoutSelectExercise.btnSave.setVisibility(View.GONE);
    }
}
