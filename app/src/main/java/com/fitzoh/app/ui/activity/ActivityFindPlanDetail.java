package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.FindPlanDietDetailFragment;
import com.fitzoh.app.ui.fragment.FindPlanWorkoutDetailFragment;
import com.fitzoh.app.ui.fragment.FindTrainingProgramDetailFragment;
import com.fitzoh.app.ui.fragment.SubscriptionFragment;

public class ActivityFindPlanDetail extends BaseActivity implements PaymentResultListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        Checkout.preload(getApplicationContext());

        Intent intent = getIntent();
        if (null != intent) {
            String intentData = intent.getExtras().getString("type");
            switch (intentData) {
                case "workout":
                    FindPlanWorkoutDetailFragment findPlanWorkoutDetailFragment = FindPlanWorkoutDetailFragment.newInstance();
                    findPlanWorkoutDetailFragment.setArguments(getIntent().getExtras());
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, findPlanWorkoutDetailFragment, "WorkoutFindPlan")
                            .commit();
                    break;
                case "diet_plan":
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, FindPlanDietDetailFragment.newInstance(getIntent().getExtras()), "DietFindPlan")
                            .commit();
                    break;
                case "training_program":
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.realtabcontent, FindTrainingProgramDetailFragment.newInstance(getIntent().getExtras()), "TrainerFindPlan")
                            .commit();
                    break;
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (null != getIntent()) {
            String intentData = getIntent().getExtras().getString("type");
            switch (intentData) {
                case "workout":
                    ((FindPlanWorkoutDetailFragment) fragment).onPaymentSuccess(s);
                case "diet_plan":
                    ((FindPlanDietDetailFragment) fragment).onPaymentSuccess(s);
                case "training_program":
                    ((FindTrainingProgramDetailFragment) fragment).onPaymentSuccess(s);
            }
        }

    }

    @Override
    public void onPaymentError(int i, String s) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (null != getIntent()) {
            String intentData = getIntent().getExtras().getString("type");
            switch (intentData) {
                case "workout":
                    ((FindPlanWorkoutDetailFragment) fragment).onPaymentError(i, s);
                case "diet_plan":
                    ((FindPlanDietDetailFragment) fragment).onPaymentError(i, s);
                case "training_program":
                    ((FindTrainingProgramDetailFragment) fragment).onPaymentError(i, s);
            }
        }
    }
}
