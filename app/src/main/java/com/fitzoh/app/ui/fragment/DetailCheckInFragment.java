package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.DetailCheckInFormListAdapter;
import com.fitzoh.app.adapter.DetailCheckInImageAdapter;
import com.fitzoh.app.databinding.FragmentDetailCheckInBinding;
import com.fitzoh.app.model.CheckInFormListModel;
import com.fitzoh.app.model.CheckInLabelModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;


public class DetailCheckInFragment extends BaseFragment implements View.OnClickListener, SingleCallback {


    FragmentDetailCheckInBinding mBinding;
    private ArrayList<String> imagesEncodedList = new ArrayList<>();
    private String userId, userAccessToken;
    DetailCheckInFormListAdapter recyclerViewAdapter;
    List<CheckInLabelModel> checkInLabelModelList = new ArrayList<>();
    private int checkInId = 0;

    public DetailCheckInFragment() {
        // Required empty public constructor
    }

    public static DetailCheckInFragment newInstance(Bundle bundle) {
        DetailCheckInFragment fragment = new DetailCheckInFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("checkin_id")) {
            checkInId = getArguments().getInt("checkin_id", 0);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_detail_check_in, container, false);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.detailCheckIn.txtEmptyText.setTextColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        recyclerViewAdapter = new DetailCheckInFormListAdapter(mActivity);
        mBinding.detailCheckIn.recyclerViewList.setAdapter(recyclerViewAdapter);

        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Check In Detail");
        getCheckInData();
        return mBinding.getRoot();
    }

    private void getCheckInData() {
        if (Utils.isOnline(mActivity)) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getCheckInForm(checkInId)
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CheckInFormListModel checkInFormListModel = (CheckInFormListModel) o;
                if (checkInFormListModel != null && checkInFormListModel.getStatus() == AppConstants.SUCCESS && checkInFormListModel.getData() != null && checkInFormListModel.getData().getForm_details() != null && checkInFormListModel.getData().getForm_details().size() > 0) {
                    checkInLabelModelList = checkInFormListModel.getData().getForm_details();
                    recyclerViewAdapter.addDataToAdapter(checkInFormListModel.getData().getForm_details());
                    if (checkInFormListModel.getData().getImages() != null && checkInFormListModel.getData().getImages().size() > 0) {
                        mBinding.detailCheckIn.recyclerView.setAdapter(new DetailCheckInImageAdapter(mActivity, checkInFormListModel.getData().getImages()));
                        mBinding.detailCheckIn.recyclerView.setVisibility(View.VISIBLE);
                        mBinding.detailCheckIn.txtEmptyText.setVisibility(View.GONE);
                    } else {
                        mBinding.detailCheckIn.recyclerView.setVisibility(View.GONE);
                        mBinding.detailCheckIn.txtEmptyText.setVisibility(View.VISIBLE);
                    }
                } else {
                    showSnackBar(mBinding.getRoot(), checkInFormListModel.getMessage(), Snackbar.LENGTH_LONG);
                    mActivity.onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.getRoot(), throwable.getMessage() == null ? "onFailure" : throwable.getMessage(), Snackbar.LENGTH_LONG);
    }


}
