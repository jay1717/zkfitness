package com.fitzoh.app.ui.fragment;


import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.RequestListAdapter;
import com.fitzoh.app.databinding.FragmentNotificationBinding;
import com.fitzoh.app.interfaces.FragmentLifeCycle;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.RequestListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getRequestList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment implements View.OnClickListener, FragmentLifeCycle, RequestListAdapter.onAction, SingleCallback, SwipeRefreshLayout.OnRefreshListener {


    FragmentNotificationBinding mBinding;
    List<RequestListModel.DataBean> dataList;
    String userId, userAccessToken;
    RequestListAdapter requestListAdapter;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
//        mBinding.toolbar.imgBack.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_back));
        //  Utils.setImageBackground(mActivity, mBinding.toolbar.imgBack, R.drawable.ic_back);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        //  mBinding.toolbar.tvTitle.setText(R.string.notificaion);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        setClickEvents();
        callRequestData();
        return mBinding.getRoot();
    }

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    private void setClickEvents() {

        //     mBinding.toolbar.imgBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().finish();
                break;
        }
    }

    private void callRequestData() {
        if (Utils.isOnline(getActivity())) {
            mBinding.swipeContainer.setRefreshing(false);
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getRequestList()
                    , getCompositeDisposable(), getRequestList, this);
        } else {
            showSnackBar(mBinding.layoutNotification, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void accept(RequestListModel.DataBean dataBean, int status) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                .setTitle("Fitzoh")
                .setMessage("Are you sure, you want to add this as a client?")
                .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    onRequest(dataBean, status);
                    initLiveClientSocket(dataBean.getClient_id());
                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
    }

    private void initLiveClientSocket(String client_id) {
        List<String> channels = new ArrayList<>();
        channels.add(client_id);
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-0b71c5ea-b727-11e8-b6ef-c2e67adadb66");
        pnConfiguration.setPublishKey("pub-c-615959d6-abc5-4ce8-ad1e-33884fffeaf1");
        pnConfiguration.setSecure(false);

        PubNub pubnub = new PubNub(pnConfiguration);

        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // create message payload using Gson
        /*JsonObject messageJsonObject = new JsonObject();
        messageJsonObject.addProperty("id", session.getAuthorizedUser(mActivity).getId());
        messageJsonObject.addProperty("name", session.getAuthorizedUser(mActivity).getName());
        messageJsonObject.addProperty("photo", session.getAuthorizedUser(mActivity).getPhoto());
        messageJsonObject.addProperty("email", session.getAuthorizedUser(mActivity).getEmail());
        messageJsonObject.addProperty("date", "" + spf.format(Calendar.getInstance().getTime()));
        messageJsonObject.addProperty("isStartedWorkout", true);

        System.out.println("Message to send: " + messageJsonObject.toString());*/
String accepted="accepted";
        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost
//                    Toast.makeText(mActivity, "connectivity is lost", Toast.LENGTH_LONG).show();
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

//                    Toast.makeText(mActivity, "Connect event", Toast.LENGTH_LONG).show();
                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
                        for (String channelName : channels)
                            pubnub.publish().channel(channelName).message(accepted).async(new PNCallback<PNPublishResult>() {
                                @Override
                                public void onResponse(PNPublishResult result, PNStatus status) {
                                    // Check whether request successfully completed or not.
//                                    Toast.makeText(mActivity, "request successfully completed", Toast.LENGTH_LONG).show();

                                    if (!status.isError()) {

//                                        Toast.makeText(mActivity, "Message successfully published to specified channel", Toast.LENGTH_LONG).show();

                                        // Message successfully published to specified channel.
                                    }
                                    // Request processing failed.
                                    else {

//                                        Toast.makeText(mActivity, "Handle message publish error", Toast.LENGTH_LONG).show();

                                        status.retry();
                                        // Handle message publish error. Check 'category' property to find out possible issue
                                        // because of which request did fail.
                                        //
                                        // Request can be resent using: [status retry];
                                    }
                                }
                            });
                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

//                    Toast.makeText(mActivity, "connectivity is lost, then regained", Toast.LENGTH_LONG).show();

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {

                // Handle new message stored in message.message
                if (message.getChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscription()
                }

                /*JsonElement receivedMessageObject = message.getMessage();
                System.out.println("Received message content: " + receivedMessageObject.toString());
                // extract desired parts of the payload, using Gson
                String msg = message.getMessage().getAsJsonObject().get("msg").getAsString();
                System.out.println("msg content: " + msg);
                System.out.println("msg subscription: " + message.getSubscription());
                System.out.println("msg time token: " + message.getTimetoken());*/


                                    /*
                                        log the following items with your favorite logger
                                            - message.getMessage()
                                            - message.getSubscription()
                                            - message.getTimetoken()
                                    */
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

            }
        });
        pubnub.subscribe().channels(channels).execute();
    }

    @Override
    public void reject(RequestListModel.DataBean dataBean, int status) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext)
                .setTitle("Fitzoh")
                .setMessage("Are you sure, you want to delete this client?")
                .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                    dialog.dismiss();
                    onRequest(dataBean, status);

                })
                .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
    }

    private void onRequest(RequestListModel.DataBean dataBean, int status) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).saveRequestResponse(dataBean.getId(), status)
                    , getCompositeDisposable(), WebserviceBuilder.ApiNames.requestResponse, this);
        } else {
            showSnackBar(mBinding.layoutNotification, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getRequestList:
                dataList = new ArrayList<>();
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                RequestListModel requestListModel = (RequestListModel) o;
                if (requestListModel.getStatus() == AppConstants.SUCCESS) {
                    if (requestListModel != null) {
                        if (requestListModel.getData().size() > 0) {
                            int size = requestListModel.getData().size();
                            for (int i = 0; i < size; i++) {
                                if (requestListModel.getData().get(i).getStatus().equals("0")) {
                                    dataList.add(requestListModel.getData().get(i));
                                }
                            }
                        }
                        // workoutListAdapter.notifyDataSetChanged();
                        if (dataList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            requestListAdapter = new RequestListAdapter(getActivity(), dataList, this);
                            mBinding.recyclerView.setAdapter(requestListAdapter);
                        }
                    } else
                        showSnackBar(mBinding.layoutNotification, requestListModel.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;

            case requestResponse:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callRequestData();
                } else if (commonApiResponse.getStatus() == AppConstants.CHECK_UPDATE)
                    showSnackBar(mBinding.layoutNotification, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);

                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        switch (apiNames) {
            case getRequestList:
                mBinding.imgNoData.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onPauseFragment() {

    }

    @Override
    public void onResumeFragment() {
        callRequestData();
    }

    @Override
    public void onRefresh() {
        callRequestData();
    }
}
