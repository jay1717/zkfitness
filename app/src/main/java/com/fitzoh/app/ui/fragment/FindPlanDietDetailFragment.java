package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.fitzoh.app.R;
import com.fitzoh.app.adapter.TrainingProgramDietAdapter;
import com.fitzoh.app.databinding.FragmentTrainingProgramDietBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.FindPlanDietModel;
import com.fitzoh.app.model.MealListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.dialog.AddMealDialog;
import com.fitzoh.app.ui.dialog.NutritionPlanDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.mealList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindPlanDietDetailFragment extends BaseFragment implements View.OnClickListener, SingleCallback, AddMealDialog.DialogClickListener, PaymentResultListener, SwipeRefreshLayout.OnRefreshListener {


    FragmentTrainingProgramDietBinding mBinding;

    private String userId, userAccessToken;
    List<MealListModel.DataBean> mealData;
    FindPlanDietModel.DataBean dataBean;

    public FindPlanDietDetailFragment() {
        // Required empty public constructor
    }

    public static FindPlanDietDetailFragment newInstance(Bundle bundle) {
        FindPlanDietDetailFragment fragment = new FindPlanDietDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.nutrition_plan));

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (FindPlanDietModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_training_program_diet, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        setHasOptionsMenu(true);
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mealData = new ArrayList<>();
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerView.setAdapter(new TrainingProgramDietAdapter(getActivity(), mealData));
        Utils.setImageBackground(mActivity, mBinding.imgEye, R.drawable.ic_view);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.txtTitle.setText(dataBean.getName());
        getNutririonList();
        setClickEvents();
        return mBinding.getRoot();
    }

    private void getNutririonList() {

        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getMealList(dataBean.getId(), session.getStringDataByKeyNull(CLIENT_ID))
                , getCompositeDisposable(), mealList, this);
    }

    private void setClickEvents() {
        mBinding.imgEye.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_eye:
                NutritionPlanDialog dialog = new NutritionPlanDialog(dataBean.getId());
                dialog.setListener(this);
                dialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), NutritionPlanDialog.class.getSimpleName());
                dialog.setCancelable(false);
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        menu.findItem(R.id.menu_notification).setVisible(false);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        textView.setText(dataBean.getPrice() == 0 ? "Add" : "Purchase");
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> purchase(dataBean.getPrice() == 0 ? "Add" : "Purchase"));
    }

    private void purchase(String type) {
        if (dataBean.getIs_purchased() == 1)
            showSnackBar(mBinding.mainLayout, "you have already purchased this nutrition plan.", Snackbar.LENGTH_LONG);
        else {
            if (type.equalsIgnoreCase("Add")) {
                //direct API
                purchaseSubscription("", "");
            } else {
                //razorpay api
                checkout();
            }
        }

    }

    private void checkout() {
        Checkout checkout = new Checkout();
        checkout.setImage(R.mipmap.ic_launcher);
        final Activity activity = mActivity;


        try {
            JSONObject options = new JSONObject();
            options.put("name", "Fitzoh");
            options.put("description", "diet plan charge");
            options.put("currency", "INR");
            options.put("amount", dataBean.getPrice() * 100);

            JSONObject preFill = new JSONObject();
            preFill.put("contact", session.getAuthorizedUser(activity).getMobile_number());
            preFill.put("email", session.getAuthorizedUser(mActivity).getEmail());
//            preFill.put("contact", "+919784849848");

            options.put("prefill", preFill);

            checkout.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case mealList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                mealData = new ArrayList<>();
                MealListModel mealListModel = (MealListModel) o;
                if (mealListModel.getStatus() == AppConstants.SUCCESS) {

                    if (mealListModel.getData() != null) {
                        mealData.addAll(mealListModel.getData());
                        if (mealData.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.imgNoData.setVisibility(View.GONE);
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setAdapter(new TrainingProgramDietAdapter(getActivity(), mealData));
                        }
                    } else {
                        showSnackBar(mBinding.mainLayout, mealListModel.getMessage(), Snackbar.LENGTH_LONG);
                    }

                }
                break;
            case deleteDietPlan:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS)
                    getNutririonList();

                else
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                break;
            case save:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                if (response != null && response.getStatus() == AppConstants.SUCCESS) {
                    mActivity.onBackPressed();
                } else {
                    showSnackBar(mBinding.mainLayout, response.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            getNutririonList();
        }
    }

    @Override
    public void refreshData() {
        if (Utils.isOnline(getActivity()))
            getNutririonList();
        else
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    @Override
    public void onPaymentSuccess(String s) {
        purchaseSubscription(s, "razorpay");

    }

    private void purchaseSubscription(String s, String razorpay) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).purchase_plan("diet_plan",
                dataBean.getId(), razorpay,s )
                , getCompositeDisposable(), save, this);
    }

    @Override
    public void onPaymentError(int i, String s) {
        showSnackBar(mBinding.mainLayout, s != null ? s : getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);

    }

    @Override
    public void onRefresh() {
        getNutririonList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
