package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;

import com.bumptech.glide.Glide;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ActivitySplashBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.UnauthorizedNetworkInterceptor;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.google.firebase.messaging.FirebaseMessaging;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.TimeZone;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.save;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class SplashActivity extends BaseActivity implements SingleCallback {
    ActivitySplashBinding mBinding;
    Animation animSlideFromBottomFade;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        prepareLayout();
    }

    private void prepareLayout() {
        // Animate the views
        /*if(session.isLogin()){
        et userType = loginDict["role_id"].intValue

        if userType == Constants.userRole.client {

            let dict = CommonFunction.getLoginData()
            let clientType = dict["client_trainer"] as? Bool ?? false

            if clientType {

                let trainerType = dict["gym_trainer"] as? Bool ?? false

                if trainerType {
   //Client With Gym Trainer
   //owner_image
  }
  else{
                    let isGym = dict["direct_gym_client"] as? Bool ?? false

                     if isGym {
     //Client with Gym Direct
    //owner_image
   }
   else {
    //Client With Trainer
    //owner_image
   }
  }
  else {
                //Client Without Trainer
  }
 }
else{

//photo
}
        }*/

        if (session.isLogin()) {
            if (session.getAuthorizedUser(this).getRoleId().equals("1")) {
                if (session.getAuthorizedUser(this).isClient_trainer()) {
                    if (session.getAuthorizedUser(this).isGym_trainer()) {
                        mBinding.imgLogo.setVisibility(View.GONE);
                        mBinding.imgOwner.setVisibility(View.VISIBLE);
                        if(session.getAuthorizedUser(this).getOwner_image()!=null){
                            Glide.with(this)
                                    .load(session.getAuthorizedUser(this).getOwner_image())
                                    .into(mBinding.imgOwner);
                        }
                        //ownerImage
                    } else
                        if (session.getAuthorizedUser(this).isDirect_gym_client()) {
                        mBinding.imgLogo.setVisibility(View.GONE);
                        mBinding.imgOwner.setVisibility(View.VISIBLE);
                        //ownerImage
                        if(session.getAuthorizedUser(this).getOwner_image()!=null){
                            Glide.with(this)
                                    .load(session.getAuthorizedUser(this).getOwner_image())
                                    .into(mBinding.imgOwner);
                        }

                    } else {
                        mBinding.imgLogo.setVisibility(View.GONE);
                        mBinding.imgOwner.setVisibility(View.VISIBLE);
                        if(session.getAuthorizedUser(this).getOwner_image()!=null){
                            Glide.with(this)
                                    .load(session.getAuthorizedUser(this).getOwner_image())
                                    .into(mBinding.imgOwner);
                        }

                        //ownerImage

                    }
                } else  if (session.getAuthorizedUser(this).isDirect_gym_client()) {
                    mBinding.imgLogo.setVisibility(View.GONE);
                    mBinding.imgOwner.setVisibility(View.VISIBLE);
                    //ownerImage
                    if(session.getAuthorizedUser(this).getOwner_image()!=null){
                        Glide.with(this)
                                .load(session.getAuthorizedUser(this).getOwner_image())
                                .into(mBinding.imgOwner);
                    }else {

                    }
                    //Nothing
                }
            } else {
                //photo
                mBinding.imgLogo.setVisibility(View.GONE);
                mBinding.imgOwner.setVisibility(View.VISIBLE);
                if(session.getAuthorizedUser(this).getPhoto()!=null){
                    Glide.with(this)
                            .load(session.getAuthorizedUser(this).getPhoto())
                            .into(mBinding.imgOwner);
                }

            }
        }


        animSlideFromBottomFade = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom_with_fade);
        animSlideFromBottomFade.setInterpolator(new DecelerateInterpolator());
        animSlideFromBottomFade.setAnimationListener(animationListener);
//        mBinding.imgLogo.startAnimation(animSlideFromBottomFade);
//        startNextActivity();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.fitzoh.app",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    private void callAPI() {

        if (Utils.isOnline(this)) {
//            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);

//            ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new UnauthorizedNetworkInterceptor(this)).create(WebserviceBuilder.class).checkVersion(BuildConfig.VERSION_NAME)
            ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new UnauthorizedNetworkInterceptor(this)).create(WebserviceBuilder.class).checkVersion("1.1")
                    , getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG, this);
        }

        if (session.isLogin() && session.getRollId() == 1) {
            TimeZone timeZone = TimeZone.getDefault();
            String userId = String.valueOf(session.getAuthorizedUser(this).getId());
            String userAccessToken = session.getAuthorizedUser(this).getUserAccessToken();
            ObserverUtil.subscribeToSingle(ApiClient.getClient(this, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).updateTimezone(timeZone.getDisplayName(false, TimeZone.SHORT))
                    , getCompositeDisposable(), save, this);
        }
    }

    private void startNextActivity() {
        new Handler().postDelayed(() -> {
            // Redirect to Home Screen
            if (session.isLogin()) {
                if (session.getRollId() == 1) {
                    startActivity(new Intent(SplashActivity.this, NavigationClientMainActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, NavigationMainActivity.class));
                }
            } else {
                startActivity(new Intent(SplashActivity.this, LoginFeildActivity.class));
            }
            finish();
        }, 3000);
    }

    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            new Handler().postDelayed(() -> {
                // Redirect to Home Screen
                if (session.isLogin()) {
                    if (session.getAuthorizedUser(getApplicationContext()).getRoleId().equals("1")) {
                        startActivity(new Intent(SplashActivity.this, NavigationClientMainActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, NavigationMainActivity.class));
                    }
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginFeildActivity.class));
                }
                finish();
            }, 500);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        callAPI();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                disableScreen(false);
                CommonApiResponse response = (CommonApiResponse) o;
                Log.e("response.getStatus()",""+response.getStatus());
                if (response.getStatus() == AppConstants.CHECK_UPDATE) {
                    showDialog("", this);
                } else {
                    startNextActivity();
                }
                break;
            case save:
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                disableScreen(false);
                showDialog("", this);


        }
    }
}
