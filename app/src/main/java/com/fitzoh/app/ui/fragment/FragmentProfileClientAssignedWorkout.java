package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ProfileWorkoutAdapter;
import com.fitzoh.app.databinding.FragmentWorkoutAssignBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.DeleteWorkOutModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ClientWorkoutDataActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.unAssignWorkout;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentProfileClientAssignedWorkout#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProfileClientAssignedWorkout extends BaseFragment implements SingleCallback, ProfileWorkoutAdapter.unAssignClient, SwipeRefreshLayout.OnRefreshListener {

    private static int clientId;
    FragmentWorkoutAssignBinding mBinding;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userId, userAccessToken;
    List<WorkOutListModel.DataBean> workOutListData;
    ProfileWorkoutAdapter profileWorkoutAdapter;
    ClientListModel.DataBean dataBean;

    public FragmentProfileClientAssignedWorkout() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static FragmentProfileClientAssignedWorkout newInstance(int id) {
        FragmentProfileClientAssignedWorkout fragment = new FragmentProfileClientAssignedWorkout();

        clientId = id;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //   dataBean = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.workouts));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_workout_assign, container, false);

        workOutListData = new ArrayList<>();
        mBinding.layoutWorkout.swipeContainer.setOnRefreshListener(this);
        mBinding.layoutWorkout.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.layoutWorkout.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        profileWorkoutAdapter = new ProfileWorkoutAdapter(getActivity(), this, workOutListData, false);
        mBinding.layoutWorkout.recyclerView.setAdapter(profileWorkoutAdapter);
        mBinding.layoutWorkout.fab.hide();

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (userId != null && userAccessToken != null)
            if (Utils.isOnline(getContext()))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    private void callWorkoutList() {
        if (Utils.isOnline(getActivity())) {

            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientWorkoutList(clientId)
                    , getCompositeDisposable(), workoutList, this);
        } else {
            showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void callUnassignClient(int clientID) {
      /* if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteWorkOut(workoutId)
                    , getCompositeDisposable(), unAssignWorkout, this);
        } else {
            showSnackBar(mBinding.getRoot(), getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
        showSnackBar(mBinding.getRoot(), "Client ID " + clientID, Snackbar.LENGTH_LONG);*/

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callWorkoutList();
            else {
                showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {


        switch (apiNames) {
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                workOutListData = new ArrayList<>();
                WorkOutListModel profileWorkoutModel = (WorkOutListModel) o;
                if (profileWorkoutModel.getStatus() == AppConstants.SUCCESS) {

                    if (profileWorkoutModel != null) {
                        int size = profileWorkoutModel.getData().size();
                        for (int i = 0; i < size; i++) {
                            if (profileWorkoutModel.getData().get(i).getIs_assigned() == 0) {
                                workOutListData.add(profileWorkoutModel.getData().get(i));
                            }
                        }
                        // workoutListAdapter.notifyDataSetChanged();
                        if (workOutListData.size() == 0) {
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.layoutWorkout.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.layoutWorkout.imgNoData.setVisibility(View.GONE);
                            profileWorkoutAdapter = new ProfileWorkoutAdapter(getActivity(), this, workOutListData, false);
                            mBinding.layoutWorkout.recyclerView.setAdapter(profileWorkoutAdapter);
                        }

                    } else
                        showSnackBar(mBinding.linear, profileWorkoutModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;
            case deleteWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteWorkOutModel deleteWorkOutModel = (DeleteWorkOutModel) o;
                if (deleteWorkOutModel.getStatus() == AppConstants.SUCCESS)
                    if (Utils.isOnline(getContext()))
                        callWorkoutList();
                    else {
                        showSnackBar(mBinding.linear, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
                    }
                else
                    showSnackBar(mBinding.linear, deleteWorkOutModel.getMessage(), Snackbar.LENGTH_LONG);

                break;

            case unAssignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callWorkoutList();
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }


    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.linear, throwable.getMessage(), Snackbar.LENGTH_LONG);
    }

    public JSONArray getData(WorkOutListModel.DataBean dataBean) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", dataBean.getId());
            jsonObject.put("is_assigned", 0);
            jsonArray.put(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();

        }
        return jsonArray;
    }


    @Override
    public void unAssign(WorkOutListModel.DataBean dataBean) {
        if (dataBean != null) {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("user_id", clientId);
                jsonObject.put("is_assigned", 0);
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();

            }
            JSONObject jsonObjectUnassign = new JSONObject();
            try {
                jsonObjectUnassign.put("client_id", clientId);
                jsonObjectUnassign.put("client_ids", jsonArray);
                jsonObjectUnassign.put("client_group_ids", new JSONArray());
//            jsonObject.putOpt("client_ids", new JSONArray(adapter.getData()));
                jsonObjectUnassign.put("workout_id", dataBean.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonObjectUnassign.toString());

            if (Utils.isOnline(getActivity())) {
                mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).unAssignWorkout(requestBody)
                        , getCompositeDisposable(), unAssignWorkout, this);
            } else {
                showSnackBar(mBinding.linear, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
            }
            callUnassignClient(dataBean.getId());
        }
    }

    @Override
    public void startWorkout(WorkOutListModel.DataBean dataBean) {
        getActivity().startActivity(new Intent(getActivity(), ClientWorkoutDataActivity.class).putExtra("workout_id", dataBean.getId()).putExtra("client_id", clientId));
    }

    @Override
    public void onRefresh() {
        callWorkoutList();
        mBinding.layoutWorkout.swipeContainer.setRefreshing(false);
    }
}
