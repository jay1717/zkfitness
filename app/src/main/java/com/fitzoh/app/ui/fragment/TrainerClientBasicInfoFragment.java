package com.fitzoh.app.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentTrainerClientBasicInfoBinding;
import com.fitzoh.app.model.VerificationOTPModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.Objects;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainerClientBasicInfoFragment extends BaseFragment implements SingleCallback {

    FragmentTrainerClientBasicInfoBinding mBinding;
    View view;
    int trainerId = 0;
    String userId, userAccessToken;
    private int client_id = 0;


    public TrainerClientBasicInfoFragment() {
        // Required empty public constructor
    }

    public static TrainerClientBasicInfoFragment newInstance(Bundle bundle) {
        TrainerClientBasicInfoFragment fragment = new TrainerClientBasicInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            client_id = getArguments().getInt("client_id");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.basic_info));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_trainer_client_basic_info, container, false);

        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        Utils.setTextViewStartImage(mActivity, mBinding.layoutEditWorkout.edtEmail, R.drawable.ic_email, true);
        Utils.setTextViewStartImage(mActivity, mBinding.layoutEditWorkout.edtName, R.drawable.ic_user, true);
        Utils.setTextViewStartImage(mActivity, mBinding.layoutEditWorkout.edtDate, R.drawable.ic_calendar, true);
        Utils.setTextViewStartImage(mActivity, mBinding.layoutEditWorkout.edtMobile, R.drawable.ic_phone_iphone_white_24dp, true);
        mBinding.layoutEditWorkout.countryCode.setCcpClickable(false);
        getClientProfile();
        return mBinding.getRoot();
    }

    private void getClientProfile() {
        if (Utils.isOnline(mActivity)) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(false);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getClientProfile(client_id, Integer.parseInt(userId))
                    , getCompositeDisposable(), single, this);


        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                VerificationOTPModel dataModel = (VerificationOTPModel) o;
                if (dataModel != null && dataModel.getStatus() == AppConstants.SUCCESS && dataModel.getData() != null) {
                    mBinding.setItem(dataModel.getData());
                    mBinding.layoutEditWorkout.setItem(dataModel.getData());
                    mBinding.executePendingBindings();
                    if (dataModel.getData().getCountry_code() != null && !dataModel.getData().getCountry_code().equalsIgnoreCase(""))
                        mBinding.layoutEditWorkout.countryCode.setCountryForPhoneCode(Integer.parseInt(dataModel.getData().getCountry_code()));

                    if (dataModel.getData().getGender().equalsIgnoreCase("m")) {
                        mBinding.layoutEditWorkout.radioMale.setChecked(true);
                    } else {
                        mBinding.layoutEditWorkout.radioFemale.setChecked(true);
                    }
                } else {
                    showSnackBar(mBinding.mainLayout, Objects.requireNonNull(dataModel).getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
