package com.fitzoh.app.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentClientprofileWorkoutBinding;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ClientLogWorkoutActivity;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClientProfileWorkoutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClientProfileWorkoutFragment extends BaseFragment {

    FragmentClientprofileWorkoutBinding mBinding;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    //  private ClientListModel.DataBean data = null;
    int clientId;
    private String mParam2;


    public ClientProfileWorkoutFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ClientProfileWorkoutFragment newInstance() {
        ClientProfileWorkoutFragment fragment = new ClientProfileWorkoutFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clientId = getArguments().getInt("data");
            // data = (ClientListModel.DataBean) getArguments().getSerializable("data");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //setupToolBarWithBackArrow(mBinding.toolbar.too, "Arm Workout - Assign Clients");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_clientprofile_workout, container, false);
        setupViewPager(mBinding.viewpager);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, "Workout");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBinding.toolbar.toolbar.setElevation(0f);
        }
        setHasOptionsMenu(true);
        mBinding.tabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        //  mBinding.tabs.setIndicator("",getResources().getDrawable(R.drawable.tab1));
        mBinding.tabs.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        mBinding.tabs.setTabTextColors(getResources().getColor(R.color.gray), getResources().getColor(R.color.gray));
        return mBinding.getRoot();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(FragmentProfileAssignedWorkout.newInstance(clientId), "Assigned");
        adapter.addFragment(FragmentProfileClientAssignedWorkout.newInstance(clientId), "Client Created");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_find_plan, menu);
        Utils.setMenuItemDrawable(mActivity, menu.findItem(R.id.menu_notification), R.drawable.ic_bell);
        MenuItem item = menu.findItem(R.id.find_plan);
        MenuItemCompat.setActionView(item, R.layout.menu_button_layout);
        RelativeLayout layout = (RelativeLayout) MenuItemCompat.getActionView(item);
        TextView textView = layout.findViewById(R.id.button);
        textView.setText("Logged");
        Utils.getItemShapeGradient(mActivity, textView);
        layout.setOnClickListener(v -> {
            session.setStringDataByKey(CLIENT_ID, "" + clientId);
            startActivity(new Intent(getActivity(), ClientLogWorkoutActivity.class));
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
