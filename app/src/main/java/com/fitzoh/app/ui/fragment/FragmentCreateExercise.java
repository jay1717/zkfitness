package com.fitzoh.app.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.system.ErrnoException;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentCreateExerciseBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.EquipmentListModel;
import com.fitzoh.app.model.MuscleListModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.CreateExerciseActivity;
import com.fitzoh.app.ui.activity.YoutubeSearchActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.specialityList;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.workoutList;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;
import static com.fitzoh.app.utils.Utils.setImagePlaceHolder;

public class FragmentCreateExercise extends BaseFragment implements SingleCallback, AdapterView.OnItemSelectedListener {

    FragmentCreateExerciseBinding mBinding;
    String userId, userAccessToken, youtube_id="", toutube_title ="", toutube_Dec="",youTube_ImageyouTube_Image="";
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImage;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int RECORD_REQUEST_CODE = 101;
    private List<MuscleListModel.DataBean> muscleDatas = new ArrayList<>();
    private List<EquipmentListModel.DataBean> equipmentDatas = new ArrayList<>();
    private boolean isEquipment, isMuscle;
    private WorkOutListModel.DataBean dataBean;

    public FragmentCreateExercise() {
        // Required empty public constructor
    }


    public static FragmentCreateExercise newInstance(Bundle bundle) {
        FragmentCreateExercise fragment = new FragmentCreateExercise();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            dataBean = (WorkOutListModel.DataBean) getArguments().getSerializable("dataBean");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.create_exercise));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_create_exercise, container, false);
        Utils.getShapeGradient(mActivity, mBinding.layoutCreateExercise.btnCreateExercise);
        Utils.setImageBackground(mActivity, mBinding.layoutCreateExercise.imgPickPhoto, R.drawable.ic_plus_icon);
        Utils.setSpinnerArrow(mActivity, new ImageView[]{mBinding.layoutCreateExercise.imgMuscle, mBinding.layoutCreateExercise.imgEquipment});
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        prepareLayout();
        return mBinding.getRoot();

    }

    private void prepareLayout() {
        userId = String.valueOf(session.getAuthorizedUser(mActivity).getId());
        userAccessToken = session.getAuthorizedUser(mActivity).getUserAccessToken();

        mBinding.layoutCreateExercise.imgExercise.setOnClickListener(view -> imgPick());
        mBinding.layoutCreateExercise.imgPickPhoto.setOnClickListener(view -> imgPick());
        mBinding.layoutCreateExercise.btnCreateExercise.setOnClickListener(view -> {
            if (validation()) {
                createExercise();
            }
        });
        setListeners();
        makeRequest();
        if (Utils.isOnline(getActivity())) {
            getMuscles();
            getEquipment();
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void createExerciseClient() {

    }

    private void setListeners() {
        mBinding.layoutCreateExercise.edtExerciseName.addTextChangedListener(new MyTaskWatcher(mBinding.layoutCreateExercise.edtExerciseName));
//        mBinding.layoutCreateExercise.edtExerciseDesc.addTextChangedListener(new MyTaskWatcher(mBinding.layoutCreateExercise.edtExerciseDesc));
        mBinding.layoutCreateExercise.edtVideoTitle.addTextChangedListener(new MyTaskWatcher(mBinding.layoutCreateExercise.edtVideoTitle));
        mBinding.layoutCreateExercise.edtVideoDesc.addTextChangedListener(new MyTaskWatcher(mBinding.layoutCreateExercise.edtVideoDesc));
//        mBinding.layoutCreateExercise.edtVideoUrl.addTextChangedListener(new MyTaskWatcher(mBinding.layoutCreateExercise.edtVideoUrl));
        mBinding.layoutCreateExercise.edtInstructions.addTextChangedListener(new MyTaskWatcher(mBinding.layoutCreateExercise.edtInstructions));
        mBinding.layoutCreateExercise.edtVideoUrl.setFocusable(false);
        mBinding.layoutCreateExercise.edtVideoUrl.setFocusableInTouchMode(false);
        mBinding.layoutCreateExercise.edtVideoUrl.setClickable(true);
        mBinding.layoutCreateExercise.edtVideoTitle.setOnClickListener(view -> {
            mActivity.startActivityForResult(new Intent(mActivity, YoutubeSearchActivity.class), 0);
        });
    }

    private void getEquipment() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getEquipments()
                , getCompositeDisposable(), specialityList, this);


    }

    private void createExercise() {
        if (Utils.isOnline(getActivity())) {
            MultipartBody.Part photo = null;
            RequestBody client_id = null;
            RequestBody muscleId, equipmentId;
            if (session.getStringDataByKeyNull(CLIENT_ID) != null)
                client_id = RequestBody.create(MediaType.parse("text/plain"), session.getStringDataByKeyNull(CLIENT_ID));
            RequestBody workout_id = RequestBody.create(MediaType.parse("text/plain"), "" + dataBean.getId());
            if (mBinding.layoutCreateExercise.spnEquipment.getSelectedItemPosition() > 0)
                equipmentId = RequestBody.create(MediaType.parse("text/plain"), "" + equipmentDatas.get(mBinding.layoutCreateExercise.spnEquipment.getSelectedItemPosition() - 1).getId());
            else
                equipmentId = RequestBody.create(MediaType.parse("text/plain"), "" + 0);
            if (mBinding.layoutCreateExercise.spnMuscle.getSelectedItemPosition() > 0)
                muscleId = RequestBody.create(MediaType.parse("text/plain"), "" + muscleDatas.get(mBinding.layoutCreateExercise.spnMuscle.getSelectedItemPosition() - 1).getId());
            else
                muscleId = RequestBody.create(MediaType.parse("text/plain"), "" + 0);

            RequestBody roleId = RequestBody.create(MediaType.parse("text/plain"), "" + session.getRollId());
            RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), "" + session.getAuthorizedUser(mContext).getId());
            RequestBody exerciseName = RequestBody.create(MediaType.parse("text/plain"), mBinding.layoutCreateExercise.edtExerciseName.getText().toString());
            RequestBody exerciseDesc = RequestBody.create(MediaType.parse("text/plain"), toutube_Dec.trim());
            RequestBody videoTitle = RequestBody.create(MediaType.parse("text/plain"),toutube_title.trim());
            RequestBody videoDesc = RequestBody.create(MediaType.parse("text/plain"), toutube_Dec.trim());
            RequestBody videoUrl = RequestBody.create(MediaType.parse("text/plain"),youtube_id.trim());
            RequestBody instructions = RequestBody.create(MediaType.parse("text/plain"), mBinding.layoutCreateExercise.edtInstructions.getText().toString());
            if (fileImage != null) {
                byte[] uploadBytes = Utils.loadImageFromStorage(mImageUri.getPath());
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), uploadBytes);
                // RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
                photo = MultipartBody.Part.createFormData("image", fileImage.getName(), reqFile);
            }
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            if (session.getRollId() == 1)
                ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, this.userId)).create(WebserviceBuilder.class).createExerciseClient(
                        workout_id,
                        equipmentId,
                        muscleId,
                        roleId,
                        userId,
                        exerciseName,
                        exerciseDesc,
                        videoTitle,
                        videoDesc,
                        videoUrl,
                        instructions,
                        photo), getCompositeDisposable(), single, this);
            else
                ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, this.userId)).create(WebserviceBuilder.class).createExercise(client_id,
                        workout_id,
                        equipmentId,
                        muscleId,
                        roleId,
                        userId,
                        exerciseName,
                        exerciseDesc,
                        videoTitle,
                        videoDesc,
                        videoUrl,
                        instructions,
                        photo), getCompositeDisposable(), single, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    private void getMuscles() {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(mActivity, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getMuscles()
                , getCompositeDisposable(), workoutList, this);
    }

    public void imgPick() {
        mActivity.startActivityForResult(getPickImageChooserIntent(), 200);
    }


    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = mActivity.getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = mActivity.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = mActivity.getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(mActivity);
    }

    protected void makeRequest() {
        requestAppPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, RECORD_REQUEST_CODE, new setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {

            }

            @Override
            public void onPermissionDenied(int requestCode) {

            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    mActivity.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {

                    mBinding.layoutCreateExercise.imgExercise.setImageURI(mImageUri);

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();


                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        } else if (requestCode == 0) {
            try {
                youtube_id = "https://www.youtube.com/watch?v="+data.getStringExtra("youTube_id");
                toutube_title = data.getStringExtra("youTubeTitle");
                toutube_Dec = data.getStringExtra("toutube_Dec");
                youTube_ImageyouTube_Image = data.getStringExtra("youTube_Image");
                Log.e("youTube_Image", youTube_ImageyouTube_Image);

                // set Video
                mBinding.layoutCreateExercise.edtVideoTitle.setText(toutube_title);
                mBinding.layoutCreateExercise.edtInstructions.setText(toutube_Dec);
                setImagePlaceHolder(getActivity(), mBinding.layoutCreateExercise.imgPickPhoto, youTube_ImageyouTube_Image);
            } catch (Exception e) {

            }

        }

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else {
                    showSnackBar(mBinding.mainLayout, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
            case workoutList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                MuscleListModel muscleListModel = (MuscleListModel) o;
                List<String> list = new ArrayList<>();
                list.add("Select Muscles");
                if (muscleListModel != null && muscleListModel.getStatus() == AppConstants.SUCCESS && muscleListModel.getData() != null && muscleListModel.getData().size() > 0) {
                    muscleDatas = muscleListModel.getData();
                    for (int i = 0; i < muscleDatas.size(); i++) {
                        list.add(muscleDatas.get(i).getValue());
                    }
                    setSpinner(mBinding.layoutCreateExercise.spnMuscle, list, true);
                } else {
                    setSpinner(mBinding.layoutCreateExercise.spnMuscle, list, false);
                }
                break;
            case specialityList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                EquipmentListModel equipmentListModel = (EquipmentListModel) o;
                List<String> listEquipments = new ArrayList<>();
                listEquipments.add("Select Equipment");
                if (equipmentListModel != null && equipmentListModel.getStatus() == AppConstants.SUCCESS && equipmentListModel.getData() != null && equipmentListModel.getData().size() > 0) {
                    equipmentDatas = equipmentListModel.getData();
                    for (int i = 0; i < equipmentDatas.size(); i++) {
                        listEquipments.add(equipmentDatas.get(i).getEquipment_name());
                    }
                    setSpinner(mBinding.layoutCreateExercise.spnEquipment, listEquipments, true);
                } else {
                    setSpinner(mBinding.layoutCreateExercise.spnEquipment, listEquipments, false);
                }
                break;
        }
    }

    private void setSpinner(Spinner spinner, List<String> list, boolean isEnable) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.spinner_item_add_workout, list);
        spinner.setAdapter(adapter);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setEnabled(isEnable);
        if (spinner.getId() == R.id.spnEquipment)
            mBinding.layoutCreateExercise.spnEquipment.setOnItemSelectedListener(this);
        if (spinner.getId() == R.id.spnMuscle)
            mBinding.layoutCreateExercise.spnMuscle.setOnItemSelectedListener(this);
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        showSnackBar(mBinding.mainLayout, getString(R.string.something_went_wrong), Snackbar.LENGTH_LONG);
    }

    private boolean validation() {
        boolean isValue = true;
        if (mBinding.layoutCreateExercise.edtExerciseName.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorExerciseName, "Exercise name must not be empty.");
            isValue = false;
        }
        if (!TextUtils.isEmpty(mBinding.layoutCreateExercise.edtVideoUrl.getText().toString()) && !Patterns.WEB_URL.matcher(mBinding.layoutCreateExercise.edtVideoUrl.getText().toString()).matches()) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoUrl, "Enter valid video url.");
            isValue = false;
        }
    /*    if (mBinding.layoutCreateExercise.edtExerciseDesc.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorExerciseDesc, "Exercise description must not be empty.");
            isValue = false;
        }
        if (mBinding.layoutCreateExercise.spnMuscle.getSelectedItemPosition() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorMuscle, "Select Muscle");
            isValue = false;
        }
        if (mBinding.layoutCreateExercise.spnEquipment.getSelectedItemPosition() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorEquipment, "Select Equipments");
            isValue = false;
        }
        if (mBinding.layoutCreateExercise.edtVideoTitle.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoTitle, "Video title must not be empty.");
            isValue = false;
        }
        if (mBinding.layoutCreateExercise.edtVideoDesc.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoDesc, "Video description must not be empty.");
            isValue = false;
        }
        if (mBinding.layoutCreateExercise.edtVideoUrl.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoUrl, "Video url must not be empty.");
            isValue = false;
        }
        if (mBinding.layoutCreateExercise.edtInstructions.getText().toString().length() <= 0) {
            setEdittextError(mBinding.layoutCreateExercise.txtErrorInstructions, "Instructions must not be empty.");
            isValue = false;
        }
        if (fileImage == null) {
            showSnackBar(mBinding.getRoot(), "Please select exercise image.", Snackbar.LENGTH_LONG);
            isValue = false;
        }*/
        return isValue;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spnMuscle:
              /*  if (isMuscle) {
                    if (mBinding.layoutCreateExercise.spnMuscle.getSelectedItemPosition() <= 0) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorMuscle, "Select Muscle");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorMuscle.setText("");
                        mBinding.layoutCreateExercise.txtErrorMuscle.setVisibility(View.GONE);
                    }
                } else {
                    isMuscle = true;
                }*/
                break;
            case R.id.spnEquipment:
                /*if (isEquipment) {
                    if (mBinding.layoutCreateExercise.spnEquipment.getSelectedItemPosition() <= 0) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorEquipment, "Select Equipments");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorEquipment.setText("");
                        mBinding.layoutCreateExercise.txtErrorEquipment.setVisibility(View.GONE);
                    }
                } else {
                    isEquipment = true;
                }*/
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class MyTaskWatcher implements TextWatcher {
        private View view;

        MyTaskWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edtExerciseName:
                    if (view.hasFocus()) {
                        if (mBinding.layoutCreateExercise.edtExerciseName.getText().toString().length() == 0) {
                            setEdittextError(mBinding.layoutCreateExercise.txtErrorExerciseName, "Exercise name must not be empty.");
                        } else {
                            mBinding.layoutCreateExercise.txtErrorExerciseName.setText("");
                            mBinding.layoutCreateExercise.txtErrorExerciseName.setVisibility(View.GONE);
                        }
                    }
                    break;
                /*case R.id.edtExerciseDesc:
                    if (mBinding.layoutCreateExercise.edtExerciseDesc.getText().toString().length() <= 0) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorExerciseDesc, "Exercise description must not be empty.");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorExerciseDesc.setText("");
                        mBinding.layoutCreateExercise.txtErrorExerciseDesc.setVisibility(View.GONE);
                    }
                    break;
                case R.id.edtVideoTitle:
                    if (mBinding.layoutCreateExercise.edtVideoTitle.getText().toString().length() <= 0) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoTitle, "Video title must not be empty.");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorVideoTitle.setText("");
                        mBinding.layoutCreateExercise.txtErrorVideoTitle.setVisibility(View.GONE);
                    }
                    break;
                case R.id.edtVideoDesc:
                    if (mBinding.layoutCreateExercise.edtVideoDesc.getText().toString().length() <= 0) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoDesc, "Video description must not be empty.");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorVideoDesc.setText("");
                        mBinding.layoutCreateExercise.txtErrorVideoDesc.setVisibility(View.GONE);
                    }
                    break;*/
                /*case R.id.edtVideoUrl:
                    if (!TextUtils.isEmpty(mBinding.layoutCreateExercise.edtVideoUrl.getText().toString()) && !Patterns.WEB_URL.matcher(mBinding.layoutCreateExercise.edtVideoUrl.getText().toString()).matches()) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorVideoUrl, "Enter valid video url.");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorVideoUrl.setText("");
                        mBinding.layoutCreateExercise.txtErrorVideoUrl.setVisibility(View.GONE);
                    }
                    break;*/
               /* case R.id.edtInstructions:
                    if (mBinding.layoutCreateExercise.edtInstructions.getText().toString().length() <= 0) {
                        setEdittextError(mBinding.layoutCreateExercise.txtErrorInstructions, "Instructions must not be empty.");
                    } else {
                        mBinding.layoutCreateExercise.txtErrorInstructions.setText("");
                        mBinding.layoutCreateExercise.txtErrorInstructions.setVisibility(View.GONE);
                    }
                    break;*/

            }
        }

    }


}
