package com.fitzoh.app.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ActivityLoginBinding;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.fragment.FragmentLogin;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Logger;

import java.util.HashMap;
import java.util.Stack;

public class LoginActivity extends BaseActivity {

    ActivityLoginBinding mBinding;


    /* A HashMap of stacks, where we use tab identifier as keys..*/
    private HashMap<String, Stack<Fragment>> mStacks;


//    private DayIncrementViewModel mDayIncrementViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);

//        mDayIncrementViewModel = ViewModelProviders.of(LoginActivity.this).get(DayIncrementViewModel.class);

        prepareLayout();
    }


    private void prepareLayout() {
        /*
         *  Navigation stacks for each tab gets created..
         *  tab identifier is used as key to get respective stack for each tab
         */
        mStacks = new HashMap<>();
        mStacks.put(AppConstants.LOGIN_KEY, new Stack<>());
        pushFragments(AppConstants.LOGIN_KEY, new FragmentLogin(), true, true);
    }

    /*
     *      To add fragment to a tab.
     *  tag             ->  Tab identifier
     *  fragment        ->  Fragment to show, in tab identified by tag
     *  shouldAnimate   ->  should animate transaction. false when we switch tabs, or adding first fragment to a tab
     *                      true when when we are pushing more fragment into navigation stack.
     *  shouldAdd       ->  Should add to fragment navigation stack (mStacks.get(tag)). false when we are switching tabs (except for the first time)
     *                      true in all other cases.
     */
    public void pushFragments(String tag, Fragment fragment, boolean shouldAnimate, boolean shouldAdd) {
        if (shouldAdd)
            mStacks.get(tag).push(fragment);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }


    public void popFragments() {

        Fragment fragment = mStacks.get(AppConstants.LOGIN_KEY).elementAt(mStacks.get(AppConstants.LOGIN_KEY).size() - 2);
        /*pop current fragment from stack.. */
        mStacks.get(AppConstants.LOGIN_KEY).pop();

        /* We have the target fragment in hand.. Just show it.. Show a standard navigation animation*/
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.realtabcontent, fragment);
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        if (!((BaseFragment) mStacks.get(AppConstants.LOGIN_KEY).lastElement()).onBackPressed()) {

            /*
             * top fragment in current tab doesn't handles back press, we can do our thing, which is
             *
             * if current tab has only one fragment in stack, ie first fragment is showing for this tab.
             *        finish the activity
             * else
             *        pop to previous fragment in stack for the same tab
             *
             */
            if (mStacks.get(AppConstants.LOGIN_KEY).size() == 1) {
                //If current tab is not Home then set Home as current tab
                finish();
            } else {
                popFragments();
            }
        } else {
            //do nothing.. fragment already handled back button press.
            Logger.e("BackPress Manage By Fragment");
        }
    }


    /*
     *   Imagine if you wanted to get an image selected using ImagePicker intent to the fragment. Ofcourse I could have created a public function
     *  in that fragment, and called it from the activity. But couldn't resist myself.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mStacks.get(AppConstants.LOGIN_KEY).size() == 0) {
            return;
        }
        /*Now current fragment on screen gets onActivityResult callback..*/
        mStacks.get(AppConstants.LOGIN_KEY).lastElement().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
