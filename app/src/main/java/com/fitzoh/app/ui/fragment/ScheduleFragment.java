package com.fitzoh.app.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.adapter.ScheduleAdapter;
import com.fitzoh.app.databinding.FragmentScheduleBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.ScheduleListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.AssignScheduleActivity;
import com.fitzoh.app.ui.dialog.AddScheduleDialog;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteSchedual;
import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.getSchedualList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScheduleFragment extends BaseFragment implements SingleCallback, ScheduleAdapter.onAddClient, SwipeRefreshLayout.OnRefreshListener {


    FragmentScheduleBinding mBinding;
    String userId, userAccessToken;
    List<ScheduleListModel.DataBean> schedualList;
    ScheduleAdapter scheduleAdapter;

    public ScheduleFragment() {
        // Required empty public constructor
    }

    public static ScheduleFragment newInstance() {
        ScheduleFragment fragment = new ScheduleFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithMenu(mBinding.toolbar.toolbar, getString(R.string.schedule));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_schedule, container, false);
        Utils.setAddFabBackground(mActivity, mBinding.fab);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.swipeContainer.setOnRefreshListener(this);
        mBinding.swipeContainer.setColorSchemeColors(getActivity().getResources().getColor(R.color.colorAccent));
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        mBinding.fab.setOnClickListener(view -> {
            AddScheduleDialog dialogSchedule = new AddScheduleDialog();
            //dialogNote.setListener(this);
            dialogSchedule.setTargetFragment(this, 0);
            dialogSchedule.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), AddScheduleDialog.class.getSimpleName());
            dialogSchedule.setCancelable(false);
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.isOnline(getContext()))
            callSchedualList();
        else {
            showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
        }
    }

    private void callSchedualList() {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).getSchedualList()
                    , getCompositeDisposable(), getSchedualList, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == Activity.RESULT_OK)
            if (Utils.isOnline(mContext))
                callSchedualList();
            else {
                showSnackBar(mBinding.mainLayout, getString(R.string.network_unavailable), Snackbar.LENGTH_LONG);
            }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {

            case deleteSchedual:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    callSchedualList();
                }
                break;


            case getSchedualList:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                schedualList = new ArrayList<>();
                ScheduleListModel scheduleListModel = (ScheduleListModel) o;
                if (scheduleListModel.getStatus() == AppConstants.SUCCESS) {

                    if (scheduleListModel != null) {
                        schedualList.addAll(scheduleListModel.getData());
                        // workoutListAdapter.notifyDataSetChanged();
                        if (schedualList.size() == 0) {
                            mBinding.imgNoData.setVisibility(View.VISIBLE);
                            mBinding.recyclerView.setVisibility(View.GONE);
                        } else {
                            mBinding.recyclerView.setVisibility(View.VISIBLE);
                            mBinding.imgNoData.setVisibility(View.GONE);
                            scheduleAdapter = new ScheduleAdapter(getActivity(), schedualList, this);
                            mBinding.recyclerView.setAdapter(scheduleAdapter);
                        }

                    } else
                        showSnackBar(mBinding.mainLayout, scheduleListModel.getMessage(), Snackbar.LENGTH_LONG);

                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

        showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);

    }

    @Override
    public void add(ScheduleListModel.DataBean dataBean) {
        mActivity.startActivityForResult(new Intent(getActivity(), AssignScheduleActivity.class).putExtra("scheduleId", dataBean.getId()), 0);
    }

    @Override
    public void edit(ScheduleListModel.DataBean dataBean) {

    }

    @Override
    public void rename(int position) {

    }

    @Override
    public void delete(ScheduleListModel.DataBean dataBean) {
        if (Utils.isOnline(getActivity())) {
            mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteSchedual(dataBean.getId())
                    , getCompositeDisposable(), deleteSchedual, this);
        } else {
            showSnackBar(mBinding.mainLayout, getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void onRefresh() {
        callSchedualList();
        mBinding.swipeContainer.setRefreshing(false);
    }
}
