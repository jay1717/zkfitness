package com.fitzoh.app.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.FragmentClientProfileBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.GymTrainerScheduleListModel;
import com.fitzoh.app.model.TrainingSessionModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.ui.activity.ActivityOfClientActivity;
import com.fitzoh.app.ui.activity.ClientDocumentActivity;
import com.fitzoh.app.ui.activity.ClientProfileMedicalFormActivity;
import com.fitzoh.app.ui.activity.ClientProfileNutritionActivity;
import com.fitzoh.app.ui.activity.ClientProfilePurchaseActivity;
import com.fitzoh.app.ui.activity.ClientProfileReportActivity;
import com.fitzoh.app.ui.activity.ClientProfileSchedulesActivity;
import com.fitzoh.app.ui.activity.ClientProfileTrainingProgramActivity;
import com.fitzoh.app.ui.activity.ClientProfileWorkoutActivity;
import com.fitzoh.app.ui.activity.TrainerClientBasicInfoActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;
import com.fitzoh.app.vectorchildfinder.VectorChildFinder;
import com.fitzoh.app.vectorchildfinder.VectorDrawableCompat;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.assignWorkout;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ClientProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ClientProfileFragment extends BaseFragment implements View.OnClickListener, SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentClientProfileBinding mBinding;
    View view;
    private String client_type = "0";
    ClientListModel.DataBean dataBean;
    TrainingSessionModel.DataBean bean;
    GymTrainerScheduleListModel.DataBean newBean;
    String userId, userAccessToken;

    public ClientProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ClientProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ClientProfileFragment newInstance(Bundle ClientData) {
        ClientProfileFragment fragment = new ClientProfileFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);*/
        fragment.setArguments(ClientData);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey("data")) {
            client_type =  getArguments().getString("client_type");
            Log.e("client_type", client_type);
            if (client_type.equals("3")){
                bean = (TrainingSessionModel.DataBean) getArguments().get("data");
            }else if (client_type.equals("2")){
                newBean = (GymTrainerScheduleListModel.DataBean) getArguments().get("data");
            }else {
                dataBean = (ClientListModel.DataBean) getArguments().get("data");

            }
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolBarWithBackArrow(mBinding.toolbar.toolbar, getString(R.string.str_client_profile));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_client_profile, container, false);
        userId = String.valueOf(session.getAuthorizedUser(getActivity()).getId());
        userAccessToken = session.getAuthorizedUser(getActivity()).getUserAccessToken();
        getClientProfile();
        setClicks();
        return mBinding.getRoot();
    }

    private void setClicks() {

        if (client_type.equals("3")){
            mBinding.layoutClientProfile.toggle.setChecked(bean.getIs_active() == 1);
            mBinding.layoutClientProfile.toggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
                setAssign(isChecked, bean.getId());
            });
        } else if (client_type.equals("2")){
            mBinding.layoutClientProfile.toggle.setChecked(newBean.getIsActive() == 1);
            mBinding.layoutClientProfile.toggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
                setAssign(isChecked, newBean.getId());
            });
        }else {
            mBinding.layoutClientProfile.toggle.setChecked(dataBean.getIs_active() == 1);
            mBinding.layoutClientProfile.toggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
                setAssign(isChecked,dataBean.getId());
            });
        }


        mBinding.layoutClientProfile.linActivity.setOnClickListener(this);
        mBinding.layoutClientProfile.linWorkout.setOnClickListener(this);
        mBinding.layoutClientProfile.linNutritions.setOnClickListener(this);
        mBinding.layoutClientProfile.linTrainingProgram.setOnClickListener(this);
        mBinding.layoutClientProfile.linSchedules.setOnClickListener(this);
        mBinding.layoutClientProfile.linReports.setOnClickListener(this);
        mBinding.layoutClientProfile.linDocuments.setOnClickListener(this);
        mBinding.layoutClientProfile.linMedical.setOnClickListener(this);
        mBinding.layoutClientProfile.linBasicInfo.setOnClickListener(this);
        mBinding.layoutClientProfile.linPurchase.setOnClickListener(this);

        setImageBackground(mActivity, mBinding.layoutClientProfile.imgActivity, R.drawable.ic_client_profile_activity_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgWorkout, R.drawable.ic_client_profile_workout_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgNutrition, R.drawable.ic_client_profile_nutrition_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgTrainigProgram, R.drawable.ic_client_profile_training_program_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgCheckin, R.drawable.ic_client_profile_checkin_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgReports, R.drawable.ic_client_profile_reports_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgNotes, R.drawable.ic_client_profile_document_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgPurchase, R.drawable.ic_client_profile_purchase_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgMedical, R.drawable.ic_client_profile_medical_info_vector);
        setImageBackground(mActivity, mBinding.layoutClientProfile.imgBasicInfo, R.drawable.ic_client_profile_basic_info_vector);
        mBinding.loadingBar.progressBar.getIndeterminateDrawable().setColorFilter(((BaseActivity) mActivity).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    public void setAssign(boolean isChecked, int id) {
        mBinding.loadingBar.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity(), new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).activateDeactivateClient(id, isChecked ? 1 : 0,null)
                , getCompositeDisposable(), assignWorkout, this);
    }

    private void getClientProfile() {
        Utils.setSwitchTint(mContext, mBinding.layoutClientProfile.toggle);
        if (getArguments() != null) {
            if (client_type.equals("3")){
                bean = (TrainingSessionModel.DataBean) getArguments().get("data");
                Utils.setImage(mContext, mBinding.layoutClientProfile.imgClient, bean.getPhoto());
                mBinding.layoutClientProfile.name.setText(bean.getName());
                mBinding.layoutClientProfile.txtEmail.setText(bean.getEmail());
                mBinding.layoutClientProfile.toggle.setChecked((bean.getIs_active() == 1));
            }else if (client_type.equals("2")){
                newBean = (GymTrainerScheduleListModel.DataBean) getArguments().get("data");
                Utils.setImage(mContext, mBinding.layoutClientProfile.imgClient, newBean.getPhoto());
                mBinding.layoutClientProfile.name.setText(newBean.getName());
                mBinding.layoutClientProfile.txtEmail.setText(newBean.getEmail());
                mBinding.layoutClientProfile.toggle.setChecked((newBean.getIsActive() == 1));
            }else {
                dataBean = (ClientListModel.DataBean) getArguments().get("data");
                Utils.setImage(mContext, mBinding.layoutClientProfile.imgClient, dataBean.getPhoto());
                mBinding.layoutClientProfile.name.setText(dataBean.getName());
                mBinding.layoutClientProfile.txtEmail.setText(dataBean.getEmail());
                mBinding.layoutClientProfile.toggle.setChecked((dataBean.getIs_active() == 1));
            }
        }
    }

    @Override
    public void onClick(View v) {

        if (client_type.equals("3")){
            switch (v.getId()) {
                case R.id.linActivity:
                    startActivity(new Intent(getActivity(), ActivityOfClientActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linWorkout:
                    startActivity(new Intent(getActivity(), ClientProfileWorkoutActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linNutritions:
                    startActivity(new Intent(getActivity(), ClientProfileNutritionActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linTrainingProgram:
                    startActivity(new Intent(getActivity(), ClientProfileTrainingProgramActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linSchedules:
                    startActivity(new Intent(getActivity(), ClientProfileSchedulesActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linReports:
                    startActivity(new Intent(getActivity(), ClientProfileReportActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linDocuments:
                    startActivity(new Intent(getActivity(), ClientDocumentActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linPurchase:
                    startActivity(new Intent(getActivity(), ClientProfilePurchaseActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linMedical:
                    startActivity(new Intent(getActivity(), ClientProfileMedicalFormActivity.class).putExtra("data", bean.getId()));
                    break;

                case R.id.linBasicInfo:
                    startActivity(new Intent(getActivity(), TrainerClientBasicInfoActivity.class).putExtra("client_id", bean.getId()));
                    break;
            }

        }else   if (client_type.equals("2")){
            switch (v.getId()) {
                case R.id.linActivity:
                    startActivity(new Intent(getActivity(), ActivityOfClientActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linWorkout:
                    startActivity(new Intent(getActivity(), ClientProfileWorkoutActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linNutritions:
                    startActivity(new Intent(getActivity(), ClientProfileNutritionActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linTrainingProgram:
                    startActivity(new Intent(getActivity(), ClientProfileTrainingProgramActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linSchedules:
                    startActivity(new Intent(getActivity(), ClientProfileSchedulesActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linReports:
                    startActivity(new Intent(getActivity(), ClientProfileReportActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linDocuments:
                    startActivity(new Intent(getActivity(), ClientDocumentActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linPurchase:
                    startActivity(new Intent(getActivity(), ClientProfilePurchaseActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linMedical:
                    startActivity(new Intent(getActivity(), ClientProfileMedicalFormActivity.class).putExtra("data", newBean.getUserId()));
                    break;

                case R.id.linBasicInfo:
                    startActivity(new Intent(getActivity(), TrainerClientBasicInfoActivity.class).putExtra("client_id", newBean.getUserId()));
                    break;
            }

        }else {
            switch (v.getId()) {
                case R.id.linActivity:
                    startActivity(new Intent(getActivity(), ActivityOfClientActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linWorkout:
                    startActivity(new Intent(getActivity(), ClientProfileWorkoutActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linNutritions:
                    startActivity(new Intent(getActivity(), ClientProfileNutritionActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linTrainingProgram:
                    startActivity(new Intent(getActivity(), ClientProfileTrainingProgramActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linSchedules:
                    startActivity(new Intent(getActivity(), ClientProfileSchedulesActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linReports:
                    startActivity(new Intent(getActivity(), ClientProfileReportActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linDocuments:
                    startActivity(new Intent(getActivity(), ClientDocumentActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linPurchase:
                    startActivity(new Intent(getActivity(), ClientProfilePurchaseActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linMedical:
                    startActivity(new Intent(getActivity(), ClientProfileMedicalFormActivity.class).putExtra("data", dataBean.getId()));
                    break;

                case R.id.linBasicInfo:
                    startActivity(new Intent(getActivity(), TrainerClientBasicInfoActivity.class).putExtra("client_id", dataBean.getId()));
                    break;
            }
        }
    }
    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case assignWorkout:
                mBinding.loadingBar.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse != null && commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    showSnackBar(mBinding.linear, "active changed successfully", Snackbar.LENGTH_LONG);
                } else {
                    showSnackBar(mBinding.linear, commonApiResponse.getMessage(), Snackbar.LENGTH_LONG);
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loadingBar.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    public static void setImageBackground(Context context, ImageView fab, int id) {
        int color = ContextCompat.getColor(context, R.color.colorAccent);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, id, fab);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(color);
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        path2.setFillColor(color);
        VectorDrawableCompat.VFullPath path3 = vector.findPathByName("path3");
        path3.setFillColor(color);
        VectorDrawableCompat.VFullPath path4 = vector.findPathByName("path4");
        path4.setFillColor(color);
        VectorDrawableCompat.VFullPath path5 = vector.findPathByName("path5");
        path5.setFillColor(color);
        VectorDrawableCompat.VFullPath path6 = vector.findPathByName("path6");
        path6.setFillColor(color);
        VectorDrawableCompat.VFullPath path7 = vector.findPathByName("path7");
        path7.setFillColor(color);
        VectorDrawableCompat.VFullPath path8 = vector.findPathByName("path8");
        path8.setFillColor(color);
        VectorDrawableCompat.VFullPath path9 = vector.findPathByName("path9");
        path9.setFillColor(color);
        VectorDrawableCompat.VFullPath path10 = vector.findPathByName("path10");
        path10.setFillColor(color);
        VectorDrawableCompat.VFullPath path11 = vector.findPathByName("path11");
        path11.setFillColor(color);
        VectorDrawableCompat.VFullPath path12 = vector.findPathByName("path12");
        path12.setFillColor(color);
        VectorDrawableCompat.VFullPath path13 = vector.findPathByName("path13");
        path13.setFillColor(color);
        VectorDrawableCompat.VFullPath path14 = vector.findPathByName("path14");
        path14.setFillColor(color);
        VectorDrawableCompat.VFullPath path15 = vector.findPathByName("path15");
        path15.setFillColor(color);
        VectorDrawableCompat.VFullPath path16 = vector.findPathByName("path16");
        path16.setFillColor(color);
        VectorDrawableCompat.VFullPath path17 = vector.findPathByName("path17");
        path17.setFillColor(color);
        VectorDrawableCompat.VFullPath path18 = vector.findPathByName("path18");
        path18.setFillColor(color);
        VectorDrawableCompat.VFullPath path19 = vector.findPathByName("path19");
        path19.setFillColor(color);
        VectorDrawableCompat.VFullPath path20 = vector.findPathByName("path20");
        path20.setFillColor(color);
        VectorDrawableCompat.VFullPath path21 = vector.findPathByName("path21");
        path21.setFillColor(color);
        VectorDrawableCompat.VFullPath path22 = vector.findPathByName("path22");
        path22.setFillColor(color);
        VectorDrawableCompat.VFullPath path23 = vector.findPathByName("path23");
        path23.setFillColor(color);
        VectorDrawableCompat.VFullPath path24 = vector.findPathByName("path24");
        path24.setFillColor(color);
        VectorDrawableCompat.VFullPath path25 = vector.findPathByName("path25");
        path25.setFillColor(color);
        VectorDrawableCompat.VFullPath path26 = vector.findPathByName("path26");
        path26.setFillColor(color);
        VectorDrawableCompat.VFullPath path27 = vector.findPathByName("path27");
        path27.setFillColor(color);
        VectorDrawableCompat.VFullPath path28 = vector.findPathByName("path28");
        path28.setFillColor(color);
        VectorDrawableCompat.VFullPath path29 = vector.findPathByName("path29");
        path29.setFillColor(color);
        VectorDrawableCompat.VFullPath path30 = vector.findPathByName("path30");
        path30.setFillColor(color);
        VectorDrawableCompat.VFullPath path31 = vector.findPathByName("path31");
        path31.setFillColor(color);
        VectorDrawableCompat.VFullPath path32 = vector.findPathByName("path32");
        path32.setFillColor(color);
        VectorDrawableCompat.VFullPath path33 = vector.findPathByName("path33");
        path33.setFillColor(color);
        VectorDrawableCompat.VFullPath path34 = vector.findPathByName("path34");
        path34.setFillColor(color);
        VectorDrawableCompat.VFullPath path35 = vector.findPathByName("path35");
        path35.setFillColor(color);
        VectorDrawableCompat.VFullPath path36 = vector.findPathByName("path36");
        path36.setFillColor(color);
        VectorDrawableCompat.VFullPath path37 = vector.findPathByName("path37");
        path37.setFillColor(color);
        if (vector.findPathByName("path38") != null) {
            VectorDrawableCompat.VFullPath path38 = vector.findPathByName("path38");
            path38.setFillColor(color);
        }
        if (vector.findPathByName("path39") != null) {
            VectorDrawableCompat.VFullPath path39 = vector.findPathByName("path39");
            path39.setFillColor(color);
        }
    }

}
