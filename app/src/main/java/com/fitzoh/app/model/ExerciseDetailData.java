package com.fitzoh.app.model;

public class ExerciseDetailData {


    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private int id;
        private String exercise_name;
        private String video_title;
        private String video_desc;
        private String video_url;
        private String muscle_name;
        private String equipment_name;
        private String instructions;
        private String personal_highest_weight;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExercise_name() {
            return exercise_name;
        }

        public void setExercise_name(String exercise_name) {
            this.exercise_name = exercise_name;
        }

        public String getVideo_title() {
            return video_title;
        }

        public void setVideo_title(String video_title) {
            this.video_title = video_title;
        }

        public String getVideo_desc() {
            return video_desc;
        }

        public void setVideo_desc(String video_desc) {
            this.video_desc = video_desc;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getMuscle_name() {
            return muscle_name;
        }

        public void setMuscle_name(String muscle_name) {
            this.muscle_name = muscle_name;
        }

        public String getEquipment_name() {
            return equipment_name;
        }

        public void setEquipment_name(String equipment_name) {
            this.equipment_name = equipment_name;
        }

        public String getInstructions() {
            return instructions;
        }

        public void setInstructions(String instructions) {
            this.instructions = instructions;
        }

        public String getPersonal_highest_weight() {
            return personal_highest_weight;
        }

        public void setPersonal_highest_weight(String personal_highest_weight) {
            this.personal_highest_weight = personal_highest_weight;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
