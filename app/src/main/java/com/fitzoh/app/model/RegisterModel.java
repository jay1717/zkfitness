package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterModel implements Serializable {


    /**
     * success : 200
     * message : Please check your mail for OTP!
     * data : {"id":103,"is_verified":null,"user_access_token":"tc664622a5bba6ac5e2a854f1","access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBkOWMyMzU2YjQ1ZTIwZWRmY2RlMWMwNWJiMzUzMmFjZGRiNGNkYmJkY2ExYThkZjZlOTBlNDk4OGE5MDc3YjM4Y2U3ZGNmYjQyYzg0MDEyIn0.eyJhdWQiOiIxNSIsImp0aSI6IjBkOWMyMzU2YjQ1ZTIwZWRmY2RlMWMwNWJiMzUzMmFjZGRiNGNkYmJkY2ExYThkZjZlOTBlNDk4OGE5MDc3YjM4Y2U3ZGNmYjQyYzg0MDEyIiwiaWF0IjoxNTM2OTE5NjIzLCJuYmYiOjE1MzY5MTk2MjMsImV4cCI6MTU2ODQ1NTYyMywic3ViIjoiMTAzIiwic2NvcGVzIjpbXX0.Fh895PT6TB63i5gpqASwsxSsY6ZpKxF0hyRNw4TRxJ02wLG8NQ2s5x3i1A2CAaPhKSSjqDEVFtuqHqwFbphH1xQYkQSZo8UoXDLd3epmXLMb0sc2OtFLwD29i-I-dMQs1oFAzEGBC95DKKwYuUF3dAlu7na5Yr3h93sHDDKUka8c54OJ24IgQsu-xZTYV0NZAl72P_wVvCtGpMDSfLg7cenUxRRh26LZ0CYWXmipkLiAH2UYW9BWvzrRqRDeySkdGucQiFEb0z2totsbbTQGtEBePHo6ExXCUFJgkCY9Cu7u5HYNhdscSnGnrhCsFeEangxxpnA3ABzjcmrEDabTLfOjcqeNXgpl1_FpQWaFDFlCRmbahm4YGlMB_SZGgSP-Tegwluv-P_u7w8P7BvTA4utMhOg6n3J4jIo27gefpi-31nSIAi47HoofokH5cWDEJpii71Edbd9Rv8k1NCaYpfifEDU8JrjdC624NM18LmI-qcne1w8vX_pM13Iial9D3ZwANQnuQ07B1975anHFC88Z5lzilXyotFl_OVLG34DTTjBj7pVa-j9w9sXfBL-dMqAUWIYarJieGmCbax3-U3ovINAIiTu5o5rAyotvLjc_HmR9SQisG2JoMjWMBd9nrsm3yhjfEYruCfGGL0LYO2hkkPPAJcASq0yjWdzB0Ww","token_type":"Bearer","expires_at":"2019-09-14 10:07:03"}
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 103
         * is_verified : null
         * user_access_token : tc664622a5bba6ac5e2a854f1
         * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBkOWMyMzU2YjQ1ZTIwZWRmY2RlMWMwNWJiMzUzMmFjZGRiNGNkYmJkY2ExYThkZjZlOTBlNDk4OGE5MDc3YjM4Y2U3ZGNmYjQyYzg0MDEyIn0.eyJhdWQiOiIxNSIsImp0aSI6IjBkOWMyMzU2YjQ1ZTIwZWRmY2RlMWMwNWJiMzUzMmFjZGRiNGNkYmJkY2ExYThkZjZlOTBlNDk4OGE5MDc3YjM4Y2U3ZGNmYjQyYzg0MDEyIiwiaWF0IjoxNTM2OTE5NjIzLCJuYmYiOjE1MzY5MTk2MjMsImV4cCI6MTU2ODQ1NTYyMywic3ViIjoiMTAzIiwic2NvcGVzIjpbXX0.Fh895PT6TB63i5gpqASwsxSsY6ZpKxF0hyRNw4TRxJ02wLG8NQ2s5x3i1A2CAaPhKSSjqDEVFtuqHqwFbphH1xQYkQSZo8UoXDLd3epmXLMb0sc2OtFLwD29i-I-dMQs1oFAzEGBC95DKKwYuUF3dAlu7na5Yr3h93sHDDKUka8c54OJ24IgQsu-xZTYV0NZAl72P_wVvCtGpMDSfLg7cenUxRRh26LZ0CYWXmipkLiAH2UYW9BWvzrRqRDeySkdGucQiFEb0z2totsbbTQGtEBePHo6ExXCUFJgkCY9Cu7u5HYNhdscSnGnrhCsFeEangxxpnA3ABzjcmrEDabTLfOjcqeNXgpl1_FpQWaFDFlCRmbahm4YGlMB_SZGgSP-Tegwluv-P_u7w8P7BvTA4utMhOg6n3J4jIo27gefpi-31nSIAi47HoofokH5cWDEJpii71Edbd9Rv8k1NCaYpfifEDU8JrjdC624NM18LmI-qcne1w8vX_pM13Iial9D3ZwANQnuQ07B1975anHFC88Z5lzilXyotFl_OVLG34DTTjBj7pVa-j9w9sXfBL-dMqAUWIYarJieGmCbax3-U3ovINAIiTu5o5rAyotvLjc_HmR9SQisG2JoMjWMBd9nrsm3yhjfEYruCfGGL0LYO2hkkPPAJcASq0yjWdzB0Ww
         * token_type : Bearer
         * expires_at : 2019-09-14 10:07:03
         */

        @SerializedName("id")
        private int id;
        @SerializedName("is_verified")
        private int isVerified;
        @SerializedName("user_access_token")
        private String userAccessToken;
        @SerializedName("access_token")
        private String accessToken;
        @SerializedName("token_type")
        private String tokenType;
        @SerializedName("expires_at")
        private String expiresAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(int isVerified) {
            this.isVerified = isVerified;
        }

        public String getUserAccessToken() {
            return userAccessToken;
        }

        public void setUserAccessToken(String userAccessToken) {
            this.userAccessToken = userAccessToken;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public String getExpiresAt() {
            return expiresAt;
        }

        public void setExpiresAt(String expiresAt) {
            this.expiresAt = expiresAt;
        }
    }
}
