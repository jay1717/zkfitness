package com.fitzoh.app.model;

import java.util.List;

public class ClientUpdateListData {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"name":"Jinank","photo":"php6.shaligraminfotech.com/public/images/transformations/1005/1539949898_1539949897595.76.png","workout_name":"Shoulder Workout","no_of_workouts":4,"no_of_diet":4},{"id":1,"name":"Jinank","photo":"php6.shaligraminfotech.com/public/images/transformations/1005/1539949898_1539949897595.76.png","workout_name":"Shoulder Workout","no_of_workouts":4,"no_of_diet":4}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : Jinank
         * photo : php6.shaligraminfotech.com/public/images/transformations/1005/1539949898_1539949897595.76.png
         * workout_name : Shoulder Workout
         * no_of_workouts : 4
         * no_of_diet : 4
         */

        private int id;
        private String name;
        private String photo;
        private String workout_name;
        private int no_of_workouts;
        private int no_of_diet;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getWorkout_name() {
            return workout_name;
        }

        public void setWorkout_name(String workout_name) {
            this.workout_name = workout_name;
        }

        public int getNo_of_workouts() {
            return no_of_workouts;
        }

        public void setNo_of_workouts(int no_of_workouts) {
            this.no_of_workouts = no_of_workouts;
        }

        public int getNo_of_diet() {
            return no_of_diet;
        }

        public void setNo_of_diet(int no_of_diet) {
            this.no_of_diet = no_of_diet;
        }
    }
}
