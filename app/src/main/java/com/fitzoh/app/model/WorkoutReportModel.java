package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class WorkoutReportModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"key":"exe0","value":4},{"key":"exe1","value":4},{"key":"exe2","value":4},{"key":"exe3","value":4},{"key":"exe4","value":4},{"key":"exe5","value":4},{"key":"exe6","value":4},{"key":"exe7","value":4},{"key":"exe8","value":4},{"key":"exe9","value":4}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * key : Qaex1
         * value : 4.50
         */

        private String key;
        private float value;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public float getValue() {
            return value;
        }

        public void setValue(float value) {
            this.value = value;
        }
    }
}
