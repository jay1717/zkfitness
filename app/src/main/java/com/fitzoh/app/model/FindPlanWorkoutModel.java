package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class FindPlanWorkoutModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"name":"Workout","exercise_count":5,"no_of_sets":8,"assigned_to":[],"price":0}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * name : Workout
         * exercise_count : 5
         * no_of_sets : 8
         * assigned_to : []
         * price : 0
         */

        private int id;
        private String name;
        private int exercise_count;
        private int no_of_sets;
        private int price;
        private int is_purchased;
        private List<?> assigned_to;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getExercise_count() {
            return exercise_count;
        }

        public void setExercise_count(int exercise_count) {
            this.exercise_count = exercise_count;
        }

        public int getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(int no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public List<?> getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(List<?> assigned_to) {
            this.assigned_to = assigned_to;
        }

        public int getIs_purchased() {
            return is_purchased;
        }

        public void setIs_purchased(int is_purchased) {
            this.is_purchased = is_purchased;
        }
    }
}
