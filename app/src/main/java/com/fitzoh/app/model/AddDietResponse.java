package com.fitzoh.app.model;

public class AddDietResponse {
    private int status;
    private String message;
    private DietListModel.DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DietListModel.DataBean getData() {
        return data;
    }

    public void setData(DietListModel.DataBean data) {
        this.data = data;
    }
}
