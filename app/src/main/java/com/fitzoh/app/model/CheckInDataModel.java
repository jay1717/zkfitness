package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class CheckInDataModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":8,"date":"2018-10-31","checkin_images":[{"id":9,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540307762_Hydrangeas.jpg"},{"id":10,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540308116_Hydrangeas.jpg"},{"id":13,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443941_1540443935290.81.jpg"}]},{"id":9,"date":"2018-10-31","checkin_images":[{"id":14,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443942_1540443935304.83.jpg"},{"id":15,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443983_1540443980033.43.jpg"},{"id":16,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443983_1540443980046.88.jpg"}]},{"id":10,"date":"2018-10-31","checkin_images":[]},{"id":11,"date":"2018-10-31","checkin_images":[]},{"id":12,"date":"2018-10-31","checkin_images":[]},{"id":13,"date":"2018-10-31","checkin_images":[]},{"id":14,"date":"2018-10-31","checkin_images":[]},{"id":15,"date":"2018-10-31","checkin_images":[]},{"id":16,"date":"2018-10-31","checkin_images":[]},{"id":17,"date":"2018-10-31","checkin_images":[]},{"id":18,"date":"2018-10-31","checkin_images":[]},{"id":19,"date":"2018-10-31","checkin_images":[]},{"id":20,"date":"2018-10-31","checkin_images":[]},{"id":21,"date":"2018-10-31","checkin_images":[]},{"id":22,"date":"2018-10-31","checkin_images":[]},{"id":23,"date":"2018-10-31","checkin_images":[]},{"id":24,"date":"2018-10-31","checkin_images":[]},{"id":25,"date":"2018-10-31","checkin_images":[]},{"id":26,"date":"2018-10-31","checkin_images":[]},{"id":27,"date":"2018-10-31","checkin_images":[]},{"id":28,"date":"2018-10-31","checkin_images":[]},{"id":29,"date":"2018-10-31","checkin_images":[]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 8
         * date : 2018-10-31
         * checkin_images : [{"id":9,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540307762_Hydrangeas.jpg"},{"id":10,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540308116_Hydrangeas.jpg"},{"id":13,"image":"https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443941_1540443935290.81.jpg"}]
         */

        private int id;
        private String date;
        private List<CheckinImagesBean> checkin_images;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<CheckinImagesBean> getCheckin_images() {
            return checkin_images;
        }

        public void setCheckin_images(List<CheckinImagesBean> checkin_images) {
            this.checkin_images = checkin_images;
        }

        public static class CheckinImagesBean implements Serializable {
            /**
             * id : 9
             * image : https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540307762_Hydrangeas.jpg
             */

            private int id;
            private String image;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}
