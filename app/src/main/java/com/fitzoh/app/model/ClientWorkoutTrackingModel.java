package com.fitzoh.app.model;

import java.util.List;

public class ClientWorkoutTrackingModel {

    /**
     * status : 200
     * message : success
     * data : [{"workout_id":"187","workout_name":"","date":"2018-11-28"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * workout_id : 187
         * workout_name :
         * date : 2018-11-28
         */

        private int workout_id;
        private String workout_name;
        private String date;

        public int getWorkout_id() {
            return workout_id;
        }

        public void setWorkout_id(int workout_id) {
            this.workout_id = workout_id;
        }

        public String getWorkout_name() {
            return workout_name;
        }

        public void setWorkout_name(String workout_name) {
            this.workout_name = workout_name;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}
