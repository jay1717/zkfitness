package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ProfileWorkoutModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":146,"name":"new exercise1","exercise_count":7,"no_of_sets":0,"is_assigned":1},{"id":173,"name":"qa1Copy","exercise_count":9,"no_of_sets":4,"is_assigned":1},{"id":222,"name":"ABC","exercise_count":3,"no_of_sets":3,"is_assigned":1},{"id":273,"name":"test prakash123","exercise_count":3,"no_of_sets":3,"is_assigned":1},{"id":278,"name":"Dixita - Fitness","exercise_count":2,"no_of_sets":2,"is_assigned":1},{"id":344,"name":"aktest","exercise_count":1,"no_of_sets":1,"is_assigned":1},{"id":349,"name":"staffs","exercise_count":1,"no_of_sets":2,"is_assigned":1},{"id":358,"name":"Nidhi Test1","exercise_count":2,"no_of_sets":6,"is_assigned":1},{"id":366,"name":"Client Test Edit","exercise_count":6,"no_of_sets":0,"is_assigned":1},{"id":367,"name":"Nidhi Profile Edit","exercise_count":5,"no_of_sets":0,"is_assigned":1},{"id":368,"name":"Jonah Edit Profile","exercise_count":5,"no_of_sets":0,"is_assigned":1}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 146
         * name : new exercise1
         * exercise_count : 7
         * no_of_sets : 0
         * is_assigned : 1
         */

        private int id;
        private String name;
        private int exercise_count;
        private int no_of_sets;
        private int is_assigned;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getExercise_count() {
            return exercise_count;
        }

        public void setExercise_count(int exercise_count) {
            this.exercise_count = exercise_count;
        }

        public int getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(int no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }
    }
}
