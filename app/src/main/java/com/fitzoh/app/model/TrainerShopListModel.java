package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainerShopListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":33,"category_id":"43","brand_id":"48","name":"custom product","carbs":"10.00","fat":"30.00","calories":null,"description":"custom description","protein":"30.00","ingredients":"custom ingredients","price":"10.00","qty":"20","image":[],"discounted_price":"15.00","trainer_id":"1","is_master_product":"0","category":"custom category","currency":"","brand":"custom brand"},{"id":45,"category_id":"46","brand_id":"50","name":"New Product","carbs":"10.00","fat":"30.00","calories":null,"description":"custom description","protein":"30.00","ingredients":"custom ingredients","price":"10.00","qty":"20","image":[],"discounted_price":"15.00","trainer_id":"1","is_master_product":"0","category":"custom category","currency":"","brand":"custom brand"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 33
         * category_id : 43
         * brand_id : 48
         * name : custom product
         * carbs : 10.00
         * fat : 30.00
         * calories : null
         * description : custom description
         * protein : 30.00
         * ingredients : custom ingredients
         * price : 10.00
         * qty : 20
         * image : []
         * discounted_price : 15.00
         * trainer_id : 1
         * is_master_product : 0
         * category : custom category
         * currency :
         * brand : custom brand
         */

        private int id;
        private String category_id;
        private String brand_id;
        private String name;
        private String carbs;
        private String fat;
        private String calories;
        private String description;
        private String protein;
        private String ingredients;
        private String price;
        private String qty;
        private String discounted_price;
        private String trainer_id;
        private String is_master_product;
        private String category;
        private String currency;
        private String brand;
        private List<String> image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(String brand_id) {
            this.brand_id = brand_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getIngredients() {
            return ingredients;
        }

        public void setIngredients(String ingredients) {
            this.ingredients = ingredients;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getDiscounted_price() {
            return discounted_price;
        }

        public void setDiscounted_price(String discounted_price) {
            this.discounted_price = discounted_price;
        }

        public String getTrainer_id() {
            return trainer_id;
        }

        public void setTrainer_id(String trainer_id) {
            this.trainer_id = trainer_id;
        }

        public String getIs_master_product() {
            return is_master_product;
        }

        public void setIs_master_product(String is_master_product) {
            this.is_master_product = is_master_product;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }
    }
}
