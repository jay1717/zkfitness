package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class RequestListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":5,"client_id":"102","status":"1","name":"nidhi","email":"nidhi@gmail.com","sent_time":"2018-10-09 18:33:57","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg"},{"id":6,"client_id":"76","status":"1","name":"urvi joshi","email":"urvi.joshi019@gmail.com","sent_time":"2018-10-11 09:28:24","photo":""},{"id":7,"client_id":"6","status":"1","name":"hrien","email":"hiren.d@sgit.iioi","sent_time":"2018-10-12 07:00:51","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg"},{"id":14,"client_id":"726","status":"0","name":"TT Name","email":"tt@gmail.com","sent_time":"2018-10-16 12:55:03","photo":""}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * id : 5
         * client_id : 102
         * status : 1
         * name : nidhi
         * email : nidhi@gmail.com
         * sent_time : 2018-10-09 18:33:57
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg
         */

        private int id;
        private String client_id;
        private String status;
        private String name;
        private String email;
        private String sent_time;
        private String photo;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSent_time() {
            return sent_time;
        }

        public void setSent_time(String sent_time) {
            this.sent_time = sent_time;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
