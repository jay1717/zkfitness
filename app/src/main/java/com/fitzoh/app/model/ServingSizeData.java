package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServingSizeData {

    /**
     * status : 200
     * message : success
     * data : [{"id":1,"value":0.5},{"id":2,"value":1},{"id":3,"value":1.5},{"id":4,"value":2},{"id":5,"value":2.5},{"id":6,"value":3},{"id":7,"value":3.5},{"id":8,"value":4},{"id":9,"value":4.5},{"id":10,"value":5},{"id":11,"value":5.5},{"id":12,"value":6},{"id":13,"value":6.5},{"id":14,"value":7},{"id":15,"value":7.5},{"id":16,"value":8},{"id":17,"value":8.5},{"id":18,"value":9},{"id":19,"value":9.5},{"id":20,"value":10},{"id":21,"value":10.5},{"id":22,"value":11},{"id":23,"value":11.5},{"id":24,"value":12},{"id":25,"value":12.5},{"id":26,"value":13},{"id":27,"value":13.5},{"id":28,"value":14},{"id":29,"value":14.5},{"id":30,"value":15},{"id":31,"value":15.5},{"id":32,"value":16},{"id":33,"value":16.5},{"id":34,"value":17},{"id":35,"value":17.5},{"id":36,"value":18},{"id":37,"value":18.5},{"id":38,"value":19},{"id":39,"value":19.5},{"id":40,"value":20},{"id":41,"value":20.5},{"id":42,"value":21},{"id":43,"value":21.5},{"id":44,"value":22},{"id":45,"value":22.5},{"id":46,"value":23},{"id":47,"value":23.5},{"id":48,"value":24},{"id":49,"value":24.5},{"id":50,"value":25},{"id":51,"value":25.5},{"id":52,"value":26},{"id":53,"value":26.5},{"id":54,"value":27},{"id":55,"value":27.5},{"id":56,"value":28},{"id":57,"value":28.5},{"id":58,"value":29},{"id":59,"value":29.5},{"id":60,"value":30},{"id":61,"value":30.5},{"id":62,"value":31},{"id":63,"value":31.5},{"id":64,"value":32},{"id":65,"value":32.5},{"id":66,"value":33},{"id":67,"value":33.5},{"id":68,"value":34},{"id":69,"value":34.5},{"id":70,"value":35},{"id":71,"value":35.5},{"id":72,"value":36},{"id":73,"value":36.5},{"id":74,"value":37},{"id":75,"value":37.5},{"id":76,"value":38},{"id":77,"value":38.5},{"id":78,"value":39},{"id":79,"value":39.5},{"id":80,"value":40},{"id":81,"value":40.5},{"id":82,"value":41},{"id":83,"value":41.5},{"id":84,"value":42},{"id":85,"value":42.5},{"id":86,"value":43},{"id":87,"value":43.5},{"id":88,"value":44},{"id":89,"value":44.5},{"id":90,"value":45},{"id":91,"value":45.5},{"id":92,"value":46},{"id":93,"value":46.5},{"id":94,"value":47},{"id":95,"value":47.5},{"id":96,"value":48},{"id":97,"value":48.5},{"id":98,"value":49},{"id":99,"value":49.5},{"id":100,"value":50}]
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * value : 0.5
         */

        @SerializedName("id")
        private int id;
        @SerializedName("value")
        private String value;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
