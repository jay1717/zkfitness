package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

public class BankDetailModel {

    /**
     * status : 200
     * message : success
     * data : {"id":250,"user_id":"841","bank_name":"","account_holder_name":"","account_number":"","ifsc_code":"","branch":""}
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 250
         * user_id : 841
         * bank_name :
         * account_holder_name :
         * account_number :
         * ifsc_code :
         * branch :
         */

        @SerializedName("id")
        private int id;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("bank_name")
        private String bankName;
        @SerializedName("account_holder_name")
        private String accountHolderName;
        @SerializedName("account_number")
        private String accountNumber;
        @SerializedName("ifsc_code")
        private String ifscCode;
        @SerializedName("branch")
        private String branch;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getBankName() {
            return bankName;
        }

        public void setBankName(String bankName) {
            this.bankName = bankName;
        }

        public String getAccountHolderName() {
            return accountHolderName;
        }

        public void setAccountHolderName(String accountHolderName) {
            this.accountHolderName = accountHolderName;
        }

        public String getAccountNumber() {
            return accountNumber;
        }

        public void setAccountNumber(String accountNumber) {
            this.accountNumber = accountNumber;
        }

        public String getIfscCode() {
            return ifscCode;
        }

        public void setIfscCode(String ifscCode) {
            this.ifscCode = ifscCode;
        }

        public String getBranch() {
            return branch;
        }

        public void setBranch(String branch) {
            this.branch = branch;
        }
    }
}
