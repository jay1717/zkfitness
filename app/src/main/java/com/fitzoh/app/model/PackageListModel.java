package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class PackageListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":20,"weeks":"5","package_name":"package 1","price":"100.00","discounted_price":"5.00","desc":"about for package, about for package, about for package, about for package","media":"https://php6.shaligraminfotech.com/public/images/packages/1539239723_54.png"},{"id":21,"weeks":"5","package_name":"package 2","price":"100.00","discounted_price":"5.00","desc":"about for package, about for package, about for package, about for package","media":"https://php6.shaligraminfotech.com/public/images/packages/1539239771_54.png"},{"id":22,"weeks":"5","package_name":"package 3","price":"100.00","discounted_price":"5.00","desc":"about for package, about for package, about for package, about for package","media":"https://php6.shaligraminfotech.com/public/images/packages/1539239775_54.png"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 141
         * weeks : 2
         * package_name : testAkash
         * price : 20.00
         * training_program_id : 59
         * discounted_price : 15.00
         * desc : Checking
         * media : https://php6.shaligraminfotech.com/public/images/packages/11/1540280933_1540280933229.63.png
         * media_type :
         * youtube_url :
         * training_program_name : check
         */

        private int id;
        private String weeks;
        private String package_name;
        private String price;
        private String training_program_id;
        private String discounted_price;
        private String desc;
        private String media;
        private String media_type;
        private String youtube_url;
        private String training_program_name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getWeeks() {
            return weeks;
        }

        public void setWeeks(String weeks) {
            this.weeks = weeks;
        }

        public String getPackage_name() {
            return package_name;
        }

        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getTraining_program_id() {
            return training_program_id;
        }

        public void setTraining_program_id(String training_program_id) {
            this.training_program_id = training_program_id;
        }

        public String getDiscounted_price() {
            return discounted_price;
        }

        public void setDiscounted_price(String discounted_price) {
            this.discounted_price = discounted_price;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getMedia() {
            return media;
        }

        public void setMedia(String media) {
            this.media = media;
        }

        public String getMedia_type() {
            return media_type;
        }

        public void setMedia_type(String media_type) {
            this.media_type = media_type;
        }

        public String getYoutube_url() {
            return youtube_url;
        }

        public void setYoutube_url(String youtube_url) {
            this.youtube_url = youtube_url;
        }

        public String getTraining_program_name() {
            return training_program_name;
        }

        public void setTraining_program_name(String training_program_name) {
            this.training_program_name = training_program_name;
        }
    }
}
