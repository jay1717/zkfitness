package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainingProgramList implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":3,"title":"First Training Program","weeks":2,"assigned_to":{"id":"10","name":"TEST CLIENT","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg"}},{"id":18,"title":"Shoulder Workout - edited","weeks":2,"assigned_to":{"id":"10","name":"TEST CLIENT","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg"}}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 3
         * title : First Training Program
         * weeks : 2
         * assigned_to : {"id":"10","name":"TEST CLIENT","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg"}
         */

        private int id;
        private String title;
        private int weeks;
        private int is_assigned;
        private List<AssignedToBean> assigned_to;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getWeeks() {
            return weeks;
        }

        public void setWeeks(int weeks) {
            this.weeks = weeks;
        }

        public List<AssignedToBean> getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(List<AssignedToBean> assigned_to) {
            this.assigned_to = assigned_to;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }
    }

}
