package com.fitzoh.app.model;

public class AddWorkoutResponse {
    private int status;
    private String message;
    private WorkOutListModel.DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WorkOutListModel.DataBean getData() {
        return data;
    }

    public void setData(WorkOutListModel.DataBean data) {
        this.data = data;
    }
}
