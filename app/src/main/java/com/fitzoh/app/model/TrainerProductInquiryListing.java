package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainerProductInquiryListing implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"inquiries":[{"product_name":"Whey Protein","product_image":""},{"product_name":"Whey Protein","product_image":"whey.jpg"}],"client_name":"Dipsheekha","client_photo":"default.jpeg","no_of_inquiries":2},{"inquiries":[{"product_name":"Whey Protein","product_image":"whey.jpg"},{"product_name":"Whey Protein","product_image":"whey.jpg"},{"product_name":"Whey Protein","product_image":"whey.jpg"},{"product_name":"Whey Protein","product_image":"whey.jpg"}],"client_name":"by","client_photo":"1539932865_1538821006.jpg","no_of_inquiries":2}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * inquiries : [{"product_name":"Whey Protein","product_image":""},{"product_name":"Whey Protein","product_image":"whey.jpg"}]
         * client_name : Dipsheekha
         * client_photo : default.jpeg
         * no_of_inquiries : 2
         */

        private String client_name;
        private String client_photo;
        private int no_of_inquiries;
        private List<InquiriesBean> inquiries;

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }

        public String getClient_photo() {
            return client_photo;
        }

        public void setClient_photo(String client_photo) {
            this.client_photo = client_photo;
        }

        public int getNo_of_inquiries() {
            return no_of_inquiries;
        }

        public void setNo_of_inquiries(int no_of_inquiries) {
            this.no_of_inquiries = no_of_inquiries;
        }

        public List<InquiriesBean> getInquiries() {
            return inquiries;
        }

        public void setInquiries(List<InquiriesBean> inquiries) {
            this.inquiries = inquiries;
        }

        public static class InquiriesBean implements Serializable {
            /**
             * product_name : Whey Protein
             * product_image :
             */

            private String product_name;
            private String product_image;
            private String qty;
            private String price;

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getProduct_image() {
                return product_image;
            }

            public void setProduct_image(String product_image) {
                this.product_image = product_image;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }
    }
}
