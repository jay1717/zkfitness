package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CheckinResponse {
    /**
     * status : 200
     * message : success
     * data : [{"id":24,"date":"2019-01-04","checkin_images":[]}]
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 24
         * date : 2019-01-04
         * checkin_images : []
         */

        @SerializedName("id")
        private int id;
        @SerializedName("date")
        private String date;
        @SerializedName("checkin_images")
        private List<?> checkinImages;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<?> getCheckinImages() {
            return checkinImages;
        }

        public void setCheckinImages(List<?> checkinImages) {
            this.checkinImages = checkinImages;
        }
    }
}
