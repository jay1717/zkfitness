package com.fitzoh.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class GetClientDetailModel {

    /**
     * status : 200
     * message : success
     * data : {"id":17,"name":"by","email":"pa@h.com","about":"","photo":"https://php6.shaligraminfotech.com/public/images/user_images/1539932865_1538821006.jpg","trainer_photo":"","total_meals":3,"todays_diet":[{"id":668,"diet_name":"Breakfast","time":"11:45","food":[{"id":10374,"name":"Cheeseburger with 1/4 Lb Meat, Grilled Onions on Rye Bun","serving_size":"1"},{"id":11535,"name":"Vegetable Samosas","serving_size":"2"}]},{"id":669,"diet_name":"Lunch","time":"11:45","food":[{"id":3208,"name":"Baked or Broiled Fish","serving_size":"1"}]},{"id":670,"diet_name":"Dinner","time":"11:45"}],"todays_workout":[{"id":149,"workout_name":"temp","sets_count":0,"exercises_count":1}]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable {
        /**
         * id : 17
         * name : by
         * email : pa@h.com
         * about :
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/1539932865_1538821006.jpg
         * trainer_photo :
         * total_meals : 3
         * todays_diet : [{"id":668,"diet_name":"Breakfast","time":"11:45","food":[{"id":10374,"name":"Cheeseburger with 1/4 Lb Meat, Grilled Onions on Rye Bun","serving_size":"1"},{"id":11535,"name":"Vegetable Samosas","serving_size":"2"}]},{"id":669,"diet_name":"Lunch","time":"11:45","food":[{"id":3208,"name":"Baked or Broiled Fish","serving_size":"1"}]},{"id":670,"diet_name":"Dinner","time":"11:45"}]
         * todays_workout : [{"id":149,"workout_name":"temp","sets_count":0,"exercises_count":1}]
         */

        private int id;
        private String name;
        private String email;
        private String about;
        private String photo;
        private String trainer_photo;
        private String allow_to_change_date;
        private int total_meals;

        private ArrayList<TodaysDietBean> todays_diet;
        private ArrayList<TodaysWorkoutBean> todays_workout;
        private ArrayList<TodaysWorkoutBean> clients_workout;
        private int is_assign;


        protected DataBean(Parcel in) {
            id = in.readInt();
            name = in.readString();
            email = in.readString();
            about = in.readString();
            photo = in.readString();
            trainer_photo = in.readString();
            allow_to_change_date = in.readString();
            total_meals = in.readInt();
            todays_diet = in.createTypedArrayList(TodaysDietBean.CREATOR);
            todays_workout = in.createTypedArrayList(TodaysWorkoutBean.CREATOR);
            clients_workout = in.createTypedArrayList(TodaysWorkoutBean.CREATOR);
            is_assign=in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeString(name);
            dest.writeString(email);
            dest.writeString(about);
            dest.writeString(photo);
            dest.writeString(trainer_photo);
            dest.writeString(allow_to_change_date);
            dest.writeInt(total_meals);
            dest.writeTypedList(todays_diet);
            dest.writeTypedList(todays_workout);
            dest.writeTypedList(clients_workout);
            dest.writeInt(is_assign);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getTrainer_photo() {
            return trainer_photo;
        }

        public String getAllow_to_change_date() {
            return allow_to_change_date;
        }

        public void setAllow_to_change_date(String allow_to_change_date) {
            this.allow_to_change_date = allow_to_change_date;
        }

        public void setTrainer_photo(String trainer_photo) {
            this.trainer_photo = trainer_photo;
        }

        public int getTotal_meals() {
            return total_meals;
        }

        public void setTotal_meals(int total_meals) {
            this.total_meals = total_meals;
        }

        public ArrayList<TodaysDietBean> getTodays_diet() {
            return todays_diet;
        }

        public void setTodays_diet(ArrayList<TodaysDietBean> todays_diet) {
            this.todays_diet = todays_diet;
        }

        public ArrayList<TodaysWorkoutBean> getTodays_workout() {
            return todays_workout;
        }

        public void setTodays_workout(ArrayList<TodaysWorkoutBean> todays_workout) {
            this.todays_workout = todays_workout;
        }

        public ArrayList<TodaysWorkoutBean> getClients_workout() {
            return clients_workout;
        }

        public void setClients_workout(ArrayList<TodaysWorkoutBean> clients_workout) {
            this.clients_workout = clients_workout;
        }
        public int getIs_assign() {
            return is_assign;
        }

        public void setIs_assign(int is_assign) {
            this.is_assign = is_assign;
        }

        public static class TodaysDietBean implements Parcelable {
            /**
             * id : 668
             * diet_name : Breakfast
             * time : 11:45
             * food : [{"id":10374,"name":"Cheeseburger with 1/4 Lb Meat, Grilled Onions on Rye Bun","serving_size":"1"},{"id":11535,"name":"Vegetable Samosas","serving_size":"2"}]
             */

            private int id;
            private String diet_name;
            private String time;
            private ArrayList<FoodBean> food;

            protected TodaysDietBean(Parcel in) {
                id = in.readInt();
                diet_name = in.readString();
                time = in.readString();
                food = in.createTypedArrayList(FoodBean.CREATOR);
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(id);
                dest.writeString(diet_name);
                dest.writeString(time);
                dest.writeTypedList(food);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<TodaysDietBean> CREATOR = new Creator<TodaysDietBean>() {
                @Override
                public TodaysDietBean createFromParcel(Parcel in) {
                    return new TodaysDietBean(in);
                }

                @Override
                public TodaysDietBean[] newArray(int size) {
                    return new TodaysDietBean[size];
                }
            };

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getDiet_name() {
                return diet_name;
            }

            public void setDiet_name(String diet_name) {
                this.diet_name = diet_name;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public ArrayList<FoodBean> getFood() {
                return food;
            }

            public void setFood(ArrayList<FoodBean> food) {
                this.food = food;
            }

            public static class FoodBean implements Parcelable {
                /**
                 * id : 10374
                 * name : Cheeseburger with 1/4 Lb Meat, Grilled Onions on Rye Bun
                 * serving_size : 1
                 */

                private int id;
                private String name;
                private String serving_size;
                private int is_assigned;


                protected FoodBean(Parcel in) {
                    id = in.readInt();
                    name = in.readString();
                    serving_size = in.readString();
                    is_assigned = in.readInt();
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(id);
                    dest.writeString(name);
                    dest.writeString(serving_size);
                    dest.writeInt(is_assigned);
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                public static final Creator<FoodBean> CREATOR = new Creator<FoodBean>() {
                    @Override
                    public FoodBean createFromParcel(Parcel in) {
                        return new FoodBean(in);
                    }

                    @Override
                    public FoodBean[] newArray(int size) {
                        return new FoodBean[size];
                    }
                };

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getServing_size() {
                    return serving_size;
                }

                public void setServing_size(String serving_size) {
                    this.serving_size = serving_size;
                }

                public int getIs_assigned() {
                    return is_assigned;
                }

                public void setIs_assigned(int is_assigned) {
                    this.is_assigned = is_assigned;
                }
            }
        }

        public static class TodaysWorkoutBean implements Parcelable {
            /**
             * id : 149
             * workout_name : temp
             * sets_count : 0
             * exercises_count : 1
             */

            private int id;
            private String workout_name;
            private int sets_count;
            private int exercises_count;
            private int training_program_id;
            private boolean is_am_workout;
            private int is_finished;


            protected TodaysWorkoutBean(Parcel in) {
                id = in.readInt();
                workout_name = in.readString();
                sets_count = in.readInt();
                exercises_count = in.readInt();
                training_program_id = in.readInt();
                is_am_workout = in.readByte() != 0;
                is_finished = in.readInt();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(id);
                dest.writeString(workout_name);
                dest.writeInt(sets_count);
                dest.writeInt(exercises_count);
                dest.writeInt(training_program_id);
                dest.writeByte((byte) (is_am_workout ? 1 : 0));
                dest.writeInt(is_finished);
            }

            public static final Creator<TodaysWorkoutBean> CREATOR = new Creator<TodaysWorkoutBean>() {
                @Override
                public TodaysWorkoutBean createFromParcel(Parcel in) {
                    return new TodaysWorkoutBean(in);
                }

                @Override
                public TodaysWorkoutBean[] newArray(int size) {
                    return new TodaysWorkoutBean[size];
                }
            };

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getWorkout_name() {
                return workout_name;
            }

            public void setWorkout_name(String workout_name) {
                this.workout_name = workout_name;
            }

            public int getSets_count() {
                return sets_count;
            }

            public void setSets_count(int sets_count) {
                this.sets_count = sets_count;
            }

            public int getExercises_count() {
                return exercises_count;
            }

            public void setExercises_count(int exercises_count) {
                this.exercises_count = exercises_count;
            }

            public int getTraining_program_id() {
                return training_program_id;
            }

            public void setTraining_program_id(int training_program_id) {
                this.training_program_id = training_program_id;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public boolean isIs_am_workout() {
                return is_am_workout;
            }

            public void setIs_am_workout(boolean is_am_workout) {
                this.is_am_workout = is_am_workout;
            }

            public int isIs_finished() {
                return is_finished;
            }

            public void setIs_finished(int is_finished) {
                this.is_finished = is_finished;
            }
        }
    }
}
