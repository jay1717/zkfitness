package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainingProgramClientList {

    /**
     * status : 200
     * message : success
     * data : [{"id":91,"start_date":"2018-11-02","allowed_to_change_date":"1","training_program_id":"136","weeks":1,"title":"Nimit\u2019s pro"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 91
         * start_date : 2018-11-02
         * allowed_to_change_date : 1
         * training_program_id : 136
         * weeks : 1
         * title : Nimit’s pro
         */

        private int id;
        private String start_date;
        private String allowed_to_change_date;
        private String training_program_id;
        private int weeks;
        private String title;
        private String trainer;
        private int is_active;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getAllowed_to_change_date() {
            return allowed_to_change_date;
        }

        public void setAllowed_to_change_date(String allowed_to_change_date) {
            this.allowed_to_change_date = allowed_to_change_date;
        }

        public String getTraining_program_id() {
            return training_program_id;
        }

        public void setTraining_program_id(String training_program_id) {
            this.training_program_id = training_program_id;
        }

        public int getWeeks() {
            return weeks;
        }

        public void setWeeks(int weeks) {
            this.weeks = weeks;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public String getTrainer() {
            return trainer;
        }

        public void setTrainer(String trainer) {
            this.trainer = trainer;
        }

    }
}
