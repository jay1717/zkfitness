package com.fitzoh.app.model;

import java.io.Serializable;

public class VerificationOTPModel implements Serializable {


    /**
     * status : 200
     * message : Login Successfully!
     * data : {"id":76,"role_id":"2","name":"urvi","email":"urvi.joshi019@gmail.com","mobile_number":"",
     "country_code":"+1",
     "gender":"m",,"birth_date":"2018-09-11","photo":"https://php6.shaligraminfotech.com/public/images/user_images","user_access_token":"E901c79f049e05af5da7d1be7","access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgwM2JjZTYwZjFhYTdiM2NmZTI4ZmU5ODZlOWM3MWE3OGE4YmY0YjQ2YmViYzY2YmEzYzQ0NGE3NjMxY2UyMjY3NDM5YmI2OWZhYmZkMGY5In0.eyJhdWQiOiIxNSIsImp0aSI6IjgwM2JjZTYwZjFhYTdiM2NmZTI4ZmU5ODZlOWM3MWE3OGE4YmY0YjQ2YmViYzY2YmEzYzQ0NGE3NjMxY2UyMjY3NDM5YmI2OWZhYmZkMGY5IiwiaWF0IjoxNTM5NzY2NjY2LCJuYmYiOjE1Mzk3NjY2NjYsImV4cCI6MTU3MTMwMjY2Niwic3ViIjoiNzYiLCJzY29wZXMiOltdfQ.rZG1wFMT0zBX_J8boRZnjaMsMqiIorFHPgENH-GMCrC9WEiUYrfZgiyiBEgQTEO9QV5NnfM9_VyYnYWvpE1SnQZnGaVQYoIoxjDB6hFpaEZueZFptSr58oL-C6ICcNstCtYER9aSvWyPCcZ1Mh4ETEvcleEWNZH_T8MH0Y_PL4ZAh7uEtZMLv_dPigpEIB-mC4npKQ5CRxJ8dcZsLoBsNg3g6AsEtAYlGbe1l3Mo7kiwLBAzwYG3qJhfuaaqXLWXEHEFR3wxEmuOtWQJvTLGYQCH0NXhrkqM-USaE8pLtHPLLviIc197ysT5Wbqn25_loNdO6AQ4E69W2ozSbO2CrAZVz0odpG-siMWF__-i4SIngD9fkVkBz-c-mpB5lUmdNSNYxCt2J_a0H0VNiLaEP2VqvBtBRUss_r6E_h2Www5-L40D4oGlRPYIyziUpuH-ReTkwRFsqLVNFNc9ouu3-n61TAQZ-7pF7NURjNRYcG61vAFSjTMh6t8l8MFRmxOXWBAGuUVi7gaNpPvrGT4pPPHhb3DAYuapdHrA3A-_4gs0rJ0Z6MlficeSsJPohaXXfrmVj1enu0KNHZMKYysMaHv-FT3mhWpEPG3W76-47_zDGl4N1P8clvF7OXAVaL2APTb3n4TFPJy7LqTM8ceZXd1esBc3JqPHxBdjmluRytA","token_type":"Bearer","subscription_id":1,"is_subscription":1,"is_profile_completed":11,"is_medical_form_completed":0,"expires_at":"2019-10-17 08:57:46"}
     */

    private int status;
    private String message;
    private UserDataModel data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataModel getData() {
        return data;
    }

    public void setData(UserDataModel data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 76
         * role_id : 2
         * name : urvi
         * email : urvi.joshi019@gmail.com
         * birth_date : 2018-09-11
         * photo : https://php6.shaligraminfotech.com/public/images/user_images
         * user_access_token : E901c79f049e05af5da7d1be7
         * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgwM2JjZTYwZjFhYTdiM2NmZTI4ZmU5ODZlOWM3MWE3OGE4YmY0YjQ2YmViYzY2YmEzYzQ0NGE3NjMxY2UyMjY3NDM5YmI2OWZhYmZkMGY5In0.eyJhdWQiOiIxNSIsImp0aSI6IjgwM2JjZTYwZjFhYTdiM2NmZTI4ZmU5ODZlOWM3MWE3OGE4YmY0YjQ2YmViYzY2YmEzYzQ0NGE3NjMxY2UyMjY3NDM5YmI2OWZhYmZkMGY5IiwiaWF0IjoxNTM5NzY2NjY2LCJuYmYiOjE1Mzk3NjY2NjYsImV4cCI6MTU3MTMwMjY2Niwic3ViIjoiNzYiLCJzY29wZXMiOltdfQ.rZG1wFMT0zBX_J8boRZnjaMsMqiIorFHPgENH-GMCrC9WEiUYrfZgiyiBEgQTEO9QV5NnfM9_VyYnYWvpE1SnQZnGaVQYoIoxjDB6hFpaEZueZFptSr58oL-C6ICcNstCtYER9aSvWyPCcZ1Mh4ETEvcleEWNZH_T8MH0Y_PL4ZAh7uEtZMLv_dPigpEIB-mC4npKQ5CRxJ8dcZsLoBsNg3g6AsEtAYlGbe1l3Mo7kiwLBAzwYG3qJhfuaaqXLWXEHEFR3wxEmuOtWQJvTLGYQCH0NXhrkqM-USaE8pLtHPLLviIc197ysT5Wbqn25_loNdO6AQ4E69W2ozSbO2CrAZVz0odpG-siMWF__-i4SIngD9fkVkBz-c-mpB5lUmdNSNYxCt2J_a0H0VNiLaEP2VqvBtBRUss_r6E_h2Www5-L40D4oGlRPYIyziUpuH-ReTkwRFsqLVNFNc9ouu3-n61TAQZ-7pF7NURjNRYcG61vAFSjTMh6t8l8MFRmxOXWBAGuUVi7gaNpPvrGT4pPPHhb3DAYuapdHrA3A-_4gs0rJ0Z6MlficeSsJPohaXXfrmVj1enu0KNHZMKYysMaHv-FT3mhWpEPG3W76-47_zDGl4N1P8clvF7OXAVaL2APTb3n4TFPJy7LqTM8ceZXd1esBc3JqPHxBdjmluRytA
         * token_type : Bearer
         * subscription_id : 1
         * is_subscription : 1
         * is_profile_completed : 11
         * is_medical_form_completed : 0
         * expires_at : 2019-10-17 08:57:46
         */

        private int id;
        private String role_id;
        private String name;
        private String email;
        private String birth_date;
        private String photo;
        private String user_access_token;
        private String access_token;
        private String token_type;
        private int subscription_id;
        private int is_subscription;
        private int is_profile_completed;
        private int is_medical_form_completed;
        private String expires_at;
        private boolean gym_trainer;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBirth_date() {
            return birth_date;
        }

        public void setBirth_date(String birth_date) {
            this.birth_date = birth_date;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getUser_access_token() {
            return user_access_token;
        }

        public void setUser_access_token(String user_access_token) {
            this.user_access_token = user_access_token;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public int getSubscription_id() {
            return subscription_id;
        }

        public void setSubscription_id(int subscription_id) {
            this.subscription_id = subscription_id;
        }

        public int getIs_subscription() {
            return is_subscription;
        }

        public void setIs_subscription(int is_subscription) {
            this.is_subscription = is_subscription;
        }

        public int getIs_profile_completed() {
            return is_profile_completed;
        }

        public void setIs_profile_completed(int is_profile_completed) {
            this.is_profile_completed = is_profile_completed;
        }

        public int getIs_medical_form_completed() {
            return is_medical_form_completed;
        }

        public void setIs_medical_form_completed(int is_medical_form_completed) {
            this.is_medical_form_completed = is_medical_form_completed;
        }

        public String getExpires_at() {
            return expires_at;
        }

        public void setExpires_at(String expires_at) {
            this.expires_at = expires_at;
        }

        public boolean isGym_trainer() {
            return gym_trainer;
        }

        public void setGym_trainer(boolean gym_trainer) {
            this.gym_trainer = gym_trainer;
        }
    }
}
