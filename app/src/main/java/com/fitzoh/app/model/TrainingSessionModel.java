package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainingSessionModel {


    /**
     * status : 200
     * message : Record find successfully.
     * data : [{"id":31,"name":"Client","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","startdatetime":"2018-09-19 00:00:00","enddate":"2018-09-15 00:00:00","diet_plan_name":"Diet1","workout_name":"Biceps Workout"},{"id":27,"name":"Client","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","startdatetime":"2018-09-19 00:00:00","enddate":"2018-09-15 00:00:00","diet_plan_name":"Diet1","workout_name":"Biceps Workout"},{"id":31,"name":"Client","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","startdatetime":"2018-09-19 00:00:00","enddate":"2018-09-15 00:00:00","diet_plan_name":"Diet1","workout_name":"Deltoids Workout"},{"id":27,"name":"Client","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","startdatetime":"2018-09-19 00:00:00","enddate":"2018-09-15 00:00:00","diet_plan_name":"Diet1","workout_name":"Deltoids Workout"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 31
         * name : Client
         * photo : http://106.201.238.169:4091/images/user_images/default.jpeg
         * startdatetime : 2018-09-19 00:00:00
         * enddate : 2018-09-15 00:00:00
         * diet_plan_name : Diet1
         * workout_name : Biceps Workout
         */

        private int id;
        private String name;
        private String photo;
        private String email;
        private String start_time;
        private String end_time;
        private String diet_plan_name;
        private String workout_name;
        private int is_active;
        private int trainer_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public String getDiet_plan_name() {
            return diet_plan_name;
        }

        public void setDiet_plan_name(String diet_plan_name) {
            this.diet_plan_name = diet_plan_name;
        }

        public String getWorkout_name() {
            return workout_name;
        }

        public void setWorkout_name(String workout_name) {
            this.workout_name = workout_name;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public int getTrainer_id() {
            return trainer_id;
        }

        public void setTrainer_id(int trainer_id) {
            this.trainer_id = trainer_id;
        }
    }
}

