package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ClientInquiryHistoryModel implements Serializable{


    /**
     * status : 200
     * message : success
     * data : [{"product_id":2,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products/11/whey.jpg"},{"product_id":1,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products/11/whey.jpg"},{"product_id":2,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products/11/whey.jpg"},{"product_id":1,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products/11/whey.jpg"},{"product_id":20,"product_name":"jinank product","price":"1000.00","qty":"5","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":16,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":17,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":9,"product_name":"custom product","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":10,"product_name":"custom product","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":11,"product_name":"custom product","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":12,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":13,"product_name":"Whey Protein","price":"1000.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":14,"product_name":"test by nimit","price":"1000.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":15,"product_name":"test 2nd","price":"10000.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":8,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":7,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":6,"product_name":"custom product","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":5,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":4,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":5,"product_name":"Whey Protein","price":"10.00","qty":"2","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":6,"product_name":"custom product","price":"10.00","qty":"5","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":7,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":8,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":9,"product_name":"custom product","price":"10.00","qty":"3","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":10,"product_name":"custom product","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":11,"product_name":"custom product","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":12,"product_name":"Whey Protein","price":"10.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":13,"product_name":"Whey Protein","price":"1000.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":14,"product_name":"test by nimit","price":"1000.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":15,"product_name":"test 2nd","price":"10000.00","qty":"1","product_image":"https://php6.shaligraminfotech.com/public/images/products"},{"product_id":4,"product_name":"Whey Protein","price":"10.00","qty":"3","product_image":"https://php6.shaligraminfotech.com/public/images/products"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * product_id : 2
         * product_name : Whey Protein
         * price : 10.00
         * qty : 1
         * product_image : https://php6.shaligraminfotech.com/public/images/products/11/whey.jpg
         */

        private int product_id;
        private String product_name;
        private String price;
        private String qty;
        private String product_image;

        public int getProduct_id() {
            return product_id;
        }

        public void setProduct_id(int product_id) {
            this.product_id = product_id;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getProduct_image() {
            return product_image;
        }

        public void setProduct_image(String product_image) {
            this.product_image = product_image;
        }
    }
}
