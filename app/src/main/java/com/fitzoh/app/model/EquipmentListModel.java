package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class EquipmentListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":6,"equipment_name":"Bands"},{"id":26,"equipment_name":"Bar"},{"id":3,"equipment_name":"Barbell"},{"id":24,"equipment_name":"Bench"},{"id":4,"equipment_name":"Body Only"},{"id":20,"equipment_name":"BOSU"},{"id":16,"equipment_name":"Box"},{"id":10,"equipment_name":"Cable"},{"id":1,"equipment_name":"Dumbbell"},{"id":7,"equipment_name":"E-Z Curl Bar"},{"id":11,"equipment_name":"Exercise Ball"},{"id":13,"equipment_name":"Foam Roll"},{"id":2,"equipment_name":"Kettlebells"},{"id":8,"equipment_name":"Machine"},{"id":12,"equipment_name":"Medicine Ball"},{"id":9,"equipment_name":"None"},{"id":5,"equipment_name":"Other"},{"id":23,"equipment_name":"Pole"},{"id":17,"equipment_name":"Ropes"},{"id":22,"equipment_name":"Sandbag"},{"id":18,"equipment_name":"Spin Bike"},{"id":19,"equipment_name":"Step"},{"id":15,"equipment_name":"TRX"},{"id":21,"equipment_name":"Tyre"},{"id":25,"equipment_name":"Wall"},{"id":14,"equipment_name":"Weight Plate"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 6
         * equipment_name : Bands
         */

        private int id;
        private String equipment_name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEquipment_name() {
            return equipment_name;
        }

        public void setEquipment_name(String equipment_name) {
            this.equipment_name = equipment_name;
        }
    }
}
