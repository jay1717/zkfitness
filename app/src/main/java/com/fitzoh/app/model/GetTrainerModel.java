package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class GetTrainerModel implements Serializable {



    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 76
         * name : urvi
         * speciality : Strength Training,Muscle Building
         * is_gym : 0
         * facilities :
         * rating : 5
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg
         * is_featured : 0
         * is_approved : 1
         * location : Ahmedabad,Gujarat,India
         */

        private int id;
        private String name;
        private String speciality;
        private int is_gym;
        private String facilities;
        private int rating;
        private String photo;
        private int is_featured;
        private int is_approved;
        private String location;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpeciality() {
            return speciality;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        public int getIs_gym() {
            return is_gym;
        }

        public void setIs_gym(int is_gym) {
            this.is_gym = is_gym;
        }

        public String getFacilities() {
            return facilities;
        }

        public void setFacilities(String facilities) {
            this.facilities = facilities;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public int getIs_featured() {
            return is_featured;
        }

        public void setIs_featured(int is_featured) {
            this.is_featured = is_featured;
        }

        public int getIs_approved() {
            return is_approved;
        }

        public void setIs_approved(int is_approved) {
            this.is_approved = is_approved;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }
    }
}
