package com.fitzoh.app.model;

import java.util.ArrayList;
import java.util.List;

public class LogWorkoutDataModel {


    /**
     * status : 200
     * message : success
     * data : [[{"id":126,"exercise_name":"jogging","image":"","no_of_sets":"1","sets":[[{"setno":"1","id":649,"parameter_id":"1","parameter_name":"Weight","value":"0","weight_unit":"1"},{"setno":"1","id":650,"parameter_id":"4","parameter_name":"As Many Reps As Possible","value":"0"},{"setno":"1","id":651,"parameter_id":"10","parameter_name":"Rest Period","value":""}]]}],[{"id":127,"exercise_name":"exetemp","image":"","no_of_sets":"2","sets":[[{"setno":"1","id":655,"parameter_id":"1","parameter_name":"Weight","value":"1","weight_unit":"1"},{"setno":"1","id":656,"parameter_id":"2","parameter_name":"Reps","value":"2"},{"setno":"1","id":657,"parameter_id":"10","parameter_name":"Rest Period","value":"3"}],[{"setno":"1","id":655,"parameter_id":"1","parameter_name":"Weight","value":"1","weight_unit":"1"},{"setno":"1","id":656,"parameter_id":"2","parameter_name":"Reps","value":"2"},{"setno":"1","id":657,"parameter_id":"10","parameter_name":"Rest Period","value":"3"}],[{"setno":"2","id":658,"parameter_id":"1","parameter_name":"Weight","value":"0","weight_unit":"1"},{"setno":"2","id":659,"parameter_id":"2","parameter_name":"Reps","value":"0"},{"setno":"2","id":660,"parameter_id":"10","parameter_name":"Rest Period","value":""}]]}],[{"id":128,"exercise_name":"new","image":"","no_of_sets":"0"}]]
     */

    private int status;
    private String message;
    private List<List<DataBean>> data = new ArrayList<>();

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 126
         * exercise_name : jogging
         * image :
         * no_of_sets : 1
         * sets : [[{"setno":"1","id":649,"parameter_id":"1","parameter_name":"Weight","value":"0","weight_unit":"1"},{"setno":"1","id":650,"parameter_id":"4","parameter_name":"As Many Reps As Possible","value":"0"},{"setno":"1","id":651,"parameter_id":"10","parameter_name":"Rest Period","value":""}]]
         */

        private int id;
        private String exercise_name;
        private String image;
        private String no_of_sets;
        private String workout_id;
        private int rating;
        private String timeStamp = "";
        private List<List<SetsBean>> sets = new ArrayList<>();

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExercise_name() {
            return exercise_name;
        }

        public void setExercise_name(String exercise_name) {
            this.exercise_name = exercise_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(String no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public String getWorkout_id() {
            return workout_id;
        }

        public void setWorkout_id(String workout_id) {
            this.workout_id = workout_id;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getTimeStamp() {
            return timeStamp;
        }

        public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
        }

        public List<List<SetsBean>> getSets() {
            return sets;
        }

        public void setSets(List<List<SetsBean>> sets) {
            this.sets = sets;
        }

        public static class SetsBean {
            /**
             * setno : 1
             * id : 649
             * parameter_id : 1
             * value :
             * parameter_name : Weight
             * set_value : 0
             * weight_unit : 1
             * history_value :
             */

            private String setno;
            private int id;
            private String parameter_id;
            private String value = "";
            private String parameter_name;
            private String set_value;
            private String weight_unit;
            private String history_value;
            private String user_value = "";

            public String getSetno() {
                return setno;
            }

            public void setSetno(String setno) {
                this.setno = setno;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getParameter_id() {
                return parameter_id;
            }

            public void setParameter_id(String parameter_id) {
                this.parameter_id = parameter_id;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getParameter_name() {
                return parameter_name;
            }

            public void setParameter_name(String parameter_name) {
                this.parameter_name = parameter_name;
            }

            public String getSet_value() {
                return set_value;
            }

            public void setSet_value(String set_value) {
                this.set_value = set_value;
            }

            public String getWeight_unit() {
                return weight_unit;
            }

            public void setWeight_unit(String weight_unit) {
                this.weight_unit = weight_unit;
            }

            public String getHistory_value() {
                return history_value;
            }

            public void setHistory_value(String history_value) {
                this.history_value = history_value;
            }

            public String getUser_value() {
                return user_value;
            }

            public void setUser_value(String user_value) {
                this.user_value = user_value;
            }
        }
    }
}
