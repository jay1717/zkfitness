package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class SubscriptionTypeListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"name":"Free 30 Day Trail","limitation":"unlimited","price":"0.00"},{"id":2,"name":"Trainer Version","limitation":"unlimited","price":"1500.00"},{"id":3,"name":"Gym or Health Club Version","limitation":"unlimited","price":"800.00"},{"id":4,"name":"Custom APP-IN Your Own Name","limitation":"unlimited","price":"12000.00"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * name : Free 30 Day Trail
         * limitation : unlimited
         * price : 0.00
         */

        private int id;
        private String name;
        private String limitation;
        private Double price;
        boolean isSelected;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLimitation() {
            return limitation;
        }

        public void setLimitation(String limitation) {
            this.limitation = limitation;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }
    }
}
