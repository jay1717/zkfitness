package com.fitzoh.app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GymTrainerScheduleListModel {

    /**
     * status : 200
     * message : success
     * data : [{"id":7,"user_id":46,"day":"Tuesday","start_time":"02:00 am","end_time":"03:01 am","name":"gumtrcl","email":"gymtrcl@gmail.com","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 7
         * user_id : 46
         * day : Tuesday
         * start_time : 02:00 am
         * end_time : 03:01 am
         * name : gumtrcl
         * email : gymtrcl@gmail.com
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg
         */

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("day")
        @Expose
        private String day;
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("end_time")
        @Expose
        private String endTime;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("photo")
        @Expose
        private String photo;
        @SerializedName("goal")
        @Expose
        private Object goal;
        @SerializedName("trainer_id")
        @Expose
        private Integer trainerId;
        @SerializedName("is_profile_completed")
        @Expose
        private Integer isProfileCompleted;
        @SerializedName("is_assigned")
        @Expose
        private Integer isAssigned;
        @SerializedName("start_date")
        @Expose
        private String startDate;
        @SerializedName("allowed_to_change_date")
        @Expose
        private Integer allowedToChangeDate;
        @SerializedName("is_active")
        @Expose
        private Integer isActive;
        @SerializedName("client_group")
        @Expose
        private String clientGroup;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public Object getGoal() {
            return goal;
        }

        public void setGoal(Object goal) {
            this.goal = goal;
        }

        public Integer getTrainerId() {
            return trainerId;
        }

        public void setTrainerId(Integer trainerId) {
            this.trainerId = trainerId;
        }

        public Integer getIsProfileCompleted() {
            return isProfileCompleted;
        }

        public void setIsProfileCompleted(Integer isProfileCompleted) {
            this.isProfileCompleted = isProfileCompleted;
        }

        public Integer getIsAssigned() {
            return isAssigned;
        }

        public void setIsAssigned(Integer isAssigned) {
            this.isAssigned = isAssigned;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public Integer getAllowedToChangeDate() {
            return allowedToChangeDate;
        }

        public void setAllowedToChangeDate(Integer allowedToChangeDate) {
            this.allowedToChangeDate = allowedToChangeDate;
        }

        public Integer getIsActive() {
            return isActive;
        }

        public void setIsActive(Integer isActive) {
            this.isActive = isActive;
        }

        public String getClientGroup() {
            return clientGroup;
        }

        public void setClientGroup(String clientGroup) {
            this.clientGroup = clientGroup;
        }
    }
}
