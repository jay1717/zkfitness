package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

public class WorkOutListModel extends Observable implements Serializable {

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        private int id;
        private String name;
        private int exercise_count;
        private String no_of_sets;
        private String assign_by;
        private int is_client_created;
        private List<AssignedToBean> assigned_to;
        private int is_assigned;
        private int is_active;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getExercise_count() {
            return exercise_count;
        }

        public void setExercise_count(int exercise_count) {
            this.exercise_count = exercise_count;
        }

        public String getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(String no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public List<AssignedToBean> getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(List<AssignedToBean> assigned_to) {
            this.assigned_to = assigned_to;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }

        public String getAssign_by() {
            return assign_by;
        }

        public void setAssign_by(String assign_by) {
            this.assign_by = assign_by;
        }

        public int getIs_client_created() {
            return is_client_created;
        }

        public void setIs_client_created(int is_client_created) {
            this.is_client_created = is_client_created;
        }
        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

    }
}
