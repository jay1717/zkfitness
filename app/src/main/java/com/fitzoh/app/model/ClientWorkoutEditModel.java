package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ClientWorkoutEditModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [[{"exercise_set_id":239,"id":1,"exercise_name":"Barbell Shrug Behind The Neck","image":"https://www.fitzoh.com/images/exercises/default1.jpg","no_of_sets":"4","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4858,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"1","id":4859,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"1","id":4860,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4861,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"2","id":4862,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"2","id":4863,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4864,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"3","id":4865,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4866,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4867,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"4","id":4868,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4869,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]},{"exercise_set_id":239,"id":8,"exercise_name":"Leverage Shrug","image":"https://www.fitzoh.com/images/exercises/default8.jpg","no_of_sets":"3","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4891,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"1","id":4892,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"8","history_value":""},{"setno":"1","id":4893,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"2","id":4894,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"2","id":4895,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"8","history_value":""},{"setno":"2","id":4896,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"3","id":4897,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"3","id":4898,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"8","history_value":""},{"setno":"3","id":4899,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"4","id":4867,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"4","id":4868,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4869,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]},{"exercise_set_id":239,"id":5,"exercise_name":"Smith Machine Behind The Back Shrugs","image":"https://www.fitzoh.com/images/exercises/default5.jpg","no_of_sets":"3","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4900,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"1","id":4901,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"1","id":4902,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"2","id":4903,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"2","id":4904,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"2","id":4905,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"3","id":4906,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"3","id":4907,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"3","id":4908,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"4","id":4867,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"4","id":4868,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4869,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]}],[{"exercise_set_id":240,"id":4,"exercise_name":"Dumbbell shrugs","image":"https://www.fitzoh.com/images/exercises/default4.jpg","no_of_sets":"3","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4882,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"1","id":4883,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"1","id":4884,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4885,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"2","id":4886,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"2","id":4887,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4888,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"3","id":4889,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4890,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4867,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"4","id":4868,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4869,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]},{"exercise_set_id":240,"id":7,"exercise_name":"Smith Machine Upright Row","image":"https://www.fitzoh.com/images/exercises/default7.jpg","no_of_sets":0,"workout_id":"176","rating":0,"sets":[]},{"exercise_set_id":240,"id":9,"exercise_name":"Standing Dumbbell Upright Row","image":"https://www.fitzoh.com/images/exercises/default9.jpg","no_of_sets":"4","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4915,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"60","weight_unit":1},{"setno":"1","id":4916,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"1","id":4917,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4918,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"60","weight_unit":1},{"setno":"2","id":4919,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"2","id":4920,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4921,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"60","weight_unit":1},{"setno":"3","id":4922,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4923,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4924,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"60","weight_unit":1},{"setno":"4","id":4925,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4926,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]}],[{"exercise_set_id":241,"id":6,"exercise_name":"Smith Machine Shrug","image":"https://www.fitzoh.com/images/exercises/default6.jpg","no_of_sets":"2","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4909,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"1","id":4910,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"6","history_value":""},{"setno":"1","id":4911,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4912,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"2","id":4913,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"6","history_value":""},{"setno":"2","id":4914,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4921,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"60","weight_unit":1},{"setno":"3","id":4922,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4923,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4924,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"60","weight_unit":1},{"setno":"4","id":4925,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4926,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]},{"exercise_set_id":241,"id":3,"exercise_name":"Cable Shrugs","image":"https://www.fitzoh.com/images/exercises/default3.jpg","no_of_sets":"4","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4870,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"1","id":4871,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"1","id":4872,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4873,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"2","id":4874,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"2","id":4875,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4876,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"3","id":4877,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4878,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4879,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"4","id":4880,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4881,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]},{"exercise_set_id":241,"id":40,"exercise_name":"client 1 exercise","image":"https://www.fitzoh.com/images/exercises/1548399772_doc_data.jpg","no_of_sets":"3","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4927,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"1","id":4928,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"1","id":4929,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4930,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"2","id":4931,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"2","id":4932,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4933,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"3","id":4934,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4935,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4879,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"70","weight_unit":1},{"setno":"4","id":4880,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4881,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]}],[{"exercise_set_id":242,"id":10,"exercise_name":"Upright Cable Row","image":"https://www.fitzoh.com/images/exercises/default10.jpg","no_of_sets":0,"workout_id":"176","rating":0,"sets":[]},{"exercise_set_id":242,"id":2,"exercise_name":"Barbell Shrugs","image":"https://www.fitzoh.com/images/exercises/default2.jpg","no_of_sets":"4","workout_id":"176","rating":0,"sets":[[{"setno":"1","id":4846,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"90","weight_unit":1},{"setno":"1","id":4847,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"1","id":4848,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"2","id":4849,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"90","weight_unit":1},{"setno":"2","id":4850,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"2","id":4851,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"3","id":4852,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"90","weight_unit":1},{"setno":"3","id":4853,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"3","id":4854,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}],[{"setno":"4","id":4855,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"90","weight_unit":1},{"setno":"4","id":4856,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"10","history_value":""},{"setno":"4","id":4857,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"5","history_value":""}]]}]]
     */

    private int status;
    private String message;
    private List<List<DataBean>> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * exercise_set_id : 239
         * id : 1
         * exercise_name : Barbell Shrug Behind The Neck
         * image : https://www.fitzoh.com/images/exercises/default1.jpg
         * no_of_sets : 4
         * workout_id : 176
         * rating : 0
         * sets : [[{"setno":"1","id":4858,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"1","id":4859,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"1","id":4860,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"2","id":4861,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"2","id":4862,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"2","id":4863,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"3","id":4864,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"3","id":4865,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"3","id":4866,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}],[{"setno":"4","id":4867,"parameter_id":1,"value":"","parameter_name":"Weight","set_value":"80","weight_unit":1},{"setno":"4","id":4868,"parameter_id":2,"value":"","parameter_name":"Reps","set_value":"5","history_value":""},{"setno":"4","id":4869,"parameter_id":10,"value":"","parameter_name":"Rest Period","set_value":"10","history_value":""}]]
         */

        private int exercise_set_id;
        private int id;
        private String exercise_name;
        private String image;
        private String no_of_sets;
        private String workout_id;
        private int rating;
        private List<List<SetsBean>> sets;

        public int getExercise_set_id() {
            return exercise_set_id;
        }

        public void setExercise_set_id(int exercise_set_id) {
            this.exercise_set_id = exercise_set_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExercise_name() {
            return exercise_name;
        }

        public void setExercise_name(String exercise_name) {
            this.exercise_name = exercise_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(String no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public String getWorkout_id() {
            return workout_id;
        }

        public void setWorkout_id(String workout_id) {
            this.workout_id = workout_id;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public List<List<SetsBean>> getSets() {
            return sets;
        }

        public void setSets(List<List<SetsBean>> sets) {
            this.sets = sets;
        }

        public static class SetsBean implements Serializable {
            /**
             * setno : 1
             * id : 4858
             * parameter_id : 1
             * value :
             * parameter_name : Weight
             * set_value : 80
             * weight_unit : 1
             * history_value :
             */

            private String setno;
            private int id;
            private int parameter_id;
            private String value;
            private String parameter_name;
            private String set_value;
            private int weight_unit;
            private String history_value;

            public String getSetno() {
                return setno;
            }

            public void setSetno(String setno) {
                this.setno = setno;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParameter_id() {
                return parameter_id;
            }

            public void setParameter_id(int parameter_id) {
                this.parameter_id = parameter_id;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getParameter_name() {
                return parameter_name;
            }

            public void setParameter_name(String parameter_name) {
                this.parameter_name = parameter_name;
            }

            public String getSet_value() {
                return set_value;
            }

            public void setSet_value(String set_value) {
                this.set_value = set_value;
            }

            public int getWeight_unit() {
                return weight_unit;
            }

            public void setWeight_unit(int weight_unit) {
                this.weight_unit = weight_unit;
            }

            public String getHistory_value() {
                return history_value;
            }

            public void setHistory_value(String history_value) {
                this.history_value = history_value;
            }
        }
    }
}
