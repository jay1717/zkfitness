package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainerListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1299,"name":"jinank","contact_number":"9737142921","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","number_of_client":1},{"id":1303,"name":"jinank","contact_number":"9737142924","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","number_of_client":0},{"id":1310,"name":"Nimit","contact_number":"","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","number_of_client":0}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1299
         * name : jinank
         * contact_number : 9737142921
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg
         * number_of_client : 1
         */

        private int id;
        private String name;
        private String contact_number;
        private String email;
        private String photo;
        private int number_of_client;
        private int number_of_schedule;
        private boolean isSelected = false;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public int getNumber_of_client() {
            return number_of_client;
        }

        public void setNumber_of_client(int number_of_client) {
            this.number_of_client = number_of_client;
        }

        public int getNumber_of_schedule() {
            return number_of_schedule;
        }

        public void setNumber_of_schedule(int number_of_schedule) {
            this.number_of_schedule = number_of_schedule;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
