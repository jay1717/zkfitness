package com.fitzoh.app.model;

import java.util.List;

public class TrainingProgramDetailClientData {

    /**
     * status : 200
     * message : success
     * data : {"id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111","training_weeks":[{"id":179,"training_plan_id":"91","week_number":"1","weekdays":[{"id":544,"training_week_id":"179","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"255","diet_plan_name":"diet3","workout_pm_id":"324","workout_pm_name":"w","workout_am_id":"327","workout_am_name":"nilesh1-26oct","is_restfull_day":"0","is_checkin":"1"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":285,"training_plan_id":"91","week_number":"2","weekdays":[{"id":549,"training_week_id":"285","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":561,"training_week_id":"285","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":290,"training_plan_id":"91","week_number":"3","weekdays":[{"id":562,"training_week_id":"290","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":563,"training_week_id":"290","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":564,"training_week_id":"290","day_number":"3","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":291,"training_plan_id":"91","week_number":"4","weekdays":[{"id":565,"training_week_id":"291","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":566,"training_week_id":"291","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":567,"training_week_id":"291","day_number":"3","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":568,"training_week_id":"291","day_number":"4","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":292,"training_plan_id":"91","week_number":"5","weekdays":[{"id":569,"training_week_id":"292","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":570,"training_week_id":"292","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":571,"training_week_id":"292","day_number":"3","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"255","diet_plan_name":"diet3","workout_pm_id":"324","workout_pm_name":"w","workout_am_id":"327","workout_am_name":"nilesh1-26oct","is_restfull_day":"0","is_checkin":"1"},{"id":572,"training_week_id":"292","day_number":"4","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":573,"training_week_id":"292","day_number":"5","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"}]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 91
         * title : nilesh1 to aaaaaaaaaaaaaaaa 111111111
         * training_weeks : [{"id":179,"training_plan_id":"91","week_number":"1","weekdays":[{"id":544,"training_week_id":"179","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"255","diet_plan_name":"diet3","workout_pm_id":"324","workout_pm_name":"w","workout_am_id":"327","workout_am_name":"nilesh1-26oct","is_restfull_day":"0","is_checkin":"1"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":285,"training_plan_id":"91","week_number":"2","weekdays":[{"id":549,"training_week_id":"285","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":561,"training_week_id":"285","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":290,"training_plan_id":"91","week_number":"3","weekdays":[{"id":562,"training_week_id":"290","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":563,"training_week_id":"290","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":564,"training_week_id":"290","day_number":"3","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":291,"training_plan_id":"91","week_number":"4","weekdays":[{"id":565,"training_week_id":"291","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":566,"training_week_id":"291","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":567,"training_week_id":"291","day_number":"3","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":568,"training_week_id":"291","day_number":"4","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"},{"id":292,"training_plan_id":"91","week_number":"5","weekdays":[{"id":569,"training_week_id":"292","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":570,"training_week_id":"292","day_number":"2","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":571,"training_week_id":"292","day_number":"3","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"255","diet_plan_name":"diet3","workout_pm_id":"324","workout_pm_name":"w","workout_am_id":"327","workout_am_name":"nilesh1-26oct","is_restfull_day":"0","is_checkin":"1"},{"id":572,"training_week_id":"292","day_number":"4","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"},{"id":573,"training_week_id":"292","day_number":"5","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"0","diet_plan_name":"","workout_pm_id":"0","workout_pm_name":"","workout_am_id":"0","workout_am_name":"","is_restfull_day":"0","is_checkin":"0"}],"training_program_id":91,"title":"nilesh1 to aaaaaaaaaaaaaaaa 111111111"}]
         */

        private int id;
        private String title;
        private List<TrainingWeeksBean> training_weeks;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<TrainingWeeksBean> getTraining_weeks() {
            return training_weeks;
        }

        public void setTraining_weeks(List<TrainingWeeksBean> training_weeks) {
            this.training_weeks = training_weeks;
        }

        public static class TrainingWeeksBean {
            /**
             * id : 179
             * training_plan_id : 91
             * week_number : 1
             * weekdays : [{"id":544,"training_week_id":"179","day_number":"1","day":"Monday","date":"2018-11-02","is_logged":1,"diet_plan_id":"255","diet_plan_name":"diet3","workout_pm_id":"324","workout_pm_name":"w","workout_am_id":"327","workout_am_name":"nilesh1-26oct","is_restfull_day":"0","is_checkin":"1"}]
             * training_program_id : 91
             * title : nilesh1 to aaaaaaaaaaaaaaaa 111111111
             */

            private int id;
            private String training_plan_id;
            private String week_number;
            private int training_program_id;
            private String title;
            private List<WeekdaysBean> weekdays;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTraining_plan_id() {
                return training_plan_id;
            }

            public void setTraining_plan_id(String training_plan_id) {
                this.training_plan_id = training_plan_id;
            }

            public String getWeek_number() {
                return week_number;
            }

            public void setWeek_number(String week_number) {
                this.week_number = week_number;
            }

            public int getTraining_program_id() {
                return training_program_id;
            }

            public void setTraining_program_id(int training_program_id) {
                this.training_program_id = training_program_id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public List<WeekdaysBean> getWeekdays() {
                return weekdays;
            }

            public void setWeekdays(List<WeekdaysBean> weekdays) {
                this.weekdays = weekdays;
            }

            public static class WeekdaysBean {
                /**
                 * id : 544
                 * training_week_id : 179
                 * day_number : 1
                 * day : Monday
                 * date : 2018-11-02
                 * is_logged : 1
                 * diet_plan_id : 255
                 * diet_plan_name : diet3
                 * workout_pm_id : 324
                 * workout_pm_name : w
                 * workout_am_id : 327
                 * workout_am_name : nilesh1-26oct
                 * is_restfull_day : 0
                 * is_checkin : 1
                 */

                private int id;
                private String training_week_id;
                private String day_number;
                private String day;
                private String date;
                private int diet_plan_id;
                private String diet_plan_name;
                private int workout_pm_id;
                private String workout_pm_name;
                private int workout_am_id;
                private String workout_am_name;
                private int is_restfull_day;
                private int is_checkin;

                private int workout_pm_logged;
                private int workout_am_logged;
                private int diet_plan_logged;
                private int checkin_logged;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTraining_week_id() {
                    return training_week_id;
                }

                public void setTraining_week_id(String training_week_id) {
                    this.training_week_id = training_week_id;
                }

                public String getDay_number() {
                    return day_number;
                }

                public void setDay_number(String day_number) {
                    this.day_number = day_number;
                }

                public String getDay() {
                    return day;
                }

                public void setDay(String day) {
                    this.day = day;
                }

                public String getDate() {
                    return date;
                }

                public void setDate(String date) {
                    this.date = date;
                }




                public int getDiet_plan_id() {
                    return diet_plan_id;
                }

                public void setDiet_plan_id(int diet_plan_id) {
                    this.diet_plan_id = diet_plan_id;
                }

                public String getDiet_plan_name() {
                    return diet_plan_name;
                }

                public void setDiet_plan_name(String diet_plan_name) {
                    this.diet_plan_name = diet_plan_name;
                }

                public int getWorkout_pm_id() {
                    return workout_pm_id;
                }

                public void setWorkout_pm_id(int workout_pm_id) {
                    this.workout_pm_id = workout_pm_id;
                }

                public String getWorkout_pm_name() {
                    return workout_pm_name;
                }

                public void setWorkout_pm_name(String workout_pm_name) {
                    this.workout_pm_name = workout_pm_name;
                }

                public int getWorkout_am_id() {
                    return workout_am_id;
                }

                public void setWorkout_am_id(int workout_am_id) {
                    this.workout_am_id = workout_am_id;
                }

                public String getWorkout_am_name() {
                    return workout_am_name;
                }

                public void setWorkout_am_name(String workout_am_name) {
                    this.workout_am_name = workout_am_name;
                }

                public int getIs_restfull_day() {
                    return is_restfull_day;
                }

                public void setIs_restfull_day(int is_restfull_day) {
                    this.is_restfull_day = is_restfull_day;
                }

                public int getIs_checkin() {
                    return is_checkin;
                }

                public void setIs_checkin(int is_checkin) {
                    this.is_checkin = is_checkin;
                }

                public int getWorkout_pm_logged() {
                    return workout_pm_logged;
                }

                public void setWorkout_pm_logged(int workout_pm_logged) {
                    this.workout_pm_logged = workout_pm_logged;
                }

                public int getWorkout_am_logged() {
                    return workout_am_logged;
                }

                public void setWorkout_am_logged(int workout_am_logged) {
                    this.workout_am_logged = workout_am_logged;
                }

                public int getDiet_plan_logged() {
                    return diet_plan_logged;
                }

                public void setDiet_plan_logged(int diet_plan_logged) {
                    this.diet_plan_logged = diet_plan_logged;
                }

                public int getCheckin_logged() {
                    return checkin_logged;
                }

                public void setCheckin_logged(int checkin_logged) {
                    this.checkin_logged = checkin_logged;
                }
            }
        }
    }
}
