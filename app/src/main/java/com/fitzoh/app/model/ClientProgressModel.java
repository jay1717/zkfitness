package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ClientProgressModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"type":"Workout logged","value":"","title":"qa1Copy","time":"2018-11-05 09:58:26"},{"type":"Workout logged","value":"","title":"Nimit - Body building","time":"2018-11-06 11:38:04"},{"type":"Workout logged","value":"","title":"ABC","time":"2018-11-06 12:27:37"},{"type":"Workout logged","value":"","title":"aktest","time":"2018-11-06 12:58:32"},{"type":"Workout logged","value":"","title":"gdgdg","time":"2018-11-06 19:47:32"},{"type":"Workout logged","value":"","title":"gdgdg","time":"2018-11-11 11:11:34"},{"type":"Workout logged","value":"","title":"Nimit - Body building","time":"2018-11-12 07:17:17"},{"type":"Workout logged","value":"","title":"new exercise11","time":"2018-11-12 09:56:05"},{"type":"Workout logged","value":"","title":"new exercise11","time":"2018-11-13 09:06:04"},{"type":"Workout logged","value":"","title":"new exercise11","time":"2018-11-14 05:10:58"},{"type":"Workout logged","value":"","title":"qa1Copy","time":"2018-11-15 06:07:28"},{"type":"Workout logged","value":"","title":"new exercise11","time":"2018-11-15 14:51:06"},{"type":"Workout logged","value":"","title":"qa1Copy","time":"2018-11-16 06:05:29"},{"type":"Workout logged","value":"","title":"new exercise11","time":"2018-11-16 08:12:15"},{"type":"Workout logged","value":"","title":"new exercise11","time":"2018-11-17 09:57:20"},{"type":"Workout logged","value":"","title":"atul111","time":"2018-11-19 06:35:42"},{"type":"Workout logged","value":"","title":"ruchi new workout 2","time":"2018-11-20 07:09:02"},{"type":"Workout logged","value":"","title":"abc omg","time":"2018-11-21 13:03:43"},{"type":"Workout logged","value":"","title":"abc omg","time":"2018-11-22 05:51:48"},{"type":"Diet logged","value":"","title":"","time":"2018-11-02 06:03:45"},{"type":"Diet logged","value":"","title":"","time":"2018-10-25 07:17:39"},{"type":"Diet logged","value":"","title":"","time":"2018-11-02 06:25:15"},{"type":"Diet logged","value":"","title":"Tushar Body Building","time":"2018-11-14 09:37:37"},{"type":"Diet logged","value":"","title":"my diet","time":"2018-10-25 06:24:10"},{"type":"Diet logged","value":"","title":"test12","time":"2018-11-02 05:49:41"},{"type":"Diet logged","value":"","title":"weight loss","time":"2018-11-02 06:07:05"},{"type":"Diet logged","value":"","title":"","time":"2018-11-02 06:26:32"},{"type":"Diet logged","value":"","title":"","time":"2018-11-02 09:58:16"},{"type":"Diet logged","value":"","title":"","time":"2018-11-01 12:49:42"},{"type":"Diet logged","value":"","title":"","time":"2018-10-25 10:14:15"},{"type":"Diet logged","value":"","title":"","time":"2018-10-25 07:17:31"},{"type":"Diet logged","value":"","title":"","time":"2018-11-01 09:45:08"},{"type":"Diet logged","value":"","title":"","time":"2018-10-30 07:38:01"},{"type":"Diet logged","value":"","title":"","time":"2018-10-25 07:27:42"},{"type":"meausurements_updated","value":"","title":"User updated measurements","time":"2018-10-30 07:53:14","images":["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540307762_Hydrangeas.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540308116_Hydrangeas.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443941_1540443935290.81.jpg"]},{"type":"meausurements_updated","value":"","title":"User updated measurements","time":"2018-10-31 07:53:14","images":["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540473320_1540473318942.63.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540473321_1540473318951.37.jpg"]},{"type":"meausurements_updated","value":"","title":"User updated measurements","time":"2018-10-29 07:53:14","images":["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540468955_1540468953021.36.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540473320_1540473318919.32.jpg"]},{"type":"meausurements_updated","value":"","title":"User updated measurements","time":"2018-10-28 07:53:14","images":["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540468955_1540468953037.32.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540473320_1540473318933.37.jpg"]},{"type":"meausurements_updated","value":"","title":"User updated measurements","time":"2018-11-01 05:10:22","images":["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541049029_1541049023318.49.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541049031_1541049023429.05.jpg"]},{"type":"meausurements_updated","value":"","title":"User updated measurements","time":"2018-11-02 09:24:34","images":["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541150677_1541150674412.47.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541150677_1541150674439.99.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541150677_1541150674460.79.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541150677_1541150674478.59.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1541150677_1541150674494.02.jpg"]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * type : Workout logged
         * value :
         * title : qa1Copy
         * time : 2018-11-05 09:58:26
         * images : ["https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540307762_Hydrangeas.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540308116_Hydrangeas.jpg","https://php6.shaligraminfotech.com/public/images/checkin_form_images/1540443941_1540443935290.81.jpg"]
         */

        private String type;
        private String value;
        private String title;
        private String time;
        private List<String> images;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }
    }
}
