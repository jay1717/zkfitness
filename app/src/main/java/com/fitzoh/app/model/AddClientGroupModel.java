package com.fitzoh.app.model;

public class AddClientGroupModel {
    private int status;
    private String message;
    private AddClientGroupListModel.DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddClientGroupListModel.DataBean getData() {
        return data;
    }

    public void setData(AddClientGroupListModel.DataBean data) {
        this.data = data;
    }
}
