package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SearchDietPlanFoodData {
    /**
     * status : 200
     * message : success
     * data : [{"id":4,"food_category_id":19,"food_name":"Vegetarian Baked Beans","serving_size":"1","serving_size_unit":"oz","calories":"27","fat":"0.1g","carbs":"5.99g","Dishes_protien":"1.35g","dietary_fiber":"1.2g","sugar":"2.56g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t96mg","potassium":"\t\t\t\t62mg","created_at":"2018-06-12 11:36:23","updated_at":"2018-06-12 11:36:23","deleted_at":null},{"id":5,"food_category_id":19,"food_name":"Vegetarian Baked Beans","serving_size":"100","serving_size_unit":"g","calories":"94","fat":"0.37g","carbs":"21.14g","Dishes_protien":"4.75g","dietary_fiber":"4.1g","sugar":"9.04g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t337mg","potassium":"\t\t\t\t217mg","created_at":"2018-06-12 11:36:23","updated_at":"2018-06-12 11:36:23","deleted_at":null},{"id":6,"food_category_id":19,"food_name":"Vegetarian Baked Beans","serving_size":"1","serving_size_unit":"cup","calories":"239","fat":"0.94g","carbs":"53.7g","Dishes_protien":"12.06g","dietary_fiber":"10.4g","sugar":"22.96g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t856mg","potassium":"\t\t\t\t551mg","created_at":"2018-06-12 11:36:23","updated_at":"2018-06-12 11:36:23","deleted_at":null}]
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 4
         * food_category_id : 19
         * food_name : Vegetarian Baked Beans
         * serving_size : 1
         * serving_size_unit : oz
         * calories : 27
         * fat : 0.1g
         * carbs : 5.99g
         * Dishes_protien : 1.35g
         * dietary_fiber : 1.2g
         * sugar : 2.56g
         * cholesterol : 				0mg
         * sodium : 				96mg
         * potassium : 				62mg
         * created_at : 2018-06-12 11:36:23
         * updated_at : 2018-06-12 11:36:23
         * deleted_at : null
         */

        @SerializedName("id")
        private int id;
        @SerializedName("food_category_id")
        private int foodCategoryId;
        @SerializedName("food_name")
        private String foodName;
        @SerializedName("serving_size")
        private String servingSize;
        @SerializedName("serving_size_unit")
        private String servingSizeUnit;
        @SerializedName("calories")
        private String calories;
        @SerializedName("fat")
        private String fat;
        @SerializedName("carbs")
        private String carbs;
        @SerializedName("protien")
        private String protien;
        @SerializedName("dietary_fiber")
        private String dietaryFiber;
        @SerializedName("sugar")
        private String sugar;
        @SerializedName("cholesterol")
        private String cholesterol;
        @SerializedName("sodium")
        private String sodium;
        @SerializedName("potassium")
        private String potassium;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("deleted_at")
        private Object deletedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getFoodCategoryId() {
            return foodCategoryId;
        }

        public void setFoodCategoryId(int foodCategoryId) {
            this.foodCategoryId = foodCategoryId;
        }

        public String getFoodName() {
            return foodName;
        }

        public void setFoodName(String foodName) {
            this.foodName = foodName;
        }

        public String getServingSize() {
            return servingSize;
        }

        public void setServingSize(String servingSize) {
            this.servingSize = servingSize;
        }

        public String getServingSizeUnit() {
            return servingSizeUnit;
        }

        public void setServingSizeUnit(String servingSizeUnit) {
            this.servingSizeUnit = servingSizeUnit;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getDishesProtien() {
            return protien;
        }

        public void setDishesProtien(String protien) {
            this.protien = protien;
        }

        public String getDietaryFiber() {
            return dietaryFiber;
        }

        public void setDietaryFiber(String dietaryFiber) {
            this.dietaryFiber = dietaryFiber;
        }

        public String getSugar() {
            return sugar;
        }

        public void setSugar(String sugar) {
            this.sugar = sugar;
        }

        public String getCholesterol() {
            return cholesterol;
        }

        public void setCholesterol(String cholesterol) {
            this.cholesterol = cholesterol;
        }

        public String getSodium() {
            return sodium;
        }

        public void setSodium(String sodium) {
            this.sodium = sodium;
        }

        public String getPotassium() {
            return potassium;
        }

        public void setPotassium(String potassium) {
            this.potassium = potassium;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getDeletedAt() {
            return deletedAt;
        }

        public void setDeletedAt(Object deletedAt) {
            this.deletedAt = deletedAt;
        }
    }
}
