package com.fitzoh.app.model;

import java.util.ArrayList;

public class Singleton {

    private static Singleton single_instance = null;

    public ArrayList<Integer> integers;

    private Singleton()
    {
        integers= new ArrayList<>();
    }

    public static Singleton getInstance()
    {
        if (single_instance == null)
            single_instance = new Singleton();

        return single_instance;
    }


}
