package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ProfileGymSubscriptionModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":4,"name":"pkp gym sub","no_of_months":"8","about_subscriptions":"nothing special enjoy","price":"300.00","discount_price":"123.00"},{"id":5,"name":"sub 1","no_of_months":"5","about_subscriptions":"teafsdfasddfasdf","price":"150.00","discount_price":"50.00"},{"id":6,"name":"sub 1","no_of_months":"5","about_subscriptions":"teafsdfasddfasdf","price":"150.00","discount_price":"50.00"},{"id":7,"name":"sub 1","no_of_months":"5","about_subscriptions":"teafsdfasddfasdf","price":"150.00","discount_price":"50.00"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 4
         * name : pkp gym sub
         * no_of_months : 8
         * about_subscriptions : nothing special enjoy
         * price : 300.00
         * discount_price : 123.00
         */

        private int id;
        private String name;
        private String no_of_months;
        private String about_subscriptions;
        private String price;
        private String discount_price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNo_of_months() {
            return no_of_months;
        }

        public void setNo_of_months(String no_of_months) {
            this.no_of_months = no_of_months;
        }

        public String getAbout_subscriptions() {
            return about_subscriptions;
        }

        public void setAbout_subscriptions(String about_subscriptions) {
            this.about_subscriptions = about_subscriptions;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount_price() {
            return discount_price;
        }

        public void setDiscount_price(String discount_price) {
            this.discount_price = discount_price;
        }
    }
}
