package com.fitzoh.app.model;

public class UserUpdateData {


    /**
     * success : 200
     * message : Profile updated successfully!
     * data : {"id":90,"role_id":"2","name":"jinank","email":"jinank.thakker@gmail.com","birth_date":"1992-06-18","photo":"http://106.201.238.169:4091/images/user_images/1536935210.jpeg"}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 90
         * role_id : 2
         * name : jinank
         * email : jinank.thakker@gmail.com
         * birth_date : 1992-06-18
         * photo : http://106.201.238.169:4091/images/user_images/1536935210.jpeg
         */

        private int id;
        private String role_id;
        private String name;
        private String email;
        private String birth_date;
        private String photo;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBirth_date() {
            return birth_date;
        }

        public void setBirth_date(String birth_date) {
            this.birth_date = birth_date;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
