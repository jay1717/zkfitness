package com.fitzoh.app.model;

import java.util.List;

public class PurchaseHistoryTrainerModel {


    /**
     * status : 200
     * message : success
     * data : {"CurrentPlan":{"id":7,"subscription_id":"1","start_date":"2018-12-11","limitation":"unlimited","price":"0.00","remaining_day":5,"plan_name":"Free 30 Day Trail"},"OwnPlan":[{"id":10,"subscription_id":"3","start_date":"2018-12-11","price":"1599.00","plan_name":"Upto 50 clients","remaining_day":5}],"Client":[{"id":20,"name":"client3","photo":"","purchase_date":"2018-12-29","package_name":"nudge test Package","desc":"Gdkldgldf gdgdf","weeks":"1","price":"5.00","discounted_price":"2.00"}]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * CurrentPlan : {"id":7,"subscription_id":"1","start_date":"2018-12-11","limitation":"unlimited","price":"0.00","remaining_day":5,"plan_name":"Free 30 Day Trail"}
         * OwnPlan : [{"id":10,"subscription_id":"3","start_date":"2018-12-11","price":"1599.00","plan_name":"Upto 50 clients","remaining_day":5}]
         * Client : [{"id":20,"name":"client3","photo":"","purchase_date":"2018-12-29","package_name":"nudge test Package","desc":"Gdkldgldf gdgdf","weeks":"1","price":"5.00","discounted_price":"2.00"}]
         */

        private CurrentPlanBean CurrentPlan;
        private List<OwnPlanBean> OwnPlan;
        private List<ClientBean> Client;

        public CurrentPlanBean getCurrentPlan() {
            return CurrentPlan;
        }

        public void setCurrentPlan(CurrentPlanBean CurrentPlan) {
            this.CurrentPlan = CurrentPlan;
        }

        public List<OwnPlanBean> getOwnPlan() {
            return OwnPlan;
        }

        public void setOwnPlan(List<OwnPlanBean> OwnPlan) {
            this.OwnPlan = OwnPlan;
        }

        public List<ClientBean> getClient() {
            return Client;
        }

        public void setClient(List<ClientBean> Client) {
            this.Client = Client;
        }

        public static class CurrentPlanBean {
            /**
             * id : 7
             * subscription_id : 1
             * start_date : 2018-12-11
             * limitation : unlimited
             * price : 0.00
             * remaining_day : 5
             * plan_name : Free 30 Day Trail
             */

            private int id;
            private String subscription_id;
            private String start_date;
            private String limitation;
            private String price;
            private int remaining_day;
            private String plan_name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSubscription_id() {
                return subscription_id;
            }

            public void setSubscription_id(String subscription_id) {
                this.subscription_id = subscription_id;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }

            public String getLimitation() {
                return limitation;
            }

            public void setLimitation(String limitation) {
                this.limitation = limitation;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public int getRemaining_day() {
                return remaining_day;
            }

            public void setRemaining_day(int remaining_day) {
                this.remaining_day = remaining_day;
            }

            public String getPlan_name() {
                return plan_name;
            }

            public void setPlan_name(String plan_name) {
                this.plan_name = plan_name;
            }
        }

        public static class OwnPlanBean {
            /**
             * id : 10
             * subscription_id : 3
             * start_date : 2018-12-11
             * price : 1599.00
             * plan_name : Upto 50 clients
             * remaining_day : 5
             */

            private int id;
            private String subscription_id;
            private String start_date;
            private String price;
            private String plan_name;
            private int remaining_day;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSubscription_id() {
                return subscription_id;
            }

            public void setSubscription_id(String subscription_id) {
                this.subscription_id = subscription_id;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getPlan_name() {
                return plan_name;
            }

            public void setPlan_name(String plan_name) {
                this.plan_name = plan_name;
            }

            public int getRemaining_day() {
                return remaining_day;
            }

            public void setRemaining_day(int remaining_day) {
                this.remaining_day = remaining_day;
            }
        }

        public static class ClientBean {
            /**
             * id : 20
             * name : client3
             * photo :
             * purchase_date : 2018-12-29
             * package_name : nudge test Package
             * desc : Gdkldgldf gdgdf
             * weeks : 1
             * price : 5.00
             * discounted_price : 2.00
             */

            private int id;
            private String name;
            private String photo;
            private String purchase_date;
            private String package_name;
            private String desc;
            private String weeks;
            private String price;
            private String discounted_price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getPurchase_date() {
                return purchase_date;
            }

            public void setPurchase_date(String purchase_date) {
                this.purchase_date = purchase_date;
            }

            public String getPackage_name() {
                return package_name;
            }

            public void setPackage_name(String package_name) {
                this.package_name = package_name;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getWeeks() {
                return weeks;
            }

            public void setWeeks(String weeks) {
                this.weeks = weeks;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscounted_price() {
                return discounted_price;
            }

            public void setDiscounted_price(String discounted_price) {
                this.discounted_price = discounted_price;
            }
        }
    }
}
