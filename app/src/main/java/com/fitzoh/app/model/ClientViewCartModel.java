package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ClientViewCartModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"product_id":"1","name":"Whey Protein","image":"https://php6.shaligraminfotech.com/public/images/products/whey.jpg","price":"10.00","discounted_price":"5.00"},{"id":2,"product_id":"1","name":"Whey Protein","image":"https://php6.shaligraminfotech.com/public/images/products/whey.jpg","price":"10.00","discounted_price":"5.00"},{"id":3,"product_id":"1","name":"Whey Protein","image":"https://php6.shaligraminfotech.com/public/images/products/whey.jpg","price":"10.00","discounted_price":"5.00"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * product_id : 1
         * name : Whey Protein
         * image : https://php6.shaligraminfotech.com/public/images/products/whey.jpg
         * price : 10.00
         * discounted_price : 5.00
         */

        private int id;
        private String product_id;
        private String name;
        private String image;
        private String price;
        private String discounted_price;
        private int qty = 1;

        public int getQty() {
            return qty;
        }

        public void setQty(int qty) {
            this.qty = qty;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscounted_price() {
            return discounted_price;
        }

        public void setDiscounted_price(String discounted_price) {
            this.discounted_price = discounted_price;
        }
    }
}
