package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

public class ClientWorkOutListModel extends Observable implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":81,"name":"nilesh4 workout","exercise_count":8,"no_of_sets":2,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":112,"name":"denisha1","exercise_count":6,"no_of_sets":9,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":155,"name":"TestNidhiCopy","exercise_count":12,"no_of_sets":8,"is_assigned":1,"assign_by":"aman"},{"id":173,"name":"qa1Copy","exercise_count":9,"no_of_sets":4,"is_assigned":1,"assign_by":"aman"},{"id":184,"name":"nilesh1-1oct","exercise_count":2,"no_of_sets":1,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":200,"name":"Nidhi workout copy","exercise_count":2,"no_of_sets":3,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":206,"name":"jinank1234","exercise_count":0,"no_of_sets":0,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":221,"name":"atul111","exercise_count":10,"no_of_sets":9,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":222,"name":"ABC","exercise_count":4,"no_of_sets":3,"is_assigned":1,"assign_by":"aman"},{"id":223,"name":"abc omg","exercise_count":3,"no_of_sets":0,"is_assigned":1,"assign_by":"aman"},{"id":273,"name":"test prakash123","exercise_count":3,"no_of_sets":3,"is_assigned":1,"assign_by":"aman"},{"id":276,"name":"Nimit - Body building","exercise_count":3,"no_of_sets":0,"is_assigned":1,"assign_by":"aman"},{"id":278,"name":"Dixita - Fitness","exercise_count":2,"no_of_sets":3,"is_assigned":1,"assign_by":"aman"},{"id":285,"name":"one workout","exercise_count":0,"no_of_sets":0,"is_assigned":1,"assign_by":"aman"},{"id":301,"name":"pkp's test","exercise_count":1,"no_of_sets":1,"is_assigned":1,"assign_by":"aman"},{"id":314,"name":"akash","exercise_count":0,"no_of_sets":0,"is_assigned":1,"assign_by":"aman"},{"id":341,"name":"new pk","exercise_count":2,"no_of_sets":2,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":344,"name":"aktest","exercise_count":1,"no_of_sets":1,"is_assigned":1,"assign_by":"aman"},{"id":346,"name":"cxxx","exercise_count":0,"no_of_sets":0,"is_assigned":1,"assign_by":"aman"},{"id":349,"name":"staffs","exercise_count":1,"no_of_sets":2,"is_assigned":1,"assign_by":"aman"},{"id":358,"name":"Nidhi Test1","exercise_count":2,"no_of_sets":3,"is_assigned":1,"assign_by":"aman"},{"id":366,"name":"Client Test Edit","exercise_count":6,"no_of_sets":3,"is_assigned":1,"assign_by":"aman"},{"id":367,"name":"Nidhi Profile Edit","exercise_count":5,"no_of_sets":4,"is_assigned":1,"assign_by":"aman"},{"id":368,"name":"Jonah Edit Profile","exercise_count":5,"no_of_sets":4,"is_assigned":1,"assign_by":"aman"},{"id":369,"name":"ruchi new workout 2","exercise_count":5,"no_of_sets":10,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":370,"name":"meet 17/11/2018","exercise_count":5,"no_of_sets":16,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":383,"name":"2 for test s - 4","exercise_count":2,"no_of_sets":4,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":384,"name":"denisha new workout","exercise_count":1,"no_of_sets":3,"is_assigned":1,"assign_by":"nilesh panchal"},{"id":3,"name":"Shoulder Workout","exercise_count":0,"no_of_sets":0,"is_assigned":0,"assign_by":"self"},{"id":7,"name":"ketan bhai nu workout","exercise_count":0,"no_of_sets":0,"is_assigned":0,"assign_by":"self"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 81
         * name : nilesh4 workout
         * exercise_count : 8
         * no_of_sets : 2
         * is_assigned : 1
         * assign_by : nilesh panchal
         */

        private int id;
        private String name;
        private int exercise_count;
        private int no_of_sets;
        private int is_assigned;
        private String assign_by;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getExercise_count() {
            return exercise_count;
        }

        public void setExercise_count(int exercise_count) {
            this.exercise_count = exercise_count;
        }

        public int getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(int no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }

        public String getAssign_by() {
            return assign_by;
        }

        public void setAssign_by(String assign_by) {
            this.assign_by = assign_by;
        }
    }
}
