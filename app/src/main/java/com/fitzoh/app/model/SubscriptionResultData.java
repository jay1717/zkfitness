package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class SubscriptionResultData implements Serializable {


    /**
     * data : [{"id":1,"name":"Free 30 Day Trail","price":0,"is_subscribe":1,"remain_day":0},{"id":2,"name":"Trainer Version","price":1500,"is_subscribe":0,"remain_day":0},{"id":3,"name":"Gym or Health Club Version","price":800,"is_subscribe":0,"remain_day":0},{"id":4,"name":"Custom APP-IN Your Own Name","price":12000.5,"is_subscribe":0,"remain_day":0}]
     * status : 200
     * message : success
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * id : 1
         * name : Free 30 Day Trail
         * price : 0
         * is_subscribe : 1
         * remain_day : 0
         */

        private int id;
        private String name;
        private double price;
        private int is_subscribe;
        private int remain_day;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getIs_subscribe() {
            return is_subscribe;
        }

        public void setIs_subscribe(int is_subscribe) {
            this.is_subscribe = is_subscribe;
        }

        public int getRemain_day() {
            return remain_day;
        }

        public void setRemain_day(int remain_day) {
            this.remain_day = remain_day;
        }
    }
}
