package com.fitzoh.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WorkoutExerciseListModel {

    private int status;
    private String message;
    private List<List<DataBean>> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable, Parcelable {

        private int id, exercise_set_id;
        private String exercise_name;
        private String image;
        private String no_of_sets;
        private boolean checked;

        protected DataBean(Parcel in) {
            id = in.readInt();
            exercise_set_id = in.readInt();
            exercise_name = in.readString();
            image = in.readString();
            no_of_sets = in.readString();
            checked = in.readByte() != 0;
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public String getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(String no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public List<List<Set>> getSets() {
            return sets;
        }

        public void setSets(List<List<Set>> sets) {
            this.sets = sets;
        }

        private List<List<Set>> sets;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExercise_name() {
            return exercise_name;
        }

        public void setExercise_name(String exercise_name) {
            this.exercise_name = exercise_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getExercise_set_id() {
            return exercise_set_id;
        }

        public void setExercise_set_id(int exercise_set_id) {
            this.exercise_set_id = exercise_set_id;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(id);
            dest.writeInt(exercise_set_id);
            dest.writeString(exercise_name);
            dest.writeString(image);
            dest.writeString(no_of_sets);
            dest.writeByte((byte) (checked ? 1 : 0));
        }

        private class Set {

            @SerializedName("setno")
            @Expose
            private String setno;
            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("parameter_id")
            @Expose
            private Integer parameterId;
            @SerializedName("value")
            @Expose
            private String value;
            @SerializedName("parameter_name")
            @Expose
            private String parameterName;
            @SerializedName("set_value")
            @Expose
            private String setValue;
            @SerializedName("weight_unit")
            @Expose
            private Integer weightUnit;
            @SerializedName("history_value")
            @Expose
            private String historyValue;

            public String getSetno() {
                return setno;
            }

            public void setSetno(String setno) {
                this.setno = setno;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getParameterId() {
                return parameterId;
            }

            public void setParameterId(Integer parameterId) {
                this.parameterId = parameterId;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getParameterName() {
                return parameterName;
            }

            public void setParameterName(String parameterName) {
                this.parameterName = parameterName;
            }

            public String getSetValue() {
                return setValue;
            }

            public void setSetValue(String setValue) {
                this.setValue = setValue;
            }

            public Integer getWeightUnit() {
                return weightUnit;
            }

            public void setWeightUnit(Integer weightUnit) {
                this.weightUnit = weightUnit;
            }

            public String getHistoryValue() {
                return historyValue;
            }

            public void setHistoryValue(String historyValue) {
                this.historyValue = historyValue;
            }

        }
    }

}
