package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

public class DietListModel extends Observable {


    /**
     * success : 200
     * message : success
     * data : [{"id":1,"name":"Diet1","no_of_meals":2,"diet_plan_macro_nutrient":["Calories","Protein","Carbs","Fat"],"assigned_to":[{"id":6,"name":"hrien","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg"},{"id":10,"name":"Dipsheekha","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg"}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * name : Diet1
         * no_of_meals : 2
         * diet_plan_macro_nutrient : ["Calories","Protein","Carbs","Fat"]
         * assigned_to : [{"id":6,"name":"hrien","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg"},{"id":10,"name":"Dipsheekha","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg"}]
         */

        private int id;
        private String name;
        private int no_of_meals;
        private int is_assigned;
        private String assign_by;
        private List<String> diet_plan_macro_nutrient;
        private List<AssignedToBean> assigned_to;

        private int is_client_created;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNo_of_meals() {
            return no_of_meals;
        }

        public void setNo_of_meals(int no_of_meals) {
            this.no_of_meals = no_of_meals;
        }

        public List<String> getDiet_plan_macro_nutrient() {
            return diet_plan_macro_nutrient;
        }

        public void setDiet_plan_macro_nutrient(List<String> diet_plan_macro_nutrient) {
            this.diet_plan_macro_nutrient = diet_plan_macro_nutrient;
        }

        public List<AssignedToBean> getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(List<AssignedToBean> assigned_to) {
            this.assigned_to = assigned_to;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }

        public String getAssign_by() {
            return assign_by;
        }

        public int getIs_client_created() {
            return is_client_created;
        }

        public void setIs_client_created(int is_client_created) {
            this.is_client_created = is_client_created;
        }


        public void setAssign_by(String assign_by) {
            this.assign_by = assign_by;
        }
    }
}
