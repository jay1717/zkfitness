package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class GetFitzohProductModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"name":"Whey Protein","category_id":"1","brand_id":"1","carbs":"20.00","fat":"20.00","calories":"20.00","description":"whey ","protein":"10.00","ingredients":"milk extracts","category":"Protein","brand":"Muscle Blaze","image":["https://php6.shaligraminfotech.com/public/images/products/whey.jpg","https://php6.shaligraminfotech.com/public/images/products/whey.jpg"]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * id : 1
         * name : Whey Protein
         * category_id : 1
         * brand_id : 1
         * carbs : 20.00
         * fat : 20.00
         * calories : 20.00
         * description : whey
         * protein : 10.00
         * ingredients : milk extracts
         * category : Protein
         * brand : Muscle Blaze
         * image : ["https://php6.shaligraminfotech.com/public/images/products/whey.jpg","https://php6.shaligraminfotech.com/public/images/products/whey.jpg"]
         */

        private int id;
        private String name;
        private String category_id;
        private int brand_id;
        private String carbs;
        private String fat;
        private String calories;
        private String description;
        private String protein;
        private String ingredients;
        private String category;
        private String brand;
        private List<String> image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public Integer getBrand_id() {
            return brand_id;
        }

        public void setBrand_id(Integer brand_id) {
            this.brand_id = brand_id;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getIngredients() {
            return ingredients;
        }

        public void setIngredients(String ingredients) {
            this.ingredients = ingredients;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }
    }
}
