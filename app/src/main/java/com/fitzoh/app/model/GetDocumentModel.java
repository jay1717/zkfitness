package com.fitzoh.app.model;

import java.util.List;

public class GetDocumentModel {

    /**
     * status : 200
     * message : success
     * data : {"uploaded_images":["https://php6.shaligraminfotech.com/public/images/documents/1542373797_DSC06125.JPG","https://php6.shaligraminfotech.com/public/images/documents/1542431918_Penguins.png","https://php6.shaligraminfotech.com/public/images/documents/1542432025_Lighthouse.jpg","https://php6.shaligraminfotech.com/public/images/documents/1542432207_createPdf.pdf","https://php6.shaligraminfotech.com/public/images/documents/1542432252_Penguins.png","https://php6.shaligraminfotech.com/public/images/documents/1542433002_image[0]","https://php6.shaligraminfotech.com/public/images/documents/1542433123_database_query.txt","https://php6.shaligraminfotech.com/public/images/documents/1542433123_add-checkin.jpg","https://php6.shaligraminfotech.com/public/images/documents/1542433381_image[0]","https://php6.shaligraminfotech.com/public/images/documents/1542433964_image[0]","https://php6.shaligraminfotech.com/public/images/documents/1542434207_image[0]","https://php6.shaligraminfotech.com/public/images/documents/1542434394_PNG%20image.png","https://php6.shaligraminfotech.com/public/images/documents/Fitzoh.1542432025_Lighthouse.jpg"],"download_files":["https://php6.shaligraminfotech.com/public/images/documents/1542373149_after.jpg","https://php6.shaligraminfotech.com/public/images/documents/demoform1.pdf"]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<String> uploaded_images;
        private List<String> download_files;

        public List<String> getUploaded_images() {
            return uploaded_images;
        }

        public void setUploaded_images(List<String> uploaded_images) {
            this.uploaded_images = uploaded_images;
        }

        public List<String> getDownload_files() {
            return download_files;
        }

        public void setDownload_files(List<String> download_files) {
            this.download_files = download_files;
        }
    }
}
