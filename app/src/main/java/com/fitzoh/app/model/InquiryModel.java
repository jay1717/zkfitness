package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class InquiryModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"client_name":"prakash new client","id":"94","client_photo":"https://php6.shaligraminfotech.com/fitzoh/public/images/user_images/1545212843_cropped289312149.jpg","no_of_inquiries":6,"inquiries":[{"product_name":"Vita pro","product_image":"","qty":"3","price":"2500.00"}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * client_name : prakash new client
         * id : 94
         * client_photo : https://php6.shaligraminfotech.com/fitzoh/public/images/user_images/1545212843_cropped289312149.jpg
         * no_of_inquiries : 6
         * inquiries : [{"product_name":"Vita pro","product_image":"","qty":"3","price":"2500.00"}]
         */

        private String client_name;
        private String id;
        private String client_photo;
        private int no_of_inquiries;
        private List<InquiriesBean> inquiries;

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getClient_photo() {
            return client_photo;
        }

        public void setClient_photo(String client_photo) {
            this.client_photo = client_photo;
        }

        public int getNo_of_inquiries() {
            return no_of_inquiries;
        }

        public void setNo_of_inquiries(int no_of_inquiries) {
            this.no_of_inquiries = no_of_inquiries;
        }

        public List<InquiriesBean> getInquiries() {
            return inquiries;
        }

        public void setInquiries(List<InquiriesBean> inquiries) {
            this.inquiries = inquiries;
        }

        public static class InquiriesBean implements Serializable {
            /**
             * product_name : Vita pro
             * product_image :
             * qty : 3
             * price : 2500.00
             */

            private String product_name;
            private String product_image;
            private String qty;
            private String price;

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getProduct_image() {
                return product_image;
            }

            public void setProduct_image(String product_image) {
                this.product_image = product_image;
            }

            public String getQty() {
                return qty;
            }

            public void setQty(String qty) {
                this.qty = qty;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }
    }
}
