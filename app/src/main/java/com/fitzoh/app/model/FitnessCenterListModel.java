package com.fitzoh.app.model;

import java.util.List;
import java.util.Observable;

public class FitnessCenterListModel extends Observable {


    /**
     * status : 200
     * message : success
     * data : [{"id":76,"name":"urvi","speciality":"weight lass training","rating":5,"photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","is_featured":0,"is_approved":1,"location":"Bodakdev, Ahmedabad, Gujarat, India","is_gym":0},{"id":10,"name":"Dipsheekha","rating":5,"photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","is_featured":0,"is_approved":0,"location":"Bodakdev, Ahmedabad, Gujarat, India","is_gym":1},{"id":12,"name":"test user","rating":5,"photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","is_featured":0,"is_approved":1,"location":"Bodakdev, Ahmedabad, Gujarat, India","is_gym":1},{"id":12,"name":"test user","rating":0,"photo":"http://106.201.238.169:4091/images/user_images/default.jpeg","is_featured":0,"is_approved":1,"location":"Bodakdev, Ahmedabad, Gujarat, India","is_gym":1}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 76
         * name : urvi
         * speciality : weight lass training
         * rating : 5
         * photo : http://106.201.238.169:4091/images/user_images/default.jpeg
         * is_featured : 0
         * is_approved : 1
         * location : Bodakdev, Ahmedabad, Gujarat, India
         * is_gym : 0
         */

        private int id;
        private String name;
        private String speciality;
        private int rating;
        private String photo;
        private int is_featured;
        private int is_approved;
        private String location;
        private int is_gym;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpeciality() {
            return speciality;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public int getIs_featured() {
            return is_featured;
        }

        public void setIs_featured(int is_featured) {
            this.is_featured = is_featured;
        }

        public int getIs_approved() {
            return is_approved;
        }

        public void setIs_approved(int is_approved) {
            this.is_approved = is_approved;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public int getIs_gym() {
            return is_gym;
        }

        public void setIs_gym(int is_gym) {
            this.is_gym = is_gym;
        }
    }
}
