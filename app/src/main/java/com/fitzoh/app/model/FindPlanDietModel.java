package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class FindPlanDietModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":26,"name":"diet plan test 2","no_of_meals":0,"diet_plan_macro_nutrient":["Calories","Protein","Carbs","Fat"],"assigned_to":[],"price":0}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 26
         * name : diet plan test 2
         * no_of_meals : 0
         * diet_plan_macro_nutrient : ["Calories","Protein","Carbs","Fat"]
         * assigned_to : []
         * price : 0
         */

        private int id;
        private String name;
        private int no_of_meals;
        private int price;
        private int is_purchased;
        private List<String> diet_plan_macro_nutrient;
        private List<?> assigned_to;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getNo_of_meals() {
            return no_of_meals;
        }

        public void setNo_of_meals(int no_of_meals) {
            this.no_of_meals = no_of_meals;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public List<String> getDiet_plan_macro_nutrient() {
            return diet_plan_macro_nutrient;
        }

        public void setDiet_plan_macro_nutrient(List<String> diet_plan_macro_nutrient) {
            this.diet_plan_macro_nutrient = diet_plan_macro_nutrient;
        }

        public List<?> getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(List<?> assigned_to) {
            this.assigned_to = assigned_to;
        }

        public int getIs_purchased() {
            return is_purchased;
        }

        public void setIs_purchased(int is_purchased) {
            this.is_purchased = is_purchased;
        }
    }
}
