package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;

public class SpecialityListModel extends Observable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"name":"Muscle Building"},{"id":2,"name":"Weight Loss"},{"id":3,"name":"Strength Training"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * id : 1
         * name : Muscle Building
         */

        private int id;
        private String name;
        public boolean isSelected;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
