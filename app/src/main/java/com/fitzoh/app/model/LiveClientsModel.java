package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class LiveClientsModel implements Serializable {


    /**
     * success : 200
     * message : Record find successfully.
     * data : [{"id":31,"name":"Client","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg"},{"id":27,"name":"Client","photo":"http://106.201.238.169:4091/images/user_images/default.jpeg"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 31
         * name : Client
         * photo : http://106.201.238.169:4091/images/user_images/default.jpeg
         */

        private int id;
        private String name;
        private String photo;
        private String email;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}
