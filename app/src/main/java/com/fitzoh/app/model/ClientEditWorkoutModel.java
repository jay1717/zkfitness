package com.fitzoh.app.model;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.List;

public class ClientEditWorkoutModel implements Serializable {

    /**
     * status : 200
     * message : success
     * data : [[{"id":1,"exercise_name":"Barbell Shrug Behind The Neck","image":"https://fitzoh.com/images/exercises/default1.jpg","no_of_sets":0,"workout_id":"227","rating":0,"sets":[]}],[{"id":2,"exercise_name":"Barbell Shrugs","image":"https://fitzoh.com/images/exercises/default2.jpg","no_of_sets":0,"workout_id":"227","rating":0,"sets":[]}],[{"id":53,"exercise_name":"test 29","image":"https://fitzoh.com/images/exercises/1548744843_cropped-545543337.jpg","no_of_sets":0,"workout_id":"227","rating":0,"sets":[]}],[{"id":55,"exercise_name":"test exercise","image":"","no_of_sets":0,"workout_id":"227","rating":0,"sets":[]}],[{"id":10,"exercise_name":"Upright Cable Row","image":"https://fitzoh.com/images/exercises/default10.jpg","no_of_sets":0,"workout_id":"227","rating":0,"sets":[]}]]
     */

    private int status;
    private String message;
    private List<List<DataBean>> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * exercise_name : Barbell Shrug Behind The Neck
         * image : https://fitzoh.com/images/exercises/default1.jpg
         * no_of_sets : 0
         * workout_id : 227
         * rating : 0
         * sets : []
         */

        private int id;
        private String exercise_name;
        private String image;
        private int no_of_sets;
        private String workout_id;
        private int rating;
        private List<?> sets;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExercise_name() {
            return exercise_name;
        }

        public void setExercise_name(String exercise_name) {
            this.exercise_name = exercise_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getNo_of_sets() {
            return no_of_sets;
        }

        public void setNo_of_sets(int no_of_sets) {
            this.no_of_sets = no_of_sets;
        }

        public String getWorkout_id() {
            return workout_id;
        }

        public void setWorkout_id(String workout_id) {
            this.workout_id = workout_id;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }

        public List<?> getSets() {
            return sets;
        }

        public void setSets(List<?> sets) {
            this.sets = sets;
        }
    }
}
