package com.fitzoh.app.model;

import java.io.Serializable;

public class TotalNutritionModel {


    /**
     * status : 200
     * message : success
     * data : {"no_of_meals":2,"calories":"0 gm","protein":"0 gm","carbs":"0 gm","fat":"0 gm"}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * no_of_meals : 2
         * calories : 0 gm
         * protein : 0 gm
         * carbs : 0 gm
         * fat : 0 gm
         */

        private int no_of_meals;
        private String calories;
        private String protein;
        private String carbs;
        private String fat;

        public int getNo_of_meals() {
            return no_of_meals;
        }

        public void setNo_of_meals(int no_of_meals) {
            this.no_of_meals = no_of_meals;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }
    }
}
