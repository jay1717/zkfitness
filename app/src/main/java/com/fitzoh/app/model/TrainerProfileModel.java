package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TrainerProfileModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : {"id":539,"name":"Maurya","email":"maurya86@gmail.com","about":"","country_code":"+91","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","is_featured":"0","logo":"","address":"206, SG हाईवे, बोडकदेव, Bodakdev","city":"Ahmedabad","state":"Gujarat 380054","country":"India","profile_desc":"Hi","facilities":[{"id":1,"name":"24/7 GYM access","image":"https://php6.shaligraminfotech.com/public/images/facilities/gym_access.jpg"},{"id":2,"name":"Free Global Membership","image":"https://php6.shaligraminfotech.com/public/images/facilities/gym_access.jpg"},{"id":4,"name":"Free highly qualified & experienced floor trainer","image":"https://php6.shaligraminfotech.com/public/images/facilities/floor_trainer.jpg"}],"speciality":""}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 539
         * name : Maurya
         * email : maurya86@gmail.com
         * about :
         * country_code : +91
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg
         * is_featured : 0
         * logo :
         * address : 206, SG हाईवे, बोडकदेव, Bodakdev
         * city : Ahmedabad
         * state : Gujarat 380054
         * country : India
         * profile_desc : Hi
         * facilities : [{"id":1,"name":"24/7 GYM access","image":"https://php6.shaligraminfotech.com/public/images/facilities/gym_access.jpg"},{"id":2,"name":"Free Global Membership","image":"https://php6.shaligraminfotech.com/public/images/facilities/gym_access.jpg"},{"id":4,"name":"Free highly qualified & experienced floor trainer","image":"https://php6.shaligraminfotech.com/public/images/facilities/floor_trainer.jpg"}]
         * speciality :
         */

        private int id;
        private String name;
        private String email;
        private String about;
        private String country_code;
        private String photo;


        private String mobile_number;

        private String birth_date;


        private String gender;


        private String location;

        private String is_featured;
        private String logo;
        private String address;


        private String client_request_status;
        private String city;
        private String state;
        private String country;
        private String landmark;


        private String profile_desc;
        private String speciality;
        private String request_status;
        private int is_subscription;

        private int subscription_id;
        private int subscription_continue;


        private String is_request_sent;
        private int remain_days;
        private List<FacilitiesBean> facilities;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAbout() {
            return about;
        }

        public void setAbout(String about) {
            this.about = about;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getIs_featured() {
            return is_featured;
        }

        public void setIs_featured(String is_featured) {
            this.is_featured = is_featured;
        }

        public String getLogo() {
            return logo;
        }

        public String getLocation() {
            return location;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }


        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getProfile_desc() {
            return profile_desc;
        }

        public String getBirth_date() {
            return birth_date;
        }

        public void setBirth_date(String birth_date) {
            this.birth_date = birth_date;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public void setProfile_desc(String profile_desc) {
            this.profile_desc = profile_desc;
        }

        public String getSpeciality() {
            return speciality;
        }

        public void setSpeciality(String speciality) {
            this.speciality = speciality;
        }

        public List<FacilitiesBean> getFacilities() {
            return facilities;
        }

        public void setFacilities(List<FacilitiesBean> facilities) {
            this.facilities = facilities;
        }

        public String getRequest_status() {
            return request_status;
        }

        public void setRequest_status(String request_status) {
            this.request_status = request_status;
        }

        public String getClient_request_status() {
            return client_request_status;
        }

        public String getIs_request_sent() {
            return is_request_sent;
        }

        public void setIs_request_sent(String is_request_sent) {
            this.is_request_sent = is_request_sent;
        }

        public void setClient_request_status(String client_request_status) {
            this.client_request_status = client_request_status;
        }

        public int getIs_subscription() {
            return is_subscription;
        }

        public void setIs_subscription(int is_subscription) {
            this.is_subscription = is_subscription;
        }

        public int getSubscription_id() {
            return subscription_id;
        }

        public void setSubscription_id(int subscription_id) {
            this.subscription_id = subscription_id;
        }

        public int getSubscription_continue() {
            return subscription_continue;
        }

        public void setSubscription_continue(int subscription_continue) {
            this.subscription_continue = subscription_continue;
        }
        public int getRemain_days() {
            return remain_days;
        }

        public void setRemain_days(int remain_days) {
            this.remain_days = remain_days;
        }

        public static class FacilitiesBean implements Serializable {
            /**
             * id : 1
             * <p>
             * name : 24/7 GYM access
             * image : https://php6.shaligraminfotech.com/public/images/facilities/gym_access.jpg
             */

            private int id;
            private String name;
            private String image;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }
        }
    }
}
