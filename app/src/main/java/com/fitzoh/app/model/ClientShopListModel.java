package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ClientShopListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"name":"Whey Protein","carbs":"20.00","calories":"10.00","description":"whey protein","fat":"20.00","protein":"10.00","ingredients":"milk extracts","price":"10.00","qty":"2","image":["https://php6.shaligraminfotech.com/public/images/products/whey.jpg","https://php6.shaligraminfotech.com/public/images/products/whey.jpg"]},{"id":2,"name":"Whey Protein","carbs":"20.00","calories":"10.00","description":"whey protein","fat":"20.00","protein":"10.00","ingredients":"milk extracts, whey","price":"12.00","qty":"2","image":["https://php6.shaligraminfotech.com/public/images/products/whey.jpg","https://php6.shaligraminfotech.com/public/images/products/whey.jpg"]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * name : Whey Protein
         * carbs : 20.00
         * calories : 10.00
         * description : whey protein
         * fat : 20.00
         * protein : 10.00
         * ingredients : milk extracts
         * price : 10.00
         * qty : 2
         * image : ["https://php6.shaligraminfotech.com/public/images/products/whey.jpg","https://php6.shaligraminfotech.com/public/images/products/whey.jpg"]
         */

        private int id;
        private String name;
        private String carbs;
        private String calories;
        private String description;
        private String fat;
        private String protein;
        private String ingredients;
        private String price;
        private String qty;
        private String discounted_price;
        private List<String> image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getIngredients() {
            return ingredients;
        }

        public void setIngredients(String ingredients) {
            this.ingredients = ingredients;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getDiscounted_price() {
            return discounted_price;
        }

        public void setDiscounted_price(String discounted_price) {
            this.discounted_price = discounted_price;
        }
    }
}
