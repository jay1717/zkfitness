package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class LiveClientDetailModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"time":"10 AM","exercise_name":"Exercise 1","rating":1,"duration":"20 min"},{"time":"10 AM","exercise_name":"Exercise 1","rating":1,"duration":"20 min"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * time : 10 AM
         * exercise_name : Exercise 1
         * rating : 1
         * duration : 20 min
         */
        private int id;
        private String exercise_name;
        private float rating;
        private String timestamp;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExercise_name() {
            return exercise_name;
        }

        public void setExercise_name(String exercise_name) {
            this.exercise_name = exercise_name;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }
    }
}
