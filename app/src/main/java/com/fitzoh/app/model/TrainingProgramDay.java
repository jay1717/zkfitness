package com.fitzoh.app.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TrainingProgramDay implements Cloneable, Parcelable {


    private int id = 0;
    private int training_week_id;
    private String day_number;
    private int diet_plan_id = 0;
    private int workout_pm_id = 0;
    private int workout_am_id = 0;
    private int is_restfull_day = 0;
    private int is_checkin = 0;
    private String diet_plan_name;
    private String workout_am_name;
    private String workout_pm_name;
    private int  isSelected = 0;
    private int  workout_am_logged = 0;
    private int  workout_pm_logged = 0;
    private int  diet_plan_logged = 0;
    private int  checkin_logged = 0;

    public TrainingProgramDay() {
    }

    protected TrainingProgramDay(Parcel in) {
        id = in.readInt();
        training_week_id = in.readInt();
        day_number = in.readString();
        diet_plan_id = in.readInt();
        workout_pm_id = in.readInt();
        workout_am_id = in.readInt();
        is_restfull_day = in.readInt();
        is_checkin = in.readInt();
        diet_plan_name = in.readString();
        workout_am_name = in.readString();
        workout_pm_name = in.readString();
        isSelected = in.readInt();
        workout_am_logged = in.readInt();
        workout_pm_logged = in.readInt();
        diet_plan_logged = in.readInt();
        checkin_logged = in.readInt();

    }

    public static final Creator<TrainingProgramDay> CREATOR = new Creator<TrainingProgramDay>() {
        @Override
        public TrainingProgramDay createFromParcel(Parcel in) {
            return new TrainingProgramDay(in);
        }

        @Override
        public TrainingProgramDay[] newArray(int size) {
            return new TrainingProgramDay[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTraining_week_id() {
        return training_week_id;
    }

    public void setTraining_week_id(int training_week_id) {
        this.training_week_id = training_week_id;
    }

    public String getDay_number() {
        return day_number;
    }

    public void setDay_number(String day_number) {
        this.day_number = day_number;
    }

    public int getDiet_plan_id() {
        return diet_plan_id;
    }

    public void setDiet_plan_id(int diet_plan_id) {
        this.diet_plan_id = diet_plan_id;
    }

    public int getWorkout_pm_id() {
        return workout_pm_id;
    }

    public void setWorkout_pm_id(int workout_pm_id) {
        this.workout_pm_id = workout_pm_id;
    }

    public int getWorkout_am_id() {
        return workout_am_id;
    }

    public void setWorkout_am_id(int workout_am_id) {
        this.workout_am_id = workout_am_id;
    }

    public int getIs_restfull_day() {
        return is_restfull_day;
    }

    public void setIs_restfull_day(int is_restfull_day) {
        this.is_restfull_day = is_restfull_day;
    }

    public String getDiet_plan_name() {
        return diet_plan_name;
    }

    public void setDiet_plan_name(String diet_plan_name) {
        this.diet_plan_name = diet_plan_name;
    }

    public String getWorkout_am_name() {
        return workout_am_name;
    }

    public void setWorkout_am_name(String workout_am_name) {
        this.workout_am_name = workout_am_name;
    }

    public String getWorkout_pm_name() {
        return workout_pm_name;
    }

    public void setWorkout_pm_name(String workout_pm_name) {
        this.workout_pm_name = workout_pm_name;
    }

    public int getIs_checkin() {
        return is_checkin;
    }

    public void setIs_checkin(int is_checkin) {
        this.is_checkin = is_checkin;
    }

    public int isSelected() {
        return isSelected;
    }

    public void setSelected(int selected) {
        isSelected = selected;
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public int getWorkout_am_logged() {
        return workout_am_logged;
    }

    public void setWorkout_am_logged(int workout_am_logged) {
        this.workout_am_logged = workout_am_logged;
    }

    public int getWorkout_pm_logged() {
        return workout_pm_logged;
    }

    public void setWorkout_pm_logged(int workout_pm_logged) {
        this.workout_pm_logged = workout_pm_logged;
    }

    public int getDiet_plan_logged() {
        return diet_plan_logged;
    }

    public void setDiet_plan_logged(int diet_plan_logged) {
        this.diet_plan_logged = diet_plan_logged;
    }

    public int getCheckin_logged() {
        return checkin_logged;
    }

    public void setCheckin_logged(int checkin_logged) {
        this.checkin_logged = checkin_logged;
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(training_week_id);
        parcel.writeString(day_number);
        parcel.writeInt(diet_plan_id);
        parcel.writeInt(workout_pm_id);
        parcel.writeInt(workout_am_id);
        parcel.writeInt(is_restfull_day);
        parcel.writeInt(is_checkin);
        parcel.writeString(diet_plan_name);
        parcel.writeString(workout_am_name);
        parcel.writeString(workout_pm_name);
        parcel.writeInt(isSelected);
        parcel.writeInt(workout_am_logged);
        parcel.writeInt(workout_pm_logged);
        parcel.writeInt(diet_plan_logged);
        parcel.writeInt(checkin_logged);


    }
}
