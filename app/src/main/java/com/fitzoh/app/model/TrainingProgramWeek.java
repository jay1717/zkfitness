package com.fitzoh.app.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class TrainingProgramWeek implements Cloneable, Parcelable {


    private int id;
    private int training_plan_id;
    private String week_number;
    private List<TrainingProgramDay> weekdays;

    public TrainingProgramWeek() {
    }

    public TrainingProgramWeek(Parcel in) {
        id = in.readInt();
        training_plan_id = in.readInt();
        week_number = in.readString();
        weekdays = new ArrayList<TrainingProgramDay>();
        in.readTypedList(weekdays, TrainingProgramDay.CREATOR);
    }

    public static final Creator<TrainingProgramWeek> CREATOR = new Creator<TrainingProgramWeek>() {
        @Override
        public TrainingProgramWeek createFromParcel(Parcel in) {
            return new TrainingProgramWeek(in);
        }

        @Override
        public TrainingProgramWeek[] newArray(int size) {
            return new TrainingProgramWeek[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTraining_plan_id() {
        return training_plan_id;
    }

    public void setTraining_plan_id(int training_plan_id) {
        this.training_plan_id = training_plan_id;
    }

    public String getWeek_number() {
        return week_number;
    }

    public void setWeek_number(String week_number) {
        this.week_number = week_number;
    }

    public List<TrainingProgramDay> getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(List<TrainingProgramDay> weekdays) {
        this.weekdays = weekdays;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(training_plan_id);
        parcel.writeString(week_number);
        parcel.writeTypedList(weekdays);
    }
}
