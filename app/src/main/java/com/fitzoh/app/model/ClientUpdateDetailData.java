package com.fitzoh.app.model;

import java.util.List;

public class ClientUpdateDetailData {


    /**
     * status : 200
     * message : success
     * data : {"name":"by","photo":"https://php6.shaligraminfotech.com/public/images/user_images/1543905910_1543905907590.06.png","date":"2018-12-04","todays_workout":[{"workout_name":"pkp's test","rating":"0.00","graph":[{"key":"burpees","value":"1"}]}],"percentage_taken":60,"todays_diet":[{"name":"meal1","percentage_taken":65,"food":[{"id":1,"name":"food1","serving_size":"100g"}]}]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : by
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/1543905910_1543905907590.06.png
         * date : 2018-12-04
         * mobile_number
         * todays_workout : [{"workout_name":"pkp's test","rating":"0.00","graph":[{"key":"burpees","value":"1"}]}]
         * percentage_taken : 60
         * todays_diet : [{"name":"meal1","percentage_taken":65,"food":[{"id":1,"name":"food1","serving_size":"100g"}]}]
         */

        private String name;
        private String photo;
        private String mobile_number;

        private String date;
        private float percentage_taken;
        private List<TodaysWorkoutBean> todays_workout;
        private List<TodaysDietBean> todays_diet;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }


        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public float getPercentage_taken() {
            return percentage_taken;
        }

        public void setPercentage_taken(float percentage_taken) {
            this.percentage_taken = percentage_taken;
        }

        public List<TodaysWorkoutBean> getTodays_workout() {
            return todays_workout;
        }

        public void setTodays_workout(List<TodaysWorkoutBean> todays_workout) {
            this.todays_workout = todays_workout;
        }

        public List<TodaysDietBean> getTodays_diet() {
            return todays_diet;
        }

        public void setTodays_diet(List<TodaysDietBean> todays_diet) {
            this.todays_diet = todays_diet;
        }

        public static class TodaysWorkoutBean {
            /**
             * workout_name : pkp's test
             * rating : 0.00
             * graph : [{"key":"burpees","value":"1"}]
             */

            private String workout_name;
            private float rating;
            private List<GraphBean> graph;

            public String getWorkout_name() {
                return workout_name;
            }

            public void setWorkout_name(String workout_name) {
                this.workout_name = workout_name;
            }

            public float getRating() {
                return rating;
            }

            public void setRating(float rating) {
                this.rating = rating;
            }

            public List<GraphBean> getGraph() {
                return graph;
            }

            public void setGraph(List<GraphBean> graph) {
                this.graph = graph;
            }

            public static class GraphBean {
                /**
                 * key : burpees
                 * value : 1
                 */

                private String key;
                private String value;

                public String getKey() {
                    return key;
                }

                public void setKey(String key) {
                    this.key = key;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }
        }

        public static class TodaysDietBean {
            /**
             * name : meal1
             * percentage_taken : 65
             * food : [{"id":1,"name":"food1","serving_size":"100g"}]
             */

            private String name;
            private float percentage_taken;
            private List<FoodBean> food;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public float getPercentage_taken() {
                return percentage_taken;
            }

            public void setPercentage_taken(float percentage_taken) {
                this.percentage_taken = percentage_taken;
            }

            public List<FoodBean> getFood() {
                return food;
            }

            public void setFood(List<FoodBean> food) {
                this.food = food;
            }

            public static class FoodBean {
                /**
                 * id : 1
                 * name : food1
                 * serving_size : 100g
                 */

                private int id;
                private String name;
                private String serving_size;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getServing_size() {
                    return serving_size;
                }

                public void setServing_size(String serving_size) {
                    this.serving_size = serving_size;
                }
            }
        }
    }
}
