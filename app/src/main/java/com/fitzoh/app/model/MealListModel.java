package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class MealListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":441,"diet_plan_id":155,"time":"03:00 AM","day_part":"","name":"meal 1","calories":"1071.00","protein":"47.84","carbs":"117.68","fat":"45.55","is_active":0,"is_deleted":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null,"diet_plan":{"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null},"diet_plan_food":[{"id":190,"diet_plan_id":155,"diet_plan_meal_id":441,"day_part":0,"food_id":2450,"no_of_servings":"1","serving_size_id":298,"serving_size":"pizza (9\" dia)","unit":0,"calories_g":"1071.00","protein_g":"48.00","carbs_g":"118.00","fat_g":"46.00","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null,"food":{"id":2450,"food_category_id":136,"food_name":"Cheese Pizza","serving_size":"1","serving_size_unit":"pizza (9\" dia) ","calories":"1071.00g","fat":"46.00g","carbs":"117.68g","dietary_fiber":"7.4g","sugar":"13.81g","cholesterol":"\t\t\t\t93mg","sodium":"\t\t\t\t2084mg","potassium":"\t\t\t\t625mg","Dishes_protien":"48.00g","carbs_g":"118.00g","fibre":"0.00g"}}]},{"id":442,"diet_plan_id":155,"time":"12:00 AM","day_part":"","name":"meal 2","calories":"468.00","protein":"0.60","carbs":"115.88","fat":"1.08","is_active":0,"is_deleted":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null,"diet_plan":{"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null},"diet_plan_food":[{"id":191,"diet_plan_id":155,"diet_plan_meal_id":442,"day_part":0,"food_id":220,"no_of_servings":"4","serving_size_id":3,"serving_size":"cup","unit":0,"calories_g":"468.00","protein_g":"1.00","carbs_g":"116.00","fat_g":"1.00","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null,"food":{"id":220,"food_category_id":32,"food_name":"Apple Juice (Canned or Bottled)","serving_size":"1","serving_size_unit":"cup","calories":"468.00g","fat":"1.00g","carbs":"28.97g","dietary_fiber":"0.2g","sugar":"27.03g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t7mg","potassium":"\t\t\t\t295mg","Dishes_protien":"1.00g","carbs_g":"116.00g","fibre":"0.00g"}}]},{"id":465,"diet_plan_id":155,"time":"12:00 AM","day_part":"","name":"meal 123","calories":"0.00","protein":"0.00","carbs":"0.00","fat":"0.00","is_active":0,"is_deleted":0,"created_at":"2018-10-04 09:41:38","updated_at":"2018-10-04 09:41:38","deleted_at":null,"diet_plan":{"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null},"diet_plan_food":[]},{"id":466,"diet_plan_id":155,"time":"06:30 AM","day_part":"","name":"meal13","calories":"0.00","protein":"0.00","carbs":"0.00","fat":"0.00","is_active":0,"is_deleted":0,"created_at":"2018-10-04 09:42:34","updated_at":"2018-10-04 09:42:34","deleted_at":null,"diet_plan":{"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null},"diet_plan_food":[]},{"id":467,"diet_plan_id":155,"time":"06:29 AM","day_part":"","name":"melatesting","calories":"0.00","protein":"0.00","carbs":"0.00","fat":"0.00","is_active":0,"is_deleted":0,"created_at":"2018-10-04 09:44:38","updated_at":"2018-10-04 09:44:38","deleted_at":null,"diet_plan":{"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null},"diet_plan_food":[]},{"id":484,"diet_plan_id":155,"time":"06:30 PM","day_part":"","name":"add_meal_12345","calories":"105.00","protein":"1.27","carbs":"24.91","fat":"1.06","is_active":0,"is_deleted":0,"created_at":"2018-10-04 11:13:02","updated_at":"2018-10-04 11:15:14","deleted_at":null,"diet_plan":{"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null},"diet_plan_food":[{"id":200,"diet_plan_id":155,"diet_plan_meal_id":484,"day_part":0,"food_id":3940,"no_of_servings":"1","serving_size_id":1,"serving_size":"oz","unit":0,"calories_g":"25.00","protein_g":"0.31","carbs_g":"6.48","fat_g":"0.09","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 11:13:48","updated_at":"2018-10-04 11:13:48","deleted_at":null,"food":{"id":3940,"food_category_id":189,"food_name":"Apple Banana","serving_size":"1","serving_size_unit":"oz","calories":"25.00g","fat":"0.09g","carbs":"6.48g","dietary_fiber":"0.7g","sugar":"3.47g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t0mg","potassium":"\t\t\t\t101mg","Dishes_protien":"0.31g","carbs_g":"6.48g","fibre":"0.00g"}},{"id":201,"diet_plan_id":155,"diet_plan_meal_id":484,"day_part":0,"food_id":3940,"no_of_servings":"1","serving_size_id":1,"serving_size":"oz","unit":0,"calories_g":"25.00","protein_g":"0.31","carbs_g":"6.48","fat_g":"0.09","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 11:13:49","updated_at":"2018-10-04 11:13:49","deleted_at":null,"food":{"id":3940,"food_category_id":189,"food_name":"Apple Banana","serving_size":"1","serving_size_unit":"oz","calories":"25.00g","fat":"0.09g","carbs":"6.48g","dietary_fiber":"0.7g","sugar":"3.47g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t0mg","potassium":"\t\t\t\t101mg","Dishes_protien":"0.31g","carbs_g":"6.48g","fibre":"0.00g"}},{"id":202,"diet_plan_id":155,"diet_plan_meal_id":484,"day_part":0,"food_id":3940,"no_of_servings":"1","serving_size_id":1,"serving_size":"oz","unit":0,"calories_g":"25.00","protein_g":"0.31","carbs_g":"6.48","fat_g":"0.09","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 11:13:49","updated_at":"2018-10-04 11:13:49","deleted_at":null,"food":{"id":3940,"food_category_id":189,"food_name":"Apple Banana","serving_size":"1","serving_size_unit":"oz","calories":"25.00g","fat":"0.09g","carbs":"6.48g","dietary_fiber":"0.7g","sugar":"3.47g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t0mg","potassium":"\t\t\t\t101mg","Dishes_protien":"0.31g","carbs_g":"6.48g","fibre":"0.00g"}},{"id":203,"diet_plan_id":155,"diet_plan_meal_id":484,"day_part":0,"food_id":9765,"no_of_servings":"1","serving_size_id":109,"serving_size":"cubic inch","unit":0,"calories_g":"30.00","protein_g":"0.34","carbs_g":"5.47","fat_g":"0.79","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 11:15:14","updated_at":"2018-10-04 11:15:14","deleted_at":null,"food":{"id":9765,"food_category_id":468,"food_name":"Apple Cobbler","serving_size":"1","serving_size_unit":"cubic inch","calories":"30.00g","fat":"0.79g","carbs":"5.47g","dietary_fiber":"0.1g","sugar":"3.48g","cholesterol":"\t\t\t\t0mg","sodium":"\t\t\t\t30mg","potassium":"\t\t\t\t12mg","Dishes_protien":"0.34g","carbs_g":"5.47g","fibre":"0.00g"}}]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 441
         * diet_plan_id : 155
         * time : 03:00 AM
         * day_part :
         * name : meal 1
         * calories : 1071.00
         * protein : 47.84
         * carbs : 117.68
         * fat : 45.55
         * is_active : 0
         * is_deleted : 0
         * created_at : 2018-10-04 05:24:06
         * updated_at : 2018-10-04 05:24:06
         * deleted_at : null
         * diet_plan : {"id":155,"owner_type":"1","owner_id":76,"diet_plan_id":null,"name":"diet new","day_part":null,"note":null,"is_active":1,"is_deleted":0,"created_by":0,"updated_by":0,"deleted_by":0,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null}
         * diet_plan_food : [{"id":190,"diet_plan_id":155,"diet_plan_meal_id":441,"day_part":0,"food_id":2450,"no_of_servings":"1","serving_size_id":298,"serving_size":"pizza (9\" dia)","unit":0,"calories_g":"1071.00","protein_g":"48.00","carbs_g":"118.00","fat_g":"46.00","fibre_g":"0.00","sodium_mg":"0.00","sugar_g":"0.00","note":null,"created_at":"2018-10-04 05:24:06","updated_at":"2018-10-04 05:24:06","deleted_at":null,"food":{"id":2450,"food_category_id":136,"food_name":"Cheese Pizza","serving_size":"1","serving_size_unit":"pizza (9\" dia) ","calories":"1071.00g","fat":"46.00g","carbs":"117.68g","dietary_fiber":"7.4g","sugar":"13.81g","cholesterol":"\t\t\t\t93mg","sodium":"\t\t\t\t2084mg","potassium":"\t\t\t\t625mg","Dishes_protien":"48.00g","carbs_g":"118.00g","fibre":"0.00g"}}]
         */

        private int id;
        private int diet_plan_id;
        private String time;
        private String day_part;
        private String name;
        private String calories;
        private String protein;
        private String carbs;
        private String fat;
        private int is_active;
        private int is_deleted;
        private String created_at;
        private String updated_at;
        private Object deleted_at;
        private DietPlanBean diet_plan;
        private List<DietPlanFoodBean> diet_plan_food;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getDiet_plan_id() {
            return diet_plan_id;
        }

        public void setDiet_plan_id(int diet_plan_id) {
            this.diet_plan_id = diet_plan_id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getDay_part() {
            return day_part;
        }

        public void setDay_part(String day_part) {
            this.day_part = day_part;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public int getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(int is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public DietPlanBean getDiet_plan() {
            return diet_plan;
        }

        public void setDiet_plan(DietPlanBean diet_plan) {
            this.diet_plan = diet_plan;
        }

        public List<DietPlanFoodBean> getDiet_plan_food() {
            return diet_plan_food;
        }

        public void setDiet_plan_food(List<DietPlanFoodBean> diet_plan_food) {
            this.diet_plan_food = diet_plan_food;
        }

        public static class DietPlanBean {
            /**
             * id : 155
             * owner_type : 1
             * owner_id : 76
             * diet_plan_id : null
             * name : diet new
             * day_part : null
             * note : null
             * is_active : 1
             * is_deleted : 0
             * created_by : 0
             * updated_by : 0
             * deleted_by : 0
             * created_at : 2018-10-04 05:24:06
             * updated_at : 2018-10-04 05:24:06
             * deleted_at : null
             */

            private int id;
            private String owner_type;
            private int owner_id;
            private Object diet_plan_id;
            private String name;
            private Object day_part;
            private Object note;
            private int is_active;
            private int is_deleted;
            private int created_by;
            private int updated_by;
            private int deleted_by;
            private String created_at;
            private String updated_at;
            private Object deleted_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getOwner_type() {
                return owner_type;
            }

            public void setOwner_type(String owner_type) {
                this.owner_type = owner_type;
            }

            public int getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(int owner_id) {
                this.owner_id = owner_id;
            }

            public Object getDiet_plan_id() {
                return diet_plan_id;
            }

            public void setDiet_plan_id(Object diet_plan_id) {
                this.diet_plan_id = diet_plan_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getDay_part() {
                return day_part;
            }

            public void setDay_part(Object day_part) {
                this.day_part = day_part;
            }

            public Object getNote() {
                return note;
            }

            public void setNote(Object note) {
                this.note = note;
            }

            public int getIs_active() {
                return is_active;
            }

            public void setIs_active(int is_active) {
                this.is_active = is_active;
            }

            public int getIs_deleted() {
                return is_deleted;
            }

            public void setIs_deleted(int is_deleted) {
                this.is_deleted = is_deleted;
            }

            public int getCreated_by() {
                return created_by;
            }

            public void setCreated_by(int created_by) {
                this.created_by = created_by;
            }

            public int getUpdated_by() {
                return updated_by;
            }

            public void setUpdated_by(int updated_by) {
                this.updated_by = updated_by;
            }

            public int getDeleted_by() {
                return deleted_by;
            }

            public void setDeleted_by(int deleted_by) {
                this.deleted_by = deleted_by;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }
        }

        public static class DietPlanFoodBean {
            /**
             * id : 190
             * diet_plan_id : 155
             * diet_plan_meal_id : 441
             * day_part : 0
             * food_id : 2450
             * no_of_servings : 1
             * serving_size_id : 298
             * serving_size : pizza (9" dia)
             * unit : 0
             * calories_g : 1071.00
             * protein_g : 48.00
             * carbs_g : 118.00
             * fat_g : 46.00
             * fibre_g : 0.00
             * sodium_mg : 0.00
             * sugar_g : 0.00
             * note : null
             * created_at : 2018-10-04 05:24:06
             * updated_at : 2018-10-04 05:24:06
             * deleted_at : null
             * food : {"id":2450,"food_category_id":136,"food_name":"Cheese Pizza","serving_size":"1","serving_size_unit":"pizza (9\" dia) ","calories":"1071.00g","fat":"46.00g","carbs":"117.68g","dietary_fiber":"7.4g","sugar":"13.81g","cholesterol":"\t\t\t\t93mg","sodium":"\t\t\t\t2084mg","potassium":"\t\t\t\t625mg","Dishes_protien":"48.00g","carbs_g":"118.00g","fibre":"0.00g"}
             */

            private int id;
            private int diet_plan_id;
            private int diet_plan_meal_id;
            private int day_part;
            private int food_id;
            private String no_of_servings;
            private int serving_size_id;
            private String serving_size;
            private int unit;
            private String calories_g;
            private String protein_g;
            private String carbs_g;
            private String fat_g;
            private String fibre_g;
            private String sodium_mg;
            private String sugar_g;
            private Object note;
            private String created_at;
            private String updated_at;
            private Object deleted_at;
            private FoodBean food;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getDiet_plan_id() {
                return diet_plan_id;
            }

            public void setDiet_plan_id(int diet_plan_id) {
                this.diet_plan_id = diet_plan_id;
            }

            public int getDiet_plan_meal_id() {
                return diet_plan_meal_id;
            }

            public void setDiet_plan_meal_id(int diet_plan_meal_id) {
                this.diet_plan_meal_id = diet_plan_meal_id;
            }

            public int getDay_part() {
                return day_part;
            }

            public void setDay_part(int day_part) {
                this.day_part = day_part;
            }

            public int getFood_id() {
                return food_id;
            }

            public void setFood_id(int food_id) {
                this.food_id = food_id;
            }

            public String getNo_of_servings() {
                return no_of_servings;
            }

            public void setNo_of_servings(String no_of_servings) {
                this.no_of_servings = no_of_servings;
            }

            public int getServing_size_id() {
                return serving_size_id;
            }

            public void setServing_size_id(int serving_size_id) {
                this.serving_size_id = serving_size_id;
            }

            public String getServing_size() {
                return serving_size;
            }

            public void setServing_size(String serving_size) {
                this.serving_size = serving_size;
            }

            public int getUnit() {
                return unit;
            }

            public void setUnit(int unit) {
                this.unit = unit;
            }

            public String getCalories_g() {
                return calories_g;
            }

            public void setCalories_g(String calories_g) {
                this.calories_g = calories_g;
            }

            public String getProtein_g() {
                return protein_g;
            }

            public void setProtein_g(String protein_g) {
                this.protein_g = protein_g;
            }

            public String getCarbs_g() {
                return carbs_g;
            }

            public void setCarbs_g(String carbs_g) {
                this.carbs_g = carbs_g;
            }

            public String getFat_g() {
                return fat_g;
            }

            public void setFat_g(String fat_g) {
                this.fat_g = fat_g;
            }

            public String getFibre_g() {
                return fibre_g;
            }

            public void setFibre_g(String fibre_g) {
                this.fibre_g = fibre_g;
            }

            public String getSodium_mg() {
                return sodium_mg;
            }

            public void setSodium_mg(String sodium_mg) {
                this.sodium_mg = sodium_mg;
            }

            public String getSugar_g() {
                return sugar_g;
            }

            public void setSugar_g(String sugar_g) {
                this.sugar_g = sugar_g;
            }

            public Object getNote() {
                return note;
            }

            public void setNote(Object note) {
                this.note = note;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }

            public FoodBean getFood() {
                return food;
            }

            public void setFood(FoodBean food) {
                this.food = food;
            }

            public static class FoodBean {
                /**
                 * id : 2450
                 * food_category_id : 136
                 * food_name : Cheese Pizza
                 * serving_size : 1
                 * serving_size_unit : pizza (9" dia)
                 * calories : 1071.00g
                 * fat : 46.00g
                 * carbs : 117.68g
                 * dietary_fiber : 7.4g
                 * sugar : 13.81g
                 * cholesterol : 				93mg
                 * sodium : 				2084mg
                 * potassium : 				625mg
                 * Dishes_protien : 48.00g
                 * carbs_g : 118.00g
                 * fibre : 0.00g
                 */

                private int id;
                private int food_category_id;
                private String food_name;
                private String serving_size;
                private String serving_size_unit;
                private String calories;
                private String fat;
                private String carbs;
                private String dietary_fiber;
                private String sugar;
                private String cholesterol;
                private String sodium;
                private String potassium;
                private String Dishes_protien;
                private String carbs_g;
                private String fibre;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public int getFood_category_id() {
                    return food_category_id;
                }

                public void setFood_category_id(int food_category_id) {
                    this.food_category_id = food_category_id;
                }

                public String getFood_name() {
                    return food_name;
                }

                public void setFood_name(String food_name) {
                    this.food_name = food_name;
                }

                public String getServing_size() {
                    return serving_size;
                }

                public void setServing_size(String serving_size) {
                    this.serving_size = serving_size;
                }

                public String getServing_size_unit() {
                    return serving_size_unit;
                }

                public void setServing_size_unit(String serving_size_unit) {
                    this.serving_size_unit = serving_size_unit;
                }

                public String getCalories() {
                    return calories;
                }

                public void setCalories(String calories) {
                    this.calories = calories;
                }

                public String getFat() {
                    return fat;
                }

                public void setFat(String fat) {
                    this.fat = fat;
                }

                public String getCarbs() {
                    return carbs;
                }

                public void setCarbs(String carbs) {
                    this.carbs = carbs;
                }

                public String getDietary_fiber() {
                    return dietary_fiber;
                }

                public void setDietary_fiber(String dietary_fiber) {
                    this.dietary_fiber = dietary_fiber;
                }

                public String getSugar() {
                    return sugar;
                }

                public void setSugar(String sugar) {
                    this.sugar = sugar;
                }

                public String getCholesterol() {
                    return cholesterol;
                }

                public void setCholesterol(String cholesterol) {
                    this.cholesterol = cholesterol;
                }

                public String getSodium() {
                    return sodium;
                }

                public void setSodium(String sodium) {
                    this.sodium = sodium;
                }

                public String getPotassium() {
                    return potassium;
                }

                public void setPotassium(String potassium) {
                    this.potassium = potassium;
                }

                public String getDishes_protien() {
                    return Dishes_protien;
                }

                public void setDishes_protien(String Dishes_protien) {
                    this.Dishes_protien = Dishes_protien;
                }

                public String getCarbs_g() {
                    return carbs_g;
                }

                public void setCarbs_g(String carbs_g) {
                    this.carbs_g = carbs_g;
                }

                public String getFibre() {
                    return fibre;
                }

                public void setFibre(String fibre) {
                    this.fibre = fibre;
                }
            }
        }
    }
}
