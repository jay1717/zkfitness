package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainingAssignModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":136,"title":"Nimit\u2019s pro","weeks":3,"is_assigned":1}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable{
        /**
         * id : 136
         * title : Nimit’s pro
         * weeks : 3
         * is_assigned : 1
         */

        private int id;
        private String title;
        private int weeks;
        private int is_assigned;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getWeeks() {
            return weeks;
        }

        public void setWeeks(int weeks) {
            this.weeks = weeks;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }
    }
}
