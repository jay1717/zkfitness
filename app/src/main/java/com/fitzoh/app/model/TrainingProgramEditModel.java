package com.fitzoh.app.model;

import java.util.ArrayList;
import java.util.List;

public class TrainingProgramEditModel {


    private int status;
    private String message;
    private ArrayList<TrainingProgramWeek> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TrainingProgramWeek> getData() {
        return data;
    }

    public void setData(ArrayList<TrainingProgramWeek> data) {
        this.data = data;
    }
}
