package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ProfilePackageModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":20,"owner_id":"76","package_name":"package 1","weeks":"5","price":"100.00","discounted_price":"5.00","desc":"about for package, about for package, about for package, about for package","media":"","media_type":"","youtube_url":""},{"id":22,"owner_id":"76","package_name":"package 3","weeks":"5","price":"100.00","discounted_price":"5.00","desc":"about for package, about for package, about for package, about for package","media":"","media_type":"","youtube_url":""},{"id":35,"owner_id":"76","package_name":null,"weeks":"2","price":"10.00","discounted_price":"5.00","desc":"about for package","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539264787_SampleVideo_1280x720_1mb.mp4","media_type":"","youtube_url":""},{"id":74,"owner_id":"76","package_name":null,"weeks":"2","price":"10.00","discounted_price":"5.00","desc":"about for package","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539669983_radio-unchecked.png","media_type":"","youtube_url":""},{"id":75,"owner_id":"76","package_name":null,"weeks":"5","price":"1000.00","discounted_price":"500.00","desc":"fghg","media":"","media_type":"","youtube_url":""},{"id":76,"owner_id":"76","package_name":null,"weeks":"35","price":"1500.00","discounted_price":"200.00","desc":"aaaaa","media":"","media_type":"","youtube_url":""},{"id":77,"owner_id":"76","package_name":null,"weeks":"2","price":"1000.00","discounted_price":"10.00","desc":"hihihihihh","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539671344_ipmsgclip_r_1538715310_0.png","media_type":"","youtube_url":""},{"id":78,"owner_id":"76","package_name":null,"weeks":"10","price":"2000.00","discounted_price":"100.00","desc":"hello testing","media":"","media_type":"","youtube_url":""},{"id":79,"owner_id":"76","package_name":null,"weeks":"2","price":"10.00","discounted_price":"5.00","desc":"about for package","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539672684_Capture.PNG","media_type":"","youtube_url":""},{"id":80,"owner_id":"76","package_name":null,"weeks":"22","price":"55.00","discounted_price":"65.00","desc":"About","media":"","media_type":"","youtube_url":""},{"id":84,"owner_id":"76","package_name":null,"weeks":"7","price":"100.00","discounted_price":"50.00","desc":"about training","media":"","media_type":"","youtube_url":""},{"id":93,"owner_id":"76","package_name":null,"weeks":"5","price":"15000.00","discounted_price":"800.00","desc":"test","media":"","media_type":"","youtube_url":""},{"id":94,"owner_id":"76","package_name":null,"weeks":"15","price":"800.00","discounted_price":"200.00","desc":"test","media":"","media_type":"","youtube_url":""},{"id":95,"owner_id":"76","package_name":null,"weeks":"5","price":"1000.00","discounted_price":"50.00","desc":"tesr","media":"","media_type":"","youtube_url":""},{"id":96,"owner_id":"76","package_name":null,"weeks":"5","price":"1000.00","discounted_price":"50.00","desc":"tesr","media":"","media_type":"","youtube_url":""},{"id":97,"owner_id":"76","package_name":null,"weeks":"5","price":"1000.00","discounted_price":"50.00","desc":"tesr","media":"","media_type":"","youtube_url":""},{"id":98,"owner_id":"76","package_name":null,"weeks":"5","price":"1000.00","discounted_price":"50.00","desc":"tesr","media":"","media_type":"","youtube_url":""},{"id":103,"owner_id":"76","package_name":null,"weeks":"50","price":"1500.00","discounted_price":"5.00","desc":"test","media":"","media_type":"","youtube_url":""},{"id":105,"owner_id":"76","package_name":null,"weeks":"10","price":"5600.00","discounted_price":"2500.00","desc":"test","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539687117_cropped1596068911.jpg","media_type":"","youtube_url":""},{"id":106,"owner_id":"76","package_name":"package name","weeks":"15","price":"500.00","discounted_price":"15.00","desc":"testing 124","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539691569_cropped725065552.jpg","media_type":"","youtube_url":""},{"id":107,"owner_id":"76","package_name":"test 123456","weeks":"152","price":"500.00","discounted_price":"5.00","desc":"add","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539691645_cropped407147506.jpg","media_type":"","youtube_url":""},{"id":108,"owner_id":"76","package_name":"test abc","weeks":"1","price":"10.00","discounted_price":"5.00","desc":"add","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539691727_cropped204501220.jpg","media_type":"","youtube_url":""},{"id":109,"owner_id":"76","package_name":"abcd","weeks":"52","price":"5000.00","discounted_price":"1000.00","desc":"training","media":"https://php6.shaligraminfotech.com/public/images/packages/76/1539693117_cropped1348615828.jpg","media_type":"","youtube_url":""}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 20
         * owner_id : 76
         * package_name : package 1
         * weeks : 5
         * price : 100.00
         * discounted_price : 5.00
         * desc : about for package, about for package, about for package, about for package
         * media :
         * media_type :
         * youtube_url :
         */

        private int id;
        private String owner_id;
        private String package_name;
        private String weeks;
        private String price;
        private String discounted_price;
        private String desc;
        private String media;
        private String media_type;
        private String youtube_url;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOwner_id() {
            return owner_id;
        }

        public void setOwner_id(String owner_id) {
            this.owner_id = owner_id;
        }

        public String getPackage_name() {
            return package_name;
        }

        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }

        public String getWeeks() {
            return weeks;
        }

        public void setWeeks(String weeks) {
            this.weeks = weeks;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscounted_price() {
            return discounted_price;
        }

        public void setDiscounted_price(String discounted_price) {
            this.discounted_price = discounted_price;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getMedia() {
            return media;
        }

        public void setMedia(String media) {
            this.media = media;
        }

        public String getMedia_type() {
            return media_type;
        }

        public void setMedia_type(String media_type) {
            this.media_type = media_type;
        }

        public String getYoutube_url() {
            return youtube_url;
        }

        public void setYoutube_url(String youtube_url) {
            this.youtube_url = youtube_url;
        }
    }
}
