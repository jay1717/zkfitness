package com.fitzoh.app.model;

import java.util.List;

public class PurchaseHistoryModel {
    /**
     * status : 200
     * message : success
     * data : {"CurrentPlan":{"id":33,"subscription_id":"1","start_date":"2018-12-15","limitation":"unlimited","price":"0.00","remaining_day":5,"plan_name":"Free 30 Day Trail"},"OwnPlan":[{"id":10,"subscription_id":"3","start_date":"2018-12-11","price":"1599.00","plan_name":"Upto 50 clients","remaining_day":5}],"client":[{"id":16,"name":"Client1 Gym","photo":"","purchase_date":"2018-12-26","package_name":"test","desc":"Jhgcjgcjgc","weeks":"5","price":"10.00","discounted_price":"2.00"},{"id":16,"name":"Client1 Gym","photo":"","purchase_date":"2018-12-26","package_name":"test 2","desc":"Sdfssd","weeks":"0","price":"10.00","discounted_price":"1.00"}]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * CurrentPlan : {"id":33,"subscription_id":"1","start_date":"2018-12-15","limitation":"unlimited","price":"0.00","remaining_day":5,"plan_name":"Free 30 Day Trail"}
         * OwnPlan : [{"id":10,"subscription_id":"3","start_date":"2018-12-11","price":"1599.00","plan_name":"Upto 50 clients","remaining_day":5}]
         * client : [{"id":16,"name":"Client1 Gym","photo":"","purchase_date":"2018-12-26","package_name":"test","desc":"Jhgcjgcjgc","weeks":"5","price":"10.00","discounted_price":"2.00"},{"id":16,"name":"Client1 Gym","photo":"","purchase_date":"2018-12-26","package_name":"test 2","desc":"Sdfssd","weeks":"0","price":"10.00","discounted_price":"1.00"}]
         */

        private CurrentPlanBean CurrentPlan;
        private List<OwnPlanBean> OwnPlan;
        private List<ClientBean> client;

        public CurrentPlanBean getCurrentPlan() {
            return CurrentPlan;
        }

        public void setCurrentPlan(CurrentPlanBean CurrentPlan) {
            this.CurrentPlan = CurrentPlan;
        }

        public List<OwnPlanBean> getOwnPlan() {
            return OwnPlan;
        }

        public void setOwnPlan(List<OwnPlanBean> OwnPlan) {
            this.OwnPlan = OwnPlan;
        }

        public List<ClientBean> getClient() {
            return client;
        }

        public void setClient(List<ClientBean> client) {
            this.client = client;
        }

        public static class CurrentPlanBean {
            /**
             * id : 33
             * subscription_id : 1
             * start_date : 2018-12-15
             * limitation : unlimited
             * price : 0.00
             * remaining_day : 5
             * plan_name : Free 30 Day Trail
             */

            private int id;
            private String subscription_id;
            private String start_date;
            private String limitation;
            private String price;
            private int remaining_day;
            private String plan_name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSubscription_id() {
                return subscription_id;
            }

            public void setSubscription_id(String subscription_id) {
                this.subscription_id = subscription_id;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }

            public String getLimitation() {
                return limitation;
            }

            public void setLimitation(String limitation) {
                this.limitation = limitation;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public int getRemaining_day() {
                return remaining_day;
            }

            public void setRemaining_day(int remaining_day) {
                this.remaining_day = remaining_day;
            }

            public String getPlan_name() {
                return plan_name;
            }

            public void setPlan_name(String plan_name) {
                this.plan_name = plan_name;
            }
        }

        public static class OwnPlanBean {

            /**
             * id : 15
             * desc : Sdfssd
             * weeks : 0
             * price : 10.00
             * discounted_price : 1.00
             * package_name : test 2
             * media_type : Image
             * owner_id : 13
             * media_name : https://php6.shaligraminfotech.com/fitzoh/public/images/packages/13/1545125297_1545125295376.07.png
             * purchase_date : 2018-12-26
             */

            private int id;
            private String desc;
            private String weeks;
            private String price;
            private String discounted_price;
            private String package_name;
            private String media_type;
            private String owner_id;
            private String media_name;
            private String purchase_date;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getWeeks() {
                return weeks;
            }

            public void setWeeks(String weeks) {
                this.weeks = weeks;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscounted_price() {
                return discounted_price;
            }

            public void setDiscounted_price(String discounted_price) {
                this.discounted_price = discounted_price;
            }

            public String getPackage_name() {
                return package_name;
            }

            public void setPackage_name(String package_name) {
                this.package_name = package_name;
            }

            public String getMedia_type() {
                return media_type;
            }

            public void setMedia_type(String media_type) {
                this.media_type = media_type;
            }

            public String getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(String owner_id) {
                this.owner_id = owner_id;
            }

            public String getMedia_name() {
                return media_name;
            }

            public void setMedia_name(String media_name) {
                this.media_name = media_name;
            }

            public String getPurchase_date() {
                return purchase_date;
            }

            public void setPurchase_date(String purchase_date) {
                this.purchase_date = purchase_date;
            }
        }

        public static class ClientBean {
            /**
             * id : 16
             * name : Client1 Gym
             * photo :
             * purchase_date : 2018-12-26
             * package_name : test
             * desc : Jhgcjgcjgc
             * weeks : 5
             * price : 10.00
             * discounted_price : 2.00
             */

            private int id;
            private String name;
            private String photo;
            private String purchase_date;
            private String package_name;
            private String desc;
            private String weeks;
            private String price;
            private String discounted_price;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getPurchase_date() {
                return purchase_date;
            }

            public void setPurchase_date(String purchase_date) {
                this.purchase_date = purchase_date;
            }

            public String getPackage_name() {
                return package_name;
            }

            public void setPackage_name(String package_name) {
                this.package_name = package_name;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getWeeks() {
                return weeks;
            }

            public void setWeeks(String weeks) {
                this.weeks = weeks;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscounted_price() {
                return discounted_price;
            }

            public void setDiscounted_price(String discounted_price) {
                this.discounted_price = discounted_price;
            }
        }
    }
}
