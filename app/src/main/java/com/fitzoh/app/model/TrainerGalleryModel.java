package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TrainerGalleryModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":21,"user_id":"76","name":"1539168817_1537428881.jpg","created_at":"2018-10-10 10:53:37","updated_at":"2018-10-10 10:53:37","deleted_at":null,"image":"https://php6.shaligraminfotech.com/public/images/trainer_gallery/76/1539168817_1537428881.jpg"},{"id":22,"user_id":"76","name":"1539168817_1537438237.jpg","created_at":"2018-10-10 10:53:37","updated_at":"2018-10-10 10:53:37","deleted_at":null,"image":"https://php6.shaligraminfotech.com/public/images/trainer_gallery/76/1539168817_1537438237.jpg"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 21
         * user_id : 76
         * name : 1539168817_1537428881.jpg
         * created_at : 2018-10-10 10:53:37
         * updated_at : 2018-10-10 10:53:37
         * deleted_at : null
         * image : https://php6.shaligraminfotech.com/public/images/trainer_gallery/76/1539168817_1537428881.jpg
         */

        private int id;
        private String user_id;
        private String name;
        private boolean isFromGallery = false;
        private String picturePath;
        private String created_at;
        private String updated_at;

        private String created_by;
        private String updated_by;

        private Object deleted_at;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public boolean isFromGallery() {
            return isFromGallery;
        }

        public void setFromGallery(boolean fromGallery) {
            isFromGallery = fromGallery;
        }

        public String getPicturePath() {
            return picturePath;
        }

        public void setPicturePath(String picturePath) {
            this.picturePath = picturePath;
        }


        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

    }
}
