package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class CertificationModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":17,"user_id":"76","name":"1539110700_Tulips.jpg","created_by":"76","updated_by":"0","created_at":"2018-10-09 18:45:00","updated_at":"2018-10-09 18:45:00","image":"https://php6.shaligraminfotech.com/public/images/certifications/76/1539110700_Tulips.jpg"},{"id":18,"user_id":"76","name":"1539110700_Penguins.png","created_by":"76","updated_by":"0","created_at":"2018-10-09 18:45:00","updated_at":"2018-10-09 18:45:00","image":"https://php6.shaligraminfotech.com/public/images/certifications/76/1539110700_Penguins.png"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 17
         * user_id : 76
         * name : 1539110700_Tulips.jpg
         * created_by : 76
         * updated_by : 0
         * created_at : 2018-10-09 18:45:00
         * updated_at : 2018-10-09 18:45:00
         * image : https://php6.shaligraminfotech.com/public/images/certifications/76/1539110700_Tulips.jpg
         */

        private int id;
        private String user_id;
        private String name;
        private String created_by;
        private String updated_by;
        private String created_at;
        private String updated_at;
        private String image;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCreated_by() {
            return created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public String getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(String updated_by) {
            this.updated_by = updated_by;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
