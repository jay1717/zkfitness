package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class TransformationListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":10,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005853_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005853_Tulips.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:37:33","updated_at":"2018-10-08 13:37:33","deleted_at":null},{"id":11,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005865_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005865_Koala.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:37:45","updated_at":"2018-10-08 13:37:45","deleted_at":null},{"id":12,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005880_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005880_Koala.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:38:00","updated_at":"2018-10-08 13:38:00","deleted_at":null},{"id":13,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005901_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005901_Koala.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:38:21","updated_at":"2018-10-08 13:38:21","deleted_at":null},{"id":14,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005972_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005972_Koala.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:39:32","updated_at":"2018-10-08 13:39:32","deleted_at":null},{"id":15,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005980_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005980_Koala.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:39:41","updated_at":"2018-10-08 13:39:41","deleted_at":null},{"id":16,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005999_Penguins.png.png","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539005999_Koala.jpg.jpg","description":"transformation description","created_at":"2018-10-08 13:39:59","updated_at":"2018-10-08 13:39:59","deleted_at":null},{"id":21,"user_id":"76","before_date":"08/10/2016","before_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539160680_1537428805.jpg","after_date":"08/10/2018","after_image":"https://php6.shaligraminfotech.com/public/images/transformations/76/1539160681_biceps.jpg","description":"transformation description","created_at":"2018-10-10 08:38:01","updated_at":"2018-10-10 08:38:01","deleted_at":null}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 10
         * user_id : 76
         * before_date : 08/10/2016
         * before_image : https://php6.shaligraminfotech.com/public/images/transformations/76/1539005853_Penguins.png.png
         * after_date : 08/10/2018
         * after_image : https://php6.shaligraminfotech.com/public/images/transformations/76/1539005853_Tulips.jpg.jpg
         * description : transformation description
         * created_at : 2018-10-08 13:37:33
         * updated_at : 2018-10-08 13:37:33
         * deleted_at : null
         */

        private int id;
        private String user_id;
        private String before_date;
        private String before_image;
        private String after_date;
        private String after_image;
        private String description;
        private String created_at;
        private String updated_at;
        private Object deleted_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getBefore_date() {
            return before_date;
        }

        public void setBefore_date(String before_date) {
            this.before_date = before_date;
        }

        public String getBefore_image() {
            return before_image;
        }

        public void setBefore_image(String before_image) {
            this.before_image = before_image;
        }

        public String getAfter_date() {
            return after_date;
        }

        public void setAfter_date(String after_date) {
            this.after_date = after_date;
        }

        public String getAfter_image() {
            return after_image;
        }

        public void setAfter_image(String after_image) {
            this.after_image = after_image;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }
    }
}
