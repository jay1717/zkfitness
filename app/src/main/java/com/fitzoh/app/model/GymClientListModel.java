package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class GymClientListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":32,"name":"Client","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","email":"phptesting143321018@2gmtail.com","trainer_id":102,"trainer":"nidhi","client_group":"","is_assigned":0,"is_active":1,"start_date":"","allowed_to_change_date":""},{"id":1302,"name":"jinank","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","email":"jinank@client1.com","trainer_id":11,"trainer":"aman","client_group":"","is_assigned":0,"is_active":1,"start_date":"","allowed_to_change_date":""},{"id":1314,"name":"nimit","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","email":"test@hh.hA","trainer_id":1320,"trainer":"hr","client_group":"","is_assigned":0,"is_active":1,"start_date":"","allowed_to_change_date":""},{"id":1323,"name":"jinank","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","email":"jinankd@client.com","trainer_id":1299,"trainer":"jinank","client_group":"","is_assigned":0,"is_active":1,"start_date":"","allowed_to_change_date":""},{"id":1325,"name":"abc","photo":"https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg","email":"absgym@gmail.com","trainer":"","client_group":"","is_assigned":0,"is_active":1,"start_date":"","allowed_to_change_date":""}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 32
         * name : Client
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/default.jpeg
         * email : phptesting143321018@2gmtail.com
         * trainer_id : 102
         * trainer : nidhi
         * client_group :
         * is_assigned : 0
         * is_active : 1
         * start_date :
         * allowed_to_change_date :
         */

        private int id;
        private String name;
        private String photo;
        private String email;
        private int trainer_id;
        private String trainer;
        private String client_group;
        private int is_assigned;
        private int is_active;
        private String start_date;
        private String allowed_to_change_date;
        private int is_profile_completed;



        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getTrainer_id() {
            return trainer_id;
        }

        public void setTrainer_id(int trainer_id) {
            this.trainer_id = trainer_id;
        }

        public String getTrainer() {
            return trainer;
        }

        public void setTrainer(String trainer) {
            this.trainer = trainer;
        }

        public String getClient_group() {
            return client_group;
        }

        public void setClient_group(String client_group) {
            this.client_group = client_group;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }

        public int getIs_active() {
            return is_active;
        }

        public void setIs_active(int is_active) {
            this.is_active = is_active;
        }

        public String getStart_date() {
            return start_date;
        }

        public void setStart_date(String start_date) {
            this.start_date = start_date;
        }

        public String getAllowed_to_change_date() {
            return allowed_to_change_date;
        }

        public void setAllowed_to_change_date(String allowed_to_change_date) {
            this.allowed_to_change_date = allowed_to_change_date;
        }

        public int getIs_profile_completed() {
            return is_profile_completed;
        }

        public void setIs_profile_completed(int is_profile_completed) {
            this.is_profile_completed = is_profile_completed;
        }
    }
}
