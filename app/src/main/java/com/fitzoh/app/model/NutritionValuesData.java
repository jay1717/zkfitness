package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

public class NutritionValuesData {

    /**
     * status : 200
     * message : success
     * data : {"calories":"86 gm","protein":" gm","carbs":"12.12 gm","fat":"2.92 gm"}
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * calories : 86 gm
         * protein :  gm
         * carbs : 12.12 gm
         * fat : 2.92 gm
         */

        @SerializedName("calories")
        private String calories;
        @SerializedName("protein")
        private String protein;
        @SerializedName("carbs")
        private String carbs;
        @SerializedName("fat")
        private String fat;

        public String getCalories() {
            return calories;
        }

        public void setCalories(String calories) {
            this.calories = calories;
        }

        public String getProtein() {
            return protein;
        }

        public void setProtein(String protein) {
            this.protein = protein;
        }

        public String getCarbs() {
            return carbs;
        }

        public void setCarbs(String carbs) {
            this.carbs = carbs;
        }

        public String getFat() {
            return fat;
        }

        public void setFat(String fat) {
            this.fat = fat;
        }
    }
}
