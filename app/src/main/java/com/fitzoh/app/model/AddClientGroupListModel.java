package com.fitzoh.app.model;

import java.util.List;

public class AddClientGroupListModel {

    /**
     * status : 200
     * message : success
     * data : [{"id":2,"name":"Nimit\u2019s Group","client_group_user":[{"id":16,"name":"Client 2","photo":"https://php6.shaligraminfotech.com/fitzoh/public/images/user_images/1544527660_1544527659333.82.png"}]},{"id":5,"name":"test1","client_group_user":[{"id":16,"name":"Client 2","photo":"https://php6.shaligraminfotech.com/fitzoh/public/images/user_images/1544527660_1544527659333.82.png"},{"id":20,"name":"client3","photo":"https://php6.shaligraminfotech.com/fitzoh/public/images/user_images/default.jpeg"}]},{"id":7,"name":"pako nu grp","client_group_user":[]},{"id":8,"name":"test","client_group_user":[]}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 2
         * name : Nimit’s Group
         * client_group_user : [{"id":16,"name":"Client 2","photo":"https://php6.shaligraminfotech.com/fitzoh/public/images/user_images/1544527660_1544527659333.82.png"}]
         */

        private int id;
        private String name;
        private List<AssignedToBean> client_group_user;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<AssignedToBean> getClient_group_user() {
            return client_group_user;
        }

        public void setClient_group_user(List<AssignedToBean> client_group_user) {
            this.client_group_user = client_group_user;
        }
    }
}
