package com.fitzoh.app.model;

import java.util.List;

public class MedicalFormModel {

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        private int id;
        private int type;
        private String question;
        private String user_answer;
        private String user_answer_id;

        private String user_notes;
        private int is_notes_required;
        private List<DataBean.DropdownValuesBean> dropdown_values;
        private int selectedPosition = -1;
        private String notes = "";

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public int getIs_notes_required() {
            return is_notes_required;
        }

        public void setIs_notes_required(int is_notes_required) {
            this.is_notes_required = is_notes_required;
        }

        public List<DataBean.DropdownValuesBean> getDropdown_values() {
            return dropdown_values;
        }

        public void setDropdown_values(List<DataBean.DropdownValuesBean> dropdown_values) {
            this.dropdown_values = dropdown_values;
        }

        public int getSelectedPosition() {
            return selectedPosition;
        }

        public void setSelectedPosition(int selectedPosition) {
            this.selectedPosition = selectedPosition;
        }

        public String getNotes() {
            return notes;
        }


        public String getUser_answer() {
            return user_answer;
        }
        public String getUser_answer_id() {
            return user_answer_id;
        }

        public void setUser_answer_id(String user_answer_id) {
            this.user_answer_id = user_answer_id;
        }


        public void setUser_answer(String user_answer) {
            this.user_answer = user_answer;
        }

        public String getUser_notes() {
            return user_notes;
        }

        public void setUser_notes(String user_notes) {
            this.user_notes = user_notes;
        }


        public void setNotes(String notes) {
            this.notes = notes;
        }

        public static class DropdownValuesBean {

            private int id;
            private String value;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }
}
