package com.fitzoh.app.model;

import java.util.List;

public class ExerciseSetListModel {

    private int status;
    private String message;
    private List<List<DataBean>> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<List<DataBean>> getData() {
        return data;
    }

    public void setData(List<List<DataBean>> data) {
        this.data = data;
    }

    public static class DataBean {

        private String setno;
        private int id = 0;
        private int parameter_id;
        private String parameter_name;
        private String value;
        private String weight_unit = "";

        public String getSetno() {
            return setno;
        }

        public void setSetno(String setno) {
            this.setno = setno;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getParameter_id() {
            return parameter_id;
        }

        public void setParameter_id(int parameter_id) {
            this.parameter_id = parameter_id;
        }

        public String getParameter_name() {
            return parameter_name;
        }

        public void setParameter_name(String parameter_name) {
            this.parameter_name = parameter_name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getWeight_unit() {
            return weight_unit;
        }

        public void setWeight_unit(String weight_unit) {
            this.weight_unit = weight_unit;
        }
    }
}
