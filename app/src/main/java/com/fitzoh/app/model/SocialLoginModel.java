package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

public class SocialLoginModel {


    /**
     * success : 200
     * message : success
     * data : {"id":96,"role_id":"0","name":null,"email":"jinank.thakker1@gmail.com","birth_date":null,"photo":"http://106.201.238.169:4091/images/user_images","user_access_token":"h7ad247cb3f6a008a1b4154b1","is_already_exist":true,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI2ZTA5NDdiYThmMzM0YWY2NDZlM2JiZWE2Zjc5OGNiY2E5OTkwOTU1NmE0YWI0MDVhNDdjZDhlZjE0OTM1Y2VmZmMxMjVkZWUxZTY0NWI4In0.eyJhdWQiOiIxNSIsImp0aSI6ImI2ZTA5NDdiYThmMzM0YWY2NDZlM2JiZWE2Zjc5OGNiY2E5OTkwOTU1NmE0YWI0MDVhNDdjZDhlZjE0OTM1Y2VmZmMxMjVkZWUxZTY0NWI4IiwiaWF0IjoxNTM2OTA5NTQwLCJuYmYiOjE1MzY5MDk1NDAsImV4cCI6MTU2ODQ0NTU0MCwic3ViIjoiOTYiLCJzY29wZXMiOltdfQ.b7kMPg2_9hkWl4odPOOT9hrCEcwoUW5YWyCemV1s1ina8WFmDrGwRSVOlBHvyN3RlAeCroq2_DCXwfUm14YCbBhyrAY-iC0M9LNssUOgDJDY7JEOYuuVXmh_XN0Aw7xxbq4FXlU8lWh7S0edYtwbJGs575kmxiQs-ndbxyOR1W8WALDB-tKsXXQ6TZWIBAqOyTQOXtFZ0A7YMJKeu_OkE73XRnv2xvwHlcmycU6o1VAPKQoSP0E-R-zejGpR9lOugBf273qCSxDC4Ar-gV9hmZhYzWhOjpDuZwpt5dvsVk8EgUfxsOaqhVbdGtXN6x7qXzHbtrHb5Z0ih9dVF1sp6gsU6W4rQ7zIYs5wBFCkqm017YWmm1dDAiVCw_yeV-1qz53bqMtItti-RqZCH6_6j06xgLs3H-4pDq6dF_6iYITH37muRe5mD7YmjQxwM6bcZD9JHP1bbunVfxPEoj0UmeTkGSnDp-21fTYBO6fMtdVmLnaXkdD2O8NjreVd34Y6L_zMPFVkj7JnFAA5lzjtx74wXGqDch-lxEcqT8YU0541MnZ1IVTOWuyMu9H-rPJPxtJ1aFiFwnoRhaeZy27slR8fYmVkMEExGl_VjKsO8heJsKcHBE9mp0Xp6jsI2qkVN-NY_aPyGveRqpf08cEj2OS7qvXx6Hy-VD4s-Sk6IsQ","token_type":"Bearer","expires_at":"2019-09-14 07:19:00"}
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private UserDataModel data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDataModel getData() {
        return data;
    }

    public void setData(UserDataModel data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 96
         * role_id : 0
         * name : null
         * email : jinank.thakker1@gmail.com
         * birth_date : null
         * photo : http://106.201.238.169:4091/images/user_images
         * user_access_token : h7ad247cb3f6a008a1b4154b1
         * is_already_exist : false
         * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI2ZTA5NDdiYThmMzM0YWY2NDZlM2JiZWE2Zjc5OGNiY2E5OTkwOTU1NmE0YWI0MDVhNDdjZDhlZjE0OTM1Y2VmZmMxMjVkZWUxZTY0NWI4In0.eyJhdWQiOiIxNSIsImp0aSI6ImI2ZTA5NDdiYThmMzM0YWY2NDZlM2JiZWE2Zjc5OGNiY2E5OTkwOTU1NmE0YWI0MDVhNDdjZDhlZjE0OTM1Y2VmZmMxMjVkZWUxZTY0NWI4IiwiaWF0IjoxNTM2OTA5NTQwLCJuYmYiOjE1MzY5MDk1NDAsImV4cCI6MTU2ODQ0NTU0MCwic3ViIjoiOTYiLCJzY29wZXMiOltdfQ.b7kMPg2_9hkWl4odPOOT9hrCEcwoUW5YWyCemV1s1ina8WFmDrGwRSVOlBHvyN3RlAeCroq2_DCXwfUm14YCbBhyrAY-iC0M9LNssUOgDJDY7JEOYuuVXmh_XN0Aw7xxbq4FXlU8lWh7S0edYtwbJGs575kmxiQs-ndbxyOR1W8WALDB-tKsXXQ6TZWIBAqOyTQOXtFZ0A7YMJKeu_OkE73XRnv2xvwHlcmycU6o1VAPKQoSP0E-R-zejGpR9lOugBf273qCSxDC4Ar-gV9hmZhYzWhOjpDuZwpt5dvsVk8EgUfxsOaqhVbdGtXN6x7qXzHbtrHb5Z0ih9dVF1sp6gsU6W4rQ7zIYs5wBFCkqm017YWmm1dDAiVCw_yeV-1qz53bqMtItti-RqZCH6_6j06xgLs3H-4pDq6dF_6iYITH37muRe5mD7YmjQxwM6bcZD9JHP1bbunVfxPEoj0UmeTkGSnDp-21fTYBO6fMtdVmLnaXkdD2O8NjreVd34Y6L_zMPFVkj7JnFAA5lzjtx74wXGqDch-lxEcqT8YU0541MnZ1IVTOWuyMu9H-rPJPxtJ1aFiFwnoRhaeZy27slR8fYmVkMEExGl_VjKsO8heJsKcHBE9mp0Xp6jsI2qkVN-NY_aPyGveRqpf08cEj2OS7qvXx6Hy-VD4s-Sk6IsQ
         * token_type : Bearer
         * expires_at : 2019-09-14 07:19:00
         */

        @SerializedName("id")
        private int id;
        @SerializedName("role_id")
        private String roleId;
        @SerializedName("name")
        private Object name;
        @SerializedName("email")
        private String email;
        @SerializedName("birth_date")
        private Object birthDate;
        @SerializedName("photo")
        private String photo;
        @SerializedName("user_access_token")
        private String userAccessToken;
        @SerializedName("is_already_exist")
        private boolean isAlreadyExist;
        @SerializedName("access_token")
        private String accessToken;
        @SerializedName("token_type")
        private String tokenType;
        @SerializedName("expires_at")
        private String expiresAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRoleId() {
            return roleId;
        }

        public void setRoleId(String roleId) {
            this.roleId = roleId;
        }

        public Object getName() {
            return name;
        }

        public void setName(Object name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Object getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(Object birthDate) {
            this.birthDate = birthDate;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getUserAccessToken() {
            return userAccessToken;
        }

        public void setUserAccessToken(String userAccessToken) {
            this.userAccessToken = userAccessToken;
        }

        public boolean isIsAlreadyExist() {
            return isAlreadyExist;
        }

        public void setIsAlreadyExist(boolean isAlreadyExist) {
            this.isAlreadyExist = isAlreadyExist;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public String getExpiresAt() {
            return expiresAt;
        }

        public void setExpiresAt(String expiresAt) {
            this.expiresAt = expiresAt;
        }
    }
}
