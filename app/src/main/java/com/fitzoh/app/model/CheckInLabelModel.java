package com.fitzoh.app.model;

import java.util.List;

public class CheckInLabelModel {


    private int id;
    private String lable;
    private List<SublableBean> sublable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public List<SublableBean> getSublable() {
        return sublable;
    }

    public void setSublable(List<SublableBean> sublable) {
        this.sublable = sublable;
    }

    public static class SublableBean {


        private int id;
        private String main_lable_id;
        private String lable;
        private List<AttributesBean> attributes;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getMain_lable_id() {
            return main_lable_id;
        }

        public void setMain_lable_id(String main_lable_id) {
            this.main_lable_id = main_lable_id;
        }

        public String getLable() {
            return lable;
        }

        public void setLable(String lable) {
            this.lable = lable;
        }

        public List<AttributesBean> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<AttributesBean> attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {


            private int id;
            private String sub_lable_id;
            private String lable;
            private String unit;
            private String value;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSub_lable_id() {
                return sub_lable_id;
            }

            public void setSub_lable_id(String sub_lable_id) {
                this.sub_lable_id = sub_lable_id;
            }

            public String getLable() {
                return lable;
            }

            public void setLable(String lable) {
                this.lable = lable;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }
    }

}
