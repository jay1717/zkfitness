package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ClientProfilePackageList implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":1,"client_id":"17","package_id":"1","transaction_id":"123456","payment_method":"razorpay","payment_status":"completed","created_at":"2018-11-16 11:47:09","updated_at":"2018-11-16 11:47:09","deleted_at":null,"package":{"id":1,"package_name":"package 1","training_program_id":"1","desc":"about for package - edited","packagetype":"2","owner_id":"0","weeks":"2","price":"10.00","discounted_price":"5.00","package_owner":"0","media":"1539170186_Koala.jpg","media_type":"Image","youtube_url":"","is_active":"1","is_deleted":"0","created_by":"1","updated_by":"0","deleted_by":"0","created_at":"2018-10-09 20:20:16","updated_at":"2018-10-09 20:23:19","deleted_at":null}},{"id":2,"client_id":"17","package_id":"1","transaction_id":"123456","payment_method":"razorpay","payment_status":"completed","created_at":"2018-11-16 11:47:42","updated_at":"2018-11-16 11:47:42","deleted_at":null,"package":{"id":1,"package_name":"package 1","training_program_id":"1","desc":"about for package - edited","packagetype":"2","owner_id":"0","weeks":"2","price":"10.00","discounted_price":"5.00","package_owner":"0","media":"1539170186_Koala.jpg","media_type":"Image","youtube_url":"","is_active":"1","is_deleted":"0","created_by":"1","updated_by":"0","deleted_by":"0","created_at":"2018-10-09 20:20:16","updated_at":"2018-10-09 20:23:19","deleted_at":null}}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * client_id : 17
         * package_id : 1
         * transaction_id : 123456
         * payment_method : razorpay
         * payment_status : completed
         * created_at : 2018-11-16 11:47:09
         * updated_at : 2018-11-16 11:47:09
         * deleted_at : null
         * package : {"id":1,"package_name":"package 1","training_program_id":"1","desc":"about for package - edited","packagetype":"2","owner_id":"0","weeks":"2","price":"10.00","discounted_price":"5.00","package_owner":"0","media":"1539170186_Koala.jpg","media_type":"Image","youtube_url":"","is_active":"1","is_deleted":"0","created_by":"1","updated_by":"0","deleted_by":"0","created_at":"2018-10-09 20:20:16","updated_at":"2018-10-09 20:23:19","deleted_at":null}
         */

        private int id;
        private String client_id;
        private String package_id;
        private String transaction_id;
        private String payment_method;
        private String payment_status;
        private String created_at;
        private String updated_at;
        private Object deleted_at;
        @SerializedName("package")
        private PackageBean packageX;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getPackage_id() {
            return package_id;
        }

        public void setPackage_id(String package_id) {
            this.package_id = package_id;
        }

        public String getTransaction_id() {
            return transaction_id;
        }

        public void setTransaction_id(String transaction_id) {
            this.transaction_id = transaction_id;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public void setPayment_method(String payment_method) {
            this.payment_method = payment_method;
        }

        public String getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public PackageBean getPackageX() {
            return packageX;
        }

        public void setPackageX(PackageBean packageX) {
            this.packageX = packageX;
        }

        public static class PackageBean implements Serializable{
            /**
             * id : 1
             * package_name : package 1
             * training_program_id : 1
             * desc : about for package - edited
             * packagetype : 2
             * owner_id : 0
             * weeks : 2
             * price : 10.00
             * discounted_price : 5.00
             * package_owner : 0
             * media : 1539170186_Koala.jpg
             * media_type : Image
             * youtube_url :
             * is_active : 1
             * is_deleted : 0
             * created_by : 1
             * updated_by : 0
             * deleted_by : 0
             * created_at : 2018-10-09 20:20:16
             * updated_at : 2018-10-09 20:23:19
             * deleted_at : null
             */

            private int id;
            private String package_name;
            private String training_program_id;
            private String desc;
            private String packagetype;
            private String owner_id;
            private String weeks;
            private String price;
            private String discounted_price;
            private String package_owner;
            private String media;
            private String media_type;
            private String youtube_url;
            private String is_active;
            private String is_deleted;
            private String created_by;
            private String updated_by;
            private String deleted_by;
            private String created_at;
            private String updated_at;
            private Object deleted_at;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPackage_name() {
                return package_name;
            }

            public void setPackage_name(String package_name) {
                this.package_name = package_name;
            }

            public String getTraining_program_id() {
                return training_program_id;
            }

            public void setTraining_program_id(String training_program_id) {
                this.training_program_id = training_program_id;
            }

            public String getDesc() {
                return desc;
            }

            public void setDesc(String desc) {
                this.desc = desc;
            }

            public String getPackagetype() {
                return packagetype;
            }

            public void setPackagetype(String packagetype) {
                this.packagetype = packagetype;
            }

            public String getOwner_id() {
                return owner_id;
            }

            public void setOwner_id(String owner_id) {
                this.owner_id = owner_id;
            }

            public String getWeeks() {
                return weeks;
            }

            public void setWeeks(String weeks) {
                this.weeks = weeks;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDiscounted_price() {
                return discounted_price;
            }

            public void setDiscounted_price(String discounted_price) {
                this.discounted_price = discounted_price;
            }

            public String getPackage_owner() {
                return package_owner;
            }

            public void setPackage_owner(String package_owner) {
                this.package_owner = package_owner;
            }

            public String getMedia() {
                return media;
            }

            public void setMedia(String media) {
                this.media = media;
            }

            public String getMedia_type() {
                return media_type;
            }

            public void setMedia_type(String media_type) {
                this.media_type = media_type;
            }

            public String getYoutube_url() {
                return youtube_url;
            }

            public void setYoutube_url(String youtube_url) {
                this.youtube_url = youtube_url;
            }

            public String getIs_active() {
                return is_active;
            }

            public void setIs_active(String is_active) {
                this.is_active = is_active;
            }

            public String getIs_deleted() {
                return is_deleted;
            }

            public void setIs_deleted(String is_deleted) {
                this.is_deleted = is_deleted;
            }

            public String getCreated_by() {
                return created_by;
            }

            public void setCreated_by(String created_by) {
                this.created_by = created_by;
            }

            public String getUpdated_by() {
                return updated_by;
            }

            public void setUpdated_by(String updated_by) {
                this.updated_by = updated_by;
            }

            public String getDeleted_by() {
                return deleted_by;
            }

            public void setDeleted_by(String deleted_by) {
                this.deleted_by = deleted_by;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }

            public Object getDeleted_at() {
                return deleted_at;
            }

            public void setDeleted_at(Object deleted_at) {
                this.deleted_at = deleted_at;
            }
        }
    }
}
