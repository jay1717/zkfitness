package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class AchivementModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : ["Loose Weight"]
     */

    private int status;
    private String message;
    private List<String> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
