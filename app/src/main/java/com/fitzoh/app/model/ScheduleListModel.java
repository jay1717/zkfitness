package com.fitzoh.app.model;

import java.io.Serializable;
import java.util.List;

public class ScheduleListModel implements Serializable {


    /**
     * status : 200
     * message : success
     * data : [{"id":5,"day":"monday","start_time":"10:00:00","end_time":"12:00:00"},{"id":6,"day":"tuesday","start_time":"10:00:00","end_time":"12:00:00"},{"id":7,"day":"wednesday","start_time":"10:00:00","end_time":"12:00:00"}]
     */

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 5
         * day : monday
         * start_time : 10:00:00
         * end_time : 12:00:00
         */

        private int id;
        private String day;
        private String start_time;
        private String end_time;

        private List<AssignedToBean> assigned_to;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }

        public List<AssignedToBean> getAssigned_to() {
            return assigned_to;
        }

        public void setAssigned_to(List<AssignedToBean> assigned_to) {
            this.assigned_to = assigned_to;
        }

    }
}
