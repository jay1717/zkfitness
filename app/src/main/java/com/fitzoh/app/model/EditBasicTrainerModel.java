package com.fitzoh.app.model;

import com.google.gson.annotations.SerializedName;

public class EditBasicTrainerModel {
    /**
     * status : 200
     * message : success
     * data : {"photo":"https://php6.shaligraminfotech.com/public/images/user_images/1540457623_cropped986757101.jpg"}
     */

    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/1540457623_cropped986757101.jpg
         */

        @SerializedName("photo")
        private String photo;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
