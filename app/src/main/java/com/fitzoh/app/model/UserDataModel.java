package com.fitzoh.app.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * id : 65
 * role_id : 0
 * name : TesternewShaligram
 * email : testshaligram@gmail.com
 * birth_date : null
 * photo : null
 */
public class UserDataModel {

    /**
     * id : 96
     * role_id : 0
     * name : null
     * email : jinank.thakker1@gmail.com
     * birth_date : null
     * "mobile_number":"",
     * "country_code":"+1",
     * "gender":"m",
     * photo : http://106.201.238.169:4091/images/user_images
     * user_access_token : h7ad247cb3f6a008a1b4154b1
     * is_already_exist : true
     * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI2ZTA5NDdiYThmMzM0YWY2NDZlM2JiZWE2Zjc5OGNiY2E5OTkwOTU1NmE0YWI0MDVhNDdjZDhlZjE0OTM1Y2VmZmMxMjVkZWUxZTY0NWI4In0.eyJhdWQiOiIxNSIsImp0aSI6ImI2ZTA5NDdiYThmMzM0YWY2NDZlM2JiZWE2Zjc5OGNiY2E5OTkwOTU1NmE0YWI0MDVhNDdjZDhlZjE0OTM1Y2VmZmMxMjVkZWUxZTY0NWI4IiwiaWF0IjoxNTM2OTA5NTQwLCJuYmYiOjE1MzY5MDk1NDAsImV4cCI6MTU2ODQ0NTU0MCwic3ViIjoiOTYiLCJzY29wZXMiOltdfQ.b7kMPg2_9hkWl4odPOOT9hrCEcwoUW5YWyCemV1s1ina8WFmDrGwRSVOlBHvyN3RlAeCroq2_DCXwfUm14YCbBhyrAY-iC0M9LNssUOgDJDY7JEOYuuVXmh_XN0Aw7xxbq4FXlU8lWh7S0edYtwbJGs575kmxiQs-ndbxyOR1W8WALDB-tKsXXQ6TZWIBAqOyTQOXtFZ0A7YMJKeu_OkE73XRnv2xvwHlcmycU6o1VAPKQoSP0E-R-zejGpR9lOugBf273qCSxDC4Ar-gV9hmZhYzWhOjpDuZwpt5dvsVk8EgUfxsOaqhVbdGtXN6x7qXzHbtrHb5Z0ih9dVF1sp6gsU6W4rQ7zIYs5wBFCkqm017YWmm1dDAiVCw_yeV-1qz53bqMtItti-RqZCH6_6j06xgLs3H-4pDq6dF_6iYITH37muRe5mD7YmjQxwM6bcZD9JHP1bbunVfxPEoj0UmeTkGSnDp-21fTYBO6fMtdVmLnaXkdD2O8NjreVd34Y6L_zMPFVkj7JnFAA5lzjtx74wXGqDch-lxEcqT8YU0541MnZ1IVTOWuyMu9H-rPJPxtJ1aFiFwnoRhaeZy27slR8fYmVkMEExGl_VjKsO8heJsKcHBE9mp0Xp6jsI2qkVN-NY_aPyGveRqpf08cEj2OS7qvXx6Hy-VD4s-Sk6IsQ
     * token_type : Bearer
     * expires_at : 2019-09-14 07:19:00
     */

    @SerializedName("id")
    private int id;
    @SerializedName("role_id")
    private String roleId;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile_number")
    private String mobile_number;
    @SerializedName("country_code")
    private String country_code;
    @SerializedName("gender")
    private String gender;

    @SerializedName("birth_date")
    private Object birthDate;
    @SerializedName("photo")
    private String photo;


    @SerializedName("owner_image")
    private String owner_image;

    @SerializedName("user_access_token")
    private String userAccessToken;
    @SerializedName("is_already_exist")
    private boolean isAlreadyExist;

    @SerializedName("is_subscription")
    private int is_subscription;

    @SerializedName("subscription_id")
    private int subscription_id;

    @SerializedName("is_verified")
    private int is_verified;
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("expires_at")
    private String expiresAt;
    @SerializedName("is_profile_completed")
    private int isProfileCompleted;
    @SerializedName("is_medical_form_completed")
    private int isMedicalFormCompleted;
    @SerializedName("gym_trainer")
    private boolean gym_trainer;
    @SerializedName("client_trainer")
    private boolean client_trainer;
    @SerializedName("gym_id")
    private String gym_id;
    @SerializedName("trainer_id")
    private String trainer_id;
    @SerializedName("direct_gym_client")
    private boolean direct_gym_client;
    @SerializedName("direct_gym_id")
    private String direct_gym_id;
    @SerializedName("primary_color_code")
    private String primary_color_code;
    @SerializedName("secondary_color_code")
    private String secondary_color_code;

    @SerializedName("goal")
    private String goal;


    @SerializedName("subscription_continue")
    private int subscription_continue;

    @SerializedName("fitness_intrested")
    private List<FitnessValuesModel.DataBean> fitness_intrested;

    @SerializedName("address")
    private String address;

    @SerializedName("life_style")
    private String life_style;

    @SerializedName("about")
    private String about;

    @SerializedName("city")
    private String city;

    @SerializedName("state")
    private String state;

    @SerializedName("country")
    private String country;

    @SerializedName("landmark")
    private String landmark;

    @SerializedName("location")
    private String location;
    @SerializedName("currency")
    private String currency = "";
    @SerializedName("weight_type")
    private String weight_type = "";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Object birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getUserAccessToken() {
        return userAccessToken;
    }

    public void setUserAccessToken(String userAccessToken) {
        this.userAccessToken = userAccessToken;
    }

    public boolean isIsAlreadyExist() {
        return isAlreadyExist;
    }

    public void setIsAlreadyExist(boolean isAlreadyExist) {
        this.isAlreadyExist = isAlreadyExist;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(String expiresAt) {
        this.expiresAt = expiresAt;
    }

    public int isIs_verified() {
        return is_verified;
    }

    public void setIs_verified(int is_verified) {
        this.is_verified = is_verified;
    }

    public int getIsProfileCompleted() {
        return isProfileCompleted;
    }

    public void setIsProfileCompleted(int isProfileCompleted) {
        this.isProfileCompleted = isProfileCompleted;
    }

    public int getIsMedicalFormCompleted() {
        return isMedicalFormCompleted;
    }

    public void setIsMedicalFormCompleted(int isMedicalFormCompleted) {
        this.isMedicalFormCompleted = isMedicalFormCompleted;
    }

    public int getSubscription_id() {
        return subscription_id;
    }

    public void setSubscription_id(int subscription_id) {
        this.subscription_id = subscription_id;
    }

    public int isIs_subscription() {
        return is_subscription;
    }

    public void setIs_subscription(int is_subscription) {
        this.is_subscription = is_subscription;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isGym_trainer() {
        return gym_trainer;
    }

    public void setGym_trainer(boolean gym_trainer) {
        this.gym_trainer = gym_trainer;
    }

    public boolean isClient_trainer() {
        return client_trainer;
    }

    public void setClient_trainer(boolean client_trainer) {
        this.client_trainer = client_trainer;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getTrainer_id() {
        return trainer_id;
    }

    public void setTrainer_id(String trainer_id) {
        this.trainer_id = trainer_id;
    }

    public boolean isDirect_gym_client() {
        return direct_gym_client;
    }

    public void setDirect_gym_client(boolean direct_gym_client) {
        this.direct_gym_client = direct_gym_client;
    }

    public String getDirect_gym_id() {
        return direct_gym_id;
    }

    public void setDirect_gym_id(String direct_gym_id) {
        this.direct_gym_id = direct_gym_id;
    }

    public String getPrimary_color_code() {
        return primary_color_code;
    }

    public void setPrimary_color_code(String primary_color_code) {
        this.primary_color_code = primary_color_code;
    }

    public String getSecondary_color_code() {
        return secondary_color_code;
    }

    public void setSecondary_color_code(String secondary_color_code) {
        this.secondary_color_code = secondary_color_code;
    }


    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public List<FitnessValuesModel.DataBean> getFitness_intrested() {
        return fitness_intrested;
    }

    public void setFitness_intrested(List<FitnessValuesModel.DataBean> fitness_intrested) {
        this.fitness_intrested = fitness_intrested;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLife_style() {
        return life_style;
    }

    public void setLife_style(String life_style) {
        this.life_style = life_style;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getWeight_type() {
        return weight_type;
    }

    public void setWeight_type(String weight_type) {
        this.weight_type = weight_type;
    }

    public int getSubscription_continue() {
        return subscription_continue;
    }

    public void setSubscription_continue(int subscription_continue) {
        this.subscription_continue = subscription_continue;
    }
    public String getOwner_image() {
        return owner_image;
    }

    public void setOwner_image(String owner_image) {
        this.owner_image = owner_image;
    }

}
