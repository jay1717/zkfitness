package com.fitzoh.app.model;

import java.util.List;

public class ClientGroupListModel {

    private int status;
    private String message;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {

        private int id;
        private String name;
        private int is_assigned;
        private List<ClientGroupUsersBean> client_group_users;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getIs_assigned() {
            return is_assigned;
        }

        public void setIs_assigned(int is_assigned) {
            this.is_assigned = is_assigned;
        }

        public List<ClientGroupUsersBean> getClient_group_users() {
            return client_group_users;
        }

        public void setClient_group_users(List<ClientGroupUsersBean> client_group_users) {
            this.client_group_users = client_group_users;
        }

        public static class ClientGroupUsersBean {

            private int id;
            private int client_group_id;
            private int user_id;
            private UsersBean users;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getClient_group_id() {
                return client_group_id;
            }

            public void setClient_group_id(int client_group_id) {
                this.client_group_id = client_group_id;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public UsersBean getUsers() {
                return users;
            }

            public void setUsers(UsersBean users) {
                this.users = users;
            }

            public static class UsersBean {

                private int id;
                private String name;
                private String photo;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getPhoto() {
                    return photo;
                }

                public void setPhoto(String photo) {
                    this.photo = photo;
                }
            }
        }
    }
}
