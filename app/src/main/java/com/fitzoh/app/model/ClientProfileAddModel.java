package com.fitzoh.app.model;

import java.io.Serializable;

public class ClientProfileAddModel implements Serializable {


    /**
     * status : 200
     * message : Profile updated successfully!
     * data : {"id":83,"role_id":"1","name":"hiren dixit","email":"hiren.d@sgit.in","birth_date":"2018-09-12","photo":"https://php6.shaligraminfotech.com/public/images/user_images/1540366387_Untitled.png"}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 83
         * role_id : 1
         * name : hiren dixit
         * email : hiren.d@sgit.in
         * birth_date : 2018-09-12
         * photo : https://php6.shaligraminfotech.com/public/images/user_images/1540366387_Untitled.png
         */

        private int id;
        private String role_id;
        private String name;
        private String email;
        private String birth_date;
        private String photo;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBirth_date() {
            return birth_date;
        }

        public void setBirth_date(String birth_date) {
            this.birth_date = birth_date;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }
}
