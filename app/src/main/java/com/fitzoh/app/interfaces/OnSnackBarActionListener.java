package com.fitzoh.app.interfaces;


public interface OnSnackBarActionListener {
    void onAction();
}
