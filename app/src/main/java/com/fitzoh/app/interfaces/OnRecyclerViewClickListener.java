package com.fitzoh.app.interfaces;


public interface OnRecyclerViewClickListener {
    void onItemClicked(int position);

}
