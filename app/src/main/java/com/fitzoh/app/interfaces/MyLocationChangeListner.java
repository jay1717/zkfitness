package com.fitzoh.app.interfaces;

import android.location.Location;

public interface MyLocationChangeListner {
    void onLocationChange(Location location);
}
