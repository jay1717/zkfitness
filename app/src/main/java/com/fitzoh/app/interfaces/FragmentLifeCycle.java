package com.fitzoh.app.interfaces;

public interface FragmentLifeCycle {
    void onPauseFragment();

    void onResumeFragment();
}
