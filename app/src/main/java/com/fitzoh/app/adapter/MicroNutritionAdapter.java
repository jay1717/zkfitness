package com.fitzoh.app.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemMicronutritionListBinding;

import java.util.List;

public class MicroNutritionAdapter extends RecyclerView.Adapter<MicroNutritionAdapter.DataViewHolder> {


    List<String> microNutrition;

    public MicroNutritionAdapter(List<String> microNutrition) {
        this.microNutrition = microNutrition;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemMicronutritionListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_micronutrition_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(microNutrition.get(position).toString());

    }

    @Override
    public int getItemCount() {
        return microNutrition.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemMicronutritionListBinding mBinding;

        public DataViewHolder(ItemMicronutritionListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }
    }
}
