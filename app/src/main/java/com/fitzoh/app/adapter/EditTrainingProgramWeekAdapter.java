package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemEditTrainingProgramWeekBinding;
import com.fitzoh.app.model.TrainingProgramDay;
import com.fitzoh.app.model.TrainingProgramWeek;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class EditTrainingProgramWeekAdapter extends RecyclerView.Adapter<EditTrainingProgramWeekAdapter.DataViewHolder> {

    public ArrayList<TrainingProgramWeek> weekList = new ArrayList<>();
    Activity context;
    EditTrainingProgramDayAdapter.TrainingProgramListener listener;
    FindTrainingProgramWeekAdapter.RedirectToDetailListener redirectListner;

    public EditTrainingProgramWeekAdapter(Activity context, EditTrainingProgramDayAdapter.TrainingProgramListener listener,FindTrainingProgramWeekAdapter.RedirectToDetailListener redirectListner) {
        this.context = context;
        this.listener = listener;
        this.redirectListner=redirectListner;
    }

    public void addNewWeek() {
        TrainingProgramWeek trainingProgramWeek = new TrainingProgramWeek();
        trainingProgramWeek.setWeek_number("Week" + weekList.size() + 1);
        List<TrainingProgramDay> days = new ArrayList<>();
        for(int i=0;i<7;i++){
            TrainingProgramDay day = new TrainingProgramDay();
            day.setDay_number("Day " + (i+1));
            days.add(new TrainingProgramDay());
           // weekList.get().setWeekdays(trainingProgramDayList);
        }
        trainingProgramWeek.setWeekdays(days);
        weekList.add(trainingProgramWeek);
        notifyItemChanged(weekList.size() - 1);
        listener.refresh(-1);
    }

    public void addDataToAdapter(ArrayList<TrainingProgramWeek> data) {
        weekList = data;
        notifyDataSetChanged();
        listener.refresh(-1);
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemEditTrainingProgramWeekBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_edit_training_program_week, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return weekList.size();
    }

    public void setNutrition(Intent data) {
        if (weekList != null && weekList.size() > 0) {
            TrainingProgramWeek week = weekList.get(data.getIntExtra("week", 0));
            List<TrainingProgramDay> weekDays = week.getWeekdays();
            TrainingProgramDay day = weekDays.get(data.getIntExtra("day", 0));
            day.setDiet_plan_id(data.getIntExtra("id", 0));
            day.setDiet_plan_name(data.getStringExtra("name"));
            day.setIs_restfull_day(0);
            weekDays.set(data.getIntExtra("day", 0), day);
            weekList.set(data.getIntExtra("week", 0), week);
            notifyItemChanged(data.getIntExtra("week", 0), week);
        }

    }

    public void setPMWorkout(Intent data) {
        if (weekList != null && weekList.size() > 0) {
            TrainingProgramWeek week = weekList.get(data.getIntExtra("week", 0));
            List<TrainingProgramDay> weekDays = week.getWeekdays();
            TrainingProgramDay day = weekDays.get(data.getIntExtra("day", 0));
            day.setWorkout_pm_id(data.getIntExtra("id", 0));
            day.setWorkout_pm_name(data.getStringExtra("name"));
            day.setIs_restfull_day(0);
            weekDays.set(data.getIntExtra("day", 0), day);
            weekList.set(data.getIntExtra("week", 0), week);
            notifyItemChanged(data.getIntExtra("week", 0), week);
        }

    }

    public void setAMWorkout(Intent data) {
        if (weekList != null && weekList.size() > 0) {
            TrainingProgramWeek week = weekList.get(data.getIntExtra("week", 0));
            List<TrainingProgramDay> weekDays = week.getWeekdays();
            TrainingProgramDay day = weekDays.get(data.getIntExtra("day", 0));
            day.setWorkout_am_id(data.getIntExtra("id", 0));
            day.setIs_restfull_day(0);
            day.setWorkout_am_name(data.getStringExtra("name"));
            weekDays.set(data.getIntExtra("day", 0), day);
            weekList.set(data.getIntExtra("week", 0), week);
            notifyItemChanged(data.getIntExtra("week", 0), week);
        }
    }

    public void setCopyDay(Intent intent) {
        TrainingProgramDay programDay = weekList.get(intent.getIntExtra("week", 0)).getWeekdays().get(intent.getIntExtra("day", 0));
        weekList = intent.getParcelableArrayListExtra("data");
        for (int i = 0; i < weekList.size(); i++) {
            TrainingProgramWeek trainingProgramWeek = weekList.get(i);
            List<TrainingProgramDay> weekListDatas = weekList.get(i).getWeekdays();
            for (int j = 0; j < weekListDatas.size(); j++) {
                if (weekListDatas.get(j).isSelected() == 1) {
                    TrainingProgramDay pday = new TrainingProgramDay();
                    try {
                        pday = (TrainingProgramDay) programDay.clone();
                        pday.setId(weekListDatas.get(j).getId());
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                    weekListDatas.set(j, pday);
                }
                trainingProgramWeek.setWeekdays(weekListDatas);
            }
            weekList.set(i, trainingProgramWeek);
        }
        notifyDataSetChanged();
    }

    public void copyWeek(int fromId, int toId) {
        TrainingProgramWeek trainingProgramWeekOld = weekList.get(toId);
       /* for (int i = 0; i < trainingProgramWeekOld.getWeekdays().size(); i++) {
            if (trainingProgramWeekOld.getWeekdays().get(i).getId() != 0) {
                listener.delete("day", trainingProgramWeekOld.getWeekdays().get(i).getId());
            }
        }*/
        List<TrainingProgramDay> weekDaysList = weekList.get(fromId).getWeekdays();
        for (int i = 0; i < weekDaysList.size(); i++) {
            TrainingProgramDay day = weekDaysList.get(i);
            day.setId(trainingProgramWeekOld.getWeekdays().get(i).getId());
            weekDaysList.set(i, day);
        }
        trainingProgramWeekOld.setWeekdays(weekDaysList);
        weekList.set(toId, trainingProgramWeekOld);
        notifyDataSetChanged();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemEditTrainingProgramWeekBinding mBinding;

        public DataViewHolder(ItemEditTrainingProgramWeekBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgDelete, R.drawable.ic_delete_green);
            Utils.setImageBackground(context, mBinding.imgCopy, R.drawable.ic_copy);
            Utils.setImageBackground(context, mBinding.imgAdd, R.drawable.ic_plus_training_program);
            mBinding.imgAdd.setVisibility(View.GONE);
          //  addDayInWeek(getAdapterPosition());

            mBinding.weekName.setOnClickListener(view -> {
                if (mBinding.recyclerView.getVisibility() == View.VISIBLE) {
                    mBinding.recyclerView.setVisibility(View.GONE);
                } else {

                    mBinding.recyclerView.setVisibility(View.VISIBLE);
                }
            });
           /* mBinding.imgAdd.setOnClickListener(view -> {
                addDayInWeek(getAdapterPosition());
            });*/
            mBinding.imgDelete.setOnClickListener(view -> {
                weekList.remove(getAdapterPosition());
//                notifyItemRemoved(weekList.size() - 1);
                notifyDataSetChanged();
            });
            mBinding.imgCopy.setOnClickListener(view -> {
                /*TrainingProgramWeek programWeek = new TrainingProgramWeek();
                TrainingProgramWeek week = weekList.get(getAdapterPosition());
                programWeek.setWeek_number("" + weekList.size());
                programWeek.setId(0);
                programWeek.setTraining_plan_id(week.getTraining_plan_id());
                List<TrainingProgramDay> days = new ArrayList<>();
                for (TrainingProgramDay day : week.getWeekdays()) {
                    TrainingProgramDay dayNew = new TrainingProgramDay();
                    dayNew.setId(0);
                    dayNew.setIs_restfull_day(day.getIs_restfull_day());
                    dayNew.setTraining_week_id(day.getTraining_week_id());
                    dayNew.setDay_number(day.getDay_number());
                    dayNew.setDiet_plan_id(day.getDiet_plan_id());
                    dayNew.setWorkout_am_id(day.getWorkout_am_id());
                    dayNew.setWorkout_pm_id(day.getWorkout_pm_id());
                    dayNew.setDiet_plan_name(day.getDiet_plan_name());
                    dayNew.setWorkout_am_name(day.getWorkout_am_name());
                    dayNew.setWorkout_pm_name(day.getWorkout_pm_name());
                    days.add(dayNew);
                }
                programWeek.setWeekdays(days);
                weekList.add(programWeek);
                notifyItemChanged(weekList.size());*/
                listener.showPopup(getAdapterPosition(), mBinding.imgCopy);
            });
            mBinding.deleteLayout.setOnClickListener(v -> {
                mBinding.swipeLayout.close(false);
                listener.delete("week", weekList.get(getAdapterPosition()).getId());
                weekList.remove(getAdapterPosition());
                listener.refresh(-1);
//                notifyItemRemoved(weekList.size() - 1);
                notifyDataSetChanged();
                Toast.makeText(context, R.string.Week_deleted_successfully, Toast.LENGTH_LONG).show();
            });

        }

        public void bind(int position) {
            mBinding.weekName.setText("Week " + (position + 1));
            if (weekList.get(position).getWeekdays() != null && weekList.get(position).getWeekdays().size() > 0) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new EditTrainingProgramDayAdapter(context, weekList.get(position).getWeekdays(), position, listener,redirectListner));

            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }

    public void addDayInWeek(int adapterPosition) {
        List<TrainingProgramDay> trainingProgramDayList=new ArrayList<>();
        for(int i=0;i<7;i++){
            TrainingProgramDay day = new TrainingProgramDay();
            day.setDay_number("Day " + (i+1));
            trainingProgramDayList.add(new TrainingProgramDay());
            weekList.get(adapterPosition).setWeekdays(trainingProgramDayList);
        }
        notifyDataSetChanged();

      /*  List<TrainingProgramDay> trainingProgramDayList = weekList.get(position).getWeekdays();
        if (trainingProgramDayList.size() < 7) {
            TrainingProgramDay day = new TrainingProgramDay();
            day.setDay_number("Day " + trainingProgramDayList.size() + 1);
            trainingProgramDayList.add(new TrainingProgramDay());
            weekList.get(position).setWeekdays(trainingProgramDayList);
            notifyItemChanged(position);
        } else {
            Toast.makeText(context, "You can not add more than 7 days in week", Toast.LENGTH_LONG).show();
        }*/
    }

}
