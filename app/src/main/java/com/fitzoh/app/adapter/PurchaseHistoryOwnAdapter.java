package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemPurchaseHistoryOwnBinding;
import com.fitzoh.app.model.PurchaseHistoryTrainerModel;

import java.util.List;

public class PurchaseHistoryOwnAdapter extends RecyclerView.Adapter<PurchaseHistoryOwnAdapter.DataViewHolder> {


    List<PurchaseHistoryTrainerModel.DataBean.OwnPlanBean> packageList;
    Context context;

    public PurchaseHistoryOwnAdapter(List<PurchaseHistoryTrainerModel.DataBean.OwnPlanBean> packageList, Context context) {
        this.packageList = packageList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPurchaseHistoryOwnBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_purchase_history_own, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(packageList.get(position).getPlan_name());
        holder.mBinding.txtDate.setText(packageList.get(position).getStart_date());
        holder.mBinding.txtPrice.setText("Price : " + packageList.get(position).getPrice());

    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemPurchaseHistoryOwnBinding mBinding;

        public DataViewHolder(ItemPurchaseHistoryOwnBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

    }
}
