package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemImagesBinding;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataViewHolder> {

    private Context context;
    private List<String> imagesList;
    boolean isEdit;

    public ImagesAdapter(Context context, List<String> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ItemImagesBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_images, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int i) {

        Utils.setImagePlaceHolder(context, dataViewHolder.mBinding.image, imagesList.get(i));
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemImagesBinding mBinding;

        public DataViewHolder(ItemImagesBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }
    }


}
