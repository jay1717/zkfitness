package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingClientAdapterBinding;
import com.fitzoh.app.model.TrainingProgramList;

import java.util.List;

public class ProfileTrainingAdapter extends RecyclerView.Adapter<ProfileTrainingAdapter.DataViewHolder> {


    Context context;
    private unAssignClient unAssignClient;
    private List<TrainingProgramList.DataBean> workoutList;


    public ProfileTrainingAdapter(Context context, unAssignClient unassignClient, List<TrainingProgramList.DataBean> workoutList) {
        this.context = context;
        this.workoutList = workoutList;
        this.unAssignClient = unassignClient;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingClientAdapterBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_client_adapter, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        //    holder.mBinding..setText(String.valueOf(workoutList.get(position).get()));
        holder.mBinding.txtNoOfWeeksValue.setText(String.valueOf(workoutList.get(position).getWeeks()));
        holder.mBinding.trainingProgramName.setText(workoutList.get(position).getTitle());


    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }


    public interface unAssignClient {
        void unAssign(TrainingProgramList.DataBean dataBean);

        void startActivity(TrainingProgramList.DataBean dataBean);
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemTrainingClientAdapterBinding mBinding;

        public DataViewHolder(ItemTrainingClientAdapterBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(view -> {

                unAssignClient.startActivity(workoutList.get(getAdapterPosition()));
            });
            mBinding.imgAddUser.setOnClickListener(view -> unAssignClient.unAssign(workoutList.get(getAdapterPosition())));
//            Utils.setImageBackground(context, mBinding.imgMenuUnassign, R.drawable.ic_unassign_clients);
        }

    }


}
