package com.fitzoh.app.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemSubscriptionBinding;
import com.fitzoh.app.model.SubscriptionResultData;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ProfileSubscriptionListAdapter extends RecyclerView.Adapter<ProfileSubscriptionListAdapter.DataViewHolder> {
    Activity context;
    List<SubscriptionResultData.DataBean> subScription;
    public int selectionPostition = 0;
    int subscription_id;

    public ProfileSubscriptionListAdapter(Activity context, List<SubscriptionResultData.DataBean> subScription, int subscription_id) {
        this.context = context;
        this.subScription = subScription;
        this.subscription_id = subscription_id;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSubscriptionBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_subscription, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return subScription.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemSubscriptionBinding mBinding;
        boolean isSelected = false;

        public DataViewHolder(ItemSubscriptionBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.chkSubsription, R.drawable.ic_check);
            mBinding.layoutSubscription.setBackgroundResource(R.color.white);
            mBinding.getRoot().setOnClickListener(this);
            mBinding.chkSubsription.setOnClickListener(this);
        }

        void bind(int position) {
            // use the sparse boolean array to check

            if (subScription.get(position).getIs_subscribe() == 1) {
                mBinding.chkSubsription.setChecked(selectionPostition == position);

            }
            mBinding.chkSubsription.setChecked(selectionPostition == position);
            if (mBinding.chkSubsription.isChecked()) {
                mBinding.chkSubsription.setVisibility(View.VISIBLE);
                mBinding.layoutSubscription.setBackgroundResource(R.drawable.green_bg);
                Utils.setGreenBackground(context, mBinding.layoutSubscription);
            } else {
                mBinding.chkSubsription.setVisibility(View.GONE);
                mBinding.layoutSubscription.setBackgroundResource(R.color.white);
            }

            /*if (!subScription.get(position).isSelected()) {
                mBinding.chkSubsription.setChecked(false);
            } else {ō
                mBinding.chkSubsription.setChecked(true);
            }*/
            mBinding.txtTrainerVersion.setText(subScription.get(position).getName());
            mBinding.txtLimitation.setText("Remaining Days : " + subScription.get(position).getRemain_day());
            mBinding.txtRupee.setText("Rs. " + subScription.get(position).getPrice());
        }

        @Override
        public void onClick(View v) {
            // int adapterPosition = getAdapterPosition();
            selectionPostition = getAdapterPosition();
            notifyDataSetChanged();
           /* if (!subScription.get(adapterPosition).isSelected()) {
                mBinding.chkSubsription.setChecked(true);
                subScription.get(adapterPosition).setSelected(true);
                mBinding.layoutSubscription.setBackgroundResource(R.drawable.green_bg);
                selectionPostition = getAdapterPosition();
            } else {
                mBinding.chkSubsription.setChecked(false);
                subScription.get(adapterPosition).setSelected(false);
                mBinding.layoutSubscription.setBackgroundResource(R.color.white);

            }*/
        }
    }
}
