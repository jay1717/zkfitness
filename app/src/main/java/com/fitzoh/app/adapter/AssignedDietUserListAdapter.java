package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemAssignedUsersBinding;
import com.fitzoh.app.model.AssignedToBean;

import java.util.List;

public class AssignedDietUserListAdapter extends RecyclerView.Adapter<AssignedDietUserListAdapter.DataViewHolder> {

    Context context;
    List<AssignedToBean> listOfUser;

    public AssignedDietUserListAdapter(Context context, List<AssignedToBean> listOfUser) {
        this.context = context;
        this.listOfUser = listOfUser;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAssignedUsersBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_assigned_users, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(listOfUser.get(position));
    }

    @Override
    public int getItemCount() {
        return listOfUser.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemAssignedUsersBinding mBinding;

        public DataViewHolder(ItemAssignedUsersBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

        public void bind(AssignedToBean assignedToBean) {
            mBinding.setItem(assignedToBean);
            mBinding.executePendingBindings();
        }
    }
}
