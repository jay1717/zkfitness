package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientDocumentBinding;
import com.fitzoh.app.ui.activity.ClientDocumentViewActivity;
import com.fitzoh.app.utils.Utils;

import java.io.File;
import java.util.List;

public class ClientDocumentAdapter extends RecyclerView.Adapter<ClientDocumentAdapter.DataViewHolder> {

    Context context;
    private List<String> documentList;
    private boolean isDownload = false;
    private DownloadListener listener;

    public ClientDocumentAdapter(Context context, List<String> documentList, boolean isDownload, DownloadListener listener) {
        this.context = context;
        this.documentList = documentList;
        this.isDownload = isDownload;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ClientDocumentAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientDocumentBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_document, parent, false);
        return new DataViewHolder(mBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ClientDocumentAdapter.DataViewHolder holder, int position) {
//        holder.bind(documentList.get(position));
        String path = documentList.get(position);
        String type = path.substring(path.lastIndexOf(".") + 1);
        String fileName = path.substring(path.lastIndexOf('/') + 1);
        holder.mBinding.txtDocumentName.setText(fileName);
        if (type.equalsIgnoreCase("pdf")) {
            holder.mBinding.imgGallery.setImageResource(R.drawable.pdf_logo_placeholder);
        } else {
            Utils.setImagePlaceHolder(context, holder.mBinding.imgGallery, path);
        }
        if (isDownload) {
            if (exists(path)) {
                holder.mBinding.imgDownload.setVisibility(View.GONE);
            } else {
                holder.mBinding.imgDownload.setVisibility(View.VISIBLE);
            }
        } else {
            holder.mBinding.imgDownload.setVisibility(View.GONE);
        }
    }

    private boolean exists(String path) {
        String fileName = path.substring(path.lastIndexOf('/') + 1);
        Log.e("fileName: ", "" + fileName);

        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
        Log.e("File: ", "" + file.exists());
        Log.e("FileNAMAMA: ", file.exists() ? file.getName() : "xyz");

        return file.exists();
    }

    @Override
    public int getItemCount() {
        return documentList.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientDocumentBinding mBinding;

        DataViewHolder(ItemClientDocumentBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(view -> context.startActivity(new Intent(context, ClientDocumentViewActivity.class).putExtra("data", documentList.get(getAdapterPosition()))));
            mBinding.imgDownload.setOnClickListener(view -> {
                if (listener != null) listener.download(documentList.get(getAdapterPosition()));
            });
        }

        public void bind(String dataBean) {
           /* mBinding.setItem(dataBean);
            mBinding.executePendingBindings();*/
        }
    }

    public interface DownloadListener {
        void download(String url);
    }
}
