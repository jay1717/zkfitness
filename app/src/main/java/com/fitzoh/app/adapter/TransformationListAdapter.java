package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTransformationListDltBinding;
import com.fitzoh.app.interfaces.OnRecyclerViewClickListener;
import com.fitzoh.app.model.TransformationListModel;

import java.util.List;

public class TransformationListAdapter extends RecyclerView.Adapter<TransformationListAdapter.DataViewHolder> {

    Context context;
    List<TransformationListModel.DataBean> transformationDataBeanList;
    OnRecyclerViewClickListener listner;
    DeleteTransformation deleteTransformation;

    public TransformationListAdapter(Context context, List<TransformationListModel.DataBean> transformationDataBeanList, OnRecyclerViewClickListener listner,DeleteTransformation deleteTransformation) {
        this.context = context;
        this.transformationDataBeanList = transformationDataBeanList;
        this.listner = listner;
        this.deleteTransformation=deleteTransformation;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemTransformationListDltBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_transformation_list_dlt, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(transformationDataBeanList.get(position));
    }

    @Override
    public int getItemCount() {
        return transformationDataBeanList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTransformationListDltBinding mBinding;

        DataViewHolder(ItemTransformationListDltBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.frontLayout.setOnClickListener(view -> {
                if (listner != null) {
                    listner.onItemClicked(getAdapterPosition());
                }
            });
            mBinding.deleteLayout.setOnClickListener(view->{
                if(deleteTransformation!=null){
                    deleteTransformation.delete(transformationDataBeanList.get(getAdapterPosition()));
                }
            });

        }

        public void bind(TransformationListModel.DataBean transformation) {
            mBinding.setItem(transformation);
            mBinding.executePendingBindings();
        }
    }

    public void removeItem(int position) {
        transformationDataBeanList.remove(position);
        notifyItemRemoved(position);
    }

    public interface DeleteTransformation {
        void delete(TransformationListModel.DataBean dietListModel);
    }
}