package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemMedicalDropdownBinding;
import com.fitzoh.app.databinding.ItemMedicalRadioButtonBinding;
import com.fitzoh.app.model.MedicalFormModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MedicalFormAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_RADIO = 0;
    private static final int ITEM_DROPDOWN = 1;
    private List<MedicalFormModel.DataBean> modelList;
    private Context context;
    private boolean isRegister = false;

    public MedicalFormAdapter(Context context, List<MedicalFormModel.DataBean> modelList, boolean isRegister) {
        this.modelList = modelList;
        this.context = context;
        this.isRegister = isRegister;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_RADIO:
                ItemMedicalRadioButtonBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_medical_radio_button, parent, false);
                return new DataRadioViewHolder(mBinding);
            case ITEM_DROPDOWN:
                ItemMedicalDropdownBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_medical_dropdown, parent, false);
                return new DataDropDownViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (modelList.get(position).getType() == 1) {
            DataRadioViewHolder dataRadioViewHolder = (DataRadioViewHolder) holder;
            dataRadioViewHolder.bind(position);
        } else {
            DataDropDownViewHolder dataDropDownViewHolder = (DataDropDownViewHolder) holder;
            dataDropDownViewHolder.bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public List<MedicalFormModel.DataBean> getData() {
        return modelList;
    }

    class DataRadioViewHolder extends RecyclerView.ViewHolder {
        ItemMedicalRadioButtonBinding mBinding;

        DataRadioViewHolder(ItemMedicalRadioButtonBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setRadioButtonSelectors(context, mBinding.radioYes);
            Utils.setRadioButtonSelectors(context, mBinding.radioNo);
        }

        public void bind(int position) {
            MedicalFormModel.DataBean medicalFormModel = modelList.get(position);
            mBinding.txtTitle.setText(medicalFormModel.getQuestion());

            mBinding.edtMoreInfo.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    MedicalFormModel.DataBean medicalFormModel = modelList.get(position);
                    medicalFormModel.setNotes(mBinding.edtMoreInfo.getText().toString());
                    modelList.set(position, medicalFormModel);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            mBinding.radioSelection.setOnCheckedChangeListener((radioGroup, i) -> {
                MedicalFormModel.DataBean medicalFormModel1 = modelList.get(position);
                if (radioGroup.getCheckedRadioButtonId() == R.id.radio_yes) {
                    medicalFormModel1.setSelectedPosition(1);
                    if (medicalFormModel.getIs_notes_required() == 1) {
                        mBinding.edtMoreInfo.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.edtMoreInfo.setVisibility(View.GONE);
                    }
                } else {
                    medicalFormModel1.setSelectedPosition(0);
                    mBinding.edtMoreInfo.setVisibility(View.GONE);
                }
                modelList.set(position, medicalFormModel1);
            });
            if (!isRegister)
                if (medicalFormModel != null && !medicalFormModel.getUser_answer().equals("0")) {
                    mBinding.radioYes.setChecked(true);
                    if (medicalFormModel.getIs_notes_required() == 1) {
                        mBinding.edtMoreInfo.setVisibility(View.VISIBLE);
                        if (medicalFormModel.getUser_notes() != null && !medicalFormModel.getUser_notes().isEmpty()) {
                            mBinding.edtMoreInfo.setText(medicalFormModel.getUser_notes());
                        }
                    } else {
                        mBinding.edtMoreInfo.setVisibility(View.GONE);
                        mBinding.edtMoreInfo.setVisibility(View.GONE);

                    }
                } else {
                    mBinding.radioNo.setChecked(true);
                }
        }
    }

    class DataDropDownViewHolder extends RecyclerView.ViewHolder {
        ItemMedicalDropdownBinding mBinding;

        DataDropDownViewHolder(ItemMedicalDropdownBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgArrow, R.drawable.ic_down_arrow);
            Utils.setRadioButtonSelectors(context, mBinding.radioYes);
            Utils.setRadioButtonSelectors(context, mBinding.radioNo);
        }

        public void bind(int position) {
            MedicalFormModel.DataBean medicalFormModel = modelList.get(position);
            int alreadySelected = -1;
            List<String> list = new ArrayList<>();
            mBinding.txtTitle.setText(medicalFormModel.getQuestion());
            if (medicalFormModel.getDropdown_values() != null)
                for (int i = 0; i < medicalFormModel.getDropdown_values().size(); i++) {
                    list.add(medicalFormModel.getDropdown_values().get(i).getValue());
                    if (!isRegister && !medicalFormModel.getUser_answer_id().equals("0")) {
                        if (medicalFormModel.getUser_answer_id().equalsIgnoreCase(String.valueOf(medicalFormModel.getDropdown_values().get(i).getId()))) {
                            alreadySelected = i;
                        }
                    }
                }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(mBinding.getRoot().getContext(), R.layout.spinner_item, list);
            mBinding.spinnerMedical.setAdapter(adapter);


            mBinding.spinnerMedical.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.e("onItemSelected: ", "" + mBinding.spinnerMedical.getVisibility());
                    medicalFormModel.setSelectedPosition(mBinding.spinnerMedical.getVisibility() == View.VISIBLE ? i : -1);
                    modelList.set(position, medicalFormModel);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            mBinding.radioSelection.setOnCheckedChangeListener((radioGroup, i) -> {
                MedicalFormModel.DataBean medicalFormModel1 = modelList.get(position);
                Log.e("OnCheckedChangeListener", "" + ((radioGroup.getCheckedRadioButtonId() == R.id.radio_yes) ? "1" : "0"));
                if (radioGroup.getCheckedRadioButtonId() == R.id.radio_yes) {
                    medicalFormModel.setSelectedPosition(mBinding.spinnerMedical.getSelectedItemPosition());
                    mBinding.spinnerMedical.setVisibility(View.VISIBLE);
                    mBinding.imgArrow.setVisibility(View.VISIBLE);
                    mBinding.lineArrow.setVisibility(View.VISIBLE);
                } else {
                    medicalFormModel1.setSelectedPosition(-1);
                    mBinding.spinnerMedical.setVisibility(View.GONE);
                    mBinding.imgArrow.setVisibility(View.GONE);
                    mBinding.lineArrow.setVisibility(View.GONE);
                }
                modelList.set(position, medicalFormModel1);
            });
            if (!isRegister)
                if (!medicalFormModel.getUser_answer_id().equals("0")) {
                    mBinding.radioYes.setChecked(true);
                    mBinding.spinnerMedical.setSelection(alreadySelected);
                    medicalFormModel.setSelectedPosition(alreadySelected);
                    modelList.set(position, medicalFormModel);
                    mBinding.spinnerMedical.setVisibility(View.VISIBLE);
                    mBinding.imgArrow.setVisibility(View.VISIBLE);
                    mBinding.lineArrow.setVisibility(View.VISIBLE);

                } else {
                    mBinding.spinnerMedical.setVisibility(View.GONE);
                    mBinding.imgArrow.setVisibility(View.GONE);
                    mBinding.lineArrow.setVisibility(View.GONE);
                }

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelList.get(position).getType() == 1) {
            return ITEM_RADIO;
        } else {
            return ITEM_DROPDOWN;
        }
    }
}
