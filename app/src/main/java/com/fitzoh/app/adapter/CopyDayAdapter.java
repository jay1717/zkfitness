package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemCopyDayBinding;
import com.fitzoh.app.model.TrainingProgramDay;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class CopyDayAdapter extends RecyclerView.Adapter<CopyDayAdapter.DataViewHolder> {
    private List<TrainingProgramDay> dataList;
    private Context context;
    private CopyDayListener listener;
    private int week = -1;
    int selectedDay,selectedweek;

    public CopyDayAdapter(Context context, List<TrainingProgramDay> dataList, CopyDayListener listener, int week,int selectedDay, int selectedweek) {
        this.dataList = dataList;
        this.context = context;
        this.listener = listener;
        this.week = week;
        this.selectedweek=selectedweek;
        this.selectedDay=selectedDay;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCopyDayBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_copy_day, parent, false);
        return new DataViewHolder(mBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemCopyDayBinding mBinding;

        public DataViewHolder(ItemCopyDayBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkbox, R.drawable.checked);
        }

        public void bind(TrainingProgramDay dataBean) {
            mBinding.dayName.setText("Day " + (getAdapterPosition() + 1));
            if(selectedweek==week && selectedDay==getAdapterPosition()){
                mBinding.checkbox.setEnabled(false);
            }
            else {
                mBinding.checkbox.setEnabled(true);

            }
            mBinding.checkbox.setChecked(dataList.get(getAdapterPosition()).isSelected() == 1);
            mBinding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
               /* Intent intent = new Intent();
                intent.putExtra("data", dataList.get(getAdapterPosition()));
                intent.putExtra("week", ((Activity) context).getIntent().getIntExtra("week", 0));
                intent.putExtra("day", ((Activity) context).getIntent().getIntExtra("day", 0));
                ((Activity) context).setResult(Activity.RESULT_OK, intent);
                ((Activity) context).finish();*/
//                listener.copy(((Activity) context).getIntent().getIntExtra("week", 0), ((Activity) context).getIntent().getIntExtra("day", 0), isChecked);
                listener.copy(week, getAdapterPosition(), isChecked);
            });
        }
    }

    public interface CopyDayListener {
        void copy(int week, int day, boolean isChecked);
    }
}
