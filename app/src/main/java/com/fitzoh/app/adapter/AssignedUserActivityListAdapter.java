package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientActivityAdapterBinding;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class AssignedUserActivityListAdapter extends RecyclerView.Adapter<AssignedUserActivityListAdapter.DataViewHolder> {

    Context context;
    List<String> listOfImage;

    public AssignedUserActivityListAdapter(Context context, List<String> listOfImage) {
        this.context = context;
        this.listOfImage = listOfImage;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientActivityAdapterBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_activity_adapter, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        Utils.setImage(context, holder.mBinding.imgUser, listOfImage.get(position));
    }

    @Override
    public int getItemCount() {
        return listOfImage.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientActivityAdapterBinding mBinding;

        public DataViewHolder(ItemClientActivityAdapterBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

    }
}
