package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemEditWorkoutExerciseBinding;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class EditWorkoutExerciseAdapter extends RecyclerView.Adapter<EditWorkoutExerciseAdapter.DataViewHolder> {
    Context context;
    private EditWorkoutAdapter.onAddClient onAddClient;
    private List<WorkoutExerciseListModel.DataBean> dataList;

    EditWorkoutExerciseAdapter(Context context, EditWorkoutAdapter.onAddClient onAddClient, List<WorkoutExerciseListModel.DataBean> dataList) {
        this.context = context;
        this.onAddClient = onAddClient;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemEditWorkoutExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_edit_workout_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
        holder.mBinding.frontLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddClient.edit(dataList.get(position));
            }
        });
       /* if (position != 0) {
            holder.mBinding.imgMenuHorizon.setVisibility(View.GONE);
        } else {
            holder.mBinding.imgMenuHorizon.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemEditWorkoutExerciseBinding mBinding;

        public DataViewHolder(ItemEditWorkoutExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkboxClientAssign, R.drawable.checked);
            Utils.setImageBackground(context, mBinding.imgMenuHorizon, R.drawable.ic_menu_horizon);
            mBinding.imgMenuHorizon.setOnClickListener(view -> showPopupMenu(mBinding.imgMenuHorizon, getAdapterPosition()));
        }

        public void bind(WorkoutExerciseListModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();

            mBinding.deleteLayout.setOnClickListener(v -> {
                if (dataList.size() > 1) {
                    onAddClient.delete(dataBean.getId(), dataBean.getExercise_set_id());
                } else {
                    onAddClient.deleteExercise(dataBean.getId());
                }
            });
        }
    }


    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.menu_edit_workout, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        MenuItem menuItem = popup.getMenu().getItem(2);
        if (dataList.size() > 1) {
            menuItem.setTitle(context.getString(R.string.break_apart));
        } else {
            menuItem.setTitle(context.getString(R.string.super_giant));
        }
        popup.show();
    }


    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    onAddClient.edit(dataList.get(position));
                    break;

                case R.id.action_view:
                    onAddClient.view(dataList.get(position).getId());
                    break;
                case R.id.action_super:
                    onAddClient.superGiant(dataList.get(position), dataList.size() > 1 ? "break" : "super");
                    break;
            }
            return false;
        }
    }
}
