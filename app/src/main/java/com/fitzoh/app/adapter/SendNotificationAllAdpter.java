package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemSendNotiAllBinding;
import com.fitzoh.app.model.SendNotiGetUserModel;
import com.fitzoh.app.model.Singleton;


import java.util.ArrayList;
import java.util.List;

public class SendNotificationAllAdpter extends RecyclerView.Adapter<SendNotificationAllAdpter.DataViewHolder>{

    Context context;
    public static List<SendNotiGetUserModel.DatumSendNoti> dataBeans = new ArrayList<>();
    private boolean ischeck;
    Singleton x = Singleton.getInstance();

    public SendNotificationAllAdpter(List<SendNotiGetUserModel.DatumSendNoti> dataBeans, Context context, boolean ischeck) {
        this.dataBeans = dataBeans;
        this.context = context;
        this.ischeck = ischeck;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSendNotiAllBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_send_noti_all, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataBeans.get(position));
        holder.mBinding.txtclientName.setText(dataBeans.get(position).getName());
        holder.mBinding.txtclientEmail.setText(dataBeans.get(position).getEmail());

        if (dataBeans.get(holder.getAdapterPosition()).getChecked()) {
            holder.mBinding.checkboxClientAssign.setChecked(true);
        } else {
            holder.mBinding.checkboxClientAssign.setChecked(false);
        }
    }

    public static List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < dataBeans.size(); i++) {
            if (dataBeans.get(i).isChecked())
                list.add(dataBeans.get(i).getId());
        }
        return list;
    }

    public static void selectAll() {
        for (int i = 0; i < dataBeans.size(); i++) {
            dataBeans.get(i).setChecked(true);
        }
    }

    public static void deselectAll() {
        for (int i = 0; i < dataBeans.size(); i++) {
            dataBeans.get(i).setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemSendNotiAllBinding mBinding;

        DataViewHolder(ItemSendNotiAllBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(SendNotiGetUserModel.DatumSendNoti sendNoti) {
            Log.e("Check", "" + sendNoti.isChecked());
            mBinding.checkboxClientAssign.setOnCheckedChangeListener((buttonView, isChecked) -> {
                Log.e("Check Change", "" + isChecked);
                SendNotiGetUserModel.DatumSendNoti data = dataBeans.get(getAdapterPosition());
                data.setChecked(isChecked);
                dataBeans.set(getAdapterPosition(), data);
                if (dataBeans.get(getAdapterPosition()).getChecked() == true)
                SetCheck.setCheck(true);
                else
                SetCheck.setCheck(false);


            });
        }
    }

    public interface SetCheck {
        static void setCheck(boolean check) {
        }
    }
}
