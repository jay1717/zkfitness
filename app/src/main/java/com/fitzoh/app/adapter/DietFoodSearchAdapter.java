package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.RowDietFoodSearchBinding;
import com.fitzoh.app.model.SearchDietPlanFoodData;

import java.util.List;

public class DietFoodSearchAdapter extends
        RecyclerView.Adapter<DietFoodSearchAdapter.ViewHolder> {


    Context context;
    private List<SearchDietPlanFoodData.DataBean> foodDataList;
    onClickItem onClickItem;


    public DietFoodSearchAdapter(Context context, List<SearchDietPlanFoodData.DataBean> foodDataList, onClickItem onClickItem) {
        this.context = context;
        this.foodDataList = foodDataList;
        this.onClickItem = onClickItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowDietFoodSearchBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_diet_food_search, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    /*public List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < clientAssignModels.size(); i++) {
            if (clientAssignModels.get(i).getIs_assigned() == 1)
                list.add(clientAssignModels.get(i).getId());
        }
        return list;
    }*/

    @Override
    public int getItemCount() {
        return foodDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RowDietFoodSearchBinding mBinding;

        public ViewHolder(RowDietFoodSearchBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(int position) {
            mBinding.textFoodName.setText(foodDataList.get(position).getFoodName());
            mBinding.getRoot().setOnClickListener(view -> onClickItem.onClick(foodDataList.get(position)));
        }
    }

    public interface onClickItem {
        void onClick(SearchDietPlanFoodData.DataBean data);
    }

}
