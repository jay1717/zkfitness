package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTodaysDietBinding;
import com.fitzoh.app.model.GetClientDetailModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class TodaysDietAdapter extends RecyclerView.Adapter<TodaysDietAdapter.DataViewHolder> {
    private Context context;
    private List<GetClientDetailModel.DataBean.TodaysDietBean> todaysDietBeans;
    private SelectExerciseListener listener;

    public TodaysDietAdapter(Context context, List<GetClientDetailModel.DataBean.TodaysDietBean> todaysDietBeans, SelectExerciseListener listener) {
        this.context = context;
        this.todaysDietBeans = todaysDietBeans;
        this.listener = listener;
    }

    @NonNull
    @Override

    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTodaysDietBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_todays_diet, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(todaysDietBeans.get(position));
    }

    @Override
    public int getItemCount() {
        return todaysDietBeans.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTodaysDietBinding mBinding;

        public DataViewHolder(ItemTodaysDietBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.getShapeGradient(context, mBinding.btnStartWorkout);
        }

        public void bind(GetClientDetailModel.DataBean.TodaysDietBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            mBinding.btnStartWorkout.setOnClickListener(view -> {
                if (mBinding.recyclerView.getAdapter() != null) {
                    StartDietListAdapter adapter = (StartDietListAdapter) mBinding.recyclerView.getAdapter();
                    ArrayList<GetClientDetailModel.DataBean.TodaysDietBean.FoodBean> data = adapter.getData();
                    List<Integer> list = new ArrayList<>();
                    List<Integer> list1 = new ArrayList<>();
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getIs_assigned() == 1) {
                            list.add(data.get(i).getId());
                        } else {
                            list1.add(data.get(i).getId());
                        }
                    }
                    if (list.size() > 0) {
                        listener.selected(list, list1, todaysDietBeans.get(getAdapterPosition()).getId());
                    } else {
                        listener.selected(null, null, 0);
                    }
                } else {
                    listener.selected(null, null, 0);
                }
            });
            if (dataBean.getFood() != null && dataBean.getFood().size() > 0) {
//                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new StartDietListAdapter(context, dataBean.getFood()));
            } else {
//                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }

    }

    public interface SelectExerciseListener {
        void selected(List<Integer> listCheck, List<Integer> listUncheck, int id);
    }
}
