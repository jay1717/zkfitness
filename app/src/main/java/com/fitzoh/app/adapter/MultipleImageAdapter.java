package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.RowUploadImageBinding;

import java.util.ArrayList;

public class MultipleImageAdapter extends RecyclerView.Adapter<MultipleImageAdapter.DataViewHolder> {


    public ArrayList<Uri> arrayList;
    Context context;
    RemoveImageListener listener;

    public MultipleImageAdapter(ArrayList<Uri> arrayList, RemoveImageListener listener) {
        this.arrayList = arrayList;
        this.context = context;
        this.listener=listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowUploadImageBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_upload_image, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.imgUpload.setImageURI(arrayList.get(position));
        holder.mBinding.imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.removeImage(position);
                   /* arrayList.remove(position);
                    notifyDataSetChanged();*/
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        RowUploadImageBinding mBinding;

        public DataViewHolder(RowUploadImageBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }

    public interface RemoveImageListener {
        void removeImage(int position);
    }
}
