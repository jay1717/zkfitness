package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemGymTrainersClientBinding;
import com.fitzoh.app.model.GymClientListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class GymClientAssigedAdapter extends RecyclerView.Adapter<GymClientAssigedAdapter.DataViewHolder> {


    Context context;
    List<GymClientListModel.DataBean> trainerListModel;
    private onDataModeified onDataModeified;

    public GymClientAssigedAdapter(Context context, List<GymClientListModel.DataBean> trainerListModel, GymClientAssigedAdapter.onDataModeified onDataModeified) {
        this.context = context;
        this.trainerListModel = trainerListModel;
        this.onDataModeified = onDataModeified;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGymTrainersClientBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_gym_trainers_client, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(trainerListModel.get(position).getName());
        holder.mBinding.txtEmail.setText(String.valueOf(trainerListModel.get(position).getEmail()));
        holder.mBinding.txtGroup.setText("Groups : " + trainerListModel.get(position).getClient_group());
        Utils.setImage(context, holder.mBinding.imgUser, trainerListModel.get(position).getPhoto());

        holder.mBinding.frontLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDataModeified.getData(trainerListModel.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return trainerListModel.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemGymTrainersClientBinding mBinding;

        public DataViewHolder(ItemGymTrainersClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }
    }

    public interface onDataModeified {
        void getData(GymClientListModel.DataBean dataBean);
    }


}
