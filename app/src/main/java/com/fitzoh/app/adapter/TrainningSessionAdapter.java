package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainningSessionBinding;
import com.fitzoh.app.model.TrainingSessionModel;
import com.fitzoh.app.ui.activity.ClientProfileActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class TrainningSessionAdapter extends RecyclerView.Adapter<TrainningSessionAdapter.DataViewHolder> {

    private Context context;
        private List<TrainingSessionModel.DataBean> dataBeanList;

    public TrainningSessionAdapter(Context context, List<TrainingSessionModel.DataBean> liveClientsModels) {
        this.context = context;
        this.dataBeanList = liveClientsModels;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @NonNull
    @Override
    public TrainningSessionAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainningSessionBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_trainning_session, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainningSessionAdapter.DataViewHolder holder, int position) {
        holder.bind(dataBeanList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainningSessionBinding mBinding;

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public DataViewHolder(ItemTrainningSessionBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.imgUser.setClipToOutline(true);
        }

        public void bind(TrainingSessionModel.DataBean dataBean) {
            Utils.setImageBackground(context, mBinding.imgStClk, R.drawable.ic_clock);
            Utils.setImageBackground(context, mBinding.imgEndClk, R.drawable.ic_clock);
            Utils.getShapeGradient(context, mBinding.txtWorkoutName);
            Utils.setTextViewStartImage(context, mBinding.txtWorkout, R.drawable.ic_workout, true);
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            mBinding.cvItemTrainingSession.setOnClickListener(view -> { context.startActivity(new Intent(context, ClientProfileActivity.class).putExtra("data", dataBeanList.get(getAdapterPosition())).putExtra("client_type", "3"));
            });
        }
    }
}