package com.fitzoh.app.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemDetailCheckInFormListBinding;
import com.fitzoh.app.model.CheckInLabelModel;

import java.util.ArrayList;
import java.util.List;

public class DetailCheckInFormListAdapter extends RecyclerView.Adapter<DetailCheckInFormListAdapter.DataViewHolder> {

    private List<CheckInLabelModel> checkInLabelModelList = new ArrayList<>();
    Activity context;

    public DetailCheckInFormListAdapter(Activity context) {
        this.context = context;
    }

    public void addDataToAdapter(List<CheckInLabelModel> data) {
        checkInLabelModelList = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDetailCheckInFormListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_detail_check_in_form_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return checkInLabelModelList.size();
    }

    public List<CheckInLabelModel> getData() {
        return checkInLabelModelList;
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemDetailCheckInFormListBinding mBinding;

        public DataViewHolder(ItemDetailCheckInFormListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

        public void bind(int position) {
            mBinding.weekName.setText(checkInLabelModelList.get(position).getLable());
            if (checkInLabelModelList.get(position).getSublable() != null && checkInLabelModelList.get(position).getSublable().size() > 0) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new DetailCheckInFormLabelAdapter(getAdapterPosition(), checkInLabelModelList.get(position).getSublable()));
            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }
}
