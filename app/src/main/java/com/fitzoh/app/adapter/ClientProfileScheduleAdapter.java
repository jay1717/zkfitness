package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientProfileScheduleBinding;
import com.fitzoh.app.model.ScheduleListModel;

import java.util.List;

public class ClientProfileScheduleAdapter extends RecyclerView.Adapter<ClientProfileScheduleAdapter.DataViewHolder> {


    Context context;
    List<ScheduleListModel.DataBean> schedualList;
    private onScheduleUnassign onScheduleUnassign;

    public ClientProfileScheduleAdapter(Context context, List<ScheduleListModel.DataBean> schedualList, ClientProfileScheduleAdapter.onScheduleUnassign onScheduleUnassign) {
        this.context = context;
        this.schedualList = schedualList;
        this.onScheduleUnassign = onScheduleUnassign;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientProfileScheduleBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_profile_schedule, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
        holder.mBinding.scheduleName.setText(schedualList.get(position).getDay());
        holder.mBinding.txtStartTimeValue.setText(String.valueOf(schedualList.get(position).getStart_time()));
        holder.mBinding.txtEndTimeValue.setText(String.valueOf(schedualList.get(position).getEnd_time()));

    }

    @Override
    public int getItemCount() {
        return schedualList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientProfileScheduleBinding mBinding;

        public DataViewHolder(ItemClientProfileScheduleBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

        public void bind(int position) {
            mBinding.imgUnassignUser.setOnClickListener(v -> onScheduleUnassign.unAssign(schedualList.get(position)));

        }
    }

    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -20;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            if (itemPosition == 0) {
                return;
            }
            outRect.set(vertOverlap, 0, 0, 0);


        }
    }

    public interface onScheduleUnassign {
        void unAssign(ScheduleListModel.DataBean dataBean);

    }


}
