package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientHistoryBinding;
import com.fitzoh.app.model.ClientInquiryHistoryModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class TrainerInquiryListAdapter extends RecyclerView.Adapter<TrainerInquiryListAdapter.DataViewHolder> {


    Context context;
    private dataPassing dataPassing;
    private List<ClientInquiryHistoryModel.DataBean> productInquiry;


    public TrainerInquiryListAdapter(Context context, dataPassing dataPassing, List<ClientInquiryHistoryModel.DataBean> productInquiry) {
        this.context = context;
        this.dataPassing = dataPassing;
        this.productInquiry = productInquiry;

    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientHistoryBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_history, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtName.setText(productInquiry.get(position).getProduct_name());
        Utils.setImage(context, holder.mBinding.imgClient, productInquiry.get(position).getProduct_image());
        holder.mBinding.txtCount.setText("Quntity : " + productInquiry.get(position).getQty());
        holder.mBinding.txtPrice.setText("Price : " + productInquiry.get(position).getPrice());

    }

    @Override
    public int getItemCount() {
        return productInquiry.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientHistoryBinding mBinding;

        public DataViewHolder(ItemClientHistoryBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dataPassing.data(productInquiry.get(getAdapterPosition()));
                }
            });

        }

    }

    public interface dataPassing {
        void data(ClientInquiryHistoryModel.DataBean dataBean);

    }

}
