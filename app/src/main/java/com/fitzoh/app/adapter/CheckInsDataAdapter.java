package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemCheckinDataBinding;
import com.fitzoh.app.model.CheckInDataModel;
import com.fitzoh.app.ui.activity.CheckInsCompareActivity;
import com.fitzoh.app.ui.activity.DetailCheckInActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class CheckInsDataAdapter extends RecyclerView.Adapter<CheckInsDataAdapter.DataViewHolder> {


    public List<CheckInDataModel.DataBean> checkInLabelModelList;
    Activity context;

    public CheckInsDataAdapter(List<CheckInDataModel.DataBean> checkInLabelModelList, Activity context) {
        this.checkInLabelModelList = checkInLabelModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCheckinDataBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_checkin_data, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtUploadDate.setText("Uploaded " + checkInLabelModelList.get(position).getCheckin_images().size() + " photos  on " + checkInLabelModelList.get(position).getDate());
        holder.mBinding.imageSlider.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        holder.mBinding.imageSlider.setScrollTimeInSec(2); //set scroll delay in seconds :
        if (checkInLabelModelList.get(position).getCheckin_images() != null && checkInLabelModelList.get(position).getCheckin_images().size() > 0) {
            holder.mBinding.txtImagePlaceholder.setVisibility(View.GONE);
            holder.mBinding.imageSlider.setVisibility(View.VISIBLE);
            int size = checkInLabelModelList.get(position).getCheckin_images().size();

            for (int i = 0; i < size; i++) {

                SliderView sliderView = new SliderView(context);
                sliderView.setImageUrl(checkInLabelModelList.get(position).getCheckin_images().get(i).getImage());
                sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);
                sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(SliderView sliderView) {
                        context.startActivity(new Intent(context, DetailCheckInActivity.class).putExtra("checkin_id", checkInLabelModelList.get(position).getId()));

                    }
                });

                holder.mBinding.imageSlider.addSliderView(sliderView);
            }
        } else {
            holder.mBinding.txtImagePlaceholder.setVisibility(View.VISIBLE);
            holder.mBinding.imageSlider.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return checkInLabelModelList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemCheckinDataBinding mBinding;

        public DataViewHolder(ItemCheckinDataBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(view -> {
                if (!context.getIntent().hasExtra("data1"))
                    context.startActivity(new Intent(context, DetailCheckInActivity.class).putExtra("checkin_id", checkInLabelModelList.get(getAdapterPosition()).getId()));
            });
            Utils.getShapeGradient(context, mBinding.txtCompare);
            mBinding.txtCompare.setOnClickListener(view -> {
                PopupMenu menu = new PopupMenu(context, view);
                for (int i = 0; i < checkInLabelModelList.size(); i++) {
                    if (i != getAdapterPosition()) {
                        menu.getMenu().add(checkInLabelModelList.get(i).getDate());
                    }
                }
                menu.setOnMenuItemClickListener(item -> {
                    if (checkInLabelModelList.size() >= 2) {
                        for (CheckInDataModel.DataBean bean : checkInLabelModelList)
                            if (bean.getDate().equalsIgnoreCase(String.valueOf(item.getTitle()))) {
                                context.startActivity(new Intent(context, CheckInsCompareActivity.class).putExtra("data1", checkInLabelModelList.get(getAdapterPosition())).putExtra("data2", bean));
                            }
                    } else {
                        Toast.makeText(context, "You have only one check in", Toast.LENGTH_LONG).show();
                    }
                    return false;
                });
                menu.show();

            });
            if (context.getIntent().hasExtra("data1")) {
                mBinding.txtCompare.setVisibility(View.GONE);
            } else {
                mBinding.txtCompare.setVisibility(View.VISIBLE);
            }
        }

    }

}
