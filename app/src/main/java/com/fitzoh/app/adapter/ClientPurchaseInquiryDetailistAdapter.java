package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientInquiryDetailBinding;
import com.fitzoh.app.model.InquiryModel;
import com.fitzoh.app.model.TrainerProductInquiryListing;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientPurchaseInquiryDetailistAdapter extends RecyclerView.Adapter<ClientPurchaseInquiryDetailistAdapter.DataViewHolder> {


    Context context;
    private List<InquiryModel.DataBean.InquiriesBean> productInquiry;


    public ClientPurchaseInquiryDetailistAdapter(Context context, List<InquiryModel.DataBean.InquiriesBean> productInquiry) {
        this.context = context;
        this.productInquiry = productInquiry;

    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientInquiryDetailBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_inquiry_detail, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtName.setText(productInquiry.get(position).getProduct_name());
        Utils.setImagePlaceHolder(context, holder.mBinding.imgClient, productInquiry.get(position).getProduct_image());
        holder.mBinding.txtPrice.setText("Price : " + productInquiry.get(position).getPrice());
        holder.mBinding.txtQty.setText("Quantity : " + productInquiry.get(position).getQty());
    }

    @Override
    public int getItemCount() {
        return productInquiry.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientInquiryDetailBinding mBinding;

        public DataViewHolder(ItemClientInquiryDetailBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

    }

}
