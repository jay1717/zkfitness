package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientWorkoutDetailBinding;
import com.fitzoh.app.model.LiveClientDetailModel;
import com.fitzoh.app.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class LiveClientStatusAdapter extends RecyclerView.Adapter<LiveClientStatusAdapter.DataViewHolder> {


    Context context;
    List<LiveClientDetailModel.DataBean> liveClientDetail;

    public LiveClientStatusAdapter(Context context, List<LiveClientDetailModel.DataBean> liveClientDetail) {
        this.context = context;
        this.liveClientDetail = liveClientDetail;
    }


    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientWorkoutDetailBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_workout_detail, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtExerciswName.setText(liveClientDetail.get(position).getExercise_name());
        holder.mBinding.txtTime.setText(liveClientDetail.get(position).getTimestamp());
//        holder.mBinding.txtTimeRemain.setText(liveClientDetail.get(position).getDuration());

        float rating = liveClientDetail.get(position).getRating();
        if (position == (getItemCount() - 1)) {
            holder.mBinding.view.setVisibility(View.GONE);
        }

        if (liveClientDetail.get(position).getTimestamp() != null && !liveClientDetail.get(position).getTimestamp().equalsIgnoreCase(""))
            checkDateDifference(liveClientDetail.get(position).getTimestamp(), holder.mBinding.txtTimeRemain);
        else
            holder.mBinding.txtTimeRemain.setVisibility(View.GONE);
        holder.mBinding.ratingBar.setRating(rating);

        if (liveClientDetail.get(position).getTimestamp() != null && !liveClientDetail.get(position).getTimestamp().equalsIgnoreCase(""))
            holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_green_shadow));
        else
            holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_red_shadow));

        /*if(rating==0)
            holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_red_shadow));
        else if(rating > 0)
            holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_green_shadow));*/

       /* switch (position) {
            case 2:
                holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_red_shadow));
                break;
            case 3:
                holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_green_shadow));
                break;
            case 6:
                holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_red_shadow));
                break;
            case 8:
                holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_green_shadow));
                break;
            default:
                holder.mBinding.status.setImageDrawable(context.getResources().getDrawable(R.drawable.shape_round_gray_shadow));
                break;

        }*/


    }

    @Override
    public int getItemCount() {
        return liveClientDetail.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientWorkoutDetailBinding mBinding;

        public DataViewHolder(ItemClientWorkoutDetailBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setTextViewStartImage(context, mBinding.txtTimeRemain, R.drawable.ic_clock_schedule, false);
        }
    }

    private void checkDateDifference(String date, TextView txtTimeRemain) {
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        long difference = 0;
        try {
            difference = Calendar.getInstance().getTimeInMillis() - spf.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
        if (min > 0 && min < 59) {
            txtTimeRemain.setText(min + "Min");
            txtTimeRemain.setVisibility(View.VISIBLE);
        } else {
            txtTimeRemain.setVisibility(View.GONE);
        }
    }
}
