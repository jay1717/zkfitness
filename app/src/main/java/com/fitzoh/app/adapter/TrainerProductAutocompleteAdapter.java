package com.fitzoh.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.model.GetFitzohProductModel;
import com.fitzoh.app.model.TrainerShopListModel;

import java.util.ArrayList;

public class TrainerProductAutocompleteAdapter extends ArrayAdapter<GetFitzohProductModel.DataBean> implements Filterable {

    ArrayList<GetFitzohProductModel.DataBean> product, tempProduct, suggestions;
    Context context;


    public TrainerProductAutocompleteAdapter(Context context, ArrayList<GetFitzohProductModel.DataBean> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        try {
            this.context = context;
            this.product = objects;
            this.tempProduct = new ArrayList<GetFitzohProductModel.DataBean>(objects);
            this.suggestions = new ArrayList<GetFitzohProductModel.DataBean>(objects);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GetFitzohProductModel.DataBean product = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_autocomplete, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.name);
        if (suggestions != null)
            textView.setText(product.getName());


        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }

    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            GetFitzohProductModel.DataBean product = (GetFitzohProductModel.DataBean) resultValue;
            return product.getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            suggestions.clear();
            if (constraint != null) {

                for (GetFitzohProductModel.DataBean productName : tempProduct) {
                    if (productName.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(productName);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<GetFitzohProductModel.DataBean> c = (ArrayList<GetFitzohProductModel.DataBean>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (GetFitzohProductModel.DataBean cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
        }
    };

}
