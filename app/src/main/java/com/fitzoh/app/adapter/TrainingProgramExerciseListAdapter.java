package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingProgramExerciseListBinding;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.ui.activity.DetailExcersiceActivity;

import java.util.List;

public class TrainingProgramExerciseListAdapter extends RecyclerView.Adapter<TrainingProgramExerciseListAdapter.DataViewHolder> {
    Context context;
    private List<WorkoutExerciseListModel.DataBean> dataList;

    TrainingProgramExerciseListAdapter(Context context, List<WorkoutExerciseListModel.DataBean> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingProgramExerciseListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_program_exercise_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainingProgramExerciseListBinding mBinding;

        public DataViewHolder(ItemTrainingProgramExerciseListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(WorkoutExerciseListModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            itemView.setOnClickListener(v -> {
                context.startActivity(new Intent(context, DetailExcersiceActivity.class).putExtra("id", dataBean.getId()));

            });
        }
    }
}
