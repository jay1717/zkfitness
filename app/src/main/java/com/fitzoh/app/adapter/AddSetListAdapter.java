package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemAddSetBinding;
import com.fitzoh.app.model.ExerciseSetListModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddSetListAdapter extends RecyclerView.Adapter<AddSetListAdapter.DataHolder> {

    Context context;
    public List<List<ExerciseSetListModel.DataBean>> dataList = new ArrayList<>();
    public AddSetInterface listener;

    public AddSetListAdapter(Context context, AddSetInterface listener) {
        this.context = context;
        this.listener = listener;
    }

    public void setAdapterData(List<List<ExerciseSetListModel.DataBean>> data) {
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addAllData(List<List<ExerciseSetListModel.DataBean>> data) {
        this.dataList = data;
        notifyDataSetChanged();
    }

    public void addData(List<ExerciseSetListModel.DataBean> data) {
        this.dataList.add(data);
        notifyItemInserted(dataList.size());
    }

    @NonNull
    @Override
    public DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAddSetBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_add_set, parent, false);
        return new DataHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataHolder holder, int position) {

        holder.mBinding.txtSetName.setText("Set " + (position + 1));
        holder.mBinding.txtWeight.setText(dataList.get(position).get(0).getParameter_name());
        holder.mBinding.edtWeight.setText(dataList.get(position).get(0).getValue());
        holder.mBinding.txtReps.setText(dataList.get(position).get(1).getParameter_name());
        holder.mBinding.edtReps.setText(dataList.get(position).get(1).getValue());
        holder.mBinding.txtRestPeriod.setText(dataList.get(position).get(2).getParameter_name());
        holder.mBinding.edtRestPeriod.setText(dataList.get(position).get(2).getValue());

        checkAndSet(holder.mBinding.txtWeight, dataList.get(position).get(0).getWeight_unit());
        checkAndSet(holder.mBinding.txtReps, dataList.get(position).get(1).getWeight_unit());
        checkAndSet(holder.mBinding.txtRestPeriod, dataList.get(position).get(2).getWeight_unit());
        holder.mBinding.edtWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                /*if (holder.mBinding.edtWeight.getText().toString().length() <= 0) {
                    dataList.get(position).get(0).setValue(0);
                } else {*/
                if(holder.mBinding.edtWeight.hasFocus())
                    dataList.get(holder.getAdapterPosition()).get(0).setValue(holder.mBinding.edtWeight.getText().toString());

//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        holder.mBinding.edtReps.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

              /*  if (holder.mBinding.edtReps.getText().toString().length() <= 0) {
                    dataList.get(position).get(1).setValue(0);
                } else {*/
              if(holder.mBinding.edtReps.hasFocus())
                dataList.get(holder.getAdapterPosition()).get(1).setValue(holder.mBinding.edtReps.getText().toString());
//                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        holder.mBinding.edtRestPeriod.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
              /*  if (holder.mBinding.edtRestPeriod.getText().toString().length() <= 0) {
                    dataList.get(position).get(2).setValue(0);
                } else {*/
                if(holder.mBinding.edtRestPeriod.hasFocus())
                    dataList.get(holder.getAdapterPosition()).get(2).setValue(holder.mBinding.edtRestPeriod.getText().toString());
//            }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void checkAndSet(TextView textView, String weight_unit) {
        if (textView.getText().toString().equalsIgnoreCase("Weight")) {
            if (weight_unit != null && !weight_unit.equalsIgnoreCase(""))
                textView.setText(Html.fromHtml("Weight" + "<small>" + (weight_unit.equalsIgnoreCase("1") ? " (kg)" : " (lbs)") + "</small>"));
        }
    }


    public interface AddSetInterface {
        void delete(int paramId);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public List<List<ExerciseSetListModel.DataBean>> getData() {
        return dataList;
    }

    class DataHolder extends RecyclerView.ViewHolder {
        ItemAddSetBinding mBinding;

        DataHolder(ItemAddSetBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context,mBinding.imgDelete,R.drawable.ic_delete_green);
            mBinding.imgDelete.setOnClickListener(view -> {
                int id = dataList.get(getAdapterPosition()).get(0).getId();
                dataList.remove(getAdapterPosition());
                notifyDataSetChanged();
                listener.delete(id);
            });
        }
    }


}
