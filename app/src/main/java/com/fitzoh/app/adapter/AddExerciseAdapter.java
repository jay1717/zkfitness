package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemAddExerciseBinding;
import com.fitzoh.app.model.ExerciseListModel;
import com.fitzoh.app.ui.activity.DetailExcersiceActivity;
import com.fitzoh.app.utils.Utils;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.utils.Utils.extractYTId;
import static com.fitzoh.app.utils.Utils.setImagePlaceHolder;

public class AddExerciseAdapter extends RecyclerView.Adapter<AddExerciseAdapter.DataViewHolder> implements Filterable {
    private List<ExerciseListModel.DataBean> dataList = new ArrayList<>();
    private List<ExerciseListModel.DataBean> dataListFilter = new ArrayList<>();
    private Context context;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    String url = "";


    public AddExerciseAdapter(Context context, List<ExerciseListModel.DataBean> dataList) {
        this.dataList = dataList;
        this.dataListFilter = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAddExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_add_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataListFilter.get(position));
        url = dataListFilter.get(position).getVideo_url();

        if(url != null && url.length()>0 && url.indexOf("v=") != -1)
        {
            String s = dataListFilter.get(position).getVideo_url().split("=")[1];
            String thumbnailMq = "http://img.youtube.com/vi/" + s + "/mqdefault.jpg"; //medium quality thumbnail
            setImagePlaceHolder(context, holder.mBinding.imgExercise, thumbnailMq);
        }
    }

    public List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < dataListFilter.size(); i++) {
            if (dataListFilter.get(i).isChecked())
                list.add(dataListFilter.get(i).getId());
        }
        Log.e("Data Filter", ""+list.size());
        return list;
    }

    @Override
    public int getItemCount() {
        if (dataListFilter != null)
            return dataListFilter.size();
        else return 0;
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemAddExerciseBinding mBinding;

        public DataViewHolder(ItemAddExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.chkExercise, R.drawable.checked);
        }

        public void bind(ExerciseListModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            mBinding.txtExerciseName.setOnClickListener(view -> context.startActivity(new Intent(context, DetailExcersiceActivity.class).putExtra("id", dataBean.getId())));
            mBinding.chkExercise.setOnCheckedChangeListener((buttonView, isChecked) -> {
                ExerciseListModel.DataBean data = dataListFilter.get(getAdapterPosition());
                data.setChecked(isChecked);
                dataListFilter.set(getAdapterPosition(), data);
            });
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = dataList;
                } else {
                    List<ExerciseListModel.DataBean> filteredList = new ArrayList<>();
                    for (ExerciseListModel.DataBean row : dataList) {

                        if (row.getExercise_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<ExerciseListModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
