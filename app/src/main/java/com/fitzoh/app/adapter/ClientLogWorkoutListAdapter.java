package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientLogWorkoutListBinding;
import com.fitzoh.app.model.ClientWorkoutTrackingModel;
import com.fitzoh.app.ui.activity.LogWorkoutExerciseListActivity;

import java.util.List;

public class ClientLogWorkoutListAdapter extends RecyclerView.Adapter<ClientLogWorkoutListAdapter.DataViewHolder> {


    Context context;
    private List<ClientWorkoutTrackingModel.DataBean> workoutList;


    public ClientLogWorkoutListAdapter(Context context, List<ClientWorkoutTrackingModel.DataBean> workoutList) {
        this.context = context;
        this.workoutList = workoutList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientLogWorkoutListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_log_workout_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.workoutName.setText(workoutList.get(position).getWorkout_name());
        holder.mBinding.txtDate.setText(workoutList.get(position).getDate());

    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientLogWorkoutListBinding mBinding;

        public DataViewHolder(ItemClientLogWorkoutListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(v -> context.startActivity(new Intent(context, LogWorkoutExerciseListActivity.class).putExtra("workout_id", workoutList.get(getAdapterPosition()).getWorkout_id()).putExtra("workout_name", workoutList.get(getAdapterPosition()).getWorkout_name()).putExtra("isFromWorkout", true).putExtra("isSetEditable", false).putExtra("is_show_log", 1)));
        }
    }


}
