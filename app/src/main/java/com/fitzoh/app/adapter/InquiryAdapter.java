package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemInquiryAdapterBinding;
import com.fitzoh.app.databinding.ItemInquiryHistoryBinding;
import com.fitzoh.app.databinding.ItemPurchaseHistoryBinding;
import com.fitzoh.app.model.InquiryModel;
import com.fitzoh.app.model.PurchaseHistoryTrainerModel;
import com.fitzoh.app.model.TrainerProductInquiryListing;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class InquiryAdapter extends RecyclerView.Adapter<InquiryAdapter.DataViewHolder> {


    List<InquiryModel.DataBean> packageList;
    Context context;
    private dataPassing dataPassing;

    public InquiryAdapter(List<InquiryModel.DataBean> packageList, Context context, InquiryAdapter.dataPassing dataPassing) {
        this.packageList = packageList;
        this.context = context;
        this.dataPassing = dataPassing;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemInquiryAdapterBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_inquiry_adapter, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(packageList.get(position).getClient_name());
        holder.mBinding.txtInquiry.setText("No. of Inquiries :" + packageList.get(position).getNo_of_inquiries());
        if (packageList.get(position).getClient_photo() != null && !packageList.get(position).getClient_photo().equalsIgnoreCase("")) {
            Utils.setImagePlaceHolder(context, holder.mBinding.imgPackage, packageList.get(position).getClient_photo());
        } else {
            holder.mBinding.imgPackage.setImageResource(R.drawable.ic_user_placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemInquiryAdapterBinding mBinding;

        public DataViewHolder(ItemInquiryAdapterBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(v -> dataPassing.data(packageList.get(getAdapterPosition())));
        }

    }
    public interface dataPassing {
        void data(InquiryModel.DataBean dataBean);

    }
}
