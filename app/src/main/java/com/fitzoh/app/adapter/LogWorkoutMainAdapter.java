package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemLogWorkoutMainBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;

import java.util.List;

public class LogWorkoutMainAdapter extends RecyclerView.Adapter<LogWorkoutMainAdapter.DataViewHolder> {

    Context context;
    private List<List<LogWorkoutDataModel.DataBean>> dataList;
    private StartWorkoutListener listener;
    private boolean isSetEditable = false;

    public LogWorkoutMainAdapter(Context context, List<List<LogWorkoutDataModel.DataBean>> dataList, StartWorkoutListener listener, boolean isSetEditable) {
        this.context = context;
        this.dataList = dataList;
        this.listener = listener;
        this.isSetEditable = isSetEditable;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLogWorkoutMainBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_log_workout_main, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemLogWorkoutMainBinding mBinding;

        public DataViewHolder(ItemLogWorkoutMainBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
           // mBinding.workoutName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }

        public void bind(List<LogWorkoutDataModel.DataBean> dataBean) {
            if (dataBean != null && dataBean.size() > 0) {
                /*if (dataBean.size() >= 3) {
                    mBinding.workoutName.setText("Giant Set");
                } else if (dataBean.size() == 2) {
                    mBinding.workoutName.setText("Super Set");
                } else {
                    mBinding.workoutName.setText("Standard Set");
                }*/
                //mBinding.workoutName.setVisibility(View.VISIBLE);
                mBinding.viewSideLine.setVisibility(dataBean.size() > 1 ? View.VISIBLE : View.GONE);
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new LogWorkoutExerciseAdapter(context, dataBean, getAdapterPosition(), listener, isSetEditable));
            } else {
                //mBinding.workoutName.setVisibility(View.GONE);
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }

    public interface StartWorkoutListener {
        void setRating(int main, int sub, int rating);

        void setWeight(int main, int sub, int set, String value);

        void setReps(int main, int sub, int set, String value);

        void setRepetations(int main, int sub, int set, String value);
    }
}
