package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemEditWorkoutBinding;
import com.fitzoh.app.model.WorkoutExerciseListModel;

import java.util.List;

public class TrainingProgramExerciseMainAdapter extends RecyclerView.Adapter<TrainingProgramExerciseMainAdapter.DataViewHolder> {
    Context context;
    private List<List<WorkoutExerciseListModel.DataBean>> dataList;

    public TrainingProgramExerciseMainAdapter(Context context, List<List<WorkoutExerciseListModel.DataBean>> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemEditWorkoutBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_edit_workout, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.viewSideLine.setVisibility(dataList.get(position).size() > 1 ? View.VISIBLE : View.GONE);
        holder.mBinding.recyclerView.setAdapter(new TrainingProgramExerciseListAdapter(context, dataList.get(position)));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemEditWorkoutBinding mBinding;

        public DataViewHolder(ItemEditWorkoutBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }
}
