package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingProgramDayClientBinding;
import com.fitzoh.app.model.TrainingProgramDetailClientData;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.activity.TrainingProgramDietActivity;

import java.util.List;

import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

public class TrainingProgramDayClientAdapter extends RecyclerView.Adapter<TrainingProgramDayClientAdapter.DataViewHolder> {

    private List<TrainingProgramDetailClientData.DataBean.TrainingWeeksBean.WeekdaysBean> daysList;
    Activity context;
    TrainingProgramWeekClientAdapter.RedirectToDetailListener listener;

    TrainingProgramDayClientAdapter(Activity context, List<TrainingProgramDetailClientData.DataBean.TrainingWeeksBean.WeekdaysBean> daysList, TrainingProgramWeekClientAdapter.RedirectToDetailListener listener) {
        this.context = context;
        this.daysList = daysList;
        this.listener = listener;
    }


    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingProgramDayClientBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_program_day_client, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainingProgramDayClientBinding mBinding;

        public DataViewHolder(ItemTrainingProgramDayClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.dayName.setOnClickListener(view -> setVisible(mBinding.layout, mBinding.imgArrow));
            mBinding.imgArrow.setOnClickListener(view -> setVisible(mBinding.layout, mBinding.imgArrow));

        }

        public void bind(int position) {
            TrainingProgramDetailClientData.DataBean.TrainingWeeksBean.WeekdaysBean day = daysList.get(position);
            mBinding.dayName.setText(String.format("%s - %s", day.getDay(), day.getDate()));
            mBinding.layoutAm.setOnClickListener(view -> {
                if (day.getWorkout_am_logged() == 1) {
                    listener.viewLogWorkout(day.getWorkout_am_id(), day.getWorkout_am_name(), true, day.getId());
                } else {
                    listener.openExerciseDetail(day.getWorkout_am_id(), day.getWorkout_am_name(), true, day.getId());
                }
            });
            mBinding.layoutPm.setOnClickListener(view -> {
                if (day.getWorkout_pm_logged() == 1) {
                    listener.viewLogWorkout(day.getWorkout_pm_id(), day.getWorkout_pm_name(), false, day.getId());
                } else {
                    listener.openExerciseDetail(day.getWorkout_pm_id(), day.getWorkout_pm_name(), false, day.getId());
                }
            });
            mBinding.layoutDiet.setOnClickListener(view -> {
                ((BaseActivity) context).session.editor.remove(CLIENT_ID).commit();
                context.startActivity(new Intent(context, TrainingProgramDietActivity.class).putExtra("dietId", day.getDiet_plan_id()).putExtra("dietName", day.getDiet_plan_name()));
            });
            mBinding.txtAMLogged.setOnClickListener(view -> {
                listener.viewLogWorkout(day.getWorkout_am_id(), day.getWorkout_am_name(), true, day.getId());
            });
            mBinding.txtPMLogged.setOnClickListener(view -> {
                listener.viewLogWorkout(day.getWorkout_pm_id(), day.getWorkout_pm_name(), false, day.getId());
            });

            if (day.getIs_restfull_day() == 1) {
                mBinding.txtRestDay.setVisibility(View.VISIBLE);
                mBinding.txtAMWorkout.setVisibility(View.GONE);
                mBinding.txtPMWorkout.setVisibility(View.GONE);
                mBinding.txtNutritionPlan.setVisibility(View.GONE);
                mBinding.txtCheckIn.setVisibility(View.GONE);
            } else {
                mBinding.txtAMWorkout.setText(String.format("%s (am)", day.getWorkout_am_name()));
                mBinding.txtPMWorkout.setText(String.format("%s (pm)", day.getWorkout_pm_name()));
                mBinding.txtNutritionPlan.setText(day.getDiet_plan_name());
                mBinding.txtRestDay.setVisibility(View.GONE);



                checkVisibility(mBinding.txtAMWorkout, mBinding.layoutAm, day.getWorkout_am_id());
                checkVisibility(mBinding.txtPMWorkout, mBinding.layoutPm, day.getWorkout_pm_id());
                checkVisibility(mBinding.txtNutritionPlan, mBinding.layoutDiet, day.getDiet_plan_id());
                checkVisibility(mBinding.txtCheckIn, mBinding.layoutCheck, day.getIs_checkin());

                checkVisibility(mBinding.txtAMLogged, day.getWorkout_am_logged());
                checkVisibility(mBinding.txtPMLogged, day.getWorkout_pm_logged());
                checkVisibility(mBinding.txtDietLogged, day.getDiet_plan_logged());
                checkVisibility(mBinding.txtCheckLogged, day.getCheckin_logged());

                checkLogged(mBinding.layoutAm, day.getWorkout_am_logged());
                checkLogged(mBinding.layoutPm, day.getWorkout_pm_logged());
                checkLogged(mBinding.layoutDiet, day.getDiet_plan_logged());
                checkLogged(mBinding.layoutCheck, day.getCheckin_logged());
            }
        }
    }

    private void checkVisibility(TextView textView, ConstraintLayout imageView, int value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
        }
    }


    private void checkLogged(ConstraintLayout imageView, int value) {
        if (value == 0) {
            imageView.setBackground(context.getResources().getDrawable(R.drawable.select_workout_bg_red));
        }
    }



    private void checkVisibility(TextView textView, int value) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void setVisible(ConstraintLayout recyclerView, ImageView imageView) {
        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
            imageView.setRotation(-90);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            imageView.setRotation(0);
        }

    }

}
