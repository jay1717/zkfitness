package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientProfileNutritionListBinding;
import com.fitzoh.app.model.DietListModel;

import java.util.List;

public class ClientProfileNutritionListAdapter extends RecyclerView.Adapter<ClientProfileNutritionListAdapter.DataViewHolder> {


    Context context;
    private onAddClient onAddClient;
    private List<DietListModel.DataBean> dietList;
    private UnAssign unAssign;
    String microNutrition;
    private int is_assigned;

    public ClientProfileNutritionListAdapter(Context context, List<DietListModel.DataBean> dietList, int created, UnAssign unAssign) {
        this.context = context;
        this.unAssign = unAssign;
        this.dietList = dietList;
        this.is_assigned = created;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemClientProfileNutritionListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_profile_nutrition_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {


        holder.mBinding.txtNoMealsValue.setText(String.valueOf(dietList.get(position).getNo_of_meals()));
        //  holder.mBinding.txtNoMeals.setText(String.valueOf(workoutList.get(position).getNo_of_sets()));
        holder.mBinding.nutritionName.setText(dietList.get(position).getName());

        microNutrition = "";
        int size = dietList.get(position).getDiet_plan_macro_nutrient().size();
        for (int i = 0; i < size; i++) {
            microNutrition = microNutrition + "" + dietList.get(position).getDiet_plan_macro_nutrient().get(i).toString() + ",";

        }
        if (microNutrition != "" && microNutrition.length() > 0 && microNutrition.endsWith(",")) {
            microNutrition = microNutrition.substring(0, microNutrition.length() - 1);
        }
        holder.mBinding.txtMicronutritionValue.setText(microNutrition);


    }

    @Override
    public int getItemCount() {
        return dietList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientProfileNutritionListBinding mBinding;

        public DataViewHolder(ItemClientProfileNutritionListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(view -> {
                unAssign.edit(dietList.get(getAdapterPosition()).getId(), dietList.get(getAdapterPosition()).getName());
            });
            mBinding.imgUnassign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    unAssign.remove(dietList.get(getAdapterPosition()));
                }
            });
            mBinding.imgUnassign.setVisibility(is_assigned == 1 ? View.VISIBLE : View.GONE);
        }

    }


    public interface onAddClient {
        void add(int id);

        void edit(int id, String name);

        void rename(int position);
    }

    public interface UnAssign {
        void remove(DietListModel.DataBean dietListModel);

        void edit(int id, String name);

    }
}
