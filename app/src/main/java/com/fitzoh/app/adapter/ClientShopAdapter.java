package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientShopBinding;
import com.fitzoh.app.model.ClientShopListModel;
import com.fitzoh.app.ui.activity.ClientShopDetailsActivity;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ClientShopAdapter extends RecyclerView.Adapter<ClientShopAdapter.DataViewHolder> implements Filterable {

    Context context;
    List<ClientShopListModel.DataBean> clientListModel;
    private List<ClientShopListModel.DataBean> dataListFilter = new ArrayList<>();

    public ClientShopAdapter(Context context, List<ClientShopListModel.DataBean> clientListModel) {
        this.context = context;
        this.clientListModel = clientListModel;
        this.dataListFilter = clientListModel;

    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ItemClientShopBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_shop, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        if (dataListFilter.get(position).getImage() != null && dataListFilter.get(position).getImage().size() > 0) {
            Utils.setImagePlaceHolder(context, holder.mBinding.imgProduct, dataListFilter.get(position).getImage().get(0));
        } else {
            holder.mBinding.imgProduct.setImageResource(R.drawable.placeholder);
        }
        holder.mBinding.txtName.setText(dataListFilter.get(position).getName());
        holder.mBinding.txtPrice.setText(String.valueOf(dataListFilter.get(position).getPrice()));
        holder.mBinding.txtDesc.setText(dataListFilter.get(position).getDiscounted_price());

        holder.mBinding.txtDesc.setPaintFlags(holder.mBinding.txtDesc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = clientListModel;
                } else {
                    List<ClientShopListModel.DataBean> filteredList = new ArrayList<>();
                    for (ClientShopListModel.DataBean row : dataListFilter) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<ClientShopListModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        if (dataListFilter != null)
            return dataListFilter.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientShopBinding mBinding;

        DataViewHolder(ItemClientShopBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

            mBinding.cardMain.setOnClickListener(v -> context.startActivity(new Intent(context, ClientShopDetailsActivity.class).putExtra("data", clientListModel.get(getAdapterPosition()))));

        }
    }

}
