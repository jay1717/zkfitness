package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemMyClientBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.ui.activity.ClientProfileActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class MyClientListAdapter extends RecyclerView.Adapter<MyClientListAdapter.DataViewHolder> {

    private Context context;
    private List<ClientListModel.DataBean> liveClientsModels;
    private ClientActivateListener listener;

    public MyClientListAdapter(Context context, List<ClientListModel.DataBean> liveClientsModels, ClientActivateListener listener) {
        this.context = context;
        this.liveClientsModels = liveClientsModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMyClientBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_my_client, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(liveClientsModels.get(position));
    }

    @Override
    public int getItemCount() {
        return liveClientsModels.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemMyClientBinding mBinding;

        public DataViewHolder(ItemMyClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(v -> {
                context.startActivity(new Intent(context, ClientProfileActivity.class).putExtra("data", liveClientsModels.get(getAdapterPosition())).putExtra("client_type", "1"));
            });
            Utils.setSwitchTint(context, mBinding.toggle);
            this.setIsRecyclable(false);

        }

        public void bind(ClientListModel.DataBean dataBean) {
            if (!dataBean.getPhoto().equals(""))
                Utils.setImage(context, mBinding.imgUser, dataBean.getPhoto());
            else
                mBinding.imgUser.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_user_placeholder));
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            mBinding.toggle.setOnCheckedChangeListener((compoundButton, b) -> {
                listener.setAssign(liveClientsModels.get(getAdapterPosition()).getId(), b ? 1 : 0);
            });
            if (dataBean.getIs_profile_completed() == 0) {
                mBinding.imgChat.setAlpha((float) 0.5);
                mBinding.imgChat.setEnabled(false);
            } else if (dataBean.getIs_profile_completed() == 1) {
                mBinding.imgChat.setAlpha((float) 1.0);
                mBinding.imgChat.setEnabled(true);
            }
            mBinding.imgChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(context, ConversationActivity.class);
                    intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(dataBean.getId()));
                    intent1.putExtra(ConversationUIService.DISPLAY_NAME, dataBean.getName()); //put it for displaying the title.
                    intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
                    context.startActivity(intent1);
                }
            });
        }
    }

    public interface ClientActivateListener {
        void setAssign(int id, int isAssign);
    }
}
