package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemLogWorkoutSetsBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.fragment.LogWorkoutExerciseListFragment;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.single;

public class LogWorkoutSetsAdapter extends RecyclerView.Adapter<LogWorkoutSetsAdapter.DataHolder> {

    Context context;
    public List<List<LogWorkoutDataModel.DataBean.SetsBean>> dataList = new ArrayList<>();
    private int main = 0, sub = 0;
    LogWorkoutMainAdapter.StartWorkoutListener listener;
    private boolean isSetEditable = false;
    private ArrayList<String> edt = new ArrayList<>();

    public LogWorkoutSetsAdapter(Context context, List<List<LogWorkoutDataModel.DataBean.SetsBean>> dataList, int main, int sub, LogWorkoutMainAdapter.StartWorkoutListener listener, boolean isSetEditable) {
        this.context = context;
        this.dataList = dataList;
        this.main = main;
        this.sub = sub;
        this.listener = listener;
        this.isSetEditable = isSetEditable;
    }


    @NonNull
    @Override
    public DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLogWorkoutSetsBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_log_workout_sets, parent, false);
        return new DataHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataHolder holder, int position) {
        List<LogWorkoutDataModel.DataBean.SetsBean> setsBeans = dataList.get(position);
        holder.mBinding.txtSetName.setText("Set " + (position + 1));
        if (setsBeans.size() > 0 && setsBeans.get(0) != null) {
            holder.mBinding.lblKg.setText(setsBeans.get(0).getParameter_name() + " : " + (setsBeans.get(0).getSet_value() == null ? "" : setsBeans.get(0).getSet_value()));
            checkAndSet(holder.mBinding.edtKg, setsBeans.get(0).getParameter_name(), setsBeans.get(0).getValue());
        } else {
            holder.mBinding.lblKg.setText("");
            holder.mBinding.edtKg.setEnabled(false);
            holder.mBinding.edtKg.setText("");
        }
        if (setsBeans.size() > 1 && setsBeans.get(1) != null) {
            holder.mBinding.lblReps.setText(setsBeans.get(1).getParameter_name() + " : " + (setsBeans.get(1).getSet_value() == null ? "" : setsBeans.get(1).getSet_value()));
            checkAndSet(holder.mBinding.edtReps, setsBeans.get(1).getParameter_name(), setsBeans.get(1).getValue());
        } else {
            holder.mBinding.lblReps.setText("");
            holder.mBinding.edtReps.setEnabled(false);
            holder.mBinding.edtReps.setText("");
        }
        if (setsBeans.size() > 2 && setsBeans.get(2) != null) {
            holder.mBinding.lblRepetation.setText(setsBeans.get(2).getParameter_name() + " : " + (setsBeans.get(2).getSet_value() == null ? "" : setsBeans.get(2).getSet_value()) + " sec");
            checkAndSet(holder.mBinding.edtRepetation, setsBeans.get(2).getParameter_name(), setsBeans.get(2).getValue());
            if(setsBeans.get(2).getSet_value()!=null && setsBeans.get(2).getSet_value().equals("")){
                holder.mBinding.lblRepetation.setText("Rest Period : 0 Sec");
            }

        } else {
            holder.mBinding.lblRepetation.setText("");
            holder.mBinding.edtRepetation.setEnabled(false);
            holder.mBinding.edtRepetation.setText("");
        }
        if (setsBeans.size() > 0 && setsBeans.get(0) != null && setsBeans.get(0).getHistory_value() != null && !setsBeans.get(0).getHistory_value().equalsIgnoreCase("")) {
            holder.mBinding.historyText.setText(setsBeans.get(0).getHistory_value());
            holder.mBinding.historyText.setVisibility(View.VISIBLE);
        } else {
            holder.mBinding.historyText.setVisibility(View.VISIBLE);
            holder.mBinding.historyText.setText("");
        }
        holder.mBinding.edtRepetation.setVisibility(View.GONE);
        holder.mBinding.edtKg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listener.setWeight(main, sub, position, holder.mBinding.edtKg.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        holder.mBinding.edtReps.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listener.setReps(main, sub, position, holder.mBinding.edtReps.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.mBinding.edtRepetation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listener.setRepetations(main, sub, position, holder.mBinding.edtRepetation.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void checkAndSet(EditText editText, String value, String input) {
        if (value.equalsIgnoreCase("N/A") || value.equalsIgnoreCase("Rest Period")) {
            editText.setEnabled(false);
            editText.setText("");
        } else if (!isSetEditable) {
            editText.setEnabled(false);
            editText.setText(input == null ? "" : input);
        } else {
            editText.setEnabled(true);
            editText.setText(input == null ? "" : input);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public List<List<LogWorkoutDataModel.DataBean.SetsBean>> getData() {
        return dataList;
    }

    class DataHolder extends RecyclerView.ViewHolder {
        ItemLogWorkoutSetsBinding mBinding;

        DataHolder(ItemLogWorkoutSetsBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }

//    private void storePref(){
//
//        SharedPreferences appSharedPrefs = PreferenceManager
//                .getDefaultSharedPreferences(context);
//        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
//        Gson gson = new Gson();
//        String json = gson.toJson(set);
//        prefsEditor.putString("MyObject", json);
//        prefsEditor.commit();
//
//
//       /* for (List<LogWorkoutDataModel.DataBean.SetsBean> beans : dataList){
//            for (LogWorkoutDataModel.DataBean.SetsBean set : beans){
//
//                set = new LogWorkoutDataModel.DataBean.SetsBean();
//                SharedPreferences appSharedPrefs = PreferenceManager
//                        .getDefaultSharedPreferences(context);
//                SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
//                Gson gson = new Gson();
//                String json = gson.toJson(set);
//                prefsEditor.putString("MyObject", json);
//                prefsEditor.commit();
//            }
//            Log.e("Data", " "+beans.size());*/
//
//        }




//    }





}
