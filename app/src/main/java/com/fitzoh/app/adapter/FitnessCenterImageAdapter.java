package com.fitzoh.app.adapter;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemFitnessCenterBinding;

import java.util.ArrayList;

public class FitnessCenterImageAdapter extends RecyclerView.Adapter<FitnessCenterImageAdapter.DataViewHolder> {


    public ArrayList<Uri> arrayList;

    public ArrayList<Uri> getData() {
        return arrayList;
    }

    private RemoveImageListener listener;

    public FitnessCenterImageAdapter(ArrayList<Uri> arrayList, RemoveImageListener listener) {
        this.arrayList = arrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFitnessCenterBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_fitness_center, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

//        holder.mBinding.imgUpload.setImageURI(arrayList.get(position));
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_user_placeholder)
                .error(R.drawable.ic_user_placeholder);
        Glide.with(holder.mBinding.imgUpload.getContext())
                .load(arrayList.get(position))
                .apply(options)
                .into(holder.mBinding.imgUpload);
        holder.mBinding.imgCancel.setOnClickListener(v -> {
            listener.removeImage(position);
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemFitnessCenterBinding mBinding;

        public DataViewHolder(ItemFitnessCenterBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }

    public interface RemoveImageListener {
        void removeImage(int position);
    }
}