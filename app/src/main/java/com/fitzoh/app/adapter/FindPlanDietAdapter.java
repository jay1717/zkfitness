package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemFindPlanBinding;
import com.fitzoh.app.model.FindPlanDietModel;

import java.util.ArrayList;
import java.util.List;

public class FindPlanDietAdapter extends RecyclerView.Adapter<FindPlanDietAdapter.DataViewHolder> implements Filterable {


    Context context;
    List<FindPlanDietModel.DataBean> findPlanList;
    private List<FindPlanDietModel.DataBean> dataListFilter = new ArrayList<>();
    private onDataModeified onDataModeified;

    public FindPlanDietAdapter(Context context, List<FindPlanDietModel.DataBean> findPlanList, FindPlanDietAdapter.onDataModeified onDataModeified) {
        this.context = context;
        this.findPlanList = findPlanList;
        this.dataListFilter = findPlanList;
        this.onDataModeified = onDataModeified;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFindPlanBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_find_plan, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(dataListFilter.get(position).getName());
        String nutritions = TextUtils.join(", ", dataListFilter.get(position).getDiet_plan_macro_nutrient());
        holder.mBinding.txtSecond.setText("Micro nutritions : " + nutritions);
        holder.mBinding.txtThird.setText("No. of meals : " + dataListFilter.get(position).getNo_of_meals());
        holder.mBinding.txtPrice.setText("Price : " + (dataListFilter.get(position).getPrice() == 0 ? "Free" : dataListFilter.get(position).getPrice()));

        holder.mBinding.frontLayout.setOnClickListener(view -> onDataModeified.getData(dataListFilter.get(position)));
    }

    @Override
    public int getItemCount() {
        if (findPlanList != null)
            return dataListFilter.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemFindPlanBinding mBinding;

        public DataViewHolder(ItemFindPlanBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = findPlanList;
                } else {
                    List<FindPlanDietModel.DataBean> filteredList = new ArrayList<>();
                    for (FindPlanDietModel.DataBean row : findPlanList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<FindPlanDietModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface onDataModeified {

        void getData(FindPlanDietModel.DataBean dataBean);

    }

}
