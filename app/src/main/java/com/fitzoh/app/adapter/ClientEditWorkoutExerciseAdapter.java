package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientEditWorkoutBinding;
import com.fitzoh.app.model.ClientEditWorkoutModel;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientEditWorkoutExerciseAdapter extends RecyclerView.Adapter<ClientEditWorkoutExerciseAdapter.DataViewHolder> {
    Context context;
    private List<List<ClientEditWorkoutModel.DataBean>> dataList;

    public ClientEditWorkoutExerciseAdapter(Context context, List<List<ClientEditWorkoutModel.DataBean>> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientEditWorkoutBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_edit_workout, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        if(dataList.size()>0){
            if(dataList.get(position).size()>0){
                for (int i = 0; i <dataList.get(position).size() ; i++) {
                    holder.mBinding.workoutName.setText(dataList.get(position).get(i).getExercise_name());
                    Utils.setImage(context,holder.mBinding.imgUser, dataList.get(position).get(i).getImage());
                }
            }
        }


       /* if (position != 0) {
            holder.mBinding.imgMenuHorizon.setVisibility(View.GONE);
        } else {
            holder.mBinding.imgMenuHorizon.setVisibility(View.VISIBLE);
        }*/
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientEditWorkoutBinding mBinding;

        public DataViewHolder(ItemClientEditWorkoutBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkboxClientAssign, R.drawable.checked);
           // Utils.setImageBackground(context, mBinding.imgMenuHorizon, R.drawable.ic_menu_horizon);
           // mBinding.imgMenuHorizon.setOnClickListener(view -> showPopupMenu(mBinding.imgMenuHorizon, getAdapterPosition()));
        }

    }
}
