package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingProgramListBinding;
import com.fitzoh.app.model.TrainingProgramList;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class TrainingProgramListAdapter extends RecyclerView.Adapter<TrainingProgramListAdapter.DataViewHolder> {


    Context context;
    private TrainingProgramListener listener;
    private List<TrainingProgramList.DataBean> trainingProgramList;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public TrainingProgramListAdapter(Context context, TrainingProgramListAdapter.TrainingProgramListener listener, List<TrainingProgramList.DataBean> trainingProgramList) {
        this.context = context;
        this.listener = listener;
        this.trainingProgramList = trainingProgramList;
    }
    public TrainingProgramListAdapter() {
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingProgramListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_program_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.bind(position);
        viewBinderHelper.bind(holder.mBinding.swipeLayout, String.valueOf(trainingProgramList.get(position).getId()));
        holder.mBinding.recyclerView.setAdapter(new AssignedUserListAdapter(context, trainingProgramList.get(position).getAssigned_to()));
        holder.mBinding.txtNoOfWeeksValue.setText(String.valueOf(trainingProgramList.get(position).getWeeks()));
        holder.mBinding.trainingProgramName.setText(trainingProgramList.get(position).getTitle());


        if (position % 2 == 0)
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        else
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
    }

    @Override
    public int getItemCount() {
        return trainingProgramList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemTrainingProgramListBinding mBinding;

        public DataViewHolder(ItemTrainingProgramListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);
            Utils.setImageBackground(context, mBinding.imgMenuHorizon, R.drawable.ic_menu_horizon);
            mBinding.recyclerView.addItemDecoration(new OverlapDecoration());

        }

        public void bind(int position) {
            mBinding.frontLayout.setOnClickListener(view -> listener.edit(trainingProgramList.get(getAdapterPosition())));
            mBinding.imgAddUser.setOnClickListener(v -> listener.add(trainingProgramList.get(getAdapterPosition())));
            mBinding.imgMenuHorizon.setOnClickListener(view -> showPopupMenu(mBinding.imgMenuHorizon, getAdapterPosition()));
            mBinding.deleteLayout.setOnClickListener(v -> listener.delete(trainingProgramList.get(getAdapterPosition())));
        }
    }


    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -20;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            if (itemPosition == 0) {
                return;
            }
            outRect.set(vertOverlap, 0, 0, 0);


        }
    }

    public interface TrainingProgramListener {
        void add(TrainingProgramList.DataBean dataBean);

        void edit(TrainingProgramList.DataBean dataBean);

        void rename(TrainingProgramList.DataBean dataBean);

        void delete(TrainingProgramList.DataBean workOutListModel);

    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.menu_worout_actions, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        popup.show();
    }


    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    listener.edit(trainingProgramList.get(position));
                    break;
                case R.id.action_rename:
                    listener.rename(trainingProgramList.get(position));
                    break;
            }
            return false;
        }
    }


}
