package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientPurchaseListBinding;
import com.fitzoh.app.model.ClientProfilePackageList;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientProfilePurchaseAdapter extends RecyclerView.Adapter<ClientProfilePurchaseAdapter.DataViewHolder> {


    List<ClientProfilePackageList.DataBean> packageList;
    Context context;

    public ClientProfilePurchaseAdapter(List<ClientProfilePackageList.DataBean> packageList, Context context) {
        this.packageList = packageList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientPurchaseListBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_purchase_list, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.packageName.setText(packageList.get(position).getPackageX().getPackage_name());
        holder.mBinding.txtWeeksValue.setText(String.valueOf(packageList.get(position).getPackageX().getWeeks()));
        holder.mBinding.txtprice.setText(String.valueOf(packageList.get(position).getPackageX().getPrice()));
        holder.mBinding.txtPriceValue.setText(String.valueOf(packageList.get(position).getPackageX().getDiscounted_price()));

        Utils.setImage(context, holder.mBinding.imgPackage, packageList.get(position).getPackageX().getMedia());
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientPurchaseListBinding mBinding;

        public DataViewHolder(ItemClientPurchaseListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.txtprice.setPaintFlags(mBinding.txtprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }

    }
}
