package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemWorkoutListBinding;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class WorkoutListAdapter extends RecyclerView.Adapter<WorkoutListAdapter.DataViewHolder> {


    Context context;
    private onAddClient onAddClient;
    private List<WorkOutListModel.DataBean> workoutList;
    private DeleteWorkOut deleteWorkOut;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public WorkoutListAdapter(Context context, WorkoutListAdapter.onAddClient onAddClient, List<WorkOutListModel.DataBean> workoutList, DeleteWorkOut deleteWorkOut) {
        this.context = context;
        this.onAddClient = onAddClient;
        this.workoutList = workoutList;
        this.deleteWorkOut = deleteWorkOut;
    }

    public WorkoutListAdapter() {
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemWorkoutListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_workout_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.bind(position);
        viewBinderHelper.bind(holder.mBinding.swipeLayout, String.valueOf(workoutList.get(position).getId()));

        holder.mBinding.recyclerView.setAdapter(new AssignedUserListAdapter(context, workoutList.get(position).getAssigned_to()));
        holder.mBinding.txtNoExcersiceValue.setText(String.valueOf(workoutList.get(position).getExercise_count()));
        holder.mBinding.txtNoSetsValue.setText(String.valueOf(workoutList.get(position).getNo_of_sets()));
        holder.mBinding.workoutName.setText(workoutList.get(position).getName());
        if (position % 2 == 0)
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        else
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemWorkoutListBinding mBinding;

        public DataViewHolder(ItemWorkoutListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.recyclerView.addItemDecoration(new OverlapDecoration());
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);
            Utils.setImageBackground(context, mBinding.imgMenuHorizon, R.drawable.ic_menu_horizon);
            mBinding.frontLayout.setOnClickListener(view -> {
                onAddClient.edit(workoutList.get(getAdapterPosition()));
            });
        }

        public void bind(int position) {
            mBinding.imgAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAddClient.add(workoutList.get(position));
                }
            });
            mBinding.imgMenuHorizon.setOnClickListener(view -> {
                showPopupMenu(mBinding.imgMenuHorizon, getAdapterPosition());

            });
            mBinding.deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    deleteWorkOut.delete(workoutList.get(getAdapterPosition()));
                }
            });
        }
    }


    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -20;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            if (itemPosition == 0) {
                return;
            }
            outRect.set(vertOverlap, 0, 0, 0);


        }
    }

    public interface onAddClient {
        void add(WorkOutListModel.DataBean dataBean);

        void edit(WorkOutListModel.DataBean dataBean);

        void rename(WorkOutListModel.DataBean dataBean);
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(R.menu.menu_worout_actions, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        popup.show();
    }


    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    onAddClient.edit(workoutList.get(position));
                    break;
                case R.id.action_rename:
                    onAddClient.rename(workoutList.get(position));
                    break;
            }
            return false;
        }
    }

    public interface DeleteWorkOut {
        void delete(WorkOutListModel.DataBean workOutListModel);
    }
}
