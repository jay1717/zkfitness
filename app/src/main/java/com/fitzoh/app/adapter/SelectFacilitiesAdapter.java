package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.RowFitnessValueBinding;
import com.fitzoh.app.model.FacilitiesListModel;
import com.fitzoh.app.model.FitnessValuesModel;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.fragment.SelectFacilitiesFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SelectFacilitiesAdapter extends RecyclerView.Adapter<SelectFacilitiesAdapter.DataViewHolder> implements Filterable {
    List<FitnessValuesModel.DataBean> dataList;
    List<FitnessValuesModel.DataBean> dataListFilter;
    Context context;

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowFitnessValueBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_fitness_value, parent, false);
        return new DataViewHolder(mBinding);
    }

    public SelectFacilitiesAdapter(Context context, List<FitnessValuesModel.DataBean> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.dataListFilter = dataList;
    }

    public void disSelectItem(int position) {
        FitnessValuesModel.DataBean dataBean = dataList.get(position);
        dataBean.setSelected(false);
        dataList.set(position, dataBean);
        notifyItemChanged(position);
    }


    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        final FitnessValuesModel.DataBean fitnessData = dataListFilter.get(position);
        if (fitnessData != null) {
            holder.bind(fitnessData);
        }
        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
             /*   if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }*/
                if (SelectFacilitiesFragment.facilitiesFragment.selectedFitneeList.containsKey(String.valueOf(fitnessData.getId()))) {
                    fitnessData.setSelected(false);
                    SelectFacilitiesFragment.facilitiesFragment.selectedFitneeList.remove(String.valueOf(fitnessData.getId()));
                    holder.mBinding.imgSelected.setVisibility(View.GONE);
                    holder.mBinding.txtUserName.setTextColor(context.getResources().getColor(R.color.gray));
                    SelectFacilitiesFragment.facilitiesFragment.mBinding.chipsView.removeChipBy(fitnessData);
                } else {
                    fitnessData.setSelected(true);
                    SelectFacilitiesFragment.facilitiesFragment.selectedFitneeList.put(String.valueOf(fitnessData.getId()), fitnessData);
                    holder.mBinding.imgSelected.setVisibility(View.VISIBLE);
                    holder.mBinding.txtUserName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    SelectFacilitiesFragment.facilitiesFragment.mBinding.chipsView.addChip(fitnessData.getValue(), "", fitnessData, false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataListFilter.size();
        else return 0;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = dataList;
                } else {
                    List<FitnessValuesModel.DataBean> filteredList = new ArrayList<>();
                    for (FitnessValuesModel.DataBean row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getValue().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<FitnessValuesModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        RowFitnessValueBinding mBinding;

        public DataViewHolder(RowFitnessValueBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Drawable pressed = ContextCompat.getDrawable(context, R.drawable.checked);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Objects.requireNonNull(pressed).setTint(((BaseActivity) context).res.getColor(R.color.colorAccent));
            } else {
                Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(pressed));
                DrawableCompat.setTint(wrappedDrawable, ((BaseActivity) context).res.getColor(R.color.colorAccent));
            }
            mBinding.imgSelected.setImageDrawable(pressed);
        }

        private void bind(FitnessValuesModel.DataBean data) {

            mBinding.txtUserName.setText(data.getValue());
           /* RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder);*/

         /*   Glide.with(mContext)
                    .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                    .apply(options)
                    .into(mBinding.imgProfilePic);*/

            if (data.isSelected()) {
                mBinding.imgSelected.setVisibility(View.VISIBLE);
                mBinding.txtUserName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            } else {
                mBinding.imgSelected.setVisibility(View.GONE);
                mBinding.txtUserName.setTextColor(context.getResources().getColor(R.color.gray));
            }

            mBinding.main.setOnClickListener(view -> {
                if (SelectFacilitiesFragment.facilitiesFragment.selectedFitneeList.containsKey(String.valueOf(data.getId()))) {
                    data.setSelected(false);
                    SelectFacilitiesFragment.facilitiesFragment.selectedFitneeList.remove(String.valueOf(data.getId()));
                    mBinding.imgSelected.setVisibility(View.GONE);
                    mBinding.txtUserName.setTextColor(context.getResources().getColor(R.color.gray));
                    SelectFacilitiesFragment.facilitiesFragment.mBinding.chipsView.removeChipBy(data);
                } else {
                    data.setSelected(true);
                    SelectFacilitiesFragment.facilitiesFragment.selectedFitneeList.put(String.valueOf(data.getId()), data);
                    mBinding.imgSelected.setVisibility(View.VISIBLE);
                    mBinding.txtUserName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    SelectFacilitiesFragment.facilitiesFragment.mBinding.chipsView.addChip(data.getValue(), "", data, false);
                }
            });

        }
    }
}
