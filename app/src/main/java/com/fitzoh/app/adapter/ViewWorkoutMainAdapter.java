package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemLogWorkoutMainBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;

import java.util.List;

public class ViewWorkoutMainAdapter extends RecyclerView.Adapter<ViewWorkoutMainAdapter.DataViewHolder> {

    Context context;
    private List<List<LogWorkoutDataModel.DataBean>> dataList;

    public ViewWorkoutMainAdapter(Context context, List<List<LogWorkoutDataModel.DataBean>> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLogWorkoutMainBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_log_workout_main, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemLogWorkoutMainBinding mBinding;

        public DataViewHolder(ItemLogWorkoutMainBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
         //   mBinding.workoutName.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }

        public void bind(List<LogWorkoutDataModel.DataBean> dataBean) {
            if (dataBean != null) {
                mBinding.viewSideLine.setVisibility(dataBean.size() > 1 ? View.VISIBLE : View.GONE);

                /*if (dataBean.size() >= 3) {
                    mBinding.workoutName.setText("Giant Set");
                    mBinding.workoutName.setVisibility(View.VISIBLE);
                } else if (dataBean.size() == 2) {
                    mBinding.workoutName.setText("Super Set");
                    mBinding.workoutName.setVisibility(View.VISIBLE);
                } else {
                    mBinding.workoutName.setText("Standard Set");
                    mBinding.workoutName.setVisibility(View.GONE);
                }*/
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new ViewWorkoutExerciseAdapter(context, dataBean));
            } else {
              //  mBinding.workoutName.setVisibility(View.GONE);
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }
}
