package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemStartDietListBinding;
import com.fitzoh.app.model.GetClientDetailModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;

public class StartDietListAdapter extends RecyclerView.Adapter<StartDietListAdapter.DataViewHolder> {
    private ArrayList<GetClientDetailModel.DataBean.TodaysDietBean.FoodBean> dataList;
    private Context context;

    public StartDietListAdapter() {

    }

    public StartDietListAdapter(Context context, ArrayList<GetClientDetailModel.DataBean.TodaysDietBean.FoodBean> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemStartDietListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_start_diet_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    /*public List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).isChecked())
                list.add(dataList.get(i).getId());
        }
        return list;
    }*/

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        if(dataList.get(position).getIs_assigned()==1){
            holder.mBinding.checkbox.setClickable(false);
        }
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public ArrayList<GetClientDetailModel.DataBean.TodaysDietBean.FoodBean> getData() {
        return dataList;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemStartDietListBinding mBinding;

        public DataViewHolder(ItemStartDietListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkbox, R.drawable.checked);
            mBinding.checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                GetClientDetailModel.DataBean.TodaysDietBean.FoodBean bean = dataList.get(getAdapterPosition());
                bean.setIs_assigned(isChecked ? 1 : 0);
                dataList.set(getAdapterPosition(), bean);
            });
        }

        public void bind(GetClientDetailModel.DataBean.TodaysDietBean.FoodBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }
}
