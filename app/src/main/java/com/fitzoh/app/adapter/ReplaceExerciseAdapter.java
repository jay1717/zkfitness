package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemAddExerciseBinding;
import com.fitzoh.app.model.ExerciseListModel;
import com.fitzoh.app.ui.activity.ReplaceExerciseSetActivity;
import com.fitzoh.app.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ReplaceExerciseAdapter extends RecyclerView.Adapter<ReplaceExerciseAdapter.DataViewHolder> implements Filterable {
    private List<ExerciseListModel.DataBean> dataList = new ArrayList<>();
    private List<ExerciseListModel.DataBean> dataListFilter = new ArrayList<>();
    private Activity context;

    public ReplaceExerciseAdapter(Activity context, List<ExerciseListModel.DataBean> dataList) {
        this.dataList = dataList;
        this.dataListFilter = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAddExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_add_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataListFilter.get(position));
    }

    public List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < dataListFilter.size(); i++) {
            if (dataListFilter.get(i).isChecked())
                list.add(dataListFilter.get(i).getId());
        }
        return list;
    }

    @Override
    public int getItemCount() {
        if (dataListFilter != null)
            return dataListFilter.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemAddExerciseBinding mBinding;

        public DataViewHolder(ItemAddExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.chkExercise, R.drawable.checked);
        }

        public void bind(ExerciseListModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            mBinding.chkExercise.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked && context.getIntent().getIntExtra("exerciseID", 0) != dataListFilter.get(getAdapterPosition()).getId()) {
                    Intent intent = new Intent(context, ReplaceExerciseSetActivity.class);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("id", dataListFilter.get(getAdapterPosition()).getId());
                        jsonObject.put("exercise_name", dataListFilter.get(getAdapterPosition()).getExercise_name());
                        jsonObject.put("image", dataListFilter.get(getAdapterPosition()).getImage());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    intent.putExtra("data", jsonObject.toString());
                    intent.putExtra("main", context.getIntent().getIntExtra("main", 0));
                    intent.putExtra("sub", context.getIntent().getIntExtra("sub", 0));
                    context.startActivityForResult(intent, 100);
                } else if (isChecked) {
                    mBinding.chkExercise.setChecked(false);
                    AlertDialog alertDialog = new AlertDialog.Builder(context)
                            .setTitle("Replace Exercise")
                            .setMessage("Please select different exercise.This exercise is already added in your workout")
//                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                                dialog.dismiss();
                            })
                            .show();
                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                }
            });
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = dataList;
                } else {
                    List<ExerciseListModel.DataBean> filteredList = new ArrayList<>();
                    for (ExerciseListModel.DataBean row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getExercise_name().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<ExerciseListModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
