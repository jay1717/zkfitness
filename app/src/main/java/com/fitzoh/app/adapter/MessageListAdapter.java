package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.contact.AppContactService;
import com.applozic.mobicommons.people.contact.Contact;
import com.bumptech.glide.Glide;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemMessageListBinding;

import com.applozic.mobicomkit.api.conversation.Message;
import com.applozic.mobicomkit.api.conversation.database.MessageDatabaseService;


import java.util.ArrayList;
import java.util.List;



public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.DataViewHolder>  {

    ItemMessageListBinding mBinding;

    private LayoutInflater inflater;
    private Context mContext;
    private List<Message> messageList = new ArrayList<>();
    private static final int ONE_TO_ONE_CHAT = 1;
    private static final int GROUP_CHAT = 2;

    private MessageDatabaseService messageDatabaseService;

    public MessageListAdapter(Context mContext, List<Message> listOfMessages){
        this.mContext = mContext;
        this.messageDatabaseService = new MessageDatabaseService(mContext);
        inflater = LayoutInflater.from(mContext);
        this.messageList = listOfMessages;
    }




    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMessageListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_message_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        final Message current = messageList.get(position);
        switch (holder.getItemViewType()){
            case ONE_TO_ONE_CHAT:
                final Contact contact = new AppContactService(mContext).getContactById(current.getContactIds());

                holder.mBinding.txtUserName.setText(contact.getDisplayName());

                if(messageDatabaseService.getUnreadMessageCountForContact(current.getContactIds()) != 0){
                    holder.mBinding.txtMsgCount.setText(String.valueOf(messageDatabaseService.getUnreadMessageCountForContact(current.getContactIds())));
                }

                if(contact.getImageURL() == null || contact.getImageURL().equalsIgnoreCase(null)) {
                    holder.mBinding.imgUser.setImageResource(R.mipmap.ic_launcher);
                }
                else{
                    Glide.with(mContext).load(contact.getImageURL()).
                            thumbnail(0.5f).
                            into(holder.mBinding.imgUser);
                }
                if(current.hasAttachment()){
                   // holder.mBinding.txtMsg.setTextColor(Color.GRAY);
                    if(current.getAttachmentType().equals(Message.VIDEO)){
                        holder.mBinding.txtMsg.setText("VIDEO");
                    }else if(current.getAttachmentType().equals(Message.AUDIO)){
                        holder.mBinding.txtMsg.setText("AUDIO");
                    }else if(current.getAttachmentType().equals(Message.CONTACT)){
                        holder.mBinding.txtMsg.setText("CONTACT");
                    }else if(current.getAttachmentType().equals(Message.LOCATION)){
                        holder.mBinding.txtMsg.setText("LOCATION");
                    }else if(current.getAttachmentType().equals(Message.OTHER)){
                        holder.mBinding.txtMsg.setText("ATTACHMENT");
                    }else{
                        //image
                        holder.mBinding.txtMsg.setText("IMAGE");
                    }
                }else if(Message.ContentType.LOCATION.getValue().equals(current.getContentType())){
                    holder.mBinding.txtMsg.setText("LOCATION");
                }else{
                    holder.mBinding.txtMsg.setText(current.getMessage());
                }

                holder.mBinding.txtMsgTime.setText(com.applozic.mobicommons.commons.core.utils.DateUtils.getFormattedDateAndTime(mContext,current.getCreatedAtTime(),1,1,1));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       /* Intent intent = new Intent(mContext, ConversationActivity.class);
                        intent.putExtra("TYPE", "CONTACT");
                        intent.putExtra("ID",current.getContactIds());
                        intent.putExtra("CHECK_INTENT","ACTIVITY");
                        mContext.startActivity(intent);*/
                    }
                });
                break;

        }


    }
    @Override
    public int getItemViewType(int position) {
        if(messageList.get(position).getGroupId() == null){
            return ONE_TO_ONE_CHAT;//This is a 1-to-1 message.
        }else{
            return GROUP_CHAT;//This is a Group message.
        }
    }
    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemMessageListBinding mBinding;

        public DataViewHolder(ItemMessageListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

    }
}
