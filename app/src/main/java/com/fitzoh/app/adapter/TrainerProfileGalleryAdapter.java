package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainerGalleryBinding;
import com.fitzoh.app.model.TrainerGalleryModel;
import com.fitzoh.app.ui.activity.ClientDocumentViewActivity;

import java.util.List;

public class TrainerProfileGalleryAdapter extends RecyclerView.Adapter<TrainerProfileGalleryAdapter.DataViewHolder> {

    Context context;
    List<TrainerGalleryModel.DataBean> trainerGalleryList;

    public TrainerProfileGalleryAdapter(Context context, List<TrainerGalleryModel.DataBean> trainerGalleryList) {
        this.context = context;
        this.trainerGalleryList = trainerGalleryList;
    }

    @NonNull
    @Override
    public TrainerProfileGalleryAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainerGalleryBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_trainer_gallery, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainerProfileGalleryAdapter.DataViewHolder holder, int position) {

        holder.bind(trainerGalleryList.get(position));
    }

    @Override
    public int getItemCount() {
        return trainerGalleryList.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainerGalleryBinding mBinding;

        DataViewHolder(ItemTrainerGalleryBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(TrainerGalleryModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            itemView.setOnClickListener(v -> context.startActivity(new Intent(context, ClientDocumentViewActivity.class).putExtra("data", dataBean.getImage())));
        }
    }
}
