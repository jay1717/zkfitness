package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientWorkoutListBinding;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.ui.activity.EditClientWorkoutActivity;
import com.fitzoh.app.ui.activity.TrainingProgramExerciseActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientWorkoutListAdapter extends RecyclerView.Adapter<ClientWorkoutListAdapter.DataViewHolder> {


    Context context;
    private List<WorkOutListModel.DataBean> workoutList;
    private DeleteWorkoutListener listener;

    public ClientWorkoutListAdapter(Context context, List<WorkOutListModel.DataBean> workoutList, DeleteWorkoutListener listener) {
        this.context = context;
        this.workoutList = workoutList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientWorkoutListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_workout_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.imgMenuHorizon.setVisibility(workoutList.get(position).getIs_client_created() == 1 ? View.VISIBLE : View.GONE);
        holder.mBinding.txtNoExcersiceValue.setText(String.valueOf(workoutList.get(position).getExercise_count()));
        holder.mBinding.txtNoSetsValue.setText(String.valueOf(workoutList.get(position).getNo_of_sets()));
        holder.mBinding.workoutName.setText(workoutList.get(position).getName());
        holder.mBinding.txtAssginedBy.setText(workoutList.get(position).getIs_client_created() == 1 ? "self" : workoutList.get(position).getAssign_by());
        holder.itemView.setOnClickListener(v -> {
            if (workoutList.get(position).getIs_client_created() == 1)
                context.startActivity(new Intent(context, EditClientWorkoutActivity.class).putExtra("data", workoutList.get(position)));
            else {
                Intent intent = new Intent(context, TrainingProgramExerciseActivity.class);
                intent.putExtra("workout_id", workoutList.get(position).getId());
                intent.putExtra("workout_name", workoutList.get(position).getName());
                intent.putExtra("isFromWorkout", true);
                if (workoutList.get(position).getIs_active() == 0) {
                    intent.putExtra("isActive", workoutList.get(position).getIs_active());
                }
                context.startActivity(intent);
            }
        });
        holder.mBinding.imgMenuHorizon.setOnClickListener(v -> showPopupMenu(holder.mBinding.imgMenuHorizon, position));

    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.menu_client_workout, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        popup.show();
    }

    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    context.startActivity(new Intent(context, EditClientWorkoutActivity.class).putExtra("data", workoutList.get(position)));
                    break;
                case R.id.action_track:
                    Intent intent = new Intent(context, TrainingProgramExerciseActivity.class);
                    intent.putExtra("workout_id", workoutList.get(position).getId());
                    intent.putExtra("workout_name", workoutList.get(position).getName());
                    intent.putExtra("isFromWorkout", true);
                    if (workoutList.get(position).getIs_active() == 0) {
                        intent.putExtra("isActive", workoutList.get(position).getIs_active());
                    }
                    context.startActivity(intent);
                    break;
                case R.id.action_delete:
                    listener.delete(workoutList.get(position));
                    break;
                case R.id.action_rename:
                    listener.rename(workoutList.get(position));
                    break;
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientWorkoutListBinding mBinding;

        public DataViewHolder(ItemClientWorkoutListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgMenuHorizon, R.drawable.ic_menu_horizon);
            mBinding.txtAssginedBy.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        }


    }

    public interface DeleteWorkoutListener {
        void delete(WorkOutListModel.DataBean dataBean);

        void rename(WorkOutListModel.DataBean dataBean);
    }
}
