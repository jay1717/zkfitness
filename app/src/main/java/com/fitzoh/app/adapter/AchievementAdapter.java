package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemAchievementBinding;

import java.util.List;

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.DataViewHolder>  {


    Context context;
    List<String> achievementModels;

    public AchievementAdapter(Context context, List<String> achievementModels) {
        this.context = context;
        this.achievementModels = achievementModels;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAchievementBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_achievement, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtAchivements.setText(achievementModels.get(position).toString());
    }

    @Override
    public int getItemCount() {
        return achievementModels.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemAchievementBinding mBinding;

        DataViewHolder(ItemAchievementBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }


    }
}
