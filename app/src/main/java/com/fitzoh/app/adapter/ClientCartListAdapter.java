package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientCartListBinding;
import com.fitzoh.app.model.ClientViewCartModel;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ClientCartListAdapter extends RecyclerView.Adapter<ClientCartListAdapter.DataViewHolder> {


    Context context;
    private List<ClientViewCartModel.DataBean> clientViewCart;


    public ClientCartListAdapter(Context context, List<ClientViewCartModel.DataBean> clientViewCart) {
        this.context = context;
        this.clientViewCart = clientViewCart;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientCartListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_cart_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtPrice.setText("\u20B9" + String.valueOf(clientViewCart.get(position).getPrice()));
        holder.mBinding.txtDiscountedPrice.setText("\u20B9" + String.valueOf(clientViewCart.get(position).getDiscounted_price()));
        holder.mBinding.txtName.setText(clientViewCart.get(position).getName());
        Utils.setImagePlaceHolder(context, holder.mBinding.imgProduct, clientViewCart.get(position).getImage());
        holder.mBinding.txtCount.setText(String.valueOf(clientViewCart.get(position).getQty()));
        holder.mBinding.imgPlus.setOnClickListener(v -> {
            int mInteger = clientViewCart.get(position).getQty() + 1;
            if(mInteger <= 5) {
                holder.mBinding.txtCount.setText(String.valueOf(mInteger));
                clientViewCart.get(position).setQty(mInteger);
            }
        });
        holder.mBinding.imgMinus.setOnClickListener(v -> {
            int mInteger = clientViewCart.get(position).getQty();
            if (mInteger > 1) {
                mInteger = mInteger - 1;
                holder.mBinding.txtCount.setText(String.valueOf(mInteger));
                clientViewCart.get(position).setQty(mInteger);
            }
        });

    }

    public JSONArray getData() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < clientViewCart.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("product_id", clientViewCart.get(i).getProduct_id());
                jsonObject.put("qty", clientViewCart.get(i).getQty());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    @Override
    public int getItemCount() {
        return clientViewCart.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientCartListBinding mBinding;

        public DataViewHolder(ItemClientCartListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgPlus, R.drawable.ic_plus_icon);
            Utils.setImageBackground(context, mBinding.imgMinus, R.drawable.ic_minus);
            mBinding.txtPrice.setPaintFlags(mBinding.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        }
    }

}
