package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.model.YouTubeModel.Item;
import com.fitzoh.app.databinding.ItemSearchYoutubeExerciseBinding;
import com.fitzoh.app.ui.activity.CreateExerciseActivity;

import java.util.ArrayList;
import java.util.List;

import static com.fitzoh.app.utils.Utils.setImagePlaceHolder;


public class SearchYoutubeExerciseAdapter extends RecyclerView.Adapter<SearchYoutubeExerciseAdapter.DataViewHolder> implements Filterable {


    private Context context;
    List<Item> items = new ArrayList<>();

    public SearchYoutubeExerciseAdapter(Context context, List<Item> items) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSearchYoutubeExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_search_youtube_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(items.get(position));
        holder.mBinding.youtubeCon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("youTube_id", items.get(position).getId().getVideoId());
                intent.putExtra("youTubeTitle", items.get(position).getSnippet().getTitle());
                intent.putExtra("youTube_Image",items.get(position).getSnippet().getThumbnails().getDefault().getUrl());
                intent.putExtra("toutube_Dec",items.get(position).getSnippet().getDescription());
                ((Activity)context).setResult(0,intent);
                ((Activity)context).finish();
            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        public ItemSearchYoutubeExerciseBinding mBinding;

        public DataViewHolder(ItemSearchYoutubeExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(Item item) {
            mBinding.executePendingBindings();
            setImagePlaceHolder(context, mBinding.imgExercise, item.getSnippet().getThumbnails().getMedium().getUrl());
            mBinding.txtExerciseName.setText(item.getSnippet().getTitle());
        }

    }
}
