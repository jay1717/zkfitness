package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemNutritionListBinding;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class NutritionListAdapter extends RecyclerView.Adapter<NutritionListAdapter.DataViewHolder> {


    Context context;
    private onAddClient onAddClient;
    private List<DietListModel.DataBean> dietList;
    private DeleteWorkOut deleteWorkOut;
    String microNutrition;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public NutritionListAdapter(Context context, NutritionListAdapter.onAddClient onAddClient, List<DietListModel.DataBean> dietList, DeleteWorkOut deleteWorkOut) {
        this.context = context;
        this.onAddClient = onAddClient;
        this.dietList = dietList;
        this.deleteWorkOut = deleteWorkOut;
    }
    public NutritionListAdapter() {
        viewBinderHelper.setOpenOnlyOne(true);
    }
    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemNutritionListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_nutrition_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        viewBinderHelper.bind(holder.mBinding.swipeLayout, String.valueOf(dietList.get(position).getId()));
        holder.mBinding.recyclerView.setAdapter(new AssignedDietUserListAdapter(context, dietList.get(position).getAssigned_to()));
        holder.mBinding.txtNoMealsValue.setText(String.valueOf(dietList.get(position).getNo_of_meals()));
        //  holder.mBinding.txtNoMeals.setText(String.valueOf(workoutList.get(position).getNo_of_sets()));
        holder.mBinding.nutritionName.setText(dietList.get(position).getName());


        if (position % 2 == 0)
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        else
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));

       /* microNutritions.add("Calories");
        microNutritions.add("Protien");
        microNutritions.add("Carbs");
        microNutritions.add("Fat");*/
        holder.mBinding.recyclerNutrition.setAdapter(new MicroNutritionAdapter(dietList.get(position).getDiet_plan_macro_nutrient()));

        microNutrition = "";
        int size = dietList.get(position).getDiet_plan_macro_nutrient().size();
        for (int i = 0; i < size; i++) {
            microNutrition = microNutrition + "" + dietList.get(position).getDiet_plan_macro_nutrient().get(i).toString() + ",";

        }
        if (microNutrition != "" && microNutrition.length() > 0 && microNutrition.endsWith(",")) {
            microNutrition = microNutrition.substring(0, microNutrition.length() - 1);
        }
        holder.mBinding.txtMicronutritionValue.setText(microNutrition);


    }

    @Override
    public int getItemCount() {
        return dietList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemNutritionListBinding mBinding;

        public DataViewHolder(ItemNutritionListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);
            Utils.setImageBackground(context, mBinding.imgMenuHorizon, R.drawable.ic_menu_horizon);
            mBinding.frontLayout.setOnClickListener(view -> {
                onAddClient.edit(dietList.get(getAdapterPosition()).getId(), dietList.get(getAdapterPosition()).getName());
            });
            mBinding.recyclerView.addItemDecoration(new OverlapDecoration());
            mBinding.imgAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onAddClient.add(dietList.get(getAdapterPosition()).getId());
                }
            });
            mBinding.imgMenuHorizon.setOnClickListener(view -> {
                showPopupMenu(mBinding.imgMenuHorizon, getAdapterPosition());
            });
            mBinding.deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    deleteWorkOut.delete(dietList.get(getAdapterPosition()));
                }
            });
            mBinding.recyclerNutrition.setLayoutManager(new LinearLayoutManager(context));
        }

    }


    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -40;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            if (itemPosition == 0) {
                return;
            }
            outRect.set(vertOverlap, 0, 0, 0);


        }
    }

    public interface onAddClient {
        void add(int id);

        void edit(int id, String name);

        void rename(DietListModel.DataBean dataBean);
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(R.menu.menu_worout_actions, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        popup.show();
    }


    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    onAddClient.edit(dietList.get(position).getId(), dietList.get(position).getName());
                    break;
                case R.id.action_rename:
                    onAddClient.rename(dietList.get(position));
                    break;
            }
            return false;
        }
    }

    public interface DeleteWorkOut {
        void delete(DietListModel.DataBean dietListModel);
    }
}
