package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemPackagesBinding;
import com.fitzoh.app.model.PackageListModel;
import com.fitzoh.app.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PackagesListAdapter extends RecyclerView.Adapter<PackagesListAdapter.DataViewHolder> {


    List<PackageListModel.DataBean> packageList;
    Context context;
    private onAction onAction;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public PackagesListAdapter(List<PackageListModel.DataBean> packageList, Context context, PackagesListAdapter.onAction onAction) {
        this.packageList = packageList;
        this.context = context;
        this.onAction = onAction;
    }

    public PackagesListAdapter() {
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPackagesBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_packages, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(packageList.get(position));
        viewBinderHelper.bind(holder.mBinding.swipeLayout, String.valueOf(packageList.get(position).getId()));
        holder.mBinding.txtRupee.setText("Price : " + "$" + packageList.get(position).getPrice());
        holder.mBinding.txtDiscountedRupee.setText("Discounted Price : " + "$" + packageList.get(position).getDiscounted_price());
        holder.mBinding.txtWeek.setText("No. of weeks : " + packageList.get(position).getWeeks());
        if (packageList.get(position).getMedia_type().equals("Image")) {
            Log.e("Image: ", "" + packageList.get(position).getMedia());
            Utils.setImagePlaceHolder(context, holder.mBinding.imgPackage, packageList.get(position).getMedia());
        } else if (packageList.get(position).getMedia_type().equals("Video")) {
            Log.e("Video: ", "check");
            holder.mBinding.imgPackage.setImageResource(R.drawable.placeholder);
        } else if (packageList.get(position).getMedia_type().equals("Youtube")) {
            Log.e("path: ", packageList.get(position).getYoutube_url());
            String videoId = "";
            try {
                videoId = extractYTId(packageList.get(position).getYoutube_url());
                if (!videoId.equals("")) {
//                    String image = String.format(context.getString(R.string.youtube_image), videoId);
                    String image = "https://img.youtube.com/vi/" + videoId + "/0.jpg";
                    Log.e("onBindViewHolder: ", image);
                    Utils.setImagePlaceHolder(context, holder.mBinding.imgPackage, image);
                } else {
                    Log.e("videoId.equals: ", "empty");
                    holder.mBinding.imgPackage.setImageResource(R.drawable.placeholder);
                }
            } catch (MalformedURLException e) {
                Log.e("videoId.equals: ", "catch");
                e.printStackTrace();
                holder.mBinding.imgPackage.setImageResource(R.drawable.placeholder);
            }
        }
    }


    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemPackagesBinding mBinding;

        public DataViewHolder(ItemPackagesBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgEdit, R.drawable.ic_edit);
            mBinding.deleteLayout.setOnClickListener(v -> onAction.delete(packageList.get(getAdapterPosition())));
            mBinding.imgEdit.setOnClickListener(v -> onAction.edit(packageList.get(getAdapterPosition())));
            mBinding.txtRupee.setPaintFlags(mBinding.txtRupee.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        public void bind(PackageListModel.DataBean packageListModel) {
            mBinding.setItem(packageListModel);
            mBinding.executePendingBindings();
        }
    }

    public interface onAction {

        void edit(PackageListModel.DataBean packageListModel);

        void delete(PackageListModel.DataBean packageListModel);
    }

    public String extractYTId(String ytUrl) throws MalformedURLException {
        String vId = "";
        Pattern pattern = Pattern.compile(
                "(?:https?:/{2})?(?:w{3}.)?youtu(?:be)?.(?:com|be)(?:/watch?v=|/)([^s&]+)",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
            if (vId.startsWith("watch?v=")) {
                vId = vId.replace("watch?v=", "");
            } else if (vId.equals("")) {
                vId = extractYoutubeId(ytUrl);
            }
        }
        return vId;
    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = "";
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

}
