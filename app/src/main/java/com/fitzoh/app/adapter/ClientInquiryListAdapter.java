package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientInquiryBinding;
import com.fitzoh.app.model.TrainerProductInquiryListing;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientInquiryListAdapter extends RecyclerView.Adapter<ClientInquiryListAdapter.DataViewHolder> {


    Context context;
    private dataPassing dataPassing;
    private List<TrainerProductInquiryListing.DataBean> productInquiry;


    public ClientInquiryListAdapter(Context context, dataPassing dataPassing, List<TrainerProductInquiryListing.DataBean> productInquiry) {
        this.context = context;
        this.dataPassing = dataPassing;
        this.productInquiry = productInquiry;

    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientInquiryBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_inquiry, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtName.setText(productInquiry.get(position).getClient_name());
        Utils.setImage(context, holder.mBinding.imgClient, productInquiry.get(position).getClient_photo());
        holder.mBinding.txtCount.setText("No. of inquiries : " + productInquiry.get(position).getNo_of_inquiries());
    }

    @Override
    public int getItemCount() {
        return productInquiry.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientInquiryBinding mBinding;

        public DataViewHolder(ItemClientInquiryBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgChat, R.drawable.ic_chat_icon);
            itemView.setOnClickListener(v -> dataPassing.data(productInquiry.get(getAdapterPosition())));

        }

    }

    public interface dataPassing {
        void data(TrainerProductInquiryListing.DataBean dataBean);

    }

}
