package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.CheckboxClientAssignBinding;
import com.fitzoh.app.model.ClientListModel;
import com.fitzoh.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ClientAssignSelectoinAdapter extends
        RecyclerView.Adapter<ClientAssignSelectoinAdapter.ViewHolder> {


    Context context;
    private List<ClientListModel.DataBean> clientAssignModels;
    private onSelect onSelect;
    ArrayList<String> daysName = new ArrayList<>();
    int selectedClientId;
    JSONObject jsonObject;
    JSONArray jsonArray = new JSONArray();


    public ClientAssignSelectoinAdapter(Context context, List<ClientListModel.DataBean> clientAssignModels, ClientAssignSelectoinAdapter.onSelect onSelect) {
        this.context = context;
        this.clientAssignModels = clientAssignModels;
        this.onSelect = onSelect;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CheckboxClientAssignBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.checkbox_client_assign, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    /*public List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < clientAssignModels.size(); i++) {
            if (clientAssignModels.get(i).getIs_assigned() == 1)
                list.add(clientAssignModels.get(i).getId());
        }
        return list;
    }*/
    public JSONArray getData() {
        return jsonArray;
    }

    public void setDays(ArrayList<String> days, int clientId) {
        daysName.addAll(days);
        selectedClientId = clientId;

        jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", clientId);
            JSONArray list = new JSONArray(days);
            jsonObject.put("day", list);
            jsonObject.put("is_assigned",1);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUnckeckedId(int clientId) {

        jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", clientId);
            jsonObject.put("is_assigned",0);
            jsonArray.put(jsonObject);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount() {
        return clientAssignModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckboxClientAssignBinding mBinding;

        public ViewHolder(CheckboxClientAssignBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkboxClientAssign, R.drawable.checked);
        }

        @Override
        public void onClick(View v) {

        }

        public void bind(int position) {
            mBinding.checkboxClientAssign.setText(clientAssignModels.get(position).getName());
            mBinding.checkboxClientAssign.setChecked(clientAssignModels.get(position).getIs_assigned() == 1);
            itemView.setOnClickListener(this);
            mBinding.checkboxClientAssign.setOnCheckedChangeListener((buttonView, isChecked) -> {
                ClientListModel.DataBean dataBean = clientAssignModels.get(position);
                onSelect.select(clientAssignModels.get(position).getId(), isChecked);
                dataBean.setIs_assigned(isChecked ? 1 : 0);
                clientAssignModels.set(position, dataBean);

            });
        }
    }

    public interface onSelect {
        void select(int clientId, boolean isChecked);
    }


}
