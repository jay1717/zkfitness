package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTransformationBinding;
import com.fitzoh.app.interfaces.OnRecyclerViewClickListener;
import com.fitzoh.app.model.TransformationListModel;

import java.util.List;


public class TrainerProfileTransformationAdapter extends RecyclerView.Adapter<TrainerProfileTransformationAdapter.DataViewHolder> {

    Context context;
    List<TransformationListModel.DataBean> transformationDataBeanList;
    OnRecyclerViewClickListener listner;

    public TrainerProfileTransformationAdapter(Context context, List<TransformationListModel.DataBean> transformationDataBeanList, OnRecyclerViewClickListener listner) {
        this.context = context;
        this.transformationDataBeanList = transformationDataBeanList;
        this.listner=listner;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

       ItemTransformationBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_transformation, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(transformationDataBeanList.get(position));
    }

    @Override
    public int getItemCount() {
        return transformationDataBeanList.size();
    }

   public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTransformationBinding mBinding;

        DataViewHolder(ItemTransformationBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.getRoot().setOnClickListener(view->{
                if(listner!=null){
                    listner.onItemClicked(getAdapterPosition());
                }
            });

        }
        public void bind(TransformationListModel.DataBean transformation) {
            mBinding.setItem(transformation);
            mBinding.executePendingBindings();
        }
    }
    public void removeItem(int position) {
        transformationDataBeanList.remove(position);
        notifyItemRemoved(position);
    }

}
