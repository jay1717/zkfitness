package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemDetailCheckinImageBinding;
import com.fitzoh.app.model.CheckInFormListModel;

import java.util.List;

public class DetailCheckInImageAdapter extends RecyclerView.Adapter<DetailCheckInImageAdapter.DataViewHolder> {

    Context context;
    List<CheckInFormListModel.DataBean.ImagesBean> certificationList;


    public DetailCheckInImageAdapter(Context context, List<CheckInFormListModel.DataBean.ImagesBean> certificationList) {
        this.context = context;
        this.certificationList = certificationList;
    }

    @NonNull
    @Override
    public DetailCheckInImageAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDetailCheckinImageBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_detail_checkin_image, parent, false);
        return new DataViewHolder(mBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull DetailCheckInImageAdapter.DataViewHolder holder, int position) {
        holder.bind(certificationList.get(position));
    }

    @Override
    public int getItemCount() {
        return certificationList.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemDetailCheckinImageBinding mBinding;

        DataViewHolder(ItemDetailCheckinImageBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(CheckInFormListModel.DataBean.ImagesBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }
}
