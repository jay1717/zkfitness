package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainerAboutUsBinding;
import com.fitzoh.app.model.TrainerProfileModel;

import java.util.List;

public class TrainerAboutUsAdapter extends RecyclerView.Adapter<TrainerAboutUsAdapter.DataViewHolder> {

    Context context;
    List<TrainerProfileModel.DataBean.FacilitiesBean> trainerProfileFacility;

    public TrainerAboutUsAdapter(Context context, List<TrainerProfileModel.DataBean.FacilitiesBean> trainerProfileFacility) {
        this.context = context;
        this.trainerProfileFacility = trainerProfileFacility;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainerAboutUsBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_trainer_about_us, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(trainerProfileFacility.get(position));
    }

    @Override
    public int getItemCount() {
        return trainerProfileFacility.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainerAboutUsBinding mBinding;

        DataViewHolder(ItemTrainerAboutUsBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
        public void bind(TrainerProfileModel.DataBean.FacilitiesBean trainerFacility) {
            mBinding.setItem(trainerFacility);
            mBinding.executePendingBindings();
        }
    }
}
