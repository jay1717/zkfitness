package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientNutritionListBinding;
import com.fitzoh.app.model.DietListModel;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.ui.activity.EditClientWorkoutActivity;
import com.fitzoh.app.ui.activity.TrainingProgramExerciseActivity;

import java.util.List;

public class ClientNutritionListAdapter extends RecyclerView.Adapter<ClientNutritionListAdapter.DataViewHolder> {


    Context context;
    private List<DietListModel.DataBean> dietList;
    String microNutrition;
    UnAssign unAssign;

    public ClientNutritionListAdapter(Context context, List<DietListModel.DataBean> dietList, UnAssign unAssign) {
        this.context = context;
        this.dietList = dietList;
        this.unAssign = unAssign;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemClientNutritionListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_nutrition_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.imgMenuHorizon.setVisibility(dietList.get(position).getIs_assigned() ==1 ? View.GONE : View.VISIBLE);
        holder.mBinding.txtNoMealsValue.setText(String.valueOf(dietList.get(position).getNo_of_meals()));
        //  holder.mBinding.txtNoMeals.setText(String.valueOf(workoutList.get(position).getNo_of_sets()));
        holder.mBinding.nutritionName.setText(dietList.get(position).getName());

        microNutrition = "";
        int size = dietList.get(position).getDiet_plan_macro_nutrient().size();
        for (int i = 0; i < size; i++) {
            microNutrition = microNutrition + "" + dietList.get(position).getDiet_plan_macro_nutrient().get(i).toString() + ",";

        }
        if (microNutrition != "" && microNutrition.length() > 0 && microNutrition.endsWith(",")) {
            microNutrition = microNutrition.substring(0, microNutrition.length() - 1);
        }
        holder.mBinding.txtMicronutritionValue.setText(microNutrition);
        holder.mBinding.txtAssginedBy.setText(dietList.get(position).getIs_assigned() == 0 ? "self" : dietList.get(position).getAssign_by());
        holder.mBinding.imgMenuHorizon.setOnClickListener(v -> showPopupMenu(holder.mBinding.imgMenuHorizon, position));

    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.menu_client_workout, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        popup.getMenu().getItem(0).setVisible(false);
        popup.getMenu().getItem(1).setVisible(false);

        popup.show();
    }

    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {

                case R.id.action_delete:
                    unAssign.remove(dietList.get(position));

                    //  listener.delete(workoutList.get(position));
                    break;

                case R.id.action_rename:
                    unAssign.rename(dietList.get(position));
                    break;
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return dietList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemClientNutritionListBinding mBinding;

        public DataViewHolder(ItemClientNutritionListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(view -> {
                unAssign.edit(dietList.get(getAdapterPosition()));
            });
        }

    }

    public interface UnAssign {
        void remove(DietListModel.DataBean dietListModel);

        void edit(DietListModel.DataBean dietListModel);

        void rename(DietListModel.DataBean dataBean);
    }
}
