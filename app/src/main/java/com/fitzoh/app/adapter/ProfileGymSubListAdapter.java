package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemProfileGymSubscriptionBinding;
import com.fitzoh.app.model.ProfileGymSubscriptionModel;

import java.util.List;

public class ProfileGymSubListAdapter extends RecyclerView.Adapter<ProfileGymSubListAdapter.DataViewHolder> {


    List<ProfileGymSubscriptionModel.DataBean> subscriptionList;
    Context context;
    private onDataPass onDataPass;

    public ProfileGymSubListAdapter(List<ProfileGymSubscriptionModel.DataBean> packageList, Context context, ProfileGymSubListAdapter.onDataPass onDataPass) {
        this.subscriptionList = packageList;
        this.context = context;
        this.onDataPass = onDataPass;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemProfileGymSubscriptionBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_profile_gym_subscription, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(subscriptionList.get(position));
        holder.mBinding.txtRupee.setText("$" + subscriptionList.get(position).getPrice());
        holder.mBinding.txtRupee.setPaintFlags(holder.mBinding.txtRupee.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mBinding.txtDiscountedRupee.setText("$" + subscriptionList.get(position).getDiscount_price());
    }


    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemProfileGymSubscriptionBinding mBinding;

        public DataViewHolder(ItemProfileGymSubscriptionBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.constMain.setOnClickListener(view -> onDataPass.pass(subscriptionList.get(getAdapterPosition())));
        }


        public void bind(ProfileGymSubscriptionModel.DataBean subscirptionListModel) {
            mBinding.setItem(subscirptionListModel);
            mBinding.executePendingBindings();
        }


    }

    public interface onDataPass {
        void pass(ProfileGymSubscriptionModel.DataBean dataBean);

    }

}
