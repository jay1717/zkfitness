package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTodaysWorkoutBinding;
import com.fitzoh.app.model.GetClientDetailModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class TodaysWorkoutAdapter extends RecyclerView.Adapter<TodaysWorkoutAdapter.DataViewHolder> {
    private Context context;
    private List<GetClientDetailModel.DataBean.TodaysWorkoutBean> todaysWorkoutBeans;
    TodaysWorkoutListener listener;

    public TodaysWorkoutAdapter(Context context, List<GetClientDetailModel.DataBean.TodaysWorkoutBean> todaysWorkoutBeans, TodaysWorkoutListener listener) {
        this.context = context;
        this.todaysWorkoutBeans = todaysWorkoutBeans;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTodaysWorkoutBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_todays_workout, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(todaysWorkoutBeans.get(position));
    }

    @Override
    public int getItemCount() {
        return todaysWorkoutBeans.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTodaysWorkoutBinding mBinding;

        public DataViewHolder(ItemTodaysWorkoutBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.getShapeGradient(context, mBinding.btnStartWorkout);
            mBinding.btnStartWorkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onStart(todaysWorkoutBeans.get(getAdapterPosition()));
                }
            });
        }

        public void bind(GetClientDetailModel.DataBean.TodaysWorkoutBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            if(dataBean.isIs_finished()==0)
                mBinding.btnStartWorkout.setText("Start");
            else if(dataBean.isIs_finished()==1)
                mBinding.btnStartWorkout.setText("Continue");
        }
    }

    public interface TodaysWorkoutListener {
        public void onStart(GetClientDetailModel.DataBean.TodaysWorkoutBean workoutBean);
    }
}
