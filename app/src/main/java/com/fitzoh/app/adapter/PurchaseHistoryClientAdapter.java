package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemPurchaseHistoryClientBinding;
import com.fitzoh.app.model.PurchaseHistoryModel;
import com.fitzoh.app.utils.Utils;

import java.net.MalformedURLException;
import java.util.List;
import java.util.TimeZone;

public class PurchaseHistoryClientAdapter extends RecyclerView.Adapter<PurchaseHistoryClientAdapter.DataViewHolder> {


    List<PurchaseHistoryModel.DataBean.OwnPlanBean> packageList;
    Context context;

    public PurchaseHistoryClientAdapter(List<PurchaseHistoryModel.DataBean.OwnPlanBean> packageList, Context context) {
        this.packageList = packageList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPurchaseHistoryClientBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_purchase_history_client, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(packageList.get(position).getPackage_name());
        holder.mBinding.txtDescription.setText(packageList.get(position).getDesc());
        holder.mBinding.txtWeek.setText("No. of weeks : " + packageList.get(position).getWeeks());
        holder.mBinding.txtDate.setText(packageList.get(position).getPurchase_date());
        holder.mBinding.txtRupee.setText("Price : " + packageList.get(position).getDiscounted_price());

        if (packageList.get(position).getMedia_type().equalsIgnoreCase("Image")) {
            Utils.setImagePlaceHolder(context, holder.mBinding.imgPackage, packageList.get(position).getMedia_name());
        } else if (packageList.get(position).getMedia_type().equalsIgnoreCase("Youtube")) {
            String videoId = "";
            try {
                videoId = Utils.extractYTId(packageList.get(position).getMedia_name());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            if (!videoId.equalsIgnoreCase("")) {
                String img_url = "http://img.youtube.com/vi/" + videoId + "/0.jpg"; // this is link which will give u thumnail image of that video
                Utils.setImagePlaceHolder(context, holder.mBinding.imgPackage, img_url);
            } else {
                holder.mBinding.imgPackage.setImageResource(R.drawable.placeholder);
            }
        } else {
            holder.mBinding.imgPackage.setImageResource(R.drawable.placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemPurchaseHistoryClientBinding mBinding;

        public DataViewHolder(ItemPurchaseHistoryClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

    }
}
