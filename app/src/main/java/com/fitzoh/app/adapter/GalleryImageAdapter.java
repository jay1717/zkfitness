package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.RowUploadImageBinding;
import com.fitzoh.app.model.TrainerGalleryModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class GalleryImageAdapter extends RecyclerView.Adapter<GalleryImageAdapter.DataViewHolder> {
/*Testing file*/
    Context context;
    List<TrainerGalleryModel.DataBean> trainerGalleryList;
    RemoveImageListener listener;

    public GalleryImageAdapter(Context context, List<TrainerGalleryModel.DataBean> trainerGalleryList, RemoveImageListener listener) {
        this.context = context;
        this.trainerGalleryList = trainerGalleryList;
        this.listener = listener;

    }

    @NonNull
    @Override
    public GalleryImageAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowUploadImageBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_upload_image, parent, false);
        return new GalleryImageAdapter.DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryImageAdapter.DataViewHolder holder, int position) {
        if (trainerGalleryList.get(position).isFromGallery()) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder);
            Glide.with(holder.mBinding.imgUpload.getContext())
                    .load(Uri.parse(trainerGalleryList.get(position).getImage()))
                    .apply(options)
                    .into(holder.mBinding.imgUpload);
//            holder.mBinding.imgUpload.setImageURI(Uri.parse(trainerGalleryList.get(position).getImage()));
        } else {
            Utils.setImagePlaceHolder(context, holder.mBinding.imgUpload, trainerGalleryList.get(position).getImage());
        }

        holder.mBinding.imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    AlertDialog alertDialog = new AlertDialog.Builder(context)
                            .setTitle("Delete Alert")
                            .setMessage("Are you sure you want to delete?")
                            .setPositiveButton(R.string.ok, (dialog, whichButton) -> {
                                dialog.dismiss();
                                listener.removeImage(position);
                            })
                            .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                            })
                            .show();
                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                    alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                   /* arrayList.remove(position);
                    notifyDataSetChanged();*/
                }

            }
        });

        //   holder.bind(trainerGalleryList.get(position));
    }

    @Override
    public int getItemCount() {
        return trainerGalleryList.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        RowUploadImageBinding mBinding;

        DataViewHolder(RowUploadImageBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
      /*  public void bind(TrainerGalleryModel.DataBean dataBean) {
            //chekif
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }*/
    }

    public interface RemoveImageListener {
        void removeImage(int position);
    }
}

