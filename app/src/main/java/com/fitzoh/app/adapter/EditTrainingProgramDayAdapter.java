package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemEditTrainingProgramDayBinding;
import com.fitzoh.app.model.TrainingProgramDay;
import com.fitzoh.app.ui.activity.CopyDayActivity;
import com.fitzoh.app.ui.activity.EditTrainingProgramActivity;
import com.fitzoh.app.ui.activity.SelectWorkoutActivity;
import com.fitzoh.app.ui.fragment.EditTrainingProgramFragment;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class EditTrainingProgramDayAdapter extends RecyclerView.Adapter<EditTrainingProgramDayAdapter.DataViewHolder> {

    private List<TrainingProgramDay> daysList;
    Activity context;
    private int week;
    TrainingProgramListener listener;
    FindTrainingProgramWeekAdapter.RedirectToDetailListener redirectListner;

    EditTrainingProgramDayAdapter(Activity context, List<TrainingProgramDay> daysList, int week, TrainingProgramListener listener,FindTrainingProgramWeekAdapter.RedirectToDetailListener redirectListner) {
        this.context = context;
        this.daysList = daysList;
        this.week = week;
        this.listener = listener;
        this.redirectListner=redirectListner;
    }


    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemEditTrainingProgramDayBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_edit_training_program_day, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemEditTrainingProgramDayBinding mBinding;

        public DataViewHolder(ItemEditTrainingProgramDayBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgCopy, R.drawable.ic_copy);
            Utils.setImageBackground(context, mBinding.imgAdd, R.drawable.ic_plus_training_program);
            mBinding.imgAMWorkout.setOnClickListener(view -> showPopupMenu(view, getAdapterPosition(), R.menu.menu_training_program_days));
            mBinding.imgPMWorkout.setOnClickListener(view -> showPopupMenu(view, getAdapterPosition(), R.menu.menu_training_program_days));
            mBinding.imgNutritionPlan.setOnClickListener(view -> showPopupMenu(view, getAdapterPosition(), R.menu.menu_training_program_days));
            mBinding.imgCheckIn.setOnClickListener(view -> showPopupMenu(view, getAdapterPosition(), R.menu.menu_training_program_days));
            mBinding.imgAdd.setOnClickListener(view -> showPopupMenu(view, getAdapterPosition(), R.menu.menu_training_program));
            mBinding.imgCopy.setOnClickListener(view -> {
                /*if (daysList.size() < 7) {
                    TrainingProgramDay day = new TrainingProgramDay();
                    TrainingProgramDay dayOld = daysList.get(getAdapterPosition());
                    day.setId(0);
                    day.setIs_restfull_day(dayOld.getIs_restfull_day());
                    day.setTraining_week_id(dayOld.getTraining_week_id());
                    day.setDay_number(dayOld.getDay_number());
                    day.setDiet_plan_id(dayOld.getDiet_plan_id());
                    day.setWorkout_am_id(dayOld.getWorkout_am_id());
                    day.setWorkout_pm_id(dayOld.getWorkout_pm_id());
                    day.setDiet_plan_name(dayOld.getDiet_plan_name());
                    day.setWorkout_am_name(dayOld.getWorkout_am_name());
                    day.setWorkout_pm_name(dayOld.getWorkout_pm_name());
                    *//*try {
                        day = (TrainingProgramDay) dayOld.clone();
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }*//*
                    daysList.add(day);
                    notifyItemChanged(daysList.size());
                } else
                    Toast.makeText(context, "You can not add more than 7 days in week", Toast.LENGTH_LONG).show();*/
                EditTrainingProgramFragment fragment = (EditTrainingProgramFragment) ((EditTrainingProgramActivity) context).getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
                context.startActivityForResult(new Intent(context, CopyDayActivity.class).putParcelableArrayListExtra("data", fragment.recyclerViewAdapter.weekList).putExtra("day", getAdapterPosition()).putExtra("week", week), 200);
            });
            mBinding.deleteLayout.setOnClickListener(v -> {
                mBinding.swipeLayout.close(false);
                listener.delete("day", daysList.get(getAdapterPosition()).getId());
                daysList.remove(getAdapterPosition());
//                notifyDataSetChanged();
                listener.refresh(week);
//                notifyItemRemoved(daysList.size() - 1);
            });
        }

        public void bind(int position) {
            TrainingProgramDay day = daysList.get(position);

            mBinding.txtAMLogged.setOnClickListener(view -> {
                redirectListner.viewLogWorkout(day.getWorkout_am_id(), day.getWorkout_am_name(), true, day.getId());
            });

            mBinding.txtPMLogged.setOnClickListener(view -> {
                redirectListner.viewLogWorkout(day.getWorkout_pm_id(), day.getWorkout_pm_name(), false, day.getId());
            });

            mBinding.dayName.setText("Day " + (position + 1));
            if (day.getIs_restfull_day() == 1) {
                mBinding.txtRestDay.setVisibility(View.VISIBLE);
                mBinding.txtAMWorkout.setVisibility(View.GONE);
                mBinding.txtPMWorkout.setVisibility(View.GONE);
                mBinding.txtNutritionPlan.setVisibility(View.GONE);
                mBinding.txtCheckIn.setVisibility(View.GONE);
                mBinding.imgAMWorkout.setVisibility(View.GONE);
                mBinding.imgPMWorkout.setVisibility(View.GONE);
                mBinding.imgNutritionPlan.setVisibility(View.GONE);
                mBinding.imgCheckIn.setVisibility(View.GONE);
            } else {
                mBinding.txtAMWorkout.setText(day.getWorkout_am_name() + " (AM)");
                mBinding.txtPMWorkout.setText(day.getWorkout_pm_name() + " (PM)");
                mBinding.txtNutritionPlan.setText(day.getDiet_plan_name());
                mBinding.txtRestDay.setVisibility(View.GONE);
                checkVisibility(mBinding.txtAMWorkout, mBinding.imgAMWorkout, mBinding.txtAMLogged, day.getWorkout_am_id(), day.getWorkout_am_logged());
                checkVisibility(mBinding.txtPMWorkout, mBinding.imgPMWorkout, mBinding.txtPMLogged, day.getWorkout_pm_id(), day.getWorkout_pm_logged());
                checkVisibility(mBinding.txtNutritionPlan, mBinding.imgNutritionPlan, mBinding.txtDietLogged, day.getDiet_plan_id(), day.getDiet_plan_logged());
                checkVisibility(mBinding.txtCheckIn, mBinding.imgCheckIn, mBinding.txtCheckLogged, day.getIs_checkin(), day.getCheckin_logged());
            }
        }
    }

    private void checkVisibility(TextView textView, ImageView imageView, TextView textLogged, int value, int isLogged) {
        if (value == 0) {
            textView.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
        } else {

            textView.setVisibility(View.VISIBLE);
            if (isLogged == 0) {
                textLogged.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);
            } else {
                textLogged.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);

            }

        }
    }

    private void showPopupMenu(View view, int position, int menu) {
        PopupMenu popup = new PopupMenu(context, view);
        // MenuInflater inflater = popup.getMenuInflater();
        popup.getMenuInflater().inflate(menu, popup.getMenu());
        Menu menu1 = popup.getMenu();
        if (view.getId() == R.id.imgAdd) {
            menu1.findItem(R.id.action_am_workout).setVisible(daysList.get(position).getWorkout_am_id() == 0);
            menu1.findItem(R.id.action_pm_workout).setVisible(daysList.get(position).getWorkout_pm_id() == 0);
            menu1.findItem(R.id.action_nutrition_plan).setVisible(daysList.get(position).getDiet_plan_id() == 0);
            menu1.findItem(R.id.action_rest_day).setVisible(daysList.get(position).getIs_restfull_day() == 0);
            menu1.findItem(R.id.action_check_in).setVisible(daysList.get(position).getIs_checkin() == 0);
        } else if (view.getId() == R.id.imgCheckIn) {
            menu1.findItem(R.id.action_edit).setVisible(false);
        }
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position, view));
        popup.show();
    }


    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;
        private View view;

        MenuActionItemClickListener(int position, View view) {
            this.position = position;
            this.view = view;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_am_workout:
                    context.startActivityForResult(new Intent(context, SelectWorkoutActivity.class).putExtra("select", 1).putExtra("day", position).putExtra("week", week), 0);
                    break;
                case R.id.action_pm_workout:
                    context.startActivityForResult(new Intent(context, SelectWorkoutActivity.class).putExtra("select", 2).putExtra("day", position).putExtra("week", week), 100);
                    break;
                case R.id.action_nutrition_plan:
                    context.startActivityForResult(new Intent(context, SelectWorkoutActivity.class).putExtra("select", 3).putExtra("day", position).putExtra("week", week), 500);
                    break;
                case R.id.action_rest_day:
                    TrainingProgramDay day = new TrainingProgramDay();
                    day.setId(daysList.get(position).getId());
                    day.setIs_restfull_day(1);
                    daysList.set(position, day);
                    notifyItemChanged(position);
                    break;
                case R.id.action_check_in:
                    TrainingProgramDay dayCheck = daysList.get(position);
                    dayCheck.setIs_restfull_day(0);
                    dayCheck.setIs_checkin(1);
                    daysList.set(position, dayCheck);
                    listener.refresh(week);
                    break;
                case R.id.action_edit:
                    switch (view.getId()) {
                        case R.id.imgAMWorkout:
                            context.startActivityForResult(new Intent(context, SelectWorkoutActivity.class).putExtra("select", 1).putExtra("day", position).putExtra("week", week), 0);
                            break;
                        case R.id.imgPMWorkout:
                            context.startActivityForResult(new Intent(context, SelectWorkoutActivity.class).putExtra("select", 2).putExtra("day", position).putExtra("week", week), 100);
                            break;
                        case R.id.imgNutritionPlan:
                            context.startActivityForResult(new Intent(context, SelectWorkoutActivity.class).putExtra("select", 3).putExtra("day", position).putExtra("week", week), 500);
                            break;
                    }
                    break;
                case R.id.action_delete:
                    switch (view.getId()) {
                        case R.id.imgAMWorkout:
                            TrainingProgramDay am = daysList.get(position);
                            am.setWorkout_am_id(0);
                            am.setWorkout_am_name("");
                            daysList.set(position, am);
                            listener.refresh(week);
                            break;
                        case R.id.imgPMWorkout:
                            TrainingProgramDay pm = daysList.get(position);
                            pm.setWorkout_pm_id(0);
                            pm.setWorkout_pm_name("");
                            daysList.set(position, pm);
                            listener.refresh(week);
                            break;
                        case R.id.imgNutritionPlan:
                            TrainingProgramDay np = daysList.get(position);
                            np.setDiet_plan_id(0);
                            np.setDiet_plan_name("");
                            daysList.set(position, np);
                            listener.refresh(week);
                            break;
                        case R.id.imgCheckIn:
                            TrainingProgramDay dayCheckIn = daysList.get(position);
                            dayCheckIn.setIs_checkin(0);
                            daysList.set(position, dayCheckIn);
                            listener.refresh(week);
                            break;
                    }
                    break;
            }
            return false;
        }
    }

    public interface TrainingProgramListener {
        void delete(String name, int id);

        void refresh(int id);

        void showPopup(int id, View view);
    }
}
