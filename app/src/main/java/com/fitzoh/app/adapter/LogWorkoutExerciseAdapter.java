package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemLogWorkoutExerciseBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.activity.DetailExcersiceActivity;
import com.fitzoh.app.ui.activity.ReplaceExerciseActivity;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class LogWorkoutExerciseAdapter extends RecyclerView.Adapter<LogWorkoutExerciseAdapter.DataViewHolder> {

    Context context;
    private List<LogWorkoutDataModel.DataBean> dataList;
    private int main = 0;
    LogWorkoutMainAdapter.StartWorkoutListener listener;
    private boolean isSetEditable = false;

    public LogWorkoutExerciseAdapter(Context context, List<LogWorkoutDataModel.DataBean> dataList, int main, LogWorkoutMainAdapter.StartWorkoutListener listener, boolean isSetEditable) {
        this.context = context;
        this.dataList = dataList;
        this.main = main;
        this.listener = listener;
        this.isSetEditable = isSetEditable;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLogWorkoutExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_log_workout_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemLogWorkoutExerciseBinding mBinding;

        public DataViewHolder(ItemLogWorkoutExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.txtReplace.setVisibility(isSetEditable ? View.VISIBLE : View.INVISIBLE);
            mBinding.ratingBar.setIsIndicator(!isSetEditable);
            Utils.getShapeGradient(context, mBinding.txtReplace);
            mBinding.txtReplace.setOnClickListener(view -> {
                Intent intent = new Intent(context, ReplaceExerciseActivity.class);
                intent.putExtra("workout_id", ((BaseActivity) context).getIntent().getIntExtra("workout_id", 0));
                intent.putExtra("main", main);
                intent.putExtra("sub", getAdapterPosition());
                intent.putExtra("exerciseID", dataList.get(getAdapterPosition()).getId());
                ((BaseActivity) context).startActivityForResult(intent, 100);
            });
        }

        public void bind(LogWorkoutDataModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
            if (dataBean.getSets() != null && dataBean.getSets().size() > 0) {
                mBinding.ratingBar.setRating(dataBean.getRating());
                mBinding.recyclerView.setAdapter(new LogWorkoutSetsAdapter(context, dataBean.getSets(), main, getAdapterPosition(), listener, isSetEditable));
                mBinding.recyclerView.setVisibility(View.VISIBLE);
            } else {
                mBinding.ratingBar.setRating(dataBean.getRating());
                mBinding.recyclerView.setAdapter(new LogWorkoutSetsAdapter(context, new ArrayList<>(), main, getAdapterPosition(), listener, isSetEditable));
                mBinding.recyclerView.setVisibility(View.GONE);
            }
            mBinding.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, DetailExcersiceActivity.class).putExtra("id", dataBean.getId()));
                }
            });
            mBinding.ratingBar.setOnRatingBarChangeListener((ratingBar, v, b) -> listener.setRating(main, getAdapterPosition(), (int) mBinding.ratingBar.getRating()));
        }
    }



}
