package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientActivityBinding;
import com.fitzoh.app.model.ClientProgressModel;
import com.fitzoh.app.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ClientActivityListAdapter extends RecyclerView.Adapter<ClientActivityListAdapter.DataViewHolder> {


    Context context;
    List<ClientProgressModel.DataBean> clientActivityData;

    public ClientActivityListAdapter(Context context, List<ClientProgressModel.DataBean> clientActivityData) {
        this.context = context;
        this.clientActivityData = clientActivityData;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientActivityBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_activity, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        switch (clientActivityData.get(position).getType()) {

            case "workout_logged":
                holder.mBinding.recyler.setVisibility(View.GONE);
                Utils.setImageBackground(context, holder.mBinding.status, R.drawable.ic_dumbbell);
                break;
            case "diet_logged":
                holder.mBinding.recyler.setVisibility(View.GONE);
                Utils.setImageBackground(context, holder.mBinding.status, R.drawable.ic_apple);
                break;
            case "progress_photo_uploaded":
                Utils.setImageBackground(context, holder.mBinding.status, R.drawable.ic_gallery_progress);
                holder.mBinding.recyler.setVisibility(View.VISIBLE);
                holder.mBinding.recyler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
                holder.mBinding.recyler.setAdapter(new AssignedUserActivityListAdapter(context, clientActivityData.get(position).getImages()));
                // holder.mBinding.recyler.setAdapter(new AssignedUserActivityListAdapter(context));
                break;
            case "meausurements_updated":
                //holder.mBinding.recyler.setVisibility(View.GONE);
                holder.mBinding.recyler.setVisibility(View.GONE);
                holder.mBinding.txtTimeout.setVisibility(View.VISIBLE);
                Utils.setImageBackground(context, holder.mBinding.status, R.drawable.ic_measurement);
                holder.mBinding.txtTimeout.setText(R.string.check_them_out);
                break;
        }

        if (position == 0)
            holder.mBinding.view.setVisibility(View.VISIBLE);
        else
            holder.mBinding.view.setVisibility(View.GONE);

        if(position==(getItemCount()-1)){
            holder.mBinding.view1.setVisibility(View.GONE);
        }

        holder.mBinding.txtName.setText(clientActivityData.get(position).getTitle());

        String date = clientActivityData.get(position).getTime();
        SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date newDate = null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf = new SimpleDateFormat("hh:mm a , dd MMM yyyy");
        date = spf.format(newDate);
        holder.mBinding.txtTime.setText(date);


    }

    public class OverlapDecoration extends RecyclerView.ItemDecoration {
        private final static int vertOverlap = -20;
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            if (itemPosition == 0) {
                return;
            }
            outRect.set(vertOverlap, 0, 0, 0);
        }
    }

    @Override
    public int getItemCount() {
        return clientActivityData.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientActivityBinding mBinding;

        public DataViewHolder(ItemClientActivityBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            //   mBinding.recyler.addItemDecoration(new OverlapDecoration());
        }
    }

}
