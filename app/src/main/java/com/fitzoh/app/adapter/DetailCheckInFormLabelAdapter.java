package com.fitzoh.app.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemDetailCheckInFormLabelBinding;
import com.fitzoh.app.model.CheckInLabelModel;

import java.util.ArrayList;
import java.util.List;

public class DetailCheckInFormLabelAdapter extends RecyclerView.Adapter<DetailCheckInFormLabelAdapter.DataViewHolder> {

    private List<CheckInLabelModel.SublableBean> sublableBeans = new ArrayList<>();
    private int positionMain = 0;

    DetailCheckInFormLabelAdapter(int positionMain, List<CheckInLabelModel.SublableBean> weekList) {
        this.sublableBeans = weekList;
        this.positionMain = positionMain;
    }


    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDetailCheckInFormLabelBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_detail_check_in_form_label, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return sublableBeans.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemDetailCheckInFormLabelBinding mBinding;

        public DataViewHolder(ItemDetailCheckInFormLabelBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

        public void bind(int position) {
            CheckInLabelModel.SublableBean sublableBean = sublableBeans.get(position);
            mBinding.txtLabelMain.setText(sublableBean.getLable());
            mBinding.txtLabelOne.setText(sublableBean.getAttributes().get(0).getLable());
            mBinding.txtLabelTwo.setText(sublableBean.getAttributes().get(1).getLable());
            mBinding.edtLabelOne.setText(sublableBean.getAttributes().get(0).getValue());
            mBinding.edtLabelTwo.setText(sublableBean.getAttributes().get(1).getValue());
        }
    }
}
