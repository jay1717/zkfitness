package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemGymClientListBinding;
import com.fitzoh.app.model.GymClientListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class GymClientListAdapter extends RecyclerView.Adapter<GymClientListAdapter.DataViewHolder> {


    Context context;
    List<GymClientListModel.DataBean> gymClientData;
    private onDataModeified onDataModeified;

    public GymClientListAdapter(Context context, List<GymClientListModel.DataBean> gymClientData, onDataModeified onDataModeified) {
        this.context = context;
        this.gymClientData = gymClientData;
        this.onDataModeified = onDataModeified;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGymClientListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_gym_client_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(gymClientData.get(position).getName());
        holder.mBinding.txtEmail.setText(String.valueOf(gymClientData.get(position).getEmail()));
        holder.mBinding.txtAssignedTrainer.setText("Assigned Trainer : " + gymClientData.get(position).getTrainer());
        Utils.setImage(context, holder.mBinding.imgUser, gymClientData.get(position).getPhoto());
        if (gymClientData.get(position).getIs_profile_completed() == 0) {
            holder.mBinding.imgChat.setAlpha((float) 0.5);
            holder.mBinding.imgChat.setEnabled(false);
        } else if (gymClientData.get(position).getIs_profile_completed() == 1) {
            holder.mBinding.imgChat.setAlpha((float) 1.0);
            holder.mBinding.imgChat.setEnabled(true);
        }
        holder.mBinding.imgChat.setOnClickListener(view -> onDataModeified.chat(gymClientData.get(position)));
        holder.mBinding.frontLayout.setOnClickListener(view -> onDataModeified.getData(gymClientData.get(position)));
        holder.mBinding.imgAddUser.setOnClickListener(view -> onDataModeified.assign(gymClientData.get(position)));
    }

    @Override
    public int getItemCount() {
        return gymClientData.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemGymClientListBinding mBinding;

        public DataViewHolder(ItemGymClientListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgChat, R.drawable.ic_chat_icon);
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);

        }
    }

    public interface onDataModeified {
        void chat(GymClientListModel.DataBean dataBean);

        void getData(GymClientListModel.DataBean dataBean);

        void assign(GymClientListModel.DataBean dataBean);

    }

}
