package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainerPackagesBinding;
import com.fitzoh.app.model.ProfilePackageModel;

import java.util.List;

public class ProfilePackagesListAdapter extends RecyclerView.Adapter<ProfilePackagesListAdapter.DataViewHolder> {


    List<ProfilePackageModel.DataBean> packageList;
    Context context;
    private onDataPass onDataPass;

    public ProfilePackagesListAdapter(List<ProfilePackageModel.DataBean> packageList, Context context, ProfilePackagesListAdapter.onDataPass onDataPass) {
        this.packageList = packageList;
        this.context = context;
        this.onDataPass = onDataPass;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainerPackagesBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_trainer_packages, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(packageList.get(position));
        holder.mBinding.txtRupee.setText("$" + packageList.get(position).getPrice());
        holder.mBinding.txtRupee.setPaintFlags(holder.mBinding.txtRupee.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mBinding.txtRupeeDiscount.setText("$" + packageList.get(position).getDiscounted_price());
        holder.mBinding.txtWeek.setText("No. of weeks : " + packageList.get(position).getWeeks());
    }


    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainerPackagesBinding mBinding;

        public DataViewHolder(ItemTrainerPackagesBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.frontLayout.setOnClickListener(view -> onDataPass.pass(packageList.get(getAdapterPosition())));
        }


        public void bind(ProfilePackageModel.DataBean packageListModel) {
            mBinding.setItem(packageListModel);
            mBinding.executePendingBindings();
        }


    }

    public interface onDataPass {
        void pass(ProfilePackageModel.DataBean dataBean);

    }

}
