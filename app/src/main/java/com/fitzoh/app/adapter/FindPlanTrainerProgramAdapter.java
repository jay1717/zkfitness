package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemFindPlanBinding;
import com.fitzoh.app.model.FindPlanTrainingProgramModel;

import java.util.ArrayList;
import java.util.List;

public class FindPlanTrainerProgramAdapter extends RecyclerView.Adapter<FindPlanTrainerProgramAdapter.DataViewHolder> implements Filterable {


    Context context;
    List<FindPlanTrainingProgramModel.DataBean> findPlanList;
    private List<FindPlanTrainingProgramModel.DataBean> dataListFilter = new ArrayList<>();
    private onDataModeified onDataModeified;

    public FindPlanTrainerProgramAdapter(Context context, List<FindPlanTrainingProgramModel.DataBean> findPlanList, FindPlanTrainerProgramAdapter.onDataModeified onDataModeified) {
        this.context = context;
        this.findPlanList = findPlanList;
        this.dataListFilter = findPlanList;
        this.onDataModeified = onDataModeified;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFindPlanBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_find_plan, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(dataListFilter.get(position).getName());
        holder.mBinding.txtSecond.setText("No. of weeks : " + dataListFilter.get(position).getNo_of_weeks());
        holder.mBinding.txtPrice.setText("Price : " + (dataListFilter.get(position).getPrice() == 0 ? "Free" : dataListFilter.get(position).getPrice() ));

        holder.mBinding.frontLayout.setOnClickListener(view -> onDataModeified.getData(dataListFilter.get(position)));
    }

    @Override
    public int getItemCount() {
        if (findPlanList != null)
            return dataListFilter.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemFindPlanBinding mBinding;

        public DataViewHolder(ItemFindPlanBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

            mBinding.txtThird.setVisibility(View.GONE);

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = findPlanList;
                } else {
                    List<FindPlanTrainingProgramModel.DataBean> filteredList = new ArrayList<>();
                    for (FindPlanTrainingProgramModel.DataBean row : findPlanList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<FindPlanTrainingProgramModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface onDataModeified {

        void getData(FindPlanTrainingProgramModel.DataBean dataBean);

    }

}
