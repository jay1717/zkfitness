package com.fitzoh.app.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingProgramWeekClientBinding;
import com.fitzoh.app.model.TrainingProgramDetailClientData;

import java.util.ArrayList;
import java.util.List;

public class TrainingProgramWeekClientAdapter extends RecyclerView.Adapter<TrainingProgramWeekClientAdapter.DataViewHolder> {

    public List<TrainingProgramDetailClientData.DataBean.TrainingWeeksBean> weekList = new ArrayList<>();
    Activity context;
    RedirectToDetailListener listener;

    public TrainingProgramWeekClientAdapter(Activity context, List<TrainingProgramDetailClientData.DataBean.TrainingWeeksBean> weekList, RedirectToDetailListener listener) {
        this.context = context;
        this.weekList = weekList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingProgramWeekClientBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_program_week_client, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return weekList.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainingProgramWeekClientBinding mBinding;

        public DataViewHolder(ItemTrainingProgramWeekClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.weekName.setOnClickListener(view -> setVisible(mBinding.recyclerView));
        }

        public void bind(int position) {
            mBinding.weekName.setText(String.format("Week %s", weekList.get(getAdapterPosition()).getWeek_number()));
            if (weekList.get(position).getWeekdays() != null && weekList.get(position).getWeekdays().size() > 0) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new TrainingProgramDayClientAdapter(context, weekList.get(position).getWeekdays(), listener));

            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }

    private void setVisible(RecyclerView recyclerView) {
        if (recyclerView.getVisibility() == View.VISIBLE) {
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    public interface RedirectToDetailListener {
        void openExerciseDetail(int workout_id, String workout_name, boolean isAM,int day_id);

        void viewLogWorkout(int workout_id, String workout_name, boolean isAM,int day_id);
    }


}
