package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientUpdateDetailBinding;
import com.fitzoh.app.model.ClientUpdateDetailData;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientUpdateDetailAdapter extends RecyclerView.Adapter<ClientUpdateDetailAdapter.DataViewHolder> {

    Context context;
    List<ClientUpdateDetailData.DataBean.TodaysDietBean> todaysDietBeans;

    public ClientUpdateDetailAdapter(Context context, List<ClientUpdateDetailData.DataBean.TodaysDietBean> todaysDietBeans) {
        this.context = context;
        this.todaysDietBeans = todaysDietBeans;
    }

    @NonNull
    @Override
    public ClientUpdateDetailAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientUpdateDetailBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_update_detail, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientUpdateDetailAdapter.DataViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return todaysDietBeans.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientUpdateDetailBinding mBinding;

        public DataViewHolder(ItemClientUpdateDetailBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.txtProgress.setTextColor(((BaseActivity) context).res.getColor(R.color.colorAccent));
            Utils.createProgressBackground(context, mBinding.progressBar);

        }

        public void bind(int position) {
            if (todaysDietBeans.get(position).getPercentage_taken() == 0) {
                mBinding.txtMealName.setTextColor(ContextCompat.getColor(context, R.color.red));
                mBinding.txtMealName.setText("Not Taken");
                mBinding.viewLineBottom.setBackgroundColor(ContextCompat.getColor(context, R.color.red));
            } else if (todaysDietBeans.get(position).getPercentage_taken() == 100) {
                mBinding.txtMealName.setTextColor(ContextCompat.getColor(context, R.color.green));
                mBinding.txtMealName.setText("Taken");
                mBinding.viewLineBottom.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
            } else {
                mBinding.txtMealName.setTextColor(ContextCompat.getColor(context, R.color.start_yellow));
                mBinding.txtMealName.setText("Partially Taken");
                mBinding.viewLineBottom.setBackgroundColor(ContextCompat.getColor(context, R.color.start_yellow));
            }
            mBinding.txtDietName.setText(todaysDietBeans.get(position).getName());
            mBinding.progressBar.setProgress((int) todaysDietBeans.get(position).getPercentage_taken());
            mBinding.txtProgress.setText("" + todaysDietBeans.get(position).getPercentage_taken() + "%");
            if (todaysDietBeans.get(position).getFood() != null) {
                mBinding.recyclerView.setVisibility(View.VISIBLE);
                mBinding.recyclerView.setAdapter(new ClientUpdateDietReportAdapter(context, todaysDietBeans.get(position).getFood()));
            } else {
                mBinding.recyclerView.setVisibility(View.GONE);
            }
        }
    }
}
