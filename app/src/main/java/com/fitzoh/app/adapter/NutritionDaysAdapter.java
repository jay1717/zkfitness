package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.RowNutritionDaysBinding;

import java.util.ArrayList;

public class NutritionDaysAdapter extends RecyclerView.Adapter<NutritionDaysAdapter.DataViewHolder> {


    ArrayList drawableArray;
    Context context;
    boolean isMonday = false, isTuesday = false, isWednesday = false, isThursday = false, isFriday = false, isSaturday = false, isSunday = false;
    private onSelect onSelect;

    public NutritionDaysAdapter(ArrayList drawableArray, Context context, NutritionDaysAdapter.onSelect onSelect) {
        this.drawableArray = drawableArray;
        this.context = context;
        this.onSelect = onSelect;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowNutritionDaysBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_nutrition_days, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.imgDays.setImageResource((Integer) drawableArray.get(position));

        holder.mBinding.imgDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (position) {

                    case 0:
                        if (isMonday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.monday));
                            isMonday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.monday_selected));
                            isMonday = true;
                            onSelect.select("mon");
                        }
                        break;
                    case 1:
                        if (isTuesday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.tuesday));
                            isTuesday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.tuesday_selected));
                            isTuesday = true;
                            onSelect.select("tue");
                        }
                        break;
                    case 2:
                        if (isWednesday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.wednesday));
                            isWednesday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.wednesday_selected));
                            isWednesday = true;
                            onSelect.select("wed");
                        }
                        break;
                    case 3:
                        if (isThursday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.thursday));
                            isThursday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.thursday_selected));
                            isThursday = true;
                            onSelect.select("thu");
                        }
                        break;
                    case 4:
                        if (isFriday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.friday));
                            isFriday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.friday_selected));
                            isFriday = true;
                            onSelect.select("fri");
                        }
                        break;
                    case 5:
                        if (isSaturday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.saturday));
                            isSaturday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.saturday_selected));
                            isSaturday = true;
                            onSelect.select("sat");
                        }
                        break;
                    case 6:
                        if (isSunday) {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.sunday));
                            isSunday = false;
                        } else {
                            holder.mBinding.imgDays.setImageDrawable(context.getResources().getDrawable(R.drawable.sunday_selected));
                            isSunday = true;
                            onSelect.select("sun");
                        }
                        break;


                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return drawableArray.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        RowNutritionDaysBinding mBinding;

        public DataViewHolder(RowNutritionDaysBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }

    public interface onSelect{
        void select(String name);
    }
}
