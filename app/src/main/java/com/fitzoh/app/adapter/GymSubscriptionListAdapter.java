package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemGymSubscriptionBinding;
import com.fitzoh.app.model.GymSubscriptionModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class GymSubscriptionListAdapter extends RecyclerView.Adapter<GymSubscriptionListAdapter.DataViewHolder> {


    List<GymSubscriptionModel.DataBean> packageList;
    Context context;
    private onAction onAction;

    public GymSubscriptionListAdapter(List<GymSubscriptionModel.DataBean> packageList, Context context, GymSubscriptionListAdapter.onAction onAction) {
        this.packageList = packageList;
        this.context = context;
        this.onAction = onAction;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGymSubscriptionBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_gym_subscription, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(packageList.get(position));
    }


    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemGymSubscriptionBinding mBinding;

        public DataViewHolder(ItemGymSubscriptionBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgEdit, R.drawable.ic_edit);
            mBinding.deleteLayout.setOnClickListener(v -> onAction.delete(packageList.get(getAdapterPosition())));
            mBinding.imgEdit.setOnClickListener(v -> onAction.edit(packageList.get(getAdapterPosition())));
            mBinding.txtRupee.setPaintFlags(mBinding.txtRupee.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        public void bind(GymSubscriptionModel.DataBean packageListModel) {
            mBinding.setItem(packageListModel);
            mBinding.executePendingBindings();
        }
    }

    public interface onAction {

        void edit(GymSubscriptionModel.DataBean packageListModel);

        void delete(GymSubscriptionModel.DataBean packageListModel);
    }

}
