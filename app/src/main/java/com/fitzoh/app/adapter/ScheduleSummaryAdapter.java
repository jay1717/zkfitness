package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemScheduleGymHomeBinding;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.activity.GymTrainerScheduleActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ScheduleSummaryAdapter extends RecyclerView.Adapter<ScheduleSummaryAdapter.DataViewHolder> {


    Activity context;
    List<TrainerListModel.DataBean> list;

    public ScheduleSummaryAdapter(Activity context, List<TrainerListModel.DataBean> achievementModels) {
        this.context = context;
        this.list = achievementModels;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemScheduleGymHomeBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_schedule_gym_home, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.trainerName.setText(list.get(position).getName() == null ? ((BaseActivity) context).session.getAuthorizedUser(context).getName() : list.get(position).getName());
        if (list.get(position).getPhoto() != null)
            Utils.setImage(context, holder.mBinding.imgTrainer, list.get(position).getPhoto());
        else
            Utils.setImage(context, holder.mBinding.imgTrainer, ((BaseActivity) context).session.getAuthorizedUser(context).getPhoto());
        holder.mBinding.txtNoOfSchedule.setText("No. of schedule: " + list.get(position).getNumber_of_schedule());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemScheduleGymHomeBinding mBinding;

        DataViewHolder(ItemScheduleGymHomeBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(context, GymTrainerScheduleActivity.class);
                intent.putExtra("trainer_id", list.get(getAdapterPosition()).getId());
                if (list.get(getAdapterPosition()).getId() == ((BaseActivity) context).session.getAuthorizedUser(context).getId()) {
                    intent.putExtra("trainer_id", 0);
                }
                context.startActivity(intent);
            });
        }
    }
}
