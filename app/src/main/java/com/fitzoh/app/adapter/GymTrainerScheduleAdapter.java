package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemGymTrainerScheduleBinding;
import com.fitzoh.app.model.GymTrainerScheduleListModel;
import com.fitzoh.app.model.ScheduleListModel;
import com.fitzoh.app.ui.activity.ClientProfileActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class GymTrainerScheduleAdapter extends RecyclerView.Adapter<GymTrainerScheduleAdapter.DataViewHolder> {


    Context context;
    List<GymTrainerScheduleListModel.DataBean> schedualList;

    public GymTrainerScheduleAdapter(Context context, List<GymTrainerScheduleListModel.DataBean> schedualList) {
        this.context = context;
        this.schedualList = schedualList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGymTrainerScheduleBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_gym_trainer_schedule, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.trainerName.setText(schedualList.get(position).getName());
        holder.mBinding.scheduleName.setText(schedualList.get(position).getDay());
        holder.mBinding.txtStartTime.setText(String.valueOf(schedualList.get(position).getStartDate()));
        holder.mBinding.txtEndTime.setText(String.valueOf(schedualList.get(position).getEndTime()));
        Utils.setImage(context, holder.mBinding.imgUser, schedualList.get(position).getPhoto());
        holder.mBinding.cvGymClientSchedule.setOnClickListener(view -> {
            context.startActivity(new Intent(context, ClientProfileActivity.class).putExtra("data", schedualList.get(position)).putExtra("client_type","2"));
        });
    }

    @Override
    public int getItemCount() {
        return schedualList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemGymTrainerScheduleBinding mBinding;

        public DataViewHolder(ItemGymTrainerScheduleBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgStart, R.drawable.ic_clock);
            Utils.setImageBackground(context, mBinding.imgEnd, R.drawable.ic_clock);
        }
    }


    public interface onAddClient {
        void add(ScheduleListModel.DataBean dataBean);

        void edit(ScheduleListModel.DataBean dataBean);

        void rename(int position);

        void delete(ScheduleListModel.DataBean dataBean);
    }


}
