package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientGroupBinding;
import com.fitzoh.app.model.AddClientGroupListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientGroupListAdapter extends RecyclerView.Adapter<ClientGroupListAdapter.DataViewHolder> {

    private Context context;
    private List<AddClientGroupListModel.DataBean> liveClientsModels;
    private ClientActivateListener listener;

    public ClientGroupListAdapter(Context context, List<AddClientGroupListModel.DataBean> liveClientsModels, ClientActivateListener listener) {
        this.context = context;
        this.liveClientsModels = liveClientsModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientGroupBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_group, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind();
        holder.mBinding.recyclerView.setAdapter(new AssignedUserListAdapter(context, liveClientsModels.get(position).getClient_group_user()));
        holder.mBinding.clientGroupName.setText(liveClientsModels.get(position).getName());
        holder.mBinding.txtTotalClientValue.setText("" + liveClientsModels.get(position).getClient_group_user().size());
    }

    @Override
    public int getItemCount() {
        return liveClientsModels.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientGroupBinding mBinding;

        public DataViewHolder(ItemClientGroupBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);
            mBinding.frontLayout.setOnClickListener(v -> listener.onSelect(liveClientsModels.get(getAdapterPosition())));
            mBinding.imgAddUser.setOnClickListener(v -> listener.onSelect(liveClientsModels.get(getAdapterPosition())));
            this.setIsRecyclable(false);
        }

        public void bind() {
            mBinding.deleteLayout.setOnClickListener(v -> listener.delete(liveClientsModels.get(getAdapterPosition())));
        }
    }

    public interface ClientActivateListener {
        void onSelect(AddClientGroupListModel.DataBean dataBean);

        void delete(AddClientGroupListModel.DataBean dataBean);
    }
}
