package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemNutrirtionInnerPlanBinding;
import com.fitzoh.app.model.MealListModel;

import java.util.List;

public class NutritionPlanInnerAdapter extends RecyclerView.Adapter<NutritionPlanInnerAdapter.DataViewHolder> {

    Context context;
    List<MealListModel.DataBean.DietPlanFoodBean> foodBeanList;
    onDeleteMeal onDeleteMeal;

    public NutritionPlanInnerAdapter(Context context, List<MealListModel.DataBean.DietPlanFoodBean> foodBeanList, NutritionPlanInnerAdapter.onDeleteMeal onDeleteMeal) {
        this.context = context;
        this.foodBeanList = foodBeanList;
        this.onDeleteMeal = onDeleteMeal;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNutrirtionInnerPlanBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_nutrirtion_inner_plan, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtTitle.setText(foodBeanList.get(position).getFood().getFood_name() + " - " + String.valueOf(foodBeanList.get(position).getFood().getServing_size()) + foodBeanList.get(position).getFood().getServing_size_unit());
        holder.mBinding.txtCarbs.setText("Carbs : " + foodBeanList.get(position).getFood().getCarbs());
        holder.mBinding.txtFat.setText("Fat : " + foodBeanList.get(position).getFood().getFat());
        holder.mBinding.txtNoCalories.setText("Calories : " + foodBeanList.get(position).getFood().getCalories());
        holder.mBinding.txtProtein.setText("Protein : " + foodBeanList.get(position).getFood().getDishes_protien());
    }

    @Override
    public int getItemCount() {
        return foodBeanList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemNutrirtionInnerPlanBinding mBinding;

        public DataViewHolder(ItemNutrirtionInnerPlanBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));

            mBinding.deleteLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDeleteMeal.delete(foodBeanList.get(getAdapterPosition()));
                }
            });

        }

    }

    public interface onDeleteMeal {

        void delete(MealListModel.DataBean.DietPlanFoodBean dataBean);
    }


}
