package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemMenuNotificationBinding;
import com.fitzoh.app.model.MenuNotificationResponseModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MenuNotificationListAdapter extends RecyclerView.Adapter<MenuNotificationListAdapter.DataViewHolder> {

    public ArrayList<MenuNotificationResponseModel.DataBean> arrayList;
    Context context;

    public MenuNotificationListAdapter(ArrayList<MenuNotificationResponseModel.DataBean> arrayList,Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ItemMenuNotificationBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_menu_notification, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder dataViewHolder, int i) {
        MenuNotificationResponseModel.DataBean data=arrayList.get(i);
        if(data!=null){
            dataViewHolder.mBinding.txtClientName.setText(data.getTitle());
            dataViewHolder.mBinding.txtDetail.setText(data.getMessage());
            if (data.getUserData().getPhoto() != null && !data.getUserData().getPhoto().equalsIgnoreCase("")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder);
                Glide.with(context)
                        .load(data.getUserData().getPhoto())
                        .apply(options)
                        .into(dataViewHolder.mBinding.imgUser);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (arrayList != null)
            return arrayList.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemMenuNotificationBinding mBinding;

        public DataViewHolder(ItemMenuNotificationBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }
}
