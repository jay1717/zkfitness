package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainerListBinding;
import com.fitzoh.app.model.GetTrainerModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class TrainerListAdapter extends RecyclerView.Adapter<TrainerListAdapter.DataViewHolder> {

    Context context;
    List<GetTrainerModel.DataBean> dataList;
    private onDataPass onDataPass;

    public TrainerListAdapter(Context context, List<GetTrainerModel.DataBean> dataList, TrainerListAdapter.onDataPass onDataPass) {
        this.context = context;
        this.dataList = dataList;
        this.onDataPass = onDataPass;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainerListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_trainer_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        GetTrainerModel.DataBean data = dataList.get(position);
        if (data != null) {
            holder.bind(data);
            holder.mBinding.imgApproved.setVisibility((data.getIs_approved() == 1) ? View.VISIBLE : View.GONE);
            holder.mBinding.imgFeatured.setVisibility((data.getIs_featured() == 1) ? View.VISIBLE : View.GONE);

            if (dataList.get(position).getSpeciality() != null) {
                if (!dataList.get(position).getSpeciality().equals("")) {
                    holder.mBinding.txtFeature.setText(dataList.get(position).getSpeciality());
                } else if (!dataList.get(position).getFacilities().equals("")) {
                    holder.mBinding.txtFeature.setText(dataList.get(position).getFacilities());
                }
                if (dataList != null && dataList.get(position).getSpeciality() != null) {
                    if (!dataList.get(position).getSpeciality().equals("")) {
                        holder.mBinding.txtFeature.setText(dataList.get(position).getSpeciality());
                    } else if (!dataList.get(position).getFacilities().equals("")) {
                        holder.mBinding.txtFeature.setText(dataList.get(position).getFacilities());
                    }
                }

                holder.itemView.setOnClickListener(view -> {
                    onDataPass.pass(data);
                    // context.startActivity(new Intent(context, TrainerProfileActivity.class).putExtra("trainerId",data.getId()).putExtra("isGym", data.getIs_gym()));
                });
            }


      /*  switch (position) {hna.s
            case 1:
                holder.mBinding.imgApproved.setVisibility(View.VISIBLE);
                holder.mBinding.imgFeatured.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.mBinding.imgApproved.setVisibility(View.VISIBLE);
                break;
            case 3:
                holder.mBinding.imgApproved.setVisibility(View.VISIBLE);
                holder.mBinding.imgApproved.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_featured_icon));
                break;


        }*/


        }
    }

        @Override
        public int getItemCount () {
            return dataList.size();

        }

        public class DataViewHolder extends RecyclerView.ViewHolder {

            ItemTrainerListBinding mBinding;

            public DataViewHolder(ItemTrainerListBinding mBinding) {
                super(mBinding.getRoot());
                this.mBinding = mBinding;
                Utils.setTextViewStartImage(context, mBinding.txtLocation, R.drawable.ic_placeholder, true);

            }

            public void bind(GetTrainerModel.DataBean item) {
                mBinding.setItem(item);
                mBinding.executePendingBindings();
            }

        }

        public interface onDataPass {
            void pass(GetTrainerModel.DataBean dataBean);

        }

}
