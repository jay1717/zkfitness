package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemScheduleBinding;
import com.fitzoh.app.model.ScheduleListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.DataViewHolder> {


    Context context;
    List<ScheduleListModel.DataBean> schedualList;
    private onAddClient onAddClient;
    private final ViewBinderHelper viewBinderHelper = new ViewBinderHelper();

    public ScheduleAdapter(Context context, List<ScheduleListModel.DataBean> schedualList, ScheduleAdapter.onAddClient onAddClient) {
        this.context = context;
        this.schedualList = schedualList;
        this.onAddClient = onAddClient;
    }
    public ScheduleAdapter() {
        viewBinderHelper.setOpenOnlyOne(true);
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemScheduleBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_schedule, parent, false);
        return new DataViewHolder(mBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.bind(position);
        viewBinderHelper.bind(holder.mBinding.swipeLayout, String.valueOf(schedualList.get(position).getId()));
        holder.mBinding.recyclerView.setAdapter(new AssignedUserListAdapter(context, schedualList.get(position).getAssigned_to()));
        holder.mBinding.scheduleName.setText(schedualList.get(position).getDay());
        holder.mBinding.txtStartTimeValue.setText(String.valueOf(schedualList.get(position).getStart_time()));
        holder.mBinding.txtEndTimeValue.setText(String.valueOf(schedualList.get(position).getEnd_time()));

    }

    @Override
    public int getItemCount() {
        return schedualList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemScheduleBinding mBinding;

        public DataViewHolder(ItemScheduleBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);
            mBinding.frontLayout.setOnClickListener(view -> {
                onAddClient.edit(schedualList.get(getAdapterPosition()));
            });
        }

        public void bind(int position) {
            mBinding.imgAddUser.setOnClickListener(v -> onAddClient.add(schedualList.get(position)));
            mBinding.deleteLayout.setOnClickListener(v -> onAddClient.delete(schedualList.get(getAdapterPosition())));
        }
    }

    public class OverlapDecoration extends RecyclerView.ItemDecoration {

        private final static int vertOverlap = -20;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            final int itemPosition = parent.getChildAdapterPosition(view);
            if (itemPosition == 0) {
                return;
            }
            outRect.set(vertOverlap, 0, 0, 0);
        }
    }

    public interface onAddClient {

        void add(ScheduleListModel.DataBean dataBean);

        void edit(ScheduleListModel.DataBean dataBean);

        void rename(int position);

        void delete(ScheduleListModel.DataBean dataBean);
    }




}
