package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemSelectWorkoutBinding;
import com.fitzoh.app.model.WorkOutListModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SelectWorkoutListAdapter extends RecyclerView.Adapter<SelectWorkoutListAdapter.DataViewHolder> implements Filterable {


    Activity context;

    public int selectionPostition = -1;
    List<WorkOutListModel.DataBean> dataBeans;
    List<WorkOutListModel.DataBean> dataListFilter;

    public SelectWorkoutListAdapter(Activity context, List<WorkOutListModel.DataBean> dataBeans) {
        this.context = context;
        this.dataListFilter = dataBeans;
        this.dataBeans = dataBeans;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        ItemSelectWorkoutBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_select_workout, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

      /*  FilterModel filterM = filterList.get(position);
        holder.brandName.setText(filterM.getName());
        holder.productCount.setText("" + filterM.getProductCount());
        if (filterM.isSelected()) {
            holder.selectionState.setChecked(true);
        } else {
            holder.selectionState.setChecked(false);
        }*/
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        if (dataBeans != null)
            return dataListFilter.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemSelectWorkoutBinding mBinding;
        boolean isSelected = false;

        public DataViewHolder(ItemSelectWorkoutBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.workoutSelect, R.drawable.ic_check);
            mBinding.getRoot().setOnClickListener(this);
            mBinding.workoutSelect.setOnClickListener(this);
            mBinding.workoutSelect.setOnCheckedChangeListener((compoundButton, b) -> {
                mBinding.workoutSelect.setVisibility(b ? View.VISIBLE : View.GONE);
            });
        }

        void bind(int position) {
            mBinding.txtWorkoutName.setText(dataListFilter.get(position).getName());
            // use the sparse boolean array to check
            mBinding.workoutSelect.setChecked(selectionPostition == position);
            if (mBinding.workoutSelect.isChecked()) {
                mBinding.cardMain.setBackgroundResource(R.drawable.green_bg);
                Utils.setGreenBackground(context,mBinding.cardMain);
            } else
                mBinding.cardMain.setBackgroundResource(R.color.white);
        }

        @Override
        public void onClick(View v) {
            selectionPostition = getAdapterPosition();
//            notifyDataSetChanged();
            Intent intent = new Intent();
            intent.putExtra("day", context.getIntent().getIntExtra("day", 0));
            intent.putExtra("week", context.getIntent().getIntExtra("week", 0));
            intent.putExtra("id", dataListFilter.get(selectionPostition).getId());
            intent.putExtra("name", dataListFilter.get(selectionPostition).getName());
            context.setResult(Activity.RESULT_OK, intent);
            context.finish();
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = dataBeans;
                } else {
                    List<WorkOutListModel.DataBean> filteredList = new ArrayList<>();
                    for (WorkOutListModel.DataBean row : dataBeans) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (List<WorkOutListModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
