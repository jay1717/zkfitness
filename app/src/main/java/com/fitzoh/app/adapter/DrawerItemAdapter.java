package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemDrawerRowBinding;
import com.fitzoh.app.model.DrawerItem;

import java.util.ArrayList;
import java.util.List;

public class DrawerItemAdapter extends RecyclerView.Adapter {

    private List<DrawerItem> mData = new ArrayList<>();
    private int selectItem;
    private Context mContext;
    private OnRecyclerViewClickListener listener;

    public DrawerItemAdapter(Context context, ArrayList<DrawerItem> list, OnRecyclerViewClickListener listener, int pos) {
        this.mData = list;
        this.selectItem = pos;
        this.mContext = context;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemDrawerRowBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_drawer_row, parent, false);
        return new MyViewHolder(mBinder);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((MyViewHolder) holder).mBinder.imageViewIcon.setImageResource(mData.get(position).icon);
        ((MyViewHolder) holder).mBinder.textViewName.setText(mData.get(position).name);
        if (selectItem == position) {
            ((MyViewHolder) holder).mBinder.selectedView.setVisibility(View.VISIBLE);
            ((MyViewHolder) holder).mBinder.imageViewIcon.setImageResource(mData.get(position).iconSelected);
        } else {
            ((MyViewHolder) holder).mBinder.selectedView.setVisibility(View.GONE);
            ((MyViewHolder) holder).mBinder.imageViewIcon.setImageResource(mData.get(position).icon);
        }
        holder.itemView.setOnClickListener(view -> {
            listener.onItemClicked(position, mData.get(position).name);
        });
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ItemDrawerRowBinding mBinder;

        MyViewHolder(ItemDrawerRowBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;


        }
    }

    public void setPosition(int position) {
        selectItem = position;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public interface OnRecyclerViewClickListener {
        void onItemClicked(int position, String name);

    }

}
