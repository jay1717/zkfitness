package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemNotificationBinding;
import com.fitzoh.app.model.RequestListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.DataViewHolder> {


    Context context;
    private List<RequestListModel.DataBean> listOfData;
    private onAction onAction;

    public RequestListAdapter(Context context, List<RequestListModel.DataBean> listOfData, RequestListAdapter.onAction onAction) {
        this.context = context;
        this.listOfData = listOfData;
        this.onAction = onAction;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNotificationBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_notification, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(listOfData.get(position));
        switch (listOfData.get(position).getStatus()) {
            case "0":
                holder.mBinding.layout.setVisibility(View.VISIBLE);
                holder.mBinding.txtStatus.setVisibility(View.GONE);
                break;
            case "1":
                holder.mBinding.layout.setVisibility(View.GONE);
                holder.mBinding.txtStatus.setVisibility(View.VISIBLE);
                holder.mBinding.txtStatus.setText(R.string.accepted);
                holder.mBinding.txtStatus.setTextColor(context.getResources().getColor(R.color.colorAccent));
                break;
            case "2":
                holder.mBinding.layout.setVisibility(View.GONE);
                holder.mBinding.txtStatus.setVisibility(View.VISIBLE);
                holder.mBinding.txtStatus.setText(R.string.declined);
                holder.mBinding.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return listOfData.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding mBinding;

        public DataViewHolder(ItemNotificationBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.txtAccept.setOnClickListener(view -> onAction.accept(listOfData.get(getAdapterPosition()), 1));
            mBinding.txtReject.setOnClickListener(view -> onAction.reject(listOfData.get(getAdapterPosition()), 2));
            mBinding.txtChat.setOnClickListener(view -> {
                Intent intent1 = new Intent(context, ConversationActivity.class);
                intent1.putExtra(ConversationUIService.USER_ID, listOfData.get(getAdapterPosition()).getClient_id());
                intent1.putExtra(ConversationUIService.DISPLAY_NAME, listOfData.get(getAdapterPosition()).getName()); //put it for displaying the title.
                intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
                context.startActivity(intent1);
            });
            mBinding.layout.setBackground(Utils.getShapeGradient(context, 45));
        }

        public void bind(RequestListModel.DataBean request) {
            mBinding.setItem(request);
            mBinding.executePendingBindings();
        }
    }

    public interface onAction {
        void accept(RequestListModel.DataBean dataBean, int status);

        void reject(RequestListModel.DataBean dataBean, int status);

    }
}
