package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainerShopBinding;
import com.fitzoh.app.model.TrainerShopListModel;
import com.fitzoh.app.ui.activity.TrainerProductDetailsActivity;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class TrainerShopAdapter extends RecyclerView.Adapter<TrainerShopAdapter.DataViewHolder> implements Filterable {

    Context context;
    List<TrainerShopListModel.DataBean> trainerListModel;
    private List<TrainerShopListModel.DataBean> dataListFilter = new ArrayList<>();
    private onAction onAction;

    public TrainerShopAdapter(Context context, List<TrainerShopListModel.DataBean> trainerListModel, TrainerShopAdapter.onAction onAction) {
        this.context = context;
        this.trainerListModel = trainerListModel;
        this.dataListFilter = trainerListModel;
        this.onAction = onAction;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ItemTrainerShopBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_trainer_shop, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(dataListFilter.get(position).getName());
        holder.mBinding.txtPrice.setText(String.valueOf(dataListFilter.get(position).getPrice()));
        holder.mBinding.txtDesc.setText(dataListFilter.get(position).getDiscounted_price());


        if (dataListFilter.get(position).getImage() != null && dataListFilter.get(position).getImage().size() > 0)
            Utils.setImagePlaceHolder(context, holder.mBinding.imgProduct, dataListFilter.get(position).getImage().get(0));

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataListFilter = trainerListModel;
                } else {
                    List<TrainerShopListModel.DataBean> filteredList = new ArrayList<>();
                    for (TrainerShopListModel.DataBean row : dataListFilter) {

                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataListFilter = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataListFilter;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataListFilter = (ArrayList<TrainerShopListModel.DataBean>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getItemCount() {
        if (dataListFilter != null)
            return dataListFilter.size();
        else return 0;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainerShopBinding mBinding;

        DataViewHolder(ItemTrainerShopBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgDelete, R.drawable.ic_delete_green);
            Utils.setImageBackground(context, mBinding.imgEdit, R.drawable.ic_edit);
            mBinding.txtPrice.setPaintFlags( mBinding.txtDesc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mBinding.txtPrice.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
            mBinding.cardMain.setOnClickListener(v -> context.startActivity(new Intent(context, TrainerProductDetailsActivity.class).putExtra("data", trainerListModel.get(getAdapterPosition())).putExtra("isTrainer", 1)));
            mBinding.imgDelete.setOnClickListener(v -> onAction.delete(trainerListModel.get(getAdapterPosition())));
            mBinding.imgEdit.setOnClickListener(v -> onAction.edit(trainerListModel.get(getAdapterPosition())));
        }
    }

    public interface onAction {
        void delete(TrainerShopListModel.DataBean dataBean);

        void edit(TrainerShopListModel.DataBean dataBean);

    }


}
