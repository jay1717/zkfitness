package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientUpdateBinding;
import com.fitzoh.app.model.ClientUpdateListData;
import com.fitzoh.app.ui.activity.ClientUpdateActivity;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class ClientUpdateAdapter extends RecyclerView.Adapter<ClientUpdateAdapter.DataViewHolder> {

    Context context;
    List<ClientUpdateListData.DataBean> clientUpdateModel;
    boolean isGym = false;

    public ClientUpdateAdapter(Context context, List<ClientUpdateListData.DataBean> clientUpdateModel, boolean isGym) {
        this.context = context;
        this.clientUpdateModel = clientUpdateModel;
        this.isGym = isGym;
    }

    @NonNull
    @Override
    public ClientUpdateAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientUpdateBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_update, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientUpdateAdapter.DataViewHolder holder, int position) {
        holder.bind(clientUpdateModel.get(position));
        holder.mBinding.txtWorkoutValue.setText(Integer.toString(clientUpdateModel.get(position).getNo_of_workouts()));
        holder.mBinding.txtDietValue.setText(Integer.toString(clientUpdateModel.get(position).getNo_of_diet()));
        holder.mBinding.constMain.setOnClickListener(view -> {
            context.startActivity(new Intent(context, ClientUpdateActivity.class).putExtra("clientID", clientUpdateModel.get(position).getId()).putExtra("isGym", isGym));

        });
    }

    @Override
    public int getItemCount() {
        return clientUpdateModel.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientUpdateBinding mBinding;

        public DataViewHolder(ItemClientUpdateBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setTextViewStartImage(context, mBinding.txtWorkout, R.drawable.ic_workout, true);
            Utils.setTextViewStartImage(context, mBinding.txtDiet, R.drawable.ic_diet, true);
            Utils.setImageBackground(context, mBinding.imgMsg);
            /*itemView.setOnClickListener(view -> {
             *//*((NavigationMainActivity) context).pushFragments(AppConstants.NAVIGATION_KEY, new FragmentClientUpdate(), true, true);*//*
                context.startActivity(new Intent(context, ClientUpdateActivity.class));
            });*/
            mBinding.imgClient.setOnClickListener(v -> {
              /*  ClientUpdateDialog dialog = new ClientUpdateDialog();
                dialog.show(((AppCompatActivity) context).getSupportFragmentManager(), ClientUpdateDialog.class.getSimpleName());*/
            });
        }

        public void bind(ClientUpdateListData.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }
}
