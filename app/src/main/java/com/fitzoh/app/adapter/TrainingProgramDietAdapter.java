package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingProgramDietBinding;
import com.fitzoh.app.model.MealListModel;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

public class TrainingProgramDietAdapter extends RecyclerView.Adapter<TrainingProgramDietAdapter.DataViewHolder> {

    Context context;
    List<MealListModel.DataBean> mealListModel;
    public CompositeDisposable compositeDisposable;
    private Snackbar snackbar;

    public TrainingProgramDietAdapter(Context context, List<MealListModel.DataBean> mealListModel) {
        this.context = context;
        this.mealListModel = mealListModel;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingProgramDietBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_program_diet, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtNameTime.setText(mealListModel.get(position).getName() + " - " + mealListModel.get(position).getTime());
        holder.mBinding.txtNoCaloriesValue.setText(String.valueOf(mealListModel.get(position).getCalories()));
        holder.mBinding.txtCarbsValue.setText(String.valueOf(mealListModel.get(position).getCarbs()));
        holder.mBinding.txtProteinValue.setText(String.valueOf(mealListModel.get(position).getProtein()));
        holder.mBinding.txtFatValue.setText(String.valueOf(mealListModel.get(position).getFat()));
        holder.mBinding.recycler.setAdapter(new TrainingProgramDietInnerAdapter(context, mealListModel.get(position).getDiet_plan_food()));
        if (mealListModel.get(position).getDiet_plan_food().size() > 0) {
            Drawable img = context.getResources().getDrawable(R.drawable.ic_down_arrow);
            img.setBounds(0, 0, 60, 60);
            // holder.mBinding.txtNameTime.setCompoundDrawables(null, null, img, null);
        }
        holder.mBinding.layoutMain.setOnClickListener(v -> {
            if (holder.mBinding.recycler.getVisibility() == View.VISIBLE)
                holder.mBinding.recycler.setVisibility(View.GONE);
            else
                holder.mBinding.recycler.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public int getItemCount() {
        return mealListModel.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemTrainingProgramDietBinding mBinding;

        public DataViewHolder(ItemTrainingProgramDietBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.recycler.setLayoutManager(new LinearLayoutManager(context));
            mBinding.recycler.setVisibility(View.GONE);
        }
    }
}
