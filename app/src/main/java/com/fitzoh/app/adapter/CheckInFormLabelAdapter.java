package com.fitzoh.app.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemCheckInFormLabelBinding;
import com.fitzoh.app.model.CheckInLabelModel;

import java.util.ArrayList;
import java.util.List;

public class CheckInFormLabelAdapter extends RecyclerView.Adapter<CheckInFormLabelAdapter.DataViewHolder> {

    public List<CheckInLabelModel.SublableBean> sublableBeans = new ArrayList<>();
    CheckInFormListener listener;
    public int positionMain = 0;

    public CheckInFormLabelAdapter(int positionMain, List<CheckInLabelModel.SublableBean> weekList, CheckInFormListener listener) {
        this.sublableBeans = weekList;
        this.positionMain = positionMain;
        this.listener = listener;
    }


    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCheckInFormLabelBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_check_in_form_label, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);

        holder.mBinding.edtLabelOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                /*List<CheckInLabelModel.SublableBean.AttributesBean> attributesBeans = sublableBeans.get(position).getAttributes();
                attributesBeans.get(0).setValue(holder.mBinding.edtLabelOne.getText().toString());
                CheckInLabelModel.SublableBean model = sublableBeans.get(position);
                model.setAttributes(attributesBeans);
                sublableBeans.set(position, model);*/
                listener.setData(positionMain, position, holder.mBinding.edtLabelOne);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        holder.mBinding.edtLabelTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
/*
                List<CheckInLabelModel.SublableBean.AttributesBean> attributesBeans = sublableBeans.get(position).getAttributes();
                attributesBeans.get(1).setValue(holder.mBinding.edtLabelTwo.getText().toString());
                CheckInLabelModel.SublableBean model = sublableBeans.get(position);
                model.setAttributes(attributesBeans);
                sublableBeans.set(position, model);
*/
                listener.setData(positionMain, position, holder.mBinding.edtLabelTwo);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return sublableBeans.size();
    }


    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemCheckInFormLabelBinding mBinding;

        public DataViewHolder(ItemCheckInFormLabelBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

        public void bind(int position) {
            CheckInLabelModel.SublableBean sublableBean = sublableBeans.get(position);
            mBinding.txtLabelMain.setText(sublableBean.getLable());
            mBinding.txtLabelOne.setText(sublableBean.getAttributes().get(0).getLable());
            mBinding.txtLabelTwo.setText(sublableBean.getAttributes().get(1).getLable());
            mBinding.edtLabelOne.setText(sublableBean.getAttributes().get(0).getValue());
            mBinding.edtLabelTwo.setText(sublableBean.getAttributes().get(1).getValue());
        }
    }

    public interface CheckInFormListener {
        void setData(int main,int position, EditText editText);
    }


}
