package com.fitzoh.app.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemDialogToAssignBinding;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class DialogToAssignAdapter extends RecyclerView.Adapter<DialogToAssignAdapter.DataViewHolder> {
    Activity context;
    List<String> subScription;
    public int selectionPostition = -1;

    public DialogToAssignAdapter(Activity context, List<String> subScription) {
        this.context = context;
        this.subScription = subScription;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDialogToAssignBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_dialog_to_assign, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtName.setText(subScription.get(position));
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return subScription.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemDialogToAssignBinding mBinding;

        public DataViewHolder(ItemDialogToAssignBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.chkAssign, R.drawable.ic_check);
            mBinding.layoutSubscription.setBackgroundResource(R.color.white);
            mBinding.getRoot().setOnClickListener(this);
            mBinding.chkAssign.setOnClickListener(this);
        }

        void bind(int position) {
            // use the sparse boolean array to check


            mBinding.chkAssign.setChecked(selectionPostition == position);
            if (mBinding.chkAssign.isChecked()) {
                mBinding.chkAssign.setVisibility(View.VISIBLE);
                mBinding.layoutSubscription.setBackgroundResource(R.drawable.green_bg);
                Utils.setGreenBackground(context, mBinding.layoutSubscription);
            } else {
                mBinding.chkAssign.setVisibility(View.GONE);
                mBinding.layoutSubscription.setBackgroundResource(R.color.white);
            }

        }

        @Override
        public void onClick(View v) {
            selectionPostition = getAdapterPosition();
            notifyDataSetChanged();
        }
    }
}
