package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemViewWorkoutExerciseBinding;
import com.fitzoh.app.model.LogWorkoutDataModel;

import java.util.List;

public class ViewWorkoutExerciseAdapter extends RecyclerView.Adapter<ViewWorkoutExerciseAdapter.DataViewHolder> {

    Context context;
    private List<LogWorkoutDataModel.DataBean> dataList;

    public ViewWorkoutExerciseAdapter(Context context, List<LogWorkoutDataModel.DataBean> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemViewWorkoutExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_view_workout_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemViewWorkoutExerciseBinding mBinding;

        public DataViewHolder(ItemViewWorkoutExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(LogWorkoutDataModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }
}
