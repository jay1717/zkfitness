package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemProfileWorkoutBinding;
import com.fitzoh.app.model.WorkOutListModel;

import java.util.List;

public class ProfileWorkoutAdapter extends RecyclerView.Adapter<ProfileWorkoutAdapter.DataViewHolder> {


    Context context;
    private unAssignClient unAssignClient;
    private List<WorkOutListModel.DataBean> workoutList;
    private boolean isVisible;

    public ProfileWorkoutAdapter(Context context, ProfileWorkoutAdapter.unAssignClient unassignClient, List<WorkOutListModel.DataBean> workoutList, boolean isVisible) {
        this.context = context;
        this.workoutList = workoutList;
        this.unAssignClient = unassignClient;
        this.isVisible = isVisible;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemProfileWorkoutBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_profile_workout, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.bind(position);
        holder.mBinding.txtNoExcersiceValue.setText(String.valueOf(workoutList.get(position).getExercise_count()));
        holder.mBinding.txtNoSetsValue.setText(String.valueOf(workoutList.get(position).getNo_of_sets()));
        holder.mBinding.workoutName.setText(workoutList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }


    public interface unAssignClient {
        void unAssign(WorkOutListModel.DataBean dataBean);

        void startWorkout(WorkOutListModel.DataBean dataBean);
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemProfileWorkoutBinding mBinding;

        public DataViewHolder(ItemProfileWorkoutBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
//            Utils.setImageBackground(context, mBinding.imgMenuUnassign, R.drawable.ic_unassign_clients);
        }

        public void bind(int position) {
            mBinding.imgMenuUnassign.setVisibility(isVisible ? View.VISIBLE : View.GONE);
            mBinding.imgMenuUnassign.setOnClickListener(v -> {
//                    onAddClient.add(workoutList.get(position));
                unAssignClient.unAssign(workoutList.get(position));

            });
            itemView.setOnClickListener(view -> unAssignClient.startWorkout(workoutList.get(getAdapterPosition())));
        }
    }


}
