package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemGymTrainerListBinding;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class GymTrainerClientAdapter extends RecyclerView.Adapter<GymTrainerClientAdapter.DataViewHolder> {


    Context context;
    List<TrainerListModel.DataBean> trainerListModel;
    private onDataModeified onDataModeified;

    public GymTrainerClientAdapter(Context context, List<TrainerListModel.DataBean> trainerListModel, GymTrainerClientAdapter.onDataModeified onDataModeified) {
        this.context = context;
        this.trainerListModel = trainerListModel;
        this.onDataModeified = onDataModeified;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGymTrainerListBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_gym_trainer_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtName.setText(trainerListModel.get(position).getName());
        if (trainerListModel.get(position).getEmail()!=null && trainerListModel.get(position).getEmail().length()>0){
            holder.mBinding.txtEmail.setText(trainerListModel.get(position).getEmail());
        }else {
            holder.mBinding.txtEmail.setText(R.string.str_no_email);

        }
        if(trainerListModel.get(position).getContact_number()!=null && trainerListModel.get(position).getContact_number().length()>0){
            holder.mBinding.txtNumber.setText(String.valueOf(trainerListModel.get(position).getContact_number()));
        }
        else {
            holder.mBinding.txtNumber.setText("No contect no. available");
        }
        holder.mBinding.txtClient.setText("No. of clients : " + trainerListModel.get(position).getNumber_of_client());
        Utils.setImage(context, holder.mBinding.imgUser, trainerListModel.get(position).getPhoto());

        holder.mBinding.imgChat.setOnClickListener(view -> onDataModeified.chat(trainerListModel.get(position)));
        holder.mBinding.imgAddUser.setOnClickListener(view -> onDataModeified.assign(trainerListModel.get(position)));
        holder.mBinding.frontLayout.setOnClickListener(view -> onDataModeified.getData(trainerListModel.get(position)));
    }

    @Override
    public int getItemCount() {
        return trainerListModel.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemGymTrainerListBinding mBinding;

        public DataViewHolder(ItemGymTrainerListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setImageBackground(context, mBinding.imgAddUser, R.drawable.ic_assign_clients);
            Utils.setImageBackground(context, mBinding.imgChat, R.drawable.ic_chat_green);

        }
    }

    public interface onDataModeified {
        void chat(TrainerListModel.DataBean dataBean);

        void assign(TrainerListModel.DataBean dataBean);

        void getData(TrainerListModel.DataBean dataBean);

    }

}
