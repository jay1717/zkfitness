package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemCertificationBinding;
import com.fitzoh.app.model.CertificationModel;

import java.util.List;

public class CertificationAdapter extends RecyclerView.Adapter<CertificationAdapter.DataViewHolder> {

    Context context;
    List<CertificationModel.DataBean> certificationList;


    public CertificationAdapter(Context context, List<CertificationModel.DataBean> certificationList) {
        this.context = context;
        this.certificationList = certificationList;
    }

    @NonNull
    @Override
    public CertificationAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCertificationBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_certification, parent, false);
        return new DataViewHolder(mBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull CertificationAdapter.DataViewHolder holder, int position) {
        holder.bind(certificationList.get(position));
    }

    @Override
    public int getItemCount() {
        return certificationList.size();
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemCertificationBinding mBinding;

        DataViewHolder(ItemCertificationBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(CertificationModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }
}
