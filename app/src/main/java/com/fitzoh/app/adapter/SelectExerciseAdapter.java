package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemSelectExerciseBinding;
import com.fitzoh.app.model.WorkoutExerciseListModel;
import com.fitzoh.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SelectExerciseAdapter extends RecyclerView.Adapter<SelectExerciseAdapter.DataViewHolder> {
    private List<WorkoutExerciseListModel.DataBean> dataList;
    private SelectExerciseListener listener;
    private Context context;

    public SelectExerciseAdapter() {

    }

    public SelectExerciseAdapter(Context context, List<WorkoutExerciseListModel.DataBean> dataList, SelectExerciseListener listener) {
        this.dataList = dataList;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSelectExerciseBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_select_exercise, parent, false);
        return new DataViewHolder(mBinding);
    }

    public List<Integer> getData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).isChecked())
                list.add(dataList.get(i).getId());
        }
        return list;
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemSelectExerciseBinding mBinding;

        public DataViewHolder(ItemSelectExerciseBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkboxClientAssign, R.drawable.checked);
            mBinding.checkboxClientAssign.setOnCheckedChangeListener((buttonView, isChecked) -> {
                WorkoutExerciseListModel.DataBean dataBean = dataList.get(getAdapterPosition());
                dataBean.setChecked(isChecked);
                dataList.set(getAdapterPosition(), dataBean);
                listener.selected();
            });
        }

        public void bind(WorkoutExerciseListModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }

    public interface SelectExerciseListener {
        void selected();
    }
}
