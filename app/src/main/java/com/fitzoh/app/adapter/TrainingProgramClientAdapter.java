package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemTrainingProgramClientBinding;
import com.fitzoh.app.model.TrainingProgramClientList;
import com.fitzoh.app.ui.activity.DetailTrainingProgramClientActivity;

import java.util.List;

public class TrainingProgramClientAdapter extends RecyclerView.Adapter<TrainingProgramClientAdapter.DataViewHolder> {


    Context context;
    private List<TrainingProgramClientList.DataBean> trainingProgramList;

    public TrainingProgramClientAdapter(Context context, List<TrainingProgramClientList.DataBean> trainingProgramList) {
        this.context = context;
        this.trainingProgramList = trainingProgramList;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTrainingProgramClientBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_training_program_client, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.bind(position);
        holder.mBinding.txtNoOfWeeks.setText("No. of Weeks: " + String.valueOf(trainingProgramList.get(position).getWeeks()));
        holder.mBinding.trainingProgramName.setText(trainingProgramList.get(position).getTitle());
        holder.mBinding.txtAssginedBy.setText(trainingProgramList.get(position).getTrainer());

        if (trainingProgramList.get(position).getIs_active() ==1){
            holder.mBinding.trainingProgramName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.mBinding.trainingActive.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.mBinding.trainingActive.setVisibility(View.VISIBLE);
        }else {
            holder.mBinding.trainingProgramName.setTextColor(context.getResources().getColor(R.color.colorBlack));
            holder.mBinding.trainingActive.setVisibility(View.GONE);
        }
        if (position % 2 == 0)
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        else
            holder.mBinding.viewSideLine.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
    }

    @Override
    public int getItemCount() {
        return trainingProgramList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {

        ItemTrainingProgramClientBinding mBinding;

        public DataViewHolder(ItemTrainingProgramClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;

        }

        public void bind(int position) {
            mBinding.frontLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, DetailTrainingProgramClientActivity.class).putExtra("data", trainingProgramList.get(getAdapterPosition())));
                }
            });
        }
    }
}
