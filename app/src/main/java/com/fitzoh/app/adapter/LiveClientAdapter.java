package com.fitzoh.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemLiveClientBinding;
import com.fitzoh.app.model.LiveClientsModel;
import com.fitzoh.app.ui.activity.LiveClientActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LiveClientAdapter extends RecyclerView.Adapter<LiveClientAdapter.DataViewHolder> {

    private Context context;
    private HashMap<Integer, LiveClientsModel.DataBean> liveClientsModels;
    boolean isGym = false;
    private List<Integer> strings;

    public LiveClientAdapter(Context context, HashMap<Integer, LiveClientsModel.DataBean> liveClientsModels, boolean isGym) {
        this.context = context;
        this.liveClientsModels = liveClientsModels;
        this.isGym = isGym;
        strings = new ArrayList<Integer>(liveClientsModels.keySet());
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemLiveClientBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_live_client, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(liveClientsModels.get(strings.get(position)));
    }

    @Override
    public int getItemCount() {
        return liveClientsModels.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemLiveClientBinding mBinding;

        public DataViewHolder(ItemLiveClientBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            itemView.setOnClickListener(v -> {
//                context.startActivity(new Intent(context, LiveClientActivity.class).putExtra("data", liveClientsModels.get(getAdapterPosition())).putExtra("isGym", isGym));
                context.startActivity(new Intent(context, LiveClientActivity.class).putExtra("data", liveClientsModels.get(strings.get(getAdapterPosition()))).putExtra("isGym", isGym));
            });

        }

        public void bind(LiveClientsModel.DataBean dataBean) {
            mBinding.setItem(dataBean);
            mBinding.executePendingBindings();
        }
    }

    public interface dataPassing {
        void data(LiveClientsModel.DataBean liveClient);
    }
}
