package com.fitzoh.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.CheckboxClientAssignBinding;
import com.fitzoh.app.model.TrainerListModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;


public class GymClientTrainerAssignAdapter extends RecyclerView.Adapter<GymClientTrainerAssignAdapter.ViewHolder> {

    Context context;
    private List<TrainerListModel.DataBean> clientAssignModels;
    public int selectionPosition = 0;

    public GymClientTrainerAssignAdapter(Activity context, List<TrainerListModel.DataBean> clientAssignModels) {
        this.context = context;
        this.clientAssignModels = clientAssignModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CheckboxClientAssignBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.checkbox_client_assign, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return clientAssignModels.size();
    }

    public int getData() {
        return clientAssignModels.get(selectionPosition).getId();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckboxClientAssignBinding mBinding;

        public ViewHolder(CheckboxClientAssignBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            Utils.setCheckBoxSelectors(context, mBinding.checkboxClientAssign, R.drawable.checked);
            mBinding.getRoot().setOnClickListener(this);
            mBinding.checkboxClientAssign.setOnClickListener(this);

        }

        void bind(int position) {
            // use the sparse boolean array to check
            mBinding.checkboxClientAssign.setChecked(selectionPosition == position);
            mBinding.checkboxClientAssign.setText(clientAssignModels.get(position).getName());
        }

        @Override
        public void onClick(View v) {
            selectionPosition = getAdapterPosition();
            notifyDataSetChanged();

        }
    }
    /*Context context;
    private List<TrainerListModel.DataBean> clientAssignModels;

    public GymClientTrainerAssignAdapter(Context context, List<TrainerListModel.DataBean> clientAssignModels) {
        this.context = context;
        this.clientAssignModels = clientAssignModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CheckboxClientAssignTrainerBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.checkbox_client_assign_trainer, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position);
    }

    public int getData() {
     *//*   if (selectedPosition == -1)
            return selectedPosition;
        else
            return clientAssignModels.get(selectedPosition).getId();*//*
        return 0;
    }


    @Override
    public int getItemCount() {
        return clientAssignModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckboxClientAssignTrainerBinding mBinding;

        public ViewHolder(CheckboxClientAssignTrainerBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        @Override
        public void onClick(View v) {
            for (int i = 0; i < clientAssignModels.size(); i++) {
                TrainerListModel.DataBean dataBean = clientAssignModels.get(i);
                if (i == getAdapterPosition()) {
                    dataBean.setSelected(!dataBean.isSelected());
                } else {
                    dataBean.setSelected(false);
                }
                clientAssignModels.set(i, dataBean);
            }
            notifyDataSetChanged();
        }

        public void bind(int position) {
            mBinding.checkboxClientAssign.setText(clientAssignModels.get(position).getName());
            itemView.setOnClickListener(this);
            mBinding.imgSelected.setImageDrawable(setCheckBoxSelectors(clientAssignModels.get(position).isSelected()));
        }
    }

    public Drawable setCheckBoxSelectors(boolean isNormal) {
        Drawable normal = ContextCompat.getDrawable(context, R.drawable.unchecked);
        Drawable pressed = ContextCompat.getDrawable(context, R.drawable.checked);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Objects.requireNonNull(pressed).setTint(((BaseActivity) context).res.getColor(R.color.colorAccent));
        } else {
            Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(pressed));
            DrawableCompat.setTint(wrappedDrawable, ((BaseActivity) context).res.getColor(R.color.colorAccent));
        }
        return !isNormal ? normal : pressed;
    }*/
}
