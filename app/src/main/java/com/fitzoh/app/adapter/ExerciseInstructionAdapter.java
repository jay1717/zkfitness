package com.fitzoh.app.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemExerciseInstructionsBinding;

public class ExerciseInstructionAdapter extends RecyclerView.Adapter<ExerciseInstructionAdapter.DataViewHolder> {
    @NonNull
    @Override
    public ExerciseInstructionAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemExerciseInstructionsBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_exercise_instructions, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ExerciseInstructionAdapter.DataViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemExerciseInstructionsBinding mBinding;

        DataViewHolder(ItemExerciseInstructionsBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
    }
}
