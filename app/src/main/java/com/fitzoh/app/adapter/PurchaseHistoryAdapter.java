package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemPurchaseHistoryBinding;
import com.fitzoh.app.model.PurchaseHistoryTrainerModel;
import com.fitzoh.app.utils.Utils;

import java.util.List;

public class PurchaseHistoryAdapter extends RecyclerView.Adapter<PurchaseHistoryAdapter.DataViewHolder> {


    List<PurchaseHistoryTrainerModel.DataBean.ClientBean> packageList;
    Context context;

    public PurchaseHistoryAdapter(List<PurchaseHistoryTrainerModel.DataBean.ClientBean> packageList, Context context) {
        this.packageList = packageList;
        this.context = context;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPurchaseHistoryBinding itemPackagesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_purchase_history, parent, false);
        return new DataViewHolder(itemPackagesBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {

        holder.mBinding.txtUserName.setText(packageList.get(position).getName());
        holder.mBinding.txtPackageName.setText(packageList.get(position).getPackage_name());
        holder.mBinding.txtDescription.setText(packageList.get(position).getDesc());
        holder.mBinding.txtWeek.setText("No. of weeks : " + packageList.get(position).getWeeks());
        holder.mBinding.txtDate.setText(packageList.get(position).getPurchase_date());
        holder.mBinding.txtRupee.setText("Price : " + packageList.get(position).getDiscounted_price());
        if (packageList.get(position).getPhoto() != null && !packageList.get(position).getPhoto().equalsIgnoreCase("")) {
            Utils.setImagePlaceHolder(context, holder.mBinding.imgPackage, packageList.get(position).getPhoto());
        } else {
            holder.mBinding.imgPackage.setImageResource(R.drawable.ic_user_placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemPurchaseHistoryBinding mBinding;

        public DataViewHolder(ItemPurchaseHistoryBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

    }
}
