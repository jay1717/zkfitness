package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemClientUpdateDietReportBinding;
import com.fitzoh.app.model.ClientUpdateDetailData;

import java.util.List;

public class ClientUpdateDietReportAdapter extends RecyclerView.Adapter<ClientUpdateDietReportAdapter.DataViewHolder> {

    private Context mContext;
    private List<ClientUpdateDetailData.DataBean.TodaysDietBean.FoodBean> todaysDietBeans;

    ClientUpdateDietReportAdapter(Context mContext, List<ClientUpdateDetailData.DataBean.TodaysDietBean.FoodBean> todaysDietBeans) {
        this.mContext = mContext;
        this.todaysDietBeans = todaysDietBeans;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemClientUpdateDietReportBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_client_update_diet_report, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.bind(position);
        /*holder.mBinding.txtProgress.setText(Html.fromHtml("65<small>%</small>"));
        if (position == 0) {
            holder.mBinding.txtMealName.setText("Meal 1");
            holder.mBinding.txtTaken.setTextColor(ContextCompat.getColor(mContext, R.color.green));
            holder.mBinding.txtTaken.setText("Taken");
            holder.mBinding.viewLineBottom.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green));
        } else if (position == 1) {
            holder.mBinding.txtMealName.setText("Meal 2");
            holder.mBinding.txtTaken.setTextColor(ContextCompat.getColor(mContext, R.color.start_yellow));
            holder.mBinding.txtTaken.setText("Partially Taken");
            holder.mBinding.viewLineBottom.setBackgroundColor(ContextCompat.getColor(mContext, R.color.start_yellow));
        } else if (position == 2) {
            holder.mBinding.txtMealName.setText("Meal 3");
            holder.mBinding.txtTaken.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            holder.mBinding.txtTaken.setText("Not Taken");
            holder.mBinding.viewLineBottom.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red));
        }*/
    }

    @Override
    public int getItemCount() {
        return todaysDietBeans.size();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemClientUpdateDietReportBinding mBinding;

        public DataViewHolder(ItemClientUpdateDietReportBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
//            mBinding.txtProgress.setTextColor(((BaseActivity) mContext).res.getColor(R.color.colorAccent));
//            Utils.createProgressBackground(mContext, mBinding.progressBar);
        }

        public void bind(int position) {
            mBinding.name.setText(todaysDietBeans.get(position).getName());
            mBinding.servingSize.setText(todaysDietBeans.get(position).getServing_size());
        }
    }
}
