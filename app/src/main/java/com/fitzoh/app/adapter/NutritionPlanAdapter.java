package com.fitzoh.app.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitzoh.app.R;
import com.fitzoh.app.databinding.ItemNutritionPlanBinding;
import com.fitzoh.app.model.CommonApiResponse;
import com.fitzoh.app.model.MealListModel;
import com.fitzoh.app.network.ApiClient;
import com.fitzoh.app.network.AuthorizedNetworkInterceptor;
import com.fitzoh.app.network.ObserverUtil;
import com.fitzoh.app.network.SingleCallback;
import com.fitzoh.app.network.WebserviceBuilder;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.utils.AppConstants;
import com.fitzoh.app.utils.Utils;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

import static com.fitzoh.app.network.WebserviceBuilder.ApiNames.deleteFood;
import static com.fitzoh.app.utils.AppConstants.CLIENT_ID;

public class NutritionPlanAdapter extends RecyclerView.Adapter<NutritionPlanAdapter.DataViewHolder> implements NutritionPlanInnerAdapter.onDeleteMeal, SingleCallback {

    Context context;
    List<MealListModel.DataBean> mealListModel;
    onAddMeal addMeal;
    private String userId, userAccessToken;
    public CompositeDisposable compositeDisposable;
    private Snackbar snackbar;

    public NutritionPlanAdapter(Context context, List<MealListModel.DataBean> mealListModel, onAddMeal addMeal, String userId, String userAccessToken) {
        this.context = context;
        this.mealListModel = mealListModel;
        this.addMeal = addMeal;
        this.userId = userId;
        this.userAccessToken = userAccessToken;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNutritionPlanBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_nutrition_plan, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.mBinding.txtNameTime.setText(mealListModel.get(position).getName() + " - " + mealListModel.get(position).getTime().toUpperCase());

        holder.mBinding.txtNoCaloriesValue.setText(String.valueOf(mealListModel.get(position).getCalories()));
        holder.mBinding.txtCarbsValue.setText(String.valueOf(mealListModel.get(position).getCarbs()));
        holder.mBinding.txtProteinValue.setText(String.valueOf(mealListModel.get(position).getProtein()));
        holder.mBinding.txtFatValue.setText(String.valueOf(mealListModel.get(position).getFat()));
        holder.mBinding.recycler.setAdapter(new NutritionPlanInnerAdapter(context, mealListModel.get(position).getDiet_plan_food(), this));
        if (mealListModel.get(position).getDiet_plan_food().size() > 0) {
            Drawable img = context.getResources().getDrawable(R.drawable.ic_down_arrow);
            img.setBounds(0, 0, 60, 60);
            // holder.mBinding.txtNameTime.setCompoundDrawables(null, null, img, null);
        }
        holder.mBinding.frontLayout.setOnClickListener(v -> {
            if (holder.mBinding.recycler.getVisibility() == View.VISIBLE)
                holder.mBinding.recycler.setVisibility(View.GONE);

            else
                holder.mBinding.recycler.setVisibility(View.VISIBLE);
        });
    }

    @Override
    public int getItemCount() {
        return mealListModel.size();
    }

    @Override
    public void delete(MealListModel.DataBean.DietPlanFoodBean dataBean) {
        String client_id = ((BaseActivity) context).session.getStringDataByKeyNull(CLIENT_ID);
        ObserverUtil.subscribeToSingle(ApiClient.getClient(context, new AuthorizedNetworkInterceptor(userAccessToken, userId)).create(WebserviceBuilder.class).deleteFood(dataBean.getId(), client_id)
                , getCompositeDisposable(), deleteFood, this);
    }

    public CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case deleteFood:
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.getStatus() == AppConstants.SUCCESS) {
                    addMeal.onRefresh();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }

    public void showSnackBar(View view, String msg, int LENGTH) {
        if (view == null) return;
        snackbar = Snackbar.make(view, msg, LENGTH);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        snackbar.show();
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        ItemNutritionPlanBinding mBinding;

        public DataViewHolder(ItemNutritionPlanBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            mBinding.recycler.setLayoutManager(new LinearLayoutManager(context));
            mBinding.recycler.setVisibility(View.GONE);
            Utils.setImageBackground(context, mBinding.imgAdd, R.drawable.ic_plus_training_program);
            Utils.setImageBackground(context, mBinding.imgEdit, R.drawable.ic_edit);
            mBinding.imgEdit.setOnClickListener(v -> addMeal.edit(mealListModel.get(getAdapterPosition()), getAdapterPosition()));
            mBinding.deleteLayout.setOnClickListener(v -> addMeal.delete(mealListModel.get(getAdapterPosition())));
            mBinding.imgAdd.setOnClickListener(v -> showPopupMenu(mBinding.imgAdd, getAdapterPosition()));

        }
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(context, view);
        popup.getMenuInflater().inflate(R.menu.menu_add_food, popup.getMenu());
        popup.setOnMenuItemClickListener(new MenuActionItemClickListener(position));
        popup.show();
    }


    private class MenuActionItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        MenuActionItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_add:
                    addMeal.add(mealListModel.get(position));
                    break;
                case R.id.action_create:
                    addMeal.create(mealListModel.get(position));
                    break;
            }
            return false;
        }
    }

    public interface onAddMeal {
        void add(MealListModel.DataBean dataBean);

        void create(MealListModel.DataBean dataBean);

        void edit(MealListModel.DataBean dataBean, int position);

        void delete(MealListModel.DataBean dataBean);

        void onRefresh();
    }
}
