package com.fitzoh.app.utils;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fitzoh.app.R;
import com.fitzoh.app.ui.BaseActivity;
import com.fitzoh.app.ui.BaseFragment;
import com.fitzoh.app.vectorchildfinder.VectorChildFinder;
import com.fitzoh.app.vectorchildfinder.VectorDrawableCompat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    public static void giveRating(Context mContext) {
//        Uri uri = Uri.parse("market://details?id=" + mContext.getPackageName());
        Uri uri = Uri.parse("market://details?id=" + AppConstants.URL_PLAY_STORE_PACKAGE);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        if (Build.VERSION.SDK_INT > 21) {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        } else {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }
        try {
            mContext.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
//            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + mContext.getPackageName())));
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AppConstants.URL_PLAY_STORE)));
        }
    }

    public static void makeCall(Fragment mCotext, String mobilenumber){
        ((BaseFragment)mCotext).requestAppPermissions(new String[]{Manifest.permission.CALL_PHONE}, 100, new BaseFragment.setPermissionListener() {
            @Override
            public void onPermissionGranted(int requestCode) {
                if (mobilenumber != null ) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + mobilenumber));
                    mCotext.startActivity(callIntent);
                }
            }

            @Override
            public void onPermissionDenied(int requestCode) {
                //requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 100);
            }

            @Override
            public void onPermissionNeverAsk(int requestCode) {

            }
        });
    }
    public static void TimePicker(Context mContext, EditText editText) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                DateFormat f1 = new SimpleDateFormat("HH:mm"); //HH for hour of the day (0 - 23)
                Date d = null;
                try {
                    d = f1.parse(selectedHour + ":" + selectedMinute);
                    DateFormat f2 = new SimpleDateFormat("h:mma");
                    editText.setText(f2.format(d).toLowerCase());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    public static void shareOnFacebook(Context mContext) {

        String urlToShare = "https://stackoverflow.com/questions/7545254";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        // intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

        // See if official Facebook app is found
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = mContext.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

        // As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }

        mContext.startActivity(intent);
    }

    public static byte[] loadImageFromStorage(String path) {
        try {
            File f = new File(path);
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            ByteArrayOutputStream uploadStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, uploadStream);
            return uploadStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*//Social Media Page
    public static void openSocialMediaPage(Context context, String id, String userName, String clicked) {
        Intent appIntent;
        Intent webIntent;
        switch (clicked) {
            case "fb":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_FACEBOOK_BROWSER_URL + userName));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;
            case "instagram":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(INFO_INSTAGRAM_APP_URL + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_INSTAGRAM_BROWSER_URL + userName));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;
            case "youtube":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("youtube:" + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_RAVWANA_YOUTUBE_URL + id));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;

            case "rawvanaWebsite":
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_RAVWANA_WEBSITE_URL));
                context.startActivity(webIntent);
                break;

            case "touchZenWebsite":
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_TOUCHZEN_WEBSITE_URL));
                context.startActivity(webIntent);
                break;

            case "touchZenInstagram":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(INFO_INSTAGRAM_APP_URL + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_INSTAGRAM_BROWSER_URL + userName));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;

            case "touchZenFacebook":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://page/" + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_FACEBOOK_BROWSER_URL + userName));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;

            case "touchZenTwitter":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("twitter:user?id=" + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_TOUCHZEN_TWITTER_BROWSER_URL + userName));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;
            case "touchZenMoreApps":
                appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://developer?id=" + id));
                webIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(INFO_TOUCHZEN_MORE_APP_URL + userName));
                try {
                    context.startActivity(appIntent);
                } catch (ActivityNotFoundException ex) {
                    context.startActivity(webIntent);
                }
                break;
        }
    }*/

    public static void technicalSupportEmail(Context mContext) {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"Support@TouchZenMedia.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Support: Rawvana's 30 Day Smoothie Challenge");
        i.putExtra(Intent.EXTRA_TEXT, "I am using a New Android device with API Level " + Build.VERSION.SDK_INT + "." + "\n \n \nSent from my Android Device.");
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
        try {
            mContext.startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(mContext, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static boolean isOnline(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        boolean is = (netInfo != null && netInfo.isConnectedOrConnecting());
        return is;
    }

    public static void setTextViewStartImage(Context context, TextView textView, int id, boolean isAccent) {
        int color = isAccent ? ContextCompat.getColor(context, R.color.colorAccent) : ContextCompat.getColor(context, R.color.colorPrimary);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, id, textView);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        VectorDrawableCompat.VFullPath path3 = vector.findPathByName("path3");
        VectorDrawableCompat.VFullPath path4 = vector.findPathByName("path4");
        VectorDrawableCompat.VFullPath path5 = vector.findPathByName("path5");
        VectorDrawableCompat.VFullPath path6 = vector.findPathByName("path6");
        VectorDrawableCompat.VFullPath path7 = vector.findPathByName("path7");
        VectorDrawableCompat.VFullPath path8 = vector.findPathByName("path8");
        VectorDrawableCompat.VFullPath path9 = vector.findPathByName("path9");
        VectorDrawableCompat.VFullPath path10 = vector.findPathByName("path10");
        path1.setFillColor(color);
        if (path2 != null) path2.setFillColor(color);
        if (path3 != null) path3.setFillColor(color);
        if (path4 != null) path4.setFillColor(color);
        if (path5 != null) path5.setFillColor(color);
        if (path6 != null) path6.setFillColor(color);
        if (path7 != null) path7.setFillColor(color);
        if (path8 != null) path8.setFillColor(color);
        if (path9 != null) path9.setFillColor(color);
        if (path10 != null)
            path10.setFillColor(color);
        textView.setCompoundDrawablesWithIntrinsicBounds(vector.getDrawable(), null, null, null);
    }

    public static void setTextViewEndImage(Context context, TextView textView, int id) {
        int color = ContextCompat.getColor(context, R.color.colorAccent);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, id, textView);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        path1.setFillColor(color);
        if (path2 != null) path2.setFillColor(color);
        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, vector.getDrawable(), null);

    }

    public static void setImageBackground(Context context, ImageView fab, int id) {
        int color = ContextCompat.getColor(context, R.color.colorAccent);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, id, fab);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        VectorDrawableCompat.VFullPath path3 = vector.findPathByName("path3");
        VectorDrawableCompat.VFullPath path4 = vector.findPathByName("path4");
        VectorDrawableCompat.VFullPath path5 = vector.findPathByName("path5");
        path1.setFillColor(color);
        if (path2 != null) path2.setFillColor(color);
        if (path3 != null) path3.setFillColor(color);
        if (path4 != null) path4.setFillColor(color);
        if (path5 != null) path5.setFillColor(color);
    }

    public static void setImageBackground(Context context, ImageView fab) {
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, R.drawable.ic_chat_icon, fab);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(ContextCompat.getColor(context, R.color.colorAccent));
    }

    public static GradientDrawable getShapeGradient(Context context, int angle) {
        GradientDrawable.Orientation orientation = GradientDrawable.Orientation.TOP_BOTTOM;
        if (angle == 90) orientation = GradientDrawable.Orientation.LEFT_RIGHT;
        return new GradientDrawable(
                orientation,
                new int[]{ContextCompat.getColor(context, R.color.colorAccent),
                        ContextCompat.getColor(context, R.color.colorPrimary)});
    }

    public static void getShapeGradient(Context context, View view) {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[]{ContextCompat.getColor(context, R.color.colorAccent),
                        ContextCompat.getColor(context, R.color.colorPrimary)});
        gradientDrawable.setCornerRadius(com.intuit.sdp.R.dimen._23sdp);
        view.setBackground(gradientDrawable);
    }

    public static void getItemShapeGradient(Context context, View view) {
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[]{ContextCompat.getColor(context, R.color.colorAccent),
                        ContextCompat.getColor(context, R.color.colorPrimary)});
        gradientDrawable.setCornerRadius(8);
        view.setBackground(gradientDrawable);
    }

    public static void getBlueGreenGradient(int color, View view, boolean b) {
        int newColor = Color.argb(200, Color.red(color), Color.green(color), Color.blue(color));
        GradientDrawable.Orientation orientation = b ? GradientDrawable.Orientation.LEFT_RIGHT : GradientDrawable.Orientation.RIGHT_LEFT;
        GradientDrawable gradientDrawable = new GradientDrawable(
                orientation,
                new int[]{color, color, newColor});
        view.setBackground(gradientDrawable);
    }


    public static void setAddFabBackground(Context context, FloatingActionButton fab) {
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, R.drawable.ic_plus_icon, fab);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(ContextCompat.getColor(context, R.color.colorAccent));
    }

    public static void setImageAddBackgroundImage(Context context, ImageView image) {
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, R.drawable.ic_plus_icon, image);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(ContextCompat.getColor(context, R.color.colorAccent));
    }

    public static Drawable setImageBackgroundDrawable(Context context, ImageView image, int res) {
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, res, image);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(ContextCompat.getColor(context, R.color.colorAccent));
        return vector.getDrawable();
    }

    public static Drawable getBackgroundDrawable(Context context, TextView image, int res) {
        int color = ContextCompat.getColor(context, R.color.colorAccent);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, res, image);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        VectorDrawableCompat.VFullPath path3 = vector.findPathByName("path3");
        VectorDrawableCompat.VFullPath path4 = vector.findPathByName("path4");
        path1.setFillColor(color);
        if (path2 != null) path2.setFillColor(color);
        if (path3 != null) path3.setFillColor(color);
        if (path4 != null) path4.setFillColor(color);
        return vector.getDrawable();
    }

    public static void setImage(Context context, ImageView imgView, String photo) {
        if (photo != null && !photo.equalsIgnoreCase("")) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_user_placeholder)
                    .error(R.drawable.ic_user_placeholder);
            Glide.with(context)
                    .load(photo)
                    .apply(options)
                    .into(imgView);
        }

    }

    public static void setImagePlaceHolder(Context context, ImageView imgView, String photo) {
        if (photo != null && !photo.equalsIgnoreCase("")) {
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder);
            Glide.with(context)
                    .load(photo)
                    .apply(options)
                    .into(imgView);
        }

    }

    public static void setMenuItemDrawable(Context context, MenuItem image, int res) {
        int color = ContextCompat.getColor(context, R.color.colorAccent);
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, res, image);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        VectorDrawableCompat.VFullPath path2 = vector.findPathByName("path2");
        VectorDrawableCompat.VFullPath path3 = vector.findPathByName("path3");
        VectorDrawableCompat.VFullPath path4 = vector.findPathByName("path4");
        VectorDrawableCompat.VFullPath path5 = vector.findPathByName("path5");
        path1.setFillColor(color);
        if (path2 != null) path2.setFillColor(color);
        if (path3 != null) path3.setFillColor(color);
        if (path4 != null) path4.setFillColor(color);
        if (path5 != null) path5.setFillColor(color);
    }

    public static void setFabBackgroundWithDrawable(Context context, FloatingActionButton fab, int id) {
        VectorChildFinder vector = new VectorChildFinder(((BaseActivity) context).res, id, fab);
        VectorDrawableCompat.VFullPath path1 = vector.findPathByName("path1");
        path1.setFillColor(ContextCompat.getColor(context, R.color.colorAccent));
    }

    public static void setLines(Context context, View view) {
        view.setBackgroundColor(((BaseActivity) context).res.getColor(R.color.colorAccent));
    }

    public static void setSpinnerArrow(Context context, ImageView[] imageView) {
        LayerDrawable layerDrawable = (LayerDrawable) (((BaseActivity) context).res.getDrawable(R.drawable.shape_round_with_image));
        Drawable drawable = layerDrawable.findDrawableByLayerId(R.id.drawable);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            drawable.setTint(((BaseActivity) context).res.getColor(R.color.colorAccent));
        } else {
            Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(wrappedDrawable, ((BaseActivity) context).res.getColor(R.color.colorAccent));
        }
        for (ImageView image : imageView)
            image.setBackground(layerDrawable);
    }

    public static void setRadioButtonSelectors(Context mActivity, RadioButton button) {
        Drawable normal = ContextCompat.getDrawable(mActivity, R.drawable.radio_unchecked_new);
        Drawable pressed = ContextCompat.getDrawable(mActivity, R.drawable.radio_checked);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Objects.requireNonNull(pressed).setTint(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        } else {
            Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(pressed));
            DrawableCompat.setTint(wrappedDrawable, ((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        }
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{-android.R.attr.state_checked}, normal);
        states.addState(new int[]{android.R.attr.state_checked}, pressed);

//        for (RadioButton button : radioButtons)
        button.setButtonDrawable(states);
    }

    public static void setCheckBoxSelectors(Context mActivity, CheckBox button, int checked) {
        Drawable normal = ContextCompat.getDrawable(mActivity, R.drawable.unchecked);
        Drawable pressed = ContextCompat.getDrawable(mActivity, checked);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Objects.requireNonNull(pressed).setTint(((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        } else {
            Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(pressed));
            DrawableCompat.setTint(wrappedDrawable, ((BaseActivity) mActivity).res.getColor(R.color.colorAccent));
        }
        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{-android.R.attr.state_checked}, normal);
        states.addState(new int[]{android.R.attr.state_checked}, pressed);
        button.setButtonDrawable(states);
    }

    public static void createProgressBackground(Context context, ProgressBar progressBar) {
        LayerDrawable layerDrawable = (LayerDrawable) (((BaseActivity) context).res.getDrawable(R.drawable.shape_progress_circular_gradient));
        Drawable drawable = layerDrawable.findDrawableByLayerId(android.R.id.progress);
        drawable.setColorFilter(((BaseActivity) context).res.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        layerDrawable.setDrawableByLayerId(android.R.id.progress, drawable);
        progressBar.setProgressDrawable(layerDrawable);
    }

    public static void setGreenBackground(Context context, ConstraintLayout cardMain) {
        GradientDrawable layerDrawable = (GradientDrawable) ((BaseActivity) context).res.getDrawable(R.drawable.green_bg);
        if (layerDrawable != null) {
            layerDrawable.setStroke(dpToPx(context, 1), ((BaseActivity) context).res.getColor(R.color.colorAccent));
            cardMain.setBackground(layerDrawable);
        }
    }

    public static int dpToPx(Context context, int dp) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, ((BaseActivity) context).res.getDisplayMetrics()) + 0.5f);
    }

    public static void setRadioButtonDrawable(Context context, RadioButton radioButton) {
//        ColorDrawable colorDrawable = new ColorDrawable(ContextCompat.getColor(context, android.R.color.white));
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(Color.WHITE);
        gd.setCornerRadius(com.intuit.sdp.R.dimen._23sdp);
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,
                new int[]{ContextCompat.getColor(context, R.color.colorAccent),
                        ContextCompat.getColor(context, R.color.colorPrimary)});
        gradientDrawable.setCornerRadius(com.intuit.sdp.R.dimen._23sdp);

        StateListDrawable states = new StateListDrawable();
        states.addState(new int[]{-android.R.attr.state_checked}, gd);
        states.addState(new int[]{android.R.attr.state_checked}, gradientDrawable);
        radioButton.setBackground(states);
    }

    public static void setSwitchBG(Context context, SwitchCompat switchInput) {
       /* ColorStateList states = new ColorStateList();
        states.addState(new int[]{-android.R.attr.state_checked}, normal);
        states.addState(new int[]{android.R.attr.state_checked}, pressed);*/

        /*ColorStateList buttonStates = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked},
                        new int[]{android.R.attr.state_checked},
                        new int[]{}
                },
                new int[]{
                        Color.GRAY,
                        ContextCompat.getColor(context, R.color.colorAccent),
                        Color.GRAY
                }
        );
        switchInput.getThumbDrawable().setTintList(buttonStates);
        switchInput.getTrackDrawable().setTintList(buttonStates);*/
        int color = ContextCompat.getColor(context, R.color.colorAccent);
        ColorStateList thumbStates = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_enabled},
                        new int[]{android.R.attr.state_checked},
                        new int[]{}
                },
                new int[]{
                        Color.WHITE,
                        color,
                        Color.argb(200, Color.red(color), Color.green(color), Color.blue(color))

                }
        );
        switchInput.setThumbTintList(thumbStates);
    }

    public static void setSwitchTint(Context context, SwitchCompat switchCompat) {
        // thumb color of your choice
        int thumbColor = ContextCompat.getColor(context, R.color.colorAccent);

        // trackColor is the thumbColor with 30% transparency (77)
        int trackColor = Color.argb(77, Color.red(thumbColor), Color.green(thumbColor), Color.blue(thumbColor));

        // setting the thumb color
        DrawableCompat.setTintList(switchCompat.getThumbDrawable(), new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{}
                },
                new int[]{
                        thumbColor,
                        Color.WHITE
                }));

        // setting the track color
        DrawableCompat.setTintList(switchCompat.getTrackDrawable(), new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{}
                },
                new int[]{
                        trackColor,
                        Color.parseColor("#4D000000") // full black with 30% transparency (4D)
                }));
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                /*final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);*/
                if (!TextUtils.isEmpty(id)) {
                    if (id.startsWith("raw:")) {
                        return id.replaceFirst("raw:", "");
                    }
                    try {
                        final Uri contentUri = ContentUris.withAppendedId(
                                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                        return getDataColumn(context, contentUri, null, null);
                    } catch (NumberFormatException e) {
                        Log.e("FileUtils", "Downloads provider returned unexpected uri " + uri.toString(), e);
                        return null;
                    }
                }
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = "";
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }

    public static String extractYTId(String ytUrl) throws MalformedURLException {
        String vId = "";
        Pattern pattern = Pattern.compile(
                "(?:https?:/{2})?(?:w{3}.)?youtu(?:be)?.(?:com|be)(?:/watch?v=|/)([^s&]+)",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
            if (vId.startsWith("watch?v=")) {
                vId = vId.replace("watch?v=", "");
            } else if (vId.equals("")) {
                vId = extractYoutubeId(ytUrl);
            }
        }
        return vId;
    }

    private static String embedYId(String yUrl) throws MalformedURLException{

        String vId = "";


        return vId;


    }

}
