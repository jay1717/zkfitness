package com.fitzoh.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.fitzoh.app.R;
import com.fitzoh.app.Services.NotificationToken;
import com.fitzoh.app.model.UserDataModel;


public class SessionManager {
    public SharedPreferences.Editor editor;
    public static final String KEY_AUTHORIZED_USER = "KEY_AUTHORIZED_USER";

    private SharedPreferences pref;

    public static final String KEY_IS_LOGIN = "is_login";
    public static final String KEY_IS_SP = "is_service_provider";
    public static final String ROLL_ID = "RollId";

    private static final String KEY_NOTIFICATION_TOKEN = "KEY_NOTIFICATION_TOKEN";


    public SessionManager(Context context) {
        String PREF_NAME = context.getResources().getString(R.string.app_name);
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    public void setStringDataByKey(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringDataByKey(String key) {
        return pref.getString(key, "");
    }

    public String getStringDataByKeyNull(String key) {
        return pref.getString(key, null);
    }

    public int getIntegerDataByKey(String key) {
        return pref.getInt(key, 0);

    }
    public void saveNotificationToken(String token) {

        editor.putString(KEY_NOTIFICATION_TOKEN, token);
        editor.commit();
    }
    public String getNotificationToken() {
       return pref.getString(KEY_NOTIFICATION_TOKEN, "");
    }
    public boolean getBooleanDataByKey(String key) {
        return pref.getBoolean(key, false);
    }

    public void setBooleanDataByKey(String key, boolean isTrue) {
        editor.putBoolean(key, isTrue);
        editor.commit();
    }

    public void setLogin(Boolean isLogin) {
        editor.putBoolean(KEY_IS_LOGIN, isLogin);
        editor.commit();
    }

    public void setRollId(int roolId) {
        editor.putInt(ROLL_ID, roolId);
        editor.commit();
    }

    public int getRollId() {
        return pref.getInt(ROLL_ID, 0);
    }

    public void saveAuthorizedUser(Context context, UserDataModel authorizedUser) {
        Gson gson = new Gson();
        String json = gson.toJson(authorizedUser);
        editor.putString(KEY_AUTHORIZED_USER, json)
                .apply();
    }

    public UserDataModel getAuthorizedUser(Context context) {
        Gson gson = new Gson();
        String json = pref.getString(KEY_AUTHORIZED_USER, "");
        UserDataModel loginData = gson.fromJson(json, UserDataModel.class);
        return loginData;
    }

    public Boolean isLogin() {
        return pref.getBoolean(KEY_IS_LOGIN, false);
    }

    public void setLogout() {
        editor.clear().commit();
    }



}
