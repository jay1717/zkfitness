package com.fitzoh.app.utils;


public class AppConstants {

    static final String URL_PLAY_STORE_PACKAGE = "com.touchzenmedia.rawvanasmoothies";
    static final String URL_PLAY_STORE = "https://play.google.com/store/apps/details?id=" + URL_PLAY_STORE_PACKAGE + "&hl=en_US";

    public static final String TAB_BLENDER = "tab_blender_identifier";
    public static final String TAB_SHOPPING_LIST = "tab_shopping_list_identifier";
    public static final String TAB_FAVOURITE = "tab_favourite_identifier";
    public static final String TAB_GUIDE = "tab_guide_identifier";
    public static final String TAB_INFO = "tab_info_identifier";


    public static final String FRAGMENT_BOOKING_HISTORY = "fragment_booking_history";
    public static final String FRAGMENT_REVIEW = "fragment_review";


    public static final String HTML_HOW_IT_WORKS = "file:///android_asset/guide_howitworks.html";
    public static final String HTML_BLENDER_GUIDE = "file:///android_asset/guide_blender.html";
    public static final String HTML_SUBSTITUTIONS = "file:///android_asset/guide_substitutions.html";
    public static final String HTML_FAQ = "file:///android_asset/guide_faq.html";

    public static final int NUTRITION_CALORIE_PERCENTAGE = 2500;
    public static final int NUTRITION_CARBOHYDRATE_PERCENTAGE = 250;
    public static final int NUTRITION_PROTEIN_PERCENTAGE = 20;
    public static final int NUTRITION_FAT_PERCENTAGE = 9;

    //Info Screen URls
    // About Ravwana
    static final String INFO_RAVWANA_YOUTUBE_URL = "https://www.youtube.com/";
    static final String INFO_RAVWANA_WEBSITE_URL = "http://www.rawvana.com";

    //TouchZen Social Media ULRs
    static final String INFO_TOUCHZEN_WEBSITE_URL = "http://www.TouchZenMedia.com/";
    static final String INFO_TOUCHZEN_TWITTER_BROWSER_URL = "https://www.twitter.com/";
    static final String INFO_TOUCHZEN_TWITTER_APP_URL = "user?id=2649636883";
    static final String INFO_TOUCHZEN_MORE_APP_URL = "https://play.google.com/store/apps/developer?id=";

    static final String INFO_INSTAGRAM_BROWSER_URL = "https://www.instagram.com/";
    static final String INFO_INSTAGRAM_APP_URL = "instagram:user?username=";
    static final String INFO_FACEBOOK_BROWSER_URL = "https://www.facebook.com/";


    // WorkManager
    public static final String KEY_STORED_DAY = "stored_day";
    public static final String KEY_STORE_DAY = "store_day";
    // The name of the day increment work
    public static final String DAY_INCREMENT_WORK_NAME = "day_increment_work";

    public static final String LOGIN_KEY = "login_stack";
    public static final String NAVIGATION_KEY = "navigation_stack";
    public static final String CAR_TYPE = "car_type";
    public static final String CAR_TYPE_ID = "car_type_id";
    public static final String PERSONAL_BOOKING_DATA = "personal_booking_data";
    public static final String ASSIGN_CLIENT = "AssignClientFragment";

    public static final String CAR_TYPE_NAME = "car_type_name";

    public static final int SUCCESS = 200;
    public static final int CHECK_UPDATE = 500;
    public static final int INTRENAL_SERVER_ERROR = 500;
    public static final int TOKEN_EXPIRE = 600;
    public static final String CLIENT_ID = "client_id";
}
