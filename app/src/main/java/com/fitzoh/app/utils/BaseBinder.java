package com.fitzoh.app.utils;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fitzoh.app.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BaseBinder {
    @BindingAdapter({"app:setPhoto"})
    public static void setPhoto(ImageView iv, String photo) {
        if (photo != null) {
            Glide.with(iv.getContext()).load(photo).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    iv.setImageResource(R.drawable.placeholder);
                    return true;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, com.bumptech.glide.load.DataSource dataSource, boolean isFirstResource) {
                    return false;
                }


            }).into(iv);
        } else {
            iv.setImageResource(R.drawable.placeholder);
        }
    }

    @BindingAdapter({"app:setDateDetail"})
    public static void setDateDetail(TextView tv, String date) {
        if (date != null) {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;
            try {
                newDate = spf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            spf = new SimpleDateFormat("hh:mm aa");
            date = spf.format(newDate);
            tv.setText(date);
        }
    }

   /* @BindingAdapter({"app:setAddress"})
    public static void setAddress(TextView tv, AddressListModel.DataBean ad) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(ad.getAddress() + " " + ad.getCityTown() + " " + ad.getCountyState() + " " + ad.getPostalCode(),Html.FROM_HTML_MODE_COMPACT));
        } else
            tv.setText(Html.fromHtml(ad.getAddress() + " " + ad.getCityTown() + " " + ad.getCountyState() + " " + ad.getPostalCode()));
    }

    @BindingAdapter({"app:setAddresssDetail"})
    public static void setAddresssDetail(TextView tv, OrderDetailModel.DataBean ad) {
        if(ad!=null){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tv.setText(Html.fromHtml(ad.getAddressLine() + " " + ad.getCityTown() + " " + ad.getCountyState(),Html.FROM_HTML_MODE_COMPACT));
        } else
            tv.setText(Html.fromHtml(ad.getAddressLine() + " " + ad.getCityTown() + " " + ad.getCountyState()));}
    }

    @BindingAdapter({"app:setSrcComp"})
    public static void setSrcComp(ImageView iv, Drawable drawable) {
        iv.setImageDrawable(drawable);
    }

    @BindingAdapter({"app:setDateDetail"})
    public static void setDateDetail(TextView tv,String date){
        if(date!=null) {
            //2018-07-06T00:00:00"
            // String date="Mar 10, 2016 6:30:00 PM" "2018-07-06T00:00:00",;
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            Date newDate = null;
            try {
                newDate = spf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            spf = new SimpleDateFormat("dd MMMM,yyyy , hh:mm aa");
            date = spf.format(newDate);
            tv.setText(date);
        }
    }

    @BindingAdapter({"app:setDate"})
    public static void setDate(TextView tv,String date){
       // String date="Mar 10, 2016 6:30:00 PM" "2018-07-06T00:00:00",;
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date newDate= null;
        if(date!=null) {
            try {
                newDate = spf.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            spf = new SimpleDateFormat("dd/MM/yyyy");
            date = spf.format(newDate);
            tv.setText(date);
        }
    }
    @BindingAdapter({"app:setOrderStatus"})
    public static void setOrderStatus(TextView tv,int orderType){
        if(orderType==completed){
            tv.setText("Completed");
        }
        else if(orderType==upcoming){
            tv.setText("Upcoming");
        }
        else if(orderType==cancelled){
            tv.setText("Cancelled");
        }
    }*/
}
